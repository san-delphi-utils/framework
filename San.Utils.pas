{*******************************************************}
{                                                       }
{       Простые функции.                                }
{       Перегрузка стандартных функций.                 }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2017              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Простые функции. Перегрузка стандартных функций.
/// </summary>
unit San.Utils;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Variants
  {$ENDREGION}
  ;
{$ENDREGION}

type
  TStrProc = reference to procedure (const AStr : string);
{$ENDIF}

function Format(const AFormat: PResStringRec; const AArgs: array of const): string; overload;

procedure Split(const AString, ASeparator: string; const AOnSplit : TStrProc);

function Max(const AArgs : array of integer) : integer; overload;

function IIF(const ACondition : boolean; const ATrue, AFalse : Variant) : Variant; overload;
function IIF(const ACondition : boolean; const ATrue : Variant) : Variant; overload;

function NOA(const ACondition : boolean; const AExtCondition : boolean) : boolean;

{$IFNDEF USE_ALIASES}
implementation

{$I San.Utils.Impl.inc}

end{$WARNINGS OFF}.
{$ENDIF}
