{*******************************************************}
{                                                       }
{       Держатели ссылок.                               }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2013-2018         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
unit San.Intfs.Holders;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  System.SysUtils, System.Classes;
{$ENDREGION}
{$ENDIF}

type
  /// <summary>
  ///   Держатель объекта.
  /// </summary>
  /// <remarks>
  ///   Объект освобождается в деструкторе.
  /// </remarks>
  THolderObject =
  {$IFDEF USE_ALIASES}
    San.Intfs.Holders.THolderObject;
  {$ELSE}
    class(TInterfacedObject)
  strict private
    FObj : TObject;

  public
    constructor Create(const AObj : TObject);
    destructor Destroy; override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Держатель массива объектов.
  /// </summary>
  /// <remarks>
  ///   Объекты освобождаются в деструкторе.
  /// </remarks>
  THolderObjectArr =
  {$IFDEF USE_ALIASES}
    San.Intfs.Holders.THolderObjectArr;
  {$ELSE}
    class(TInterfacedObject)
  strict private
    FObjsArr : TArray<TObject>;

  public
    constructor Create(const AObjs : array of TObject);
    destructor Destroy; override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Держатель ссылки на процедуру.
  /// </summary>
  /// <remarks>
  ///   Процедура вызывается в деструкторе. МОЖЕТ УТЕЧ РЕФЕРЕНС!
  /// </remarks>
  THolderProcRef =
  {$IFDEF USE_ALIASES}
    San.Intfs.Holders.THolderProcRef;
  {$ELSE}
    class(TInterfacedObject)
  strict private
    FProcRef : TProc;

  public
    constructor Create(const AProcRef : TProc);
    destructor Destroy; override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Держатель ссылки на структуру Api.
  /// </summary>
  /// <remarks>
  ///   Api финализируется деструкторе.
  /// </remarks>
  THolderApi =
  {$IFDEF USE_ALIASES}
    San.Intfs.Holders.THolderApi;
  {$ELSE}
    class(TInterfacedObject)
  strict private
    FFinal : TProcedure;

  public
    constructor Create(const AFinal : TProcedure);
    destructor Destroy; override;

    class procedure Init(out AHolder : IInterface; const AInit, AFinal : TProcedure); overload;
    class procedure Init(out AHolder : IInterface; const AFinal : TProcedure); overload;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
implementation

{$REGION 'THolderObject'}
constructor THolderObject.Create(const AObj: TObject);
begin
  inherited Create();

  FObj := AObj;
end;

destructor THolderObject.Destroy;
begin
  FreeAndNil(FObj);
  inherited;
end;
{$ENDREGION}

{$REGION 'THolderObjectArr'}
constructor THolderObjectArr.Create(const AObjs: array of TObject);
var
  i, l : integer;
begin
  inherited Create();

  l := High(AObjs) + 1;

  SetLength(FObjsArr, l);

  for i := 0 to l - 1 do
    FObjsArr[i] := AObjs[i];
end;

destructor THolderObjectArr.Destroy;
var
  i : integer;
begin
  for i := 0 to High(FObjsArr) do
    FreeAndNil(FObjsArr[i]);

  inherited;
end;
{$ENDREGION}

{$REGION 'THolderProcRef'}
constructor THolderProcRef.Create(const AProcRef: TProc);
begin
  inherited Create();

  FProcRef := AProcRef;
end;

destructor THolderProcRef.Destroy;
begin
  FProcRef();

  inherited;
end;
{$ENDREGION}

{$REGION 'THolderApi'}
constructor THolderApi.Create(const AFinal : TProcedure);
begin
  inherited Create();

  Assert(Assigned(@AFinal));

  FFinal := AFinal;
end;

destructor THolderApi.Destroy;
begin
  FFinal();

  inherited;
end;

class procedure THolderApi.Init(out AHolder : IInterface; const AInit, AFinal : TProcedure);
begin
  if Assigned(@AInit) then
    AInit();

  AHolder := Self.Create(AFinal);
end;

class procedure THolderApi.Init(out AHolder: IInterface;
  const AFinal: TProcedure);
begin
  AHolder := Self.Create(AFinal);
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
