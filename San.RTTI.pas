{*******************************************************}
{                                                       }
{       Работа с RTTI.                                  }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2013-2016         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Работа с RTTI.
/// </summary>
unit San.Rtti;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  System.Sysutils, System.TypInfo, System.Rtti, System.Generics.Collections,

  San.Intfs.Holders, San.Excps;
{$ENDREGION}
{$ENDIF}
type
  /// <summary>
  ///   Указатель на контекст Rtti.
  /// </summary>
  PRttiContext =
  {$IFDEF USE_ALIASES}
    San.Rtti.PRttiContext;
  {$ELSE}
    ^TRttiContext;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
type

  /// <summary>
  ///   Доступ к методам работы с Rtti.
  /// </summary>
  Api = record
  private
    class var FHolder: IInterface;
    class procedure Initialize(); static;
    class procedure Finalize(); static;

  strict private
    type
      TPointerTRttiMethodDict       = TDictionary<Pointer,TRttiMethod>;
      TClassPointerTRttiMethodDict  = TObjectDictionary<TClass,TPointerTRttiMethodDict>;

  strict private
    class var FContext: PRttiContext;
    class var FClassesDict : TClassPointerTRttiMethodDict;
    class function GetContext: TRttiContext; static;
    class function GetClassesDict: TClassPointerTRttiMethodDict; static;

  private
    class property ClassesDict : TClassPointerTRttiMethodDict  read GetClassesDict;

  public
    /// <summary>
    ///   Попытка получить атрибут заданного класса RTTI-объекта.
    /// </summary>
    class function TryGetAttr(const ARttiObj : TRttiObject; const AAttrClass : TClass; out AAttr) : boolean; overload; static;

    /// <summary>
    ///   Попытка получить атрибут заданного класса RTTI-объекта.
    /// </summary>
    class function TryGetAttr(const ARttiObj : TRttiObject; const AAttrClass : TClass) : boolean; overload; static;

    /// <summary>
    ///   Попытка получить атрибут заданного класса RTTI-объекта.
    /// </summary>
    class function TryGetAttr<T : class>(const ARttiObj : TRttiObject; out AAttr : T) : boolean; overload; static;

    /// <summary>
    ///   Попытка получить атрибут заданного класса RTTI-объекта.
    /// </summary>
    class function TryGetAttr<T : class>(const ARttiObj : TRttiObject) : boolean; overload; static;

    /// <summary>
    ///   RTTI-объекта метода по адресу внутри метода в коде метода.
    /// </summary>
    /// <param name="AObj">
    ///   Объект.
    /// </param>
    class function MetodByAddr(const AObj: TObject) : TRttiMethod; static;

    /// <summary>
    ///   Держатель данных Api.
    /// </summary>
    class property Holder : IInterface  read FHolder;

    /// <summary>
    ///   Синглтон контекста RTTI.
    /// </summary>
    class property Context : TRttiContext  read GetContext;
  end;

(*
function IsField(P: Pointer): Boolean; inline;
begin
  Result := (IntPtr(P) and PROPSLOT_MASK) = PROPSLOT_FIELD;
end;

function GetPropGetterField(AProp : TRttiProperty) : TRttiField;
var
  LPropInfo : PPropInfo;
  LField: TRttiField;
  LOffset : Integer;
begin
  Result:=nil;
  //Is a readable property?
  if (AProp.IsReadable) and (AProp.ClassNameIs('TRttiInstancePropertyEx')) then
  begin
    //get the propinfo of the porperty
    LPropInfo:=TRttiInstanceProperty(AProp).PropInfo;
    //check if the GetProc represent a field
    if (LPropInfo<>nil) and (LPropInfo.GetProc<>nil) and IsField(LPropInfo.GetProc) then
    begin
      //get the offset of the field
      LOffset:= IntPtr(LPropInfo.GetProc) and PROPSLOT_MASK_F;
      //iterate over the fields of the class
      for LField in AProp.Parent.GetFields do
         //compare the offset the current field with the offset of the getter
         if LField.Offset=LOffset then
           Exit(LField);
    end;
  end;
end;
*)

implementation

{$REGION 'uses'}
uses
  {$REGION 'San'}
  San.Messages
  {$ENDREGION}
;
{$ENDREGION}

{$REGION 'Api'}
class procedure Api.Initialize();
begin
end;

class procedure Api.Finalize;
begin
  if Assigned(FContext) then
  begin
    FContext^.Free;
    Dispose(FContext);
  end;

  FreeAndNil(FClassesDict);
end;

class function Api.GetClassesDict: TClassPointerTRttiMethodDict;
begin
  if not Assigned(FClassesDict) then
    FClassesDict := TClassPointerTRttiMethodDict.Create([doOwnsValues]);

  Result := FClassesDict;
end;

class function Api.GetContext: TRttiContext;
begin

  if not Assigned(FContext) then
  begin
    New(FContext);
    FContext^ := TRttiContext.Create();
  end;

  Result := FContext^;
end;

class function Api.TryGetAttr(const ARttiObj: TRttiObject;
  const AAttrClass: TClass; out AAttr): boolean;
var
  Attrs : TArray<TCustomAttribute>;
  Attr : TCustomAttribute;
begin
  Attrs := ARttiObj.GetAttributes();

  for Attr in Attrs do
    if Attr.InheritsFrom(AAttrClass)  then
    begin
      TCustomAttribute(AAttr) := Attr;
      Exit(True);
    end;

  TCustomAttribute(AAttr) := nil;
  Result := False;
end;

class function Api.TryGetAttr(const ARttiObj: TRttiObject;
  const AAttrClass: TClass): boolean;
var
  Dummy : TCustomAttribute;
begin
  Result := Api.TryGetAttr(ARttiObj, AAttrClass, Dummy);
end;

class function Api.TryGetAttr<T>(const ARttiObj: TRttiObject;
  out AAttr: T): boolean;
var
  TI : PTypeInfo;
  Attr : TCustomAttribute;
begin
  TI := TypeInfo(T);
  Result := Api.TryGetAttr(ARttiObj, TI^.TypeData^.ClassType, Attr);
  AAttr := T(Attr);
end;

class function Api.TryGetAttr<T>(const ARttiObj: TRttiObject): boolean;
var
  Dummy : T;
begin
  Result := Api.TryGetAttr<T>(ARttiObj, Dummy);
end;

class function Api.MetodByAddr(const AObj: TObject): TRttiMethod;
var
  RetAttr : Pointer;
  PtrRttiMtdDict : TPointerTRttiMethodDict;

  function FindMtd : TRttiMethod;
  var
    RttiInstanceType : TRttiInstanceType;
    DeclaredMethods : TArray<TRttiMethod>;
    P : Pointer;
    i : integer;
  begin
    RttiInstanceType := Context.GetType(AObj.ClassType) as TRttiInstanceType;

    DeclaredMethods := RttiInstanceType.GetDeclaredMethods;

    P := RetAttr;
    repeat
      P := Pointer( NativeInt(P) - 1);

      for i := Low(DeclaredMethods) to High(DeclaredMethods) do
        if DeclaredMethods[i].CodeAddress = P then
        begin
          Result := DeclaredMethods[i];

          if not Assigned(PtrRttiMtdDict) then
          begin
            PtrRttiMtdDict := TPointerTRttiMethodDict.Create();
            FClassesDict.Add(AObj.ClassType, PtrRttiMtdDict);
          end;

          PtrRttiMtdDict.Add(RetAttr, Result);
          Exit;
        end;

      if NativeInt(P) < NativeInt(RetAttr) - 1024 then
        raise ESanRtti.CreateResFmt(@s_San_Rtti_MethodNotFoundForReturnAddressErrorFmt, [RetAttr]);

    until FALSE;

  end;

begin
  RetAttr := ReturnAddress;

  if not (ClassesDict.TryGetValue(AObj.ClassType, PtrRttiMtdDict) or PtrRttiMtdDict.TryGetValue(RetAttr, Result) ) then
    Result := FindMtd;
end;
{$ENDREGION}

initialization
  THolderApi.Init(Api.FHolder, Api.Initialize, Api.Finalize);

end{$WARNINGS OFF}.
{$ENDIF}
