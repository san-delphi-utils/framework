{*******************************************************}
{                                                       }
{       Абстрактный тест                                }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2014-2016         Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   Абстрактный тест
/// </summary>
unit San.Test.Custom;
{$I San.inc}
interface

uses
  {$REGION 'WinApi'}
  WinApi.Windows,
  {$ENDREGION}

  {$REGION 'System'}
  System.SysUtils, System.Classes, System.TypInfo, System.Generics.Defaults,
  System.Rtti,
  {$ENDREGION}

  {$REGION 'San'}
  {$ENDREGION}

  {$REGION 'DUnit'}
  TestFramework
  {$ENDREGION}
;

type
  TSanTestCase = class abstract(TTestCase)
  private
    FCoroutinesCounter : integer;

  protected
    procedure SetUp; override;
    procedure TearDown; override;

    procedure ValidateCoroutinesEmpty;

    class function GetTypeInfo : PTypeInfo; virtual;

  public
    class function Suite: ITestSuite; override;

    procedure WaitForCoroutines(const ATimeOut : Cardinal = 10000);
    procedure StartCoroutine(const AProc : TThreadProcedure);

    procedure CheckEqualsGen<T>(expected, actual: T; msg: string);
    procedure CheckNotEqualsGen<T>(expected, actual: T; msg: string);

  end;

  TSanTestCase<T> = class(TSanTestCase)
  protected
    class function GetTypeInfo : PTypeInfo; override;
  end;

implementation

type
  TCoroutineThread = class(TThread)
  strict private
    FCoroutinesCounterPtr : PInteger;
    FCoroutine : TThreadProcedure;

  protected
    procedure Execute; override;

  public
    constructor Create(const ACoroutinesCounterPtr : PInteger; const ACoroutine : TThreadProcedure);
    destructor Destroy; override;

  end;

{$REGION 'TCoroutineThread'}
constructor TCoroutineThread.Create(const ACoroutinesCounterPtr: PInteger;
  const ACoroutine: TThreadProcedure);
begin
  FreeOnTerminate := True;

  FCoroutinesCounterPtr := ACoroutinesCounterPtr;
  FCoroutine := ACoroutine;

  AtomicIncrement(FCoroutinesCounterPtr^);
  inherited Create;
end;

destructor TCoroutineThread.Destroy;
begin
  AtomicDecrement(FCoroutinesCounterPtr^);
  inherited;
end;

procedure TCoroutineThread.Execute;
begin
  FCoroutine();
end;
{$ENDREGION}

{$REGION 'TSanTestCase'}
class function TSanTestCase.GetTypeInfo: PTypeInfo;
begin
  Result := nil;
end;

class function TSanTestCase.Suite: ITestSuite;
var
  S : TTestSuite;
begin
  S := TTestSuite.Create( GetTypeInfo^.NameFld.ToString() );
  S.AddTests(self);

  Result := S;
end;

procedure TSanTestCase.SetUp;
begin
  inherited;
  ValidateCoroutinesEmpty;
end;

procedure TSanTestCase.TearDown;
begin
  ValidateCoroutinesEmpty;
  inherited;
end;

procedure TSanTestCase.ValidateCoroutinesEmpty;
begin
  Check(AtomicCmpExchange(FCoroutinesCounter, 0, 0) = 0, 'Coroutines is not empty');
end;

procedure TSanTestCase.StartCoroutine(const AProc: TThreadProcedure);
begin
  TCoroutineThread.Create(@FCoroutinesCounter, AProc);
end;

procedure TSanTestCase.WaitForCoroutines(const ATimeOut : Cardinal = 10000);
var
  StartTicks : Cardinal;
begin
  StartTicks := TThread.GetTickCount();

  while (AtomicCmpExchange(FCoroutinesCounter, 0, 0) <> 0) do
  begin
    if TThread.GetTickCount() - StartTicks > ATimeOut then
      Check(False, 'WaitForCoroutines time is out');
  end;

end;

procedure TSanTestCase.CheckEqualsGen<T>(expected, actual: T; msg: string);
var
  Cmp : IEqualityComparer<T>;
begin
  Cmp := TEqualityComparer<T>.Default;
  FCheckCalled := True;
  if not Cmp.Equals(expected, actual) then
    FailNotEquals(TValue.From<T>(expected).AsString, TValue.From<T>(actual).AsString, msg, ReturnAddress);
end;

procedure TSanTestCase.CheckNotEqualsGen<T>(expected, actual: T; msg: string);
var
  Cmp : IEqualityComparer<T>;
begin
  Cmp := TEqualityComparer<T>.Default;
  FCheckCalled := True;
  if Cmp.Equals(expected, actual) then
    FailEquals(TValue.From<T>(expected).AsString, TValue.From<T>(actual).AsString, msg, ReturnAddress);
end;
{$ENDREGION}

{$REGION 'TSanTestCase<T>'}
class function TSanTestCase<T>.GetTypeInfo: PTypeInfo;
begin
  Result := TypeInfo(T);
end;
{$ENDREGION}

end.
