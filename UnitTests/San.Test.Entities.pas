{*******************************************************}
{                                                       }
{       Динамическая структура данных и метаданных.     }
{       Тесты                                           }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   Работа с сущностями. Тесты
/// </summary>
unit San.Test.Entities;
{$I San.inc}
{$DEFINE USE_INDEXES}
interface

uses
  {$REGION 'WinApi'}
  WinApi.Windows,
  {$ENDREGION}

  {$REGION 'System'}
  System.SysUtils, System.Classes, System.TypInfo, System.Math, System.StrUtils,
  System.JSON,
  {$ENDREGION}

  {$REGION 'San'}
   San.Utils, San.Events, San.Entities,
  {$ENDREGION}

  {$REGION 'DUnit'}
  TestFramework,
  {$ENDREGION}

  {$REGION 'San.Test'}
  San.Test.Custom
  {$ENDREGION}
;

type
  TestEntities = class(TSanTestCase<TEntitiesObject>, IEventsSubscriber)
  private
    FGuard: IEventsSubscribeGuard;
    FMetaRoot : TMetaEntityRoot;
    FEntitiesEventHandlerCallBack : TProc<TMEEntities.Event>;

  protected
    procedure SetUp; override;
    procedure TearDown; override;

    {$REGION 'IEventsSubscriber'}
    function GetGuard : IEventsSubscribeGuard;
    {$ENDREGION}

  public
    class function Suite: ITestSuite; override;

    procedure EntitiesEventHandler(const AEvent : TMEEntities.Event);

  published
    procedure MetaFieldsAdd;
    procedure MetaFieldsAddGen;

    procedure DataObjs;

    procedure LexicalAnalize;
    procedure SintaxAnalize;
    procedure PathFindMeta;

    procedure Links;
    procedure Recursion;

    procedure MetaExportJSON;
    procedure DataExportJSON;
  end;

implementation

{$REGION 'TestEntities'}
class function TestEntities.Suite: ITestSuite;
var
  S : TTestSuite;
begin
  S := TTestSuite.Create('Entities');
  S.AddTests(self);

  Result := S;
end;

procedure TestEntities.SetUp;
begin
  inherited;
  FMetaRoot := TMetaEntityRoot.Create();
  FMetaRoot.Name := 'TestRoot';
  FMetaRoot.Version := 1;
end;

procedure TestEntities.TearDown;
begin
  FreeAndNil(FMetaRoot);
  FGuard := nil;
  FEntitiesEventHandlerCallBack := nil;
  inherited;
end;

procedure TestEntities.EntitiesEventHandler(const AEvent: TMEEntities.Event);
begin
  if Assigned(FEntitiesEventHandlerCallBack) then
    FEntitiesEventHandlerCallBack(AEvent);
end;

function TestEntities.GetGuard: IEventsSubscribeGuard;
begin
  if not Assigned(FGuard) then
    FGuard := FMetaRoot.EventSpace.CreateEventsSubscribeGuard(Self);

  Result := FGuard;
end;

procedure TestEntities.MetaFieldsAdd;
var
  IntFld : TMetaFieldInteger;
  ExtFld : TMetaFieldExtended;
  EnFld : TMetaFieldEntity;
  ClFld : TMetaFieldCollection;
  tool : TMetaEntity;
  StrFld : TMetaFieldString;
begin
  Check(Assigned(FMetaRoot), 'Assigned');

  IntFld := TMetaFieldInteger.Create(FMetaRoot);
  FMetaRoot.Fields.Add(IntFld);
  IntFld.Name := 'Tag';
  Check(IntFld.Parent = FMetaRoot, '!(IntFld.Parent = FMetaRoot)');

  EnFld := TMetaFieldEntity.Create(FMetaRoot);
  FMetaRoot.Fields.Add(EnFld);
  EnFld.Name := 'Base';
  Check(EnFld.Parent = FMetaRoot, '!(EnFld.Parent = FMetaRoot)');

  ExtFld := TMetaFieldExtended.Create(EnFld.Entity);
  EnFld.Entity.Fields.Add(ExtFld);
  ExtFld.Name := 'Price';
  Check(ExtFld.Parent = EnFld.Entity, '!(ExtFld.Parent = EnFld.Entity)');

  ClFld := TMetaFieldCollection.Create(FMetaRoot);
  FMetaRoot.Fields.Add(ClFld);
  ClFld.Name := 'Tools';
  Check(ClFld.Parent = FMetaRoot, '!(ClFld.Parent = FMetaRoot)');

  tool := ClFld.Collection.ItemMeta;

  StrFld := TMetaFieldString.Create(tool);
  tool.Fields.Add(StrFld);
  StrFld.Name := 'Caption';
  Check(StrFld.Parent = tool, '!(StrFld.Parent = tool)');

  tool.Ancestor := EnFld.Entity;
  CheckEquals(2, tool.Fields.Count, 'tool.Fields.Count');
  CheckEquals(ExtFld.Name, tool.Fields[1].Name, 'tool.Fields[1].Name');
end;

procedure TestEntities.MetaFieldsAddGen;
var
  IntFld : TMetaFieldInteger;
  ExtFld : TMetaFieldExtended;
  EnFld : TMetaFieldEntity;
  ClFld : TMetaFieldCollection;
  tool : TMetaEntity;
  StrFld : TMetaFieldString;
begin
  Check(Assigned(FMetaRoot), 'Assigned');

  IntFld := FMetaRoot.Fields.Add<TMetaFieldInteger>('Tag');
  Check(IntFld.Parent = FMetaRoot, '!(IntFld.Parent = FMetaRoot)');

  EnFld := FMetaRoot.Fields.Add<TMetaFieldEntity>('Base');
  Check(EnFld.Parent = FMetaRoot, '!(EnFld.Parent = FMetaRoot)');

  ExtFld := EnFld.Entity.Fields.Add<TMetaFieldExtended>('Price');
  Check(ExtFld.Parent = EnFld.Entity, '!(ExtFld.Parent = EnFld.Entity)');

  ClFld := FMetaRoot.Fields.Add<TMetaFieldCollection>('Tools');
  Check(ClFld.Parent = FMetaRoot, '!(ClFld.Parent = FMetaRoot)');

  tool := ClFld.Collection.ItemMeta;

  StrFld := tool.Fields.Add<TMetaFieldString>('Caption');
  Check(StrFld.Parent = tool, '!(StrFld.Parent = FMetaRoot)');

  tool.Ancestor := EnFld.Entity;
  CheckEquals(2, tool.Fields.Count, 'tool.Fields.Count');
  CheckEquals(ExtFld.Name, tool.Fields[1].Name, 'tool.Fields[1].Name');
end;

procedure TestEntities.DataObjs;
var
  DataRoot : TDataEntityRoot;
  Fired : boolean;
begin
  MetaFieldsAddGen;

  FMetaRoot.MakeDataRaw(DataRoot);
  Check(Assigned(DataRoot), 'Assigned');
  Check(Assigned(DataRoot.Root), 'Assigned DataRoot.Root');

  FMetaRoot.IsEventsEnabled := True;

  DataRoot.MetaEvent.Subscribe(@TestEntities.EntitiesEventHandler, Self).Std();

  Fired := False;
  FEntitiesEventHandlerCallBack := procedure (AEvent: TMEEntities.Event)
  begin
    Fired := True;
    CheckEquals(1, Length(AEvent.Context.Events), 'Length(AEvent.Context.Events)');

    CheckEquals('Tag', (AEvent.Context.Events[0] as TEntitiesChangeEvent).Field.Name, '(AEvent.Context.Events[0] as TEntitiesChangeEvent).Field.Name');
    CheckEquals(0, (AEvent.Context.Events[0] as TEntitiesChangeEvent).OldValue.AsInteger, '(AEvent.Context.Events[0] as TEntitiesChangeEvent).OldValue.AsInteger');
    CheckEquals(15, (AEvent.Context.Events[0] as TEntitiesChangeEvent).NewValue.AsInteger, '(AEvent.Context.Events[0] as TEntitiesChangeEvent).NewValue.AsInteger');

  end;

  DataRoot.Fields['Tag'].AsInteger := 15;
  CheckEquals(15, DataRoot.Fields['Tag'].AsInteger, 'Tag field');

  Check(Fired, 'Event handler not fired');
end;

procedure TestEntities.LexicalAnalize;
var
  Counter : integer;

  function Lx(const AKind : TPathResolver.TLexemeKind; const AStart, ALength : integer) : TPathResolver.TLexeme;
  begin
    Result := TPathResolver.TLexeme.Create(AKind, AStart, ALength);
  end;

  procedure ChLexemesKinds(const AText : string; const ALexemes : array of TPathResolver.TLexeme);
  var
    Lexemes : TArray<TPathResolver.TLexeme>;
    i, eL, aL : integer;
    e, a : TPathResolver.TLexeme;
    H, ek, ak, D : string;
  begin
    Inc(Counter);
    Lexemes := TPathResolver.LexicalAnalize(AText);

    H := Format('#%d.3 lexemes for '#13#10'%s', [Counter, AText]);
    CheckEquals(Length(ALexemes), Length(Lexemes),  H);

    for i := 0 to High(Lexemes) do
    begin
      e := ALexemes[i];
      a := Lexemes[i];

      if e = a then
      begin
        Check(True);
        Continue;
      end;

      ek := e.Kind.ToString;
      ak := a.Kind.ToString;

      eL := Max([Length('Expected'), Length(ek), Length(e.Start.ToString), Length(e.Length.ToString)]);
      aL := Max([Length('Actual'),   Length(ak), Length(a.Start.ToString), Length(a.Length.ToString)]);

      D := 'Index in array ' + i.ToString                                                                                            + #13#10
         + '         | Expected' + DupeString(' ', eL - Length('Expected'))                      + '  | Actual'                      + #13#10
         + '---------+-' + DupeString('-', eL)                                                   + '--+-' + DupeString('-', aL + 2)  + #13#10
         + ' Kind    | ' + ek                 + DupeString(' ', eL - Length(ek))                 + '  | ' + ak                       + #13#10
         + ' Start   | ' + e.Start.ToString   + DupeString(' ', eL - Length(e.Start.ToString))   + '  | ' + a.Start.ToString         + #13#10
         + ' Length  | ' + e.Length.ToString  + DupeString(' ', eL - Length(e.Length.ToString))  + '  | ' + a.Length.ToString        + #13#10
         + '---------+-' + DupeString('-', eL)                                                   + '--+-' + DupeString('-', aL + 2)  + #13#10
         + ' Expected value: «' + e.Value[AText] + '»'#13#10
         + ' Actual   value: «' + a.Value[AText] + '»';

      Check(False, H + #13#10 + D);
    end;

  end;

begin
  Counter := 0;

  // #001
  ChLexemesKinds('e',
    [
      Lx(lxkIdentifier, 1, 1)
    ]
  );

  // #002
  ChLexemesKinds('@root.tool',
    [
      Lx(lxkSysIdentifierRoot, 1, 5),
      Lx(lxkDot, 6, 1),
      Lx(lxkIdentifier, 7, 4)
    ]
  );

  // #003
  ChLexemesKinds('@parent.tools[@this.ID != ID && @this.AutoNo > AutoNo]',
    [
      Lx(lxkSysIdentifierParent, 1, 7),
      Lx(lxkDot, 8, 1),
      Lx(lxkIdentifier, 9, 5),
      Lx(lxkSquareBracketOpen, 14, 1),
      Lx(lxkSysIdentifierThis, 15, 5),
      Lx(lxkDot, 20, 1),
      Lx(lxkIdentifier, 21, 2),
      Lx(lxkNotEquals, 24, 2),
      Lx(lxkIdentifier, 27, 2),
      Lx(lxkAnd, 30, 2),
      Lx(lxkSysIdentifierThis, 33, 5),
      Lx(lxkDot, 38, 1),
      Lx(lxkIdentifier, 39, 6),
      Lx(lxkGreaterThan, 46, 1),
      Lx(lxkIdentifier, 48, 6),
      Lx(lxkSquareBracketClose, 54, 1)
    ]
  );

  // #004
  ChLexemesKinds('children[/* не знаю что написать...  */]',
    [
      Lx(lxkIdentifier, 1, 8),
      Lx(lxkSquareBracketOpen, 9, 1),
      Lx(lxkSquareBracketClose, 40, 1)
    ]
  );

  // #005
  ChLexemesKinds('@root.catigories[@this.AutoNo == AutoNo]',
    [
      Lx(lxkSysIdentifierRoot, 1, 5),
      Lx(lxkDot, 6, 1),
      Lx(lxkIdentifier, 7, 10),
      Lx(lxkSquareBracketOpen, 17, 1),
      Lx(lxkSysIdentifierThis, 18, 5),
      Lx(lxkDot, 23, 1),
      Lx(lxkIdentifier, 24, 6),
      Lx(lxkEquals, 31, 2),
      Lx(lxkIdentifier, 34, 6),
      Lx(lxkSquareBracketClose, 40, 1)
    ]
  );

  // #006
  ChLexemesKinds('@root.Categories[ID == 11]',
    [
      Lx(lxkSysIdentifierRoot, 1, 5),
      Lx(lxkDot, 6, 1),
      Lx(lxkIdentifier, 7, 10),
      Lx(lxkSquareBracketOpen, 17, 1),
      Lx(lxkIdentifier, 18, 2),
      Lx(lxkEquals, 21, 2),
      Lx(lxkNumber, 24, 2),
      Lx(lxkSquareBracketClose, 26, 1)
    ]
  );

  // #007
  ChLexemesKinds('children[Name == ''Green'']',
    [
      Lx(lxkIdentifier, 1, 8),
      Lx(lxkSquareBracketOpen, 9, 1),
      Lx(lxkIdentifier, 10, 4),
      Lx(lxkEquals, 15, 2),
      Lx(lxkString, 19, 5),
      Lx(lxkSquareBracketClose, 25, 1)
    ]
  );

  // #008
  ChLexemesKinds( QuotedStr('Это текст: ' + QuotedStr('Текст')),
    [
      Lx(lxkString, 2, 20)
    ]
  );


end;

procedure TestEntities.SintaxAnalize;

  procedure Ch(const AText : string; const AAction : TProc<TPathResolver.TPathItem>);
  var
    PathItem : TPathResolver.TPathItem;
  begin
    PathItem := TPathResolver.SintaxAnalize(AText);
    try

      AAction(PathItem);

    finally
      FreeAndNil(PathItem);
    end;
  end;

begin
  Ch('@root',
    procedure (APathItem : TPathResolver.TPathItem)
    begin
      Check(APathItem is TPathResolver.TPathItemSysRoot, 'APathItem is TPathResolver.TPathItemSysRoot');
      Check(not Assigned(APathItem.Child), 'End of path');
    end
  );

  Ch('@parent.Figures.Area',
    procedure (APathItem : TPathResolver.TPathItem)
    var
      Figures, Area : TPathResolver.TPathItemIdentifier;
    begin
      Check(APathItem is TPathResolver.TPathItemSysParent, 'APathItem is TPathResolver.TPathItemSysParent');

      Check(APathItem.Child is TPathResolver.TPathItemIdentifier, 'APathItem.Child is TPathResolver.TPathItemIdentifier');
      Figures := APathItem.Child as TPathResolver.TPathItemIdentifier;
      CheckEquals('Figures', Figures.Name, 'Figures.Name');

      Check(Figures.Child is TPathResolver.TPathItemIdentifier);
      Area := Figures.Child as TPathResolver.TPathItemIdentifier;
      CheckEquals('Area', Area.Name, 'Area.Name');

      Check(not Assigned(Area.Child), 'End of path');
    end
  );

  Ch('@root.Categories[AutoNo == @this.AutoNo]',
    procedure (APathItem : TPathResolver.TPathItem)
    var
      Categories : TPathResolver.TPathItemFilter;
      Condition : TPathResolver.TPathFilterFieldsCompareCondition;
      LAutoNo, RAutoNo : TPathResolver.TPathItemIdentifier;
    begin
      Check(APathItem is TPathResolver.TPathItemSysRoot, 'APathItem is TPathResolver.TPathItemSysRoot');

      Check(APathItem.Child is TPathResolver.TPathItemFilter, 'APathItem.Child is TPathResolver.TPathItemFilter');
      Categories := APathItem.Child as TPathResolver.TPathItemFilter;
      CheckEquals('Categories', Categories.Name, 'Categories.Name');
      Check(not Assigned(Categories.Child), 'End of path');

      Check(Categories.Condition is TPathResolver.TPathFilterFieldsCompareCondition, 'Categories.Condition is TPathResolver.TPathFilterFieldsCompareCondition');
      Condition := Categories.Condition as TPathResolver.TPathFilterFieldsCompareCondition;

      Check(Condition.Left is TPathResolver.TPathItemIdentifier, 'Condition.Left is TPathResolver.TPathItemIdentifier');
      LAutoNo := Condition.Left as TPathResolver.TPathItemIdentifier;
      CheckEquals('AutoNo', LAutoNo.Name, 'LAutoNo.Name');

      Check(Condition.Right is TPathResolver.TPathItemSysThis, 'Condition.Right is TPathResolver.TPathItemSysThis');
      Check(Condition.Right.Child is TPathResolver.TPathItemIdentifier, 'Condition.Right.Child is TPathResolver.TPathItemIdentifier');
      RAutoNo := Condition.Right.Child as TPathResolver.TPathItemIdentifier;
      CheckEquals('AutoNo', RAutoNo.Name, 'RAutoNo.Name');
    end
  );

  Ch('@root.rocks[ID == 11]',
    procedure (APathItem : TPathResolver.TPathItem)
    var
      Rocks : TPathResolver.TPathItemFilter;
      Condition : TPathResolver.TPathFilterFieldsCompareCondition;
    begin
      Check(APathItem is TPathResolver.TPathItemSysRoot, 'APathItem is TPathResolver.TPathItemSysRoot');

      Rocks := APathItem.Child as TPathResolver.TPathItemFilter;
      Condition := Rocks.Condition as TPathResolver.TPathFilterFieldsCompareCondition;

      Check(Condition.Left is TPathResolver.TPathItemIdentifier, 'Condition.Left is TPathResolver.TPathItemIdentifier');
      Check(Condition.Right is TPathResolver.TPathItemInt64, 'Condition.Right is TPathResolver.TPathItemInt64');
      CheckEquals(11, TPathResolver.TPathItemInt64(Condition.Right).Value, 'TPathResolver.TPathItemInt64(Condition.Right).Value');

      Check(not Assigned(Rocks.Child), 'End of path');
    end
  );

  Ch('@root.rocks[ID == -11]',
    procedure (APathItem : TPathResolver.TPathItem)
    var
      Rocks : TPathResolver.TPathItemFilter;
      Condition : TPathResolver.TPathFilterFieldsCompareCondition;
    begin
      Check(APathItem is TPathResolver.TPathItemSysRoot, 'APathItem is TPathResolver.TPathItemSysRoot');

      Rocks := APathItem.Child as TPathResolver.TPathItemFilter;
      Condition := Rocks.Condition as TPathResolver.TPathFilterFieldsCompareCondition;

      Check(Condition.Left is TPathResolver.TPathItemIdentifier, 'Condition.Left is TPathResolver.TPathItemIdentifier');
      Check(Condition.Right is TPathResolver.TPathItemInt64, 'Condition.Right is TPathResolver.TPathItemInt64');
      CheckEquals(-11, TPathResolver.TPathItemInt64(Condition.Right).Value, 'TPathResolver.TPathItemInt64(Condition.Right).Value');

      Check(not Assigned(Rocks.Child), 'End of path');
    end
  );

  Ch('@root.rocks[Name == ''Мрамор'']',
    procedure (APathItem : TPathResolver.TPathItem)
    var
      Rocks : TPathResolver.TPathItemFilter;
      Condition : TPathResolver.TPathFilterFieldsCompareCondition;
    begin
      Check(APathItem is TPathResolver.TPathItemSysRoot, 'APathItem is TPathResolver.TPathItemSysRoot');

      Rocks := APathItem.Child as TPathResolver.TPathItemFilter;
      Condition := Rocks.Condition as TPathResolver.TPathFilterFieldsCompareCondition;

      Check(Condition.Left is TPathResolver.TPathItemIdentifier, 'Condition.Left is TPathResolver.TPathItemIdentifier');
      Check(Condition.Right is TPathResolver.TPathItemString, 'Condition.Right is TPathResolver.TPathItemString');
      CheckEquals('Мрамор', TPathResolver.TPathItemString(Condition.Right).Value, 'TPathResolver.TPathItemString(Condition.Right).Value');

      Check(not Assigned(Rocks.Child), 'End of path');
    end
  );
end;

procedure TestEntities.PathFindMeta;
var
  MFldCategories : TMetaFieldCollection;
  MFldPart : TMetaFieldEntity;

  PathItem : TPathResolver.TPathItem;
  MetaNode : TMetaNode;

  DataRoot : TDataEntityRoot;
  Cats : TDataCollection;
  Cat1, Cat2, Part : TDataEntity;
  DataNode : TDataNode;
begin
  MFldCategories := FMetaRoot.Fields.Add<TMetaFieldCollection>('Categories');
  MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldInteger>('ID');
  MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldString>('Name');

  MFldPart := FMetaRoot.Fields.Add<TMetaFieldEntity>('Part');
  MFldPart.Entity.Fields.Add<TMetaFieldInteger>('CatID');
  MFldPart.Entity.Fields.Add<TMetaFieldEntity>('Category');

  PathItem := TPathResolver.SintaxAnalize('@Root.Categories[ID == @this.CatID]');
  try

    MetaNode := PathItem.FindEndTargetMeta(enntField, MFldPart.Entity, MFldPart.Entity);
    Check(MetaNode = MFldCategories, 'MetaNode');

    FMetaRoot.MakeDataRaw(DataRoot);

    Cats := DataRoot.Fields['Categories'].AsItems;

    Cat1 := Cats.Add();
    Cat1.Fields['ID'].AsInt := 11;
    Cat1.Fields['Name'].AsStr := 'Овощи';

    Cat2 := Cats.Add();
    Cat2.Fields['ID'].AsInt := 56;
    Cat2.Fields['Name'].AsStr := 'Фрукты';

    Part := DataRoot.Fields['Part'].AsEntity;
    Part.Fields['CatID'].AsInt := 56;

    DataNode := PathItem.FindEndTargetData(enntEntity, Part, Part);
    Check(DataNode = Cat2, 'DataNode');

  finally
    FreeAndNil(PathItem);
  end;

end;

procedure TestEntities.Links;
var
  MFldCategories : TMetaFieldCollection;
  MFldCategoryID, MFldPartCategoryID : TMetaFieldInteger;
  MFldCategoryName, MFldPartCategoryName : TMetaFieldString;
  MFldPart, MFldCategory : TMetaFieldEntity;

  DataRoot : TDataEntityRoot;
  Cats : TDataCollection;
  Cat1, Cat2, Part, PartCat : TDataEntity;
begin
  MFldCategories := FMetaRoot.Fields.Add<TMetaFieldCollection>('Categories');
  MFldCategoryID    := MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldInteger>('ID');
  MFldCategoryName  := MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldString>('Name');
  Check(Assigned(MFldCategoryID));

  MFldPart := FMetaRoot.Fields.Add<TMetaFieldEntity>('Part');
  MFldPartCategoryID  := MFldPart.Entity.Fields.Add<TMetaFieldInteger>('CatID');
  MFldCategory        := MFldPart.Entity.Fields.Add<TMetaFieldEntity>('Category');
  Check(Assigned(MFldPartCategoryID));

  MFldCategory.Entity.Link := '@Root.Categories[ID == @this.CatID]';
  Check(MFldCategory.Entity.Target = MFldCategories.Collection.ItemMeta, 'MFldCategory.Entity.Target != MFldCategories.Collection.ItemMeta');

  Check(MFldCategory.Entity.Fields.TryFindByName('Name', MFldPartCategoryName), 'TryFindByName');
  Check(MFldPartCategoryName = MFldCategoryName, '==');


  FMetaRoot.MakeDataRaw(DataRoot);

  Cats := DataRoot.Fields['Categories'].AsItems;

  Cat1 := Cats.Add();
  Cat1.Fields['ID'].AsInt := 11;
  Cat1.Fields['Name'].AsStr := 'Овощи';

  Cat2 := Cats.Add();
  Cat2.Fields['ID'].AsInt := 56;
  Cat2.Fields['Name'].AsStr := 'Фрукты';

  Part := DataRoot.Fields['Part'].AsEntity;
  Part.Fields['CatID'].AsInt := 56;
  PartCat := Part.Fields['Category'].AsEntity;

  Check(PartCat.Target = Cat2, 'Data');
end;

procedure TestEntities.Recursion;
var
  MFldCategories, MFldCategoryChildren : TMetaFieldCollection;
  MFldCategoryID : TMetaFieldInteger;
  MFldCategoryRoot : TMetaFieldEntity;

  DataRoot : TDataEntityRoot;
  Cats, CatsLinks : TDataCollection;
  CatRoot, Cat0, Cat1, Cat2 : TDataEntity;
  {$IFDEF USE_INDEXES}
  DataIndex : TDataCollectionIndexSingleInteger;
  {$ENDIF}

begin
  MFldCategories := FMetaRoot.Fields.Add<TMetaFieldCollection>('Categories');
  MFldCategoryID        := MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldInteger>('ID');
  MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldString>('Name');
  MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldInteger>('ParentID');
  MFldCategoryChildren  := MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldCollection>('Children');
  MFldCategoryChildren.Collection.Link := '@root.Categories[ParentID == @this.ID]';

  {$IFDEF USE_INDEXES}
  MFldCategories.Collection.Indexes.AddSingleIndex(MFldCategoryID, True);
  {$ENDIF}

  MFldCategoryRoot := FMetaRoot.Fields.Add<TMetaFieldEntity>('RootCategory');
  MFldCategoryRoot.Entity.Link := '@root.Categories[ID == 1]';


  FMetaRoot.MakeDataRaw(DataRoot);

  Cats := DataRoot.Fields['Categories'].AsItems;

  Cat0 := Cats.Add();
  Cat0.Fields['ID'].AsInt := 1;
  Cat0.Fields['Name'].AsStr := 'Еда';

  Cat1 := Cats.Add();
  Cat1.Fields['ID'].AsInt := 11;
  Cat1.Fields['Name'].AsStr := 'Овощи';
  Cat1.Fields['ParentID'].AsInt := 1;

  Cat2 := Cats.Add();
  Cat2.Fields['ID'].AsInt := 56;
  Cat2.Fields['Name'].AsStr := 'Фрукты';
  Cat2.Fields['ParentID'].AsInt := 1;

  {$IFDEF USE_INDEXES}
  DataIndex := Cats.Indexes[0] as TDataCollectionIndexSingleInteger;

  Check(Cat0 = DataIndex.Finder( 1).FindFirst, 'Cat0 by DataIndex');
  Check(Cat1 = DataIndex.Finder(11).FindFirst, 'Cat1 by DataIndex');
  Check(Cat2 = DataIndex.Finder(56).FindFirst, 'Cat2 by DataIndex');
  {$ENDIF}


  CatRoot := DataRoot.Fields['RootCategory'].AsEntity;

  DataRoot.Refresh();

  Check(CatRoot.Target = Cat0, 'CatRoot.Target != Cat0');

  CatsLinks := Cat0.Fields['Children'].AsItems;
  CheckEquals(2, CatsLinks.Count, 'Cat0.Children');

  Check(Cat1 = CatsLinks[0], 'Cat1 = Cat0.Children[0]');
  Check(Cat2 = CatsLinks[1], 'Cat2 = Cat0.Children[1]');
end;

procedure TestEntities.MetaExportJSON;
var
  MFldCategories, MFldCatSubItems, MFldCategories2, MFldCatSubItems2 : TMetaFieldCollection;
  MFldCategoryID, MFldPartCategoryID, MFldCategoryID2, MFldPartCategoryID2, MFldSubItemID, MFldSubItemID2 : TMetaFieldInteger;
  MFldCategoryName, MFldCategoryName2 : TMetaFieldString;
  MFldPart, MFldCategory, MFldPart2, MFldCategory2 : TMetaFieldEntity;
  J : TJSONObject;

  MetaRoot2 : TMetaEntityRoot;
begin
  MFldCategories := FMetaRoot.Fields.Add<TMetaFieldCollection>('Categories');
  MFldCategoryID    := MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldInteger>('ID');
  MFldCategoryName  := MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldString>('Name');

  MFldCatSubItems := MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldCollection>('SubItems');
  MFldSubItemID := MFldCatSubItems.Collection.ItemMeta.Fields.Add<TMetaFieldInteger>('ID');

  {$IFDEF USE_INDEXES}
  MFldCategories.Collection.Indexes.AddSingleIndex(MFldCategoryID, True);
  {$ENDIF}

  MFldPart := FMetaRoot.Fields.Add<TMetaFieldEntity>('Part');
  MFldPartCategoryID  := MFldPart.Entity.Fields.Add<TMetaFieldInteger>('CatID');
  MFldCategory        := MFldPart.Entity.Fields.Add<TMetaFieldEntity>('Category');

  MFldCategory.Entity.Link := '@Root.Categories[ID == @this.CatID]';

  J := FMetaRoot.ExportToJSON();
  try
//    CheckEquals('', J.ToString);

    MetaRoot2 := TMetaEntityRoot.Create();
    try
      MetaRoot2.ImportFromJSON(J, True);

      CheckEquals(FMetaRoot.Version,        MetaRoot2.Version,       'Root.Version');
      CheckEquals(FMetaRoot.Name,           MetaRoot2.Name,          'Root.Name');
      CheckEquals(FMetaRoot.Fields.Count,   MetaRoot2.Fields.Count,  'Root.Fields');

      MFldCategories2 := MetaRoot2.Fields[0] as TMetaFieldCollection;
      CheckEquals(MFldCategories.Name, MFldCategories2.Name, 'Categories.Name');

      CheckEquals(MFldCategories.Collection.ItemMeta.Fields.Count, MFldCategories2.Collection.ItemMeta.Fields.Count,  'Categories.ItemMeta.Fields');

      MFldCategoryID2    := MFldCategories2.Collection.ItemMeta.Fields[0] as TMetaFieldInteger;
      CheckEquals(MFldCategoryID.Name, MFldCategoryID2.Name, 'Categories.Fields[0]');

      MFldCategoryName2  := MFldCategories2.Collection.ItemMeta.Fields[1] as TMetaFieldString;
      CheckEquals(MFldCategoryName.Name, MFldCategoryName2.Name, 'Categories.Fields[1]');

      MFldCatSubItems2   := MFldCategories2.Collection.ItemMeta.Fields[2] as TMetaFieldCollection;
      CheckEquals(MFldCatSubItems.Name, MFldCatSubItems2.Name, 'Categories.Fields[2]');

      MFldSubItemID2 := MFldCatSubItems2.Collection.Fields[0] as TMetaFieldInteger;
      CheckEquals(MFldSubItemID.Name, MFldSubItemID2.Name, 'SubItems.Fields[0]');

      MFldPart2 := MetaRoot2.Fields[1] as TMetaFieldEntity;
      CheckEquals(MFldPart.Name, MFldPart2.Name, 'Part.Name');

      CheckEquals(MFldPart.Entity.Fields.Count, MFldPart2.Entity.Fields.Count,  'Part.Fields');

      MFldPartCategoryID2 := MFldPart2.Entity.Fields[0] as TMetaFieldInteger;
      CheckEquals(MFldPartCategoryID.Name, MFldPartCategoryID2.Name, 'Part.Fields[0]');

      MFldCategory2 := MFldPart2.Entity.Fields[1] as TMetaFieldEntity;
      CheckEquals(MFldCategory.Name, MFldCategory2.Name, 'Part.Fields[1]');

      CheckEquals(MFldCategory.Entity.Link, MFldCategory2.Entity.Link,  'Category.Link');
      CheckEquals(MFldCategory.Entity.Fields.Count, MFldCategory2.Entity.Fields.Count,  'Category.Fields');

    finally
      FreeAndNil(MetaRoot2);
    end;

  finally
    FreeAndNil(J);
  end;

end;

procedure TestEntities.DataExportJSON;
var
  MFldCategories, MFldCategoryChildren : TMetaFieldCollection;
  MFldCategoryID : TMetaFieldInteger;
  MFldCategoryRoot : TMetaFieldEntity;

  DataRoot, DataRoot2 : TDataEntityRoot;
  Cats : TDataCollection;
  CatRoot, Cat0, Cat1, Cat2 : TDataEntity;

  J : TJSONValue;

begin
  MFldCategories := FMetaRoot.Fields.Add<TMetaFieldCollection>('Categories');
  MFldCategoryID        := MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldInteger>('ID');
  MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldString>('Name');
  MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldInteger>('ParentID');
  MFldCategoryChildren  := MFldCategories.Collection.ItemMeta.Fields.Add<TMetaFieldCollection>('Children');
  MFldCategoryChildren.Collection.Link := '@root.Categories[ParentID == @this.ID]';

  {$IFDEF USE_INDEXES}
  MFldCategories.Collection.Indexes.AddSingleIndex(MFldCategoryID, True);
  {$ENDIF}

  MFldCategoryRoot := FMetaRoot.Fields.Add<TMetaFieldEntity>('RootCategory');
  MFldCategoryRoot.Entity.Link := '@root.Categories[ID == 1]';


  FMetaRoot.MakeDataRaw(DataRoot);

  Cats := DataRoot.Fields['Categories'].AsItems;

  Cat0 := Cats.Add();
  Cat0.Fields['ID'].AsInt := 1;
  Cat0.Fields['Name'].AsStr := 'Еда';

  Cat1 := Cats.Add();
  Cat1.Fields['ID'].AsInt := 11;
  Cat1.Fields['Name'].AsStr := 'Овощи';
  Cat1.Fields['ParentID'].AsInt := 1;

  Cat2 := Cats.Add();
  Cat2.Fields['ID'].AsInt := 56;
  Cat2.Fields['Name'].AsStr := 'Фрукты';
  Cat2.Fields['ParentID'].AsInt := 1;

  CatRoot := DataRoot.Fields['RootCategory'].AsEntity;


  DataRoot.Refresh();

  Check(CatRoot.Target = Cat0, 'CatRoot.Target != Cat0');
//  CheckEquals(2, Cat0.Fields['Children'].AsItems.Count, 'Cat0.Children');
//
//  Check(Cat1 = Cat0.Fields['Children'].AsItems[0], 'Cat1 = Cat0.Children[0]');
//  Check(Cat2 = Cat0.Fields['Children'].AsItems[1], 'Cat2 = Cat0.Children[1]');

  FMetaRoot.MakeDataRaw(DataRoot2);
  CheckEquals(0, DataRoot2.Fields['Categories'].AsCollection.Count, 'DataRoot2.Fields[''Categories''].AsCollection.Count = 0');

  J := DataRoot.ExportToJSON();
  try

    //CheckEquals('', J.ToString);

    DataRoot2.ImportFromJSON(J, True);
  finally
    FreeAndNil(J);
  end;

  CheckEquals(Cats.Count, DataRoot2.Fields['Categories'].AsCollection.Count, 'DataRoot2.Fields[''Categories''].AsCollection.Count = 0');

end;
{$ENDREGION}

initialization
  // Register any test cases with the test runner
  RegisterTest(TestEntities.Suite);
end.

