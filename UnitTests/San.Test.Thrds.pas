{*******************************************************}
{                                                       }
{       Работа с потоками. Тесты                        }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   Работа с потоками. Тесты
/// </summary>
unit San.Test.Thrds;
{$I San.inc}
interface

uses
  {$REGION 'WinApi'}
  WinApi.Windows,
  {$ENDREGION}

  {$REGION 'System'}
  System.SysUtils, System.Classes, System.TypInfo,
  {$ENDREGION}

  {$REGION 'San'}
  San.Thrds,
  {$ENDREGION}

  {$REGION 'DUnit'}
  TestFramework,
  {$ENDREGION}

  {$REGION 'San.Test'}
  San.Test.Custom
  {$ENDREGION}

;

type
  TLockerTarget = class(TInterfacedObject, TMultithreadLockerFacade.ITarget)
  private
    FLockerData : TMultithreadLockerFacade.TMultithreadLockerData;
    FValue : integer;

  protected
    {$REGION 'TMultithreadLockerFacade.ITarget'}
    function GetLockerData() : TMultithreadLockerFacade.TMultithreadLockerData;
    function ExternalTryEnterRead(const AStartTicks, ATimeOut : Cardinal; out AFailedToLock : TObject) : boolean;
    procedure ExternalLeaveRead(const AFailedToLock : TObject);
    {$ENDREGION}

  public
    constructor Create();
    destructor Destroy; override;

  end;

  TestTMultithreadLockerFacade = class(TSanTestCase<TMultithreadLockerFacade>)
  private
    FLockerTarget : TLockerTarget;
    FLocker : TMultithreadLockerFacade;

  protected
    procedure SetUp; override;
    procedure TearDown; override;

  published
    procedure NoLock;
    procedure ReadOneThread;
    procedure WriteOneThread;
    procedure ReadTwoThread;
    procedure WriteTwoThread;
    procedure Hard;

  end;

implementation

{$REGION 'TLockerTarget'}
constructor TLockerTarget.Create;
begin
  inherited Create;

  FLockerData := TMultithreadLockerFacade.TMultithreadLockerData.Create();
end;

destructor TLockerTarget.Destroy;
begin
  FreeAndNil(FLockerData);
  inherited;
end;

function TLockerTarget.GetLockerData: TMultithreadLockerFacade.TMultithreadLockerData;
begin
  Result := FLockerData;
end;

function TLockerTarget.ExternalTryEnterRead(const AStartTicks, ATimeOut: Cardinal;
  out AFailedToLock: TObject): boolean;
begin
  AFailedToLock := nil;
  Result := True;
end;

procedure TLockerTarget.ExternalLeaveRead(const AFailedToLock: TObject);
begin
end;
{$ENDREGION}

{$REGION 'TestTMultithreadLockerFacade'}
procedure TestTMultithreadLockerFacade.SetUp;
begin
  inherited;

  FLockerTarget := TLockerTarget.Create();
  FLocker := TMultithreadLockerFacade.Create(FLockerTarget);
end;

procedure TestTMultithreadLockerFacade.TearDown;
begin
  Check(not FLockerTarget.FLockerData.HasLocks, 'HasLocks on test TearDown');

  FLockerTarget := nil;
  FLocker.Free();

  inherited;
end;

procedure TestTMultithreadLockerFacade.NoLock;
begin
  FLockerTarget.FValue := 1;
  FLockerTarget.FValue := 2;
  FLockerTarget.FValue := 3;
  CheckEquals(3, FLockerTarget.FValue);
end;

procedure TestTMultithreadLockerFacade.ReadOneThread;
begin
  FLocker.EnterRead();
  try
    NoLock();
  finally
    FLocker.LeaveRead();
  end;
end;

procedure TestTMultithreadLockerFacade.WriteOneThread;
begin
  FLocker.EnterWrite();
  try
    NoLock();
  finally
    FLocker.LeaveWrite();
  end;
end;

procedure TestTMultithreadLockerFacade.ReadTwoThread;
begin
  FLockerTarget.FValue := 5;

  StartCoroutine(
    procedure
    begin
      Sleep(200);

      FLocker.EnterRead();
      try
        CheckEquals(FLockerTarget.FValue, 5);
      finally
        FLocker.LeaveRead();
      end;

    end
  );

  StartCoroutine(
    procedure
    begin
      Sleep(100);

      FLocker.EnterRead();
      try
        CheckEquals(FLockerTarget.FValue, 5);
      finally
        FLocker.LeaveRead();
      end;

    end
  );

  Sleep(400);

  CheckEquals(FLockerTarget.FValue, 5);

  WaitForCoroutines();
end;

procedure TestTMultithreadLockerFacade.WriteTwoThread;
begin
  FLockerTarget.FValue := 5;

  StartCoroutine(
    procedure
    begin
      Sleep(100);

      FLocker.EnterRead();
      try
        CheckEquals(FLockerTarget.FValue, 5);
        Sleep(400);
      finally
        FLocker.LeaveRead();
      end;

    end
  );

  StartCoroutine(
    procedure
    begin
      Sleep(200);

      if FLocker.TryEnterWrite(100) then
      try
        FLockerTarget.FValue := 17;
      finally
        FLocker.LeaveWrite();
      end;

    end
  );

  Sleep(400);

  WaitForCoroutines();

  CheckEquals(FLockerTarget.FValue, 5);
end;

procedure TestTMultithreadLockerFacade.Hard;
begin
  FLockerTarget.FValue := 5;

  StartCoroutine(
    procedure
    begin
      Sleep(100);

      FLocker.EnterRead();
      try
        CheckEquals(FLockerTarget.FValue, 5);
        Sleep(200);
      finally
        FLocker.LeaveRead();
      end;

      FLocker.EnterWrite();
      try
        FLockerTarget.FValue := 19;
        Sleep(400);
      finally
        FLocker.LeaveWrite();
      end;

    end
  );

  StartCoroutine(
    procedure
    begin
      Sleep(200);

      FLocker.EnterRead();
      try
        CheckEquals(FLockerTarget.FValue, 5);
        Sleep(700);
      finally
        FLocker.LeaveRead();
      end;

      Sleep(1);
      repeat

        FLocker.EnterRead();
        try
          if FLockerTarget.FValue = 19 then
            Break;

        finally
          FLocker.LeaveRead();
        end;

      until False;
    end
  );

  Sleep(400);

  WaitForCoroutines();

  CheckEquals(FLockerTarget.FValue, 19);
end;
{$ENDREGION}


initialization
  // Register any test cases with the test runner
  RegisterTest('Thrds', TestTMultithreadLockerFacade.Suite);
end.

