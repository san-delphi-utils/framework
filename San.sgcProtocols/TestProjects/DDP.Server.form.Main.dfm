object fmServerMain: TfmServerMain
  Left = 0
  Top = 0
  Caption = 'DDP.Server'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object stat: TStatusBar
    Left = 0
    Top = 280
    Width = 635
    Height = 19
    Panels = <
      item
        Width = 50
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object tbcClients: TTabControl
    Left = 0
    Top = 65
    Width = 635
    Height = 215
    Align = alClient
    TabOrder = 1
    OnChange = tbcClientsChange
  end
  object pnlHeader: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 65
    Align = alTop
    Padding.Left = 8
    Padding.Top = 8
    Padding.Right = 8
    Padding.Bottom = 8
    ShowCaption = False
    TabOrder = 2
    object bvlConnection: TBevel
      AlignWithMargins = True
      Left = 334
      Top = 9
      Width = 2
      Height = 47
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Align = alLeft
      Shape = bsLeftLine
      ExplicitLeft = 324
      ExplicitTop = 15
    end
    object btnStart: TButton
      AlignWithMargins = True
      Left = 155
      Top = 12
      Width = 82
      Height = 44
      Margins.Left = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Action = actStart
      Align = alLeft
      TabOrder = 0
    end
    object btnStop: TButton
      AlignWithMargins = True
      Left = 245
      Top = 12
      Width = 81
      Height = 44
      Margins.Left = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Action = actStop
      Align = alLeft
      TabOrder = 1
    end
    object pnlPort: TPanel
      AlignWithMargins = True
      Left = 9
      Top = 9
      Width = 65
      Height = 47
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Align = alLeft
      BevelOuter = bvNone
      ShowCaption = False
      TabOrder = 2
      object lbledtPort: TLabeledEdit
        Left = 8
        Top = 20
        Width = 57
        Height = 21
        EditLabel.Width = 20
        EditLabel.Height = 13
        EditLabel.Caption = 'Port'
        MaxLength = 5
        NumbersOnly = True
        TabOrder = 0
        Text = '21256'
      end
    end
    object pnlPing: TPanel
      AlignWithMargins = True
      Left = 82
      Top = 9
      Width = 65
      Height = 47
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Align = alLeft
      BevelOuter = bvNone
      ShowCaption = False
      TabOrder = 3
      object lbledtPing: TLabeledEdit
        Left = 0
        Top = 20
        Width = 65
        Height = 21
        EditLabel.Width = 58
        EditLabel.Height = 13
        EditLabel.Caption = 'PingInterval'
        NumbersOnly = True
        TabOrder = 0
        OnChange = lbledtPingChange
      end
    end
    object btnAdded: TButton
      AlignWithMargins = True
      Left = 344
      Top = 12
      Width = 81
      Height = 44
      Margins.Left = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Action = actAdded
      Align = alLeft
      TabOrder = 4
    end
    object btnChanged: TButton
      AlignWithMargins = True
      Left = 433
      Top = 12
      Width = 81
      Height = 44
      Margins.Left = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Action = actChanged
      Align = alLeft
      TabOrder = 5
    end
    object btnRemoved: TButton
      AlignWithMargins = True
      Left = 522
      Top = 12
      Width = 81
      Height = 44
      Margins.Left = 0
      Margins.Right = 8
      Margins.Bottom = 0
      Action = actRemoved
      Align = alLeft
      TabOrder = 6
    end
  end
  object sgcwss1: TsgcWebSocketServer
    Port = 21256
    OnConnect = sgcwss1Connect
    OnMessage = sgcwss1Message
    OnDisconnect = sgcwss1Disconnect
    LoadBalancer.AutoRegisterBindings = False
    LoadBalancer.AutoRestart = 0
    LoadBalancer.Enabled = False
    LoadBalancer.Port = 0
    Authentication.Enabled = False
    Authentication.AllowNonAuth = False
    Authentication.URL.Enabled = True
    Authentication.Session.Enabled = True
    Authentication.Basic.Enabled = False
    Bindings = <>
    HeartBeat.Enabled = False
    HeartBeat.Interval = 300
    HeartBeat.Timeout = 0
    MaxConnections = 0
    SSLOptions.Port = 0
    ThreadPool = False
    ThreadPoolOptions.MaxThreads = 0
    ThreadPoolOptions.PoolSize = 32
    Extensions.DeflateFrame.Enabled = False
    Extensions.DeflateFrame.WindowBits = 15
    Extensions.PerMessage_Deflate.Enabled = False
    Extensions.PerMessage_Deflate.ClientMaxWindowBits = 15
    Extensions.PerMessage_Deflate.ClientNoContextTakeOver = False
    Extensions.PerMessage_Deflate.MemLevel = 1
    Extensions.PerMessage_Deflate.ServerMaxWindowBits = 15
    Extensions.PerMessage_Deflate.ServerNoContextTakeOver = False
    FallBack.Flash.Domain = '*'
    FallBack.Flash.Enabled = False
    FallBack.Flash.Ports = '*'
    FallBack.ServerSentEvents.Enabled = False
    FallBack.ServerSentEvents.Retry = 3
    Options.FragmentedMessages = frgOnlyBuffer
    Options.HTMLFiles = True
    Options.JavascriptFiles = True
    Options.ReadTimeOut = 10
    Options.RaiseDisconnectExceptions = True
    Options.ValidateUTF8 = False
    QueueOptions.Binary.Level = qmNone
    QueueOptions.Ping.Level = qmNone
    QueueOptions.Text.Level = qmNone
    Specifications.Drafts.Hixie76 = True
    Specifications.RFC6455 = True
    NotifyEvents = neNoSync
    LogFile.Enabled = False
    Throttle.BitsPerSec = 0
    Throttle.Enabled = False
    WatchDog.Attempts = 0
    WatchDog.Enabled = False
    WatchDog.Interval = 60
    Left = 32
    Top = 64
  end
  object actlst: TActionList
    Left = 80
    Top = 64
    object actStart: TAction
      Caption = 'Start'
      OnExecute = actStartExecute
      OnUpdate = actStartUpdate
    end
    object actStop: TAction
      Caption = 'Stop'
      OnExecute = actStopExecute
      OnUpdate = actStopUpdate
    end
    object actAdded: TAction
      Caption = 'Added'
      OnExecute = actAddedExecute
      OnUpdate = actAddedChangedRemovedUpdate
    end
    object actChanged: TAction
      Caption = 'Changed'
      OnExecute = actChangedExecute
      OnUpdate = actAddedChangedRemovedUpdate
    end
    object actRemoved: TAction
      Caption = 'Removed'
      OnExecute = actRemovedExecute
      OnUpdate = actAddedChangedRemovedUpdate
    end
  end
  object ssDDPsrv1: TSansgcWSProtocolDDP_Server
    ServerID = '1101'
    Polite = True
    OnDDPMsg = ssDDPsrv1DDPMsg
    Server = sgcwss1
    OnConnect = sgcwss1Connect
    OnDisconnect = sgcwss1Disconnect
    OnException = ssDDPsrv1Exception
    OnMessage = sgcwss1Message
    Left = 32
    Top = 121
  end
end
