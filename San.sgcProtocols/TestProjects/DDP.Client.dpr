program DDP.Client;

uses
  Vcl.Forms,
  DDP.Client.form.Main in 'DDP.Client.form.Main.pas' {fmClientMain};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmClientMain, fmClientMain);
  Application.Run;
end.
