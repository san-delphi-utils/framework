program DDP.Server.Console;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  System.Classes,
  System.JSON,
  System.Hash,

  sgcWebSocket_Classes, sgcWebSocket_Types, sgcWebSocket_Classes_Indy,
  sgcWebSocket_Server, sgcWebSocket, sgcWebSocket_Protocol_Base_Server,

  San.sgcProtocols.DDP;

type
  TServerApp = class
  private
    FWSServer : TsgcWebSocketServer;
    FDDPServer : TSansgcWSProtocolDDP_Server;

    FLastWriteStatus : integer;

  protected
    procedure sgcwss1Connect(Connection: TsgcWSConnection);
    procedure sgcwss1Disconnect(Connection: TsgcWSConnection; Code: Integer);
    procedure sgcwss1Message(Connection: TsgcWSConnection; const Text: string);
    procedure ssDDPsrv1Exception(Connection: TsgcWSConnection; E: Exception);
    procedure ssDDPsrv1DDPMsg(
      AContext: TSansgcWSProtocolDDP_Server.TDDPMsgContext;
      var AAcquireContext: Boolean);

  public
    constructor Create;
    destructor Destroy; override;

    procedure WriteStatus(const AStatus : string); overload;
    procedure WriteStatus(const AStatusParts : array of string); overload;
  end;


{$REGION 'TServerApp'}
constructor TServerApp.Create;
var
  PortStr : string;
  Port : integer;
begin
  inherited Create();

  FWSServer := TsgcWebSocketServer.Create(nil);

  FWSServer.Name := 'sgcWSS';
  if FindCmdLineSwitch('p', PortStr) and TryStrToInt(PortStr, Port) then
    FWSServer.Port := Port
  else
    FWSServer.Port := 21256;

  FWSServer.OnConnect := sgcwss1Connect;
  FWSServer.OnMessage := sgcwss1Message;
  FWSServer.OnDisconnect := sgcwss1Disconnect;
  FWSServer.MaxConnections := 0;
  FWSServer.ThreadPool := False;
  FWSServer.NotifyEvents := neNoSync;


  FDDPServer := TSansgcWSProtocolDDP_Server.Create(FWSServer);

  FDDPServer.Name := 'ssDDPsrv';
  FDDPServer.ServerID := '1101';
  FDDPServer.Polite := True;
  FDDPServer.OnDDPMsg := ssDDPsrv1DDPMsg;
  FDDPServer.Server := FWSServer;
  FDDPServer.OnConnect := sgcwss1Connect;
  FDDPServer.OnDisconnect := sgcwss1Disconnect;
  FDDPServer.OnException := ssDDPsrv1Exception;
  FDDPServer.OnMessage := sgcwss1Message;
end;

destructor TServerApp.Destroy;
begin
  if Assigned(FWSServer) then
    FWSServer.Active := False;

  FreeAndNil(FWSServer);

  inherited;
end;

procedure TServerApp.WriteStatus(const AStatus: string);
var
  NewHash : integer;
begin
  NewHash := THashBobJenkins.GetHashValue(AStatus);
  if NewHash = FLastWriteStatus then
    Write('.')
  else
  begin
    Write(#13#10, AStatus);
    FLastWriteStatus := NewHash;
  end;
end;

procedure TServerApp.WriteStatus(const AStatusParts: array of string);
var
  S : string;
  i : integer;
begin
  Assert(Length(AStatusParts) > 0);

  S := AStatusParts[Low(AStatusParts)];
  for i := Succ(Low(AStatusParts)) to High(AStatusParts) do
    S := S + #13#10 + AStatusParts[i];

  WriteStatus(S);
end;

procedure TServerApp.sgcwss1Connect(Connection: TsgcWSConnection);
begin
  WriteStatus('Connect: ' + Connection.Guid);
end;

procedure TServerApp.sgcwss1Disconnect(Connection: TsgcWSConnection;
  Code: Integer);
begin
  WriteStatus('Disconnect: ' + Connection.Guid);
end;

procedure TServerApp.sgcwss1Message(Connection: TsgcWSConnection;
  const Text: string);
begin
  WriteStatus(['Message: ' + Connection.Guid, Text]);
end;

procedure TServerApp.ssDDPsrv1Exception(Connection: TsgcWSConnection;
  E: Exception);
begin
  WriteStatus(['Exception: ' + Connection.Guid, E.ToString()]);
end;

procedure TServerApp.ssDDPsrv1DDPMsg(
  AContext: TSansgcWSProtocolDDP_Server.TDDPMsgContext;
  var AAcquireContext: Boolean);

  procedure DoConnect(AContext : TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect);
  begin
    AContext.AcceptSession(AContext.Connection.Guid);
  end;

  procedure DoMethod(AContext : TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method);
  var
    MetodHandler : procedure (AContext: TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method;
      var AAcquireContext: Boolean) of object;
  begin
    AContext.Updated();

    TMethod(MetodHandler).Code := Self.MethodAddress( 'DDP_Mtd_' + AContext.Method );

    if not Assigned(TMethod(MetodHandler).Code) then
      AContext.Result('Method "' + AContext.Method + '" not found')

    else
    begin
      TMethod(MetodHandler).Data := Self;
      try
        MetodHandler(AContext, AAcquireContext);
      except
        on E : Exception do
          AContext.Result(E.ToString())
      end;
    end;

  end;

  procedure DoSub(AContext : TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub);
  begin
    if AContext.IsIDSubscribed then
      AContext.Result('Already subscribed for collection name "' + AContext.Name + '" and id "' + AContext.ID + '"')

    else
    if AnsiSameText(AContext.Name, 'control') then
      AContext.Ready()
    else
      AContext.Result('Unknown collection name "' + AContext.Name + '"');
  end;

  procedure DoUnSub(AContext : TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Unsub);
  begin
    if not AContext.IsIDSubscribed then
      AContext.Result('Not subscription for id "' + AContext.ID + '"')
    else
      AContext.Result();
  end;

begin
  case AContext.MsgKind of
    mksConnect:
      DoConnect(AContext as TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect);

    mksMethod:
      DoMethod(AContext as TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method);

    mksSub:
      DoSub(AContext as TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub);

    mksUnsub:
      DoUnSub(AContext as TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Unsub);
  end;

end;
{$ENDREGION}

procedure Main();
var
  app : TServerApp;
begin
  app := TServerApp.Create();
  try
    app.FWSServer.Active := True;

    Writeln('DDP server started at port ', app.FWSServer.Port);

    while app.FWSServer.Active do
      Sleep(10);

  finally
    FreeAndNil(app);
  end;
end;

begin
  try

    Main();

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
