unit DDP.Server.form.Main;
{$DEFINE DDP_PROTOCOL}
interface

uses
  Winapi.Windows, Winapi.Messages,

  System.SysUtils, System.Variants, System.Classes, System.JSON,

  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.ActnList, Vcl.ComCtrls,

  System.Actions,

  sgcWebSocket_Classes, sgcWebSocket_Types, sgcWebSocket_Classes_Indy,
  sgcWebSocket_Server, sgcWebSocket, sgcWebSocket_Protocol_Base_Server,

  San.sgcProtocols.DDP,

  DDP.Server.frame.Client
  ;

type
  TfmServerMain = class(TForm)
    sgcwss1: TsgcWebSocketServer;
    lbledtPort: TLabeledEdit;
    btnStart: TButton;
    btnStop: TButton;
    actlst: TActionList;
    actStart: TAction;
    actStop: TAction;
    stat: TStatusBar;
    tbcClients: TTabControl;
    pnlHeader: TPanel;
    pnlPort: TPanel;
    ssDDPsrv1: TSansgcWSProtocolDDP_Server;
    pnlPing: TPanel;
    lbledtPing: TLabeledEdit;
    btnAdded: TButton;
    actAdded: TAction;
    actChanged: TAction;
    actRemoved: TAction;
    btnChanged: TButton;
    btnRemoved: TButton;
    bvlConnection: TBevel;
    procedure FormCreate(Sender: TObject);
    procedure tbcClientsChange(Sender: TObject);
    procedure lbledtPingChange(Sender: TObject);
    procedure actStartExecute(Sender: TObject);
    procedure actStartUpdate(Sender: TObject);
    procedure actStopExecute(Sender: TObject);
    procedure actStopUpdate(Sender: TObject);
    procedure actAddedExecute(Sender: TObject);
    procedure actAddedChangedRemovedUpdate(Sender: TObject);
    procedure actChangedExecute(Sender: TObject);
    procedure actRemovedExecute(Sender: TObject);
    procedure sgcwss1Connect(Connection: TsgcWSConnection);
    procedure sgcwss1Disconnect(Connection: TsgcWSConnection; Code: Integer);
    procedure sgcwss1Message(Connection: TsgcWSConnection; const Text: string);
    procedure ssDDPsrv1Exception(Connection: TsgcWSConnection; E: Exception);
    procedure ssDDPsrv1DDPMsg(
      AContext: TSansgcWSProtocolDDP_Server.TDDPMsgContext;
      var AAcquireContext: Boolean);

  strict private
    function GetClientsFrames(const AConnection: TsgcWSConnection): TfrmClient;
    procedure SetClientFrames(const AConnection: TsgcWSConnection;
      const AValue: TfrmClient);

  private
    FFramesCounter : integer;
    FLastChangeTicks : Cardinal;
    procedure UpdateClientCountStatus();
    procedure HideAllClientsFrames();

  public
    property ClientsFrames[const AConnection : TsgcWSConnection] : TfrmClient
      read GetClientsFrames  write SetClientFrames;

  published
    procedure DDP_Mtd_ChangeColor(AContext: TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method;
      var AAcquireContext: Boolean);
  end;

var
  fmServerMain: TfmServerMain;

implementation

{$R *.dfm}

{$REGION 'TfmServerMain'}
function TfmServerMain.GetClientsFrames(
  const AConnection: TsgcWSConnection): TfrmClient;
begin
  if not Assigned(AConnection.Data) {$IFDEF DDP_PROTOCOL} or not Assigned(ssDDPsrv1.ConnectionDDPData[AConnection].ExtData){$ENDIF} then
    Result := nil
  else
    Result := {$IFDEF DDP_PROTOCOL}ssDDPsrv1.ConnectionDDPData[AConnection].ExtData{$ELSE}AConnection.Data{$ENDIF} as TfrmClient;
end;

procedure TfmServerMain.SetClientFrames(const AConnection: TsgcWSConnection;
  const AValue: TfrmClient);
begin
  if (not Assigned(AConnection.Data) {$IFDEF DDP_PROTOCOL} or not Assigned(ssDDPsrv1.ConnectionDDPData[AConnection].ExtData){$ENDIF})
  and not Assigned(AValue)
  then
    Exit;

  {$IFDEF DDP_PROTOCOL}ssDDPsrv1.ConnectionDDPData[AConnection].ExtData{$ELSE}AConnection.Data{$ENDIF} := AValue;
end;

procedure TfmServerMain.actStartExecute(Sender: TObject);
begin
  sgcwss1.Port := StrToInt(lbledtPort.Text);
  sgcwss1.Active := True;
end;

procedure TfmServerMain.actStartUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not sgcwss1.Active;
end;

procedure TfmServerMain.actStopExecute(Sender: TObject);
begin
  sgcwss1.Active := False;
end;

procedure TfmServerMain.actStopUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := sgcwss1.Active;
end;

procedure TfmServerMain.actAddedChangedRemovedUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := sgcwss1.Active;
end;

procedure TfmServerMain.actAddedExecute(Sender: TObject);
begin
  ssDDPsrv1.EnumConnectionsDDPData(
    procedure (AConnDDP : TSansgcWSProtocolDDP_Server.TConnectionDDPData)
    var
      Fields : TJSONObject;
    begin
      if not AConnDDP.IsSubscribed('control') then
        Exit;

      Fields := TJSONObject.Create();
      Fields.AddPair('up', TJSONNumber.Create(15));
      AConnDDP.Added('control', '1', Fields);
    end
  );

end;

procedure TfmServerMain.actChangedExecute(Sender: TObject);
begin
  ssDDPsrv1.EnumConnectionsDDPData(
    procedure (AConnDDP : TSansgcWSProtocolDDP_Server.TConnectionDDPData)
    var
      Fields : TJSONObject;
    begin
      if not AConnDDP.IsSubscribed('control') then
        Exit;

      Fields := TJSONObject.Create();
      Fields.AddPair('left', TJSONNumber.Create(7));
      AConnDDP.Changed('control', '1', Fields, ['f1']);
    end
  );
end;

procedure TfmServerMain.actRemovedExecute(Sender: TObject);
begin
  ssDDPsrv1.EnumConnectionsDDPData(
    procedure (AConnDDP : TSansgcWSProtocolDDP_Server.TConnectionDDPData)
    begin
      if not AConnDDP.IsSubscribed('control') then
        Exit;

      AConnDDP.Removed('control', '1');
    end
  );
end;

procedure TfmServerMain.FormCreate(Sender: TObject);
begin
  Application.Title := Caption;

  UpdateClientCountStatus;

  lbledtPing.Text := IntToStr(ssDDPsrv1.PingInterval);
end;

procedure TfmServerMain.HideAllClientsFrames;
var
  i : integer;
begin
  for i := 0 to tbcClients.ComponentCount - 1 do
    (tbcClients.Components[i] as TControl).Hide;
end;

procedure TfmServerMain.lbledtPingChange(Sender: TObject);
var
  le : TLabeledEdit;
  S : string;
begin
  le := Sender as TLabeledEdit;

  S := Trim(le.Text);
  if (S = '') then
    Exit;

  ssDDPsrv1.PingInterval := StrToInt(S);
end;

procedure TfmServerMain.sgcwss1Connect(Connection: TsgcWSConnection);
var
  Frame : TfrmClient;
begin
  UpdateClientCountStatus;
  tbcClients.Tabs.Add(Connection.Guid);
  tbcClients.TabIndex := tbcClients.Tabs.Count - 1;

  Frame := TfrmClient.Create(tbcClients);
  Frame.Name := Format('frmClient%d', [AtomicIncrement(FFramesCounter)]);
  Frame.Parent := tbcClients;
  Frame.Align := alClient;

  ClientsFrames[Connection] := Frame;

  tbcClientsChange(tbcClients);
end;

procedure TfmServerMain.sgcwss1Disconnect(Connection: TsgcWSConnection;
  Code: Integer);
var
  Frame : TfrmClient;
  Idx : integer;
begin
  UpdateClientCountStatus;

  Frame := ClientsFrames[Connection];
  ClientsFrames[Connection] := nil;
  Frame.Free;

  Idx := tbcClients.Tabs.IndexOf(Connection.Guid);
  if Idx >= 0 then
    tbcClients.Tabs.Delete(Idx);

  if tbcClients.Tabs.Count > 0 then
  begin
    tbcClients.TabIndex := 0;
    tbcClientsChange(tbcClients);
  end;
end;

procedure TfmServerMain.sgcwss1Message(Connection: TsgcWSConnection;
  const Text: string);
var
  Frame : TfrmClient;
begin
  Frame := ClientsFrames[Connection];
  Frame.mmLog.Lines.Add(Text);

  if FLastChangeTicks < GetTickCount - 1000 then
  begin
    tbcClients.TabIndex := tbcClients.Tabs.IndexOf(Connection.Guid);
    tbcClientsChange(tbcClients);
  end;

end;

procedure TfmServerMain.ssDDPsrv1Exception(Connection: TsgcWSConnection;
  E: Exception);
begin
//  TThread.Queue(TThread.Current, procedure
//    begin
      MessageBox(Handle, PChar(E.ClassName + #13#10 + #13#10 + E.ToString), PChar(Format('%s — %s', [Application.Title, ssDDPsrv1.Name])), MB_ICONERROR);
//    end
//  );
end;

procedure TfmServerMain.tbcClientsChange(Sender: TObject);
var
  Connection: TsgcWSConnection;
  Frame : TfrmClient;
begin
  FLastChangeTicks := GetTickCount;

  HideAllClientsFrames;

  if tbcClients.Tabs.Count = 0 then
    Exit;

  Connection := sgcwss1.Connections[tbcClients.Tabs[tbcClients.TabIndex]];
  Frame := ClientsFrames[Connection];
  Frame.Show;

end;

procedure TfmServerMain.UpdateClientCountStatus;
begin
  stat.Panels[0].Text := Format('C - %d', [sgcwss1.Count]);
end;

procedure TfmServerMain.ssDDPsrv1DDPMsg(
  AContext: TSansgcWSProtocolDDP_Server.TDDPMsgContext;
  var AAcquireContext: Boolean);

  procedure DoConnect(AContext : TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect);
  begin
    AContext.AcceptSession(AContext.Connection.Guid);
  end;

  procedure DoMethod(AContext : TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method);
  var
    MetodHandler : procedure (AContext: TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method;
      var AAcquireContext: Boolean) of object;
  begin
    AContext.Updated();

    TMethod(MetodHandler).Code := Self.MethodAddress( 'DDP_Mtd_' + AContext.Method );

    if not Assigned(TMethod(MetodHandler).Code) then
      AContext.Result('Method "' + AContext.Method + '" not found')

    else
    begin
      TMethod(MetodHandler).Data := Self;
      try
        MetodHandler(AContext, AAcquireContext);
      except
        on E : Exception do
          AContext.Result(E.ToString())
      end;
    end;

  end;

  procedure DoSub(AContext : TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub);
  begin
    if AContext.IsIDSubscribed then
      AContext.Result('Already subscribed for collection name "' + AContext.Name + '" and id "' + AContext.ID + '"')

    else
    if AnsiSameText(AContext.Name, 'control') then
      AContext.Ready()
    else
      AContext.Result('Unknown collection name "' + AContext.Name + '"');
  end;

  procedure DoUnSub(AContext : TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Unsub);
  begin
    if not AContext.IsIDSubscribed then
      AContext.Result('Not subscription for id "' + AContext.ID + '"')
    else
      AContext.Result();
  end;

begin
  case AContext.MsgKind of
    mksConnect:
      DoConnect(AContext as TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect);

    mksMethod:
      DoMethod(AContext as TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method);

    mksSub:
      DoSub(AContext as TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub);

    mksUnsub:
      DoUnSub(AContext as TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Unsub);
  end;

end;

procedure TfmServerMain.DDP_Mtd_ChangeColor(
  AContext: TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method;
  var AAcquireContext: Boolean);
var
  Param, J : TJSONObject;
  C : TColor;
  D : integer;
begin
  Sleep(2000);

  Param := AContext.Params.Items[0] as TJSONObject;
  C := StringToColor( Param.Values['color'].Value );
  D := (Param.Values['dValue'] as TJSONNumber).AsInt;

  C := RGB(GetRValue(C) + D, GetGValue(C) + D, GetBValue(C) + D);

  J := TJSONObject.Create(TJSONPair.Create('color', ColorToString(C)));

  AContext.Result(J);
end;
{$ENDREGION}

end.
