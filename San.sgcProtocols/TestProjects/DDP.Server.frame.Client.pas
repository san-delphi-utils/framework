unit DDP.Server.frame.Client;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TfrmClient = class(TFrame)
    mmLog: TMemo;
    procedure mmLogChange(Sender: TObject);
  strict private

  public
    constructor Create(AOwner : TComponent); override;
  end;

implementation

{$R *.dfm}

{$REGION 'TfrmClient'}
constructor TfrmClient.Create(AOwner: TComponent);
begin
  inherited;
  mmLog.Text := '';
end;

procedure TfrmClient.mmLogChange(Sender: TObject);
var
  mm : TMemo;
begin
  mm := Sender as TMemo;
  PostMessage(mm.Handle, EM_LINESCROLL, 0, mm.Lines.Count);
end;
{$ENDREGION}

end.
