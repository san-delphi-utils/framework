program DDP.Server;

uses
  Vcl.Forms,
  DDP.Server.form.Main in 'DDP.Server.form.Main.pas' {fmServerMain},
  DDP.Server.frame.Client in 'DDP.Server.frame.Client.pas' {frmClient: TFrame};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmServerMain, fmServerMain);
  Application.Run;
end.
