unit DDP.Client.form.Main;

interface

uses
  Winapi.Windows, Winapi.Messages,

  System.SysUtils, System.Variants, System.Classes, System.JSON,

  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Actions,
  Vcl.ActnList, sgcWebSocket_Classes, sgcWebSocket_Classes_Indy,
  sgcWebSocket_Client, sgcWebSocket, Vcl.ExtCtrls, Vcl.ComCtrls,
  sgcWebSocket_Protocol_Base_Client,
  San.sgcProtocols.DDP;

type
  TfmClientMain = class(TForm)
    lbledtPort: TLabeledEdit;
    sgcwsc1: TsgcWebSocketClient;
    actlst: TActionList;
    actConnect: TAction;
    actDisconnect: TAction;
    btnConnect: TButton;
    btnDisconnect: TButton;
    lbledtIP: TLabeledEdit;
    mmLog: TMemo;
    pnlPortIP: TPanel;
    pnlHeader: TPanel;
    mmMessage: TMemo;
    tbcMessage: TTabControl;
    tbcLog: TTabControl;
    bvlConnection: TBevel;
    btnSend: TButton;
    actSend: TAction;
    ssDDPcln1: TSansgcWSProtocolDDP_Client;
    btnChangeColor: TButton;
    actChangeColor: TAction;
    pnlChangeColor: TPanel;
    chChangeColorSync: TCheckBox;
    btnSub: TButton;
    btnUnSub: TButton;
    actSub: TAction;
    actUnSub: TAction;
    procedure actConnectExecute(Sender: TObject);
    procedure actConnectUpdate(Sender: TObject);
    procedure actDisconnectExecute(Sender: TObject);
    procedure actDisconnectUpdate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sgcwsc1Exception(Connection: TsgcWSConnection; E: Exception);
    procedure mmLogChange(Sender: TObject);
    procedure actSendExecute(Sender: TObject);
    procedure actSendUpdate(Sender: TObject);
    procedure ssDDPcln1Message(Connection: TsgcWSConnection;
      const Text: string);
    procedure sgcwsc1Error(Connection: TsgcWSConnection; const Error: string);
    procedure actChangeColorExecute(Sender: TObject);
    procedure ssDDPcln1DDPMsg(
      AContext: TSansgcWSProtocolDDP_Client.TDDPMsgContext;
      var AAcquireContext: Boolean);
    procedure actSubExecute(Sender: TObject);
    procedure actSubUpdate(Sender: TObject);
    procedure actUnSubExecute(Sender: TObject);
    procedure actUnSubUpdate(Sender: TObject);
    procedure sgcwsc1Disconnect(Connection: TsgcWSConnection; Code: Integer);
  private
    FSubscribed : boolean;

  public
  end;

var
  fmClientMain: TfmClientMain;

implementation

{$R *.dfm}

{$REGION 'TfmClientMain'}
procedure TfmClientMain.FormCreate(Sender: TObject);
begin
  Application.Title := Caption;

  mmMessage.Text := '';
  mmLog.Text := '';
end;

procedure TfmClientMain.mmLogChange(Sender: TObject);
var
  mm : TMemo;
begin
  mm := Sender as TMemo;
  PostMessage(mm.Handle, EM_LINESCROLL, 0, mm.Lines.Count);
end;

procedure TfmClientMain.actConnectExecute(Sender: TObject);
begin
  sgcwsc1.Port := StrToInt(lbledtPort.Text);
  sgcwsc1.Host := lbledtIP.Text;
  sgcwsc1.Active := True;
end;

procedure TfmClientMain.actConnectUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not sgcwsc1.Active;
end;

procedure TfmClientMain.actDisconnectExecute(Sender: TObject);
begin
  sgcwsc1.Active := False;
end;

procedure TfmClientMain.actDisconnectUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := sgcwsc1.Active;
end;

procedure TfmClientMain.actSendExecute(Sender: TObject);
begin
  sgcwsc1.WriteData(mmMessage.Text);
end;

procedure TfmClientMain.actSendUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := sgcwsc1.Active;
end;

procedure TfmClientMain.actChangeColorExecute(Sender: TObject);

  function GetDDPCall : TSansgcWSProtocolDDP_Client.TDDPMethodCall;
  var
    Params : TJSONArray;
    Param : TJSONObject;
  begin
    Param := TJSONObject.Create();
    Param.AddPair('color', ColorToString(Self.Color));
    Param.AddPair('dValue', TJSONNumber.Create(5));

    Params := TJSONArray.Create(Param);

    Result := ssDDPcln1.DDP.Method('changeColor', GUIDToString(TGuid.NewGuid), Params);
  end;

  procedure AsyncCall(const ADDPCall : TSansgcWSProtocolDDP_Client.TDDPMethodCall);
  begin

    ADDPCall.Async(
      procedure (AContext : TSansgcWSProtocolDDP_Client.TDDPMsgContext;
        var AAcquireContext, AHandled : boolean)
      var
        CtxRes : TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result;
      begin
        if AContext.MsgKind = mkcUpdated then
          Exit;

        CtxRes := AContext as TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result;
        Self.Color := StringToColor( CtxRes.Result.Values['color'].Value );
      end
    );

  end;

  procedure SyncCall(const ADDPCall : TSansgcWSProtocolDDP_Client.TDDPMethodCall);
  var
    SyncContexts : TSansgcWSProtocolDDP_Client.TDDPMsgContexts;
    SyncContext : TSansgcWSProtocolDDP_Client.TDDPMsgContext;
    CtxRes : TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result;
  begin
    if ADDPCall.Sync(SyncContexts, 10000,
      procedure
      begin
        //Application.ProcessMessages();
      end
    )
    then
    try
      for SyncContext in SyncContexts do
        if SyncContext.MsgKind = mkcResult then
        begin
          CtxRes := SyncContext as TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result;
          Self.Color := StringToColor( CtxRes.Result.Values['color'].Value );
        end

    finally
      FreeAndNil(SyncContexts);
    end

    else
      mmLog.Lines.Add('ChangeColor: TimeOut!');

  end;

begin
  if chChangeColorSync.Checked then
    SyncCall(GetDDPCall())
  else
    AsyncCall(GetDDPCall());
end;

procedure TfmClientMain.actSubExecute(Sender: TObject);
var
  Params : TJSONArray;
  Param : TJSONObject;
begin
  Param := TJSONObject.Create();
  Param.AddPair('a', 'abcd');
  Param.AddPair('b', TJSONNumber.Create(12.5));

  Params := TJSONArray.Create(Param);

  ssDDPcln1.DDP.Sub('control', '123qwer', Params).Async(
    procedure (AContext : TSansgcWSProtocolDDP_Client.TDDPMsgContext;
      var AAcquireContext, AHandled : boolean)
    begin
      if AContext.MsgKind = mkcUpdated then
        Exit;

      AHandled := AContext.MsgKind = mkcReady;
      FSubscribed := AHandled;
    end
  );
end;

procedure TfmClientMain.actSubUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := sgcwsc1.Active and not FSubscribed;
end;

procedure TfmClientMain.actUnSubExecute(Sender: TObject);
begin
  ssDDPcln1.DDP.Unsub('123qwer').Async(
    procedure (AContext : TSansgcWSProtocolDDP_Client.TDDPMsgContext;
      var AAcquireContext, AHandled : boolean)
    begin
      if AContext.MsgKind = mkcUpdated then
        Exit;

      AHandled := (AContext.MsgKind = mkcResult) and (AContext as TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result).Success;
      FSubscribed := not AHandled;
    end
  );
end;

procedure TfmClientMain.actUnSubUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := sgcwsc1.Active and FSubscribed;
end;

procedure TfmClientMain.sgcwsc1Disconnect(Connection: TsgcWSConnection;
  Code: Integer);
begin
  FSubscribed := False;
end;

procedure TfmClientMain.sgcwsc1Error(Connection: TsgcWSConnection;
  const Error: string);
begin
  TThread.Queue(TThread.Current, procedure
    begin
      mmLog.Lines.Add('Error:');
      mmLog.Lines.Add(Error);
    end
  );
end;

procedure TfmClientMain.sgcwsc1Exception(Connection: TsgcWSConnection;
  E: Exception);
begin
  TThread.Queue(TThread.Current, procedure
    begin
      mmLog.Lines.Add('Error:');
      mmLog.Lines.Add(E.ToString());
    end
  );
end;

procedure TfmClientMain.ssDDPcln1Message(Connection: TsgcWSConnection;
  const Text: string);
begin
  TThread.Queue(TThread.Current, procedure
    begin
      mmLog.Lines.Add('Message:');
      mmLog.Lines.Add(Text);
    end
  );
end;

procedure TfmClientMain.ssDDPcln1DDPMsg(
  AContext: TSansgcWSProtocolDDP_Client.TDDPMsgContext;
  var AAcquireContext: Boolean);

  procedure DoResult(AContext: TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result);
  var
    S : string;
  begin
    if AContext.Success then
      Exit;

    S := AContext.Error;

    TThread.Queue(TThread.Current, procedure
      begin
        mmLog.Lines.Add('Result.Error:'#13#10 + S);
      end
    );

  end;

  procedure MoveFormByFields(const AFields : TJSONValue);
  var
    u, r, d, l : integer;
    V : TJSONNumber;
  begin
    if AFields.TryGetValue('up', V) then
      u := V.AsInt;

    if AFields.TryGetValue('down', V) then
      d := V.AsInt;

    if AFields.TryGetValue('left', V) then
      l := V.AsInt;

    if AFields.TryGetValue('right', V) then
      r := V.AsInt;

    TThread.Queue(TThread.Current, procedure
      begin
        Top   := Top   - u + d;
        Left  := Left  - l + r;
      end
    );
  end;

  procedure DoAdded(AContext: TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Added);
  var
    S : string;
  begin
    if AContext.Collection <> 'control' then
      Exit;

    MoveFormByFields(AContext.Fields);

    S := AContext.Collection + #13#10 + AContext.ID;

    TThread.Queue(TThread.Current, procedure
      begin
        mmLog.Lines.Add('Added:'#13#10 + S);
      end
    );
  end;

  procedure DoChanged(AContext: TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Changed);
  var
    S : string;
  begin
    if AContext.Collection <> 'control' then
      Exit;

    MoveFormByFields(AContext.Fields);

    S := AContext.Collection + #13#10 + AContext.ID;
    if Length(AContext.Cleared) > 0 then
      S := S + #13#10 + AContext.Cleared[0];

    mmLog.Lines.Add(S);

    TThread.Queue(TThread.Current, procedure
      begin
        mmLog.Lines.Add('Changed:'#13#10 + S);
      end
    );
  end;

  procedure DoRemoved(AContext: TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Removed);
  var
    S : string;
  begin
    if AContext.Collection <> 'control' then
      Exit;

    S := AContext.Collection + #13#10 + AContext.ID;

    TThread.Queue(TThread.Current, procedure
      begin
        mmLog.Lines.Add('Removed:'#13#10 + S);
      end
    );
  end;

begin
  case AContext.MsgKind of
    mkcResult:
      DoResult(AContext as TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result);

    mkcAdded:
      DoAdded(AContext as TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Added);

    mkcChanged:
      DoChanged(AContext as TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Changed);

    mkcRemoved:
      DoRemoved(AContext as TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Removed);
  end;

end;
{$ENDREGION}

end.
