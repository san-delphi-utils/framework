unit San.sgcProtocols.Reg;

interface

uses
  System.SysUtils, System.Classes,
  San.sgcProtocols.DDP;

procedure Register();

implementation

procedure Register();
begin
  System.Classes.RegisterComponents('San SGC WS Protocols', [TSansgcWSProtocolDDP_Client, TSansgcWSProtocolDDP_Server]);

end;


end.
