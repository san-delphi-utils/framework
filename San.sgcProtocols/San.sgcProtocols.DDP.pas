{*******************************************************}
{                                                       }
{       Протоколы, основанные на сетевых компонентах    }
{       SGC.                                            }
{                                                       }
{       Протокол DDP (Distributed Data Protocol)        }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Управление подсистемами
/// </summary>
unit San.sgcProtocols.DDP;
{$I San.sgcProtocols.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Classes, System.Generics.Collections, System.TypInfo,
  System.StrUtils, System.Types, System.DateUtils, System.SyncObjs, System.Math,
  System.JSON,
  {$ENDREGION}

  {$REGION 'Indy'}
  IdContext, IdThread, IdCustomTCPServer,
  {$ENDREGION}

  {$REGION 'Sgc'}
  sgcWebSocket_Protocol_Base_Client, sgcWebSocket_Protocol_Base_Server, sgcWebSocket_Classes,
  sgcWebSocket_Types,
  {$ENDREGION}

  {$REGION 'San'}
  San.Messages, San.Thrds, San.Intfs, San.Intfs.Basic
  {$ENDREGION}
;
{$ENDREGION}

{$ENDIF}

const
  /// <summary>
  ///   Интервал пинга по умолчанию
  /// </summary>
  DDP_DEFAULT_PING_INTERVAL = 3000;

  /// <summary>
  ///   Шаг ожидания события по умолчанию.
  /// </summary>
  DDP_DEFAULT_WAIT_STEP = 250;

  {$REGION 'DDP strings'}
  sDDP         = 'DDP';
  sServerID    = 'server_id';
  sMsg         = 'msg';
  sConnect     = 'connect';
  sConnected   = 'connected';
  sVersion     = 'version';
  sSupport     = 'support';
  sRejected    = 'rejected';
  sSession     = 'session';
  sError       = 'error';
  sPing        = 'ping';
  sPong        = 'pong';
  sMethod      = 'method';
  sParams      = 'params';
  sID          = 'id';
  sUpdated     = 'updated';
  sMethods     = 'methods';
  sResult      = 'result';
  sSub         = 'sub';
  sName        = 'name';
  sReady       = 'ready';
  sSubs        = 'subs';
  sUnsub       = 'unsub';
  sAdded       = 'added';
  sChanged     = 'changed';
  sRemoved     = 'removed';
  sCollection  = 'collection';
  sFields      = 'fields';
  sCleared     = 'cleared';
  {$ENDREGION}

resourcestring
  {$REGION 'Messages'}
  sMessageJSONParsingError =
    'Message JSON parse error';

  sMessageMustBeInJSONObjectFormatInUTF8 =
    'Message must be in JSON-object format in UTF8';

  sMessageMustContainNotEmptyMsgKey =
    'Message must contain not empty "msg" key';

  sInMessageWithMsgAAAKeysBBBExpectedFmt =
    'In message "%s" key(-s) %s expected';

  sUnexpectedMessageInStateAAAFmt =
    'Unexpected message in state %s';

  sConnectedAcceptOrRejectSessionAlreadyCalled =
    'Connected msg: AcceptSession or RejectSession already called';

  sPingThreadFromAAADDPProtocolServerFmt =
    'Ping thread from "%s" DDP protocol server';

  sMethodXXXAlreadyCalledFmt =
    'Method msg: "%s" already called';

  sSubXXXorYYYAlreadyCalledFmt =
    'Sub msg: "%s" or "%s" already called';

  sUnsubXXXAlreadyCalledFmt =
    'Unsub msg: "%s" already called';

  sServerIDMustBeAssigned =
    'Server ID must be assigned';

  sMsgContextClassNotRegistredForXXXFmt =
    'Msg context class not registred for "%s"';

  sInternalErrorForMessageXXXFmt =
    'Internal error for message:'#13#10'%s';
 {$ENDREGION}

type
  {$REGION 'forward'}
  TSansgcWSProtocolDDP_Client = class;
  TSansgcWSProtocolDDP_Server = class;
  {$ENDREGION}

  {$REGION 'events'}
  TSansgcWSProtocolDDP_ThreadErrorEvent = procedure (Sender : TObject;
     AThread : TThread; AError : Exception) of object;

  TSansgcWSProtocolDDP_Server_GetDynServerIDEvent = procedure (Sender : TSansgcWSProtocolDDP_Server;
    aConnection: TsgcWSConnection; var AServerID : string) of object;
  {$ENDREGION}

  {$REGION 'exceptions'}
  ESansgcWSProtocolDDP = class(TsgcWSException);
  {$ENDREGION}

  /// <summary>
  ///   Класс базовового контекста события DDP-сообщения
  /// </summary>
  TSansgcWSProtocolDDP_DDPMsgCustomContextClass = class of TSansgcWSProtocolDDP_DDPMsgCustomContext;

  /// <summary>
  ///   Базовый контекст события DDP-сообщения
  /// </summary>
  TSansgcWSProtocolDDP_DDPMsgCustomContext = class abstract(TObject)
  protected type
    TMsgClassesDict = TDictionary<string, TSansgcWSProtocolDDP_DDPMsgCustomContextClass>;

  strict private
    FSender: TsgcWSProtocol;
    FConnection: TsgcWSConnection;
    FRawData : TJSONObject;
    FKeys : TStrings;

  private
    class procedure RegClasses(const AClasses : array of TSansgcWSProtocolDDP_DDPMsgCustomContextClass); static;

  protected
    /// <summary>
    ///   Справочник классов сообщений
    /// </summary>
    class function MsgClasses : TMsgClassesDict; virtual;

    /// <summary>
    ///   Проверка параметров методом конечного класса контекста
    /// </summary>
    class procedure Validation(const ASender : TsgcWSProtocol; const AConnection: TsgcWSConnection;
      const ARawData : TJSONObject); virtual;

    /// <summary>
    ///   Регистрация конечного класса контекста в <c>FMsgClasses</c>
    /// </summary>
    class procedure RegClass();

    /// <summary>
    ///   <c>FRawData.TryGetValue + FKeys.Add</c>
    /// </summary>
    function TryGetValue<T>(const APath: string; out AValue: T): Boolean;

    /// <summary>
    ///   Если существует <c>FKeys</c>, поднять исключение
    /// </summary>
    procedure ValidateKeys();

    /// <summary>
    ///   Инициализация специфических полей приоизводных классов, изменение свойств компонента
    /// </summary>
    procedure Init(); virtual;

    /// <summary>
    ///   Запись данных в соединение
    /// </summary>
    procedure WriteData(const aText : string);

  public
    /// <summary>
    ///   Создать контекст соответствующего класса.
    /// </summary>
    class procedure Make(const ASender : TsgcWSProtocol; const AConnection: TsgcWSConnection;
      var ARawData : TJSONObject; out AContext);

    /// <summary>
    ///   Текст ключа "msg"
    /// </summary>
    class function Msg : string; virtual;

    constructor Create(const ASender : TsgcWSProtocol; const AConnection: TsgcWSConnection;
      const ARawData : TJSONObject);
    destructor Destroy(); override;

    /// <summary>
    ///   Компонент протокола, в котором произошло событие
    /// </summary>
    property Sender : TsgcWSProtocol  read FSender;

    /// <summary>
    ///   Соединение, в котором произошло событие
    /// </summary>
    property Connection : TsgcWSConnection  read FConnection;

    /// <summary>
    ///   Полный JSON сообщения
    /// </summary>
    /// <remarks>
    ///   Объект принадлежит компоненту протокола и будет разрушен после события
    /// </remarks>
    property RawData : TJSONObject read FRawData;
  end;

  /// <summary>
  ///   Базовый контекст события DDP-сообщения. Генерик Sender-а
  /// </summary>
  TSansgcWSProtocolDDP_DDPMsgCustomContext<TSender : class> = class abstract(TSansgcWSProtocolDDP_DDPMsgCustomContext)
  strict private
    function GetSender: TSender;

  private
    class var FMsgClasses : TSansgcWSProtocolDDP_DDPMsgCustomContext.TMsgClassesDict;

  protected
    /// <summary>
    ///   Справочник классов сообщений
    /// </summary>
    class function MsgClasses : TSansgcWSProtocolDDP_DDPMsgCustomContext.TMsgClassesDict; override;

  public
    /// <summary>
    ///   Компонент протокола, в котором произошло событие
    /// </summary>
    property Sender : TSender  read GetSender;
  end;

  /// <summary>
  ///   Компонент клиентской части протокола DDP
  /// </summary>
  TSansgcWSProtocolDDP_Client = class(TsgcWSProtocol_Client_Base)
  public type
    /// <summary>
    ///   Состояние подключения клиента
    /// </summary>
    TConnectionState = (csDisconnected, csWaitForServerID, csWaitForConnect, csConnected);
    TConnectionStates = set of TConnectionState;

    {$REGION 'Msg kind and known msgs'}
    /// <summary>
    ///   Сообщения, которые ПРИХОДЯТ на клиента
    /// </summary>
    TMsgKind =
    (
      mkcUnknown   = -1,
      mkcConnected = 0,
      mkcRejected  = 1,
      mkcPing      = 2,
      mkcUpdated   = 3,
      mkcResult    = 4,
      mkcReady     = 5,
      mkcAdded     = 6,
      mkcChanged   = 7,
      mkcRemoved   = 8,
      mkcError     = 9
    );

  protected const
    /// <summary>
    ///   Известные сообщения. Соответствие строк сообщений и TMsgKind
    /// </summary>
    KNOWN_MSGs : array [Succ(Low(TMsgKind))..High(TMsgKind)] of string =
    (
      sConnected,
      sRejected,
      sPing,
      sUpdated,
      sResult,
      sReady,
      sAdded,
      sChanged,
      sRemoved,
      sError
    );
    {$ENDREGION}

  public type
    {$REGION 'TDDPMsgCtx'}
    /// <summary>
    ///   Базовый контекст события DDP-сообщения на клиенте
    /// </summary>
    TDDPMsgContext = class abstract(TSansgcWSProtocolDDP_DDPMsgCustomContext<TSansgcWSProtocolDDP_Client>)
    protected
      class procedure Validation(const ASender : TsgcWSProtocol;
        const AConnection: TsgcWSConnection; const ARawData : TJSONObject); override;

      /// <summary>
      ///   Ожидаемое состояние соединения для данного сообщения
      /// </summary>
      class function ExpectedConnectionStates : TConnectionStates; virtual;

    public
      class function Msg : string; override;

      /// <summary>
      ///   Тип клиентского сообщения
      /// </summary>
      class function MsgKind : TMsgKind; virtual;
    end;

    /// <summary>
    ///   Список контекстов
    /// </summary>
    TDDPMsgContexts = class(TObjectList<TDDPMsgContext>)
    public
      /// <summary>
      ///   Найти первый контекст заданного класса.
      /// </summary>
      function TryFind<T : class>(out AContext : T) : boolean;
    end;

    TSansgcWSProtocolDDP_Client_MsgEvent = procedure(AContext : TSansgcWSProtocolDDP_Client.TDDPMsgContext;
      var AAcquireContext : boolean) of object;

    TSansgcWSProtocolDDP_Client_MsgReference = reference to procedure(AContext : TSansgcWSProtocolDDP_Client.TDDPMsgContext;
      var AAcquireContext, AHandled : boolean);

    /// <summary>
    ///   Абстрактный контекст одного из событий DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"connected","session":"XXX"}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"rejected", "error":"XXX"}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_CustomWaitSession = class abstract(TDDPMsgContext)
    protected
      class function ExpectedConnectionStates : TConnectionStates; override;
    end;

    /// <summary>
    ///   Абстрактный контекст одного из событий DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"ping"}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"updated","methods":[ZZZ]}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"result","id":"ZZZ","result":AAA}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"result","id":"ZZZ","error":BBB}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"ready","subs":["ZZZ"]}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"added","collection":"XXX","id":"YYY","fields":ZZZ}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"changed","collection":"XXX","id":"YYY","fields":AAA, "cleared":[BBB]}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"removed","collection":"XXX","id":"YYY"}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_CustomConnectedSession = class abstract(TDDPMsgContext)
    protected
      class function ExpectedConnectionStates : TConnectionStates; override;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    /// <para>
    ///   <c>{"msg":"connected","session":"XXX"}</c>
    /// </para>
    /// </summary>
    TDDPMsgCtx_Connected = class(TDDPMsgCtx_CustomWaitSession)
    strict private
      function GetSessionID: string;

    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

      /// <summary>
      ///   ID сессии
      /// </summary>
      property SessionID : string  read GetSessionID;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    /// <para>
    ///   <c>{"msg":"rejected", "error":"XXX"}</c>
    /// </para>
    /// </summary>
    TDDPMsgCtx_Rejected = class(TDDPMsgCtx_CustomWaitSession)
    strict private
      FError : TJSONString;
      function GetError: string;

    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

      /// <summary>
      ///   Сообщение об ошибке
      /// </summary>
      property Error : string  read GetError;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"ping"}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_Ping = class(TDDPMsgCtx_CustomConnectedSession)
    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"updated","methods":[ZZZ]}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_Updated = class(TDDPMsgCtx_CustomConnectedSession)
    strict private
      FMethodsJ : TJSONArray;
      FMethods : TStringDynArray;
      function GetMethods: TStringDynArray;

    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

      /// <summary>
      ///   Идентификаторы методов
      /// </summary>
      property Methods : TStringDynArray  read GetMethods;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"result","id":"ZZZ","result":AAA}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"result","id":"ZZZ","error":BBB}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_Result = class(TDDPMsgCtx_CustomConnectedSession)
    strict private
      FSuccess: boolean;
      FID, FError : TJSONString;
      FResult : TJSONObject;
      function GetError: string;
      function GetID: string;

    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

      /// <summary>
      ///   JSON-объект результата перестает быть собственностью контекста
      /// </summary>
      /// <returns>
      ///   Self.Result
      /// </returns>
      function AcquireResult : TJSONObject;

      /// <summary>
      ///   Идентификатор запроса для которого пришел результат
      /// </summary>
      property ID : string  read GetID;

      /// <summary>
      ///   <para>
      ///     <c>True</c> — нет ошибок, доступно св-во Result
      ///   </para>
      ///   <para>
      ///     <c>False</c> — ошибки, доступно св-во Error
      ///   </para>
      /// </summary>
      property Success : boolean  read FSuccess;

      /// <summary>
      ///   JSON-объект результата, доступен при <c>Success = True</c>,
      ///   может быть пустым (nil)
      /// </summary>
      /// <remarks>
      ///   Объет в собственности контекста события и будет разрущен после него.
      ///   <para>
      ///     Для захвата объекта в собственность исп. <c>AcquireResult</c>
      ///   </para>
      /// </remarks>
      property Result : TJSONObject  read FResult;

      /// <summary>
      ///   Сообщение об ошибке, доступно при <c>Success = False</c>
      /// </summary>
      property Error : string  read GetError;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"ready","subs":["ZZZ"]}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_Ready = class(TDDPMsgCtx_CustomConnectedSession)
    strict private
      FSubsJ : TJSONArray;
      FSubs : TStringDynArray;
      function GetSubs: TStringDynArray;

    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

      /// <summary>
      ///   Идентификаторы подписок
      /// </summary>
      property Subs : TStringDynArray  read GetSubs;
    end;

    /// <summary>
    ///   Абстрактный контекст одного из событий DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"added","collection":"XXX","id":"YYY","fields":ZZZ}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"changed","collection":"XXX","id":"YYY","fields":AAA, "cleared":[BBB]}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"removed","collection":"XXX","id":"YYY"}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_CustomCollection = class abstract(TDDPMsgCtx_CustomConnectedSession)
    strict private
      FCollection, FID : TJSONString;
      function GetCollection: string;
      function GetID: string;

    protected
      procedure Init(); override;

    public
      /// <summary>
      ///   Коллекция
      /// </summary>
      property Collection : string  read GetCollection;

      /// <summary>
      ///   Идентификатор документа в коллекции
      /// </summary>
      property ID : string  read GetID;
    end;

    /// <summary>
    ///   Абстрактный контекст одного из событий DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"added","collection":"XXX","id":"YYY","fields":ZZZ}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"changed","collection":"XXX","id":"YYY","fields":AAA, "cleared":[BBB]}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_CustomCollectionFields = class abstract(TDDPMsgCtx_CustomCollection)
    strict private
      FFields : TJSONObject;

    protected
      procedure Init(); override;

    public
      /// <summary>
      ///   JSON-объект cправочника новых значений полей по именам
      ///   перестает быть собственностью контекста
      /// </summary>
      /// <returns>
      ///   Self.Fields
      /// </returns>
      function AcquireFields : TJSONObject;

      /// <summary>
      ///   JSON-объект cправочника новых значений полей по именам.
      ///   <para>
      ///     Может быть пустым (nil)
      ///   </para>
      /// </summary>
      /// <remarks>
      ///   Объет в собственности контекста события и будет разрущен после него.
      ///   <para>
      ///     Для захвата объекта в собственность исп. <c>AcquireFields</c>
      ///   </para>
      /// </remarks>
      property Fields : TJSONObject  read FFields;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"added","collection":"XXX","id":"YYY","fields":ZZZ}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_Added = class(TDDPMsgCtx_CustomCollectionFields)
    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"changed","collection":"XXX","id":"YYY","fields":AAA, "cleared":[BBB]}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_Changed = class(TDDPMsgCtx_CustomCollectionFields)
    strict private
      FClearedJ : TJSONArray;
      FCleared : TStringDynArray;
      function GetCleared: TStringDynArray;

    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

      /// <summary>
      ///   Список удаленных полей
      /// </summary>
      property Cleared : TStringDynArray  read GetCleared;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"removed","collection":"XXX","id":"YYY"}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_Removed = class(TDDPMsgCtx_CustomCollection)
    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    /// <para>
    ///   <c>{"msg":"error", "error":"XXX"}</c>
    /// </para>
    /// </summary>
    TDDPMsgCtx_Error = class(TDDPMsgContext)
    strict private
      FError : TJSONString;
      function GetError: string;

    protected
      class function ExpectedConnectionStates : TConnectionStates; override;
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

      /// <summary>
      ///   Сообщение об ошибке
      /// </summary>
      property Error : string  read GetError;
    end;
    {$ENDREGION}

    /// <summary>
    ///   Вызов метода DDP
    /// </summary>
    TDDPMethodCall = record
    strict private
      FOwner : TSansgcWSProtocolDDP_Client;
      FID : string;
      FData : TJSONObject;
      FHolder : IInterface;

    private
      procedure Init(const AOwner : TSansgcWSProtocolDDP_Client;
        const AID : string; const AData : TJSONObject);

    public
      /// <summary>
      ///   Асинхронный вызов. Сразу продолжается выполнение
      /// </summary>
      /// <param name="AOnResult">
      ///   Индивидуальная обработка результата этого сообщения
      ///   <para>
      ///     Может вызываться несколько раз:
      ///   </para>
      ///   <para>
      ///     В первый раз для <c>{"msg":"updated","methods":[ZZZ]}</c>
      ///   </para>
      ///   <para>
      ///     В во второй для <c>{"msg":"result","id":"ZZZ","result":AAA}</c>
      ///   </para>
      ///   <para>
      ///     или <c>{"msg":"result","id":"ZZZ","error":BBB}</c>
      ///   </para>
      /// </param>
      procedure Async(const AOnResult : TSansgcWSProtocolDDP_Client_MsgReference = nil);

      /// <summary>
      ///   Синхронный вызов с ожиданием результата
      /// </summary>
      /// <param name="AResultContext">
      ///   При успехе: cписок контекстов всех результатов полученных во время ожидания
      ///   <para>
      ///     ! Должен быть разрушен после использования!
      ///   </para>
      /// </param>
      /// <param name="AWaitTimeout">
      ///   Таймаут ожидания
      /// </param>
      /// <param name="AOnWaitStep">
      ///   Действие шага ожидания результата
      ///   <para>
      ///     Например, здесь можно вызывать <c>ProcessMessages()</c>, если требуется
      ///   </para>
      /// </param>
      /// <param name="AWaitStep">
      ///   Действие шага ожидания результата
      ///   <para>
      ///     Размер шага ожидания результата
      ///   </para>
      /// </param>
      /// <remarks>
      ///   Работа метода очень зависит от режима <c>FOwner.Client.NotifyEvents</c>
      ///   <para>
      ///     <c>neAsynchronous, neSynchronous</c> из ГЛАВНОГО потока будут работать
      ///     только при
      ///       <code Personality="Delphi">
      ///         AOnWaitStep = procedure begin Application.ProcessMessages() end
      ///       </code>
      ///   </para>
      ///   <para>
      ///     <c>neNoSync</c> из ГЛАВНОГО потока будет работать и без <c>AOnWaitStep</c>
      ///   </para>
      /// </remarks>
      /// <returns>
      ///  <c>True</c> — получен нужный ответ, <c>False</c> — таймаут ожидания превышен
      /// </returns>
      function Sync(out AResultContexts : TDDPMsgContexts;
        const AWaitTimeout : Cardinal = INFINITE;
        const AOnWaitStep : TProc = nil;
        const AWaitStep : Cardinal = DDP_DEFAULT_WAIT_STEP) : boolean;
    end;

    /// <summary>
    ///   Группа методов DDP
    /// </summary>
    TDDPMethods = record
    private
      FOwner : TSansgcWSProtocolDDP_Client;
    public

      /// <summary>
      ///   Вызов метода
      ///   <para>
      ///     {"msg":"method","method":"XXX","params":[YYY],"id":"ZZZ"}
      ///   </para>
      /// </summary>
      /// <param name="AMethod">
      ///   Имя метода
      /// </param>
      /// <param name="AID">
      ///   Идентификатор запроса, он вернется в результате
      /// </param>
      /// <param name="AParams">
      ///   Массив параметров метода
      /// </param>
      function Method(const AMethod, AID : string; const AParams : TJSONArray = nil) : TDDPMethodCall; overload;

      /// <summary>
      ///   Вызов метода
      ///   <para>
      ///     {"msg":"method","method":"XXX","params":[YYY],"id":"ZZZ"}
      ///   </para>
      /// </summary>
      /// <param name="AMethod">
      ///   Имя метода
      /// </param>
      /// <param name="AID">
      ///   Идентификатор запроса, он вернется в результате
      /// </param>
      /// <param name="AParam">
      ///   Единственный параметр метода
      /// </param>
      function Method(const AMethod, AID : string; const AParam : TJSONObject) : TDDPMethodCall; overload;

      /// <summary>
      ///   Подписка на данные коллекции
      ///   <para>
      ///     {"msg":"sub","id":"ZZZ","name":"XXX","params":[YYY]}
      ///   </para>
      /// </summary>
      /// <param name="AName">
      ///   Имя коллекции данных
      /// </param>
      /// <param name="AID">
      ///   Идентификатор запроса, он вернется в результате
      /// </param>
      /// <param name="AParams">
      ///   Массив параметров метода
      /// </param>
      function Sub(const AName, AID : string; const AParams : TJSONArray = nil) : TDDPMethodCall;

      /// <summary>
      ///   Отписка от данных коллекции
      ///   <para>
      ///     {"msg":"unsub","id":"ZZZ"}
      ///   </para>
      /// </summary>
      /// <param name="AID">
      ///   Идентификатор запроса, он вернется в результате
      /// </param>
      function Unsub(const AID : string) : TDDPMethodCall;

    end;

  private type
    /// <summary>
    ///   Индивидуальная обработка результата и событие синхронизации для <c>TDDPMethodCall.Sync</c>
    /// </summary>
    /// <remarks>
    ///   Поддерживает учет ссылок.
    /// </remarks>
    TCustomDDPHandler = class(TInterfacedObject, IObjRef)
    strict private
      FHandler : TSansgcWSProtocolDDP_Client_MsgReference;
      FSyncEvent : TEvent;

    protected
      {$REGION 'IObjRef'}
      function GetObjRef : TObject;
      {$ENDREGION}

    public
      constructor Create(const AHandler : TSansgcWSProtocolDDP_Client_MsgReference;
        const ANeedEvent : boolean);
      destructor Destroy; override;

      property Handler : TSansgcWSProtocolDDP_Client_MsgReference  read FHandler;
      property SyncEvent : TEvent  read FSyncEvent;
    end;

    /// <summary>
    ///   Справочник индивидуальной обработки по "id" сообщения
    /// </summary>
    TIDCustomDDPHandlersDict = TDictionary<string, IObjRef>;

  strict private
    FSupportVersions: TStrings;
    FVersion: string;

  private
    FConnectionState : TConnectionState;
    FServerID, FSessionID: string;
    FOnDDPMsg: TSansgcWSProtocolDDP_Client_MsgEvent;
    FOnWriteData: TsgcWSMessageEvent;
    FIDCustomDDPHandlersDict : TIDCustomDDPHandlersDict;
    FIDCustomDDPHandlersCS : TCriticalSection;

  protected
    procedure DoEventConnect(aConnection: TsgcWSConnection); override;
    procedure DoEventDisconnect(aConnection: TsgcWSConnection; Code: Integer); override;
    procedure DoEventMessage(aConnection: TsgcWSConnection; const Text: string); override;

    procedure DoEventDDPMsg(AContext : TSansgcWSProtocolDDP_Client.TDDPMsgContext;
      var AAcquireContext : boolean); virtual;

    /// <summary>
    ///   Синхронное действие с ожиданием результата
    /// </summary>
    /// <param name="AID">
    ///   Идентификатор синхронного действия
    /// </param>
    /// <param name="AAction">
    ///   Действие перед ожиданием результата
    /// </param>
    /// <param name="AResultContext">
    ///   При успехе: cписок контекстов всех результатов полученных во время ожидания
    ///   <para>
    ///     ! Должен быть разрушен после использования!
    ///   </para>
    /// </param>
    /// <param name="AWaitTimeout">
    ///   Таймаут ожидания
    /// </param>
    /// <param name="AOnWaitStep">
    ///   Действие шага ожидания результата
    ///   <para>
    ///     Например, здесь можно вызывать <c>ProcessMessages()</c>, если требуется
    ///   </para>
    /// </param>
    /// <param name="AWaitStep">
    ///   Действие шага ожидания результата
    ///   <para>
    ///     Размер шага ожидания результата
    ///   </para>
    /// </param>
    /// <remarks>
    ///   Работа метода очень зависит от режима <c>FOwner.Client.NotifyEvents</c>
    ///   <para>
    ///     <c>neAsynchronous, neSynchronous</c> из ГЛАВНОГО потока будут работать
    ///     только при
    ///       <code Personality="Delphi">
    ///         AOnWaitStep = procedure begin Application.ProcessMessages() end
    ///       </code>
    ///   </para>
    ///   <para>
    ///     <c>neNoSync</c> из ГЛАВНОГО потока будет работать и без <c>AOnWaitStep</c>
    ///   </para>
    /// </remarks>
    /// <returns>
    ///  <c>True</c> — получен нужный ответ, <c>False</c> — таймаут ожидания превышен
    /// </returns>
    function SyncAction(const AID : string; const AAction : TProc;
      out AResultContexts : TDDPMsgContexts;
      const AWaitTimeout : Cardinal = INFINITE;
      const AOnWaitStep : TProc = nil;
      const AWaitStep : Cardinal = DDP_DEFAULT_WAIT_STEP) : boolean;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy(); override;

    /// <summary>
    ///   Вызов методов DDP
    /// </summary>
    function DDP : TDDPMethods;

    /// <summary>
    ///   Синхронное ожидание создания сессии или отказа
    /// </summary>
    /// <param name="AResultContext">
    ///   При успехе: cписок контекстов всех результатов полученных во время ожидания
    ///   <para>
    ///     ! Должен быть разрушен после использования!
    ///   </para>
    /// </param>
    /// <param name="AWaitTimeout">
    ///   Таймаут ожидания
    /// </param>
    /// <param name="AOnWaitStep">
    ///   Действие шага ожидания результата
    ///   <para>
    ///     Например, здесь можно вызывать <c>ProcessMessages()</c>, если требуется
    ///   </para>
    /// </param>
    /// <param name="AWaitStep">
    ///   Действие шага ожидания результата
    ///   <para>
    ///     Размер шага ожидания результата
    ///   </para>
    /// </param>
    /// <remarks>
    ///   Работа метода очень зависит от режима <c>FOwner.Client.NotifyEvents</c>
    ///   <para>
    ///     <c>neAsynchronous, neSynchronous</c> из ГЛАВНОГО потока будут работать
    ///     только при
    ///       <code Personality="Delphi">
    ///         AOnWaitStep = procedure begin Application.ProcessMessages() end
    ///       </code>
    ///   </para>
    ///   <para>
    ///     <c>neNoSync</c> из ГЛАВНОГО потока будет работать и без <c>AOnWaitStep</c>
    ///   </para>
    /// </remarks>
    /// <returns>
    ///  <c>True</c> — получен нужный ответ, <c>False</c> — таймаут ожидания превышен
    /// </returns>
    function SyncConnect(out AResultContexts : TDDPMsgContexts;
      const AWaitTimeout : Cardinal = INFINITE;
      const AOnWaitStep : TProc = nil;
      const AWaitStep : Cardinal = DDP_DEFAULT_WAIT_STEP) : boolean;

    /// <summary>
    ///   Состояние соединения
    /// </summary>
    property ConnectionState : TConnectionState  read FConnectionState;

    /// <summary>
    ///   ID сервера, полученное в начале подключения.
    /// </summary>
    /// <remarks>
    ///   В состояниях <c>csDisconnected</c>, <c>csWaitForServerID</c> - пустая строка
    /// </remarks>
    property ServerID : string  read FServerID;

    /// <summary>
    ///   ID сессии, созданой между клиентом и сервером
    /// </summary>
    /// <remarks>
    ///   В состояниях <c>csDisconnected</c>, <c>csWaitForServerID</c>, <c>csWaitForConnect</c> - пустая строка
    /// </remarks>
    property SessionID : string  read FSessionID;

  published
    /// <summary>
    ///   Версия при создании сессии
    /// </summary>
    property Version : string  read FVersion  write FVersion;

    /// <summary>
    ///   Поддерживаемые версии для создания сессии
    /// </summary>
    property SupportVersions : TStrings  read FSupportVersions;

    /// <summary>
    ///   Получено DDP-сообщение
    /// </summary>
    property OnDDPMsg : TSansgcWSProtocolDDP_Client_MsgEvent
      read FOnDDPMsg  write FOnDDPMsg;

    /// <summary>
    ///   Событие записи данных
    /// </summary>
    property OnWriteData : TsgcWSMessageEvent
      read FOnWriteData  write FOnWriteData;

    property Client;

    property OnConnect;
    property OnDisconnect;
    property OnError;
    property OnException;
    property OnMessage;
  end;

  /// <summary>
  ///   Компонент серверной части протокола DDP
  /// </summary>
  TSansgcWSProtocolDDP_Server = class(TsgcWSProtocol_Server_Base)
  public type
    {$REGION 'Msg kind and known msgs'}
    /// <summary>
    ///   Сообщения, которые ПРИХОДЯТ на сервер
    /// </summary>
    TMsgKind =
    (
      mksUnknown  = -1,
      mksConnect  = 0,
      mksPong     = 1,
      mksMethod   = 2,
      mksSub      = 3,
      mksUnsub    = 4
    );

  protected const
    /// <summary>
    ///   Известные сообщения. Соответствие строк сообщений и TMsgKind
    /// </summary>
    KNOWN_MSGs : array [Succ(Low(TMsgKind))..High(TMsgKind)] of string =
    (
      sConnect,
      sPong,
      sMethod,
      sSub,
      sUnsub
    );
    {$ENDREGION}

  public type
    /// <summary>
    ///   Данные о подключенном клиенте в рамках протокола
    /// </summary>
    TConnectionDDPData = class(TObject)
    strict private
      FServer : TSansgcWSProtocolDDP_Server;
      FConnection: TsgcWSConnection;
      FOriginalIdRunDoExecute : TIdContextRun;
      FSubscriptionsCS : TCriticalSection;
      FExtDataIsManged: boolean;
      FLastPing : TDateTime;

      class function GetCurrentIdServerContext: TIdServerContext; static;

    private
      FVersion, FSessionID: string;
      FSupportVersions: TStrings;
      FExtData: TObject;

      /// <summary>
      ///   Выполнение в потоке в формате Indy
      /// </summary>
      /// <remarks>
      ///   Действия в потоке соединения
      /// </remarks>
      function IdRunDoExecute(AContext: TIdContext): Boolean;

    protected
      procedure WriteData(const aText : string);

      /// <summary>
      ///   Попытка пинга
      /// </summary>
      /// <returns>
      ///   <c>True</c> — продолжаем работать, <c>False</c> — таймаут
      /// </returns>
      function TryPing() : boolean;

      property SubscriptionsCS : TCriticalSection  read FSubscriptionsCS;

    public
      constructor Create(const AServer : TSansgcWSProtocolDDP_Server; const AConnection: TsgcWSConnection);
      destructor Destroy; override;

      /// <summary>
      ///   Текущий контекст Indy-сервера
      /// </summary>
      class property CurrentIdServerContext : TIdServerContext  read GetCurrentIdServerContext;

      /// <summary>
      ///   Есть подписка на коллекцию
      /// </summary>
      function IsSubscribed(const ACollection : string) : boolean;

      /// <summary>
      ///   Вызов метода оповещения о добавлении данных по подписке
      ///   <para>
      ///      <c>{"msg":"added","collection":"XXX","id":"YYY","fields":ZZZ}</c>
      ///   </para>
      /// </summary>
      procedure Added(const ACollection, AID : string; const AFields : TJSONObject = nil);

      /// <summary>
      ///   Вызов метода оповещения о изменении данных по подписке
      ///   <para>
      ///      <c>{"msg":"changed","collection":"XXX","id":"YYY","fields":AAA, "cleared":[BBB]}</c>
      ///   </para>
      /// </summary>
      procedure Changed(const ACollection, AID : string; const AFields : TJSONObject; const ACleared : TStringDynArray = nil); overload;

      /// <summary>
      ///   Вызов метода оповещения о изменении данных по подписке
      ///   <para>
      ///      <c>{"msg":"changed","collection":"XXX","id":"YYY","fields":AAA, "cleared":[BBB]}</c>
      ///   </para>
      /// </summary>
      procedure Changed(const ACollection, AID : string; const ACleared : TStringDynArray); overload;

      /// <summary>
      ///   Вызов метода оповещения о добавлении данных по подписке
      ///   <para>
      ///      <c>{"msg":"removed","collection":"XXX","id":"YYY"}</c>
      ///   </para>
      /// </summary>
      procedure Removed(const ACollection, AID : string);

      /// <summary>
      ///   Соединение
      /// </summary>
      property Connection : TsgcWSConnection  read FConnection;

      /// <summary>
      ///   Версия при создании сессии
      /// </summary>
      /// <remarks>
      ///   Заполняется при <c>mksConnect</c>
      /// </remarks>
      property Version : string  read FVersion;

      /// <summary>
      ///   Поддерживаемые версии для создания сессии
      /// </summary>
      /// <remarks>
      ///   Заполняется при <c>mksConnect</c>
      /// </remarks>
      property SupportVersions : TStrings  read FSupportVersions;

      /// <summary>
      ///   ID сессии, созданой между клиентом и сервером
      /// </summary>
      /// <remarks>
      ///   Заполняется при <c>mksConnect</c>, методом <c>TDDPMsgCtx_Connected.AcceptSession</c>
      /// </remarks>
      property SessionID : string  read FSessionID;

      /// <summary>
      ///   Дополнительные данные сессии для внешнего использования
      /// </summary>
      property ExtData : TObject  read FExtData  write FExtData;

      /// <summary>
      ///   <c>True</c> — объект <c>ExtData</c> будет очищен в деструкторе через <c>FreeAndNil</c>
      /// </summary>
      property ExtDataIsManged : boolean  read FExtDataIsManged  write FExtDataIsManged  default True;
    end;

    {$REGION 'TDDPMsgCtx'}
    /// <summary>
    ///   Базовый контекст события DDP-сообщения на сервере
    /// </summary>
    TDDPMsgContext = class abstract(TSansgcWSProtocolDDP_DDPMsgCustomContext<TSansgcWSProtocolDDP_Server>)
    strict private
      function GetConnectionDDPData: TConnectionDDPData;

    public
      class function Msg : string; override;

      /// <summary>
      ///   Тип клиентского сообщения
      /// </summary>
      class function MsgKind : TMsgKind; virtual;

      /// <summary>
      ///   Данные DDP в рамках подключения
      /// </summary>
      property ConnectionDDPData : TConnectionDDPData  read GetConnectionDDPData;
    end;

    TSansgcWSProtocolDDP_Server_MsgEvent = procedure(AContext : TSansgcWSProtocolDDP_Server.TDDPMsgContext;
      var AAcquireContext : boolean) of object;

    /// <summary>
    ///   Контекст события DDP-сообщения
    /// <para>
    ///   <c>{"msg":"connect","version":"XXX", "support":[YYY]}</c>
    /// </para>
    /// </summary>
    TDDPMsgCtx_Connect = class(TDDPMsgContext)
    strict private type
      TState = (sttNotChosen, sttAccepted, sttRejected);

    strict private
      FState : TState;

      function GetSupportVersions: TStrings;
      function GetVersion: string;

    protected
      procedure Init(); override;

      /// <summary>
      ///   Общий код для <c>AcceptSession</c> и <c>RejectSession</c>
      /// </summary>
      procedure Choice(const AState : TState; const AValue : string);

    public
      class function MsgKind : TMsgKind; override;

      /// <summary>
      ///   Принять сессию
      /// </summary>
      /// <remarks>
      ///   В обработчике необходимо вызвать один раз один из методов
      ///   <para>
      ///     <c>AcceptSession</c>
      ///   </para>
      ///   или
      ///   <para>
      ///     <c>RejectSession</c>
      ///   </para>
      /// </remarks>
      procedure AcceptSession(const ASessionID : string);

      /// <summary>
      ///   Отклонить сессию
      /// </summary>
      /// <remarks>
      ///   В обработчике необходимо вызвать один раз один из методов
      ///   <para>
      ///     <c>AcceptSession</c>
      ///   </para>
      ///   или
      ///   <para>
      ///     <c>RejectSession</c>
      ///   </para>
      /// </remarks>
      procedure RejectSession(const AError : string);

      /// <summary>
      ///   Версия при создании сессии
      /// </summary>
      property Version : string  read GetVersion;

      /// <summary>
      ///   Поддерживаемые версии для создания сессии
      /// </summary>
      property SupportVersions : TStrings  read GetSupportVersions;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    /// <para>
    ///   <c>{"msg":"pong"}</c>
    /// </para>
    /// </summary>
    TDDPMsgCtx_Pong = class(TDDPMsgContext)
    public
      class function MsgKind : TMsgKind; override;
    end;

    /// <summary>
    ///   Абстрактный контекст одного из событий DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"method","method":"XXX","params":[YYY],"id":"ZZZ"}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"sub","id":"ZZZ","name":"XXX","params":[YYY]}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"unsub","id":"ZZZ"}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_CustomID = class abstract(TDDPMsgContext)
    strict private
      FID : TJSONString;
      function GetID: string;

    protected
      procedure Init(); override;

    public
      /// <summary>
      ///   Идентификатор запроса
      /// </summary>
      property ID : string  read GetID;
    end;

    /// <summary>
    ///   Абстрактный контекст одного из событий DDP-сообщения
    ///   <para>
    ///     <c>{"msg":"method","method":"XXX","params":[YYY],"id":"ZZZ"}</c>
    ///   </para>
    ///   <para>
    ///     <c>{"msg":"sub","id":"ZZZ","name":"XXX","params":[YYY]}</c>
    ///   </para>
    /// </summary>
    TDDPMsgCtx_CustomIDParams = class abstract(TDDPMsgCtx_CustomID)
    strict private
      FParams : TJSONArray;

    protected
      procedure Init(); override;

    public
      /// <summary>
      ///   JSON-массив JSON-объектов, параметров запроса
      ///   перестает быть собственностью контекста
      /// </summary>
      /// <returns>
      ///   Self.Params
      /// </returns>
      function AcquireParams : TJSONArray;

      /// <summary>
      ///   JSON-массив JSON-объектов, параметров запроса
      ///   <para>
      ///     Может быть пустым (nil)
      ///   </para>
      /// </summary>
      /// <remarks>
      ///   Объет в собственности контекста события и будет разрущен после него.
      ///   <para>
      ///     Для захвата объекта в собственность исп. <c>AcquireParams</c>
      ///   </para>
      /// </remarks>
      property Params : TJSONArray  read FParams;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    /// <para>
    ///   <c>{"msg":"method","method":"XXX","params":[YYY],"id":"ZZZ"}</c>
    /// </para>
    /// </summary>
    TDDPMsgCtx_Method = class(TDDPMsgCtx_CustomIDParams)
    strict private
      FMethod : TJSONString;
      FUpdated, FResult : boolean;
      function GetMethod : string;

    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

      /// <summary>
      ///   Отправка сообщения updated.
      ///   <para>
      ///     <c>{"msg":"updated","methods":[ZZZ]}</c>
      ///   </para>
      /// </summary>
      procedure Updated();

      /// <summary>
      ///   Отправка сообщения result при успехе.
      ///   <para>
      ///     <c>{"msg":"result","id":"ZZZ","result":AAA}</c>
      ///   </para>
      /// </summary>
      procedure Result(const AResult : TJSONObject = nil); overload;

      /// <summary>
      ///   Отправка сообщения result при ошибке.
      ///   <para>
      ///     <c>{"msg":"result","id":"ZZZ","error":BBB}</c>
      ///   </para>
      /// </summary>
      procedure Result(const AError : string); overload;

      /// <summary>
      ///   Имя метода
      /// </summary>
      property Method : string  read GetMethod;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    /// <para>
    ///   <c>{"msg":"sub","id":"ZZZ","name":"XXX","params":[YYY]}</c>
    /// </para>
    /// </summary>
    TDDPMsgCtx_Sub = class(TDDPMsgCtx_CustomIDParams)
    strict private
      FName : TJSONString;
      FReadyOrResult : boolean;
      function GetName : string;

    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

      /// <summary>
      ///   Есть подписка с таким id
      /// </summary>
      function IsIDSubscribed() : boolean;

      /// <summary>
      ///   Отправка сообщения ready.
      ///   <para>
      ///     <c>{"msg":"ready","subs":["ZZZ"]}</c>
      ///   </para>
      /// </summary>
      procedure Ready();

      /// <summary>
      ///   Отправка сообщения result при ошибке.
      ///   <para>
      ///     <c>{"msg":"result","id":"ZZZ","error":BBB}</c>
      ///   </para>
      /// </summary>
      procedure Result(const AError : string);

      /// <summary>
      ///   Имя коллекции данных
      /// </summary>
      property Name : string  read GetName;
    end;

    /// <summary>
    ///   Контекст события DDP-сообщения
    /// <para>
    ///   <c>{"msg":"unsub","id":"ZZZ"}</c>
    /// </para>
    /// </summary>
    TDDPMsgCtx_Unsub = class(TDDPMsgCtx_CustomID)
    strict private
      FResult : boolean;

    protected
      procedure Init(); override;

    public
      class function MsgKind : TMsgKind; override;

      /// <summary>
      ///   Есть подписка с таким id
      /// </summary>
      function IsIDSubscribed() : boolean;

      /// <summary>
      ///   Отправка сообщения result при успехе.
      ///   <para>
      ///     <c>{"msg":"result","id":"ZZZ"}</c>
      ///   </para>
      /// </summary>
      procedure Result(); overload;

      /// <summary>
      ///   Отправка сообщения result при ошибке.
      ///   <para>
      ///     <c>{"msg":"result","id":"ZZZ","error":BBB}</c>
      ///   </para>
      /// </summary>
      procedure Result(const AError : string); overload;
    end;
    {$ENDREGION}

  strict private
    FServerID: string;
    FPingInterval: Cardinal;
    FOnGetDynServerID: TSansgcWSProtocolDDP_Server_GetDynServerIDEvent;
    FOnThreadError: TSansgcWSProtocolDDP_ThreadErrorEvent;
    FOnDDPMsg: TSansgcWSProtocolDDP_Server_MsgEvent;
    FOnWriteData: TsgcWSMessageEvent;
    FPolite: boolean;

    function GetConnectionDDPData(const aConnection: TsgcWSConnection): TConnectionDDPData;

  protected
    procedure DoWriteData(aConnection: TsgcWSConnection; const aText: String);

    procedure DoEventConnect(aConnection: TsgcWSConnection); override;
    procedure DoEventDisconnect(aConnection: TsgcWSConnection; Code: Integer); override;
    procedure DoEventMessage(aConnection: TsgcWSConnection; const Text: string); override;

    procedure DoEventDDPMsg(AContext : TSansgcWSProtocolDDP_Server.TDDPMsgContext;
      var AAcquireContext : boolean); virtual;
    procedure DoEventGetDynServerID(aConnection: TsgcWSConnection; var AServerID : string); virtual;
    procedure DoEventSubThreadError(AThread : TThread; AError : Exception); virtual;

  public
    constructor Create(AOwner : TComponent); override;
    procedure BeforeDestruction; override;
    destructor Destroy; override;

    /// <summary>
    ///   Перебор всех соединений
    /// </summary>
    /// <param name="AEnumProc">
    ///   Действие на соединением
    /// </param>
    /// <param name="Direction">
    ///   Направление перебора: <c>FromBeginning</c> — с начала, <c>FromEnd</c> — с конца
    /// </param>
    procedure EnumConnections(const AEnumAction : TProc<TsgcWSConnection>; const ADirection : System.Types.TDirection = FromEnd);

    /// <summary>
    ///   Перебор всех данных DDP в соединениях
    /// </summary>
    /// <param name="AEnumProc">
    ///   Действие на соединением
    /// </param>
    /// <param name="Direction">
    ///   Направление перебора: <c>FromBeginning</c> — с начала, <c>FromEnd</c> — с конца
    /// </param>
    procedure EnumConnectionsDDPData(const AEnumAction : TProc<TConnectionDDPData>; const ADirection : System.Types.TDirection = FromEnd);

    /// <summary>
    ///   Дополнительные данные для соединения.
    /// </summary>
    property ConnectionDDPData[const aConnection: TsgcWSConnection] : TConnectionDDPData
      read GetConnectionDDPData;

  published
    /// <summary>
    ///   Статический ID сервера
    /// </summary>
    property ServerID : string  read FServerID  write FServerID;

    /// <summary>
    ///   Интервал между пингами
    /// </summary>
    /// <remarks>
    ///   Если ответ не пришел примерно в этом интервале, сервер разорвет соединение
    /// </remarks>
    property PingInterval : Cardinal  read FPingInterval  write FPingInterval default DDP_DEFAULT_PING_INTERVAL;

    /// <summary>
    ///   <c>Polite=True</c> — вежливый сервер отвечает клиентам, грубо нарушающим протокол
    ///   <para>
    ///     <c>{"msg":"error","error":BBB}</c>
    ///   </para>
    ///   <c>Polite=False</c> — не вежливый сервер игнорирует клиентов, грубо нарушающих протокол
    /// </summary>
    property Polite : boolean  read FPolite  write FPolite  default False;

    /// <summary>
    ///   Обработчик для выдачи динамического ID сервера
    /// </summary>
    property OnGetDynServerID : TSansgcWSProtocolDDP_Server_GetDynServerIDEvent
      read FOnGetDynServerID  write FOnGetDynServerID;

    /// <summary>
    ///   Ошибка в контексте потока
    /// </summary>
    property OnThreadError : TSansgcWSProtocolDDP_ThreadErrorEvent
      read FOnThreadError  write FOnThreadError;

    /// <summary>
    ///   Получено DDP-сообщение
    /// </summary>
    property OnDDPMsg : TSansgcWSProtocolDDP_Server_MsgEvent
      read FOnDDPMsg  write FOnDDPMsg;

    /// <summary>
    ///   Событие записи данных
    /// </summary>
    property OnWriteData : TsgcWSMessageEvent
      read FOnWriteData  write FOnWriteData;

    property Server;

    property OnConnect;
    property OnDisconnect;
    property OnError;
    property OnException;
    property OnMessage;
  end;

{$IFNDEF USE_ALIASES}
implementation

uses
  WinApi.Windows;

function JSONArrToStrArr(const AJSONArr : TJSONArray) : TStringDynArray;
var
  i : integer;
begin
  if not Assigned(AJSONArr) then
    Exit(nil);

  SetLength(Result, AJSONArr.Count);
  for i := 0 to AJSONArr.Count - 1 do
    Result[i] := AJSONArr.Items[i].Value;
end;

function MakeMsgJSON(const AMsg : string) : TJSONObject;
var
  P : TJSONPair;
begin
  P := TJSONPair.Create(sMsg, AMsg);
  try

   Result := TJSONObject.Create(P);

  except
    FreeAndNil(P);
    raise;
  end;
end;

function JSONFromMessage(const AMessage : string) : TJSONObject;
var
  V : TJSONValue;
begin
  try

    V := TJSONObject.ParseJSONValue(AMessage);
    try

      if not Assigned(V) then
        raise ESansgcWSProtocolDDP.CreateRes(@sMessageJSONParsingError);

      Result := V as TJSONObject;

    except
      FreeAndNil(V);
      raise;
    end;

  except
    Exception.RaiseOuterException( ESansgcWSProtocolDDP.CreateRes(@sMessageMustBeInJSONObjectFormatInUTF8) );
    raise;// suppress compiler warnings
  end;

end;

type
  TThreadHack = class(TThread);

function WaitEvent(const AEvent : TEvent; const AWaitTimeout: Cardinal;
  const AOnWaitStep: TProc; const AWaitStep : Cardinal) : boolean;
var
  Handles : THandles2;
  WaitRes, Elapsed : Cardinal;
begin
  if TThread.Current is TEventedThread then
    Handles[0] := (TThread.Current as TEventedThread).TerminateEvent
  else
    Handles[0] := AEvent.Handle;

  Handles[1] := AEvent.Handle;

  Elapsed := 0;
  repeat
    WaitRes := WaitForMultipleObjects(Succ(High(Handles)), @Handles, False, Min(AWaitStep, AWaitTimeout - Elapsed) - 1);  // -1 to Sleep(1)
    case WaitRes of
      WAIT_OBJECT_0, WAIT_OBJECT_0 + 1:
        Break;

      WAIT_TIMEOUT:
      begin
        Sleep(1);
        Inc(Elapsed, AWaitStep);

        if Assigned(AOnWaitStep) then
          AOnWaitStep();

        if Elapsed >= AWaitTimeout then
          Exit(False);
      end;

      WAIT_FAILED:
        RaiseLastOSError;

    else
      raise EThread.CreateResFmt(@sUnexpectedWaitResultFmt, [WaitRes]);
    end;

  until TThreadHack(TThread.Current).Terminated;

  Result := not TThreadHack(TThread.Current).Terminated;
end;

{$REGION 'TSansgcWSProtocolDDP_DDPMsgCustomContext'}
class procedure TSansgcWSProtocolDDP_DDPMsgCustomContext.RegClass;
begin
  MsgClasses.Add(Self.Msg, Self);
end;

class procedure TSansgcWSProtocolDDP_DDPMsgCustomContext.RegClasses(
  const AClasses: array of TSansgcWSProtocolDDP_DDPMsgCustomContextClass);
var
  C : TSansgcWSProtocolDDP_DDPMsgCustomContextClass;
begin
  for C in AClasses do
    C.RegClass;
end;

function TSansgcWSProtocolDDP_DDPMsgCustomContext.TryGetValue<T>(
  const APath: string; out AValue: T): Boolean;
begin
  Result := FRawData.TryGetValue<T>(APath, AValue);
  if Result then
    Exit;

  if not Assigned(FKeys) then
    FKeys := TStringList.Create();

  FKeys.Add(APath);
end;

procedure TSansgcWSProtocolDDP_DDPMsgCustomContext.ValidateKeys;
begin
  if Assigned(FKeys) then
    raise ESansgcWSProtocolDDP.CreateResFmt(@sInMessageWithMsgAAAKeysBBBExpectedFmt, [Msg, FKeys.CommaText]);
end;

class procedure TSansgcWSProtocolDDP_DDPMsgCustomContext.Validation(
  const ASender : TsgcWSProtocol; const AConnection: TsgcWSConnection;
  const ARawData : TJSONObject);
begin
end;

procedure TSansgcWSProtocolDDP_DDPMsgCustomContext.WriteData(
  const aText: string);
var
  OnWriteData : TsgcWSMessageEvent;
begin
  if Sender is TSansgcWSProtocolDDP_Client then
    OnWriteData := TSansgcWSProtocolDDP_Client(Sender).OnWriteData
  else
  if Sender is TSansgcWSProtocolDDP_Server then
    OnWriteData := TSansgcWSProtocolDDP_Server(Sender).OnWriteData
  else
    OnWriteData := nil;

  Connection.WriteData(aText);

  if Assigned(OnWriteData) then
    OnWriteData(Connection, aText);
end;

class procedure TSansgcWSProtocolDDP_DDPMsgCustomContext.Make(
  const ASender: TsgcWSProtocol; const AConnection: TsgcWSConnection;
  var ARawData: TJSONObject; out AContext);
var
  V : TJSONValue;
  C : TSansgcWSProtocolDDP_DDPMsgCustomContextClass;
begin
  if not (ARawData.TryGetValue(sMsg, V) and (V.Value <> '')) then
    raise ESansgcWSProtocolDDP.CreateRes(@sMessageMustContainNotEmptyMsgKey);

  try
    C := MsgClasses[V.Value];
  except
    Exception.RaiseOuterException( ESansgcWSProtocolDDP.CreateResFmt(@sMsgContextClassNotRegistredForXXXFmt, [V.Value]));
    raise;//suppress compiler warnings
  end;

  C.Validation(ASender, AConnection, ARawData);

  TObject(AContext) := C.Create(ASender, AConnection, ARawData);

  // JSON-объект стал собственностью контекста
  ARawData := nil;
end;

class function TSansgcWSProtocolDDP_DDPMsgCustomContext.Msg: string;
begin
  raise ENotImplemented.CreateFmt('%s.%s.%s', [UnitName, ClassName, 'Msg']);
end;

class function TSansgcWSProtocolDDP_DDPMsgCustomContext.MsgClasses: TMsgClassesDict;
begin
  raise ENotImplemented.CreateFmt('%s.%s.%s', [UnitName, ClassName, 'MsgClasses']);
end;

procedure TSansgcWSProtocolDDP_DDPMsgCustomContext.Init();
begin
end;

constructor TSansgcWSProtocolDDP_DDPMsgCustomContext.Create(
  const ASender: TsgcWSProtocol; const AConnection: TsgcWSConnection;
  const ARawData: TJSONObject);
begin
  inherited Create();

  FSender := ASender;
  FConnection := AConnection;
  FRawData := ARawData;
  FKeys := nil;

  Init();
end;

destructor TSansgcWSProtocolDDP_DDPMsgCustomContext.Destroy;
begin
  FreeAndNil(FKeys);
  FreeAndNil(FRawData);
  inherited;
end;

{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_DDPMsgCustomContext<TSender>'}
function TSansgcWSProtocolDDP_DDPMsgCustomContext<TSender>.GetSender: TSender;
begin
  Result := (inherited Sender) as TSender;
end;

class function TSansgcWSProtocolDDP_DDPMsgCustomContext<TSender>.MsgClasses: TSansgcWSProtocolDDP_DDPMsgCustomContext.TMsgClassesDict;
begin
  Result := FMsgClasses;
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgContext'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgContext.Msg: string;
var
  MsgKind : TMsgKind;
begin
  MsgKind := Self.MsgKind;

  Assert(MsgKind > mkcUnknown);

  Result := KNOWN_MSGs[MsgKind];
end;

class function TSansgcWSProtocolDDP_Client.TDDPMsgContext.MsgKind: TMsgKind;
begin
  raise ENotImplemented.CreateFmt('%s.%s.%s', [UnitName, ClassName, 'MsgKind']);
end;

class function TSansgcWSProtocolDDP_Client.TDDPMsgContext.ExpectedConnectionStates: TConnectionStates;
begin
  raise ENotImplemented.CreateFmt('%s.%s.%s', [UnitName, ClassName, 'ExpectedConnectionStates']);
end;

class procedure TSansgcWSProtocolDDP_Client.TDDPMsgContext.Validation(
  const ASender : TsgcWSProtocol; const AConnection: TsgcWSConnection;
  const ARawData : TJSONObject);
var
  State : TConnectionState;
begin
  State := (ASender as TSansgcWSProtocolDDP_Client).FConnectionState;
  if not (State in ExpectedConnectionStates) then
    raise ESansgcWSProtocolDDP.CreateResFmt(@sUnexpectedMessageInStateAAAFmt, [GetEnumName(TypeInfo(TConnectionState), Ord(State))]);
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgContexts'}
function TSansgcWSProtocolDDP_Client.TDDPMsgContexts.TryFind<T>(
  out AContext: T): boolean;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    if Self[i] is T then
    begin
      AContext := Self[i] as T;
      Exit(True);
    end;

  AContext  := nil;
  Result    := False;
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_CustomWaitSession'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_CustomWaitSession.ExpectedConnectionStates: TConnectionStates;
begin
  Result := [csWaitForConnect];
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_CustomConnectedSession'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_CustomConnectedSession.ExpectedConnectionStates: TConnectionStates;
begin
  Result := [csConnected];
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Connected'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Connected.MsgKind: TMsgKind;
begin
  Result := mkcConnected;
end;

function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Connected.GetSessionID: string;
begin
  Result := Sender.SessionID;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Connected.Init;
var
  S : TJSONString;
begin
  TryGetValue(sSession, S);
  ValidateKeys();

  Sender.FSessionID := S.Value;
  Sender.FConnectionState := csConnected;
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Rejected'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Rejected.MsgKind: TMsgKind;
begin
  Result := mkcRejected;
end;

function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Rejected.GetError: string;
begin
  Result := FError.Value;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Rejected.Init;
begin
  TryGetValue(sError, FError);
  ValidateKeys();

  Connection.Disconnect();
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Ping'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Ping.MsgKind: TMsgKind;
begin
  Result := mkcPing;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Ping.Init;
var
  J : TJSONObject;
begin
  J := MakeMsgJSON(sPong);
  try
    WriteData(J.ToJSON());
  finally
    FreeAndNil(J);
  end;
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Updated'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Updated.MsgKind: TMsgKind;
begin
  Result := mkcUpdated;
end;

function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Updated.GetMethods: TStringDynArray;
begin
  if not Assigned(FMethods) then
    FMethods := JSONArrToStrArr(FMethodsJ);

  Result := FMethods;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Updated.Init;
begin
  TryGetValue(sMethods, FMethodsJ);
  ValidateKeys();
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result.MsgKind: TMsgKind;
begin
  Result := mkcResult;
end;

function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result.GetID: string;
begin
  Result := FID.Value;
end;

function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result.GetError: string;
begin
  if FSuccess then
    Result := ''
  else
    Result := FError.Value;
end;

function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result.AcquireResult: TJSONObject;
begin
  if not Assigned(FResult) then
    Exit(nil);

  Result := FResult;
  FResult.Owned := False;
  FResult := nil;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result.Init;
begin
  TryGetValue(sID, FID);
  ValidateKeys;

  RawData.TryGetValue(sResult, FResult);
  FSuccess := not RawData.TryGetValue(sError, FError);
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Ready'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Ready.MsgKind: TMsgKind;
begin
  Result := mkcReady;
end;

function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Ready.GetSubs: TStringDynArray;
begin
  if not Assigned(FSubs) then
    FSubs := JSONArrToStrArr(FSubsJ);

  Result := FSubs;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Ready.Init;
begin
  TryGetValue(sSubs, FSubsJ);
  ValidateKeys();
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_CustomCollection'}
function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_CustomCollection.GetCollection: string;
begin
  Result := FCollection.Value;
end;

function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_CustomCollection.GetID: string;
begin
  Result := FID.Value;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_CustomCollection.Init;
begin
  TryGetValue(sCollection, FCollection);
  TryGetValue(sID, FID);
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_CustomCollectionFields'}
function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_CustomCollectionFields.AcquireFields: TJSONObject;
begin
  if not Assigned(FFields) then
    Exit(nil);

  Result := FFields;
  FFields.Owned := False;
  FFields := nil;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_CustomCollectionFields.Init;
begin
  inherited;
  RawData.TryGetValue(sFields, FFields);
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Added'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Added.MsgKind: TMsgKind;
begin
  Result := mkcAdded;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Added.Init;
begin
  inherited;
  ValidateKeys();
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Changed'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Changed.MsgKind: TMsgKind;
begin
  Result := mkcChanged;
end;

function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Changed.GetCleared: TStringDynArray;
begin
  if not Assigned(FCleared) then
    FCleared := JSONArrToStrArr(FClearedJ);

  Result := FCleared;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Changed.Init;
begin
  inherited;
  RawData.TryGetValue(sCleared, FClearedJ);
  ValidateKeys();
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Removed'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Removed.MsgKind: TMsgKind;
begin
  Result := mkcRemoved;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Removed.Init;
begin
  inherited;
  ValidateKeys();
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Error'}
class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Error.ExpectedConnectionStates: TConnectionStates;
begin
  Result := [Low(TConnectionState)..High(TConnectionState)];
end;

class function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Error.MsgKind: TMsgKind;
begin
  Result := mkcError;
end;

function TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Error.GetError: string;
begin
  Result := FError.Value;
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Error.Init;
begin
  inherited;
  TryGetValue(sError, FError);
  ValidateKeys;

  Sender.DoEventError(Connection, Error);
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMethodCall'}
procedure TSansgcWSProtocolDDP_Client.TDDPMethodCall.Init(
  const AOwner: TSansgcWSProtocolDDP_Client; const AID : string;
  const AData: TJSONObject);
begin
  FOwner := AOwner;
  FID := AID;
  FData := AData;
  FHolder := San.Intfs.Api.Hold(FData);
end;

procedure TSansgcWSProtocolDDP_Client.TDDPMethodCall.Async(
  const AOnResult: TSansgcWSProtocolDDP_Client_MsgReference);
var
  CH : TCustomDDPHandler;
begin
  if Assigned(AOnResult) then
  begin
    CH := TCustomDDPHandler.Create(AOnResult, False);

    FOwner.FIDCustomDDPHandlersCS.Enter;
    try
      FOwner.FIDCustomDDPHandlersDict.Add(FID, CH);
    finally
      FOwner.FIDCustomDDPHandlersCS.Leave;
    end;

  end;

  FOwner.WriteData(FData.ToJSON());
  if Assigned(FOwner.OnWriteData) then
    FOwner.OnWriteData(nil, FData.ToJSON());
end;

function TSansgcWSProtocolDDP_Client.TDDPMethodCall.Sync(
  out AResultContexts : TDDPMsgContexts;
  const AWaitTimeout: Cardinal;
  const AOnWaitStep: TProc;
  const AWaitStep : Cardinal): boolean;
var
  ClientDDP : TSansgcWSProtocolDDP_Client;
  Data : TJSONObject;
begin
  ClientDDP  := FOwner;
  Data       := FData;

  Result := FOwner.SyncAction
  (
    FID,
    procedure
    begin
      ClientDDP.WriteData(Data.ToJSON());
      if Assigned(ClientDDP.OnWriteData) then
        ClientDDP.OnWriteData(nil, Data.ToJSON());
    end,
    AResultContexts,
    AWaitTimeout,
    AOnWaitStep,
    AWaitStep
  );
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TDDPMethods'}
function TSansgcWSProtocolDDP_Client.TDDPMethods.Method(const AMethod,
  AID: string; const AParams: TJSONArray): TDDPMethodCall;
var
  J : TJSONObject;
begin
  J := MakeMsgJSON(sMethod);
  try
    J.AddPair(sMethod, AMethod);
    J.AddPair(sID, AID);

    if Assigned(AParams) then
      J.AddPair(sParams, AParams);

    Result.Init(FOwner, AID, J);
  except
    FreeAndNil(J);
    raise;
  end;
end;

function TSansgcWSProtocolDDP_Client.TDDPMethods.Method(const AMethod,
  AID: string; const AParam: TJSONObject): TDDPMethodCall;
var
  JParams: TJSONArray;
begin
  JParams := TJSONArray.Create(AParam);
  try
    Result := Method(AMethod, AID, JParams);
  except
    FreeAndNil(JParams);
    raise;
  end;
end;

function TSansgcWSProtocolDDP_Client.TDDPMethods.Sub(const AName, AID: string;
  const AParams: TJSONArray): TDDPMethodCall;
var
  J : TJSONObject;
begin
  J := MakeMsgJSON(sSub);
  try
    J.AddPair(sName, AName);
    J.AddPair(sID, AID);

    if Assigned(AParams) then
      J.AddPair(sParams, AParams);

    Result.Init(FOwner, AID, J);
  except
    FreeAndNil(J);
    raise;
  end;
end;

function TSansgcWSProtocolDDP_Client.TDDPMethods.Unsub(
  const AID: string): TDDPMethodCall;
var
  J : TJSONObject;
begin
  J := MakeMsgJSON(sUnsub);
  try
    J.AddPair(sID, AID);

    Result.Init(FOwner, AID, J);
  except
    FreeAndNil(J);
    raise;
  end;
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client.TCustomDDPHandler'}
function TSansgcWSProtocolDDP_Client.TCustomDDPHandler.GetObjRef: TObject;
begin
  Result := Self;
end;

constructor TSansgcWSProtocolDDP_Client.TCustomDDPHandler.Create(
  const AHandler: TSansgcWSProtocolDDP_Client_MsgReference;
  const ANeedEvent: boolean);
begin
  inherited Create;

  FHandler := AHandler;

  if ANeedEvent then
    FSyncEvent := TEvent.Create(nil, True, False, '');
end;

destructor TSansgcWSProtocolDDP_Client.TCustomDDPHandler.Destroy;
begin
  FHandler := nil;
  FreeAndNil(FSyncEvent);
  inherited;
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Client'}
constructor TSansgcWSProtocolDDP_Client.Create(AOwner: TComponent);
begin
  inherited;

  FProtocol := sDDP;

  FVersion := '1';

  FSupportVersions := TStringList.Create;
  FSupportVersions.Add('1');
  FSupportVersions.Add('pre2');
  FSupportVersions.Add('pre1');

  FIDCustomDDPHandlersDict := TIDCustomDDPHandlersDict.Create();
  FIDCustomDDPHandlersCS := TCriticalSection.Create;
end;

destructor TSansgcWSProtocolDDP_Client.Destroy;
begin
  FreeAndNil(FSupportVersions);
  FreeAndNil(FIDCustomDDPHandlersDict);
  FreeAndNil(FIDCustomDDPHandlersCS);
  inherited;
end;

function TSansgcWSProtocolDDP_Client.DDP: TDDPMethods;
begin
  Result.FOwner := Self;
end;

procedure TSansgcWSProtocolDDP_Client.DoEventConnect(
  aConnection: TsgcWSConnection);
begin
  inherited;
  FConnectionState := csWaitForServerID;
end;

procedure TSansgcWSProtocolDDP_Client.DoEventDisconnect(
  aConnection: TsgcWSConnection; Code: Integer);
var
  Intf : IObjRef;
  CH : TCustomDDPHandler;
begin
  inherited;
  FConnectionState := csDisconnected;

  FIDCustomDDPHandlersCS.Enter();
  try
    for Intf in FIDCustomDDPHandlersDict.Values do
    begin
      CH := Intf.ObjRef as TCustomDDPHandler;
      CH.SyncEvent.SetEvent();
    end;

    FIDCustomDDPHandlersDict.Clear;
  finally
    FIDCustomDDPHandlersCS.Leave();
  end;

end;

procedure TSansgcWSProtocolDDP_Client.DoEventMessage(
  aConnection: TsgcWSConnection; const Text: string);

  procedure RecvServerID(const AServerID : string);
  var
    J : TJSONObject;
    A : TJSONArray;
    i : integer;
  begin
    FServerID := AServerID;

    J := MakeMsgJSON(sConnect);
    try
      J.AddPair(sVersion, FVersion);

      A := TJSONArray.Create();
      try

        for i := 0 to FSupportVersions.Count - 1 do
          A.Add(FSupportVersions[i]);

        J.AddPair(sSupport, A);
        A := nil;

      finally
        FreeAndNil(A);
      end;

      aConnection.WriteData(J.ToJSON());
      if Assigned(OnWriteData) then
        OnWriteData(aConnection, J.ToJSON());

    finally
      FreeAndNil(J);
    end;

    FConnectionState := csWaitForConnect;
  end;

var
  J : TJSONObject;
  V : TJSONValue;
  AcquireContext : boolean;
  C : TDDPMsgContext;
begin
  try

      try

      Assert(FConnectionState <> csDisconnected);

      J := JSONFromMessage(Text);
      try

        if FConnectionState = csWaitForServerID then
        begin
          if J.TryGetValue(sServerID, V) then
            RecvServerID(V.Value)
          else
            raise ESansgcWSProtocolDDP.CreateResFmt(@sUnexpectedMessageInStateAAAFmt, [GetEnumName(TypeInfo(TConnectionState), Ord(FConnectionState))]);
        end

        else
        begin
          AcquireContext := False;

          TDDPMsgContext.Make(Self, aConnection, J, C);
          try
            DoEventDDPMsg(C, AcquireContext);
          finally
            if not AcquireContext then
              FreeAndNil(C);
          end;
        end;

      finally
        FreeAndNil(J);
      end;

      inherited;

    except
      Exception.RaiseOuterException( ESansgcWSProtocolDDP.CreateResFmt(@sInternalErrorForMessageXXXFmt, [Text]));
    end;

  except
    on E : Exception do
    begin
      if Assigned(OnException) then
        OnException(aConnection, E);
    end;
  end;
end;

function TSansgcWSProtocolDDP_Client.SyncAction(const AID: string;
  const AAction: TProc; out AResultContexts: TDDPMsgContexts;
  const AWaitTimeout: Cardinal; const AOnWaitStep: TProc;
  const AWaitStep: Cardinal): boolean;
var
  ResultContexts : TDDPMsgContexts;
  CH : TCustomDDPHandler;
begin
  AResultContexts := nil;

  ResultContexts := nil;
  try

    CH := TCustomDDPHandler.Create(
      procedure (AContext : TDDPMsgContext; var AAcquireContext, AHandled : boolean)
      begin

        if not Assigned(ResultContexts) then
          ResultContexts := TDDPMsgContexts.Create(True);

        ResultContexts.Add(AContext);
        AAcquireContext := True;
        AHandled := True;
      end,
      True
    );

    CH._AddRef();
    try

      FIDCustomDDPHandlersCS.Enter;
      try
        FIDCustomDDPHandlersDict.Add(AID, CH);
      finally
        FIDCustomDDPHandlersCS.Leave;
      end;

      if Assigned(AAction) then
        AAction();

      Result := WaitEvent(CH.SyncEvent, AWaitTimeout, AOnWaitStep, AWaitStep)
            and (ConnectionState = csConnected);
      if Result then
      begin
        AResultContexts := ResultContexts;
        ResultContexts := nil;
      end;

    finally
      CH._Release;
      ZeroMemory(@CH, SizeOf(TCustomDDPHandler));
    end;

  finally
    FreeAndNil(ResultContexts);
  end;
end;

function TSansgcWSProtocolDDP_Client.SyncConnect(
  out AResultContexts: TDDPMsgContexts; const AWaitTimeout: Cardinal;
  const AOnWaitStep: TProc; const AWaitStep: Cardinal): boolean;
begin
  Result := (ConnectionState = csConnected)
         or SyncAction
         (
           sConnect,
           nil,
           AResultContexts,
           AWaitTimeout,
           AOnWaitStep,
           AWaitStep
         );
end;

procedure TSansgcWSProtocolDDP_Client.DoEventDDPMsg(AContext : TSansgcWSProtocolDDP_Client.TDDPMsgContext;
  var AAcquireContext : boolean);

var
  Handled : boolean;

  function DoCustomDDPHandler(const AID : string; const ANeedRemove : boolean) : boolean;
  var
    Intf : IObjRef;
    CH : TCustomDDPHandler;
  begin
    FIDCustomDDPHandlersCS.Enter;
    try
      Result := FIDCustomDDPHandlersDict.TryGetValue(AID, Intf);
    finally
      FIDCustomDDPHandlersCS.Leave;
    end;

    if not Result then
      Exit;

    CH := Intf.ObjRef as TCustomDDPHandler;

    if Assigned(CH.Handler) then
      CH.Handler(AContext, AAcquireContext, Handled);

    if ANeedRemove then
    begin
      if Assigned(CH.SyncEvent) then
        CH.SyncEvent.SetEvent();

      FIDCustomDDPHandlersCS.Enter;
      try
        FIDCustomDDPHandlersDict.Remove(AID);
      finally
        FIDCustomDDPHandlersCS.Leave;
      end;
    end;
  end;

  function DoIDArrCustomDDPHandler(const AIDs : TStringDynArray; const ANeedRemove : boolean) : boolean;
  var
    ID : string;
  begin
    Result := False;

    for ID in AIDs do
      if DoCustomDDPHandler(ID, ANeedRemove) then
        Exit(True);
  end;

begin
  Handled := False;

  case AContext.MsgKind of
    mkcUpdated:
      if DoIDArrCustomDDPHandler((AContext as TDDPMsgCtx_Updated).Methods, False) and Handled then
        Exit;

    mkcResult:
      if DoCustomDDPHandler((AContext as TDDPMsgCtx_Result).ID, True) and Handled  then
        Exit;

    // AVStantso:
    //   Стоит учитывать, что если для хотябы одного id в сообщении ready
    //   вызовется индивидуальная обработка, то FOnDDPMsg вызван НЕ БУДЕТ
    mkcReady:
      if DoIDArrCustomDDPHandler((AContext as TDDPMsgCtx_Ready).Subs, True) and Handled then
         Exit;

    mkcConnected, mkcRejected:
      if DoCustomDDPHandler(sConnect, True) then
        Exit;
  end;

  if Assigned(FOnDDPMsg) then
    FOnDDPMsg(AContext, AAcquireContext);
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Server.TConnectionDDPData'}
constructor TSansgcWSProtocolDDP_Server.TConnectionDDPData.Create(
  const AServer : TSansgcWSProtocolDDP_Server; const AConnection: TsgcWSConnection);
var
  IdSrvCtx : TIdServerContext;
begin
  inherited Create();

  FServer := AServer;

  FConnection := AConnection;

  FSupportVersions := TStringList.Create;

  FSubscriptionsCS := TCriticalSection.Create;

  FExtDataIsManged := True;

  IdSrvCtx := CurrentIdServerContext;
  FOriginalIdRunDoExecute := IdSrvCtx.OnRun;
  IdSrvCtx.OnRun := IdRunDoExecute;

end;

destructor TSansgcWSProtocolDDP_Server.TConnectionDDPData.Destroy;
begin
  CurrentIdServerContext.OnRun := FOriginalIdRunDoExecute;

  if FExtDataIsManged then
    FreeAndNil(FExtData);

  FreeAndNil(FSupportVersions);
  FreeAndNil(FSubscriptionsCS);
  inherited;
end;

class function TSansgcWSProtocolDDP_Server.TConnectionDDPData.GetCurrentIdServerContext: TIdServerContext;
begin
  Result := (TThread.Current as TIdThreadWithTask).Task as TIdServerContext;
end;

function TSansgcWSProtocolDDP_Server.TConnectionDDPData.IsSubscribed(
  const ACollection: string): boolean;
var
  i : integer;
begin
  SubscriptionsCS.Enter;
  try

    for i := 0 to Connection.Subscriptions.Count - 1 do
      if AnsiSameText(ACollection, Connection.Subscriptions.ValueFromIndex[i]) then
        Exit(True);

  finally
    SubscriptionsCS.Leave;
  end;

  Result := False;
end;

procedure TSansgcWSProtocolDDP_Server.TConnectionDDPData.Added(
  const ACollection, AID: string; const AFields: TJSONObject);
var
  J : TJSONObject;
begin
  J := MakeMsgJSON(sAdded);
  try
    J.AddPair(sCollection, ACollection);
    J.AddPair(sID, AID);

    if Assigned(AFields) then
      J.AddPair(sFields, AFields);

    WriteData(J.ToJSON());

  finally
    FreeAndNil(J);
  end;
end;

procedure TSansgcWSProtocolDDP_Server.TConnectionDDPData.Changed(
  const ACollection, AID: string; const AFields: TJSONObject;
  const ACleared: TStringDynArray);
var
  J : TJSONObject;
  A : TJSONArray;
  S : string;
begin
  J := MakeMsgJSON(sChanged);
  try
    J.AddPair(sCollection, ACollection);
    J.AddPair(sID, AID);

    if Assigned(AFields) then
      J.AddPair(sFields, AFields);

    if Assigned(ACleared) then
    begin
      A := TJSONArray.Create();
      try
        for S in ACleared do
          A.Add(S);

        J.AddPair(sCleared, A);
        A := nil;

      finally
        FreeAndNil(A);
      end;
    end;

    WriteData(J.ToJSON());

  finally
    FreeAndNil(J);
  end;
end;

procedure TSansgcWSProtocolDDP_Server.TConnectionDDPData.Changed(
  const ACollection, AID: string; const ACleared: TStringDynArray);
begin
  Changed(ACollection, AID, nil, ACleared);
end;

procedure TSansgcWSProtocolDDP_Server.TConnectionDDPData.Removed(
  const ACollection, AID: string);
var
  J : TJSONObject;
begin
  J := MakeMsgJSON(sRemoved);
  try
    J.AddPair(sCollection, ACollection);
    J.AddPair(sID, AID);

    WriteData(J.ToJSON());

  finally
    FreeAndNil(J);
  end;
end;

procedure TSansgcWSProtocolDDP_Server.TConnectionDDPData.WriteData(
  const aText: string);
begin
  Connection.WriteData(aText);
  if Assigned(FServer.OnWriteData) then
    FServer.OnWriteData(Connection, aText);
end;

function TSansgcWSProtocolDDP_Server.TConnectionDDPData.IdRunDoExecute(
  AContext: TIdContext): Boolean;
begin
  Result := FOriginalIdRunDoExecute(AContext);

  if not TryPing() then
    Connection.Close;
end;

function TSansgcWSProtocolDDP_Server.TConnectionDDPData.TryPing: boolean;
var
  NeedPing, Expired : TDateTime;
  J : TJSONObject;
begin
  NeedPing := IncMilliSecond(Now(),    -FServer.PingInterval    );
  if Connection.LastPong > NeedPing then
    Exit(True);

  Expired  := IncMilliSecond(NeedPing, -FServer.PingInterval * 2);
  if Connection.LastPong <= Expired then
    Exit(False);

  J := MakeMsgJSON(sPing);
  try
    WriteData(J.ToJSON());
  finally
    FreeAndNil(J);
  end;

  FLastPing := Now();

  Result := True;
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Server.TDDPMsgContext'}
function TSansgcWSProtocolDDP_Server.TDDPMsgContext.GetConnectionDDPData: TConnectionDDPData;
begin
  Result := Connection.Data as TConnectionDDPData;
end;

class function TSansgcWSProtocolDDP_Server.TDDPMsgContext.Msg: string;
var
  MsgKind : TMsgKind;
begin
  MsgKind := Self.MsgKind;

  Assert(MsgKind > mksUnknown);

  Result := KNOWN_MSGs[MsgKind];
end;

class function TSansgcWSProtocolDDP_Server.TDDPMsgContext.MsgKind: TMsgKind;
begin
  raise ENotImplemented.CreateFmt('%s.%s.%s', [UnitName, ClassName, 'MsgKind']);
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect'}
class function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect.MsgKind: TMsgKind;
begin
  Result := mksConnect;
end;

function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect.GetSupportVersions: TStrings;
begin
  Result := Sender.ConnectionDDPData[Connection].SupportVersions;
end;

function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect.GetVersion: string;
begin
  Result := Sender.ConnectionDDPData[Connection].Version;
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect.Init;
var
  V : TJSONString;
  S : TJSONArray;
  D : TSansgcWSProtocolDDP_Server.TConnectionDDPData;
  i : integer;
begin
  TryGetValue(sVersion, V);
  TryGetValue(sSupport, S);
  ValidateKeys();

  D := Sender.ConnectionDDPData[Connection];
  D.FVersion := V.Value;

  D.FSupportVersions.BeginUpdate();
  try

    for i := 0 to S.Count - 1 do
      D.FSupportVersions.Add(S.Items[i].Value);

  finally
    D.FSupportVersions.EndUpdate();
  end;

end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect.Choice(
  const AState: TState; const AValue: string);
var
  J : TJSONObject;
begin
  Assert(AState in [sttAccepted, sttRejected], 'Invalid state' + IntToStr(Ord(AState)));

  if FState <> sttNotChosen then
    raise ESansgcWSProtocolDDP.CreateRes(@sConnectedAcceptOrRejectSessionAlreadyCalled);

  FState := AState;

  J := MakeMsgJSON( IfThen(AState = sttAccepted, sConnected, sRejected) );
  try

    J.AddPair( IfThen(AState = sttAccepted, sSession, sError), AValue);

    WriteData(J.ToJSON());

  finally
    FreeAndNil(J);
  end;

  if FState = sttRejected then
    Connection.Close();
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect.AcceptSession(
  const ASessionID: string);
begin
  Choice(sttAccepted, ASessionID);
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect.RejectSession(
  const AError: string);
begin
  Choice(sttRejected, AError);
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Pong'}
class function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Pong.MsgKind: TMsgKind;
begin
  Result := mksPong;
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Server.TDDPMsgCtx_CustomID'}
function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_CustomID.GetID: string;
begin
  Result := FID.Value;
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_CustomID.Init;
begin
  TryGetValue(sID, FID);
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Server.TDDPMsgCtx_CustomIDParams'}
procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_CustomIDParams.Init;
begin
  inherited;
  RawData.TryGetValue(sParams, FParams);
end;

function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_CustomIDParams.AcquireParams: TJSONArray;
begin
  if not Assigned(FParams) then
    Exit(nil);

  Result := FParams;
  FParams.Owned := False;
  FParams := nil;
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method'}
class function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method.MsgKind: TMsgKind;
begin
  Result := mksMethod;
end;

function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method.GetMethod: string;
begin
  Result := FMethod.Value;
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method.Init;
begin
  inherited;
  TryGetValue(sMethod, FMethod);
  ValidateKeys();
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method.Updated;
var
  J : TJSONObject;
  A : TJSONArray;
begin
  if FUpdated then
    raise ESansgcWSProtocolDDP.CreateResFmt(@sMethodXXXAlreadyCalledFmt, [sUpdated]);

  FUpdated := True;

  J := MakeMsgJSON(sUpdated);
  try

    A := TJSONArray.Create;
    try
      A.Add(ID);
      J.AddPair(sMethods, A);
      A := nil;

    finally
      FreeAndNil(A);
    end;

    WriteData(J.ToJSON());

  finally
    FreeAndNil(J);
  end;
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method.Result(
  const AResult: TJSONObject);
var
  J : TJSONObject;
begin
  if FResult then
    raise ESansgcWSProtocolDDP.CreateResFmt(@sMethodXXXAlreadyCalledFmt, [sResult]);

  FResult := True;

  J := MakeMsgJSON(sResult);
  try
    J.AddPair(sId, ID);

    if Assigned(AResult) then
      J.AddPair(sResult, AResult);

    WriteData(J.ToJSON());

  finally
    FreeAndNil(J);
  end;

end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method.Result(
  const AError: string);
var
  J : TJSONObject;
begin
  if FResult then
    raise ESansgcWSProtocolDDP.CreateResFmt(@sMethodXXXAlreadyCalledFmt, [sResult]);

  FResult := True;

  J := MakeMsgJSON(sResult);
  try
    J.AddPair(sID, ID);
    J.AddPair(sError, AError);

    WriteData(J.ToJSON());

  finally
    FreeAndNil(J);
  end;

end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub'}
class function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub.MsgKind: TMsgKind;
begin
  Result := mksSub;
end;

function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub.GetName: string;
begin
  Result := FName.Value;
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub.Init;
begin
  inherited;
  TryGetValue(sName, FName);
  ValidateKeys();
end;

function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub.IsIDSubscribed: boolean;
begin
  ConnectionDDPData.SubscriptionsCS.Enter;
  try

    Result := Connection.Subscriptions.IndexOfName(ID) > -1;

  finally
    ConnectionDDPData.SubscriptionsCS.Leave;
  end;
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub.Ready;
var
  J : TJSONObject;
  A : TJSONArray;
begin
  if FReadyOrResult then
    raise ESansgcWSProtocolDDP.CreateResFmt(@sSubXXXorYYYAlreadyCalledFmt, [sReady, sResult]);

  FReadyOrResult := True;


  ConnectionDDPData.SubscriptionsCS.Enter;
  try

    Connection.Subscriptions.AddPair(ID, Name);

  finally
    ConnectionDDPData.SubscriptionsCS.Leave;
  end;


  J := MakeMsgJSON(sReady);
  try

    A := TJSONArray.Create;
    try
      A.Add(ID);
      J.AddPair(sSubs, A);
      A := nil;

    finally
      FreeAndNil(A);
    end;

    WriteData(J.ToJSON());

  finally
    FreeAndNil(J);
  end;

end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub.Result(
  const AError: string);
var
  J : TJSONObject;
begin
  if FReadyOrResult then
    raise ESansgcWSProtocolDDP.CreateResFmt(@sSubXXXorYYYAlreadyCalledFmt, [sReady, sResult]);

  FReadyOrResult := True;

  J := MakeMsgJSON(sResult);
  try
    J.AddPair(sID, ID);
    J.AddPair(sError, AError);

    WriteData(J.ToJSON());

  finally
    FreeAndNil(J);
  end;
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Unsub'}
class function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Unsub.MsgKind: TMsgKind;
begin
  Result := mksUnsub;
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Unsub.Init;
begin
  inherited;
  ValidateKeys();
end;

function TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Unsub.IsIDSubscribed: boolean;
begin
  ConnectionDDPData.SubscriptionsCS.Enter;
  try

    Result := Connection.Subscriptions.IndexOfName(ID) > -1;

  finally
    ConnectionDDPData.SubscriptionsCS.Leave;
  end;
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Unsub.Result();
var
  J : TJSONObject;
begin
  if FResult then
    raise ESansgcWSProtocolDDP.CreateResFmt(@sUnsubXXXAlreadyCalledFmt, [sResult]);

  FResult := True;


  ConnectionDDPData.SubscriptionsCS.Enter;
  try

    Connection.Subscriptions.Delete(Connection.Subscriptions.IndexOfName(Id));

  finally
    ConnectionDDPData.SubscriptionsCS.Leave;
  end;


  J := MakeMsgJSON(sResult);
  try
    J.AddPair(sID, ID);

    WriteData(J.ToJSON());

  finally
    FreeAndNil(J);
  end;
end;

procedure TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Unsub.Result(
  const AError: string);
var
  J : TJSONObject;
begin
  if FResult then
    raise ESansgcWSProtocolDDP.CreateResFmt(@sUnsubXXXAlreadyCalledFmt, [sResult]);

  FResult := True;

  J := MakeMsgJSON(sResult);
  try
    J.AddPair(sID, ID);
    J.AddPair(sError, AError);

    WriteData(J.ToJSON());

  finally
    FreeAndNil(J);
  end;
end;
{$ENDREGION}

{$REGION 'TSansgcWSProtocolDDP_Server'}
function TSansgcWSProtocolDDP_Server.GetConnectionDDPData(
  const aConnection: TsgcWSConnection): TConnectionDDPData;
begin
  Assert(Assigned(aConnection), 'aConnection is nil');
  Result := aConnection.Data as TConnectionDDPData;
end;

constructor TSansgcWSProtocolDDP_Server.Create(AOwner: TComponent);
begin
  inherited;

  FProtocol := sDDP;
  FPingInterval := DDP_DEFAULT_PING_INTERVAL;
end;

procedure TSansgcWSProtocolDDP_Server.BeforeDestruction;
begin
  if Assigned(Server) then
    EnumConnections(
      procedure (AConnection : TsgcWSConnection)
      begin
        AConnection.Data.Free();
        AConnection.Data := nil;
      end
    );

  inherited;
end;

destructor TSansgcWSProtocolDDP_Server.Destroy;
begin

  inherited;
end;

procedure TSansgcWSProtocolDDP_Server.EnumConnections(
  const AEnumAction: TProc<TsgcWSConnection>; const ADirection : System.Types.TDirection = FromEnd);
var
  List: TList{$IFDEF NEXTGEN}<TIdContext>{$ENDIF};

  function TryGetConnection(const AIndex : integer; out AConnection : TsgcWSConnection) : boolean;
  begin
    AConnection := TsgcWSConnection(TIdContext(List[AIndex]).{$IFDEF NEXTGEN}DataObject{$ELSE}Data{$ENDIF});
    Result := Assigned(AConnection);
  end;

var
  i : integer;
  C : TsgcWSConnection;
begin
  List := Server.LockList;
  try

    case ADirection of
      FromBeginning:
        for i := 0 to List.Count - 1 do
          if TryGetConnection(i, C) then
            AEnumAction(C);

      FromEnd:
        for i := List.Count - 1 downto 0 do
          if TryGetConnection(i, C) then
            AEnumAction(C);
    end;

  finally
    Server.UnlockList;
  end;

end;

procedure TSansgcWSProtocolDDP_Server.EnumConnectionsDDPData(
  const AEnumAction: TProc<TConnectionDDPData>;
  const ADirection: System.Types.TDirection);
begin
  EnumConnections(
    procedure (AConnection : TsgcWSConnection)
    begin
      if Assigned(AConnection.Data) then
        AEnumAction(AConnection.Data as TConnectionDDPData);
    end,
    ADirection
  );
end;

procedure TSansgcWSProtocolDDP_Server.DoEventGetDynServerID(
  aConnection: TsgcWSConnection; var AServerID: string);
begin
  if Assigned(FOnGetDynServerID) then
    FOnGetDynServerID(Self, aConnection, AServerID);
end;

procedure TSansgcWSProtocolDDP_Server.DoEventConnect(
  aConnection: TsgcWSConnection);
var
  ServerID : string;
  J : TJSONObject;
  DDPData : TConnectionDDPData;
begin
  try
    aConnection.LastPong := Now();

    ServerID := FServerID;
    DoEventGetDynServerID(aConnection, ServerID);
    if ServerID = '' then
      raise ESansgcWSProtocolDDP.CreateRes(@sServerIDMustBeAssigned);

    DDPData := TConnectionDDPData.Create(Self, aConnection);
    aConnection.Data := DDPData;

    J := TJSONObject.Create();
    try
      J.AddPair(sServerID, ServerID);
      DDPData.WriteData(J.ToJSON());
    finally
      FreeAndNil(J);
    end;

    inherited;

  except
    on E : Exception do
    begin
      if Assigned(OnException) then
        OnException(aConnection, E);

      if FPolite then
      begin
        J := MakeMsgJSON(sError);
        try
          J.AddPair(sError, E.ToString);
          aConnection.WriteData(J.ToJSON());

          if Assigned(OnWriteData) then
            OnWriteData(aConnection, J.ToJSON());

        finally
          FreeAndNil(J);
        end;
      end;

    end;
  end;

end;

procedure TSansgcWSProtocolDDP_Server.DoEventDisconnect(
  aConnection: TsgcWSConnection; Code: Integer);
begin
  inherited;
  aConnection.Data.Free;
  aConnection.Data := nil;
end;

procedure TSansgcWSProtocolDDP_Server.DoEventMessage(
  aConnection: TsgcWSConnection; const Text: string);
var
  J : TJSONObject;
  AcquireContext : boolean;
  C : TDDPMsgContext;
begin
  try

    try
      J := JSONFromMessage(Text);
      try
        AcquireContext := False;

        TDDPMsgContext.Make(Self, aConnection, J, C);
        try
          // Нет смысла слать лишние пинги, общение и так идет
          aConnection.LastPong := Now();

          DoEventDDPMsg(C, AcquireContext);
        finally
          if not AcquireContext then
            FreeAndNil(C);
        end;

      finally
        FreeAndNil(J);
      end;

      inherited;

    except
      Exception.RaiseOuterException( ESansgcWSProtocolDDP.CreateResFmt(@sInternalErrorForMessageXXXFmt, [Text]));
    end;

  except
    on E : Exception do
    begin
      if Assigned(OnException) then
        OnException(aConnection, E);

      if FPolite then
      begin
        J := MakeMsgJSON(sError);
        try
          J.AddPair(sError, E.ToString);
          aConnection.WriteData(J.ToJSON());

          if Assigned(OnWriteData) then
            OnWriteData(aConnection, J.ToJSON());

        finally
          FreeAndNil(J);
        end;
      end;

    end;
  end;

end;

procedure TSansgcWSProtocolDDP_Server.DoEventSubThreadError(AThread : TThread;
  AError : Exception);
begin
  if Assigned(FOnThreadError) then
    FOnThreadError(Self, AThread, AError);
end;

procedure TSansgcWSProtocolDDP_Server.DoEventDDPMsg(AContext : TSansgcWSProtocolDDP_Server.TDDPMsgContext;
  var AAcquireContext : boolean);
begin
  if Assigned(FOnDDPMsg) then
    FOnDDPMsg(AContext, AAcquireContext);
end;

procedure TSansgcWSProtocolDDP_Server.DoWriteData(aConnection: TsgcWSConnection;
  const aText: String);
begin
  aConnection.WriteData(aText);

  if Assigned(FOnWriteData) then
    FOnWriteData(aConnection, aText);
end;
{$ENDREGION}





initialization
  TSansgcWSProtocolDDP_Client.TDDPMsgContext.FMsgClasses := TSansgcWSProtocolDDP_DDPMsgCustomContext.TMsgClassesDict.Create(Succ(Ord(High(TSansgcWSProtocolDDP_Client.TMsgKind))));
  TSansgcWSProtocolDDP_Server.TDDPMsgContext.FMsgClasses := TSansgcWSProtocolDDP_DDPMsgCustomContext.TMsgClassesDict.Create(Succ(Ord(High(TSansgcWSProtocolDDP_Server.TMsgKind))));

  TSansgcWSProtocolDDP_DDPMsgCustomContext.RegClasses
  (
    [
      {$REGION 'TSansgcWSProtocolDDP_Client'}
      TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Connected,
      TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Rejected,
      TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Ping,
      TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Updated,
      TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Result,
      TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Ready,
      TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Added,
      TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Changed,
      TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Removed,
      TSansgcWSProtocolDDP_Client.TDDPMsgCtx_Error,
      {$ENDREGION}

      {$REGION 'TSansgcWSProtocolDDP_Server'}
      TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Connect,
      TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Pong,
      TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Method,
      TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Sub,
      TSansgcWSProtocolDDP_Server.TDDPMsgCtx_Unsub
      {$ENDREGION}
    ]
  );

finalization
  FreeAndNil(TSansgcWSProtocolDDP_Client.TDDPMsgContext.FMsgClasses);
  FreeAndNil(TSansgcWSProtocolDDP_Server.TDDPMsgContext.FMsgClasses);

end{$WARNINGS OFF}.
{$ENDIF}
