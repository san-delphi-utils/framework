SET UP_PATH=..
SET BIN_PATH=%~1\%UP_PATH%\build\bin\%3\%4
SET RES_PATH=%~1\%UP_PATH%\build\res\%3\%4
SET LOG_PATH=%~1\%UP_PATH%\build\log\%3\%4
SET RES_SRC_PATH=%~1\%UP_PATH%\res
SET CFG_PATH=%~1\%UP_PATH%\config
SET CMD_PATH=%~1\%UP_PATH%\cmd
SET EXTERNAL_BIN_PATH=%~1\%UP_PATH%\externalBin

IF NOT EXIST "%LOG_PATH%" (mkdir "%LOG_PATH%") else (del %LOG_PATH%\%2.%5Build.log)

IF EXIST "Common.%5Build.cmd" (call Common.%5Build.cmd %1 %2 %3 %4 >> %LOG_PATH%\%2.%5Build.log)

call %2.%5Build.cmd %1 %2 %3 %4 >> %LOG_PATH%\%2.%5Build.log