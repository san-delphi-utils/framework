{*******************************************************}
{                                                       }
{       Список обработчиков событий.                    }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2014-2016         Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   Список обработчиков событий.
/// </summary>
unit San.Events.Handlers.Generic;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  System.SysUtils, System.Classes, System.Math;
{$ENDREGION}

type
  /// <summary>
  ///   Элемент списка обработчиков.
  /// </summary>
  THandlerItem<T> = packed record
  strict private
    function GetFixedPosition: integer;
    function GetGenericPosition: integer;

  private
    FHandler: T;
    FPosition : Int64;

  public
    /// <summary>
    ///   Данные обработчика.
    /// </summary>
    property Handler: T  read FHandler;

    /// <summary>
    ///   Позиция, признак очередности.
    /// </summary>
    property Position : Int64  read FPosition;

    /// <summary>
    ///   Задаваемая часть позиции.
    /// </summary>
    property FixedPosition    : integer  read GetFixedPosition;

    /// <summary>
    ///   Автогенерируемая часть позиции.
    /// </summary>
    property GenericPosition  : integer  read GetGenericPosition;

  end;

  /// <summary>
  ///   Список обработчиков.
  /// </summary>
  /// <remarks>
  ///   Необходимо переопределение IsEmptyItem и IsEqualsItems.
  /// </remarks>
  THandlers<T> = class abstract(TObject)
  strict private
    FSorted: boolean;
    FCount: integer;
    FCapacity: integer;
    FData: array of THandlerItem<T>;
    FGenericCounter : integer;

    procedure SetSorted(AValue: boolean);
    procedure SetCapacity(value: integer);
    function GetValue(const AKey: Int64): T;
    procedure SetValue(const AKey: Int64; const Value: T);
    function getKeyIndex(const AKey: Int64): integer;
    procedure qSort(var aData: array of THandlerItem<T>; left, right: integer);

  protected

  public
    class function IsEmptyItem(const AItem : T) : boolean; virtual;
    class function IsEqualsItems(const AItem1, AItem2 : T) : boolean; virtual;

    constructor Create(AInitialCapacity: integer = 10);
    destructor Destroy; override;
    function Get(index: integer): THandlerItem<T>;
    function Exists(const AKey: Int64): boolean;

    procedure Add(const AFixedPosition: integer; const AHandler: T);
    procedure DeleteByIndex(const AIndex: integer);
    procedure Delete(const AKey: Int64);
    function IndexOf(const AHandler: T) : integer;
    procedure Remove(const AHandler: T);
    procedure Clear();
    procedure Compress();


    property IsSorted: boolean read FSorted write SetSorted;
    property Count: integer read FCount;
    property Capacity: integer read FCapacity;
    property Items[const AKey: Int64]: T read GetValue write SetValue; default;
  end;

implementation

{$REGION 'THandlerItem<T>'}
function THandlerItem<T>.GetFixedPosition: integer;
begin
  Result := Position div (Int64(High(Cardinal)) + 1);
end;

function THandlerItem<T>.GetGenericPosition: integer;
begin
  Result := Position mod (Int64(High(Cardinal)) + 1);
end;
{$ENDREGION}

{$REGION 'THandlers<T>'}
constructor THandlers<T>.Create(AInitialCapacity: integer);
begin
  FSorted := True;
  FCapacity := 0;
  FCount := 0;
  SetCapacity(AInitialCapacity);
end;

destructor THandlers<T>.Destroy;
begin
  FCount := 0;
  FCapacity := 0;
  SetLength(FData, 0);
  FData := nil;
  inherited;
end;

procedure THandlers<T>.SetCapacity(value: integer);
begin
  if FCapacity < value then
  begin
    FCapacity := value * 2;
    SetLength(FData, FCapacity);
  end;
end;

procedure THandlers<T>.SetSorted(AValue: boolean);
begin
  FSorted := AValue;
  if FSorted then
    qSort(FData, 0, FCount - 1);
end;

procedure THandlers<T>.Add(const AFixedPosition: integer; const AHandler: T);
var
  Position : Int64;
  PositionGeneric, index: integer;
begin
  PositionGeneric := AtomicIncrement(FGenericCounter);

  Position := AFixedPosition*(Int64(High(Cardinal)) + 1) + IfThen(AFixedPosition < 0, -1, 1)*PositionGeneric;

  index := getKeyIndex(Position);
  if index < 0 then
  begin
    Inc(FCount);
    SetCapacity(FCount);
    FData[FCount - 1].FPosition := Position;
    FData[FCount - 1].FHandler := AHandler;

    if IsSorted then
      qSort(FData, 0, FCount - 1);
  end;
end;

procedure THandlers<T>.DeleteByIndex(const AIndex: integer);
var
  i : integer;
begin

  // копируем со смещением
  for i := AIndex + 1 to FCount - 1 do
    FData[i - 1] := FData[i];

  // чистим последний элемент
  FData[FCount - 1].FPosition := 0;
  FData[FCount - 1].FHandler := T(nil);

  // уменьшаем сам массив
  Dec(FCount);
end;

procedure THandlers<T>.Delete(const AKey: Int64);
var
  index: integer;
begin
  index := getKeyIndex(AKey);
  if index >= 0 then
    DeleteByIndex(index);
end;

function THandlers<T>.IndexOf(const AHandler: T): integer;
var
  i : integer;
begin
  for i := 0 to FCount - 1 do
    if IsEqualsItems(FData[i].Handler, AHandler) then
      Exit(i);

  Result := -1;
end;

procedure THandlers<T>.Remove(const AHandler: T);
var
  Idx : integer;
begin
  //TODO: Подумать над убыстрением

  Idx := IndexOf(AHandler);
  if Idx >= 0 then
    DeleteByIndex(Idx);
end;

procedure THandlers<T>.Clear;
begin
  FCount := 0;
end;

procedure THandlers<T>.Compress;
var
  i, shift: integer;
begin
  shift := 0;

  // смещаем
  for i := 0 to FCount - 1 do
    if IsEmptyItem(FData[i].Handler) then
      shift := shift + 1

    else
    if shift > 0 then
      FData[i - shift] := FData[i];

  FCount := FCount - shift;

  // упаковываем
  for i := FCount to High(FData) do
  begin
    FData[i].FPosition := 0;
    FData[i].FHandler    := T(nil);
  end;

end;

function THandlers<T>.Get(index: integer): THandlerItem<T>;
begin
  if (index < 0) or (index >= Length(FData)) then
  begin
    Result.FPosition := 0;
    Result.FHandler := T(nil);
  end
  else
    Result := FData[index];
end;

function THandlers<T>.GetValue(const AKey: Int64): T;
var
  index: integer;
begin
  index := getKeyIndex(AKey);
  if index >= 0 then
    Result := FData[index].Handler
  else
    Result := T(nil);
end;

class function THandlers<T>.IsEmptyItem(const AItem: T): boolean;
begin
  Result := False;
end;

class function THandlers<T>.IsEqualsItems(const AItem1, AItem2: T): boolean;
begin
  Result := False;
end;

procedure THandlers<T>.SetValue(const AKey: Int64; const Value: T);
var
  index: integer;
begin
  index := getKeyIndex(AKey);
  if index >= 0 then
    FData[index].FHandler := Value
  else
    Add(AKey, Value);
end;

function THandlers<T>.Exists(const AKey: Int64): boolean;
var
  index: integer;
begin
  index := getKeyIndex(AKey);
  Result := (index >= 0);
end;

function THandlers<T>.getKeyIndex(const AKey: Int64): integer;
var
  left, right, center: integer;
  k, i: integer;
  x: Int64;
begin

  if IsSorted then
  begin
    // передаем входные параметры
    left := 0;
    right := FCount - 1;
    x := AKey;

    // проверяем вхождение в диапазон
    if right < left then
      k := -1

    else
    begin
      if (x < FData[left].Position)or(x > FData[right].Position) then
        k := -1

      else
      begin

        // сужаем диапазон поиска до одного индекса
        while((right - left) > 1)do
        begin
          center := (left + right) div 2;
          if x < FData[center].Position then
            right := center
          else
            left := center;
        end;

        // проверяем что осталось
        if x = FData[left].Position then
          k := left

        else
        if x = FData[right].Position then
          k := right

        else
          k := -1;
      end;
    end;
  end

  else
  begin
    k := -1;
    for i := 0 to FCount - 1 do
      if FData[i].Position = AKey then
      begin
        k := i;
        break;
      end;
  end;

  Result := k;
end;

procedure THandlers<T>.qSort(var aData: array of THandlerItem<T>; left, right: integer);
var
  i, j: integer;
  center, x: THandlerItem<T>;
begin
  i := left;
	j := right;
	center := aData[(left + right) div 2];
	while(i <= j)do
  begin
		while ((aData[i].position < center.position) and (i < right)) do
			inc(i);

		while ((aData[j].position > center.position) and (j > left)) do
			dec(j);

		if (i <= j) then
    begin
			x := aData[i];
			aData[i] := aData[j];
			aData[j] := x;
			//
			inc(i);
			dec(j);
    end;
  end;

	if (left < j) then
		qSort(aData, left, j);

	if (right > i) then
		qSort(aData, i, right);
end;
{$ENDREGION}

end.
