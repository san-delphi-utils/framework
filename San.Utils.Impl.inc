{*******************************************************}
{                                                       }
{       Простые функции.                                }
{       Перегрузка стандартных функций.                 }
{       Реализация.                                     }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2017              Станцо А.В.     }
{                                                       }
{*******************************************************}
function Format(const AFormat: PResStringRec; const AArgs: array of const): string;
begin
  Result := System.SysUtils.Format( LoadResString(AFormat), AArgs);
end;

procedure Split(const AString, ASeparator: string; const AOnSplit : TStrProc);
var
  Offset, p, L : integer;
begin
  L := Length(ASeparator);

  Offset := 1;
  repeat

    p := Pos(ASeparator, AString, Offset);
    if P < 1 then
      Break;

    AOnSplit( Copy(AString, Offset, p - Offset) );

    Offset := p + L;
  until False;

  if Length(AString) >= Offset then
    AOnSplit( Copy(AString, Offset) );
end;

function Max(const AArgs : array of integer) : integer;
var
  i : integer;
begin
  if Length(AArgs) = 0 then
    raise ENotSupportedException.Create('Max([])');

  Result := AArgs[0];
  for i := 1 to High(AArgs) do
    if Result < AArgs[i] then
      Result := AArgs[i];
end;

function IIF(const ACondition : boolean; const ATrue, AFalse : Variant) : Variant;
begin
  if ACondition then
    Result := ATrue
  else
    Result := AFalse;
end;

function IIF(const ACondition : boolean; const ATrue : Variant) : Variant;
begin
  if ACondition then
    Result := ATrue
  else
    Result := Null;
end;

function NOA(const ACondition : boolean; const AExtCondition : boolean) : boolean;
begin
  Result := not ACondition or ACondition and AExtCondition;
end;
