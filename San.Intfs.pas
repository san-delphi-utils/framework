{*******************************************************}
{                                                       }
{       Работа с интерфейсами.                          }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2013-2016         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Работа с интерфейсами.
/// </summary>
unit San.Intfs;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  System.SysUtils, System.Generics.Collections, System.TypInfo, System.Rtti,

  San.Intfs.Basic, San.Intfs.Holders;
{$ENDREGION}
{$ENDIF}

type
  /// <summary>
  ///   Исключение интерфейсов.
  /// </summary>
  ESanIntfs =
  {$IFDEF USE_ALIASES}
    San.Intfs.ESanIntfs;
  {$ELSE}
    class (Exception);
  {$ENDIF}

{$IFNDEF USE_ALIASES}
type
  TGUIDIntfsRTTITypes = TDictionary<TGUID, TRttiInterfaceType>;

  /// <summary>
  ///   Доступ к методам работы с интерфейсами.
  /// </summary>
  Api = record
  private
    class var FHolder: IInterface;
    class procedure Initialize(); static;
    class procedure Finalize(); static;


  strict private
    class var FIntfsRTTITypesCache : TGUIDIntfsRTTITypes;
    class procedure MustSupportsError(const AArgTypeRS : PResStringRec; const AClassText : string; const AIID: TGUID); static;

  public
    /// <summary>
    ///   Получение интерфейса с исключением при неудаче.
    /// </summary>
    /// <param name="AInstance">
    ///   У кого запрашиваем.
    /// </param>
    /// <param name="AIID">
    ///   Что запрашиваем.
    /// </param>
    /// <param name="AIntf">
    ///   Результат.
    /// </param>
    /// <exception cref="ESanIntfs">
    ///   При неудаче.
    /// </exception>
    /// <returns>
    ///   Всегда Truе. Для возможности участия в булевских вычислениях.
    /// </returns>
    class function MustSupports(const AInstance: IInterface; const AIID: TGUID; out AIntf) : boolean; overload; static;

    /// <summary>
    ///   Получение интерфейса с исключением при неудаче.
    /// </summary>
    /// <param name="AInstance">
    ///   У кого запрашиваем.
    /// </param>
    /// <param name="AIID">
    ///   Что запрашиваем.
    /// </param>
    /// <param name="AIntf">
    ///   Результат.
    /// </param>
    /// <exception cref="ESanIntfs">
    ///   При неудаче.
    /// </exception>
    /// <returns>
    ///   Всегда Truе. Для возможности участия в булевских вычислениях.
    /// </returns>
    class function MustSupports(const AInstance: TObject; const AIID: TGUID; out AIntf) : boolean; overload; static;

    /// <summary>
    ///   Получение интерфейса с исключением при неудаче.
    /// </summary>
    /// <param name="AInstance">
    ///   У кого запрашиваем.
    /// </param>
    /// <param name="AIID">
    ///   Что запрашиваем.
    /// </param>
    /// <exception cref="ESanIntfs">
    ///   При неудаче.
    /// </exception>
    /// <returns>
    ///   Всегда Truе. Для возможности участия в булевских вычислениях.
    /// </returns>
    class function MustSupports(const AClass: TClass; const AIID: TGUID) : boolean; overload; static;

    /// <summary>
    ///   Получение интерфейса с исключением при неудаче.
    /// </summary>
    /// <param name="TIntf">
    ///   Что запрашиваем.
    /// </param>
    /// <param name="AInstance">
    ///   У кого запрашиваем.
    /// </param>
    /// <param name="AIntf">
    ///   Результат.
    /// </param>
    /// <exception cref="ESanIntfs">
    ///   При неудаче.
    /// </exception>
    /// <returns>
    ///   Всегда Truе. Для возможности участия в булевских вычислениях.
    /// </returns>
    class function MustSupports<TIntf : IInterface>(const AInstance: IInterface; out AIntf : TIntf) : boolean; overload; static;

    /// <summary>
    ///   Получение интерфейса с исключением при неудаче.
    /// </summary>
    /// <param name="TIntf">
    ///   Что запрашиваем.
    /// </param>
    /// <param name="AInstance">
    ///   У кого запрашиваем.
    /// </param>
    /// <param name="AIntf">
    ///   Результат.
    /// </param>
    /// <exception cref="ESanIntfs">
    ///   При неудаче.
    /// </exception>
    /// <returns>
    ///   Всегда Truе. Для возможности участия в булевских вычислениях.
    /// </returns>
    class function MustSupports<TIntf : IInterface>(const AInstance: TObject; out AIntf : TIntf) : boolean; overload; static;

    /// <summary>
    ///   Получение интерфейса с исключением при неудаче.
    /// </summary>
    /// <param name="TIntf">
    ///   Что запрашиваем.
    /// </param>
    /// <param name="AInstance">
    ///   У кого запрашиваем.
    /// </param>
    /// <exception cref="ESanIntfs">
    ///   При неудаче.
    /// </exception>
    /// <returns>
    ///   Всегда Truе. Для возможности участия в булевских вычислениях.
    /// </returns>
    class function MustSupports<TIntf : IInterface>(const AClass: TClass) : boolean; overload; static;

    /// <summary>
    ///   Получение интерфейса с исключением при неудаче.
    /// </summary>
    /// <param name="TIntf">
    ///   Что запрашиваем.
    /// </param>
    /// <param name="AInstance">
    ///   У кого запрашиваем.
    /// </param>
    /// <exception cref="ESanIntfs">
    ///   При неудаче.
    /// </exception>
    /// <returns>
    ///   Интерфейс типа <c>TIntf</c>.
    /// </returns>
    class function Cast<TIntf : IInterface>(const AInstance: IInterface) : TIntf; overload; static;

    /// <summary>
    ///   Получение интерфейса с исключением при неудаче.
    /// </summary>
    /// <param name="TIntf">
    ///   Что запрашиваем.
    /// </param>
    /// <param name="AInstance">
    ///   У кого запрашиваем.
    /// </param>
    /// <exception cref="ESanIntfs">
    ///   При неудаче.
    /// </exception>
    /// <returns>
    ///   Интерфейс типа <c>TIntf</c>.
    /// </returns>
    class function Cast<TIntf : IInterface>(const AInstance: TObject) : TIntf; overload; static;

    /// <summary>
    ///   Получение типа интерфейса по его IID.
    /// </summary>
    /// <param name="AIID">
    ///   IID интерфейса.
    /// </param>
    /// <returns>
    ///   RTTI-объект типа интерфейса.
    /// </returns>
    class function TypeByIID(const AIID: TGUID) : TRttiInterfaceType; static;

    /// <summary>
    ///   Преобразовать интерфейс в объект.
    /// </summary>
    /// <param name="AIntf">
    ///   Интерфейса.
    /// </param>
    /// <param name="AObj">
    ///   Объект.
    /// </param>
    class procedure IntfToObj(AIntf : IInterface; out AObj); overload; static;

    /// <summary>
    ///   Преобразовать интерфейс в объект.
    /// </summary>
    /// <param name="TObj">
    ///   Тип объекта.
    /// </param>
    /// <param name="AIntf">
    ///   Интерфейс.
    /// </param>
    /// <returns>
    ///   Объект.
    /// </returns>
    class function IntfToObj<TObj : class>(AIntf : IInterface) : TObj; overload; static;

    /// <summary>
    ///   Сравнение интерфейсов через IObjRef.
    /// </summary>
    /// <param name="AIntf1">
    ///   Интерфейс.
    /// </param>
    /// <param name="AIntf2">
    ///   Интерфейс.
    /// </param>
    /// <returns>
    ///   True - реализации совпадают.
    /// </returns>
    class function IntfCompareAsObj(AIntf1, AIntf2 : IObjRef) : boolean; overload; static;

    /// <summary>
    ///   Сравнение интерфейсов через IObjRef.
    /// </summary>
    /// <param name="AIntf1">
    ///   Интерфейс.
    /// </param>
    /// <param name="AIntf2">
    ///   Интерфейс.
    /// </param>
    /// <returns>
    ///   True - реализации совпадают.
    /// </returns>
    class function IntfCompareAsObj(AIntf1 : IInterface; AIntf2 : IObjRef) : boolean; overload; static;

    /// <summary>
    ///   Сравнение интерфейсов через IObjRef.
    /// </summary>
    /// <param name="AIntf1">
    ///   Интерфейс.
    /// </param>
    /// <param name="AIntf2">
    ///   Интерфейс.
    /// </param>
    /// <returns>
    ///   True - реализации совпадают.
    /// </returns>
    class function IntfCompareAsObj(AIntf1 : IObjRef; AIntf2 : IInterface) : boolean; overload; static;

    /// <summary>
    ///   Сравнение интерфейсов через IObjRef.
    /// </summary>
    /// <param name="AIntf1">
    ///   Интерфейс.
    /// </param>
    /// <param name="AIntf2">
    ///   Интерфейс.
    /// </param>
    /// <returns>
    ///   True - реализации совпадают.
    /// </returns>
    class function IntfCompareAsObj(AIntf1, AIntf2 : IInterface) : boolean; overload; static;

    /// <summary>
    ///   Создать интерфейсную ссылку, которая при разрушении, очистит переданные объекты.
    /// </summary>
    /// <param name="AObjs">
    ///   Объекты.
    /// </param>
    /// <returns>
    ///   Ссылка на интерфейс держателя.
    /// </returns>
    class function Hold(const AObjs : array of TObject) : IInterface; overload; static;

    /// <summary>
    ///   Создать интерфейсную ссылку, которая при разрушении, очистит переданный объект.
    /// </summary>
    /// <param name="AObj">
    ///   Объект.
    /// </param>
    /// <returns>
    ///   Ссылка на интерфейс держателя.
    /// </returns>
    class function Hold(const AObj : TObject) : IInterface; overload; static;

    /// <summary>
    ///   Создать интерфейсную ссылку, которая при разрушении, вызовит ссылку на процедуру.
    /// </summary>
    /// <param name="AProc">
    ///   Ссылка на процедуру.
    /// </param>
    /// <returns>
    ///   Ссылка на интерфейс держателя.
    /// </returns>
    class function Hold(const AProc : TProc) : IInterface; overload; static;

    /// <summary>
    ///   Пустой интерфейс. Addref.
    /// </summary>
    class function NopAddref(inst: Pointer): Integer; stdcall; static;
    /// <summary>
    ///   Пустой интерфейс. Release.
    /// </summary>
    class function NopRelease(inst: Pointer): Integer; stdcall; static;
    /// <summary>
    ///   Пустой интерфейс. QueryInterface.
    /// </summary>
    class function NopQueryInterface(inst: Pointer; const IID: TGUID; out Obj): HResult; stdcall; static;

    /// <summary>
    ///   Функция создает интерфейс, который ничего не делает.
    /// </summary>
    /// <returns>
    ///   Интерфейс IInterface.
    /// </returns>
    /// <remarks>
    ///   Хотя этот интерфейс ничего не делает, переменная интерфейсного типа автоматически
    ///   очищается при FinalizeArray, а значит ее можно использовать,
    ///   как признак того, что FinalizeArray уже вызывалась.
    /// </remarks>
    class function NopInterfaceCreate : IInterface; static;

    /// <summary>
    ///   Держатель данных Api.
    /// </summary>
    class property Holder : IInterface  read FHolder;

  end;

implementation

{$REGION 'uses'}
uses
  {$REGION 'San'}
  San.Messages,
  San.Rtti
  {$ENDREGION}
;
{$ENDREGION}

{$REGION 'Api'}
class procedure Api.Initialize;
begin
end;

class function Api.IntfCompareAsObj(AIntf1, AIntf2: IObjRef): boolean;
begin
  Result := (AIntf1 = AIntf2)
         or (AIntf1.ObjRef = AIntf2.ObjRef)
end;

class function Api.IntfCompareAsObj(AIntf1: IInterface;
  AIntf2: IObjRef): boolean;
var
  ObjRef : IObjRef;
begin
  Result := (AIntf1 = AIntf2)
         or Supports(AIntf1, IObjRef, ObjRef) and (ObjRef.ObjRef = AIntf2.ObjRef);
end;

class function Api.IntfCompareAsObj(AIntf1: IObjRef;
  AIntf2: IInterface): boolean;
var
  ObjRef : IObjRef;
begin
  Result := (AIntf1 = AIntf2)
         or Supports(AIntf2, IObjRef, ObjRef) and (ObjRef.ObjRef = AIntf1.ObjRef);
end;

class function Api.IntfCompareAsObj(AIntf1, AIntf2: IInterface): boolean;
var
  ObjRef1, ObjRef2 : IObjRef;
begin
  Result := (AIntf1 = AIntf2)
         or Supports(AIntf1, IObjRef, ObjRef1) and Supports(AIntf2, IObjRef, ObjRef2) and (ObjRef1.ObjRef = ObjRef2.ObjRef);
end;

class procedure Api.IntfToObj(AIntf: IInterface; out AObj);
begin
  TObject(AObj) := (AIntf as IObjRef).ObjRef;
end;

class function Api.IntfToObj<TObj>(AIntf: IInterface): TObj;
begin
  Api.IntfToObj(AIntf, Result);
end;

class function Api.Cast<TIntf>(const AInstance: IInterface): TIntf;
begin
  Api.MustSupports<TIntf>(AInstance, Result);
end;

class function Api.Cast<TIntf>(const AInstance: TObject): TIntf;
begin
  Api.MustSupports<TIntf>(AInstance, Result);
end;

class procedure Api.Finalize;
begin
  FreeAndNil(FIntfsRTTITypesCache);
end;

class procedure Api.MustSupportsError(const AArgTypeRS : PResStringRec; const AClassText : string; const AIID: TGUID);
var
  IntfType : TRttiInterfaceType;
  IntfTypeStr : string;
begin

  IntfType := TypeByIID(AIID);
  if Assigned(IntfType) then
    IntfTypeStr := IntfType.Name
  else
    IntfTypeStr := '?';

  raise ESanIntfs.CreateResFmt(@s_San_Intfs_InterfaceMustSupportsErrFmt, [IntfTypeStr, GUIDToString(AIID), LoadResString(AArgTypeRS), AClassText]);
end;

class function Api.MustSupports(const AInstance: IInterface; const AIID: TGUID;
  out AIntf): boolean;
var
  Ref : IObjRef;
begin
  Result := True;

  if not Supports(AInstance, AIID, AIntf) then
    if not Assigned(AInstance) then
      MustSupportsError(@s_San_Intfs_InterfaceMustSupportsErr_Interface, 'nil', AIID)

    else
    if Supports(AInstance, IObjRef, Ref) then
      MustSupportsError(@s_San_Intfs_InterfaceMustSupportsErr_Object, Ref.ObjRef.ClassName, AIID)

    else
      MustSupportsError(@s_San_Intfs_InterfaceMustSupportsErr_Interface, '?', AIID);
end;

class function Api.MustSupports(const AInstance: TObject; const AIID: TGUID;
  out AIntf): boolean;
begin
  Result := True;

  if not Supports(AInstance, AIID, AIntf) then
    if not Assigned(AInstance) then
      MustSupportsError(@s_San_Intfs_InterfaceMustSupportsErr_Object, 'nil', AIID)

    else
      MustSupportsError(@s_San_Intfs_InterfaceMustSupportsErr_Object, AInstance.ClassName, AIID);
end;

class function Api.MustSupports(const AClass: TClass;
  const AIID: TGUID): boolean;
begin
  Result := True;

  if not Supports(AClass, AIID) then
    MustSupportsError(@s_San_Intfs_InterfaceMustSupportsErr_Class, AClass.ClassName, AIID);
end;

class function Api.MustSupports<TIntf>(const AInstance: IInterface;
  out AIntf: TIntf): boolean;
begin
  Result := MustSupports(AInstance, GetTypeData(TypeInfo(TIntf))^.Guid, AIntf);
end;

class function Api.MustSupports<TIntf>(const AInstance: TObject;
  out AIntf: TIntf): boolean;
begin
  Result := MustSupports(AInstance, GetTypeData(TypeInfo(TIntf))^.Guid, AIntf);
end;

class function Api.MustSupports<TIntf>(const AClass: TClass): boolean;
begin
  Result := MustSupports(AClass, GetTypeData(TypeInfo(TIntf))^.Guid);
end;

class function Api.TypeByIID(const AIID: TGUID): TRttiInterfaceType;
var
  Types : TArray<TRttiType>;
  T : TRttiType;
begin

  if not Assigned(FIntfsRTTITypesCache) then
    FIntfsRTTITypesCache := TGUIDIntfsRTTITypes.Create()

  else
  if FIntfsRTTITypesCache.TryGetValue(AIID, Result) then
    Exit;

  Types := San.Rtti.Api.Context.GetTypes();
  for T in Types do
    if (T is TRttiInterfaceType) and (TRttiInterfaceType(T).GUID = AIID) then
    begin
      Result := TRttiInterfaceType(T);
      FIntfsRTTITypesCache.Add(AIID, Result);
      Exit;
    end;

  Result := nil;
end;

class function Api.Hold(const AObjs: array of TObject): IInterface;
begin
  Result := THolderObjectArr.Create(AObjs);
end;

class function Api.Hold(const AObj: TObject): IInterface;
begin
  Result := THolderObject.Create(AObj);
end;

class function Api.Hold(const AProc: TProc): IInterface;
begin
  Result := THolderProcRef.Create(AProc);
end;

class function Api.NopAddref(inst: Pointer): Integer;
begin
  Result := -1;
end;

class function Api.NopQueryInterface(inst: Pointer; const IID: TGUID;
  out Obj): HResult;
begin
  Result := E_NOINTERFACE;
end;

class function Api.NopRelease(inst: Pointer): Integer;
begin
  Result := -1;
end;

class function Api.NopInterfaceCreate: IInterface;
const
  NopInterfaceVtable: array[0..2] of Pointer =
  (
    @San.Intfs.Api.NopQueryInterface,
    @San.Intfs.Api.NopAddref,
    @San.Intfs.Api.NopRelease
  );

  NopInterfaceVtablePtr : Pointer = @NopInterfaceVtable;
begin
  Result := IInterface(@NopInterfaceVtablePtr);
end;
{$ENDREGION}

initialization
  THolderApi.Init(Api.FHolder, Api.Initialize, Api.Finalize);

end{$WARNINGS OFF}.
{$ENDIF}
