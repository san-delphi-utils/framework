object frmLogView: TfrmLogView
  Left = 0
  Top = 0
  Width = 734
  Height = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  OnResize = FrameResize
  object vstRecs: TVirtualStringTree
    Left = 0
    Top = 0
    Width = 734
    Height = 400
    Align = alClient
    Header.AutoSizeIndex = -1
    Header.DefaultHeight = 24
    Header.Height = 24
    Header.Options = [hoAutoResize, hoColumnResize, hoDrag, hoShowSortGlyphs, hoVisible]
    Images = dmMain.il16x16
    ScrollBarOptions.AlwaysVisible = True
    ScrollBarOptions.ScrollBars = ssVertical
    TabOrder = 1
    TreeOptions.MiscOptions = [toAcceptOLEDrop, toEditable, toFullRepaintOnResize, toGridExtensions, toInitOnSave, toWheelPanning, toVariableNodeHeight, toEditOnDblClick]
    TreeOptions.PaintOptions = [toShowButtons, toShowDropmark, toShowHorzGridLines, toShowTreeLines, toShowVertGridLines, toThemeAware, toUseBlendedImages]
    TreeOptions.SelectionOptions = [toExtendedFocus, toFullRowSelect]
    OnCreateEditor = vstRecsCreateEditor
    OnDrawText = vstRecsDrawText
    OnEditing = vstRecsEditing
    OnGetText = vstRecsGetText
    OnPaintText = vstRecsPaintText
    OnGetImageIndexEx = vstRecsGetImageIndexEx
    OnInitNode = vstRecsInitNode
    Columns = <
      item
        Position = 0
        Width = 250
        WideText = 'Header'
      end
      item
        Position = 1
        Width = 463
        WideText = 'Message'
      end>
  end
  object pnlEmpty: TPanel
    Left = 288
    Top = 120
    Width = 185
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Caption = 'All data filtered'
    TabOrder = 0
  end
  object mmEdit: TMemo
    Left = 32
    Top = 48
    Width = 185
    Height = 89
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
    Visible = False
    OnExit = mmEditExit
  end
end
