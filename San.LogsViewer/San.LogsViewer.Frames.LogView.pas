{*******************************************************}
{                                                       }
{       LogsViewer                                      }
{                                                       }
{       Фрейм одного просмотра лога                     }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   LogsViewer
///   Фрейм одного просмотра лога
/// </summary>
unit San.LogsViewer.Frames.LogView;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'Winapi'}
  Winapi.Windows, Winapi.Messages,
  {$ENDREGION}

  {$REGION 'System'}
  System.SysUtils, System.Variants, System.Classes, System.Generics.Collections,
  System.Math,
  {$ENDREGION}

  {$REGION 'Vcl'}
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ImgList,
  Vcl.ExtCtrls,
  {$ENDREGION}

  // After Vcl.ImgList
  System.ImageList, System.UITypes,

  VirtualTrees,

  {$REGION 'San'}
  San,
  {$ENDREGION}

  {$REGION 'San.LogsViewer'}
  San.LogsViewer.Types,
  San.LogsViewer.DataModules.Main
  {$ENDREGION}
  ;
{$ENDREGION}

type
  ILogViewController = interface(IInterface)
  ['{BF698386-5D36-46D0-99FA-32FD85C73704}']
    {$REGION 'Gets&Sets'}
    function GetAutoScroll : boolean;
    {$ENDREGION}

    function IsFiltered(const ALogRec : TLogRecord) : boolean;

    property AutoScroll : boolean  read GetAutoScroll;
  end;

  TPropertyEditLink = class(TInterfacedObject, IVTEditLink)
  private
    FEdit: TWinControl;        // One of the property editor classes.
    FTree: TVirtualStringTree; // A back reference to the tree calling.
    FNode: PVirtualNode;       // The node being edited.
    FColumn: Integer;          // The column of the node being edited.
  protected
    procedure EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);

  public
    destructor Destroy; override;

    function BeginEdit: Boolean; stdcall;
    function CancelEdit: Boolean; stdcall;
    function EndEdit: Boolean; stdcall;
    function GetBounds: TRect; stdcall;
    function PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean; stdcall;
    procedure ProcessMessage(var Message: TMessage); stdcall;
    procedure SetBounds(R: TRect); stdcall;
  end;

  /// <summary>
  ///   Фрейм одного просмотра лога
  /// </summary>
  TfrmLogView = class(TFrame)
    pnlEmpty: TPanel;
    vstRecs: TVirtualStringTree;
    mmEdit: TMemo;
    procedure FrameResize(Sender: TObject);
    procedure vstRecsInitNode(Sender: TBaseVirtualTree; AParentNode,
      ANode: PVirtualNode; var AInitialStates: TVirtualNodeInitStates);
    procedure vstRecsGetText(Sender: TBaseVirtualTree; ANode: PVirtualNode;
      AColumn: TColumnIndex; ATextType: TVSTTextType; var ACellText: string);
    procedure vstRecsGetImageIndexEx(Sender: TBaseVirtualTree;
      ANode: PVirtualNode; AKind: TVTImageKind; AColumn: TColumnIndex;
      var AGhosted: Boolean; var AImageIndex: TImageIndex;
      var AImageList: TCustomImageList);
    procedure vstRecsPaintText(Sender: TBaseVirtualTree;
      const ATargetCanvas: TCanvas; ANode: PVirtualNode; AColumn: TColumnIndex;
      ATextType: TVSTTextType);
    procedure vstRecsDrawText(Sender: TBaseVirtualTree; ATargetCanvas: TCanvas;
      ANode: PVirtualNode; AColumn: TColumnIndex; const AText: string;
      const ACellRect: TRect; var ADefaultDraw: Boolean);
    procedure vstRecsEditing(Sender: TBaseVirtualTree; ANode: PVirtualNode;
      AColumn: TColumnIndex; var AAllowed: Boolean);
    procedure vstRecsCreateEditor(Sender: TBaseVirtualTree; ANode: PVirtualNode;
      AColumn: TColumnIndex; out AEditLink: IVTEditLink);
    procedure mmEditExit(Sender: TObject);

  strict private
    FLogRecords, FVisibleLogRecords : TLogRecords;

    function GetController: ILogViewController;
    function GetRecByNode(const ANode: PVirtualNode): TLogRecord;

  protected
    procedure LogRecordsNotify(Sender: TObject; const Item: TLogRecord;
      Action: TCollectionNotification);

    procedure VisibleLogRecordsNotify(Sender: TObject; const Item: TLogRecord;
      Action: TCollectionNotification);

    procedure UpdateNodeHeight(const ANode: PVirtualNode);

    property Controller : ILogViewController  read GetController;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    procedure ReFilter();

    property LogRecords : TLogRecords  read FLogRecords;

    property RecByNode[const ANode : PVirtualNode] : TLogRecord  read GetRecByNode;

    property Caption;
  end;

implementation

{$R *.dfm}

{$REGION 'TPropertyEditLink'}
destructor TPropertyEditLink.Destroy;
begin

  inherited;
end;

function TPropertyEditLink.PrepareEdit(Tree: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex): Boolean;
var
  MM : TMemo;
begin
  Result := True;
  FTree := Tree as TVirtualStringTree;
  FNode := Node;
  FColumn := Column;

  MM := (FTree.Owner as TfrmLogView).mmEdit;
  FEdit := MM;

  MM.Text := FTree.Text[Node, Column];
  MM.Hide;
  MM.Parent     := Tree;
  MM.OnKeyDown  := EditKeyDown;
  MM.OnKeyUp    := EditKeyUp;
end;

procedure TPropertyEditLink.EditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  CanAdvance: Boolean;

begin
  CanAdvance := true;

  case Key of
    VK_ESCAPE:
      begin
        Key := 0;//ESC will be handled in EditKeyUp()
      end;
    VK_RETURN:
      if CanAdvance then
      begin
        FTree.EndEditNode;
        Key := 0;
      end;

    VK_UP,
    VK_DOWN:
      begin
        // Consider special cases before finishing edit mode.
        CanAdvance := Shift = [];
        if CanAdvance then
        begin
          // Forward the keypress to the tree. It will asynchronously change the focused node.
          PostMessage(FTree.Handle, WM_KEYDOWN, Key, 0);
          Key := 0;
        end;
      end;
  end;
end;

procedure TPropertyEditLink.EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_ESCAPE:
      begin
        FTree.CancelEditNode;
        Key := 0;
      end;//VK_ESCAPE
  end;//case
end;

function TPropertyEditLink.BeginEdit: Boolean;
begin
  Result := True;
  FEdit.Show;
  FEdit.SetFocus;
end;

function TPropertyEditLink.CancelEdit: Boolean;
begin
  Result := True;
  FEdit.Hide;
end;

function TPropertyEditLink.EndEdit: Boolean;
begin
  Result := True;
end;

function TPropertyEditLink.GetBounds: TRect;
begin
  Result := FEdit.BoundsRect;
end;

procedure TPropertyEditLink.ProcessMessage(var Message: TMessage);
begin
  FEdit.WindowProc(Message);
end;

procedure TPropertyEditLink.SetBounds(R: TRect);
var
  Dummy: Integer;
begin
  // Since we don't want to activate grid extensions in the tree (this would influence how the selection is drawn)
  // we have to set the edit's width explicitly to the width of the column.
  FTree.Header.Columns.GetColumnBounds(FColumn, Dummy, R.Right);
  R.Height := FTree.NodeHeight[FNode];
  FEdit.BoundsRect := R;
end;
{$ENDREGION}

{$REGION 'TfrmLogView'}
constructor TfrmLogView.Create(AOwner: TComponent);
begin
  inherited;
  FLogRecords := TLogRecords.Create(False);
  FLogRecords.OnNotify := LogRecordsNotify;

  FVisibleLogRecords := TLogRecords.Create(False);
  FVisibleLogRecords.OnNotify := VisibleLogRecordsNotify;

end;

destructor TfrmLogView.Destroy;
begin
  FVisibleLogRecords.OnNotify := nil;
  FLogRecords.OnNotify := nil;

  FreeAndNil(FVisibleLogRecords);
  FreeAndNil(FLogRecords);
  inherited;
end;

procedure TfrmLogView.FrameResize(Sender: TObject);
begin
  pnlEmpty.Left  := (ClientWidth  - pnlEmpty.Width ) div 2;
  pnlEmpty.Top   := (ClientHeight - pnlEmpty.Height) div 2;
end;

function TfrmLogView.GetController: ILogViewController;
begin
  Result := Owner as ILogViewController;
end;

function TfrmLogView.GetRecByNode(const ANode: PVirtualNode): TLogRecord;
begin
  if not Assigned(ANode) then
    Result := nil
  else
    Result := FVisibleLogRecords[ANode^.Index];
end;

procedure TfrmLogView.LogRecordsNotify(Sender: TObject; const Item: TLogRecord;
  Action: TCollectionNotification);
begin
  if (Action = cnAdded) and not Controller.IsFiltered(Item) then
    FVisibleLogRecords.Add(Item)
  else
    FVisibleLogRecords.Remove(Item);

  pnlEmpty.Visible := LogRecords.Count = 0;
end;

procedure TfrmLogView.mmEditExit(Sender: TObject);
begin
  (Sender as TControl).Hide;
end;

procedure TfrmLogView.VisibleLogRecordsNotify(Sender: TObject;
  const Item: TLogRecord; Action: TCollectionNotification);
begin
  vstRecs.RootNodeCount := TLogRecords(Sender).Count;
  if not Assigned(vstRecs.FocusedNode) or Controller.AutoScroll then
    vstRecs.FocusedNode := vstRecs.RootNode^.LastChild;
end;

procedure TfrmLogView.ReFilter;
var
  Cursor : TCursor;
  Controller : ILogViewController;
  OnNotify : TCollectionNotifyEvent<TLogRecord>;
  R : TLogRecord;
  Idx : integer;
  Node: PVirtualNode;
begin
  Cursor := Screen.Cursor;

  Screen.Cursor := crHourGlass;
  try
    Controller := Self.Controller;

    vstRecs.BeginUpdate;
    try
      OnNotify := FVisibleLogRecords.OnNotify;

      FVisibleLogRecords.OnNotify := nil;
      try
        LockWindowUpdate(Handle);
        try
          Idx := -1;
          for R in LogRecords do
            if not Controller.IsFiltered(R) then
            begin
              Inc(Idx);
              if FVisibleLogRecords.Count > Idx then
                FVisibleLogRecords[Idx] := R
              else
                FVisibleLogRecords.Add(R);
            end;

          FVisibleLogRecords.Count := Idx + 1;

          pnlEmpty.Visible := FVisibleLogRecords.Count = 0;

        finally
          LockWindowUpdate(0);
        end;

      finally
        FVisibleLogRecords.OnNotify := OnNotify;
      end;

      OnNotify(FVisibleLogRecords, nil, cnAdded);

      for Node in vstRecs.Nodes do
        vstRecs.ReinitNode(Node, False);

    finally
      vstRecs.EndUpdate;
    end;

  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TfrmLogView.UpdateNodeHeight(const ANode: PVirtualNode);
var
  R : TLogRecord;
begin
  R := RecByNode[ANode];
  vstRecs.NodeHeight[ANode] := Max(3, R.Message.CountChar(#13)) * (-vstRecs.Font.Height + 3) + 8;
end;

procedure TfrmLogView.vstRecsInitNode(Sender: TBaseVirtualTree; AParentNode,
  ANode: PVirtualNode; var AInitialStates: TVirtualNodeInitStates);
begin
  Include(AInitialStates, ivsMultiline);
  UpdateNodeHeight(ANode);
end;

procedure TfrmLogView.vstRecsGetText(Sender: TBaseVirtualTree;
  ANode: PVirtualNode; AColumn: TColumnIndex; ATextType: TVSTTextType;
  var ACellText: string);
var
  R : TLogRecord;
begin
  R := RecByNode[ANode];
  case AColumn of
    0: ACellText := R.PutDate + #13#10
                  + San.Api.Logger.LogRecordKindToStr(R.Kind) + #13#10
                  + R.SubType;
    1: ACellText := R.Message;
  end;
//
end;

procedure TfrmLogView.vstRecsGetImageIndexEx(Sender: TBaseVirtualTree;
  ANode: PVirtualNode; AKind: TVTImageKind; AColumn: TColumnIndex;
  var AGhosted: Boolean; var AImageIndex: TImageIndex;
  var AImageList: TCustomImageList);
var
  R : TLogRecord;
begin
  if (AColumn <> 0) or not (AKind in [ikNormal, ikSelected]) then
    Exit;

  R := RecByNode[ANode];

  AImageList := (Sender as TVirtualStringTree).Images;
  AImageIndex := Min(R.Kind, 4);
end;

procedure TfrmLogView.vstRecsPaintText(Sender: TBaseVirtualTree;
  const ATargetCanvas: TCanvas; ANode: PVirtualNode; AColumn: TColumnIndex;
  ATextType: TVSTTextType);
begin
  case AColumn of
    0: ATargetCanvas.Font.Name := Self.Font.Name;
    1: ATargetCanvas.Font.Name := 'Courier New';
  end;
end;

procedure TfrmLogView.vstRecsDrawText(Sender: TBaseVirtualTree;
  ATargetCanvas: TCanvas; ANode: PVirtualNode; AColumn: TColumnIndex;
  const AText: string; const ACellRect: TRect; var ADefaultDraw: Boolean);
var
  R : TLogRecord;
  Pt : TPoint;

  procedure Output(const AStr : string; const AFontStyles : TFontStyles);
  begin
    ATargetCanvas.Font.Style := AFontStyles;
    Pt.X := ACellRect.Left + (ACellRect.Width - ATargetCanvas.TextWidth(AStr)) div 2;
    ATargetCanvas.TextRect(ACellRect, Pt.X, Pt.Y, AStr);
    Pt.Y := Pt.Y + -ATargetCanvas.Font.Height + 2;
  end;

begin
  if AColumn <> 0 then
    Exit;

  ADefaultDraw := False;


  R := RecByNode[ANode];

  ATargetCanvas.Brush.Style := bsClear;

  Pt.Y := ACellRect.Top  + 0;

  Output(R.PutDate, []);
  Output(San.Api.Logger.LogRecordKindToStr(R.Kind), [fsItalic]);
  Output(R.SubType, [fsBold]);
end;

procedure TfrmLogView.vstRecsEditing(Sender: TBaseVirtualTree;
  ANode: PVirtualNode; AColumn: TColumnIndex; var AAllowed: Boolean);
begin
  AAllowed := AColumn = 1;
end;

procedure TfrmLogView.vstRecsCreateEditor(Sender: TBaseVirtualTree;
  ANode: PVirtualNode; AColumn: TColumnIndex; out AEditLink: IVTEditLink);
begin
  if AColumn = 1 then
    AEditLink := TPropertyEditLink.Create();
end;
{$ENDREGION}



end.
