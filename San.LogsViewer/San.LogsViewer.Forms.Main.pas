{*******************************************************}
{                                                       }
{       LogsViewer                                      }
{                                                       }
{       Главная форма                                   }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   LogsViewer
///   Главная форма
/// </summary>
unit San.LogsViewer.Forms.Main;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'Winapi'}
  Winapi.Windows, Winapi.Messages,
  {$ENDREGION}

  {$REGION 'System'}
  System.SysUtils, System.Variants, System.Classes, System.Generics.Collections,
  System.IniFiles, System.Actions, System.StrUtils, System.Math,
  {$ENDREGION}

  {$REGION 'Vcl'}
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls,Vcl.ImgList,
  Vcl.ActnList, Vcl.Menus,
  {$ENDREGION}

  System.ImageList, System.UITypes,

  {$REGION 'San'}
  San,
  {$ENDREGION}

  {$REGION 'San.LogsViewer'}
  San.LogsViewer.Types,
  San.LogsViewer.DataModules.Main,
  San.LogsViewer.Controls,
  San.LogsViewer.Frames.LogView,
  San.LogsViewer.Frames.LogFile, Vcl.ExtCtrls
  {$ENDREGION}
  ;
{$ENDREGION}

type
  /// <summary>
  ///   Главная форма
  /// </summary>
  TfmMain = class(TForm, ILogFileController)
    tbcLogs: TTabControl;
    sb: TStatusBar;
    mmn: TMainMenu;
    actlst: TActionList;
    dlgOpen: TOpenDialog;
    miFile: TMenuItem;
    miOpen: TMenuItem;
    miExitSep: TMenuItem;
    miExit: TMenuItem;
    actOpen: TAction;
    actExit: TAction;
    pnlEmpty: TPanel;
    tmrRead: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure actOpenExecute(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure tbcLogsCloseClick(Sender : TObject; const ATabIndex : integer);
    procedure FormResize(Sender: TObject);
    procedure tbcLogsChange(Sender: TObject);
    procedure tmrReadTimer(Sender: TObject);

  strict private
    FShowed : boolean;
    FLogFileCounter : integer;
    function GetActiveLogFile: TfrmLogFile;
    function GetLogFiles(const AIndex: integer): TfrmLogFile;

  protected
    procedure INIWork(const AAction : TProc<TMemIniFile>);
    procedure AfterFirstShow();
    function AddLogFile(const AFileName : TFileName): TfrmLogFile;

    {$REGION 'ILogFileController'}
    procedure UpdateLogFileTimes(const ALogFile : TfrmLogFile);
    {$ENDREGION}

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy(); override;

    function TryFindLogFileByFileName(const AFileName : TFileName; out ALogFile : TfrmLogFile;
      const AIndexPtr : PInteger = nil) : boolean;
    function ExistsLogFileByFileName(const AFileName : TFileName) : boolean;


    property LogFiles[const AIndex : integer] : TfrmLogFile  read GetLogFiles;
    property ActiveLogFile : TfrmLogFile  read GetActiveLogFile;

  end;

var
  fmMain: TfmMain;

implementation

{$R *.dfm}

{$REGION 'TfmMain'}
constructor TfmMain.Create(AOwner: TComponent);
begin
  inherited;

end;

destructor TfmMain.Destroy;
begin

  inherited;
end;

function TfmMain.TryFindLogFileByFileName(const AFileName: TFileName;
  out ALogFile: TfrmLogFile; const AIndexPtr : PInteger = nil): boolean;
var
  i : integer;
  LFF : TfrmLogFile;
begin
  for i := 0 to tbcLogs.Tabs.Count - 1 do
  begin
    LFF := LogFiles[i];

    if not AnsiSameText(AFileName, LFF.FileName) then
      Continue;

    ALogFile := LFF;

    if Assigned(AIndexPtr) then
      AIndexPtr^ := i;

    Exit(True);
  end;

  ALogFile := nil;

  if Assigned(AIndexPtr) then
    AIndexPtr^ := -1;

  Result := False;
end;

procedure TfmMain.UpdateLogFileTimes(const ALogFile: TfrmLogFile);
begin
  if ALogFile <> ActiveLogFile then
    Exit;

  sb.Panels[0].Text := FormatDateTime('hh:nn:ss.zzz', ALogFile.MaxReadTime);
  sb.Panels[1].Text := FormatDateTime('hh:nn:ss.zzz', ALogFile.MinReadTime);
end;

function TfmMain.ExistsLogFileByFileName(const AFileName: TFileName): boolean;
var
  Dummy : TfrmLogFile;
begin
  Result := TryFindLogFileByFileName(AFileName, Dummy);
end;

procedure TfmMain.INIWork(const AAction: TProc<TMemIniFile>);
var
  INI : TMemIniFile;
begin
  INI := TMemIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  try
    AAction(INI);
    INI.UpdateFile;
  finally
    FreeAndNil(INI);
  end;
end;

procedure TfmMain.FormCreate(Sender: TObject);
begin
  {$IFDEF DEBUG}
  Caption := Caption + ' [DEBUG]';
  {$ENDIF}

  Application.Title := Caption;

  tbcLogs.OnCloseClickEvent := tbcLogsCloseClick;

  pnlEmpty.BringToFront;

  INIWork(
    procedure (AINI : TMemIniFile)
    var
      dir : string;
    begin
      dir := AINI.ReadString(Name, dlgOpen.Name + '.InitialDir', '');
      if DirectoryExists(dir) then
        dlgOpen.InitialDir := dir
      else
        dlgOpen.InitialDir := ExtractFileDir(Application.ExeName);
    end
  );

end;

procedure TfmMain.FormDestroy(Sender: TObject);
begin
//
end;

procedure TfmMain.FormResize(Sender: TObject);
begin
  pnlEmpty.Left  := (ClientWidth  - pnlEmpty.Width ) div 2;
  pnlEmpty.Top   := (ClientHeight - pnlEmpty.Height) div 2;
end;

procedure TfmMain.FormShow(Sender: TObject);
begin
  if FShowed then
    Exit;

  San.Api.PostProcessor.Execute(AfterFirstShow);

  FShowed := True;
end;

function TfmMain.GetLogFiles(const AIndex: integer): TfrmLogFile;
begin
  Result := tbcLogs.Tabs.Objects[AIndex] as TfrmLogFile;
end;

function TfmMain.GetActiveLogFile: TfrmLogFile;
begin
  Result := GetLogFiles(tbcLogs.TabIndex);
end;

procedure TfmMain.actExitExecute(Sender: TObject);
begin
  Close();
end;

procedure TfmMain.actOpenExecute(Sender: TObject);
var
  LogFile: TfrmLogFile;
  i : integer;
begin
  if not dlgOpen.Execute(Handle) then
    Exit;

  INIWork(
    procedure (AINI : TMemIniFile)
    begin
      AINI.WriteString(Name, dlgOpen.Name + '.InitialDir', ExtractFileDir(dlgOpen.FileName));
    end
  );

  if TryFindLogFileByFileName(dlgOpen.FileName, LogFile, @i) then
    tbcLogs.TabIndex := i
  else
    AddLogFile(dlgOpen.FileName);
end;

function TfmMain.AddLogFile(const AFileName : TFileName): TfrmLogFile;
begin
  Result := TfrmLogFile.Create(Self, AFileName);
  Result.Name := Format('frmLogFile%d', [AtomicIncrement(FLogFileCounter)]);
  Result.Align := alClient;

  tbcLogs.Tabs.AddObject(ExtractFileName(AFileName) + DupeString(' ', 5), Result);
  tbcLogs.TabIndex := tbcLogs.Tabs.Count - 1;

  pnlEmpty.Hide;

  tbcLogs.OnChange(tbcLogs);
end;

procedure TfmMain.AfterFirstShow;
begin
  if tbcLogs.Tabs.Count = 0 then
    actOpen.Execute;
end;

procedure TfmMain.tbcLogsChange(Sender: TObject);
var
  TBC : TTabControl;
  LFF : TfrmLogFile;
begin
  TBC := Sender as TTabControl;
  while TBC.ControlCount > 0 do
    TBC.Controls[0].Parent := nil;

  LFF := ActiveLogFile;
  LFF.Parent := TBC;

  UpdateLogFileTimes(LFF);
end;

procedure TfmMain.tbcLogsCloseClick(Sender: TObject; const ATabIndex: integer);
var
  TBC : TTabControl;
  LFF : TfrmLogFile;
  NewActiveIndex : integer;
begin
  TBC := Sender as TTabControl;
  LFF := LogFiles[ATabIndex];

  FreeAndNil(LFF);

  NewActiveIndex := Min(TBC.Tabs.Count - 2, TBC.TabIndex);
  TBC.Tabs.Delete(ATabIndex);
  TBC.TabIndex := NewActiveIndex;

  pnlEmpty.Visible := TBC.Tabs.Count > 0;
end;

procedure TfmMain.tmrReadTimer(Sender: TObject);
var
  Timer : TTimer;
  Cursor : TCursor;
  i : integer;
begin
  if tbcLogs.Tabs.Count = 0 then
    Exit;

  Cursor := Screen.Cursor;

  Screen.Cursor := crHourGlass;
  try
    Timer := Sender as TTimer;

    Timer.Enabled := False;

    for i := 0 to tbcLogs.Tabs.Count - 1 do
      LogFiles[i].ReadRecords();

    Timer.Enabled := True;

  finally
    Screen.Cursor := Cursor;
  end;

end;
{$ENDREGION}

end.
