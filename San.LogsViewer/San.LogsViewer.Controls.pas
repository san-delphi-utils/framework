{*******************************************************}
{                                                       }
{       LogsViewer                                      }
{                                                       }
{       Элементы управления                             }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   LogsViewer
///   Главная форма
/// </summary>
unit San.LogsViewer.Controls;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'Winapi'}
  Winapi.Windows, Winapi.Messages,
  {$ENDREGION}

  {$REGION 'System'}
  System.SysUtils, System.Variants, System.Classes, System.Generics.Collections,
  System.IniFiles, System.Actions, System.StrUtils, System.Types,
  {$ENDREGION}

  {$REGION 'Vcl'}
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ImgList,
  Vcl.ActnList, Vcl.Menus,
  {$ENDREGION}

  System.ImageList, System.UITypes,

  {$REGION 'San'}
  San,
  {$ENDREGION}

  {$REGION 'San.LogsViewer'}
  San.LogsViewer.DataModules.Main
  {$ENDREGION}
  ;
{$ENDREGION}

type

  TTabControl = class(Vcl.ComCtrls.TTabControl)
  public type
    TCloseClickEvent = procedure (Sender : TObject; const ATabIndex : integer) of object;

  strict private
    FOnCloseClickEvent : TCloseClickEvent;

  private
    procedure WMLButtonDown(var Message: TWMLButtonDown); message WM_LBUTTONDOWN;

  protected
    procedure DrawTab(TabIndex: Integer; const Rect: TRect; Active: Boolean); override;

  public
    function TryHitTest(const APoint : TPoint; out ATabIndex : integer) : boolean;

    property OnCloseClickEvent : TCloseClickEvent
      read FOnCloseClickEvent  write FOnCloseClickEvent;
  end;

implementation

{$REGION 'TTabControl'}
function TTabControl.TryHitTest(const APoint : TPoint; out ATabIndex: integer): boolean;
var
  i : integer;
begin
  for i := 0 to Tabs.Count - 1 do
    if TabRect(i).Contains(APoint) then
    begin
      ATabIndex := i;
      Exit(True);
    end;

  ATabIndex := -1;
  Result := False;
end;

procedure TTabControl.WMLButtonDown(var Message: TWMLButtonDown);
var
  Pt : TPoint;
  Idx : integer;
////  Rect
begin
  Pt := Point(Message.XPos, Message.YPos);
  if TryHitTest(Pt, Idx) and (Pt.X >= TabRect(Idx).Right - 20) then
    if Assigned(FOnCloseClickEvent) then
    begin
      FOnCloseClickEvent(Self, Idx);
      Exit;
    end;

  inherited;
end;

procedure TTabControl.DrawTab(TabIndex: Integer; const Rect: TRect;
  Active: Boolean);
var
  d : TPoint;
begin
  inherited;

  d := Point(0, Ord(not Active) * 3);
  dmMain.il16x16.Draw(Canvas, Rect.Right - dmMain.il16x16.Width - 4 + d.X, Rect.Top + (Rect.Height - dmMain.il16x16.Height) div 2 + d.Y, 5);
  Canvas.Brush.Style := bsClear;
  Canvas.TextRect(Rect, Rect.Left + 6 + d.X, (Rect.Height - Canvas.TextHeight('1')) div 2 + d.Y, Trim(Tabs[TabIndex]));
end;
{$ENDREGION}



end.
