{*******************************************************}
{                                                       }
{       LogsViewer                                      }
{                                                       }
{       Типы данных                                     }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   LogsViewer
///   Типы данных
/// </summary>
unit San.LogsViewer.Types;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Variants, System.Classes, System.Generics.Collections,
  {$ENDREGION}

  {$REGION 'San'}
  San.Logger
  {$ENDREGION}
  ;
{$ENDREGION}

type
  TLogRecord = class(TObject)
  private
    FMessage: TSANLogRecordText;
    FKind: TSANLogRecordKind;
    FSubType: TSANLogRecordSubType;
    FPutDate: string;

  public
    constructor Create(const APutDate : string; const AKind : TSANLogRecordKind;
      const ASubType : TSANLogRecordSubType; const AMessage : TSANLogRecordText);

   procedure AddToMessage(const AStr : string);

    property PutDate : string  read FPutDate;

    property Kind : TSANLogRecordKind  read FKind;

    property SubType : TSANLogRecordSubType  read FSubType;

    property Message : TSANLogRecordText  read FMessage;
  end;

  TLogRecords = class(TObjectList<TLogRecord>)
    function TryFindBySubType(const ASubType : TSANLogRecordSubType; out AItem : TLogRecord;
      const AIndexPtr : PInteger = nil; const AFirstIndex : integer = 0) : boolean;

    function Add(const APutDate : string; const AKind : TSANLogRecordKind;
      const ASubType : TSANLogRecordSubType; const AMessage : TSANLogRecordText) : TLogRecord; overload;
  end;

implementation

{$REGION 'TLogRecord'}
procedure TLogRecord.AddToMessage(const AStr: string);
begin
  FMessage := FMessage + AStr;
end;

constructor TLogRecord.Create(const APutDate: string;
  const AKind: TSANLogRecordKind; const ASubType: TSANLogRecordSubType;
  const AMessage: TSANLogRecordText);
begin
  inherited Create;
  FPutDate := APutDate;
  FKind := AKind;
  FSubType := ASubType;
  FMessage := AMessage;
end;
{$ENDREGION}

{$REGION 'TLogRecords'}
function TLogRecords.TryFindBySubType(const ASubType: TSANLogRecordSubType;
  out AItem: TLogRecord; const AIndexPtr: PInteger; const AFirstIndex : integer): boolean;
var
  i : integer;
  Item : TLogRecord;
begin
   for i := AFirstIndex to Count - 1 do
   begin
     Item := Items[i];

     if not AnsiSameText(ASubType, Item.SubType) then
       Continue;

     AItem := Item;

     if Assigned(AIndexPtr) then
       AIndexPtr^ := i;

     Exit(True);
   end;

   AItem := nil;

   if Assigned(AIndexPtr) then
     AIndexPtr^ := -1;

  Result := False;
end;

function TLogRecords.Add(const APutDate: string; const AKind: TSANLogRecordKind;
  const ASubType: TSANLogRecordSubType;
  const AMessage: TSANLogRecordText): TLogRecord;
begin
  Result := TLogRecord.Create(APutDate, AKind, ASubType, AMessage);
  try
    inherited Add(Result);
  except
    FreeAndNil(Result);
    raise;
  end;
end;
{$ENDREGION}

end.
