{*******************************************************}
{                                                       }
{       LogsViewer                                      }
{                                                       }
{       Фрейм одного Log-файла                          }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   LogsViewer
///   Фрейм одного Log-файла
/// </summary>
unit San.LogsViewer.Frames.LogFile;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'Winapi'}
  Winapi.Windows, Winapi.Messages,
  {$ENDREGION}

  {$REGION 'System'}
  System.SysUtils, System.Variants, System.Classes, System.Generics.Collections,
  System.Generics.Defaults, System.StrUtils, System.Math, System.RegularExpressions,
  {$ENDREGION}

  {$REGION 'Vcl'}
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.ComCtrls, Vcl.CheckLst, Vcl.Buttons,
  {$ENDREGION}

  {$REGION 'San'}
  San, San.Utils,
  {$ENDREGION}

  {$REGION 'San.LogsViewer'}
  San.LogsViewer.Types,
  San.LogsViewer.DataModules.Main,
  San.LogsViewer.Frames.LogView
  {$ENDREGION}
  ;
{$ENDREGION}

const
  BUFFER_SIZE = 1024;
  HEADER_DATETIME_REG_EX = '(\d{4}.\d{2}.\d{2})?.\d{2}.\d{2}.\d{2}(.\d{3})?';

type
  TfrmLogFile = class;

  ILogFileController = interface
  ['{81643BA8-8112-4D94-B91E-A6A4A7F905DB}']
    procedure UpdateLogFileTimes(const ALogFile : TfrmLogFile);

  end;

  /// <summary>
  ///   Фрейм одного Log-файла
  /// </summary>
  TfrmLogFile = class(TFrame, ILogViewController)
    pnlTop: TPanel;
    grpDivide: TGroupBox;
    cbbDivideBy: TComboBox;
    cbbViews: TComboBox;
    pnlDivideBy: TPanel;
    pnlViews: TPanel;
    lblDivideBy: TLabel;
    lblViews: TLabel;
    grpFilter: TGroupBox;
    chInfo: TCheckBox;
    chWarning: TCheckBox;
    chError: TCheckBox;
    chFatal: TCheckBox;
    bvlKinds: TBevel;
    pnlFilterKinds: TPanel;
    chklstFilterSubtypes: TCheckListBox;
    grpControl: TGroupBox;
    chAutoScroll: TCheckBox;
    procedure cbbDivideByChange(Sender: TObject);
    procedure cbbViewsChange(Sender: TObject);
    procedure chFilterKindClick(Sender: TObject);
    procedure chklstFilterSubtypesClickCheck(Sender: TObject);
  public type
    TfrmLogViews = class(TObjectList<TfrmLogView>)
    strict private
      FOwner : TfrmLogFile;
      FViewCounter : integer;
      function GetActiveView: TfrmLogView;

    public
      constructor Create(const AOwner : TfrmLogFile);
      destructor Destroy; override;

      function Add(const ACaption : TCaption) : TfrmLogView; overload;
      function AddOrGet(const AViewIndex : integer; const ACaption : string) : TfrmLogView;


      property ActiveView : TfrmLogView  read GetActiveView;
    end;

  strict private
    FFileStream : TFileStream;
    FByffer : TBytes;
    FLogRecords, FLogRecordsBuffer : TLogRecords;
    FPart : string;
    FKnownSubtypes : TDictionary<TUpperString,TSANLogRecordSubType>;
    FDivideSubType : TSANLogRecordSubType;
    FViews : TfrmLogViews;
    FReadTimes : TArray<TDateTime>;
    FMinReadTime, FMaxReadTime : TDateTime;
    function GetFileName: TFileName;
    function GetController: ILogFileController;

  strict private
    class var FHeaderDateTimeRegEx : TRegEx;

  protected
    {$REGION 'ILogViewController'}
    function IsFiltered(const ALogRec : TLogRecord) : boolean;
    function GetAutoScroll : boolean;
    {$ENDREGION}

    procedure ReFillSubTypes();
    procedure LogRecordsNotify(Sender: TObject; const Item: TLogRecord;
      Action: TCollectionNotification);
    procedure UpdateDivision(const AFirstIndex : integer = 0);

    property Controller : ILogFileController  read GetController;

  public
    class constructor Create;

    constructor Create(AOwner : TComponent; const AFileName : TFileName); reintroduce;
    destructor Destroy; override;

    procedure ReadRecords();

    class property HeaderDateTimeRegEx : TRegEx  read FHeaderDateTimeRegEx;

    property FileName : TFileName  read GetFileName;
    property LogRecords : TLogRecords  read FLogRecords;
    property Views : TfrmLogViews  read FViews;

    property MinReadTime : TDateTime  read FMinReadTime;
    property MaxReadTime : TDateTime  read FMaxReadTime;
  end;

implementation

{$R *.dfm}

{$REGION 'TfrmLogFile.TfrmLogViews'}
constructor TfrmLogFile.TfrmLogViews.Create(const AOwner: TfrmLogFile);
begin
  inherited Create(True);
  FOwner := AOwner;
end;

destructor TfrmLogFile.TfrmLogViews.Destroy;
begin
  OwnsObjects := False;
  inherited;
end;

function TfrmLogFile.TfrmLogViews.Add(const ACaption: TCaption): TfrmLogView;
begin
  Result := TfrmLogView.Create(FOwner);
  try
    Result.Name := Format('frmLogView%d', [AtomicIncrement(FViewCounter)]);
    Result.Align := alClient;
    Result.Caption := ACaption;
    FOwner.cbbViews.Items.AddObject(ACaption, Result);

    inherited Add(Result);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TfrmLogFile.TfrmLogViews.AddOrGet(const AViewIndex: integer;
  const ACaption: string): TfrmLogView;
var
  Idx : integer;
begin
  if AViewIndex >= Count then
    Result := Add(ACaption)
  else
  begin
    Result := Items[AViewIndex];
    Result.Caption := ACaption;
    Idx := FOwner.cbbViews.Items.IndexOfObject(Result);
    FOwner.cbbViews.Items[Idx] := ACaption;
  end;
end;

function TfrmLogFile.TfrmLogViews.GetActiveView: TfrmLogView;
begin
  Result := Items[FOwner.cbbViews.ItemIndex];
end;
{$ENDREGION}

{$REGION 'TfrmLogFile'}
procedure TfrmLogFile.cbbDivideByChange(Sender: TObject);
var
  cb : TComboBox;
  S : string;
begin
  cb := Sender as TComboBox;
  S := cb.Items[cb.ItemIndex];

  if FDivideSubType = S then
    Exit;

  FDivideSubType := S;

  UpdateDivision();
end;

procedure TfrmLogFile.cbbViewsChange(Sender: TObject);
var
  i : integer;
  ActiveView : TfrmLogView;
begin
  ActiveView := Views.ActiveView;

  for i := 0 to ControlCount - 1 do
    if Controls[i] is TfrmLogView then
    begin
      if Controls[i] = ActiveView then
        Exit;

      LockWindowUpdate(Handle);

      Controls[i].Parent := nil;
      Break;
    end;

  ActiveView.Parent := Self;
  ActiveView.TabOrder := 0;

  LockWindowUpdate(0);
end;

procedure TfrmLogFile.chFilterKindClick(Sender: TObject);
begin
  Views.ActiveView.ReFilter;
end;

procedure TfrmLogFile.chklstFilterSubtypesClickCheck(Sender: TObject);
begin
  Views.ActiveView.ReFilter;
end;

class constructor TfrmLogFile.Create;
begin
  FHeaderDateTimeRegEx := TRegEx.Create(HEADER_DATETIME_REG_EX);
end;

constructor TfrmLogFile.Create(AOwner: TComponent; const AFileName : TFileName);
begin
  inherited Create(AOwner);

  FFileStream := TFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
  SetLength(FByffer, BUFFER_SIZE);

  FLogRecordsBuffer := TLogRecords.Create(True);

  FLogRecords := TLogRecords.Create(True);
  FLogRecords.OnNotify := LogRecordsNotify;


  FKnownSubtypes := TDictionary<TUpperString,TSANLogRecordSubType>.Create(TUpperStringEqualityComparer.Create);
  FViews := TfrmLogViews.Create(Self);
end;

destructor TfrmLogFile.Destroy;
begin
  FreeAndNil(FViews);
  FLogRecords.OnNotify := nil;
  FreeAndNil(FKnownSubtypes);
  FreeAndNil(FFileStream);
  FreeAndNil(FLogRecordsBuffer);
  FreeAndNil(FLogRecords);
  inherited;
end;

procedure TfrmLogFile.LogRecordsNotify(Sender: TObject; const Item: TLogRecord;
  Action: TCollectionNotification);
begin
  if Action = cnAdded then
    FKnownSubtypes.AddOrSetValue(Item.SubType, Item.SubType);
end;

function TfrmLogFile.GetFileName: TFileName;
begin
  Result := FFileStream.FileName;
end;

function TfrmLogFile.IsFiltered(const ALogRec: TLogRecord): boolean;

  function SameKind : boolean;
  var
    ch : TCheckBox;
  begin
    case ALogRec.Kind of
      LRK_INFO:     ch := chInfo;
      LRK_WARNING:  ch := chWarning;
      LRK_ERROR:    ch := chError;
      LRK_FATAL:    ch := chFatal;
    else
      Exit(True);
    end;

    Result := ch.Checked;
  end;

  function SameSubType : boolean;
  var
    Idx : integer;
  begin
    Idx := chklstFilterSubtypes.Items.IndexOf(ALogRec.SubType);
    Result := (Idx < 0) or chklstFilterSubtypes.Checked[Idx];
  end;

begin
  Result := not (SameKind and SameSubType);
end;

function TfrmLogFile.GetAutoScroll: boolean;
begin
  Result := chAutoScroll.Checked;
end;

function TfrmLogFile.GetController: ILogFileController;
begin
  Result := Owner as ILogFileController;
end;

procedure TfrmLogFile.ReadRecords;

  function ReadText() : string;
  var
    Readed : integer;
  begin
    Result := '';

    repeat
      Readed := FFileStream.Read(FByffer, BUFFER_SIZE);

      if Readed > 0 then
        Result := Result + StringOf(FByffer);

      Application.ProcessMessages;
    until Application.Terminated or (Readed < BUFFER_SIZE);
  end;

  procedure Split(const AString, ASeparator: string; const AOnSplit : TStrProc);
  var
    Offset, p, L : integer;
  begin
    L := Length(ASeparator);

    Offset := 1;
    repeat

      p := Pos(ASeparator, AString, Offset);
      if P < 1 then
        Break;

      AOnSplit( Copy(AString, Offset, p - Offset) );

      Offset := p + L;

      Application.ProcessMessages;
    until Application.Terminated;

    if not Application.Terminated and (Length(AString) >= Offset) then
      AOnSplit( Copy(AString, Offset) );
  end;

var
  Cursor : TCursor;
  Time : TDateTime;
  Text : string;
  Idx, LastCount : integer;
begin
  Cursor := Screen.Cursor;

  Screen.Cursor := crHourGlass;
  try

    Time := Now();
    try

      Text := FPart + ReadText();

      Idx := 0;
      Split(Text, LOG_REC_MARKER, procedure (const AStr : string)
        var
          P : integer;
          Header, PutDate, KindStr, Subtype : string;
          R : TLogRecord;
          M : TMatch;
  //        VWF : TfrmLogView;
        begin
          if (Idx = 0) and not Trim(AStr).IsEmpty then
            Self.FLogRecords.Last.AddToMessage(AStr)
          else
          begin
            P := Pos(#13#10, AStr);

            if P = 0 then
              Self.FPart := AStr
            else
            begin
              Header := Trim(Copy(AStr, 1, P - 1));
              M := FHeaderDateTimeRegEx.Match(Header);
              if not M.Success then
                Exit;

              P := Pos(' ', Header, M.Index + M.Length + 1);

              PutDate := M.Value;
              KindStr := Trim(Copy(Header, M.Index + M.Length + 1, P - 1));
              Subtype := Trim(Copy(Header, P + 1));

              R := Self.FLogRecordsBuffer.Add(PutDate, San.Api.Logger.StrToLogRecordKind(KindStr), Subtype, '');
              R.AddToMessage(TrimLeft(Copy(AStr, P + 2)));
  //
  //            if Self.Views.Count = 0 then
  //            begin
  //              VWF := Self.Views.Add(R.PutDate);
  //              Self.cbbViews.ItemIndex := 0;
  //              Self.cbbViews.OnChange(Self.cbbViews);
  //            end
  //            else
  //              VWF := Self.Views.Last;
  //
  //            VWF.LogRecords.Add(R);
            end
          end;

          Inc(Idx);
        end
      );

      LastCount := LogRecords.Count;

      LogRecords.AddRange(FLogRecordsBuffer);
      FLogRecordsBuffer.OwnsObjects := False;
      try
        FLogRecordsBuffer.Clear;
      finally
        FLogRecordsBuffer.OwnsObjects := True;
      end;

      UpdateDivision(LastCount);

      if not Application.Terminated then
        ReFillSubTypes();

    finally
      Time := Now() - Time;
      System.Insert(Time, FReadTimes, Length(FReadTimes));
      if Length(FReadTimes) = 1 then
      begin
        FMinReadTime := Time;
        FMaxReadTime := Time;
      end

      else
      begin
        FMinReadTime := Min(FMinReadTime, Time);
        FMaxReadTime := Max(FMaxReadTime, Time);
      end;

      Controller.UpdateLogFileTimes(Self);
    end;

  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TfrmLogFile.ReFillSubTypes;
var
  Old, S : string;
  List : TList<string>;
  SS : TStrings;
  i : integer;
begin

  List := TList<string>.Create();
  try

    for S in FKnownSubtypes.Values do
      List.Add(S);

    List.Sort(TStringComparer.Ordinal);


    SS := cbbDivideBy.Items;
    if SS.Count = 0 then
      Old := ''
    else
      Old := SS[cbbDivideBy.ItemIndex];

    SS.BeginUpdate;
    try
      if SS.Count = 0 then
        SS.Add('<None>');

      for i := 0 to List.Count - 1 do
        if (SS.Count <= i + 1) or (SS[i + 1] <> List[i]) then
          SS.Insert(i + 1, List[i]);

    finally
      SS.EndUpdate;
    end;

    cbbDivideBy.ItemIndex := Max(0, SS.IndexOf(Old));


    SS := chklstFilterSubtypes.Items;
    if SS.Count = 0 then
      Old := ''
    else
      Old := SS[chklstFilterSubtypes.ItemIndex];

    SS.BeginUpdate;
    try
      for i := 0 to List.Count - 1 do
        if (SS.Count <= i) or (SS[i] <> List[i]) then
        begin
          SS.Insert(i, List[i]);
          chklstFilterSubtypes.Checked[i] := True;
        end;

    finally
      SS.EndUpdate;
    end;
    chklstFilterSubtypes.ItemIndex := Max(0, SS.IndexOf(Old));

  finally
    FreeAndNil(List);
  end;
end;

procedure TfrmLogFile.UpdateDivision(const AFirstIndex : integer);
var
  Cursor : TCursor;
  i, Idx : integer;
  R : TLogRecord;
  VWF : TfrmLogView;
begin
  Cursor := Screen.Cursor;

  Screen.Cursor := crHourGlass;
  try

    LockWindowUpdate(Handle);
    try
      if AFirstIndex > 0 then
        Idx := Views.Count - 1
      else
        Idx := -1;

      VWF := nil;
      for i := AFirstIndex to LogRecords.Count - 1 do
      begin
        if Application.Terminated then
          Break;

        R := LogRecords[i];

        if not Assigned(VWF) or AnsiSameText(R.SubType, FDivideSubType) then
        begin
          Inc(Idx);
          VWF := Views.AddOrGet(Idx, R.PutDate);
          VWF.LogRecords.Clear;
        end;

        VWF.LogRecords.Add(R);
      end;

      Views.Count := Idx + 1;
      cbbViews.ItemIndex := Views.Count - 1;
      cbbViews.Enabled := Views.Count > 1;
      cbbViews.OnChange(cbbViews);
    finally
      LockWindowUpdate(0);
    end;

  finally
    Screen.Cursor := Cursor;
  end;

end;

{$ENDREGION}


end.
