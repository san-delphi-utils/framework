object fmMain: TfmMain
  Left = 0
  Top = 0
  Caption = 'San.LogsViewer'
  ClientHeight = 541
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mmn
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object tbcLogs: TTabControl
    Left = 0
    Top = 0
    Width = 784
    Height = 522
    Align = alClient
    OwnerDraw = True
    TabOrder = 0
    OnChange = tbcLogsChange
  end
  object sb: TStatusBar
    Left = 0
    Top = 522
    Width = 784
    Height = 19
    Panels = <
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object pnlEmpty: TPanel
    Left = 288
    Top = 120
    Width = 185
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Caption = 'No data'
    TabOrder = 2
  end
  object mmn: TMainMenu
    Left = 16
    Top = 16
    object miFile: TMenuItem
      Caption = '&File'
      object miOpen: TMenuItem
        Action = actOpen
      end
      object miExitSep: TMenuItem
        Caption = '-'
      end
      object miExit: TMenuItem
        Action = actExit
      end
    end
  end
  object actlst: TActionList
    Left = 64
    Top = 16
    object actOpen: TAction
      Category = 'File'
      Caption = '&Open'
      ShortCut = 16463
      OnExecute = actOpenExecute
    end
    object actExit: TAction
      Category = 'File'
      Caption = 'E&xit'
      OnExecute = actExitExecute
    end
  end
  object dlgOpen: TOpenDialog
    DefaultExt = '.log'
    Filter = 'Log file (*.log)|*.log|Text file (*.txt)|*.txt|Any file|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofShareAware, ofEnableSizing]
    Left = 112
    Top = 16
  end
  object tmrRead: TTimer
    OnTimer = tmrReadTimer
    Left = 160
    Top = 16
  end
end
