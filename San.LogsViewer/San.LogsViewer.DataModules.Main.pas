{*******************************************************}
{                                                       }
{       LogsViewer                                      }
{                                                       }
{       Главный модуль данных                           }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   LogsViewer
///   Главный модуль данных
/// </summary>
unit San.LogsViewer.DataModules.Main;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'Winapi'}
  Winapi.Windows, Winapi.Messages,
  {$ENDREGION}

  {$REGION 'System'}
  System.SysUtils, System.Variants, System.Classes, System.Generics.Collections,
  System.IniFiles, System.Actions, System.StrUtils,
  {$ENDREGION}

  {$REGION 'Vcl'}
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ImgList,
  Vcl.ActnList, Vcl.Menus,
  {$ENDREGION}

  System.ImageList, System.UITypes,

  {$REGION 'San'}
  San,
  {$ENDREGION}

  {$REGION 'San.LogsViewer'}
  San.LogsViewer.Types
  {$ENDREGION}
  ;
{$ENDREGION}

type
  /// <summary>
  ///   Главный модуль данных
  /// </summary>
  TdmMain = class(TDataModule)
    il16x16: TImageList;

  public
    procedure Draw16x16ByImgIdx(const ACanvas : TCanvas; const AImageIndex : TImageIndex);

  end;

var
  dmMain: TdmMain;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{$REGION 'TdmMain'}
procedure TdmMain.Draw16x16ByImgIdx(const ACanvas: TCanvas;
  const AImageIndex: TImageIndex);
var
  BMP : TBitmap;
begin
  BMP := TBitmap.Create;
  try
    BMP.Width   := dmMain.il16x16.Width;
    BMP.Height  := dmMain.il16x16.Height;
    BMP.Canvas.FillRect(BMP.Canvas.ClipRect);
    dmMain.il16x16.Draw(BMP.Canvas, 0, 0, AImageIndex);
    ACanvas.Draw(0, 0, BMP);
  finally
    FreeAndNil(BMP);
  end;
end;
{$ENDREGION}

end.
