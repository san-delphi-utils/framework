program San.LogsViewer;

uses
  Vcl.Forms,
  San.LogsViewer.Forms.Main in 'San.LogsViewer.Forms.Main.pas' {fmMain},
  San.LogsViewer.Frames.LogFile in 'San.LogsViewer.Frames.LogFile.pas' {frmLogFile: TFrame},
  San.LogsViewer.Types in 'San.LogsViewer.Types.pas',
  San.LogsViewer.Frames.LogView in 'San.LogsViewer.Frames.LogView.pas' {frmLogView: TFrame},
  San.LogsViewer.Controls in 'San.LogsViewer.Controls.pas',
  San.LogsViewer.DataModules.Main in 'San.LogsViewer.DataModules.Main.pas' {dmMain: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmMain, fmMain);
  Application.CreateForm(TdmMain, dmMain);
  Application.Run;
end.
