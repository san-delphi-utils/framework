{*******************************************************}
{                                                       }
{       Работа с объектами.                             }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2013-2016         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Работа с объектами.
/// </summary>
unit San.Objs;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  System.SysUtils, System.Classes, System.TypInfo, System.Generics.Collections,

  San.Messages,
  San.Intfs.Basic;
{$ENDREGION}
{$ENDIF}

{$IFNDEF USE_ALIASES}
type
  /// <summary>
  ///   Доступ к методам работы с объектами.
  /// </summary>
  Api = record
  strict private

  public
//    class constructor Create();
//    class destructor Destroy();

    /// <summary>
    ///   Аналог <c>FreeAndNil</c>, но вместо освобождения вызвает <c>_Release</c>
    /// </summary>
    /// <returns>
    ///   <c>-1</c> для <c>Obj=nil</c>, иначе результат <c>_Release</c>
    /// </returns>
    /// <remarks>
    ///
    /// </remarks>
    class function ReleaseAndNil(var Obj) : integer; static;

    /// <summary>
    ///   Аналог <c>FreeAndNil</c>, снимающий ссылку на владельца.
    /// </summary>
    class procedure FreeAndNilOwned(var Obj); static;

    /// <summary>
    ///   Аналог <c>FreeAndNilOwned</c>, но действует только при владении данным объектом.
    /// </summary>
    class procedure FreeAndNilOwnedIf(const AOwner : TObject; var Obj); static;

    /// <summary>
    ///   Получить описание объекта.
    /// </summary>
    /// <param name="AObj">
    ///   Объект.
    /// </param>
    /// <param name="ASeporator">
    ///   Разделитель.
    /// </param>
    class function Description(const AObj : TObject; const ASeporator : string = '; ') : string; overload; static;

    /// <summary>
    ///   Получить описание объекта.
    /// </summary>
    /// <param name="AObj">
    ///   Объект в виде указателя.
    /// </param>
    /// <param name="ASeporator">
    ///   Разделитель.
    /// </param>
    class function Description(const AObj : Pointer; const ASeporator : string = '; ') : string; overload; static; inline;

    /// <summary>
    ///   Проверить класс типового параметра TTest на наследование от TStandard
    /// </summary>
    class procedure ValidateGen<TTest, TStandard : class>(); static;

    /// <summary>
    ///   Проверить класс типового параметра TTest на наследование от TStandard. Без типизации класса генерика
    /// </summary>
    class procedure ValidateGenRaw<TTest; TStandard : class>(); static;

    /// <summary>
    ///   Преведение типов объекта с информативной ошибкой
    /// </summary>
    /// <param name="AObj">
    ///   Приводимый объект
    /// </param>
    /// <param name="ACanNil">
    ///   Возможность приводить <c>nil</c>
    /// </param>
    class function Cast<T : class>(const AObj : TObject; const ACanNil : boolean = False) : T; static;

    /// <summary>
    ///   Преведение типов объекта с информативной ошибкой без типизации класса генерика
    /// </summary>
    /// <param name="AObj">
    ///   Приводимый объект
    /// </param>
    /// <param name="ACanNil">
    ///   Возможность приводить <c>nil</c>
    /// </param>
    class function CastRaw<T>(const AObj : TObject; const ACanNil : boolean = False) : T; static;

    /// <summary>
    ///   Преведение типов динамических массивов объектов
    /// </summary>
    /// <param name="AIn">
    ///   Входной массив
    /// </param>
    class function CastArray<TIn, TOut : class>(const AIn : TArray<TIn>) : TArray<TOut>; overload; static;

    /// <summary>
    ///   Преведение типов динамических массивов объектов
    /// </summary>
    /// <param name="AIn">
    ///   Входной массив
    /// </param>
    /// <param name="AOut">
    ///   Выходной массив
    /// </param>
    class procedure CastArray<TIn, TOut : class>(const AIn : TArray<TIn>; out AOut : TArray<TOut>); overload; static;

    /// <summary>
    ///   Преведение типов динамических массивов объектов
    /// </summary>
    /// <param name="AIn">
    ///   Входной массив
    /// </param>
    /// <param name="AOut">
    ///   Выходной массив
    /// </param>
    class procedure CastArrayRaw(const AIn; out AOut); static;

    /// <summary>
    ///   Очистить массив объектов, вроде <c>TArray&lt;TObject&gt;</c>
    /// </summary>
    /// <param name="AArray">
    ///   Входной массив
    /// </param>
    class procedure ClearObjsArray(var AArray); static;
  end;

implementation

type
  TInterfacedObjectHack = class(TInterfacedObject);

{$REGION 'Api'}
class function Api.ReleaseAndNil(var Obj) : integer;
var
  Temp: TInterfacedObjectHack;
begin
  Temp := TInterfacedObjectHack(Obj);

  if not Assigned(Temp) then
    Exit(-1);

  if not (Temp is TInterfacedObject) then
    raise ENotSupportedException.CreateResFmt(@sClassXXXNotInheritsFromYYYFmt, [Temp.UnitName, Temp.ClassName, TInterfacedObject.UnitName, TInterfacedObject.ClassName]);

  Pointer(Obj) := nil;
  Result := Temp._Release();
end;

class procedure Api.FreeAndNilOwned(var Obj);
var
  Temp: TObject;
  OO : IOwnedObj;
begin
  Temp := TObject(Obj);

  if Assigned(Temp) then
  begin

    if Supports(Temp, IOwnedObj, OO) then
    begin
      OO.Owner := nil;
      OO := nil;
    end;

    Pointer(Obj) := nil;
    Temp.Free;
  end;
end;

class procedure Api.FreeAndNilOwnedIf(const AOwner: TObject; var Obj);
var
  Temp: TObject;
  OO : IOwnedObj;
begin
  Temp := TObject(Obj);

  if Supports(Temp, IOwnedObj, OO) and (AOwner = OO.Owner) then
  begin
    OO.Owner := nil;
    OO := nil;

    Pointer(Obj) := nil;
    Temp.Free;
  end;
end;

class function Api.Description(const AObj : TObject; const ASeporator: string): string;
var
  D : IDescribed;
begin

  if not Assigned(AObj) then
    Exit('nil');

  Result := Format('ClassName: %s%sToString: %s', [AObj.ClassName, ASeporator, AObj.ToString()]);

  if AObj is TComponent then
    Result := Result + Format('%sComponentName: %s', [ASeporator, TComponent(AObj).Name]);

  if Supports(AObj, IDescribed, D) then
    Result := Result + Format('%sDescription: %s', [ASeporator, D.Description]);
end;

class function Api.Description(const AObj: Pointer;
  const ASeporator: string): string;
begin
  Result := Description(TObject(AObj), ASeporator);
end;

class procedure Api.ValidateGen<TTest, TStandard>;
var
  Test, Standard : TClass;
begin
  Test      := PTypeInfo(TypeInfo(TTest    ))^.TypeData^.ClassType;
  Standard  := PTypeInfo(TypeInfo(TStandard))^.TypeData^.ClassType;

  if not Test.InheritsFrom(Standard) then
    raise ENotSupportedException.CreateResFmt(@s_San_ClassMustBeInheritedFromFmt, [Test.ClassName, Standard.ClassName]);
end;

class procedure Api.ValidateGenRaw<TTest, TStandard>;
var
  TI : PTypeInfo;
  Test, Standard : TClass;
begin
  TI := TypeInfo(TTest);
  if TI^.Kind <> tkClass then
    raise ENotSupportedException.CreateResFmt(@s_San_TypeParamMustBeClassFmt, [TI^.NameFld.ToString, GetEnumName(TypeInfo(TTypeKind), Ord(TI^.Kind))]);

  Test      := TI^.TypeData^.ClassType;
  Standard  := PTypeInfo(TypeInfo(TStandard))^.TypeData^.ClassType;

  if not Test.InheritsFrom(Standard) then
    raise ENotSupportedException.CreateResFmt(@s_San_ClassMustBeInheritedFromFmt, [Test.ClassName, Standard.ClassName]);
end;

class function Api.Cast<T>(const AObj: TObject; const ACanNil : boolean): T;
var
  TI : PTypeInfo;
begin
  if not Assigned(AObj) and ACanNil then
    Exit(Default(T));

  TI := TypeInfo(T);

  if not Assigned(AObj) then
    raise EConvertError.CreateResFmt(@sNilNotInheritsFromYYYFmt, [TI^.TypeData^.ClassType.UnitName, TI^.TypeData^.ClassType.ClassName])

  else
  if not AObj.InheritsFrom(TI^.TypeData^.ClassType) then
    raise EConvertError.CreateResFmt(@sClassXXXNotInheritsFromYYYFmt, [AObj.UnitName, AObj.ClassName, TI^.TypeData^.ClassType.UnitName, TI^.TypeData^.ClassType.ClassName])

  else
    Result := T(AObj);
end;

class function Api.CastRaw<T>(const AObj: TObject; const ACanNil: boolean): T;
var
  TI : PTypeInfo;
begin
  TI := TypeInfo(T);
  if TI^.Kind <> tkClass then
    raise ENotSupportedException.CreateResFmt(@s_San_TypeParamMustBeClassFmt, [TI^.NameFld.ToString, GetEnumName(TypeInfo(TTypeKind), Ord(TI^.Kind))]);

  if not Assigned(AObj) and ACanNil then
    Exit(Default(T));

  if not Assigned(AObj) then
    raise EConvertError.CreateResFmt(@sNilNotInheritsFromYYYFmt, [TI^.TypeData^.ClassType.UnitName, TI^.TypeData^.ClassType.ClassName])

  else
  if not AObj.InheritsFrom(TI^.TypeData^.ClassType) then
    raise EConvertError.CreateResFmt(@sClassXXXNotInheritsFromYYYFmt, [AObj.UnitName, AObj.ClassName, TI^.TypeData^.ClassType.UnitName, TI^.TypeData^.ClassType.ClassName])

  else
    PObject(@Result)^ := AObj;
end;

class function Api.CastArray<TIn, TOut>(const AIn: TArray<TIn>): TArray<TOut>;
begin
  CastArrayRaw(AIn, Result);
end;

class procedure Api.CastArray<TIn, TOut>(const AIn: TArray<TIn>;
  out AOut: TArray<TOut>);
begin
  CastArrayRaw(AIn, AOut);
end;

class procedure Api.CastArrayRaw(const AIn; out AOut);
begin
  TArray<TObject>(AOut) := TArray<TObject>(AIn);
end;

class procedure Api.ClearObjsArray(var AArray);
var
  Arr : TArray<TObject>;
  i : integer;
  Item : TObject;
begin
  Arr := TArray<TObject>(AArray);
  for i := High(Arr) downto Low(Arr) do
  begin
    Item := Arr[i];
    System.Delete(Arr, i, 1);
    FreeAndNil(Item);
  end;

  TArray<TObject>(AArray) := nil;
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
