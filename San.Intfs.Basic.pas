{*******************************************************}
{                                                       }
{       Базовые интерфейсы.                             }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2013-2018         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Работа с интерфейсами.
/// </summary>
unit San.Intfs.Basic;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  System.SysUtils;
{$ENDREGION}
{$ENDIF}

type
  /// <summary>
  ///   Интерфейс ссылки на объект.
  /// </summary>
  IObjRef =
  {$IFDEF USE_ALIASES}
    San.Intfs.Basic.IObjRef;
  {$ELSE}
    interface(IInterface)
  ['{22A71EBD-9658-4F67-8E54-9D5A4486E27B}']
    {$REGION 'Gets&Sets'}
    function GetObjRef : TObject;
    {$ENDREGION}

    /// <summary>
    ///   Интерфейс ссылки на объект.
    /// </summary>
    property ObjRef : TObject  read GetObjRef;
  end;
  {$ENDIF}

  /// <summary>
  ///   Интерфейс позволяет получить ссылку на владеющий объект,
  ///   т.е. объект, ответсвенный за разрушение.
  /// </summary>
  IOwnedObj =
  {$IFDEF USE_ALIASES}
    San.Intfs.Basic.IOwnedObj;
  {$ELSE}
    interface(IInterface)
  ['{BBA6DAAB-1596-4E0E-9523-B222D14EDD4A}']
    {$REGION 'Gets&Sets'}
    function GetOwner : TObject;
    procedure SetOwner(const AValue : TObject);
    {$ENDREGION}

    /// <summary>
    ///   Ссылка на объект, ответственный за разрушение.
    /// </summary>
    property Owner : TObject  read GetOwner  write SetOwner;
  end;
  {$ENDIF}

  /// <summary>
  ///   Интерфейс позволяет получить уведомление о разрушении объекта,
  ///   который ссылается на имплементацию, как на владельца.
  /// </summary>
  INotifyOwnedObjDestroying =
  {$IFDEF USE_ALIASES}
    San.Intfs.Basic.INotifyOwnedObjDestroying;
  {$ELSE}
    interface(IInterface)
  ['{F3E177D7-7A6D-4ABE-9697-EBA981CD4BBB}']
    /// <summary>
    ///   Оповещение о разрушении дочернего объекта.
    /// </summary>
    procedure NotifyOwnedObjDestroying(const AOwnedObj : TObject);
  end;
  {$ENDIF}

  /// <summary>
  ///   Интерфейс предоставляет текстовое описание
  ///   для внешнего использования.
  /// </summary>
  IDescribed =
  {$IFDEF USE_ALIASES}
    San.Intfs.Basic.IDescribed;
  {$ELSE}
    interface(IInterface)
  ['{B1B38DF3-2DD5-43DE-9587-7D037B80719A}']
    {$REGION 'Gets&Sets'}
    function GetDescription : string;
    {$ENDREGION}

    /// <summary>
    ///   Текстовое описание.
    /// </remarks>
    property Description : string  read GetDescription;
  end;
  {$ENDIF}

  /// <summary>
  ///   Интерфейс прогресса некоторого процесса.
  /// </summary>
  IProgress =
  {$IFDEF USE_ALIASES}
    San.Intfs.Basic.IProgress;
  {$ELSE}
    interface(IInterface)
  ['{78E19F06-0FA1-42EB-A352-D19F02E55441}']
    {$REGION 'Gets&Sets'}
    function GetValue : integer;
    procedure SetValue(const AValue : integer);
    function GetMaxValue : integer;
    procedure SetMaxValue(const AValue : integer);
    function GetTitle : string;
    procedure SetTitle(const AValue : string);
    function GetStage : string;
    procedure SetStage(const AValue : string);
    function GetVisible : boolean;
    procedure SetVisible(const AValue : boolean);
    {$ENDREGION}

    /// <summary>
    ///   Шаг прогресса увеличивающий Value на AStep.
    /// </summary>
    /// <param name="AStep">
    ///   Размер шага.
    /// </param>
    /// <returns>
    ///   <c>True</c> — процесс продолжается / <c>False</c> — процесс прерван
    /// </returns>
    function Next(const AStep : integer = 1) : boolean;

    /// <summary>
    ///   Значение прогресса.
    /// </remarks>
    property Value : integer  read GetValue  write SetValue;

    /// <summary>
    ///   Максимальное значение прогресса.
    /// </remarks>
    property MaxValue : integer  read GetMaxValue  write SetMaxValue;

    /// <summary>
    ///   Заголовок (если он есть).
    /// </remarks>
    property Title : string  read GetTitle  write SetTitle;

    /// <summary>
    ///   Текстовое описание этапа.
    /// </remarks>
    property Stage : string  read GetStage  write SetStage;

    /// <summary>
    ///   Видимость
    /// </remarks>
    property Visible : boolean  read GetVisible  write SetVisible;
  end;
  {$ENDIF}

  /// <summary>
  ///   Безопасный интерфейс прогресса некоторого процесса.
  /// </summary>
  TProgress =
  {$IFDEF USE_ALIASES}
    San.Intfs.Basic.TProgress;
  {$ELSE}
    record
  strict private
    function GetValue : integer;
    procedure SetValue(const AValue : integer);
    function GetMaxValue : integer;
    procedure SetMaxValue(const AValue : integer);
    function GetTitle : string; inline;
    procedure SetTitle(const AValue : string);
    function GetStage : string;
    procedure SetStage(const AValue : string);
    function GetVisible : boolean;
    procedure SetVisible(const AValue : boolean);

  private
    FInnerData : IProgress;

  public
    class operator Implicit(A : IProgress) : TProgress;
    class operator Implicit(A : TProgress) : IProgress;

    /// <summary>
    ///   Шаг прогресса увеличивающий Value на AStep.
    /// </summary>
    /// <param name="AStep">
    ///   Размер шага.
    /// </param>
    /// <returns>
    ///   <c>True</c> — процесс продолжается / <c>False</c> — процесс прерван
    /// </returns>
    function Next(const AStep : integer = 1) : boolean;

    /// <summary>
    ///   Значение прогресса.
    /// </remarks>
    property Value : integer  read GetValue  write SetValue;

    /// <summary>
    ///   Максимальное значение прогресса.
    /// </remarks>
    property MaxValue : integer  read GetMaxValue  write SetMaxValue;

    /// <summary>
    ///   Заголовок (если он есть).
    /// </remarks>
    property Title : string  read GetTitle  write SetTitle;

    /// <summary>
    ///   Текстовое описание этапа.
    /// </remarks>
    property Stage : string  read GetStage  write SetStage;

    /// <summary>
    ///   Видимость
    /// </remarks>
    property Visible : boolean  read GetVisible  write SetVisible;
  end;
  {$ENDIF}


{$IFNDEF USE_ALIASES}
implementation

{$REGION 'TProgress'}
class operator TProgress.Implicit(A: TProgress): IProgress;
begin
  Result := A.FInnerData;
end;

class operator TProgress.Implicit(A: IProgress): TProgress;
begin
  Result.FInnerData := A;
end;

function TProgress.GetMaxValue: integer;
begin
  if Assigned(FInnerData) then
    Result := FInnerData.MaxValue
  else
    Result := 0;
end;

function TProgress.GetStage: string;
begin
  if Assigned(FInnerData) then
    Result := FInnerData.Stage
  else
    Result := '';
end;

function TProgress.GetTitle: string;
begin
  if Assigned(FInnerData) then
    Result := FInnerData.Title
  else
    Result := '';
end;

function TProgress.GetValue: integer;
begin
  if Assigned(FInnerData) then
    Result := FInnerData.Value
  else
    Result := 0;
end;

function TProgress.GetVisible: boolean;
begin
  if Assigned(FInnerData) then
    Result := FInnerData.Visible
  else
    Result := False;
end;

function TProgress.Next(const AStep: integer): boolean;
begin
  if Assigned(FInnerData) then
    Result := FInnerData.Next(AStep)
  else
    Result := False;
end;

procedure TProgress.SetMaxValue(const AValue: integer);
begin
  if Assigned(FInnerData) then
    FInnerData.MaxValue := AValue;
end;

procedure TProgress.SetStage(const AValue: string);
begin
  if Assigned(FInnerData) then
    FInnerData.Stage := AValue;
end;

procedure TProgress.SetTitle(const AValue: string);
begin
  if Assigned(FInnerData) then
    FInnerData.Title := AValue;
end;

procedure TProgress.SetValue(const AValue: integer);
begin
  if Assigned(FInnerData) then
    FInnerData.Value := AValue;
end;

procedure TProgress.SetVisible(const AValue: boolean);
begin
  if Assigned(FInnerData) then
    FInnerData.Visible := AValue;
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
