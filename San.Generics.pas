{*******************************************************}
{                                                       }
{       Геннерики.                                      }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2014-2018         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Работа с потоками.
/// </summary>
unit San.Generics;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  System.SysUtils, System.Generics.Defaults, System.Generics.Collections,

  San.UpperString;
{$ENDREGION}

{$ENDIF}

type
  /// <summary>
  ///   Справочник, с ключевой строкой не зависящей от регистра.
  /// </summary>
  TTextObjectDictionary<TValue : class> = class(TObjectDictionary<TUpperString, TValue>)
  strict private

  public
    constructor Create(ACapacity: Integer = 0); overload;
    constructor Create(Collection: TEnumerable<TPair<TUpperString,TValue>>); overload;

    procedure Add(const Key: TUpperString; const Value: TValue);

  end;

  /// <summary>
  ///   Список интерфейсных объектов с учетом ссылок.
  /// </summary>
  TIntfObjList<T : class> = class(TList<T>)
  strict private type
    TInterfacedObjectHack = class(TInterfacedObject);
  protected
    procedure Notify(const Item: T; Action: TCollectionNotification); override;

  end;

{$IFNDEF USE_ALIASES}
implementation

{$I San.Generics.Impl.inc}

end{$WARNINGS OFF}.
{$ENDIF}
