{*******************************************************}
{                                                       }
{       SANDebugSvc                                     }
{                                                       }
{       Фрейм эксперта отладки служб.                   }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit San.Experts.DebugSvc.frameDS;
{$I ..\..\shared\San.Experts.inc}
interface

uses
  ToolsApi, DesignIntf,

  WinApi.Windows, WinApi.Messages, WinApi.WinSvc,

  System.SysUtils, System.Variants, System.Classes, System.INIFiles,
  System.Actions, System.Win.Registry,

  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ToolWin, Vcl.ComCtrls,
  Vcl.ImgList, Vcl.ActnList, Vcl.Menus, Vcl.SvcMgr, Vcl.StdCtrls,

  // After Vcl.ImgList
  System.UITypes, System.ImageList;

const
  MI_SAN_DEBUG_SVC_CAPTION  = 'Debug Windows Service';

  CM_LATE_REFRESH = WM_USER + 1;

type
  ESANDebugSvcError = class(Exception);

  ISvcRec = interface(IInterface)
    ['{B9A444A3-EF4C-46BF-8349-9E8A0D10B7CA}']
    {$REGION 'Gets&Sets'}
    function GetService : TService;
    function GetExecStr : string;
    function GetServiceStatus : DWORD;
    function GetPID : DWORD;
    {$ENDREGION}

    procedure Refresh;

    property Service : TService  read GetService;
    property ExecStr : string read GetExecStr;
    property ServiceStatus : DWORD read GetServiceStatus;
    property PID : DWORD  read GetPID;
  end;

  TAction = class(Vcl.ActnList.TAction)
  public
    function Execute: Boolean; override;
  end;

  TfrmDebugSvc = class(TFrame)
    ilSDS: TImageList;
    actlstSDS: TActionList;
    actAttache: TAction;
    actRun: TAction;
    actPause: TAction;
    actStop: TAction;
    actRestart: TAction;
    actKillProcess: TAction;
    actRefresh: TAction;
    lvServices: TListView;
    chkPauseAfterAttache: TCheckBox;
    actDetach: TAction;
    actInstall: TAction;
    actUninstall: TAction;
    procedure actRefreshExecute(Sender: TObject);
    procedure actAttacheExecute(Sender: TObject);
    procedure actRunExecute(Sender: TObject);
    procedure actStopExecute(Sender: TObject);
    procedure actRestartExecute(Sender: TObject);
    procedure actKillProcessExecute(Sender: TObject);
    procedure lvServicesSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure actAttacheUpdate(Sender: TObject);
    procedure actDetachExecute(Sender: TObject);
    procedure actDetachUpdate(Sender: TObject);
    procedure actInstallExecute(Sender: TObject);
    procedure actUninstallExecute(Sender: TObject);
  private
    FDockableForm : TObject;
    FProjectServices : IInterfaceList;
    function GetCurRec: ISvcRec;
    procedure CmAfterCreate(var Message : TMessage); message CM_LATE_REFRESH;

  protected
    procedure ReFillProjectServices;
    procedure RefreshActEnableds;
    procedure ValidateCurProject;
    procedure ExecBuildTargetProcess(const AParams : string);


  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    property CurRec : ISvcRec  read GetCurRec;
  end;

  TSANDebugSvcDockableForm = class(TInterfacedObject, INTACustomDockableForm,
    IOTANotifier, IOTAModuleNotifier, IOTAProjectNotifier)
  private
    FFrame : TfrmDebugSvc;
    FForm : TCustomForm;
    FProject : IOTAProject;
    FNotifireIndex : integer;

  protected
    {$REGION 'INTACustomDockableForm'}
    function GetCaption: string;
    function GetIdentifier: string;
    function GetFrameClass: TCustomFrameClass;
    procedure FrameCreated(AFrame: TCustomFrame);
    function GetMenuActionList: TCustomActionList;
    function GetMenuImageList: TCustomImageList;
    procedure CustomizePopupMenu(PopupMenu: TPopupMenu);
    function GetToolBarActionList: TCustomActionList;
    function GetToolBarImageList: TCustomImageList;
    procedure CustomizeToolBar(ToolBar: TToolBar);
    procedure SaveWindowState(Desktop: TCustomIniFile; const Section: string; IsProject: Boolean);
    procedure LoadWindowState(Desktop: TCustomIniFile; const Section: string);
    function GetEditState: TEditState;
    function EditAction(Action: TEditAction): Boolean;
    {$ENDREGION}

    {$REGION 'IOTANotifier'}
    procedure AfterSave;
    procedure BeforeSave;
    procedure Destroyed;
    procedure Modified;
    {$ENDREGION}

    {$REGION 'IOTAModuleNotifier'}
    function CheckOverwrite: Boolean;
    procedure ModuleRenamed(const NewName: string);
    {$ENDREGION}

    {$REGION 'IOTAProjectNotifier'}
    procedure IOTAProjectNotifier.ModuleRenamed = ProjectModuleRenamed;
    procedure ModuleAdded(const AFileName: string);
    procedure ModuleRemoved(const AFileName: string);
    procedure ProjectModuleRenamed(const AOldFileName, ANewFileName: string);
    {$ENDREGION}

    procedure RemoveProjectNotifer;

  public
    constructor Create;
    destructor Destroy; override;
    function CreateDockableForm : TCustomForm;

    property Frame : TfrmDebugSvc  read FFrame;
    property Form : TCustomForm  read FForm;
  end;

implementation

{$R *.dfm}

uses
  TlHelp32;

{$I ..\..\shared\San.ToolsApiEx.Impl.inc}

const
  FRAME_SAN_DEBUG_SVC_NAME = '_8C9F63E24E1E4421976166B7CDC0C641';
  DCC_ExeOutput = 'DCC_ExeOutput';
//==============================================================================
function ReadServiceExecStrFromRegistry(const AServiceName : string) : string;
var
  Reg: TRegIniFile;
begin
  Reg := TRegIniFile.Create(KEY_ALL_ACCESS);
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    Result := Reg.ReadString('\SYSTEM\CurrentControlSet\Services\' + AServiceName, 'ImagePath', '');
  finally
    FreeAndNil(Reg);
  end;//t..f
end;

function SvcStateToStr(const AStste : DWORD) : string;
begin
  case AStste of
    SERVICE_STOPPED:
      Result := 'SERVICE_STOPPED';

    SERVICE_START_PENDING:
      Result := 'SERVICE_START_PENDING';

    SERVICE_STOP_PENDING:
      Result := 'SERVICE_STOP_PENDING';

    SERVICE_RUNNING:
      Result := 'SERVICE_RUNNING';

    SERVICE_CONTINUE_PENDING:
      Result := 'SERVICE_CONTINUE_PENDING';

    SERVICE_PAUSE_PENDING:
      Result := 'SERVICE_PAUSE_PENDING';

    SERVICE_PAUSED:
      Result := 'SERVICE_PAUSED';

  else
    Result := '';
  end;
end;
//==============================================================================

//==============================================================================
// TAction
function TAction.Execute: Boolean;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  try
    Screen.Cursor := crHourGlass;

    Result := inherited;

  finally
    Screen.Cursor := Cursor;
  end;
end;
//==============================================================================

type
  TSvcRec = class(TInterfacedObject, ISvcRec)
  private
    FService : TService;
    FExecStr : string;
    FServiceStatus : DWORD;
    FPID : DWORD;

  protected
    {$REGION 'ISvcRec'}
    function GetService : TService;
    function GetExecStr : string;
    function GetServiceStatus : DWORD;
    function GetPID : DWORD;
    procedure Refresh;
    {$ENDREGION}

  public
    constructor Create(const AService : TService);

  end;

{$REGION 'TfrmDebugSvc'}
procedure TfrmDebugSvc.actAttacheExecute(Sender: TObject);
var
  Rec : ISvcRec;
begin
  ValidateCurProject;

  actRun.Execute;

  Rec := CurRec;

  (BorlandIDEServices as IOTADebuggerServices).AttachProcess(Rec.PID, chkPauseAfterAttache.Checked, True);
end;

procedure TfrmDebugSvc.actAttacheUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not Assigned((BorlandIDEServices as IOTADebuggerServices).CurrentProcess);
end;

procedure TfrmDebugSvc.actDetachExecute(Sender: TObject);
begin
  (BorlandIDEServices as IOTADebuggerServices).CurrentProcess.Detach;
end;

procedure TfrmDebugSvc.actDetachUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned((BorlandIDEServices as IOTADebuggerServices).CurrentProcess);
end;

procedure TfrmDebugSvc.actKillProcessExecute(Sender: TObject);
var
  Process : DWORD;
begin
  try

    Process := OpenProcess(PROCESS_TERMINATE, False, CurRec.PID);
    if Process <> 0 then
      try

        if not TerminateProcess(Process, 0) then
          RaiseLastOSError;

      finally
        CloseHandle(Process)
      end
    else
      RaiseLastOSError;

  finally
    actRefresh.Execute;
  end;
end;

procedure TfrmDebugSvc.actRefreshExecute(Sender: TObject);

  procedure RefreshListView;
  var
    i, Index : integer;
    Rec : ISvcRec;
    LI : TListItem;
    Items : TListItems;
  begin
    Items := lvServices.Items;

    Items.BeginUpdate;
    try

      if Assigned(lvServices.Selected) then
        Index := lvServices.Selected.Index
      else
        Index := -1;

      Items.Clear;

      for i := 0 to FProjectServices.Count - 1 do
      begin
        Rec := FProjectServices[i] as ISvcRec;
        Rec.Refresh;

        LI := Items.Add;

        LI.Caption := Rec.Service.Name;
        LI.SubItems.Add(Rec.ExecStr);
        LI.SubItems.Add(SvcStateToStr(Rec.ServiceStatus));

        if Rec.PID = 0 then
          LI.SubItems.Add('')
        else
          LI.SubItems.Add(IntToStr(Rec.PID));
      end;

      if Items.Count > 0 then
        if Index < 0 then
          lvServices.Selected := Items[0]

        else
        if Items.Count <= Index then
          lvServices.Selected := Items[Items.Count - 1]

        else
          lvServices.Selected := Items[Index];

    finally
      lvServices.Items.EndUpdate;
    end;
  end;

begin
  RefreshListView;

  RefreshActEnableds;
end;

procedure TfrmDebugSvc.actRestartExecute(Sender: TObject);
begin
  actStop.Execute;
  actRun.Execute;
end;

procedure TfrmDebugSvc.actRunExecute(Sender: TObject);
var
  h_manager,h_svc: SC_Handle;
  svc_status: TServiceStatus;
  Temp: PChar;
  dwCheckPoint: DWord;
begin
  try
    svc_status.dwCurrentState := 1;
    h_manager := OpenSCManager(nil, nil, SC_MANAGER_CONNECT);

    if h_manager > 0 then
    try
      h_svc := OpenService(h_manager, PChar(CurRec.Service.Name), SERVICE_START or SERVICE_QUERY_STATUS);

      if h_svc > 0 then
      try
        temp := nil;

        if StartService(h_svc,0,temp) then
          if QueryServiceStatus(h_svc,svc_status) then
          begin

            while (SERVICE_RUNNING <> svc_status.dwCurrentState) do
            begin
              lvServices.Selected.SubItems[1] := SvcStateToStr(svc_status.dwCurrentState);
              lvServices.Repaint;

              dwCheckPoint := svc_status.dwCheckPoint;

              Sleep(svc_status.dwWaitHint);

              if (not QueryServiceStatus(h_svc,svc_status)) then
                break;

              if (svc_status.dwCheckPoint < dwCheckPoint) then
              begin
                // QueryServiceStatus не увеличивает dwCheckPoint
                break;
              end;
            end;
          end;

      finally
        CloseServiceHandle(h_svc);
      end

      else
        RaiseLastOSError;

    finally
      CloseServiceHandle(h_manager);
    end

    else
      RaiseLastOSError;

  finally
    actRefresh.Execute;
  end;

end;

procedure TfrmDebugSvc.actStopExecute(Sender: TObject);
type
  TSleepProc = procedure (const ASvcStatus : TServiceStatus);

  procedure NoDebugSleep(const ASvcStatus : TServiceStatus);
  begin
    Sleep(ASvcStatus.dwWaitHint);
  end;

  procedure DebugSleep(const ASvcStatus : TServiceStatus);
  const
    SLEEP_STEP = 250;
  var
    ElapsedTime : Cardinal;
    DebugServices : IOTADebuggerServices;
  begin

    if not Supports(BorlandIDEServices, IOTADebuggerServices, DebugServices) then
    begin
      NoDebugSleep(ASvcStatus);
      Exit;
    end;

    ElapsedTime := 0;

    repeat

      if Assigned(DebugServices.CurrentProcess) then
        DebugServices.ProcessDebugEvents;

      Sleep(SLEEP_STEP);
      Inc(ElapsedTime, SLEEP_STEP);
    until ElapsedTime >= ASvcStatus.dwWaitHint;

    Sleep(ASvcStatus.dwWaitHint);
  end;

var
  h_manager, h_svc   : SC_Handle;
  svc_status     : TServiceStatus;
  dwCheckPoint : DWord;
  DebugServices : IOTADebuggerServices;
  SleepProc : TSleepProc;
begin

  if Supports(BorlandIDEServices, IOTADebuggerServices, DebugServices)
  and Assigned(DebugServices.CurrentProcess)
  then
    @SleepProc := @DebugSleep

  else
    @SleepProc := @NoDebugSleep;

  try
    h_manager := OpenSCManager(nil, nil, SC_MANAGER_CONNECT);

    if h_manager > 0 then
    try
      h_svc := OpenService(h_manager, PChar(CurRec.Service.Name),
                           SERVICE_STOP or SERVICE_QUERY_STATUS);

      if h_svc > 0 then
      try

        if ControlService(h_svc, SERVICE_CONTROL_STOP, svc_status) then
        begin

          if(QueryServiceStatus(h_svc,svc_status))then
          begin
            while(SERVICE_STOPPED <> svc_status.dwCurrentState)do
            begin
              lvServices.Selected.SubItems[1] := SvcStateToStr(svc_status.dwCurrentState);
              lvServices.Repaint;

              dwCheckPoint := svc_status.dwCheckPoint;

              SleepProc(svc_status);

              if(not QueryServiceStatus(h_svc,svc_status))then
              begin
                // couldn't check status
                break;
              end;

              if(svc_status.dwCheckPoint < dwCheckPoint)then
                break;

            end;

          end;

        end;

      finally
        CloseServiceHandle(h_svc);
      end

      else
        RaiseLastOSError;

    finally
      CloseServiceHandle(h_manager);
    end

    else
      RaiseLastOSError;

  finally
    actRefresh.Execute;
  end;
end;

procedure TfrmDebugSvc.actInstallExecute(Sender: TObject);
begin
  ExecBuildTargetProcess('/Install ');
  ReFillProjectServices;
  PostMessage(Handle, CM_LATE_REFRESH, 0, 0);
end;

procedure TfrmDebugSvc.actUninstallExecute(Sender: TObject);
begin
  ExecBuildTargetProcess('/Uninstall');
  ReFillProjectServices;
  PostMessage(Handle, CM_LATE_REFRESH, 0, 0);
end;

procedure TfrmDebugSvc.CmAfterCreate(var Message: TMessage);
begin
  actRefresh.Execute;
end;

constructor TfrmDebugSvc.Create(AOwner: TComponent);

  procedure SetActionsHints;
  var
    i : integer;
  begin
    for i := 0 to actlstSDS.ActionCount - 1 do
      with actlstSDS.Actions[i] as TAction do
        Hint := Caption;
  end;

begin
  inherited;

  SetActionsHints;

  FProjectServices := TInterfaceList.Create;

  ReFillProjectServices;
end;

destructor TfrmDebugSvc.Destroy;
begin

  inherited;
end;

function TfrmDebugSvc.GetCurRec: ISvcRec;
begin
  Result := FProjectServices[lvServices.Selected.Index] as ISvcRec;
end;

procedure TfrmDebugSvc.lvServicesSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  if Selected and Assigned(Item) then
    (FProjectServices[Item.Index] as ISvcRec).Refresh;

  RefreshActEnableds;
end;

procedure TfrmDebugSvc.ReFillProjectServices;
var
  i : integer;
  OTAProject : IOTAProject;
  OTAModuleInfo : IOTAModuleInfo;
  OTAModule : IOTAModule;
  RootComponent : TComponent;
begin
  FProjectServices.Clear;

  OTAProject := GetActiveProject;
  if OTAProject = nil then
    Exit;

  for i := 0 to OTAProject.GetModuleCount - 1 do
  begin
    OTAModuleInfo := OTAProject.GetModule(i);

    if OTAModuleInfo.GetFormName = '' then
      Continue;

    OTAModule := OTAModuleInfo.OpenModule;

    if  GetModuleRootComponent(RootComponent, OTAModule)
    and RootComponent.InheritsFrom(TService)
    then
      FProjectServices.Add( TSvcRec.Create(RootComponent as TService));

  end;//for
end;

procedure TfrmDebugSvc.RefreshActEnableds;

  function GetTargetExists : boolean;
  var
    Project : IOTAProject;
  begin
    if not (FDockableForm is TSANDebugSvcDockableForm) then
      Exit(False);

    Project := (FDockableForm as TSANDebugSvcDockableForm).FProject;
    if not Assigned(Project) then
      Exit(False);

    if not Assigned(Project.ProjectOptions) then
      Exit(False);

    Result := FileExists(Project.ProjectOptions.TargetName);
  end;

var
  Rec : ISvcRec;
  TargetExists : boolean;
begin
  actAttache.Update;

  if not Assigned(lvServices.Selected) then
  begin
    actAttache.Enabled      := False;
    actRun.Enabled          := False;
    actPause.Enabled        := False;
    actStop.Enabled         := False;
    actKillProcess.Enabled  := False;
    actInstall.Enabled      := False;
    actUninstall.Enabled    := False;
  end

  else
  begin
    Rec := CurRec;

    TargetExists := GetTargetExists();

    actAttache.Enabled      := actAttache.Enabled and (Rec.ExecStr <> '');
    actRun.Enabled          := (Rec.ServiceStatus = SERVICE_STOPPED) or (Rec.ServiceStatus = SERVICE_PAUSED);
    actPause.Enabled        := (Rec.ServiceStatus = SERVICE_RUNNING);
    actStop.Enabled         := (Rec.ServiceStatus = SERVICE_RUNNING) or (Rec.ServiceStatus = SERVICE_PAUSED);
    actKillProcess.Enabled  := Rec.PID > 0;
    actInstall.Enabled      := TargetExists and (Rec.ExecStr  = '');
    actUnInstall.Enabled    := TargetExists and (Rec.ExecStr <> '');
  end;

  actRestart.Enabled  := actStop.Enabled;
end;

procedure TfrmDebugSvc.ValidateCurProject;
//var
//  Project : IOTAProject;
begin
//  Project := (FDockableForm as TSANDebugSvcDockableForm).FProject;
//
//  if not
//  (
//    Assigned(Project)
//    and
//    Assigned(GetActiveProject())
//    and
//    IsEqualGUID(Project.ProjectGUID, GetActiveProject.ProjectGUID)
//  )
//  then
//    raise ESANDebugSvcError.Create('Выбран другой проект');
//
end;

procedure TfrmDebugSvc.ExecBuildTargetProcess(const AParams: string);
var
  Project : IOTAProject;
  FullCMDStr : string;
  StartInfo    : TStartupInfo;
  ProcInfo     : TProcessInformation;
begin
  Project := (FDockableForm as TSANDebugSvcDockableForm).FProject;

  ZeroMemory(@StartInfo,SizeOf(StartInfo));
  StartInfo.cb := SizeOf(StartInfo);

  FullCMDStr := Trim(Project.ProjectOptions.TargetName + ' ' + AParams);
  //ShowMessage(FullCMDStr);
  if not Createprocess( nil, PChar(FullCMDStr), nil, nil, False,
                     CREATE_NEW_PROCESS_GROUP+NORMAL_PRIORITY_CLASS,
                     nil, nil, StartInfo, ProcInfo)
  then
    RaiseLastOSError();
  try

    WaitForSingleObject(ProcInfo.hProcess, INFINITE);

  finally
    CloseHandle(ProcInfo.hThread);
    CloseHandle(ProcInfo.hProcess);
  end;
end;
{$ENDREGION}

{$REGION 'TSANDebugSvcDockableForm'}
procedure TSANDebugSvcDockableForm.AfterSave;
begin
end;

procedure TSANDebugSvcDockableForm.BeforeSave;
begin
end;

function TSANDebugSvcDockableForm.CheckOverwrite: Boolean;
begin
  Result := False;
end;

constructor TSANDebugSvcDockableForm.Create;
begin
  inherited;

  FProject := GetActiveProject;
  FNotifireIndex := FProject.AddNotifier(Self);
end;

function TSANDebugSvcDockableForm.CreateDockableForm: TCustomForm;
begin
  FForm := (BorlandIDEServices as INTAServices).CreateDockableForm(Self);
  Result := FForm;
end;

procedure TSANDebugSvcDockableForm.CustomizePopupMenu(PopupMenu: TPopupMenu);
begin
end;

procedure TSANDebugSvcDockableForm.CustomizeToolBar(ToolBar: TToolBar);
begin
end;

destructor TSANDebugSvcDockableForm.Destroy;
begin
  RemoveProjectNotifer;

  FFrame  := nil;
  FForm   := nil;

  inherited;
end;

procedure TSANDebugSvcDockableForm.Destroyed;
begin
  RemoveProjectNotifer;
  (BorlandIDEServices as INTAServices).UnregisterDockableForm(Self);
end;

function TSANDebugSvcDockableForm.EditAction(Action: TEditAction): Boolean;
begin
  Result := False;
end;

procedure TSANDebugSvcDockableForm.FrameCreated(AFrame: TCustomFrame);
begin
  FFrame := AFrame as TfrmDebugSvc;
  FFrame.FDockableForm := Self;
end;

function TSANDebugSvcDockableForm.GetCaption: string;
begin
  Result := MI_SAN_DEBUG_SVC_CAPTION;
end;

function TSANDebugSvcDockableForm.GetEditState: TEditState;
begin
  Result := [];
end;

function TSANDebugSvcDockableForm.GetFrameClass: TCustomFrameClass;
begin
  Result := TfrmDebugSvc;
end;

function TSANDebugSvcDockableForm.GetIdentifier: string;
begin
  Result := FRAME_SAN_DEBUG_SVC_NAME;
end;

function TSANDebugSvcDockableForm.GetMenuActionList: TCustomActionList;
begin
  Result := FFrame.actlstSDS;
end;

function TSANDebugSvcDockableForm.GetMenuImageList: TCustomImageList;
begin
  Result := FFrame.ilSDS;
end;

function TSANDebugSvcDockableForm.GetToolBarActionList: TCustomActionList;
begin
  Result := FFrame.actlstSDS;
end;

function TSANDebugSvcDockableForm.GetToolBarImageList: TCustomImageList;
begin
  Result := FFrame.ilSDS;
end;

procedure TSANDebugSvcDockableForm.LoadWindowState(Desktop: TCustomIniFile;
  const Section: string);
begin
end;

procedure TSANDebugSvcDockableForm.Modified;
begin
end;

procedure TSANDebugSvcDockableForm.ModuleRenamed(const NewName: string);
begin
end;

procedure TSANDebugSvcDockableForm.ProjectModuleRenamed(const AOldFileName,
  ANewFileName: string);
begin
end;

procedure TSANDebugSvcDockableForm.RemoveProjectNotifer;
begin
  if Assigned(FProject) then
    FProject.RemoveNotifier(FNotifireIndex);

  FProject        := nil;
  FNotifireIndex  := -1;
end;

procedure TSANDebugSvcDockableForm.SaveWindowState(Desktop: TCustomIniFile;
  const Section: string; IsProject: Boolean);
begin
end;

procedure TSANDebugSvcDockableForm.ModuleAdded(const AFileName: string);
begin
end;

procedure TSANDebugSvcDockableForm.ModuleRemoved(const AFileName: string);
begin
end;
{$ENDREGION}

{$REGION 'TSvcRec'}
constructor TSvcRec.Create(const AService: TService);
begin
  inherited Create;

  FService := AService;

  FExecStr := ReadServiceExecStrFromRegistry(AService.Name);

  Refresh;
end;

function TSvcRec.GetExecStr: string;
begin
  Result := FExecStr;
end;

function TSvcRec.GetPID: DWORD;
begin
  Result := FPID;
end;

function TSvcRec.GetService: TService;
begin
  Result := FService;
end;

function TSvcRec.GetServiceStatus: DWORD;
begin
  Result := FServiceStatus;
end;

procedure TSvcRec.Refresh;

  procedure RefreshServiceStatus;
  var
    h_manager, h_service: SC_Handle;
    service_status     : TServiceStatus;
  begin
    FServiceStatus := SERVICE_STOPPED;

    h_manager := OpenSCManager(nil, nil, SC_MANAGER_CONNECT);
    if h_manager > 0 then
    try
      h_service := OpenService(h_manager, PChar(FService.Name), SERVICE_QUERY_STATUS);

      if h_service > 0 then
      try

        if(QueryServiceStatus(h_service, service_status)) then
          FServiceStatus := service_status.dwCurrentState;

      finally
        CloseServiceHandle(h_service);
      end;

    finally
      CloseServiceHandle(h_manager);
    end;

  end;

  procedure RefreshPID;
  var
    SnapProc : THandle;
    ProcEntry : TProcessEntry32;
  begin
    FPID := 0;
    SnapProc := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if SnapProc <> INVALID_HANDLE_VALUE then
      try
        ProcEntry.dwSize := SizeOf(TProcessEntry32);

        if Process32First(SnapProc, ProcEntry) then
          repeat

            if ProcEntry.szExeFile = String(AnsiString(ExtractFileName(FExecStr))) then  begin
              FPID := ProcEntry.th32ProcessID;
              Break;
            end;//t..f

          until not Process32Next(SnapProc, ProcEntry);

      finally
        CloseHandle(SnapProc);
      end//t..f

    else
      RaiseLastOSError;

  end;

begin
  RefreshServiceStatus;
  RefreshPID;
end;
{$ENDREGION}


end.
