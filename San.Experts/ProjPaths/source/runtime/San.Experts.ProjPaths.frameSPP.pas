{*******************************************************}
{                                                       }
{       SANProjPaths                                    }
{                                                       }
{       Фрейм пользовательского интерфейса настройки    }
{       путей.                                          }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit San.Experts.ProjPaths.frameSPP;
{$I ..\..\..\shared\San.Experts.inc}
interface

uses
  WinApi.Windows, WinApi.Messages, WinApi.ShellApi,

  System.SysUtils, System.Variants, System.Classes, System.StrUtils,
  System.Actions, System.Math, System.IOUtils,

  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ImgList,
  Vcl.ToolWin, Vcl.ActnList, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Clipbrd,

  VirtualTrees, Vcl.Buttons, System.ImageList;

const
  CM_SEARCH_IN_PROGRESS  = WM_USER + 1;
  CM_SEARCH_COMPLETE     = WM_USER + 2;
  CM_HIDE_OLDS           = WM_USER + 3;
  CM_EDIT_OLDS           = WM_USER + 4;

type
  TProcEvent = procedure of object;

  TfrmProjPaths = class;

  TSearchThread = class(TThread)
  private
    FFrameSPP : TfrmProjPaths;
    FStrings : TStrings;

  protected
    procedure StringsChange(Sender : TObject);
    procedure Execute; override;

  public
    constructor Create(const AFrameSPP : TfrmProjPaths);
    destructor Destroy; override;
  end;

  /// <summary>
  ///   Фрейм пользовательского интерфейса настройки путей.
  /// </summary>
  TfrmProjPaths = class(TFrame)
    vstSPP: TVirtualStringTree;
    ilIcons: TImageList;
    actlst: TActionList;
    actBreakSearch: TAction;
    tlbSPP: TToolBar;
    ilAct: TImageList;
    btnBreakSearch: TToolButton;
    actCollapseAll: TAction;
    actExpandAll: TAction;
    btnCollapseAll: TToolButton;
    btnExpandAll: TToolButton;
    trckbrLvlCollapse: TTrackBar;
    sepBreakSearch: TToolButton;
    bvlResults: TBevel;
    edResults: TEdit;
    sepTree: TToolButton;
    actCopyToClipboard: TAction;
    btnCopyToClipboard: TToolButton;
    pnProjPath: TPanel;
    lblProjPath: TLabel;
    bvlOldPaths: TBevel;
    sepCopyToClipboard: TToolButton;
    cbbBuildCfg: TComboBox;
    sepBuildCfg: TToolButton;
    cbbPlatform: TComboBox;
    actApplyResults: TAction;
    sepPlatform: TToolButton;
    btnApplyResults: TToolButton;
    pnResults: TPanel;
    lblResults: TLabel;
    sepCheckType: TToolButton;
    actChangeCheckType: TAction;
    btnChangeCheckType: TToolButton;
    edbtOldSearchPath: TButtonedEdit;
    vstOld: TVirtualStringTree;
    ilOld: TImageList;
    tlbOld: TToolBar;
    btnOldAdd: TToolButton;
    btnOldEdit: TToolButton;
    btnOldDelete: TToolButton;
    btnOldClear: TToolButton;
    sepApplyCancel: TToolButton;
    btnOldApply: TToolButton;
    btnOldCancel: TToolButton;
    actOldAdd: TAction;
    actOldEdit: TAction;
    actOldDelete: TAction;
    actOldClear: TAction;
    actOldApply: TAction;
    actOldCancel: TAction;
    actOldHide: TAction;
    btnOldHide: TToolButton;
    sepHide: TToolButton;
    pnlUpLevels: TPanel;
    lblUpLevels: TLabel;
    cbbUpLevels: TComboBox;
    procedure vstSPPGetText(Sender: TBaseVirtualTree; ANode: PVirtualNode;
      AColumn: TColumnIndex; ATextType: TVSTTextType; var ACellText: string);
    procedure vstSPPGetImageIndexEx(Sender: TBaseVirtualTree;
      ANode: PVirtualNode; AKind: TVTImageKind; AColumn: TColumnIndex;
      var AGhosted: Boolean; var AImageIndex: TImageIndex;
      var AImageList: TCustomImageList);
    procedure vstSPPChecked(Sender: TBaseVirtualTree; ANode: PVirtualNode);
    procedure actBreakSearchExecute(Sender: TObject);
    procedure actBreakSearchUpdate(Sender: TObject);
    procedure actCollapseAllExecute(Sender: TObject);
    procedure actExpandAllExecute(Sender: TObject);
    procedure trckbrLvlCollapseChange(Sender: TObject);
    procedure actCopyToClipboardExecute(Sender: TObject);
    procedure actCopyToClipboardUpdate(Sender: TObject);
    procedure edOldSearchPathsChange(Sender: TObject);
    procedure actChangeCheckTypeExecute(Sender: TObject);
    procedure vstOldExit(Sender: TObject);
    procedure edbtOldSearchPathRightButtonClick(Sender: TObject);
    procedure vstOldGetText(Sender: TBaseVirtualTree; ANode: PVirtualNode;
      AColumn: TColumnIndex; ATextType: TVSTTextType; var ACellText: string);
    procedure vstOldFocusChanged(Sender: TBaseVirtualTree; ANode: PVirtualNode;
      AColumn: TColumnIndex);
    procedure vstOldGetImageIndexEx(Sender: TBaseVirtualTree;
      ANode: PVirtualNode; AKind: TVTImageKind; AColumn: TColumnIndex;
      var AGhosted: Boolean; var AImageIndex: TImageIndex;
      var AImageList: TCustomImageList);
    procedure vstOldMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure actOldHasFocusedUpdate(Sender: TObject);
    procedure actOldClearUpdate(Sender: TObject);
    procedure actOldModifiedUpdate(Sender: TObject);
    procedure actOldAddExecute(Sender: TObject);
    procedure actOldEditExecute(Sender: TObject);
    procedure actOldDeleteExecute(Sender: TObject);
    procedure actOldClearExecute(Sender: TObject);
    procedure actOldApplyExecute(Sender: TObject);
    procedure actOldCancelExecute(Sender: TObject);
    procedure vstOldEditCancelled(Sender: TBaseVirtualTree;
      AColumn: TColumnIndex);
    procedure vstOldNewText(Sender: TBaseVirtualTree; ANode: PVirtualNode;
      AColumn: TColumnIndex; ANewText: string);
    procedure vstOldEdited(Sender: TBaseVirtualTree; ANode: PVirtualNode;
      AColumn: TColumnIndex);
    procedure vstOldKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure actOldHideExecute(Sender: TObject);
    procedure cbbUpLevelsChange(Sender: TObject);

  private
    FProjFileName : string;
    FСacheDepthProjFileName : integer;
    FPathsList, FOldSearchPaths : TStringList;

    /// <summary>
    ///   Событие ожидания загрузки и размещения фрейма (для потока FSearchThread).
    /// </summary>
    FLoadedAndPlacedEvent : THandle;

    /// <summary>
    ///   Поток поиска. Существует, только пока идет поиск.
    /// </summary>
    FSearchThread : TSearchThread;

    procedure SetProjFileName(const AValue: string);
    function GetProjPath: string;
    function GetProjDir: string;

    procedure CmSearchInProgress(var AMessage : TMessage); message CM_SEARCH_IN_PROGRESS;
    procedure CmSearchComplete(var AMessage : TMessage); message CM_SEARCH_COMPLETE;
    procedure CmHideOlds(var AMessage : TMessage); message CM_HIDE_OLDS;
    procedure CmEditOlds(var AMessage : TMessage); message CM_EDIT_OLDS;


    function GetResults: string;
    function GetOldSearchPaths: string;
    procedure SetOldSearchPaths(const AValue: string);


  protected
    procedure PathsListChange(Sender : TObject);
    procedure OldSearchPathsChange(Sender : TObject);
    procedure ReLoadPathsList;
    procedure UpdateIcons;
    procedure ReCalcMaxLevel;
    procedure ReCalcResults;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    /// <summary>
    ///   Загрузка и размещение закончены.
    /// </summary>
    procedure LoadedAndPlaced;

    /// <summary>
    ///   Выполнить метод с указанным курсором.
    /// </summary>
    /// <param name="AMethod">
    ///   Выполняемый метод.
    /// </param>
    /// <param name="ACursor">
    ///   Вид курсора.
    /// </param>
    class procedure HGC(const AMethod : TProcEvent; const ACursor : TCursor = crHourGlass);

    /// <summary>
    ///   Получить для пути с ".." путь без "..".
    /// </summary>
    /// <param name="ADottedPath">
    ///   Путь с "..".
    /// </param>
    /// <returns>
    ///   Путь без "..".
    /// </returns>
    class function NormalPath(ADottedPath : string) : string;

    /// <summary>
    ///   Перезаполнение дерева.
    /// </summary>
    procedure ReFillTree;

    /// <summary>
    ///   Перезаполнение отметок в дереве.
    /// </summary>
    procedure ReFillTreeChecks;

    /// <summary>
    ///   Полное имя файла проекта.
    /// </summary>
    property ProjFileName : string  read FProjFileName  write SetProjFileName;

    /// <summary>
    ///   Путь к файлу проекта, для которого идет настройка.
    /// </summary>
    /// <remarks>
    ///   С "\"
    /// </remarks>
    property ProjPath : string  read GetProjPath;

    /// <summary>
    ///   Папка, в которой лежит фал проекта.
    /// </summary>
    /// <remarks>
    ///   Без "\"
    /// </remarks>
    property ProjDir : string  read GetProjDir;

    /// <summary>
    ///   Старое значение путей поиска.
    /// </summary>
    property OldSearchPaths : string  read GetOldSearchPaths  write SetOldSearchPaths;

    /// <summary>
    ///   Результирующая строка путей поиска.
    /// </summary>
    property Results : string  read GetResults;

  end;

implementation

{$R *.dfm}

//==============================================================================
function CalcPathDepth(const AValue: string) : integer;
var
  i : integer;
begin
  Result := 0;
  for i := 0 to Length(AValue) do
    if AValue[i] = TPath.DirectorySeparatorChar then
      Inc(Result);
end;

function GetCurPathPart(const AOffset, ALength, APos : integer; const ADir : string) : string;
begin
  if APos = 0 then
    Result := Copy(ADir, AOffset, ALength - AOffset + 1)
  else
    Result := Copy(ADir, AOffset, APos - AOffset);
end;

//==============================================================================

//==============================================================================
{$REGION 'TSearchThread'}
constructor TSearchThread.Create(const AFrameSPP: TfrmProjPaths);
begin
  FFrameSPP := AFrameSPP;

  FStrings := TStringList.Create;

  inherited Create(False);
end;

destructor TSearchThread.Destroy;
begin
  FreeAndNil(FStrings);

  inherited;
end;

procedure TSearchThread.Execute;

  procedure WaitForLoadedAndPlacedEvent;
  begin

    repeat

      if Terminated then
        Exit;

      case WaitForSingleObject(FFrameSPP.FLoadedAndPlacedEvent, 500) of
        WAIT_OBJECT_0:
          Break;

        WAIT_TIMEOUT:
          Sleep(500);

      else
        RaiseLastOSError;
      end;

    until False;

  end;

  function CalcLevel(const ANextDir : string) : NativeInt;
  const
    SEP = '\';
  var
    Offset, LP, LN, PosP, PosN : integer;
    DirP, DirN, CurP, CurN : string;
  begin
    DirP    := FFrameSPP.ProjDir;
    DirN    := ANextDir;

    LP := Length(DirP);
    LN := Length(DirN);

    if (LN >= LP) and (Pos(DirP, DirN) = 1) then
      Exit(0);

    Result := -FFrameSPP.FСacheDepthProjFileName;
    Offset  := 1;

    repeat
      PosP := PosEx(SEP, DirP, Offset);
      PosN := PosEx(SEP, DirN, Offset);

      CurP := GetCurPathPart(Offset, LP, PosP, DirP);
      CurN := GetCurPathPart(Offset, LN, PosN, DirN);

      if not SameStr(CurP, CurN) then
        Break;

      Inc(Result);
      Offset := PosP + Length(SEP);
    until FALSE;

  end;

  procedure DownRecursion(const AAbsDir : string);
  var
    SR : TSearchRec;
    NextDir : string;
  begin

    if FindFirst(AAbsDir + '\*.*', faDirectory, SR) = 0 then
      try

        repeat

          if (SR.Attr and faDirectory <> faDirectory)
          or AnsiMatchText(SR.Name, ['.', '..'])
          then
            Continue;

          NextDir := AAbsDir + '\' + SR.Name;

          FStrings.AddObject(NextDir, TObject(CalcLevel(NextDir)));

          DownRecursion(NextDir);
        until Terminated or (FindNext(SR) > 0);

      finally
        FindClose(SR);
      end;

  end;

var
  Dir : string;
  i : integer;
begin
  NameThreadForDebugging(TfrmProjPaths.ClassName + '.' + Self.ClassName);

  WaitForLoadedAndPlacedEvent;

  Dir := FFrameSPP.ProjDir;
  for i := 1 to FFrameSPP.cbbUpLevels.ItemIndex do
    Dir := ExtractFileDir(Dir);

  DownRecursion(Dir);

  PostMessage(FFrameSPP.Handle, CM_SEARCH_COMPLETE, 0, 0);
end;

procedure TSearchThread.StringsChange(Sender: TObject);
begin
  PostMessage(FFrameSPP.Handle, CM_SEARCH_IN_PROGRESS, (Sender as TStrings).Count, 0);
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TfrmProjPaths'}
procedure TfrmProjPaths.actBreakSearchExecute(Sender: TObject);
begin
  FSearchThread.Terminate;
  FSearchThread.WaitFor;
  FreeAndNil(FSearchThread);
end;

procedure TfrmProjPaths.actBreakSearchUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(FSearchThread);
end;

procedure TfrmProjPaths.actChangeCheckTypeExecute(Sender: TObject);
var
  NodesEnum : TVTVirtualNodeEnumerator;
  Act : TAction;
begin
  Act := Sender as TAction;

  with Act do
    ImageIndex := ImageIndex + IfThen(Checked, 1, -1);

  vstSPP.BeginUpdate;
  try

    NodesEnum := vstSPP.Nodes().GetEnumerator;
    while NodesEnum.MoveNext do
      if actChangeCheckType.Checked and vstSPP.HasChildren[NodesEnum.Current] then
        vstSPP.CheckType[NodesEnum.Current] := ctTriStateCheckBox
      else
        vstSPP.CheckType[NodesEnum.Current] := ctCheckBox;


    if Act.Checked then
    begin
      NodesEnum := vstSPP.CheckedNodes().GetEnumerator;
      while NodesEnum.MoveNext do
      begin
        vstSPP.CheckState[NodesEnum.Current] := csCheckedPressed;
        vstSPP.CheckState[NodesEnum.Current] := csCheckedNormal;
      end;
    end;

  finally
    vstSPP.EndUpdate;
  end;

  ReCalcResults;
end;

procedure TfrmProjPaths.actCollapseAllExecute(Sender: TObject);
begin
  trckbrLvlCollapse.Position := 0;
  vstSPP.FullCollapse;
end;

procedure TfrmProjPaths.actCopyToClipboardExecute(Sender: TObject);
begin
  Clipboard.AsText := edResults.Text;
end;

procedure TfrmProjPaths.actCopyToClipboardUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Results <> '';
end;

procedure TfrmProjPaths.actExpandAllExecute(Sender: TObject);
begin
  trckbrLvlCollapse.Position := trckbrLvlCollapse.Max;
  vstSPP.FullExpand;
end;

procedure TfrmProjPaths.actOldAddExecute(Sender: TObject);
begin
  FOldSearchPaths.Add('');
  vstOld.FocusedNode := vstOld.RootNode^.LastChild;
  actOldEdit.Execute;
end;

procedure TfrmProjPaths.actOldApplyExecute(Sender: TObject);
begin
  OldSearchPaths := Copy(FOldSearchPaths.Text, 1, Length(FOldSearchPaths.Text) - 1);
end;

procedure TfrmProjPaths.actOldCancelExecute(Sender: TObject);
begin
  FOldSearchPaths.Text := OldSearchPaths;
  vstOld.Repaint;
end;

procedure TfrmProjPaths.actOldClearExecute(Sender: TObject);
begin
  FOldSearchPaths.Clear;
end;

procedure TfrmProjPaths.actOldClearUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := FOldSearchPaths.Count > 0;
end;

procedure TfrmProjPaths.actOldDeleteExecute(Sender: TObject);
begin
  FOldSearchPaths.Delete(vstOld.FocusedNode^.Index);
end;

procedure TfrmProjPaths.actOldEditExecute(Sender: TObject);
begin
  vstOld.EditNode(vstOld.FocusedNode, vstOld.FocusedColumn);
end;

procedure TfrmProjPaths.actOldHasFocusedUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(vstOld.FocusedNode);
end;


procedure TfrmProjPaths.actOldHideExecute(Sender: TObject);
begin
  Perform(CM_HIDE_OLDS, 0, 0);
end;

procedure TfrmProjPaths.actOldModifiedUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not SameStr(Copy(FOldSearchPaths.Text, 1, Length(FOldSearchPaths.Text) - 1), OldSearchPaths);
end;

procedure TfrmProjPaths.CmEditOlds(var AMessage: TMessage);
begin
  actOldEdit.Execute;
end;

procedure TfrmProjPaths.CmHideOlds(var AMessage: TMessage);
begin
  tlbOld.Hide;
  vstOld.Hide;
end;

procedure TfrmProjPaths.CmSearchComplete(var AMessage: TMessage);
begin
  Assert(Assigned(FSearchThread));

  FSearchThread.WaitFor;
  try

    FPathsList.BeginUpdate;
    try

      FPathsList.Assign(FSearchThread.FStrings);

      // Может быть иконки?

    finally
      FPathsList.EndUpdate;
    end;

    trckbrLvlCollapse.Position := trckbrLvlCollapse.Max;
  finally
    FreeAndNil(FSearchThread);
  end;
end;

procedure TfrmProjPaths.CmSearchInProgress(var AMessage: TMessage);
begin

end;

constructor TfrmProjPaths.Create(AOwner: TComponent);
begin
  FPathsList := TStringList.Create;
  FPathsList.OnChange := PathsListChange;

  FOldSearchPaths := TStringList.Create;
  FOldSearchPaths.LineBreak  := ';';
  FOldSearchPaths.OnChange   := OldSearchPathsChange;

  FLoadedAndPlacedEvent := CreateEvent(nil, True, False, nil);
  if FLoadedAndPlacedEvent = 0 then
    RaiseLastOSError;

  inherited;
end;

destructor TfrmProjPaths.Destroy;
begin
  FreeAndNil(FPathsList);

  FreeAndNil(FOldSearchPaths);

  CloseHandle(FLoadedAndPlacedEvent);

  inherited;
end;

procedure TfrmProjPaths.edbtOldSearchPathRightButtonClick(Sender: TObject);
var
  P : TPoint;
begin

  with edbtOldSearchPath do
    P := Parent.ClientToScreen(Point(Left, Top));

  Dec(P.Y, vstOld.Height);
  P := Self.ScreenToClient(P);

  vstOld.Left := P.X;
  vstOld.Top  := P.Y;

  vstOld.Width := edbtOldSearchPath.Width;

  tlbOld.Left     := vstOld.Left + vstOld.Width - tlbOld.Width;
  tlbOld.Top      := vstOld.Top  - tlbOld.Height;

  vstOld.Show;
  tlbOld.Show;

  vstOld.SetFocus;

end;

procedure TfrmProjPaths.edOldSearchPathsChange(Sender: TObject);
begin
  FOldSearchPaths.Text := (Sender as TCustomEdit).Text;

  HGC(ReCalcResults);
  HGC(ReFillTreeChecks);
end;

function TfrmProjPaths.GetOldSearchPaths: string;
begin
  Result := edbtOldSearchPath.Text;
end;

function TfrmProjPaths.GetProjDir: string;
begin
  Result := ExtractFileDir(FProjFileName);
end;

function TfrmProjPaths.GetProjPath: string;
begin
  Result := ExtractFilePath(FProjFileName);
end;

function TfrmProjPaths.GetResults: string;
begin
  Result := edResults.Text;
end;

class procedure TfrmProjPaths.HGC(const AMethod: TProcEvent; const ACursor: TCursor);
var
  Cusor : TCursor;
begin
  Cusor := Screen.Cursor;

  Screen.Cursor := ACursor;
  try

    AMethod;

  finally
    Screen.Cursor := Cusor;
  end;

end;

procedure TfrmProjPaths.LoadedAndPlaced;
begin
  SetEvent(FLoadedAndPlacedEvent);
end;

class function TfrmProjPaths.NormalPath(ADottedPath: string): string;
var
  CurPart : string;
  DDCounter : integer;
begin
  Result := '';
  DDCounter := 0;

  repeat
    CurPart      := ExtractFileName(ADottedPath);
    ADottedPath  := ExtractFileDir(ADottedPath);

    if SameStr(CurPart, '..') then
      Inc(DDCounter)

    else
    if DDCounter > 0 then
      Dec(DDCounter)

    else
    if Result = '' then
      Result := CurPart
    else
      Result := CurPart + '\' + Result;

  until  (ADottedPath = '') or (ADottedPath = ExtractFileDrive(ADottedPath));

  Result := ADottedPath + '\' + Result;
end;

procedure TfrmProjPaths.OldSearchPathsChange(Sender: TObject);
begin
  vstOld.RootNodeCount := (Sender as TStrings).Count;
end;

procedure TfrmProjPaths.PathsListChange(Sender: TObject);
begin
  HGC(UpdateIcons);
  HGC(ReFillTree);
end;

procedure TfrmProjPaths.ReCalcMaxLevel;
var
  NodesEnum : TVTVirtualNodeEnumerator;
  MaxLevel : integer;
begin
  MaxLevel := 0;

  NodesEnum := vstSPP.Nodes().GetEnumerator;
  while NodesEnum.MoveNext do
    MaxLevel := MAX(MaxLevel, vstSPP.GetNodeLevel(NodesEnum.Current));

  trckbrLvlCollapse.Max       := MaxLevel;

  trckbrLvlCollapse.Enabled := trckbrLvlCollapse.Max > trckbrLvlCollapse.Min;
end;

procedure TfrmProjPaths.ReCalcResults;
var
  ResStrs : TStringList;
  S : string;
  NE : TVTVirtualNodeEnumerator;
begin
  ResStrs := TStringList.Create;
  try
    ResStrs.LineBreak   := ';';

    ResStrs.Text := Trim(OldSearchPaths);

    NE := vstSPP.Nodes(True).GetEnumerator;

    while NE.MoveNext do
      if vstSPP.CheckState[NE.Current] in [csCheckedNormal, csMixedNormal] then
      begin
        S := vstSPP.Text[NE.Current, 1];

        if not SameStr(S, '.')
        and (ResStrs.IndexOf(S) = -1)
        then
          ResStrs.Add(S);
      end;

    edResults.Text := Copy(ResStrs.Text, 1, Length(ResStrs.Text) - 1);

  finally
    FreeAndNil(ResStrs);
  end;
end;

procedure TfrmProjPaths.ReFillTree;
var
  Index : integer;

  procedure AddNodes(const AParentNode : PVirtualNode; const AIndex : integer);
  var
    Node : PVirtualNode;
  begin
    Node := vstSPP.AddChild(AParentNode, Pointer(Index));

    while (Index < FPathsList.Count - 1)
    and SameStr(FPathsList[AIndex], ExtractFileDir(FPathsList[Index + 1]))
//    (Pos(FPathsList[AIndex], FPathsList[Index + 1]) = 1)
//    and SameStr()
    do
    begin
      Inc(Index);
      AddNodes(Node, Index);
    end;

    if actChangeCheckType.Checked and (Index <> AIndex) then
      vstSPP.CheckType[Node] := ctTriStateCheckBox
    else
      vstSPP.CheckType[Node] := ctCheckBox;

    vstSPP.CheckState[Node] := csUncheckedNormal;
  end;

begin
  Index := 0;

  vstSPP.BeginUpdate;
  try
    vstSPP.Clear;

    while (Index < FPathsList.Count) do
    begin
      AddNodes(vstSPP.RootNode, Index);
      Inc(Index);
    end;

    ReFillTreeChecks;

    vstSPP.FullExpand();

  finally
    vstSPP.EndUpdate;
  end;

  ReCalcMaxLevel;

  ReCalcResults;
end;

procedure TfrmProjPaths.ReFillTreeChecks;
var
  NE : TVTVirtualNodeEnumerator;
  InOld : boolean;
begin

  vstSPP.BeginUpdate;
  try

    NE := vstSPP.Nodes().GetEnumerator;
    while NE.MoveNext do
    begin
      InOld := FOldSearchPaths.IndexOf(vstSPP.Text[NE.Current, 1]) > -1;

      if vstSPP.CheckType[NE.Current] <> ctTriStateCheckBox then
        if InOld then
          vstSPP.CheckState[NE.Current] := csCheckedNormal
        else
          vstSPP.CheckState[NE.Current] := csUncheckedNormal;
    end;

  finally
    vstSPP.EndUpdate;
  end;

end;

procedure TfrmProjPaths.ReLoadPathsList;
begin

  if not Assigned(FSearchThread) then
    FSearchThread := TSearchThread.Create(Self);

end;

procedure TfrmProjPaths.SetOldSearchPaths(const AValue: string);
begin
  edbtOldSearchPath.Text := AValue;
end;

procedure TfrmProjPaths.SetProjFileName(const AValue: string);

  procedure FillcbbUpLevels(AValue: string);
  var
    i : integer;
  begin
    for i := 0 to Pred(FСacheDepthProjFileName) do
    begin
      AValue := ExtractFileDir(AValue);
      cbbUpLevels.Items.Add(AValue);
    end;

    cbbUpLevels.ItemIndex := 0;
  end;

begin
  FProjFileName := AValue;

  FСacheDepthProjFileName := CalcPathDepth(AValue);

  FillcbbUpLevels(AValue);

  ReLoadPathsList;
end;

procedure TfrmProjPaths.trckbrLvlCollapseChange(Sender: TObject);
var
  NodesEnum : TVTVirtualNodeEnumerator;
  P : integer;
begin
  P := (Sender as TTrackBar).Position;

  NodesEnum := vstSPP.Nodes().GetEnumerator;
  while NodesEnum.MoveNext do
    if vstSPP.GetNodeLevel(NodesEnum.Current) >= Cardinal(P) then
      vstSPP.FullCollapse(NodesEnum.Current)
    else
      vstSPP.FullExpand(NodesEnum.Current);
end;

procedure TfrmProjPaths.cbbUpLevelsChange(Sender: TObject);
begin
  HGC(ReLoadPathsList);
  HGC(ReCalcResults);
end;

procedure TfrmProjPaths.UpdateIcons;
var
  i : integer;
  IconIndex : Word;
  Icon : TIcon;
begin
  ilIcons.Clear;

  Icon := TIcon.Create;
  try

    for i := 0 to FPathsList.Count - 1 do
    begin
      IconIndex := 0;
      Icon.Handle := ExtractAssociatedIcon(HInstance, PChar(FPathsList[i]), IconIndex);
      ilIcons.AddIcon(Icon);
    end;

  finally
    FreeAndNil(Icon);
  end;

end;

procedure TfrmProjPaths.vstOldEditCancelled(Sender: TBaseVirtualTree;
  AColumn: TColumnIndex);
begin
  if (Sender as TVirtualStringTree).Text[Sender.FocusedNode, AColumn] = '' then
    FOldSearchPaths.Delete(Sender.FocusedNode^.Index);
end;

procedure TfrmProjPaths.vstOldEdited(Sender: TBaseVirtualTree; ANode: PVirtualNode;
  AColumn: TColumnIndex);
begin
  Application.ProcessMessages;
  Sender.Show;
  tlbOld.Show;
  Sender.SetFocus;
end;

procedure TfrmProjPaths.vstOldExit(Sender: TObject);
begin
  PostMessage(Handle, CM_HIDE_OLDS, 0, 0);
end;

procedure TfrmProjPaths.vstOldFocusChanged(Sender: TBaseVirtualTree;
  ANode: PVirtualNode; AColumn: TColumnIndex);
begin
  Sender.Selected[ANode] := True;
end;

procedure TfrmProjPaths.vstOldGetImageIndexEx(Sender: TBaseVirtualTree;
  ANode: PVirtualNode; AKind: TVTImageKind; AColumn: TColumnIndex;
  var AGhosted: Boolean; var AImageIndex: TImageIndex;
  var AImageList: TCustomImageList);
begin
  if AKind <> ikNormal then
    Exit;

  AImageIndex := 0;
  AImageList  := (Sender as TVirtualStringTree).Images;
end;

procedure TfrmProjPaths.vstOldGetText(Sender: TBaseVirtualTree; ANode: PVirtualNode;
  AColumn: TColumnIndex; ATextType: TVSTTextType; var ACellText: string);
begin
  ACellText := FOldSearchPaths[ANode^.Index];
end;

procedure TfrmProjPaths.vstOldKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  VST : TVirtualStringTree;
begin
  VST := Sender as TVirtualStringTree;

  if not VST.IsEditing then
    case Key of
      VK_INSERT:
        actOldAdd.Execute;

      VK_RETURN:
        actOldEdit.Execute;

    end;

end;

procedure TfrmProjPaths.vstOldMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  VST : TVirtualStringTree;
  Node : PVirtualNode;
  SaveCount : integer;
begin
  VST := Sender as TVirtualStringTree;

  Node := VST.GetNodeAt(X, Y);

  if not Assigned(Node) then
    Exit;

  if X <= ilOld.Width then
  begin
    SaveCount := FOldSearchPaths.Count;

    if VST.IsEditing then
      VST.CancelEditNode;

    if  (FOldSearchPaths.Count = SaveCount)
    and (Node^.Index < Cardinal(FOldSearchPaths.Count))
    then
      FOldSearchPaths.Delete(Node^.Index)
  end

  else
    PostMessage(Handle, CM_EDIT_OLDS, 0, 0);
end;

procedure TfrmProjPaths.vstOldNewText(Sender: TBaseVirtualTree; ANode: PVirtualNode;
  AColumn: TColumnIndex; ANewText: string);
begin
  FOldSearchPaths[ANode^.Index] := Trim(ANewText);
end;

procedure TfrmProjPaths.vstSPPChecked(Sender: TBaseVirtualTree; ANode: PVirtualNode);
begin
  ReCalcResults;
end;

procedure TfrmProjPaths.vstSPPGetImageIndexEx(Sender: TBaseVirtualTree;
  ANode: PVirtualNode; AKind: TVTImageKind; AColumn: TColumnIndex;
  var AGhosted: Boolean; var AImageIndex: TImageIndex;
  var AImageList: TCustomImageList);
begin

  if (AColumn > 0) or (AKind <> ikNormal) then
    Exit;

  AImageIndex := ANode^.Index;
  AImageList  := ilIcons;
end;

procedure TfrmProjPaths.vstSPPGetText(Sender: TBaseVirtualTree; ANode: PVirtualNode;
  AColumn: TColumnIndex; ATextType: TVSTTextType; var ACellText: string);
var
  Index : integer;

  function RelPath : string;
  const
    SEP   = '\';
    DOTS  = '..';
  var
    UpCount : NativeInt;
    i : integer;
    DelDir : string;
  begin
    Result := FPathsList[Index];

    UpCount := ABS( NativeInt(FPathsList.Objects[Index]) );

    DelDir := ProjDir;

    for i := 1 to UpCount do
      DelDir := ExtractFileDir(DelDir);

    System.Delete(Result, 1, Length(DelDir));

    if (Result <> '') and (Result[1] = SEP)  then
      System.Delete(Result, 1, 1);

    if UpCount > 0 then
      Result := DOTS + DupeString(SEP + DOTS, UpCount - 1) + IfThen(Result <> '', SEP + Result)

    else
    if Result = '' then
      Result := '.';
  end;

begin
  Index := Integer(Sender.GetNodeData(ANode)^);

  case AColumn of
    0: ACellText := ExtractFileName(FPathsList[Index]);
    1: ACellText := RelPath; //Format('%s (%d)', [RelPath, NativeInt(FPathsList.Objects[Index])]);
  end;
end;
{$ENDREGION}
//==============================================================================



end.
