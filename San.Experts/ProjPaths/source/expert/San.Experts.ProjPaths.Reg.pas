{*******************************************************}
{                                                       }
{       SANProjPaths                                    }
{                                                       }
{       Главный модуль пакета времение разработки.      }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit San.Experts.ProjPaths.Reg;
{$I ..\..\..\shared\San.Experts.inc}
interface

uses
  ToolsApi, DesignIntf,

  WinApi.Windows,

  System.SysUtils, System.Classes, System.Math, System.StrUtils, System.TypInfo, System.INIFiles,

  vcl.Graphics, vcl.Menus, vcl.Actnlist, vcl.ImgList, vcl.Forms, vcl.ToolWin,
  vcl.ComCtrls,

  // After vcl.ImgList
  System.UITypes,

  San.Experts.ProjPaths.frameSPP
;

type
  TSANProjPaths = class(TNotifierObject, IOTAWizard)
  private
    FMenuItem : TMenuItem;

  protected
    function GetIDString: string;
    function GetName: string;
    function GetState: TWizardState;
    procedure Execute;

    function LoadResBMPToGlbImageList(const AResName : string; const AMaskColor : TColor = clWhite) : TImageIndex;

    procedure miExec(Sender : TObject);

    procedure cbbConfigChange(Sender : TObject);
    procedure actApplyResultsExecute(Sender : TObject);

  public
    constructor Create;
    destructor Destroy; override;

  end;


  TSANProjPathsDockableForm = class(TInterfacedObject, INTACustomDockableForm,
    IOTANotifier, IOTAModuleNotifier, IOTAProjectNotifier)
  private
    FFrame : TfrmProjPaths;
    FForm : TCustomForm;
    FProject : IOTAProject;
    FNotifireIndex : integer;

  protected
    {$REGION 'INTACustomDockableForm'}
    function GetCaption: string;
    function GetIdentifier: string;
    function GetFrameClass: TCustomFrameClass;
    procedure FrameCreated(AFrame: TCustomFrame);
    function GetMenuActionList: TCustomActionList;
    function GetMenuImageList: TCustomImageList;
    procedure CustomizePopupMenu(PopupMenu: TPopupMenu);
    function GetToolBarActionList: TCustomActionList;
    function GetToolBarImageList: TCustomImageList;
    procedure CustomizeToolBar(ToolBar: TToolBar);
    procedure SaveWindowState(Desktop: TCustomIniFile; const Section: string; IsProject: Boolean);
    procedure LoadWindowState(Desktop: TCustomIniFile; const Section: string);
    function GetEditState: TEditState;
    function EditAction(Action: TEditAction): Boolean;
    {$ENDREGION}

    {$REGION 'IOTANotifier'}
    procedure AfterSave;
    procedure BeforeSave;
    procedure Destroyed;
    procedure Modified;
    {$ENDREGION}

    {$REGION 'IOTAModuleNotifier'}
    function CheckOverwrite: Boolean;
    procedure ModuleRenamed(const NewName: string);
    {$ENDREGION}

    {$REGION 'IOTAProjectNotifier'}
    procedure IOTAProjectNotifier.ModuleRenamed = ProjectModuleRenamed;
    procedure ModuleAdded(const AFileName: string);
    procedure ModuleRemoved(const AFileName: string);
    procedure ProjectModuleRenamed(const AOldFileName, ANewFileName: string);
    {$ENDREGION}

    procedure RemoveProjectNotifer;

  public
    constructor Create;
    destructor Destroy; override;
    function CreateDockableForm : TCustomForm;

    property Frame : TfrmProjPaths  read FFrame;
    property Form : TCustomForm  read FForm;
  end;


procedure Register;

implementation

{$R *.res}

{$I ..\..\..\shared\San.Experts.Menu.inc}

const
  MI_SAN_PROJ_PATHS_NAME     = '_748B513D03F74386BDC3CC8AF93B45C6';
  MI_SAN_PROJ_PATHS_CAPTION  = 'Project Search Paths';

  SAN_PROJ_PATHS_DOCKABLE_FORM_ID = '_6D2CB2D432894A40853E2DEBA26F8552';

  DCC_UnitSearchPath = 'DCC_UnitSearchPath';

procedure Register;
begin
  RegisterPackageWizard(TSANProjPaths.Create);
end;

//==============================================================================
{$REGION 'TSANProjPaths'}
constructor TSANProjPaths.Create;
var
  NTAServices          : INTAServices;
  OTAModuleServices    : IOTAModuleServices;
  OTADebuggerServices  : IOTADebuggerServices;

  miSAN : TMenuItem;
begin
  inherited;

  if not
    (
          Supports(BorlandIDEServices, INTAServices,          NTAServices)
      and Supports(BorlandIDEServices, IOTAModuleServices,    OTAModuleServices)
      and Supports(BorlandIDEServices, IOTADebuggerServices,  OTADebuggerServices)
    )
  then
    Abort;

  miSAN := SANMenuCreateOrFind(NTAServices.MainMenu.Items);

  FMenuItem := TMenuItem.Create(miSAN);
  FMenuItem.Name        := MI_SAN_PROJ_PATHS_NAME;
  FMenuItem.Caption     := MI_SAN_PROJ_PATHS_CAPTION + '...';
  FMenuItem.ImageIndex  := LoadResBMPToGlbImageList('SPP_16', clFuchsia);
  FMenuItem.OnClick     := miExec;

  miSAN.Insert(miSAN.Count - 1, FMenuItem);
end;

destructor TSANProjPaths.Destroy;
begin
  FreeAndNil(FMenuItem);

  SANMenuRelease((BorlandIDEServices as INTAServices).MainMenu.Items);

  inherited;
end;

procedure TSANProjPaths.Execute;
begin
end;

function TSANProjPaths.GetIDString: string;
begin
  Result := '{5B95A4FC-23D1-4AA5-8320-FAAEDDCB9CE5}';
end;

function TSANProjPaths.GetName: string;
begin
  Result := MI_SAN_PROJ_PATHS_CAPTION;
end;

function TSANProjPaths.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

function TSANProjPaths.LoadResBMPToGlbImageList(const AResName: string;
  const AMaskColor: TColor): TImageIndex;
var
  NTAServices : INTAServices;
  BMP : TBitmap;
begin

  if not Supports(BorlandIDEServices, INTAServices, NTAServices) then  begin
    Result := -1;
    Exit;
  end;//if

  BMP := TBitmap.Create;
  try
    BMP.LoadFromResourceName(HInstance, AResName);
    Result := NTAServices.ImageList.AddMasked(BMP, AMaskColor);
  finally
    FreeAndNil(BMP);
  end;//t..f

end;

procedure TSANProjPaths.miExec(Sender: TObject);
var
  PPDF : TSANProjPathsDockableForm;

  PJ    : IOTAProject;
  PJOC  : IOTAProjectOptionsConfigurations;
  BC    : IOTABuildConfiguration;

  i : integer;
  Strings : TStrings;
  Pltf : TArray<string>;
begin
  PJ := GetActiveProject;

  if PJ = nil then
  begin
    Application.MessageBox('Need active project for use expert', PChar(GetName), MB_ICONWARNING);
    Exit;
  end;

  PPDF  := TSANProjPathsDockableForm.Create;
  PPDF.CreateDockableForm;
  OutputDebugString(PChar('PJ.FileName: ' + PJ.FileName));

  PPDF.FFrame.ProjFileName := PJ.FileName;

  if Supports(PJ.ProjectOptions, IOTAProjectOptionsConfigurations, PJOC) then
  begin

    Strings := PPDF.FFrame.cbbBuildCfg.Items;
    Strings.BeginUpdate;
    try

      for i := 0 to PJOC.ConfigurationCount - 1 do
      begin
        BC := PJOC.Configurations[i];

        Strings.Add(BC.Name);
      end;

      for i := 0 to PJOC.ConfigurationCount - 1 do
      begin
        BC := PJOC.Configurations[i];

        if not Assigned(BC.Parent) then
        begin
          PPDF.FFrame.cbbBuildCfg.ItemIndex := i;
          Break;
        end;
      end;

    finally
      Strings.EndUpdate;
    end;
    PPDF.FFrame.cbbBuildCfg.OnChange := cbbConfigChange;


    Assert(Assigned(BC));

    Strings := PPDF.FFrame.cbbPlatform.Items;
    Strings.BeginUpdate;
    try
      Strings.Add('All');

      Pltf := BC.Platforms;

      for i := Low(Pltf) to High(Pltf) do
        Strings.Add(Pltf[i]);

      PPDF.FFrame.cbbPlatform.ItemIndex := 0;

    finally
      Strings.EndUpdate;
    end;
    PPDF.FFrame.cbbPlatform.OnChange := cbbConfigChange;

    PPDF.FFrame.OldSearchPaths := BC.Value[DCC_UnitSearchPath];
  end;

  PPDF.FFrame.actApplyResults.OnExecute  := actApplyResultsExecute;
end;

procedure TSANProjPaths.actApplyResultsExecute(Sender: TObject);
var
  Frame : TfrmProjPaths;

  PJ    : IOTAProject;
  PJOC  : IOTAProjectOptionsConfigurations;
  BC    : IOTABuildConfiguration;
begin
  PJ := GetActiveProject;

  if not Supports(PJ.ProjectOptions, IOTAProjectOptionsConfigurations, PJOC) then
    Exit;

  Frame := (Sender as TComponent).Owner as TfrmProjPaths;

  BC := PJOC.Configurations[Frame.cbbBuildCfg.ItemIndex];

  if Frame.cbbPlatform.ItemIndex > 0 then
    BC := BC.PlatformConfiguration[Frame.cbbPlatform.Items[Frame.cbbPlatform.ItemIndex]];

  BC.Value[DCC_UnitSearchPath] := Frame.Results;

  Frame.OldSearchPaths := BC.Value[DCC_UnitSearchPath];
end;

procedure TSANProjPaths.cbbConfigChange(Sender: TObject);
var
  Frame : TfrmProjPaths;

  PJ    : IOTAProject;
  PJOC  : IOTAProjectOptionsConfigurations;
  BC    : IOTABuildConfiguration;
begin
  PJ := GetActiveProject;

  if not Supports(PJ.ProjectOptions, IOTAProjectOptionsConfigurations, PJOC) then
    Exit;

  Frame := (Sender as TComponent).Owner as TfrmProjPaths;

  BC := PJOC.Configurations[Frame.cbbBuildCfg.ItemIndex];

  BC := BC.PlatformConfiguration[Frame.cbbPlatform.Items[Frame.cbbPlatform.ItemIndex]];

  Frame.OldSearchPaths := BC.Value[DCC_UnitSearchPath];

  PJ.MarkModified;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANProjPathsDockableForm'}
procedure TSANProjPathsDockableForm.AfterSave;
begin
end;

procedure TSANProjPathsDockableForm.BeforeSave;
begin
end;

function TSANProjPathsDockableForm.CheckOverwrite: Boolean;
begin
  Result := False;
end;

constructor TSANProjPathsDockableForm.Create;
begin
  inherited;

  FProject := GetActiveProject;
  FNotifireIndex := FProject.AddNotifier(Self);
end;

function TSANProjPathsDockableForm.CreateDockableForm: TCustomForm;
begin
  FForm := (BorlandIDEServices as INTAServices).CreateDockableForm(Self);
  Result := FForm;
end;

procedure TSANProjPathsDockableForm.CustomizePopupMenu(PopupMenu: TPopupMenu);
begin
end;

procedure TSANProjPathsDockableForm.CustomizeToolBar(ToolBar: TToolBar);
begin
end;

destructor TSANProjPathsDockableForm.Destroy;
begin
  RemoveProjectNotifer;

  FFrame  := nil;
  FForm   := nil;

  inherited;
end;

procedure TSANProjPathsDockableForm.Destroyed;
begin
  RemoveProjectNotifer;
  (BorlandIDEServices as INTAServices).UnregisterDockableForm(Self);
end;

function TSANProjPathsDockableForm.EditAction(Action: TEditAction): Boolean;
begin
  Result := False;
end;

procedure TSANProjPathsDockableForm.FrameCreated(AFrame: TCustomFrame);
begin
  FFrame := AFrame as TfrmProjPaths;

  FFrame.LoadedAndPlaced;
end;

function TSANProjPathsDockableForm.GetCaption: string;
var
  PJ    : IOTAProject;
begin
  PJ := GetActiveProject;

  if Assigned(PJ) then
    Result := Format('%s - %s', [ChangeFileExt(ExtractFileName(PJ.FileName), ''), MI_SAN_PROJ_PATHS_CAPTION])
  else
    Result := MI_SAN_PROJ_PATHS_CAPTION;
end;

function TSANProjPathsDockableForm.GetEditState: TEditState;
begin
  Result := [];
end;

function TSANProjPathsDockableForm.GetFrameClass: TCustomFrameClass;
begin
  Result := TfrmProjPaths;
end;

function TSANProjPathsDockableForm.GetIdentifier: string;
begin
  Result := SAN_PROJ_PATHS_DOCKABLE_FORM_ID;
end;

function TSANProjPathsDockableForm.GetMenuActionList: TCustomActionList;
begin
  Result := nil;
end;

function TSANProjPathsDockableForm.GetMenuImageList: TCustomImageList;
begin
  Result := nil;
end;

function TSANProjPathsDockableForm.GetToolBarActionList: TCustomActionList;
begin
  Result := nil;
end;

function TSANProjPathsDockableForm.GetToolBarImageList: TCustomImageList;
begin
  Result := nil;
end;

procedure TSANProjPathsDockableForm.LoadWindowState(Desktop: TCustomIniFile;
  const Section: string);
begin
end;

procedure TSANProjPathsDockableForm.Modified;
begin
end;

procedure TSANProjPathsDockableForm.ModuleAdded(const AFileName: string);
begin
end;

procedure TSANProjPathsDockableForm.ModuleRemoved(const AFileName: string);
begin
end;

procedure TSANProjPathsDockableForm.ModuleRenamed(const NewName: string);
begin
end;

procedure TSANProjPathsDockableForm.ProjectModuleRenamed(const AOldFileName,
  ANewFileName: string);
begin
end;

procedure TSANProjPathsDockableForm.RemoveProjectNotifer;
begin
  if Assigned(FProject) then
    FProject.RemoveNotifier(FNotifireIndex);

  FProject        := nil;
  FNotifireIndex  := -1;
end;

procedure TSANProjPathsDockableForm.SaveWindowState(Desktop: TCustomIniFile;
  const Section: string; IsProject: Boolean);
begin
end;
{$ENDREGION}
//==============================================================================

end.
