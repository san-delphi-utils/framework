{*******************************************************}
{                                                       }
{       SANProjPaths                                    }
{                                                       }
{       Главная форма утилиты.                          }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit SPP.formMain;

interface

{$REGION 'uses'}
uses
  {$REGION 'WinApi'}
    WinApi.Windows, WinApi.Messages,
  {$ENDREGION}

  {$REGION 'System'}
    System.SysUtils, System.Variants, System.Classes, System.IniFiles,
  {$ENDREGION}

  {$REGION 'Vcl'}
    Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ImgList,
    Vcl.StdCtrls, Vcl.ExtCtrls,
  {$ENDREGION}

  {$REGION 'San.Experts.ProjPaths'}
    San.Experts.ProjPaths.frameSPP
  {$ENDREGION}
  ;
{$ENDREGION}

type
  TINIWorkEvent = procedure(const AINI : TIniFile) of object;

  /// <summary>
  ///   Главная форма утилиты.
  /// </summary>
  TformSPPMain = class(TForm)
    pnProjPath: TPanel;
    edbtProjPath: TButtonedEdit;
    il: TImageList;
    dlgOpen: TOpenDialog;
    lblProjPath: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edbtProjPathRightButtonClick(Sender: TObject);
    procedure edbtProjPathChange(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    function GetFrameSPP: TfrmProjPaths;

  protected
    procedure INIWork(const AINIWorkEvent : TINIWorkEvent);
    procedure INISave(const AINI : TIniFile);
    procedure INILoad(const AINI : TIniFile);

  public
    property FrameSPP : TfrmProjPaths  read GetFrameSPP;

  end;

var
  formSPPMain: TformSPPMain;

implementation

{$R *.dfm}

const
  FRAME_SPP_NAME    = 'frameSPP';
  INI_MAIN_SECTION  = 'Main';
  INI_PROJ_PATH     = 'ProjPath';

//==============================================================================
{$REGION 'TformSPPMain'}
procedure TformSPPMain.edbtProjPathChange(Sender: TObject);
begin
  FrameSPP.ProjFileName := (Sender as TCustomEdit).Text;
end;

procedure TformSPPMain.edbtProjPathRightButtonClick(Sender: TObject);
begin
  dlgOpen.FileName := edbtProjPath.Text;

  if dlgOpen.Execute(Handle) then
    edbtProjPath.Text := dlgOpen.FileName;
end;

procedure TformSPPMain.FormCreate(Sender: TObject);

  procedure InitFrame;
  var
    FrameSPP : TfrmProjPaths;
  begin
    FrameSPP         := TfrmProjPaths.Create(Self);
    FrameSPP.Name    := FRAME_SPP_NAME;
    FrameSPP.Align   := alClient;
    FrameSPP.Parent  := Self;

    FrameSPP.sepBuildCfg.Hide;
    FrameSPP.sepPlatform.Hide;
    FrameSPP.cbbBuildCfg.Hide;
    FrameSPP.cbbPlatform.Hide;
    FrameSPP.actApplyResults.Visible := False;

    FrameSPP.Show;
  end;

begin
  Application.Title := Caption;

  InitFrame;

  INIWork(INILoad)
end;

procedure TformSPPMain.FormDestroy(Sender: TObject);
begin
  INIWork(INISave)
end;

procedure TformSPPMain.FormShow(Sender: TObject);
begin
  FrameSPP.LoadedAndPlaced;
end;

function TformSPPMain.GetFrameSPP: TfrmProjPaths;
begin
  Result := FindComponent(FRAME_SPP_NAME) as TfrmProjPaths;
end;

procedure TformSPPMain.INILoad(const AINI: TIniFile);
begin
  edbtProjPath.Text := AINI.ReadString(INI_MAIN_SECTION, INI_PROJ_PATH, '');
end;

procedure TformSPPMain.INISave(const AINI: TIniFile);
begin
  AINI.WriteString(INI_MAIN_SECTION, INI_PROJ_PATH, edbtProjPath.Text);
end;

procedure TformSPPMain.INIWork(const AINIWorkEvent: TINIWorkEvent);
var
  FileName : TFileName;
  INI : TIniFile;
begin
  FileName := ExtractFileName(Application.ExeName);
  SetLength(FileName, Length(FileName) - Length(ExtractFileExt(Application.ExeName)));
  FileName := ExtractFilePath(Application.ExeName) + FileName + '.ini';

  INI := TIniFile.Create(FileName);
  try

    AINIWorkEvent(INI);

  finally
    FreeAndNil(INI);
  end;
end;
{$ENDREGION}
//==============================================================================

end.
