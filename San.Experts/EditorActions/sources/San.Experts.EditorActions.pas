{*******************************************************}
{                                                       }
{       SANEditorActions                                }
{                                                       }
{       Главный модуль эксперта управления действиями   }
{       редактора кода.                                 }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit San.Experts.EditorActions;
{$I ..\..\shared\San.Experts.inc}
interface

uses
  ToolsApi,

  Windows,

  SysUtils, Classes, Math, StrUtils, TypInfo, RTTI,

  vcl.Graphics, vcl.Menus, vcl.Actnlist, vcl.ImgList, vcl.Forms,

  System.UITypes;

{$I ..\..\shared\San.Experts.RTTIActionsWizard.Intf.inc}

type
  /// <summary>
  ///   Вызов действий активного редактора из пункта меню.
  /// </summary>
  TSANEditorActions = class(TSANCustomRTTIActionsWizard, IOTAWizard)
  private

  protected
    {$REGION 'IOTAWizard'}
    function GetIDString: string;
    function GetName: string;
    {$ENDREGION}

    procedure ActUpdate(Sender : TObject); override;

  public

  published
    [AER('Fold XMLDoc Comments')]
    procedure EA_FoldXMLDocComments;

  end;

procedure Register;

implementation

{$I ..\..\shared\San.Experts.RTTIActionsWizard.Impl.inc}

const
  MI_SAN_EDITOR_ACTIONS_NAME        = '_9B6278C30D834077A76A60E525505BDE';
  MI_SAN_EDITOR_ACTIONS_CAPTION     = 'Editor Actions';
  EA_PREFIX                         = 'EA_';

procedure Register;
begin
  RegisterPackageWizard(TSANEditorActions.Create(EA_PREFIX, MI_SAN_EDITOR_ACTIONS_NAME));
end;

//==============================================================================
{$REGION 'TSANEditorActions'}
procedure TSANEditorActions.ActUpdate(Sender: TObject);

  function GetEnabled : boolean;
  var
    Module : IOTAModule;
    SourceEditor : IOTASourceEditor;
    i, j : integer;
  begin
    Module := (BorlandIDEServices as IOTAModuleServices).CurrentModule;

    if not Assigned(Module) then
      Exit(False);

    for i := 0 to Module.ModuleFileCount - 1 do
     if Supports(Module.ModuleFileEditors[i], IOTASourceEditor, SourceEditor) then
       for j := 0 to SourceEditor.EditViewCount - 1 do
         if Supports(SourceEditor.EditViews[i], IOTAElideActions) then
           Exit(True);

    Result := False;
  end;

begin
  (Sender as TAction).Enabled := GetEnabled;
end;

procedure TSANEditorActions.EA_FoldXMLDocComments;
var
  Module : IOTAModule;
  SourceEditor : IOTASourceEditor;
  ElideActions : IOTAElideActions;
  i, j : integer;
begin
  Module := (BorlandIDEServices as IOTAModuleServices).CurrentModule;

  Assert(Assigned(Module));

  for i := 0 to Module.ModuleFileCount - 1 do
   if Supports(Module.ModuleFileEditors[i], IOTASourceEditor, SourceEditor) then
     for j := 0 to SourceEditor.EditViewCount - 1 do
       if Supports(SourceEditor.EditViews[i], IOTAElideActions, ElideActions) then
       begin
         ElideActions.ElideDocRegions;

         SourceEditor.EditViews[i].Buffer.BlockVisible := True;
         SourceEditor.Show;
         Exit;
       end;
end;

function TSANEditorActions.GetIDString: string;
begin
  Result := '{8A980150-0058-4F2F-B829-CF4E1B6D76FD}'
end;

function TSANEditorActions.GetName: string;
begin
  Result := MI_SAN_EDITOR_ACTIONS_CAPTION;
end;
{$ENDREGION}
//==============================================================================


end.
