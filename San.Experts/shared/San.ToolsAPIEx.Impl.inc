function TryFindOpenModuleByFileName(const AFileName : TFileName;
  out AModule : IOTAModule) : boolean;
var
  i : integer;
  MS : IOTAModuleServices;
  M : IOTAModule;
begin
  AModule := nil;

  if not Supports(BorlandIDEServices, IOTAModuleServices, MS) then
    Exit(False);

  for i := 0 to MS.ModuleCount - 1 do
  begin
    M := MS.Modules[i];

    if not AnsiSameText(M.FileName, AFileName) then
      Continue;

    AModule := M;
    Exit(True);
  end;

  Result := False;
end;

function GetEditBuffer(const AModule: IOTAModule; out ABuffer: IOTAEditBuffer): boolean;
var
  i : integer;
begin

  if Assigned(AModule) then
    for i := 0 to AModule.ModuleFileCount - 1 do
     if Supports(AModule.ModuleFileEditors[i], IOTAEditBuffer, ABuffer) then
       Exit(True);

  ABuffer := nil;
  Result := False;
end;

function GetBufferSourceCode(const ABuffer: IOTAEditBuffer) : string;
const
  BUF_SIZE = 1024;
var
  Reader     : IOTAEditReader;
  L, Offset  : integer;
  CurBuf     : array [0..BUF_SIZE - 1] of AnsiChar;
  TotalBuff  : AnsiString;
begin
  Reader := ABuffer.CreateReader;

  TotalBuff := '';
  Offset := 0;
  repeat
    L := Reader.GetText(Offset, PAnsiChar(@CurBuf), BUF_SIZE);

    if L = BUF_SIZE then
    begin
      TotalBuff := TotalBuff + CurBuf;
      Inc(Offset, L);
    end
    else
    begin
      TotalBuff := TotalBuff + Copy(CurBuf, 1, L);
      Break;
    end;

  until FALSE;

  Result := string(TotalBuff);
end;

function GetModuleRootComponent(out ARootComponent; const AModule : IOTAModule) : boolean;
var
  i : integer;
  OTAFormEditor : IOTAFormEditor;
  OTAComponent : IOTAComponent;
begin

  for i := 0 to AModule.GetModuleFileCount - 1 do
    if Supports(AModule.GetModuleFileEditor(i), IOTAFormEditor, OTAFormEditor) then
    begin
      OTAComponent := OTAFormEditor.GetRootComponent;
      if OTAComponent = nil then
        Continue;

      Pointer(ARootComponent) := OTAComponent.GetComponentHandle;
      Exit(True);
    end;

  Result := False;
end;
