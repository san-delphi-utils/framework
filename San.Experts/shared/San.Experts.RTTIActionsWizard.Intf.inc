type
  /// <summary>
  ///   Атрибут, описывающий параметры действия TAction.
  /// </summary>
  AER = class(TCustomAttribute)
  private
    FCaption : string;
    FImageIndex : integer;

  public
    constructor Create(const ACaption : string; const AImageIndex : integer = -1);
  end;

  {$M+}
  /// <summary>
  ///   Абстрактный эксперт, создающий набор действий для всех опубликованных
  ///   методов с указанным префиксом.
  /// </summary>
  TSANCustomRTTIActionsWizard = class(TNotifierObject, IOTAWizard)
  private
    FMenuItem : TMenuItem;
    FActionsPrefix, FActionsFirstName : string;

    class var FRttiContext : TRttiContext;

  protected
    {$REGION 'IOTAWizard'}
    function GetIDString: string;
    function GetName: string;
    function GetState: TWizardState;
    procedure Execute;
    {$ENDREGION}

    function LoadResBMPToGlbImageList(const AResName : string; const AMaskColor : TColor = clWhite) : TImageIndex;

    procedure InitActions(const ANTAServices : INTAServices);
    procedure ActExecute(Sender : TObject);

    /// <summary>
    ///   Переопределяется, чтобы управлять активностью действий.
    /// </summary>
    /// <remarks>
    ///   По умолчанию, пусто.
    /// </remarks>
    procedure ActUpdate(Sender : TObject); virtual;

    /// <summary>
    ///   Выполнение метода.
    /// </summary>
    /// <remarks>
    ///   По умолчанию, метод считается процедурой объекта без дополнительных параметров.
    /// </remarks>
    procedure DoActExecute(const AAction : TAction; const AMethod : TMethod); virtual;

    /// <summary>
    ///   Дополнительная инициализация действий.
    /// </summary>
    procedure DoAfterCreateAction(const AAction : TAction); virtual;

    /// <summary>
    ///   Контекст RTTI.
    /// </summary>
    class function RttiContext : TRttiContext;

  public
    class constructor Create;
    class destructor Destroy;
    constructor Create(const AActionsPrefix, AActionsFirstName : string;
      const AMenuItemImageName : string = ''; const AMenuItemImageMaskColor : TColor = clWhite);

    destructor Destroy; override;

    property MenuItem : TMenuItem  read FMenuItem;
  end;
  {$M-}
