{$I ..\..\shared\San.Experts.Menu.inc}

//==============================================================================
{$REGION 'AER'}
constructor AER.Create(const ACaption: string; const AImageIndex: integer);
begin
  inherited Create;

  FCaption := ACaption;
  FImageIndex := AImageIndex;
end;
{$ENDREGION}
//==============================================================================

//==============================================================================
{$REGION 'TSANCustomRTTIActionsWizard'}
type
  TCustomActionListHack = class(TActionList);

procedure TSANCustomRTTIActionsWizard.ActExecute(Sender: TObject);
var
  MethodName : string;
  Method : TMethod;
begin
  MethodName := (Sender as TComponent).Name;
  Delete(MethodName, 1, Length('act' + FActionsFirstName));
  MethodName := FActionsPrefix + MethodName;

  with TMethod(Method) do
  begin
    Code := MethodAddress(MethodName);
    Data := Self;
  end;

  DoActExecute(Sender as TAction, Method);
end;

procedure TSANCustomRTTIActionsWizard.ActUpdate(Sender: TObject);
begin
end;

class function TSANCustomRTTIActionsWizard.RttiContext : TRttiContext;
begin
  Result := FRttiContext;
end;

class constructor TSANCustomRTTIActionsWizard.Create;
begin
  FRttiContext := TRttiContext.Create;
end;

class destructor TSANCustomRTTIActionsWizard.Destroy;
begin
  FRttiContext.Free;
end;

constructor TSANCustomRTTIActionsWizard.Create(const AActionsPrefix, AActionsFirstName : string;
  const AMenuItemImageName : string; const AMenuItemImageMaskColor : TColor);
var
  NTAServices  : INTAServices;
  miSAN : TMenuItem;
begin
  inherited Create;

  if not
    (
     Supports(BorlandIDEServices, IOTAModuleServices)
       and
     Supports(BorlandIDEServices, INTAServices,          NTAServices)
    )
  then
    Abort;


  FActionsPrefix         := AActionsPrefix;
  FActionsFirstName      := AActionsFirstName;


  miSAN := SANMenuCreateOrFind(NTAServices.MainMenu.Items);

  FMenuItem             := TMenuItem.Create(miSAN);
  FMenuItem.Name        := AActionsFirstName;
  FMenuItem.Caption     := (Self as IOTAWizard).GetName;

  if AMenuItemImageName <> '' then
    FMenuItem.ImageIndex  := LoadResBMPToGlbImageList(AMenuItemImageName, AMenuItemImageMaskColor);

  miSAN.Insert(miSAN.Count - 1, FMenuItem);

  InitActions(NTAServices);
end;

destructor TSANCustomRTTIActionsWizard.Destroy;
begin
  FreeAndNil(FMenuItem);

  SANMenuRelease((BorlandIDEServices as INTAServices).MainMenu.Items);

  inherited;
end;

procedure TSANCustomRTTIActionsWizard.DoActExecute(const AAction : TAction; const AMethod: TMethod);
type
  TSimpleMethod = procedure of object;
var
  Method : TSimpleMethod;
begin
  Method := TSimpleMethod(AMethod);
  Method();
end;

procedure TSANCustomRTTIActionsWizard.DoAfterCreateAction(const AAction : TAction);
begin
end;

procedure TSANCustomRTTIActionsWizard.Execute;
begin
end;

function TSANCustomRTTIActionsWizard.GetIDString: string;
begin
  Result := '';
end;

function TSANCustomRTTIActionsWizard.GetName: string;
begin
  Result := '';
end;

function TSANCustomRTTIActionsWizard.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

procedure TSANCustomRTTIActionsWizard.InitActions(
  const ANTAServices: INTAServices);
var
  Methods  : TArray<TRttiMethod>;
  Method   : TRttiMethod;
  AERAttr  : AER;
  Name     : string;
  act      : TAction;
  mi       : TMenuItem;
begin
  Methods := (FRttiContext.GetType(ClassType) as TRttiInstanceType).GetDeclaredMethods;

  for Method in Methods do
    if  (Method.Visibility = mvPublished)
    and (Pos(FActionsPrefix, Method.Name) = 1)
    and (Length(Method.GetAttributes) > 0)
    and (Method.GetAttributes[0] is AER)
    then
    begin
      Name := Method.Name;
      Delete(Name, 1, Length(FActionsPrefix));

      AERAttr := Method.GetAttributes[0] as AER;

      act := TAction.Create(FMenuItem);
      act.Name        := Format('act%s%s', [FActionsFirstName, Name]);
      act.Caption     := AERAttr.FCaption;
      act.OnExecute   := ActExecute;
      act.OnUpdate    := ActUpdate;
      act.ImageIndex  := AERAttr.FImageIndex;
      TCustomActionListHack(ANTAServices.ActionList).AddAction(act);

      DoAfterCreateAction(act);

      mi := TMenuItem.Create(FMenuItem);
      mi.Name    := Format('mi%s%s', [FActionsFirstName, Name]);
      mi.Action  := act;
      FMenuItem.Add(mi);
    end;
end;

function TSANCustomRTTIActionsWizard.LoadResBMPToGlbImageList(
  const AResName: string; const AMaskColor: TColor): TImageIndex;
var
  NTAServices : INTAServices;
  BMP : TBitmap;
begin

  if not Supports(BorlandIDEServices, INTAServices, NTAServices) then
    Exit(-1);

  BMP := TBitmap.Create;
  try
    BMP.LoadFromResourceName(HInstance, AResName);
    Result := NTAServices.ImageList.AddMasked(BMP, AMaskColor);
  finally
    FreeAndNil(BMP);
  end;

end;
{$ENDREGION}
//==============================================================================