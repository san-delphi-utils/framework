{*******************************************************}
{                                                       }
{       SANEditorReplace                                }
{                                                       }
{       Главный модуль эксперта замены текста           }
{       в окне редактора кода.                          }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit San.Experts.EditorReplace;
{$I ..\..\shared\San.Experts.inc}
interface

uses
  ToolsApi,

  Windows,

  SysUtils, Classes, Math, StrUtils, TypInfo, RTTI,

  vcl.Graphics, vcl.Menus, vcl.Actnlist, vcl.ImgList, vcl.Forms,

  System.UITypes;

{$I ..\..\shared\San.Experts.RTTIActionsWizard.Intf.inc}

type
  /// <summary>
  ///   Эксперт замены текста в окне редактора кода.
  /// </summary>
  TSANEditorReplace = class(TSANCustomRTTIActionsWizard, IOTAWizard)
  private
    function GetCurrentModule: IOTAModule;

  protected
    {$REGION 'IOTAWizard'}
    function GetIDString: string;
    function GetName: string;
    {$ENDREGION}

    procedure ActUpdate(Sender : TObject); override;
    procedure DoActExecute(const AAction : TAction; const AMethod : TMethod); override;

    function GetMaxFirstSpacesInStrings(const AStrings : TStrings) : integer;

  public
    property CurrentModule: IOTAModule  read GetCurrentModule;

  published
    [AER('AbCdEf -> AB_CD_EF')]
    procedure ERC_InfixToUPPER(const AStrings : TStrings;
      const AFirstLineOffset : integer; const AParam : NativeInt);

    [AER('AB_CD_EF -> AbCdEf')]
    procedure ERC_UPPERToInfix(const AStrings : TStrings;
      const AFirstLineOffset : integer; const AParam : NativeInt);

    [AER('Comment -> Summary')]
    procedure ERC_CommentToSummary(const AStrings : TStrings;
      const AFirstLineOffset : integer; const AParam : NativeInt);

    [AER('Comment -> Remarks')]
    procedure ERC_CommentToRemarks(const AStrings : TStrings;
      const AFirstLineOffset : integer; const AParam : NativeInt);

    [AER('Comment -> Region')]
    procedure ERC_CommentToRegion(const AStrings : TStrings;
      const AFirstLineOffset : integer; const AParam : NativeInt);

    [AER('Remove regions')]
    procedure ERC_RemoveRegions(const AStrings : TStrings;
      const AFirstLineOffset : integer; const AParam : NativeInt);

  end;

procedure Register;

implementation

uses
  San.Experts.EditBlockModification;

{$R *.res}

{$I ..\..\shared\San.ToolsApiEx.Impl.inc}
{$I ..\..\shared\San.Experts.RTTIActionsWizard.Impl.inc}

const
  MI_SAN_EDITOR_REPLACE_NAME     = '_19122D813EC84F3EBFA359F5F293E6B6';
  MI_SAN_EDITOR_REPLACE_CAPTION  = 'Editor Replace';
  ERC_PREFIX                     = 'ERC_';

procedure Register;
begin
  RegisterPackageWizard(TSANEditorReplace.Create(ERC_PREFIX, MI_SAN_EDITOR_REPLACE_NAME, 'SER_BMP', clFuchsia));
end;

//==============================================================================
{$REGION 'TSANEditorReplace'}
procedure TSANEditorReplace.ERC_InfixToUPPER(const AStrings: TStrings;
  const AFirstLineOffset: integer; const AParam: NativeInt);
var
  vResult : string;
  vChar : Char;
  i, j : integer;
begin

  for j := 0 to AStrings.Count - 1 do
  begin
    vResult := '';

    for i := 1 to Length(AStrings[j]) do
    begin
      vChar := AStrings[j][i];

      if  (i > 1) and (AStrings[j][i - 1] <> ' ')
      and CharInSet(vChar, ['a'..'z', 'A'..'Z', 'а'..'я', 'А'..'Я'])
      and (vChar = AnsiUpperCase(vChar))
      then
        vResult := vResult + '_';

      vResult := vResult + vChar;
    end;

    AStrings[j] := AnsiUpperCase(vResult);
  end;

end;

procedure TSANEditorReplace.ERC_UPPERToInfix(const AStrings: TStrings;
  const AFirstLineOffset: integer; const AParam: NativeInt);
var
  vResult : string;
  i, j : integer;
begin

  for j := 0 to AStrings.Count - 1 do
  begin
    vResult := AStrings[j];

    for i := 1 to Length(vResult) do
    begin

      if (i > 1) and (not CharInSet(vResult[i - 1], [' ', '_'])) then
        vResult[i] := AnsiLowerCase(vResult[i])[1];
    end;

    AStrings[j] := StringReplace(vResult, '_', '', [rfReplaceAll]);
  end;

end;

function TSANEditorReplace.GetCurrentModule: IOTAModule;
begin
  Result := (BorlandIDEServices as IOTAModuleServices).CurrentModule;
end;

function TSANEditorReplace.GetIDString: string;
begin
  Result := '{1F528EFE-25B1-422A-B005-519CB6E9020B}'
end;

function TSANEditorReplace.GetMaxFirstSpacesInStrings(
  const AStrings: TStrings): integer;
var
  i, j, vCount : integer;
begin
  Result := 0;

  for i := 0 to AStrings.Count - 1 do
  begin
    vCount := 0;

    for j := 1 to Length(AStrings[i]) do
      if AStrings[i][j] = ' ' then
        Inc(vCount)
      else
        Break;

    Result := MAX(Result, vCount);
  end;
end;

function TSANEditorReplace.GetName: string;
begin
  Result := MI_SAN_EDITOR_REPLACE_CAPTION;
end;

procedure TSANEditorReplace.ERC_CommentToSummary(const AStrings: TStrings;
  const AFirstLineOffset: integer; const AParam: NativeInt);
var
  SpcMaxCount : integer;
begin
  SpcMaxCount := GetMaxFirstSpacesInStrings(AStrings);

  AStrings.Text := StringReplace(AStrings.Text, '//', '///  ', [rfReplaceAll, rfIgnoreCase]);

  AStrings.Insert(0, DupeString(' ', SpcMaxCount) + '/// <summary>');
  AStrings.Add(      DupeString(' ', SpcMaxCount) + '/// </summary>');
end;

procedure TSANEditorReplace.ERC_CommentToRemarks(const AStrings: TStrings;
  const AFirstLineOffset: integer; const AParam: NativeInt);
var
  SpcMaxCount : integer;
begin
  SpcMaxCount := GetMaxFirstSpacesInStrings(AStrings);

  AStrings.Text := StringReplace(AStrings.Text, '//', '///  ', [rfReplaceAll, rfIgnoreCase]);

  AStrings.Insert(0, DupeString(' ', SpcMaxCount) + '/// <remarks>');
  AStrings.Add(      DupeString(' ', SpcMaxCount) + '/// </remarks>');
end;

procedure TSANEditorReplace.ActUpdate(Sender: TObject);
var
  Buffer: IOTAEditBuffer;
begin
  (Sender as TAction).Enabled := GetEditBuffer(CurrentModule, Buffer)
                             and Assigned(Buffer.EditBlock)
                             and (Buffer.EditBlock.Text <> '');
end;

procedure TSANEditorReplace.DoActExecute(const AAction : TAction; const AMethod: TMethod);
var
  Method : TModificationProc;
  Buffer : IOTAEditBuffer;
begin
  if not GetEditBuffer(CurrentModule, Buffer) then
    Exit;

  Method := TModificationProc(AMethod);

  EditBlockModification(Buffer, Method);
end;

procedure TSANEditorReplace.ERC_CommentToRegion(const AStrings: TStrings;
  const AFirstLineOffset: integer; const AParam: NativeInt);
//------------------------------------------------------------------------------
  function CalcSkipEdEmptyFirst : integer;
  var
    i : integer;
  begin
    Result := 0;

    for i := 0 to AStrings.Count - 1 do
      if Trim(AStrings[i]) = EmptyStr then
        Inc(Result)
      else
        Break;
  end;
//------------------------------------------------------------------------------
  function CalcSkipEdEmptyLast : integer;
  var
    i : integer;
  begin
    Result := 0;

    for i := AStrings.Count - 1 downto 0 do
      if Trim(AStrings[i]) = EmptyStr then
        Inc(Result)
      else
        Break;
  end;
//------------------------------------------------------------------------------
  function IsDoubleSlashComment(const AFirstStr : string; out APos : integer;
    out APreparedStr : string) : boolean;
  begin
    APos := POS('//', AFirstStr);

    Result := (APos > 0) and (Trim(Copy(AFirstStr, 1, APos - 1)) = EmptyStr);

    if not Result then
      Exit;

    APreparedStr := AFirstStr;
    Delete(APreparedStr, 1, APos + 1);
    APreparedStr := Trim(APreparedStr);
  end;
//------------------------------------------------------------------------------
  function IsBraceComment(const AFirstStr : string; out APos : integer;
    out APreparedStr : string) : boolean;
  var
    SubS : string;
  begin
    SubS := Trim(AFirstStr);

    Result := (Length(SubS) > 1) and (SubS[1] = '{') and (SubS[Length(SubS)] = '}');

    if not Result then
      Exit;

    APos := POS('{', AFirstStr);

    APreparedStr := TrimRight(AFirstStr);
    Delete(APreparedStr, 1, APos + 1);
    SetLength(APreparedStr, Length(APreparedStr) - 1);
    APreparedStr := TrimRight(APreparedStr);
  end;
//------------------------------------------------------------------------------
var
  SpcMaxCount, SkipEdEmptyFirst, SkipEdEmptyLast, PosFirstComment : integer;
  S : string;
begin
  SkipEdEmptyFirst := CalcSkipEdEmptyFirst;
  SkipEdEmptyLast  := CalcSkipEdEmptyLast;

  if SkipEdEmptyFirst + SkipEdEmptyLast >= AStrings.Count then
    Exit;

  if IsDoubleSlashComment(AStrings[SkipEdEmptyFirst], PosFirstComment, S)
  or IsBraceComment(AStrings[SkipEdEmptyFirst], PosFirstComment, S)
  then
  begin
    AStrings[SkipEdEmptyFirst] := DupeString(' ', PosFirstComment - 1) +
                                  '{$REGION' +
                                  IfThen(S <> '', ' ''' + S + '''') +
                                  '}';
    AStrings.Insert(AStrings.Count - SkipEdEmptyLast, DupeString(' ', PosFirstComment - 1) + '{$ENDREGION}');
  end

  else
  begin
    SpcMaxCount := GetMaxFirstSpacesInStrings(AStrings);

    AStrings.Insert(SkipEdEmptyFirst, DupeString(' ', SpcMaxCount) + '{$REGION}');
    AStrings.Insert(AStrings.Count - SkipEdEmptyLast, DupeString(' ', SpcMaxCount) + '{$ENDREGION}');
  end;
end;

procedure TSANEditorReplace.ERC_RemoveRegions(const AStrings: TStrings;
  const AFirstLineOffset: integer; const AParam: NativeInt);
var
  i : integer;
  S : string;
begin
  for i := AStrings.Count - 1 downto 0 do
  begin
    S := AnsiUpperCase(AStrings[i]);

    if (POS('{$ENDREGION}', S) > 0) or (POS('{$REGION', S) > 0) then
      AStrings.Delete(i);
  end;
end;
{$ENDREGION}
//==============================================================================

end.
