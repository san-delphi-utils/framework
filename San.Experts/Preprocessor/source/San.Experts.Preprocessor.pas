{*******************************************************}
{                                                       }
{       SANMacro                                        }
{                                                       }
{       Главный модуль эксперта макросов                }
{       в исходном коде.                                }
{                                                       }
{       Copyright (C) 2013 SAN Software                 }
{                                                       }
{*******************************************************}
unit San.Experts.Preprocessor;
{$I ..\..\shared\San.Experts.inc}
interface

{$REGION 'uses'}
uses
  ToolsApi,

  WinApi.Windows,

  System.SysUtils, System.Classes, System.Math, System.StrUtils, System.TypInfo,
  System.RTTI, System.IOUtils, System.Generics.Collections,

  vcl.Graphics, vcl.Menus, vcl.Actnlist, vcl.ImgList, vcl.Forms,

  // After vcl.ImgList
  System.UITypes
  ;
{$ENDREGION}

{$I ..\..\shared\San.Experts.RTTIActionsWizard.Intf.inc}

{$REGION 'const'}
const
  MI_SAN_PREPROCESSOR_ENABLED_CAPTION   = 'Enabled';
  SAN_PREPROCESSOR_SOURC_FILE_EXT       = '.pastmpl';
  SAN_PREPROCESSOR_PROJ_CONFIG_EXT      = '.sppcfg';
  SAN_PREPROCESSOR_PROJ_CONFIG_COMMENT  = '#';
{$ENDREGION}

type
  /// <summary>
  ///   Ошибка препроцессора.
  /// </summary>
  ESANPreprocessor = class(Exception);

  /// <summary>
  ///   Ошибка этапа препроцессора.
  /// </summary>
  ESANPreprocessorStage = class(ESANPreprocessor);

  /// <summary>
  ///   Тип этапа препроцессинга.
  /// </summary>
  TSANPreprocessorStageClass = class of TSANPreprocessorStageCustom;

  /// <summary>
  ///   Этап препроцессинга.
  /// </summary>
  TSANPreprocessorStageCustom = class abstract(TObject)
  protected
    /// <summary>
    ///   Обработка исходного кода.
    /// </summary>
    procedure Preprocess(const ASourceCode : TStrings); virtual; abstract;

    /// <summary>
    ///   Попытка прочитать и создать этап по конфигурационной строке.
    /// </summary>
    class function TryReadCfgStr(const AConfigStr : string;
      out AStage : TSANPreprocessorStageCustom) : boolean; virtual;
  end;

  /// <summary>
  ///   Типы этапов препроцессинга.
  /// </summary>
  TSANPreprocessorStagesTypes = class(TList<TSANPreprocessorStageClass>)
  public
    /// <summary>
    ///   Попытка прочитать и создать этап по конфигурационной строке.
    /// </summary>
    function TryReadCfgStr(const AConfigStr : string;
      out AStage : TSANPreprocessorStageCustom) : boolean;
  end;

  /// <summary>
  ///   Этапы препроцессинга.
  /// </summary>
  TSANPreprocessorStages = class(TObjectList<TSANPreprocessorStageCustom>)
  public
    /// <summary>
    ///   Обработка исходного кода всеми этапами.
    /// </summary>
    procedure PreprocessAll(const ASourceCode : TStrings);

    /// <summary>
    ///   Загрузить этапы из конфига.
    /// </summary>
    procedure LoadConfig(const AFileName : TFileName);
  end;

  /// <summary>
  ///   Этап препроцессинга. Из библиотеки.
  /// </summary>
  TSANPreprocessorStageLib = class abstract(TSANPreprocessorStageCustom)
  strict private
    FFileName : TFileName;
    FFuncName : string;
    FHandle : THandle;

  protected
    procedure Preprocess(const ASourceCode : TStrings); override;

    procedure Load(out AHandle : THandle); virtual; abstract;
    procedure UnLoad(const AHandle : THandle); virtual; abstract;
    procedure CallFromLib(const AFuncAddr : Pointer;
       const ASourceCode : TStrings); virtual; abstract;

    class function TryReadCfgStr(const AConfigStr : string;
      out AStage : TSANPreprocessorStageCustom) : boolean; override;
    class function TryReadCfgStrStart : string; virtual;

  public
    constructor Create(const AFileName : TFileName; const AFuncName : string);

    /// <summary>
    ///   Имя файла библиотеки.
    /// </summary>
    property FileName : TFileName  read FFileName;

    /// <summary>
    ///   Имя функции в библиотеке.
    /// </summary>
    property FuncName : string  read FFuncName;

    /// <summary>
    ///   Описатель библиотеки.
    /// </summary>
    property Handle : THandle  read FHandle;
  end;

  /// <summary>
  ///   Этап препроцессинга. Из библиотеки BPL.
  /// </summary>
  TSANPreprocessorStageBPL = class(TSANPreprocessorStageLib)
  public
    type
      TBPLFunc = procedure (const ASourceCode : TStrings);

  protected
    procedure Load(out AHandle : THandle); override;
    procedure UnLoad(const AHandle : THandle); override;
    procedure CallFromLib(const AFuncAddr : Pointer;
       const ASourceCode : TStrings); override;

    class function TryReadCfgStrStart : string; override;
  end;

  /// <summary>
  ///   Управление макросами в исходном коде.
  /// </summary>
  TSANPreprocessor = class(TSANCustomRTTIActionsWizard, IOTAWizard)
  public
    type
      /// <summary>
      ///   Контекст препроцессинга.
      /// </summary>
      TPreprocessingContext = class
      strict private
        FConfigFileName : TFileName;
        FStages : TSANPreprocessorStages;
        FSourceCode : TStrings;

        function TryInitializeOnce() : boolean;
        procedure LoadConfig();

      public
        constructor Create(const AConfigFileName : TFileName);
        destructor Destroy(); override;

        function TryProcessModule(const AModuleInfo : IOTAModuleInfo) : Boolean;

        property ConfigFileName : TFileName  read FConfigFileName;
        property Stages : TSANPreprocessorStages  read FStages;
        property SourceCode : TStrings read FSourceCode;
      end;

  strict private
    FStagesTypes : TSANPreprocessorStagesTypes;

  protected
    class var Instance : TSANPreprocessor;

  protected
    {$REGION 'IOTAWizard'}
    function GetIDString: string;
    function GetName: string;
    {$ENDREGION}

    procedure ActUpdate(Sender : TObject); override;
    procedure DoActExecute(const AAction : TAction; const AMethod : TMethod); override;
    procedure DoAfterCreateAction(const AAction : TAction); override;


  public
    constructor Create(const AActionsPrefix, AActionsFirstName : string;
      const AMenuItemImageName : string = ''; const AMenuItemImageMaskColor : TColor = clWhite);
    destructor Destroy; override;

    /// <summary>
    ///   Произвести препроцессинг исходного кода в файлах проекта с расширением SAN_PREPROCESSOR_SOURC_FILE_EXT.
    /// </summary>
    procedure PreprocessSourceCode(const AProject : IOTAProject);

    /// <summary>
    ///   Типы этапов.
    /// </summary>
    property StagesTypes : TSANPreprocessorStagesTypes  read FStagesTypes;

  published
    [AER(MI_SAN_PREPROCESSOR_ENABLED_CAPTION)]
    procedure P_Enabled(const AAction : TAction);

  end;

  /// <summary>
  ///   Перехватчик событий компиляции.
  /// </summary>
  TSANPreprocessorCompileNotifier = class(TNotifierObject, IOTACompileNotifier)
  strict private
    class var FNtfHandle : integer;

  protected
    {$REGION 'IOTACompileNotifier'}
    procedure ProjectCompileStarted(const Project: IOTAProject; Mode: TOTACompileMode);
    procedure ProjectCompileFinished(const Project: IOTAProject; Result: TOTACompileResult);
    procedure ProjectGroupCompileStarted(Mode: TOTACompileMode);
    procedure ProjectGroupCompileFinished(Result: TOTACompileResult);
    {$ENDREGION}

  public
    class constructor Create;
    class procedure RegNotifier;
    class procedure UnRegNotifier;

  end;

{$REGION 'resourcestring'}
resourcestring
  sPreprocessorStageLibFuncNotFoundFmt =
    'Function "%s" not found in library "%s"';

  sPreprocessorStageUnknownTypeFmt =
    'Unknown preprocessor stage type "%s"';

  sPreprocessorStageUnexpectedEndOfCfgLineFmt =
    'Unexpected end of config line "%s"';

  sPreprocessorChangesNotApplyedForModuleFmt =
    'Changes not applyed for module "%s"';
{$ENDREGION}

procedure Register;

implementation

{$IFDEF LOGGING}
uses
  System.SyncObjs, System.DateUtils;
{$ENDIF}

{$I ..\..\shared\San.ToolsApiEx.Impl.inc}

{$I ..\..\shared\San.Experts.RTTIActionsWizard.Impl.inc}

{$IFDEF LOGGING}
{$I San.Logger.Embedded.inc}

var
  Log : ISANLog;
{$ENDIF}

{$REGION 'const'}
const
  MI_SAN_PREPROCESSOR_NAME     = '_9FABCCFEFD264DF4B035B45C396474DA';
  MI_SAN_PREPROCESSOR_CAPTION  = 'Preprocessor';
  P_PREFIX                     = 'P_';
{$ENDREGION}


{$REGION 'proc&func'}
procedure Register;
{$IFDEF LOGGING}
var
  LogFileName : string;
begin
  LogFileName := TPath.ChangeExtension( GetModuleName(hInstance), '.log');

  MessageBox(0, PChar(LogFileName), 'Logging enabled, log:', MB_ICONINFORMATION);

  Log := Api.Add('SPP', LogFileName);

{$ELSE}
begin
{$ENDIF}

  TSANPreprocessor.Instance := TSANPreprocessor.Create(P_PREFIX, MI_SAN_PREPROCESSOR_NAME);

  // Reg All Stage Types here
  TSANPreprocessor.Instance.StagesTypes.Add(TSANPreprocessorStageBPL);


  RegisterPackageWizard(TSANPreprocessor.Instance);
end;

function CfgStrOffset(const AConfigStr : string; const AInitValue : integer) : integer;
begin
  Result := AInitValue;

  repeat

    if Result > Length(AConfigStr) then
      raise ESANPreprocessorStage.CreateResFmt(@sPreprocessorStageUnexpectedEndOfCfgLineFmt, [AConfigStr]);

    if AConfigStr[Result] = ' ' then
      Inc(Result)
    else
      Break;

  until False;

end;
{$ENDREGION}

{$REGION 'TSANPreprocessorStageCustom'}
class function TSANPreprocessorStageCustom.TryReadCfgStr(
  const AConfigStr: string; out AStage: TSANPreprocessorStageCustom): boolean;
begin
  AStage := nil;
  Result := False;
end;
{$ENDREGION}

{$REGION 'TSANPreprocessorStagesTypes'}
function TSANPreprocessorStagesTypes.TryReadCfgStr(const AConfigStr: string;
  out AStage: TSANPreprocessorStageCustom): boolean;
var
  Item : TSANPreprocessorStageClass;
begin
  for Item in Self do
    if Item.TryReadCfgStr(AConfigStr, AStage) then
      Exit(True);

  AStage := nil;
  Result := False;
end;
{$ENDREGION}

{$REGION 'TSANPreprocessorStages'}
procedure TSANPreprocessorStages.LoadConfig(const AFileName: TFileName);
var
  SL : TStrings;
  i : integer;
  S : string;
  Stage : TSANPreprocessorStageCustom;
begin
  SL := TStringList.Create();
  try

    SL.LoadFromFile(AFileName);

    for i := 0 to SL.Count - 1 do
    begin
      S := Trim(SL[i]);

      {$IFDEF LOGGING}Log.Writeln(S);{$ENDIF}

      if Pos(SAN_PREPROCESSOR_PROJ_CONFIG_COMMENT, S) = 1 then
        Continue;

      {$IFDEF LOGGING}Log.Writeln('Not comment');{$ENDIF}

      if TSANPreprocessor.Instance.StagesTypes.TryReadCfgStr(S, Stage) then
        Add(Stage)
      else
        raise ESANPreprocessorStage.CreateResFmt(@sPreprocessorStageUnknownTypeFmt, [S]);
    end;

  finally
    FreeAndNil(SL);
  end;
end;

procedure TSANPreprocessorStages.PreprocessAll(const ASourceCode: TStrings);
var
  Item : TSANPreprocessorStageCustom;
begin
  for Item in Self do
    Item.Preprocess(ASourceCode);
end;
{$ENDREGION}

{$REGION 'TSANPreprocessorStageLib'}
constructor TSANPreprocessorStageLib.Create(const AFileName: TFileName;
  const AFuncName: string);
begin
  inherited Create();

  FFileName := AFileName;
  FFuncName := AFuncName;
end;

procedure TSANPreprocessorStageLib.Preprocess(const ASourceCode: TStrings);
var
  FuncAddr : Pointer;
begin
  Load(FHandle);
  try

     {$IFDEF LOGGING}Log.WritelnFmt('TSANPreprocessorStageLib.Preprocess: FFuncName: "%s"', [FFuncName]);{$ENDIF}

     FuncAddr := GetProcAddress(FHandle, PChar(FFuncName));

     {$IFDEF LOGGING}Log.WritelnFmt('TSANPreprocessorStageLib.Preprocess: FuncAddr: $%p', [FuncAddr]);{$ENDIF}

     if not Assigned(FuncAddr) then
       raise ESANPreprocessorStage.CreateResFmt(@sPreprocessorStageLibFuncNotFoundFmt, [FFuncName, FFileName]);

     CallFromLib(FuncAddr, ASourceCode);

  finally
    UnLoad(FHandle);
  end;
end;

class function TSANPreprocessorStageLib.TryReadCfgStr(const AConfigStr: string;
  out AStage: TSANPreprocessorStageCustom): boolean;
var
  S, FuncName, FileName : string;
  P, Offset : integer;
begin
  AStage := nil;

  // Первое слово - тип
  S := AnsiUpperCase(TryReadCfgStrStart());
  if Pos(S, AnsiUpperCase(AConfigStr) ) <> 1 then
    Exit(False);

  Offset := CfgStrOffset(AConfigStr, Length(S) + 1);

  // Второе слово - имя функции
  P := PosEx(' ', AConfigStr, Offset);
  if P = -1 then
    Exit(False);

  FuncName := Copy(AConfigStr, Offset, P - Offset);

  Offset := CfgStrOffset(AConfigStr, P);

  // Далее имя файла
  FileName := Copy(AConfigStr, Offset);


  AStage := Self.Create(FileName, FuncName);
  Result := True;
end;

class function TSANPreprocessorStageLib.TryReadCfgStrStart: string;
begin
  Result := '';
end;
{$ENDREGION}

{$REGION 'TSANPreprocessorStageBPL'}
class function TSANPreprocessorStageBPL.TryReadCfgStrStart: string;
begin
  Result := 'BPL';
end;

procedure TSANPreprocessorStageBPL.Load(out AHandle: THandle);
begin
  AHandle := LoadPackage(FileName);
end;

procedure TSANPreprocessorStageBPL.UnLoad(const AHandle: THandle);
begin
  UnloadPackage(AHandle);
end;

procedure TSANPreprocessorStageBPL.CallFromLib(const AFuncAddr : Pointer;
  const ASourceCode : TStrings);
var
  Func : TBPLFunc;
begin
  Func := TBPLFunc(AFuncAddr);

  Func(ASourceCode);
end;
{$ENDREGION}

{$REGION 'TSANPreprocessor.TPreprocessingContext'}
constructor TSANPreprocessor.TPreprocessingContext.Create(
  const AConfigFileName: TFileName);
begin
  inherited Create();

  FConfigFileName := AConfigFileName;
end;

destructor TSANPreprocessor.TPreprocessingContext.Destroy();
begin
  FreeAndNil(FStages);
  FreeAndNil(FSourceCode);

  inherited;
end;

function TSANPreprocessor.TPreprocessingContext.TryInitializeOnce() : boolean;
begin

  if Assigned(FStages) then
    Exit(False);

  FStages := TSANPreprocessorStages.Create();
  FSourceCode := TStringList.Create();

  LoadConfig();

  Result := True;
end;

procedure TSANPreprocessor.TPreprocessingContext.LoadConfig();
begin
  Stages.LoadConfig(FConfigFileName);
end;

function TSANPreprocessor.TPreprocessingContext.TryProcessModule(
  const AModuleInfo: IOTAModuleInfo): Boolean;

  function SetDestFileName(out ADestFileName : TFileName) : Boolean;
  begin
    ADestFileName := TPath.ChangeExtension(AModuleInfo.FileName, '.pas');
    Result := False;
  end;

  function TrySaveOpened(const ADestFileName : TFileName; const AOldSourceCode : string) : Boolean;
  var
    Module      : IOTAModule;
    EditBuffer  : IOTAEditBuffer;
    Writer      : IOTAEditWriter;
  begin
    Result := TryFindOpenModuleByFileName(ADestFileName, Module)
          and GetEditBuffer(Module, EditBuffer);

    if not Result then
      Exit;

    Writer := EditBuffer.CreateWriter;
    Writer.DeleteTo(Length(AOldSourceCode));
    Writer.Insert(PAnsiChar(AnsiString(SourceCode.Text)));
  end;

  function TrySaveClosed(const ADestFileName : TFileName) : Boolean;
  begin
    SourceCode.SaveToFile(ADestFileName);
    Result := True;
  end;

var
  OldSourceCode : string;
  DestFileName : TFileName;
begin

  if not AnsiSameText( TPath.GetExtension(AModuleInfo.FileName), SAN_PREPROCESSOR_SOURC_FILE_EXT) then
    Exit(False);

  TryInitializeOnce();

  SourceCode.LoadFromFile(AModuleInfo.FileName);
  OldSourceCode := SourceCode.Text;

  Stages.PreprocessAll(SourceCode);

  Result := AnsiSameStr(OldSourceCode, SourceCode.Text)
         or SetDestFileName(DestFileName)
         or TrySaveOpened(DestFileName, OldSourceCode)
         or TrySaveClosed(DestFileName);

  if not Result then
    raise ESANPreprocessor.CreateResFmt(@sPreprocessorChangesNotApplyedForModuleFmt, [AModuleInfo.FileName]);
end;

{$ENDREGION}

{$REGION 'TSANPreprocessor'}
procedure TSANPreprocessor.ActUpdate(Sender: TObject);
var
  Act : TAction;
begin
  Act := Sender as TAction;

  if not SameText(Act.Caption, MI_SAN_PREPROCESSOR_ENABLED_CAPTION) then
    Act.Enabled := GetActiveProject <> nil;
end;

constructor TSANPreprocessor.Create(const AActionsPrefix, AActionsFirstName,
  AMenuItemImageName: string; const AMenuItemImageMaskColor: TColor);
begin
  inherited Create(AActionsPrefix, AActionsFirstName, AMenuItemImageName, AMenuItemImageMaskColor);

  FStagesTypes := TSANPreprocessorStagesTypes.Create();
end;

destructor TSANPreprocessor.Destroy;
begin
  TSANPreprocessorCompileNotifier.UnRegNotifier;

  FreeAndNil(FStagesTypes);

  inherited;
end;

procedure TSANPreprocessor.DoActExecute(const AAction: TAction;
  const AMethod: TMethod);
type
  TActionMethod = procedure (const AAction: TAction) of object;
begin
  TActionMethod(AMethod)(AAction);
end;

procedure TSANPreprocessor.DoAfterCreateAction(const AAction: TAction);
begin
  if SameText(AAction.Caption, MI_SAN_PREPROCESSOR_ENABLED_CAPTION) then
    AAction.AutoCheck := True;
end;

procedure TSANPreprocessor.P_Enabled(const AAction : TAction);
begin
  if AAction.Checked then
    TSANPreprocessorCompileNotifier.RegNotifier
  else
    TSANPreprocessorCompileNotifier.UnRegNotifier;
end;

function TSANPreprocessor.GetIDString: string;
begin
  Result := '{99972E84-7E27-424E-92F7-17EDD0BBAA35}'
end;

function TSANPreprocessor.GetName: string;
begin
  Result := MI_SAN_PREPROCESSOR_CAPTION;
end;

procedure TSANPreprocessor.PreprocessSourceCode(const AProject: IOTAProject);
var
  StagesCfgFile  : TFileName;
  Context        : TPreprocessingContext;
  i              : integer;
begin

  StagesCfgFile := TPath.ChangeExtension(AProject.FileName, SAN_PREPROCESSOR_PROJ_CONFIG_EXT);

  {$IFDEF LOGGING}Log.WritelnFmt('PreprocessSourceCode: StagesCfgFile: "%s"', [StagesCfgFile]);{$ENDIF}

  if not TFile.Exists(StagesCfgFile) then
    Exit;

  {$IFDEF LOGGING}Log.Writeln('PreprocessSourceCode: StagesCfgFile exists');{$ENDIF}

  Context := TPreprocessingContext.Create(StagesCfgFile);
  try

    for i := 0 to AProject.GetModuleCount - 1 do
      Context.TryProcessModule( AProject.GetModule(i) );

  finally
    FreeAndNil(Context);
  end;

end;
{$ENDREGION}

{$REGION 'TSANPreprocessorCompileNotifier'}
procedure TSANPreprocessorCompileNotifier.ProjectCompileStarted(
  const Project: IOTAProject; Mode: TOTACompileMode);
begin
  {$IFDEF LOGGING}Log.WritelnFmt('ProjectCompileStarted for "%s"', [Project.FileName]);{$ENDIF}

  TSANPreprocessor.Instance.PreprocessSourceCode(Project);

//  Application.MessageBox('ProjectCompileStarted', '', MB_ICONINFORMATION);
end;

procedure TSANPreprocessorCompileNotifier.ProjectCompileFinished(
  const Project: IOTAProject; Result: TOTACompileResult);
begin
//  if Result = crOTASucceeded then

//  Application.MessageBox('ProjectCompileFinished', '', MB_ICONINFORMATION);
end;

procedure TSANPreprocessorCompileNotifier.ProjectGroupCompileStarted(
  Mode: TOTACompileMode);
begin
end;

procedure TSANPreprocessorCompileNotifier.ProjectGroupCompileFinished(
  Result: TOTACompileResult);
begin
end;

class constructor TSANPreprocessorCompileNotifier.Create;
begin
  FNtfHandle := -1;
end;

class procedure TSANPreprocessorCompileNotifier.RegNotifier;
var
  CompileServices : IOTACompileServices;
begin

  if not Supports(BorlandIDEServices, IOTACompileServices, CompileServices) then
    Exit;

  FNtfHandle := CompileServices.AddNotifier(Self.Create);

  {$IFDEF LOGGING}Log.WritelnFmt('Added: Compile notifier id %d', [FNtfHandle]);{$ENDIF}
end;

class procedure TSANPreprocessorCompileNotifier.UnRegNotifier;
var
  CompileServices : IOTACompileServices;
begin

  if not Supports(BorlandIDEServices, IOTACompileServices, CompileServices)
  or (FNtfHandle < 0)
  then
    Exit;

  CompileServices.RemoveNotifier(FNtfHandle);

  {$IFDEF LOGGING}Log.WritelnFmt('Removed: Compile notifier id %d', [FNtfHandle]);{$ENDIF}

  FNtfHandle := -1;
end;
{$ENDREGION}


end.
