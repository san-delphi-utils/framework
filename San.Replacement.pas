{*******************************************************}
{                                                       }
{       Замена в тексте.                                }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Замена в тексте.
/// </summary>
unit San.Replacement;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils
  {$ENDREGION}
  ;
{$ENDREGION}
{$ENDIF}

type
  /// <summary>
  ///   Информация о замене.
  /// </summary>
  TReplacement =
  {$IFDEF USE_ALIASES}
    San.Replacement.TReplacement;
  {$ELSE}
  record
    /// <summary>
    ///   Что заменить.
    /// </summary>
    F : string;

    /// <summary>
    ///   Чем заменить.
    /// </summary>
    T : string;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
const
  HTML_REPLACEMENTS : array[0..2] of TReplacement =
    (
        (F: '&';    T: '&amp;')
      , (F: '<';    T: '&lt;')
      , (F: '>';    T: '&gt;')
    );

  JSON_REPLACEMENTS : array[0..4] of TReplacement =
  (
      (F: '\';    T: '\\')
    , (F: '"';    T: '\"')
    , (F: #13;    T: '\r')
    , (F: #10;    T: '\n')
    , (F: #9;     T: '\t')
  );

type
  /// <summary>
  ///   Замена в тексте.
  /// </summary>
  Api = record
  public
    class function ReplaceBy(const AString : string; const AReplacements : array of TReplacement) : string; static;
    class function HTMLStrReplace(const AString : string) : string; inline; static;
    class function JSONStrReplace(const AString : string) : string; inline; static;
  end;

implementation

{$REGION 'Api'}
class function Api.HTMLStrReplace(const AString: string): string;
begin
  Result := ReplaceBy(AString, HTML_REPLACEMENTS);
end;

class function Api.JSONStrReplace(const AString: string): string;
begin
  Result := ReplaceBy(AString, JSON_REPLACEMENTS);
end;

class function Api.ReplaceBy(const AString: string;
  const AReplacements: array of TReplacement): string;
var
  i : integer;
begin
  Result := AString;

  for i := Low(AReplacements) to High(AReplacements) do
    Result := StringReplace(Result, AReplacements[i].F, AReplacements[i].T, [rfReplaceAll, rfIgnoreCase]);
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
