{*******************************************************}
{                                                       }
{       Работа с интерфейсами.                          }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2017              Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   Сообщения.
/// </summary>
unit San.Messages;
{$I San.inc}
interface

resourcestring
  {$REGION 'San.Intfs'}
  s_San_Intfs_InterfaceMustSupportsErr_Interface =
    'интерфейсом';

  s_San_Intfs_InterfaceMustSupportsErr_Object =
    'объектом класса';

  s_San_Intfs_InterfaceMustSupportsErr_Class =
    'классом';

  s_San_Intfs_InterfaceMustSupportsErrFmt =
    'Интерфейс "%s" {%s} не поддерживается %s "%s"';
  {$ENDREGION}

  {$REGION 'San.Rtti'}
  s_San_Rtti_MethodNotFoundForReturnAddressErrorFmt =
    'Rtti method not found for return addrres %p';

  s_San_Rtti_TypeNotFoundByNameErrorFmt =
    'Rtti type not found by name %s';
  {$ENDREGION}

  {$REGION 'San.Events'}
  s_San_Events_ErrorForSubscraberFmt =
    'Ошибка для %s';

  s_San_Events_ErrorEventFiringForSubscraberFmt =
    'Ошибка при вызове события %s. Подписчик: %s';

  s_San_Events_InternalErrorDescriptionGettingFmt =
    'Внутренняя ошибка (%s) при получении описания обработчика: "%s". Описание: "%s"';

  s_San_Events_NilAsSubscriber =
    'Nil в качестве данных подписчика';

  s_San_Events_IntfSubscriberNotSupported =
    'Интерфейс IEventsSubscriber не поддерживается';

  s_San_Events_SubscriberGuardNotInitialized =
    'Не инициализирован сторож подписки (Guard)';

  s_San_Events_SubscribeRule =
    'Подписаться на событие может только существующий объект, реализующий интерфейс IEventsSubscriber и имеющий инициализированного сторожа (Guard).';

  s_San_Events_SupposedSubscriberFmt =
    'Предполагаемый подписчик: %s';

  s_San_Events_ErrorAddingMetaEventToSpaceFmt =
    'Ошибка добавления метасобытия %s в пространство событий';

  s_San_Events_NeedOverrideMethodForFmt =
    'Требуется переопределить метод "%s" для класса "%s"';

  s_San_Events_GenСlassMustBeInheritedFromFmt =
    'Класс генерика "%s" должен быть уноследован от класса "%s"';
  {$ENDREGION}

  {$REGION 'San'}
  s_San_TypeParamMustBeClassFmt =
    'Типовой параметр генерика должен быть классом. Вместо этого задан тип "%s" разновидности "%s"';

  s_San_ClassMustBeInheritedFromFmt =
    'Класс "%s" должен быть унаследован от "%s"';

  s_San_InternalErrorForFmt =
    'Внутренняя ошибка для "%s"';

  s_San_InternalErrorForWithDetailsFmt =
    'Внутренняя ошибка для "%s", детали '#13#10'%s';
  {$ENDREGION}

  sNilNotInheritsFromYYYFmt =
    'Nil не унаследован от %s.%s';

  sClassXXXNotInheritsFromYYYFmt =
    'Класс %s.%s не унаследован от %s.%s';

  sUnexpectedWaitResultFmt =
    'Неожиданный результат (%d) ожидания события(-й) WaitForSingle/MultipleObjects';

  sMethodMustBeOverriddenFmt =
    '%s.%s.%s must be overridden';

implementation

end.

