{*******************************************************}
{                                                       }
{       Геннерики. Реализация                           }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2014-2018         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$REGION 'TTextObjectDictionary<TValue>'}
constructor TTextObjectDictionary<TValue>.Create(ACapacity: Integer);
begin
  inherited Create(ACapacity, TUpperStringEqualityComparer.Create());
end;

constructor TTextObjectDictionary<TValue>.Create(
  Collection: TEnumerable<TPair<TUpperString, TValue>>);
begin
  inherited Create(Collection, TUpperStringEqualityComparer.Create());
end;

procedure TTextObjectDictionary<TValue>.Add(const Key: TUpperString;
  const Value: TValue);
begin
  try

    inherited Add(Key, Value);

  except
    Exception.RaiseOuterException(EListError.CreateFmt('Key = %s', [Key.ToString()]));
  end;
end;
{$ENDREGION}

{$REGION 'TIntfObjList<T>'}
procedure TIntfObjList<T>.Notify(const Item: T; Action: TCollectionNotification);
var
  Obj : TInterfacedObjectHack;
begin
  Obj := TInterfacedObjectHack(Item);

  if Action = TCollectionNotification.cnAdded then
  begin
    Obj._AddRef;
    inherited;
  end

  else
  begin
    inherited;
    Obj._Release;
  end;
end;
{$ENDREGION}
