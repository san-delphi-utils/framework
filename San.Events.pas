{*******************************************************}
{                                                       }
{       События и подписки.                             }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2014-2016         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Список обработчиков событий.
/// </summary>
unit San.Events;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Classes, System.Generics.Collections, System.StrUtils,
  System.Math, System.Rtti, System.Variants, System.TypInfo,
  {$ENDREGION}

  {$REGION 'San'}
  San.Utils,
  San.UpperString,
  San.Excps,
  San.Intfs.Basic,
  San.Intfs,
  San.Objs,
  San.Rtti,
  San.Events.Handlers.Generic
  {$ENDREGION}
  ;
{$ENDREGION}

type
  TMetaEvent = class;
  TEventSpace = class;
{$ELSE}
type
{$ENDIF}

  TMetaEventClass =
  {$IFDEF USE_ALIASES}
    San.Events.TMetaEventClass;
  {$ELSE}
    class of TMetaEvent;
  {$ENDIF}

  TMetaEventSeparateClass =
  {$IFDEF USE_ALIASES}
    San.Events.TMetaEventSeparateClass;
  {$ELSE}
    class of TMetaEventSeparate;
  {$ENDIF}

  /// <summary>
  ///   Базовое исключение системы событий.
  /// </summary>
  EEvents =
  {$IFDEF USE_ALIASES}
    San.Events.EEvents;
  {$ELSE}
    class(ESanBasic);
  {$ENDIF}

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Данные обработчика.
  /// </summary>
  THandlerData = record
  private
    ///	<summary>
    ///	  Метод обработчика.
    ///	</summary>
    FMethod : TMethod;

    ///	<summary>
    ///	  Фильтр по отправителю.
    ///	</summary>
    ///	<remarks>
    ///	   nil - без фильтрации, иначе обработчик будет вызываться
    ///    ТОЛЬКО для этого Sender-а.
    ///	</remarks>
    FSender : Pointer;

    /// <summary>
    ///   Rtti-объект метода. Для отладочных целей.
    /// </summary>
    /// <remarks>
    ///   Заполняется по необходимости.
    /// </remarks>
    FRttiMethod : TRttiMethod;

  public
    class operator Equal(const AItem1, AItem2 : THandlerData) : boolean;

    /// <summary>
    ///   Создать экземпляр пустой записи.
    /// </summary>
    class function Empty : THandlerData; static;

    /// <summary>
    ///   Создать экземпляр, заполнив поля.
    /// </summary>
    class function Create(const ASender : Pointer; const AMethod : TMethod) : THandlerData; static;

    /// <summary>
    ///   Признак: можно ли считать запись пустой.
    /// </summary>
    function IsEmpty : boolean; inline;

    /// <summary>
    ///   Описание одработчика.
    /// </summary>
    /// <remarks>
    ///   Используется для отладки.
    /// </remarks>
    function Description(const ASeporator : string = '; ') : string;
  end;

  /// <summary>
  ///   Элемент списка обработчиков.
  /// </summary>
  THandlerItem = San.Events.Handlers.Generic.THandlerItem<THandlerData>;

  /// <summary>
  ///   Список обработчиков.
  /// </summary>
  THandlers = class(San.Events.Handlers.Generic.THandlers<THandlerData>)
  strict private

  protected
    // TODO: Move to ~WriteToLog();
    //procedure THandlers.DoLogToFileHeader(var AWriter);
    //begin
    //  WriteLn(Text(AWriter), 'Idx   FixPos     GenPos     Sender   Code     Data     Desc/ExtInfo');
    //end;

    //procedure THandlers.DoLogToFile(var AWriter; const AIndex : integer;
    //  const ARec : THandlerItem);
    //begin
    //  WriteLn(Text(AWriter), Format('%.4d %s%.10d %.10d %p %p %p %s', [AIndex, IfThen(ARec.FixedPosition = 0, ' ', IfThen(ARec.FixedPosition > 0, '+')), ARec.FixedPosition, Abs(ARec.GenericPosition), ARec.event.FSender, ARec.event.FCode, ARec.event.FData, ARec.event.GetExtInfo]))
    //end;

  public
    class function IsEmptyItem(const AItem : THandlerData) : boolean; override;
    class function IsEqualsItems(const AItem1, AItem2 : THandlerData) : boolean; override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Интерфейс cторож подписки.
  ///   <para>
  ///     Интерфейс объекта, отвечающего за отписывание от событий при разрушении подписчика.
  ///   </para>
  /// </summary>
  /// <remarks>
  ///   Не рекомендуется реализовывать этот интерфейс самостоятельно (хотя иногда это оправдано).
  ///   <para>
  ///     Имплементации следует создавать функцией CreateEventsSubscribeGuard.
  ///   </para>
  /// </remarks>
  IEventsSubscribeGuard =
  {$IFDEF USE_ALIASES}
    San.Events.IEventsSubscribeGuard;
  {$ELSE}
    interface(IInterface)
  ['{E4918139-FA51-4A56-9B2C-6F8FB7B7E5D3}']
  end;
  {$ENDIF}

  /// <summary>
  ///   Интерфейс подписчика на событие.
  ///   <para>
  ///     Подписчик должен предъявить сторожа, чтобы подписаться.
  ///   </para>
  /// </summary>
  IEventsSubscriber =
  {$IFDEF USE_ALIASES}
    San.Events.IEventsSubscriber;
  {$ELSE}
    interface(IInterface)
  ['{191D2764-F080-4D0F-8EB7-150BD3346D7F}']
    {$REGION 'Gets&Sets'}
    function GetGuard : IEventsSubscribeGuard;
    {$ENDREGION}

    /// <summary>
    ///   Сторож для данного подписчика.
    /// </summary>
    property Guard : IEventsSubscribeGuard  read GetGuard;
  end;
  {$ENDIF}

  /// <summary>
  ///   Метаданные события. ~Тип события. ~Класс события.
  /// </summary>
  TMetaEvent =
  {$IFDEF USE_ALIASES}
    San.Events.TMetaEvent;
  {$ELSE}
    class abstract(TObject)
  private
    {$REGION 'type'}
    type
      /// <summary>
      ///   Событие. Реализация.
      /// </summary>
      EventImpl = class(TInterfacedObject, IObjRef)
      private
        FMetaEvent : TMetaEvent;
        FSender, FContext : TObject;
        FHandled : boolean;
        FExceptions : TExceptionList;

      protected
        {$REGION 'IObjRef'}
        function GetObjRef : TObject;
        {$ENDREGION}

      public
        procedure BeforeDestruction(); override;
        destructor Destroy(); override;

        procedure AcquireExcp();
      end;
    {$ENDREGION}

  public
    {$REGION 'type'}
    type
      /// <summary>
      ///   Событие с генериком контекста.
      /// </summary>
      Event<T : class> = record
      private
        FInnerData : IObjRef;

      strict private
        {$REGION 'Gets&Sets'}
        function GetMetaEvent: TMetaEvent;
        function GetSender : TObject;
        function GetContext : T;
        function GetHandled : boolean;
        procedure SetHandled(const AValue : boolean);
        function GetExceptions : TExceptionList;
        {$ENDREGION}

      public
        /// <summary>
        ///   Метаданные события.
        /// </summary>
        property MetaEvent : TMetaEvent  read GetMetaEvent;

        /// <summary>
        ///   Отправитель события.
        /// </summary>
        property Sender : TObject  read GetSender;

        /// <summary>
        ///   Данные контекста события.
        /// </summary>
        /// <remarks>
        ///   Объект контекста в собственности события!
        /// </remarks>
        property Context : T  read GetContext;

        /// <summary>
        ///   Признак остановки дальнейшей обработки.
        /// </summary>
        property Handled : boolean  read GetHandled  write SetHandled;

        /// <summary>
        ///   Произошедшие ошибки, для режима Safe.
        /// </summary>
        property Exceptions : TExceptionList  read GetExceptions;
      end;

      /// <summary>
      ///   Простое событие.
      /// </summary>
      Event = TMetaEvent.Event<TObject>;

      /// <summary>
      ///   Подтипы.
      /// </summary>
      Subtypes = record
      public
        type
          /// <summary>
          ///   Прототип обработки события с генериком контекста.
          /// </summary>
          HandlerPrototype<T : class> = procedure (const AEvent : TMetaEvent.Event<T>) of object;

          /// <summary>
          ///   Прототип обработки простого события.
          /// </summary>
          // А вот это считается другим прототипом:
          //   HandlerPrototypeSimple = HandlerPrototypeGen<TObject>;
          HandlerPrototype = procedure (const AEvent : TMetaEvent.Event) of object;

          /// <summary>
          ///   Ссылка на процедуру обработки события каждым перехватчиком.
          /// </summary>
          HandleEventReference = reference to procedure (const AHandlerData : THandlerData;
            const AEvent : TMetaEvent.Event);

          /// <summary>
          ///   Фаринг.
          /// </summary>
          /// <remarks>
          ///   FEvent будет существовать по крайней мере до очистки экземпляра TFiring.
          ///   А может и дольше: смотря как использовать результат Std(), Safe() или Ex().
          /// </remarks>
          TFiring = record
          strict private
            procedure DoFiring(const ASafe : boolean; const ABeforeHandlerExec, AAfterHandlerExec : HandleEventReference;
              out AEvent : TMetaEvent.Event);

          private
            FEvent : IObjRef;

          public
            /// <summary>
            ///   Стандартный фаринг события.
            /// </summary>
            /// <remarks>
            ///   Исключения прерывают фаринг.
            /// </remarks>
            function Std() : TMetaEvent.Event;

            /// <summary>
            ///   Фаринг события с учетом исключений.
            /// </summary>
            /// <remarks>
            ///   Все исключения складываются в Result.Exceptions.
            /// </remarks>
            function Safe() : TMetaEvent.Event;

            /// <summary>
            ///   Расширенный фаринг события.
            /// </summary>
            /// <param name="ABeforeHandlerExec">
            ///   Если ссылка задана — вызывается перед выполнением каждого обработчика.
            /// </param>
            /// <param name="AAfterHandlerExec">
            ///   Если ссылка задана — вызывается после выполнением каждого обработчика.
            /// </param>
            /// <remarks>
            ///   Исключения прерывают фаринг.
            /// </remarks>
            function Ex(const ABeforeHandlerExec, AAfterHandlerExec : HandleEventReference) : TMetaEvent.Event;
          end;

          /// <summary>
          ///   Подписывание на обработчики.
          /// </summary>
          TSubscribing = record
          private
            FMetaEvent : TMetaEvent;
            FEventHandler: TMethod;

          public
            /// <summary>
            ///   Подписать обработчик события.
            /// </summary>
            procedure Std();

            /// <summary>
            ///   Подписать обработчик события c позицией.
            /// </summary>
            procedure AtPos(const AEventPosition: Integer);

            /// <summary>
            ///   Подписать обработчик события с фильтрацией.
            /// </summary>
            procedure Sender(const ASenderFilter: TObject);

            /// <summary>
            ///   Подписать обработчик события c позицией и фильтрацией.
            /// </summary>
            procedure Ex(const AEventPosition: Integer;
              const ASenderFilter : TObject); overload;

            /// <summary>
            ///   Подписать обработчик события c фильтрацией и позицией.
            /// </summary>
            procedure Ex(const ASenderFilter : TObject;
              const AEventPosition: Integer); overload;
          end;

          /// <summary>
          ///   Отписывание от обработчиков.
          /// </summary>
          TUnSubscribing = record
          strict private
            type
              TMatchFunc = reference to function (const AHandlerData : THandlerData) : boolean;
            procedure DoUnSubscribe(const AMatchFunc : TMatchFunc);

          private
            FMetaEvent : TMetaEvent;

          public
            /// <summary>
            ///   Отписать один обработчик события.
            /// </summary>
            /// <param name="ASenderFilter">
            ///   Признак фильтрации по отправителю.
            /// </param>
            /// <param name="AEventHandlerCode">
            ///   Для указанного метода.
            /// </param>
            /// <param name="AEventHandlerData">
            ///   Для указанного объекта.
            /// </param>
            /// <remarks>
            ///   One и All схожи в реализации. Различие только в условии.
            /// </remarks>
            procedure One(const ASenderFilter : TObject; const AEventHandlerCode, AEventHandlerData : Pointer); overload;

            /// <summary>
            ///   Отписать один обработчик события.
            /// </summary>
            /// <param name="ASenderFilter">
            ///   Признак фильтрации по отправителю.
            /// </param>
            /// <param name="AEventHandler">
            ///   Для указанного метода.
            /// </param>
            /// <remarks>
            ///   One и All схожи в реализации. Различие только в условии.
            /// </remarks>
            procedure One(const ASenderFilter : TObject; const AEventHandler : TMethod); overload; inline;

            /// <summary>
            ///   Отписать все обработчики событий, для конкретного Sender.
            /// </summary>
            procedure Sender(const ASenderFilter: TObject);

            /// <summary>
            ///   Отписать все обработчики событий, для данного объекта.
            /// </summary>
            /// <param name="ASubscriber">
            ///   Для указанного объекта (подписчика).
            /// </param>
            /// <remarks>
            ///   One и All схожи в реализации. Различие только в условии.
            /// </remarks>
            procedure All(const ASubscriber : TObject); overload; inline;

            /// <summary>
            ///   Отписать все обработчики событий, для данного объекта.
            /// </summary>
            /// <param name="AEventHandlerData">
            ///   Для указанного объекта.
            /// </param>
            /// <remarks>
            ///   One и All схожи в реализации. Различие только в условии.
            /// </remarks>
            procedure All(const AEventHandlerData : Pointer); overload;

          end;

          /// <summary>
          ///   Атрибут метода для RTTI подписки.
          /// </summary>
          SubscribeAttribute = class(TCustomAttribute)
          private
            FFilterCode, FPosition: integer;

          public
            /// <summary>
            ///   Конструктор.
            /// </summary>
            /// <param name="AFilterCode">
            ///   Код фильтрации позволяет обрабатывать только группу методов.
            /// </param>
            /// <param name="APosition">
            ///   Позиция подписки.
            /// </param>
            constructor Create(const AFilterCode, APosition : integer); overload;

            /// <summary>
            ///   Конструктор.
            /// </summary>
            /// <param name="AFilterCode">
            ///   Код фильтрации позволяет обрабатывать только группу методов.
            /// </param>
            constructor Create(const AFilterCode : integer); overload;

            /// <summary>
            ///   Код фильтрации позволяет обрабатывать только группу методов.
            /// </summary>
            property FilterCode : integer  read FFilterCode;

            /// <summary>
            ///   Позиция подписки.
            /// </summary>
            property Position : integer  read FPosition;

          end;

      end;
    {$ENDREGION}

  strict private
    FIsFiring : boolean;
    procedure SetIsFiring(const AValue: boolean);

  private
    FHandlers: THandlers;

  protected
    function GetEventName : string; virtual; abstract;
    function GetDescription : string; virtual; abstract;

    /// <summary>
    ///   Соответствует ли атрибут подписки метасобытию.
    /// </summary>
    function MatchSubscribeAttr(const AAttr : TMetaEvent.Subtypes.SubscribeAttribute) : boolean; virtual;

  public
    constructor Create(); virtual;
    destructor Destroy(); override;

    /// <summary>
    ///   Создать метасобытие и добавить его в пространство событий.
    /// </summary>
    /// <param name="AEventSpace">
    ///   Пространство событий, в которое добавляется метасобытие.
    /// </param>
    /// <param name="AMetaEvent">
    ///   Метасобытие.
    /// </param>
    class procedure CreateInSpace(const AEventSpace : TEventSpace; out AMetaEvent); overload;

    /// <summary>
    ///   Создать метасобытие и добавить его в пространство событий.
    /// </summary>
    /// <param name="AEventSpace">
    ///   Пространство событий, в которое добавляется метасобытие.
    /// </param>
    class procedure CreateInSpace(const AEventSpace : TEventSpace); overload;

    /// <summary>
    ///   Фаринг события...
    /// </summary>
    /// <param name="ASender">
    ///   Отправитель события.
    /// </param>
    /// <param name="AContext">
    ///   Контекст события. Переданный объект БУДЕТ РАЗРУШЕН ВМЕСТЕ С СОБЫТИЕМ!
    /// </param>
    function Fire(const ASender : TObject; const AContext : TObject = nil) : Subtypes.TFiring;

    /// <summary>
    ///   Подписка на событие...
    /// </summary>
    /// <param name="AEventHandler">
    ///   Обработчик.
    /// </param>
    function Subscribe(const AEventHandler : TMethod) : Subtypes.TSubscribing; overload;

    /// <summary>
    ///   Подписка на событие...
    /// </summary>
    /// <param name="AEventHandlerCode">
    ///   Код обработчика.
    /// </param>
    /// <param name="AEventHandlerData">
    ///   Данные обработчика.
    /// </param>
    function Subscribe(const AEventHandlerCode, AEventHandlerData : Pointer) : Subtypes.TSubscribing; overload;

    /// <summary>
    ///   Подписка на событие...
    /// </summary>
    /// <param name="AEventHandler">
    ///   Обработчик.
    /// </param>
    function Subscribe(const AEventHandler: TMetaEvent.Subtypes.HandlerPrototype) : Subtypes.TSubscribing; overload;

    /// <summary>
    ///   Подписка на событие...
    /// </summary>
    /// <param name="T">
    ///   Класс контекста события.
    /// </param>
    /// <param name="AEventHandler">
    ///   Обработчик.
    /// </param>
    function Subscribe<T : class>(const AEventHandler: TMetaEvent.Subtypes.HandlerPrototype<T>) : Subtypes.TSubscribing; overload;

    /// <summary>
    ///   Отписать...
    /// </summary>
    function UnSubscribe : Subtypes.TUnSubscribing;

    /// <summary>
    ///   Имя события.
    /// </summary>
    property EventName : string  read GetEventName;

    /// <summary>
    ///   Идет процесс фаринга, длинна списков не должна сокращаться.
    /// </summary>
    property IsFiring: boolean  read FIsFiring  write SetIsFiring;

    /// <summary>
    ///   Обработчики событий данного типа.
    /// </summary>
    property Handlers: THandlers  read FHandlers;

    /// <summary>
    ///   Описание события.
    /// </summary>
    property Description: string  read GetDescription;
  end;
  {$ENDIF}

  /// <summary>
  ///   Методанные простого события.
  /// </summary>
  TMetaEventSimple =
  {$IFDEF USE_ALIASES}
    San.Events.TMetaEventSimple;
  {$ELSE}
    class(TMetaEvent)
  strict private
    FEventName, FDescription : string;

  protected
    function GetEventName : string; override;
    function GetDescription : string; override;

  public
    /// <summary>
    ///   Конструктор.
    /// </summary>
    /// <param name="AEventName">
    ///   Имя события.
    /// </param>
    /// <param name="ADescription">
    ///   Описание события.
    /// </param>
    constructor Create(const AEventName, ADescription : string); reintroduce;

    /// <summary>
    ///   Создать метасобытие и добавить его в пространство событий.
    /// </summary>
    /// <param name="AEventName">
    ///   Имя события.
    /// </param>
    /// <param name="ADescription">
    ///   Описание события.
    /// </param>
    /// <param name="AEventSpace">
    ///   Пространство событий, в которое добавляется метасобытие.
    /// </param>
    /// <param name="AMetaEvent">
    ///   Метасобытие.
    /// </param>
    class procedure CreateInSpace(const AEventName, ADescription : string;
      const AEventSpace : TEventSpace; out AMetaEvent);
  end;
  {$ENDIF}

  /// <summary>
  ///   Метаданные одиночного события.
  /// </summary>
  /// <remarks>
  ///   У таких событий на каждое метасобытие отдельный класс.
  /// </remarks>
  TMetaEventSeparate =
  {$IFDEF USE_ALIASES}
    San.Events.TMetaEventSeparate;
  {$ELSE}
    class abstract(TMetaEvent)
  protected
    function GetEventName : string; override;
    function GetDescription : string; override;

  public
    /// <summary>
    ///   Имя события.
    /// </summary>
    class function EventName() : string; virtual;

    /// <summary>
    ///   Описание события.
    /// </summary>
    class function Description() : string; virtual;
  end;
  {$ENDIF}

  /// <summary>
  ///   Атрибут параметров метасобытия
  /// </summary>
  MetaEv =
  {$IFDEF USE_ALIASES}
    San.Events.MetaEv;
  {$ELSE}
    class(TCustomAttribute)
  strict private
    FDescription, FEventName: string;

  public
    /// <summary>
    ///   Конструктор.
    /// </summary>
    /// <param name="AEventName">
    ///   Имя события.
    /// </param>
    /// <param name="ADescription">
    ///   Описание события.
    /// </param>
    constructor Create(const AEventName, ADescription : string);

    /// <summary>
    ///   Имя события.
    /// </summary>
    property EventName : string  read FEventName;

    /// <summary>
    ///   Описание события.
    /// </summary>
    property Description: string  read FDescription;
  end;
  {$ENDIF}

  /// <summary>
  ///   Метаданные события с чтением атрибутов.
  /// </summary>
  TMetaEventEx =
  {$IFDEF USE_ALIASES}
    San.Events.TMetaEventEx;
  {$ELSE}
    class (TMetaEvent)
  strict private
    FEventName, FDescription : string;
    FEvAttrClass : TClass;

  protected
    function GetEventName : string; override;
    function GetDescription : string; override;
    function MatchSubscribeAttr(const AAttr : TMetaEvent.Subtypes.SubscribeAttribute) : boolean; override;

  public
    constructor Create(); override;

  end;
  {$ENDIF}

  /// <summary>
  ///   Пространство событий.
  /// </summary>
  /// <remarks>
  ///   Разные пространства событий не зависят друг от друга.
  /// </remarks>
  TEventSpace =
  {$IFDEF USE_ALIASES}
    San.Events.TEventSpace;
  {$ELSE}
    class(TObject)
  public
    {$REGION 'type'}
    type
      TMetaEvents = class(TObjectDictionary<TUpperString, TMetaEvent>)
      private
        /// <summary>
        ///   Вызвать UnSubscribe.All для каждого метасобытия в справочнике.
        /// </summary>
        procedure UnSubscribeAll(const AEventHandlerData : Pointer);

      public
        /// <summary>
        ///   Проверка наличия метасобытия.
        /// </summary>
        function Contains(const AMetaEvent : TMetaEvent) : boolean; overload;

        /// <summary>
        ///   Добавление метасобытия.
        /// </summary>
        function Add(const AMetaEvent : TMetaEvent) : TMetaEvent;

        /// <summary>
        ///   Изятие метасобытия.
        /// </summary>
        function Extract(const AMetaEvent : TMetaEvent) : TMetaEvent;

        /// <summary>
        ///   Удаление метасобытия.
        /// </summary>
        procedure Remove(const AMetaEvent : TMetaEvent); overload;

        /// <summary>
        ///   Получение элемента заданного типа для наследников <c>TMetaEventSeparate</c>
        /// </summary>
        function TypedItem<T : class> : T;
      end;
    {$ENDREGION}

  strict private
    type
      TByAttrsCtx = record
        FSubscriber, FSender : TObject;
        FMetaEvent : TMetaEvent;
        FRttiMethod : TRttiMethod;
        FPosition : integer;
      end;

      TByAttrsRef = reference to procedure (const ACtx : TByAttrsCtx);

    function DoByAttrs(const ASubsriber, ASenderFilter : TObject; const AFilterCode : integer;
      const ARef : TByAttrsRef) : integer;

  private
    FMetaEvents: TMetaEvents;
    FMetaEventsHolder : IInterface;

  public
    constructor Create(const ACapacity: Integer = 0);
    destructor Destroy; override;

    /// <summary>
    ///   Создание сторожа подписки.
    /// </summary>
    /// <param name="ASubscriber">
    ///   Подписчик.
    /// </param>
    function CreateEventsSubscribeGuard(const ASubscriber : TObject) : IEventsSubscribeGuard;

    /// <summary>
    ///   Подписать методы подписчика по атрибутам и фильтру.
    /// </summary>
    /// <param name="ASubscriber">
    ///   Подписчик.
    /// </param>
    /// <param name="AFilterCode">
    ///   Код фильтрации позволяет обрабатывать только группу методов.
    /// </param>
    /// <param name="ASenderFilter">
    ///   Фильтрация по Sender.
    /// </param>
    /// <returns>
    ///   Количество подписанных методов.
    /// </returns>
    function SubscribeByAttrs(const ASubsriber : TObject; const AFilterCode : integer;
      const ASenderFilter : TObject = nil) : integer;

    /// <summary>
    ///   Подписать методы подписчика по атрибутам и фильтру.
    /// </summary>
    /// <param name="ASubscriber">
    ///   Подписчик.
    /// </param>
    /// <param name="AFilterCode">
    ///   Код фильтрации позволяет обрабатывать только группу методов.
    /// </param>
    /// <param name="ASenderFilter">
    ///   Фильтрация по Sender.
    /// </param>
    /// <returns>
    ///   Количество отписанных методов.
    /// </returns>
    function UnSubscribeByAttrs(const ASubsriber : TObject; const AFilterCode : integer;
      const ASenderFilter : TObject = nil) : integer;

    /// <summary>
    ///   Справочник метасобытий по именам.
    /// </summary>
    property MetaEvents : TMetaEvents  read FMetaEvents;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}

implementation

{$REGION 'uses'}
uses
  {$REGION 'San'}
  San.Messages
  {$ENDREGION}
;
{$ENDREGION}


{$REGION 'type'}
type
  /// <summary>
  ///   Весь смысл жизни такого сторожа (в виде интерфесной ссылки) - достойная смерть,
  ///   когда он, покрыв себя славой, отпишится от всех событий переданного подписчика.
  /// </summary>
  TEventsSubscribeGuard = class(TInterfacedObject, IEventsSubscribeGuard)
  strict private
    /// <summary>
    ///   Справочник метасобытий, в котором подписка.
    /// </summary>
    FMetaEvents : TEventSpace.TMetaEvents;

    /// <summary>
    ///   Подписчик.
    /// </summary>
    FSubscriber : TObject;

    /// <summary>
    ///   Каждый экземпляр имеет ссылку на интерфейс держателя общего списка.
    ///   Пока ВСЕ ссылки не сброшены, общий список будет жить.
    /// </summary>
    FMetaEventsHolderLink : IInterface;

  public
    constructor Create(const AEventSpaces : TEventSpace; const ASubscriber : TObject);
    destructor Destroy; override;
  end;
{$ENDREGION}

{$REGION 'TEventsSubscribeGuard'}
constructor TEventsSubscribeGuard.Create(const AEventSpaces : TEventSpace; const ASubscriber: TObject);
begin
  inherited Create;

  FMetaEvents            := AEventSpaces.FMetaEvents;
  FMetaEventsHolderLink  := AEventSpaces.FMetaEventsHolder;
  FSubscriber            := ASubscriber;
end;

destructor TEventsSubscribeGuard.Destroy;
begin
  try

    FMetaEvents.UnSubscribeAll(FSubscriber);

  except
    EEvents.CreateResFmt(@s_San_Events_ErrorForSubscraberFmt, [FSubscriber.ClassName]).RaiseOuter();
  end;

  inherited;
end;
{$ENDREGION}

{$REGION 'THandlerData'}
class function THandlerData.Empty: THandlerData;
begin
  FillChar(Result, SizeOf(Result), 0);
end;

class operator THandlerData.Equal(const AItem1,
  AItem2: THandlerData): boolean;
begin
  Result := CompareMem(@AItem1, @AItem2, SizeOf(THandlerData));
end;

class function THandlerData.Create(const ASender : Pointer; const AMethod : TMethod): THandlerData;
begin
  Result.FMethod  := AMethod;
  Result.FSender  := ASender;
end;

function THandlerData.IsEmpty: boolean;
begin
  Result := (FMethod.Code = nil) or (FMethod.Data = nil);
end;

function THandlerData.Description(const ASeporator: string): string;

  function TryFindRttiMethod() : boolean;
  var
    ObjTp    : TRttiInstanceType;
    Methods  : TArray<TRttiMethod>;
    Method_Old   : TRttiMethod;
  begin
    ObjTp    := San.Rtti.Api.Context.GetType( TObject(FMethod.Data).ClassType ) as TRttiInstanceType;
    Methods  := ObjTp.GetMethods;

    for Method_Old in Methods do
      if Method_Old.CodeAddress = FMethod.Code then
      begin
        FRttiMethod := Method_Old;
        Exit(True);
      end;

    Result := False;
  end;

begin
  Result := '';

  if IsEmpty then
    Exit('Empty');

  try
    Result := Format('Data: %s', [San.Objs.Api.Description(FMethod.Data, ASeporator)]);

    if Assigned(FRttiMethod) or TryFindRttiMethod() then
      Result := Result + Format('%sMethod: %s', [ASeporator, FRttiMethod.Name])

    else
      Result := Result + Format('%sMethod: RTTI not found', [ASeporator]);

    if Assigned(FSender) then
      Result := Result + Format('%sSender: %s', [ASeporator, San.Objs.Api.Description(FSender, ASeporator)]);

  except
    on E : Exception do
      Result := Format(@s_San_Events_InternalErrorDescriptionGettingFmt, [E.ClassName, E.ToString, Result]);
  end;
end;
{$ENDREGION}

{$REGION 'THandlers'}
class function THandlers.IsEmptyItem(const AItem: THandlerData): boolean;
begin
  Result := AItem.IsEmpty;
end;

class function THandlers.IsEqualsItems(const AItem1,
  AItem2: THandlerData): boolean;
begin
  Result := AItem1 = AItem2;
end;
{$ENDREGION}

{$REGION 'TMetaEvent.EventImpl'}
procedure TMetaEvent.EventImpl.BeforeDestruction();
begin
  FreeAndNil(FContext);

  inherited;
end;

destructor TMetaEvent.EventImpl.Destroy();
begin
  FreeAndNil(FExceptions);

  inherited;
end;

function TMetaEvent.EventImpl.GetObjRef: TObject;
begin
  Result := Self;
end;

procedure TMetaEvent.EventImpl.AcquireExcp();
begin

  if not Assigned(FExceptions) then
    FExceptions := TExceptionList.Create();

  FExceptions.AddAcquired;
end;
{$ENDREGION}

{$REGION 'TMetaEvent.Event<T>'}
function TMetaEvent.Event<T>.GetContext: T;
begin
  Result := T(TMetaEvent.EventImpl(FInnerData.ObjRef).FContext);
end;

function TMetaEvent.Event<T>.GetExceptions: TExceptionList;
begin
  Result := TMetaEvent.EventImpl(FInnerData.ObjRef).FExceptions;
end;

function TMetaEvent.Event<T>.GetHandled: boolean;
begin
  Result := TMetaEvent.EventImpl(FInnerData.ObjRef).FHandled;
end;

function TMetaEvent.Event<T>.GetMetaEvent: TMetaEvent;
begin
  Result := TMetaEvent.EventImpl(FInnerData.ObjRef).FMetaEvent;
end;

function TMetaEvent.Event<T>.GetSender: TObject;
begin
  Result := TMetaEvent.EventImpl(FInnerData.ObjRef).FSender;
end;

procedure TMetaEvent.Event<T>.SetHandled(const AValue: boolean);
begin
  TMetaEvent.EventImpl(FInnerData.ObjRef).FHandled := AValue;
end;
{$ENDREGION}

{$REGION 'TMetaEvent.Subtypes.TFiring'}
procedure TMetaEvent.Subtypes.TFiring.DoFiring(const ASafe: boolean;
  const ABeforeHandlerExec, AAfterHandlerExec: HandleEventReference;
  out AEvent: TMetaEvent.Event);
var
  Ev  : TMetaEvent.EventImpl;
  Hs  : THandlers;
  HD  : THandlerData;
  i   : integer;
begin
  Ev := TMetaEvent.EventImpl(FEvent.ObjRef);
  AEvent.FInnerData := Ev;

  Hs := Ev.FMetaEvent.Handlers;

  Ev.FMetaEvent.IsFiring := True;
  try

    i := 0;
    while (i < Hs.Count) and not AEvent.Handled do
    begin
      HD := Hs.Get(i).Handler;

      try

        if Assigned(ABeforeHandlerExec) then
          ABeforeHandlerExec(HD, AEvent);

        if not HD.IsEmpty and (not Assigned(HD.FSender) or (HD.FSender = Ev.FSender)) then
        try

          TMetaEvent.Subtypes.HandlerPrototype(HD.FMethod)(AEvent);

        except
          on E: Exception do
            if not (E is EAbort) then
              EEvents.CreateResFmt(@s_San_Events_ErrorEventFiringForSubscraberFmt, [Ev.FMetaEvent.EventName, HD.Description()]).RaiseOuter;
        end;

        if Assigned(AAfterHandlerExec) then
          AAfterHandlerExec(HD, AEvent);

      except
        if ASafe then
          Ev.AcquireExcp()
        else
          raise;
      end;

      Inc(i);
    end;

  finally
    Ev.FMetaEvent.IsFiring := False;
  end;

end;

function TMetaEvent.Subtypes.TFiring.Std(): TMetaEvent.Event;
begin
  DoFiring(False, nil, nil, Result);
end;

function TMetaEvent.Subtypes.TFiring.Safe(): TMetaEvent.Event;
begin
  DoFiring(True, nil, nil, Result);
end;

function TMetaEvent.Subtypes.TFiring.Ex(const ABeforeHandlerExec,
  AAfterHandlerExec: HandleEventReference): TMetaEvent.Event;
begin
  DoFiring(False, ABeforeHandlerExec, AAfterHandlerExec, Result);
end;
{$ENDREGION}

{$REGION 'TMetaEvent.Subtypes.TSubscribing'}
procedure TMetaEvent.Subtypes.TSubscribing.Std();
begin
  Ex(-1, nil);
end;

procedure TMetaEvent.Subtypes.TSubscribing.AtPos(const AEventPosition: Integer);
begin
  Ex(AEventPosition, nil);
end;

procedure TMetaEvent.Subtypes.TSubscribing.Sender(const ASenderFilter: TObject);
begin
  Ex(-1, ASenderFilter);
end;

procedure TMetaEvent.Subtypes.TSubscribing.Ex(const ASenderFilter: TObject;
  const AEventPosition: Integer);
begin
  Ex(AEventPosition, ASenderFilter);
end;

procedure TMetaEvent.Subtypes.TSubscribing.Ex(const AEventPosition: Integer;
  const ASenderFilter: TObject);
begin
  // Всегда пытаемся убрать старую подписку, перед тем как делать новую
  FMetaEvent.UnSubscribe.One(ASenderFilter, FEventHandler);

  FMetaEvent.Handlers.Add(AEventPosition, THandlerData.Create(ASenderFilter, FEventHandler));
end;
{$ENDREGION}

{$REGION 'TMetaEvent.Subtypes.TUnSubscribing'}
procedure TMetaEvent.Subtypes.TUnSubscribing.DoUnSubscribe(
  const AMatchFunc: TMatchFunc);
var
  Hs  : THandlers;
  HI  : THandlerItem;
  HD  : THandlerData;
  i   : integer;
begin
  Hs := FMetaEvent.Handlers;

  if not Assigned(Hs) then
    Exit;

  for i := Hs.Count - 1 downto 0 do
  begin
    HI := Hs.Get(i);
    HD := HI.Handler;

    if AMatchFunc(HD) then
      if FMetaEvent.IsFiring then
        FMetaEvent.Handlers.Items[HI.Position] := THandlerData.Empty

      else
        FMetaEvent.Handlers.Delete(HI.Position);
  end;

  if not FMetaEvent.IsFiring then
    Hs.Compress;
end;

procedure TMetaEvent.Subtypes.TUnSubscribing.One(const ASenderFilter: TObject;
  const AEventHandler: TMethod);
begin
  One(ASenderFilter, AEventHandler.Code, AEventHandler.Data);
end;

procedure TMetaEvent.Subtypes.TUnSubscribing.One(const ASenderFilter: TObject;
  const AEventHandlerCode, AEventHandlerData: Pointer);
begin
  DoUnSubscribe
  (
    function (const HD : THandlerData) : boolean
    begin
      Result := (HD.FSender = ASenderFilter)
            and (HD.FMethod.Code = AEventHandlerCode)
            and (HD.FMethod.Data = AEventHandlerData)
    end
  );
end;

procedure TMetaEvent.Subtypes.TUnSubscribing.Sender(const ASenderFilter: TObject);
begin
  DoUnSubscribe
  (
    function (const HD : THandlerData) : boolean
    begin
      Result := HD.FSender = ASenderFilter
    end
  );
end;

procedure TMetaEvent.Subtypes.TUnSubscribing.All(const AEventHandlerData: Pointer);
begin
  DoUnSubscribe
  (
    function (const HD : THandlerData) : boolean
    begin
      Result := HD.FMethod.Data = AEventHandlerData
    end
  );
end;

procedure TMetaEvent.Subtypes.TUnSubscribing.All(const ASubscriber: TObject);
begin
  All(Pointer(ASubscriber));
end;
{$ENDREGION}

{$REGION 'TMetaEvent.Subtypes.SubscribeAttribute'}
constructor TMetaEvent.Subtypes.SubscribeAttribute.Create(
  const AFilterCode, APosition: integer);
begin
  inherited Create();

  FFilterCode := AFilterCode;
  FPosition   := APosition;
end;

constructor TMetaEvent.Subtypes.SubscribeAttribute.Create(
  const AFilterCode: integer);
begin
  inherited Create();

  FFilterCode := AFilterCode;
  FPosition   := -1;
end;

{$ENDREGION}

{$REGION 'TMetaEvent'}
constructor TMetaEvent.Create();
begin
  inherited;

  FHandlers := THandlers.Create();
end;

destructor TMetaEvent.Destroy();
begin
  FreeAndNil(FHandlers);
  inherited;
end;

class procedure TMetaEvent.CreateInSpace(const AEventSpace: TEventSpace;
  out AMetaEvent);
var
  ME : TMetaEvent;
begin
  ME := Self.Create();
  try

    AEventSpace.MetaEvents.Add(ME);

  except
    FreeAndNil(ME);
    raise;
  end;

  TMetaEvent(AMetaEvent) := ME;
end;

class procedure TMetaEvent.CreateInSpace(const AEventSpace: TEventSpace);
var
  Dummy : TMetaEvent;
begin
  CreateInSpace(AEventSpace, Dummy);
end;

procedure TMetaEvent.SetIsFiring(const AValue: boolean);
begin
  FIsFiring := AValue;

  if not FIsFiring then
    Handlers.Compress;

  Handlers.IsSorted := not FIsFiring;
end;

function TMetaEvent.Fire(const ASender, AContext: TObject): Subtypes.TFiring;
var
  Ev : TMetaEvent.EventImpl;
begin
  Ev := TMetaEvent.EventImpl.Create;

  Result.FEvent := Ev;

  Ev.FMetaEvent  := Self;
  Ev.FSender     := ASender;
  Ev.FContext    := AContext;
end;

function TMetaEvent.Subscribe(const AEventHandler: TMethod): Subtypes.TSubscribing;

  procedure ValidateSubscriber;
  var
    Obj : TObject;
    ES : IEventsSubscriber;
    S : string;
  begin
    Obj := TObject(AEventHandler.Data);

    try

      if not Assigned(Obj) then
        raise EEvents.CreateRes(@s_San_Events_NilAsSubscriber);

      if not Supports(Obj, IEventsSubscriber, ES) then
        raise EEvents.CreateRes(@s_San_Events_IntfSubscriberNotSupported);

      if not Assigned(ES.Guard) then
        raise EEvents.CreateRes(@s_San_Events_SubscriberGuardNotInitialized);

    except
      S := LoadResString(@s_San_Events_SubscribeRule);

      if Assigned(Obj) then
        S := S + ' ' + Format(@s_San_Events_SupposedSubscriberFmt, [Obj.ToString]);

      EEvents.Create(S).RaiseOuter();
    end;

  end;

begin
  ValidateSubscriber;

  Result.FMetaEvent     := Self;
  Result.FEventHandler  := AEventHandler;
end;

function TMetaEvent.Subscribe(const AEventHandlerCode, AEventHandlerData : Pointer): Subtypes.TSubscribing;
var
  m : TMethod;
begin
  m.Code := AEventHandlerCode;
  m.Data := AEventHandlerData;

  Result := Subscribe(m);
end;

function TMetaEvent.Subscribe(const AEventHandler: TMetaEvent.Subtypes.HandlerPrototype) : Subtypes.TSubscribing;
begin
  Result := Subscribe(TMethod(AEventHandler));
end;

function TMetaEvent.Subscribe<T>(
  const AEventHandler: TMetaEvent.Subtypes.HandlerPrototype<T>): Subtypes.TSubscribing;
begin
  Result := Subscribe(TMethod(AEventHandler));
end;

function TMetaEvent.UnSubscribe: Subtypes.TUnSubscribing;
begin
  Result.FMetaEvent := Self;
end;

function TMetaEvent.MatchSubscribeAttr(
  const AAttr: TMetaEvent.Subtypes.SubscribeAttribute): boolean;
begin
  Result := False;
end;
{$ENDREGION}

{$REGION 'TMetaEventSimple'}
function TMetaEventSimple.GetEventName: string;
begin
  Result := FEventName;
end;

function TMetaEventSimple.GetDescription: string;
begin
  Result := FDescription;
end;

constructor TMetaEventSimple.Create(const AEventName, ADescription: string);
begin
  inherited Create();

  FEventName    := AEventName;
  FDescription  := ADescription;
end;

class procedure TMetaEventSimple.CreateInSpace(const AEventName,
  ADescription: string; const AEventSpace: TEventSpace; out AMetaEvent);
var
  ME : TMetaEvent;
begin
  ME := Self.Create(AEventName, ADescription);
  try

    AEventSpace.MetaEvents.Add(ME);

  except
    FreeAndNil(ME);
    raise;
  end;

  TMetaEvent(AMetaEvent) := ME;
end;
{$ENDREGION}

{$REGION 'TMetaEventSeparate'}
class function TMetaEventSeparate.Description: string;
begin
  Result := '';
end;

class function TMetaEventSeparate.EventName: string;
begin
  raise EEvents.CreateResFmt(@s_San_Events_NeedOverrideMethodForFmt, ['EventName', ClassName]);
end;

function TMetaEventSeparate.GetDescription: string;
begin
  Result := Description;
end;

function TMetaEventSeparate.GetEventName: string;
begin
  Result := EventName;
end;
{$ENDREGION}

{$REGION 'TEventSpace.TMetaEvents'}
procedure TEventSpace.TMetaEvents.UnSubscribeAll(
  const AEventHandlerData: Pointer);
var
  ME : TMetaEvent;
begin
  for ME in Values do
    ME.UnSubscribe.All(AEventHandlerData);
end;

function TEventSpace.TMetaEvents.Add(const AMetaEvent: TMetaEvent): TMetaEvent;
begin
  Result := AMetaEvent;

  try

    inherited Add(AMetaEvent.EventName, AMetaEvent);

  except
    EEvents.CreateResFmt(@s_San_Events_ErrorAddingMetaEventToSpaceFmt, [AMetaEvent.EventName]).RaiseOuter();
  end;
end;

function TEventSpace.TMetaEvents.Contains(
  const AMetaEvent: TMetaEvent): boolean;
begin
  Result := ContainsKey(AMetaEvent.EventName);
end;

function TEventSpace.TMetaEvents.Extract(
  const AMetaEvent: TMetaEvent): TMetaEvent;
var
  P : TPair<TUpperString, TMetaEvent>;
begin
  P := ExtractPair(AMetaEvent.EventName);
  Result := P.Value;
end;

procedure TEventSpace.TMetaEvents.Remove(const AMetaEvent: TMetaEvent);
begin
  inherited Remove(AMetaEvent.EventName);
end;

function TEventSpace.TMetaEvents.TypedItem<T>: T;
var
  Cl : TMetaEventSeparateClass;
begin
  Cl := TMetaEventSeparateClass(T);

  Assert(Cl.InheritsFrom(TMetaEventSeparate), 'Require TMetaEventSeparate inheritance');

  Result := T(Self[Cl.EventName]);
end;

{$ENDREGION}

{$REGION 'MetaEv'}
constructor MetaEv.Create(const AEventName, ADescription: string);
begin
  inherited Create();

  FEventName    := AEventName;
  FDescription  := ADescription;
end;
{$ENDREGION}

{$REGION 'TMetaEventEx'}
constructor TMetaEventEx.Create;
var
  RttiType : TRttiInstanceType;
  Attrs : TArray<TCustomAttribute>;
  Attr : TCustomAttribute;

begin
  inherited Create();

  RttiType := San.Rtti.Api.Context.GetType(ClassType) as TRttiInstanceType;

  Attrs := RttiType.GetAttributes();

  for Attr in Attrs do
    if Attr is MetaEv then
    begin
      FEventName    := MetaEv(Attr).EventName;
      FDescription  := MetaEv(Attr).Description;
    end

    else
    if Attr is TMetaEvent.Subtypes.SubscribeAttribute then
      FEvAttrClass := Attr.ClassType;
end;

function TMetaEventEx.GetDescription: string;
begin
  Result := FDescription;
end;

function TMetaEventEx.GetEventName: string;
begin
  Result := FEventName;
end;

function TMetaEventEx.MatchSubscribeAttr(
  const AAttr: TMetaEvent.Subtypes.SubscribeAttribute): boolean;
begin
  Result := AAttr.InheritsFrom(FEvAttrClass);
end;
{$ENDREGION}

{$REGION 'TEventSpace'}
constructor TEventSpace.Create(const ACapacity: Integer);
begin
  inherited Create();

  FMetaEvents := TMetaEvents.Create([doOwnsValues], ACapacity, TUpperStringEqualityComparer.Create);
  FMetaEventsHolder := San.Intfs.Api.Hold(FMetaEvents);
end;

destructor TEventSpace.Destroy;
begin
  // Основная ссылка сбрасывается здесь.
  // Объект будет уничтожен, когда сбросятся остальные ссылки.
  FMetaEventsHolder := nil;

  inherited;
end;

function TEventSpace.CreateEventsSubscribeGuard(
  const ASubscriber: TObject): IEventsSubscribeGuard;
begin
  Result := TEventsSubscribeGuard.Create(Self, ASubscriber);
end;

function TEventSpace.DoByAttrs(const ASubsriber, ASenderFilter: TObject;
  const AFilterCode: integer; const ARef: TByAttrsRef): integer;
var
  RttiType : TRttiInstanceType;
  RttiMethods : TArray<TRttiMethod>;
  RttiMethod : TRttiMethod;
  MetaEvent : TMetaEvent;
  Context : TByAttrsCtx;
  Attr : TMetaEvent.Subtypes.SubscribeAttribute;
begin
  Result := 0;

  Context.FSubscriber  := ASubsriber;
  Context.FSender      := ASenderFilter;

  RttiType := San.Rtti.Api.Context.GetType(ASubsriber.ClassType) as TRttiInstanceType;
  RttiMethods := RttiType.GetMethods();

  for RttiMethod in RttiMethods do
    if  San.Rtti.Api.TryGetAttr(RttiMethod, TMetaEvent.Subtypes.SubscribeAttribute, Attr) then
      if (AFilterCode = Attr.FilterCode) then
      begin
        Context.FRttiMethod := RttiMethod;

        for MetaEvent in MetaEvents.Values do
          if MetaEvent.MatchSubscribeAttr(Attr) then
          begin
            Context.FMetaEvent  := MetaEvent;
            Context.FPosition   := Attr.Position;
            ARef(Context);

            Inc(Result);
          end;

      end;

end;

function TEventSpace.SubscribeByAttrs(const ASubsriber: TObject;
  const AFilterCode: integer; const ASenderFilter : TObject): integer;
begin
  Result := DoByAttrs
  (
    ASubsriber, ASenderFilter, AFilterCode,
    procedure (const ACtx : TByAttrsCtx)
    begin
      ACtx.FMetaEvent.Subscribe(ACtx.FRttiMethod.CodeAddress, ACtx.FSubscriber).Ex(ACtx.FPosition, ACtx.FSender);
    end
  );
end;

function TEventSpace.UnSubscribeByAttrs(const ASubsriber: TObject;
  const AFilterCode: integer; const ASenderFilter : TObject): integer;
begin
  Result := DoByAttrs
  (
    ASubsriber, ASenderFilter, AFilterCode,
    procedure (const ACtx : TByAttrsCtx)
    begin
      ACtx.FMetaEvent.UnSubscribe.One(ACtx.FSender, ACtx.FRttiMethod.CodeAddress, ACtx.FSubscriber);
    end
  );

end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
