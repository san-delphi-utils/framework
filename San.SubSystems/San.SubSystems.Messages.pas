{                                                       }
{       Сообщения                                       }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Сообщения
/// </summary>
unit San.SubSystems.Messages;
{$I San.SubSystems.inc}
interface
{$ENDIF}

resourcestring
  sUndefined =
    'Undefined';

  sNone =
    'None';

  sInactive =
    'Inactive';

  sActive =
    'Active';

  sDisabled =
    'Disabled';

  sError =
    'Error';

  sCOM =
    'COM';

  sDirect =
    'Direct';

  sDisconnected =
    'Disconnected';

  sInternalErrorForFmt =
    'Internal error for %s';

  sMakerIsNil =
    'Maker is nil';

  sSubSystemIsNil =
    'SubSystem is nil';

  sFailsMakeSubSystemFmt =
    'Не получилось инициализировать подсистему %s';

  sInvalidCfgGUIDForSubSystemFmt =
    'Не верный GUID (%s) конфигурации для подсистемы %s';

  sInvalidCfgInterfaceGUIDFmt =
    'Не верный GUID (%s) конфигурации';

  sSubSystemDLLInitFails =
    'Fails init function for subsystem in DLL';

  sSubSystemMustExplicitSupportISubSystemFmt =
    'Подсистема %s должна поддерживать ISubSystem в явном виде';

  sSubSystemRequirementNotRegistredErrorFmt =
    'Перед регистрацией подсистемы %s должна быть зарегистрирована необходимая подсистема %s';

  sSubSystemRequirementsErrorFmt =
    'Активация подсистемы %s не возможна, т.к. необходимые подсистемы %s не активны';

  sSubSystemIsNotActiveFmt =
    'Подсистема %s не активна';

{$IFNDEF USE_ALIASES}
implementation
end{$WARNINGS OFF}.
{$ENDIF}
