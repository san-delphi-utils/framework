{*******************************************************}
{                                                       }
{       Базовая реализация подсистем                    }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Базовая реализация подсистем
/// </summary>
unit San.SubSystems.Impl;
{$I San.SubSystems.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Classes, System.TypInfo, System.Types, System.IOUtils,
  {$ENDREGION}

  {$REGION 'San'}
  San.Logger,
  San.SubSystems.Messages,
  San.SubSystems.Exceptions,
  San.SubSystems.Config.Intf,
  San.SubSystems.Config,
  San.SubSystems.Intf,
  San.SubSystems.Reporter,
  San.SubSystems.Config.Storage
  {$ENDREGION}
;
{$ENDREGION}

type
  Exception = System.SysUtils.Exception;
  TCustomSubSystem = class;

{$ELSE}
type
{$ENDIF}

  /// <summary>
  ///   Класс блока параметров конфигурации.
  /// </summary>
  TSubSystemCfgClass =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Impl.TSubSystemCfgClass;
  {$ELSE}
    class of TSubSystemCfg;
  {$ENDIF}

  /// <summary>
  ///   Блок параметров конфигурации.
  /// </summary>
  TSubSystemCfg =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Impl.TSubSystemCfg;
  {$ELSE}
  class abstract(TInterfacedObject, ICfg, ICfgLog)
  strict private
    FSubSystem : TCustomSubSystem;
    FLog : string;
    FLogLevel : integer;

  strict protected
    property SubSystem : TCustomSubSystem  read FSubSystem;

  protected
    {$REGION 'ICfg'}
    procedure ReadFromStorage(AStorage : ICfgStorage);
    procedure WriteToStorage(AStorage : ICfgStorage);
    function Report(const ADelimiter : string; const APrefix : string = '';
      const APostfix : string = '') : string;
    {$ENDREGION}

    {$REGION 'ICfgLog'}
    function GetLog : string;
    function GetLogLevel : integer;
    {$ENDREGION}

    function GetSection : string; virtual;
    procedure DoReadFromStorage(const AStorage : TCfgSectionOfStorage); virtual;
    procedure DoWriteToStorage(const AStorage : TCfgSectionOfStorage); virtual;
    procedure DoReport(const AReporter : TReporter); virtual;

  public
    constructor Create(const ASubSystem : TCustomSubSystem); virtual;

    /// <summary>
    ///    Секция.
    /// </summary>
    property Section : string  read GetSection;
  end;
  {$ENDIF}

  /// <summary>
  ///   Заготовка реализации подсистемы.
  /// </summary>
  TCustomSubSystem =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Impl.TCustomSubSystem;
  {$ELSE}
  class abstract(TInterfacedObject, ISubSystem)
  strict private
    FConfig : TSubSystemCfg;
    FLog: ISANLog;
    FLogLevel : integer;

  protected
    FStatusStr : string;

    {$REGION 'ISubSystem'}
    function GetConfig : ICfg;
    function GetLog : ISANLog;
    function GetIsActive : boolean; virtual;
    function GetStatusStr : string;
    procedure Activate();
    procedure Deactivate(); virtual;
    function Report(const ADelimiter : string) : string;
    {$ENDREGION}

    /// <summary>
    ///   Создание экземпляра подсистемы.
    /// </summary>
    class function Make : ISubSystem;

    /// <summary>
    ///   TypeInfo интерфейса системы.
    /// </summary>
    class function SubSystemTypeInfo : PTypeInfo; virtual;

    /// <summary>
    ///   Метаданные системы.
    /// </summary>
    class function SubSystemMeta : TSubSystemMetadata;

    /// <summary>
    ///   Проверка необходимых подсистем.
    /// </summary>
    class procedure CheckRequirements();

    /// <summary>
    ///   Регистрация создателя.
    /// </summary>
    class procedure RegMaker;

    /// <summary>
    ///   Класс конфига.
    /// </summary>
    class function CfgClass : TSubSystemCfgClass; virtual;

    /// <summary>
    ///   Секция конфига.
    /// </summary>
    function GetCfgSection : string; virtual;

    /// <summary>
    ///   Отладочное отключение.
    /// </summary>
    /// <remarks>
    ///   Переопределяется и запрашивается в потомках.
    /// </remarks>
    function IsDesabled : boolean; virtual;

    /// <summary>
    ///   Установка многопоточной блокировки.
    /// </summary>
    /// <remarks>
    ///   Для реальной блокировки требуется переопределение.
    /// </remarks>
    procedure Lock; virtual;

    /// <summary>
    ///   Снятие многопоточной блокировки.
    /// </summary>
    /// <remarks>
    ///   Для реальной блокировки требуется переопределение.
    /// </remarks>
    procedure UnLock; virtual;

    /// <summary>
    ///   Активация подсистемы.
    /// </summary>
    procedure DoActivate(); virtual; abstract;

    /// <summary>
    ///   Внешнее исключение при активации
    /// </summary>
    procedure DoActivateOuterException(const AInnerException : Exception); virtual;

    /// <summary>
    ///   Построение отчета по системе.
    /// </summary>
    procedure DoReport(const AReporter : TReporter); virtual;

    /// <summary>
    ///   Проверка активности с исключением
    /// </summary>
    procedure ValidateIsActive(); virtual;

    /// <summary>
    ///   Доступ к конфигу.
    /// </summary>
    property Config : TSubSystemCfg  read FConfig;

    /// <summary>
    ///   Лог, в который подсистема может вести запись.
    /// </summary>
    property Log: ISANLog  read FLog;

    /// <summary>
    ///   Уровень логирования. 0 — ничего не логировать
    /// </summary>
    property LogLevel : integer  read FLogLevel;

  public
    constructor Create; virtual;
    destructor Destroy; override;

    function SafeCallException(ExceptObject: TObject; ExceptAddr: Pointer):
          HResult; override;

    /// <summary>
    ///   Создать запись лога для текущего потока
    /// </summary>
    function LogRecordThreadMake(const AHeader : string; const AKind : TSANLogRecordKind = LRK_INFO) : ISANLogRecord;

    /// <summary>
    ///   Обработать запись лога для текущего потока
    /// </summary>
    procedure LogRecordThread(const AHeader : string; const AKind : TSANLogRecordKind; const AAction : TProc<ISANLogRecord> = nil); overload;

    /// <summary>
    ///   Обработать запись лога для текущего потока
    /// </summary>
    procedure LogRecordThread(const AHeader : string; const AAction : TProc<ISANLogRecord> = nil); overload;
  end;
  {$ENDIF}

  /// <summary>
  ///   Заготовка реализации подсистемы. Генерик.
  /// </summary>
  TCustomSubSystem<TIntf : ISubSystem; TCfg : class> = class abstract(TCustomSubSystem)
  strict private
    function GetConfig : TCfg;

  protected
    class function SubSystemTypeInfo : PTypeInfo; override;
    class function CfgClass : TSubSystemCfgClass; override;

    /// <summary>
    ///   Доступ к конфигу.
    /// </summary>
    property Config : TCfg  read GetConfig;

  end;

{$IFNDEF USE_ALIASES}
implementation

{$REGION 'uses'}
uses
  {$REGION 'San'}
  San
  {$ENDREGION}
  ;
{$ENDREGION}

{$REGION 'TSubSystemCfg'}
constructor TSubSystemCfg.Create(const ASubSystem: TCustomSubSystem);
begin
  inherited Create;
  FSubSystem := ASubSystem;
end;

procedure TSubSystemCfg.DoReadFromStorage(const AStorage: TCfgSectionOfStorage);
begin
  FLog       := AStorage.Read('Log', '');

  if (FLog <> '') and not TPath.IsDriveRooted( StringReplace(FLog, '|', '', [rfReplaceAll])) then
    FLog := San.SubSystems.Config.Api.ProgramData + TPath.DirectorySeparatorChar + FLog;

  FLogLevel  := AStorage.Read('LogLevel', 0);
end;

procedure TSubSystemCfg.DoReport(const AReporter: TReporter);
begin
  if FLog <> '' then
    AReporter.RepFmt('Log=%s', [FLog]);

  if FLogLevel > 0 then
    AReporter.RepFmt('LogLevel=%d', [FLogLevel]);
end;

procedure TSubSystemCfg.DoWriteToStorage(const AStorage: TCfgSectionOfStorage);
begin
end;

function TSubSystemCfg.GetLog: string;
begin
  Result := FLog;
end;

function TSubSystemCfg.GetLogLevel: integer;
begin
  Result := FLogLevel;
end;

function TSubSystemCfg.GetSection: string;
begin
  Result := SubSystem.GetCfgSection();
end;

procedure TSubSystemCfg.ReadFromStorage(AStorage: ICfgStorage);
begin
  DoReadFromStorage( TCfgSectionOfStorage.Create(AStorage, Section) );
end;

procedure TSubSystemCfg.WriteToStorage(AStorage: ICfgStorage);
begin
  if not Supports(SubSystem, ISubSystemCanWriteCfg) then
    Exit;

  DoWriteToStorage( TCfgSectionOfStorage.Create(AStorage, Section) );
end;

function TSubSystemCfg.Report(const ADelimiter, APrefix, APostfix : string): string;
var
  R : string;
  Rp : TReporter;
begin
  R := '';

  Rp := TReporter.Create(procedure (const AData : array of string)
    var
      S : string;
    begin
      for S in AData do
        if R = '' then
          R := APrefix + S + APostfix
        else
          R := R + ADelimiter + APrefix + S + APostfix;
    end
  );

  DoReport(Rp);

  Result := R;
end;
{$ENDREGION}

{$REGION 'TCustomSubSystem'}
class function TCustomSubSystem.SubSystemTypeInfo: PTypeInfo;
begin
  raise ENotImplemented(ClassName + '.SubSystemTypeInfo');
end;

class function TCustomSubSystem.CfgClass: TSubSystemCfgClass;
begin
  raise ENotImplemented(ClassName + '.CfgClass');
end;

class function TCustomSubSystem.SubSystemMeta: TSubSystemMetadata;
begin
  Result := San.SubSystems.Intf.Api.Get(SubSystemTypeInfo);
end;

class procedure TCustomSubSystem.CheckRequirements;
var
  M : TSubSystemMetadata;
  SS : ISubSystem;
  S : string;
begin
  S := '';
  for M in SubSystemMeta.Requirements do
    if not (San.SubSystems.Intf.Api.SubSystems.TryGetValue(M, SS) or SS.IsActive) then
      if S = '' then
        S := M.Name
      else
        S := S + ',' + M.Name;

  if S <> '' then
    raise ESubSystemActivationError.CreateResFmt(@sSubSystemRequirementsErrorFmt, [SubSystemMeta.Name, S]);
end;

class function TCustomSubSystem.Make: ISubSystem;
var
  Obj : TCustomSubSystem;
begin
  Obj := Self.Create();
  if not Supports(Obj, SubSystemMeta.GUID, Result) then
    Assert(False);
end;

class procedure TCustomSubSystem.RegMaker;
begin
  try
    San.SubSystems.Intf.Api.RegMaker(SubSystemTypeInfo(), Make);
  except
    ESubSystemMakeError.CreateResFmt(@sInternalErrorForFmt, [SubSystemTypeInfo()^.NameFld.ToString()]).RaiseOuter();
  end;
end;

function TCustomSubSystem.Report(const ADelimiter: string): string;
var
  R : string;
  Rp : TReporter;
begin
  R := '';

  Rp := TReporter.Create(procedure (const AData : array of string)
    var
      S : string;
    begin
      for S in AData do
        if R = '' then
          R := S
        else
          R := R + ADelimiter + S;
    end
  );

  if Assigned(FLog) then
    Rp.RepFmt('Log.Location=%s', [FLog.Location])
  else
    Rp.Rep('Log=nil');

  Rp.RepFmt('LogLevel=%d', [FLogLevel]);

  DoReport(Rp);

  Result := R;
end;

function TCustomSubSystem.GetCfgSection: string;
begin
  Result := SubSystemMeta.Name;
end;

function TCustomSubSystem.GetConfig: ICfg;
begin
  Result := FConfig;
end;

function TCustomSubSystem.GetIsActive: boolean;
begin
  Result := not IsDesabled();
end;

function TCustomSubSystem.GetLog: ISANLog;
begin
  Result := FLog;
end;

function TCustomSubSystem.GetStatusStr: string;
begin
  Result := FStatusStr;
end;

procedure TCustomSubSystem.Activate();

  function ErrorFmt(const AError : Exception) : string;
  const
    FMT = '%s: %s';
  begin
    Result := Format(FMT, [AError.ClassName, AError.Message]);
  end;

  function ErrorStr(const AError : Exception) : string;
  var
    E : Exception;
  begin
    Result := ErrorFmt(AError);
    E := AError.InnerException;
    while Assigned(E) do
    begin
      Result := Result + #13#10#13#10 + ErrorFmt(E);
      E := E.InnerException;
    end;
  end;

  procedure SetupLog;
  var
    Cfg : ICfgLog;
  begin
    Cfg := Config as ICfgLog;
    FLogLevel := Cfg.LogLevel;

    if Cfg.Log <> '' then
    begin
      FLog := San.Logger.Api.AddOrGet(SubSystemMeta.Name, Cfg.Log);
      FLog.DateTimeFormat := 'hh:nn:ss.zzz';
    end
    else
    if San.Logger.Api.LogsCount > 0 then
      FLog := San.Logger.Api.Logs[0]
    else
      FLog := nil;
  end;

var
  OldStatusStr : string;
begin
  CheckRequirements();

  if IsDesabled() then
  begin
    FStatusStr := LoadResString(@sDisabled);
    Exit;
  end;

  OldStatusStr := '';

  Lock();
  try
    Deactivate();

    OldStatusStr := FStatusStr;

    SetupLog;
    try

      DoActivate();

    except
      on E : Exception do
      begin
        if (OldStatusStr = FStatusStr) then
          FStatusStr := ErrorStr(E);

        if LogLevel >= 1 then
        begin
          Log.Header(LRK_ERROR, ClassName + '.Activate');
          Log.Write(E.ClassName);
          Log.Write(E.ToString());
        end;

        Deactivate;

        DoActivateOuterException(E);
      end
    end;

  finally
    UnLock();
  end;

  if (OldStatusStr = FStatusStr) then
    FStatusStr := LoadResString(@sActive);

end;

procedure TCustomSubSystem.DoActivateOuterException(const AInnerException : Exception);
var
  E : Exception;
begin
  // Нет внутренней обработки
  if not (AInnerException is ESubSystemActivationError) then
    ESubSystemActivationError.CreateResFmt(@sInternalErrorForFmt, [SubSystemTypeInfo()^.NameFld.ToString()]).RaiseOuter()
  else
  begin
    E := Exception(AcquireExceptionObject);
    raise E;
  end;
end;

procedure TCustomSubSystem.Deactivate;
begin
  if Assigned(FLog) then
  begin
    if not SameText(San.Logger.Api.Logs[0].Name, FLog.Name) then
      San.Logger.Api.Remove(FLog);

    FLog := nil;
  end;

  FStatusStr := LoadResString(@sInactive);
end;

constructor TCustomSubSystem.Create;
begin
  inherited Create;

  Assert(Assigned(SubSystemTypeInfo()));
  Assert(SubSystemTypeInfo^.Kind = tkInterface);
  Assert(Assigned(CfgClass()));

  FStatusStr := LoadResString(@sInactive);

  FConfig := CfgClass.Create(Self);
  FConfig._AddRef();
end;

destructor TCustomSubSystem.Destroy;
begin
  Deactivate;
  FConfig._Release();
  inherited;
end;

procedure TCustomSubSystem.DoReport(const AReporter: TReporter);
begin
end;

function TCustomSubSystem.IsDesabled: boolean;
var
  SSD : ISubSystemCanDisable;
begin
  Result := Supports(Self, ISubSystemCanDisable, SSD) and not SSD.Enabled;
end;

procedure TCustomSubSystem.Lock;
begin
end;

procedure TCustomSubSystem.UnLock;
begin
end;

procedure TCustomSubSystem.ValidateIsActive;
begin
  if not GetIsActive then
    raise ESubSystemActivationError.CreateResFmt(@sSubSystemIsNotActiveFmt, [SubSystemMeta.Name]);
end;

function TCustomSubSystem.SafeCallException(ExceptObject: TObject;
  ExceptAddr: Pointer): HResult;
begin
  {$IFDEF USE_DLLS}
  // From http://delphikingdom.ru/asp/viewitem.asp?catalogid=1392#SubSubHeader_1_2_3

  // Здесь Result - это код ошибки, вы можете вернуть свой код
  // в зависимости от типа исключения.
  // HandleSafeCallException релизует стандартное добавление информации к исключению
  Result := HandleSafeCallException(ExceptObject, ExceptAddr, SubSystemGUID,
    String(ExceptObject.ClassName), '');
  {$ELSE}
  Result := inherited;
  {$ENDIF}
end;

function TCustomSubSystem.LogRecordThreadMake(const AHeader: string;
  const AKind : TSANLogRecordKind): ISANLogRecord;
begin
  Result := Log.NewRecord();
  Result.AutoFlush := False;

  Result.Header(AKind, AHeader);
  Result.WritelnFmt('Thread ID: %d', [TThread.Current.ThreadID]);
end;

procedure TCustomSubSystem.LogRecordThread(const AHeader: string;
  const AKind: TSANLogRecordKind; const AAction: TProc<ISANLogRecord>);
var
  R : ISANLogRecord;
begin
  R := LogRecordThreadMake(AHeader, AKind);
  if Assigned(AAction) then
    AAction(R);
  R.Flush();
  R.Close();
end;

procedure TCustomSubSystem.LogRecordThread(const AHeader: string;
  const AAction: TProc<ISANLogRecord>);
begin
  LogRecordThread(AHeader, LRK_INFO, AAction);
end;
{$ENDREGION}

{$I San.SubSystems.Impl.GenImpl.inc}

end{$WARNINGS OFF}.
{$ENDIF}
