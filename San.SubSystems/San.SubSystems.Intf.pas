{*******************************************************}
{                                                       }
{       Базовые интерфейсы подсистем.                   }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Базовые интерфейсы подсистем.
/// </summary>
unit San.SubSystems.Intf;
{$I San.SubSystems.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.TypInfo, System.Generics.Collections,
  {$ENDREGION}

  {$REGION 'San'}
  San.Logger,
  San.SubSystems.Reporter,
  San.SubSystems.Config.Storage.Intf,
  San.SubSystems.Config.Intf
  {$ENDREGION}
;
{$ENDREGION}

type
  ISubSystem = interface;
  TMakerSubSystem = function : ISubSystem of object;
{$ELSE}
type
{$ENDIF}
  /// <summary>
  ///   Состояние подсистемы приложения.
  /// </summary>
  TSubSystemState =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Intf.TSubSystemState;
  {$ELSE}
    (sssUnassigned, sssInactive, sssActive);
  {$ENDIF}

  /// <summary>
  ///   Метаданные подсистемы.
  /// </summary>
  TSubSystemMetadata =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Intf.TSubSystemMetadata;
  {$ELSE}
  class sealed
  strict private
    function GetDLLFileName: string;
    function GetName: string;
    function GetGUID: TGUID;

  private
    FTypeInfo : PTypeInfo;
    FInDLL: boolean;
    FIsCritical: boolean;
    FRequirements : TArray<TSubSystemMetadata>;
    FMaker : TMakerSubSystem;

  public
    /// <summary>
    ///   Информация о типе подсистемы.
    /// </summary>
    property TypeInfo : PTypeInfo  read FTypeInfo;

    /// <summary>
    ///   GUID интерфейса подистемы.
    /// </summary>
    property GUID : TGUID  read GetGUID;

    /// <summary>
    ///   Является ли подсистема критически важной.
    /// </summary>
    property IsCritical : boolean read FIsCritical;

    /// <summary>
    ///   Имя подсистемы.
    /// </summary>
    property Name : string  read GetName;

    /// <summary>
    ///   Расположена ли подсистема в отдельной DLL.
    /// </summary>
    property InDLL : boolean  read FInDLL;

    /// <summary>
    ///   Имя файла DLL в которой должна быть расположена подсистема.
    /// </summary>
    property DLLFileName : string  read GetDLLFileName;

    /// <summary>
    ///   Зависимость от других систем.
    /// </summary>
    /// <remarks>
    ///   Активация не возможна, до активации всех указанных систем.
    /// </remarks>
    property Requirements : TArray<TSubSystemMetadata>  read FRequirements;

    /// <summary>
    ///   Метод создания подсистемы.
    /// </summary>
    property Maker : TMakerSubSystem  read FMaker;
  end;
  {$ENDIF}

  /// <summary>
  ///   Подсистема приложения.
  /// </summary>
  ISubSystem =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Intf.ISubSystem;
  {$ELSE}
  interface(IInterface)
  ['{8AFCA906-44BC-4F70-B573-61620C59F359}']
    {$REGION 'Gets&Sets'}
    function GetConfig : ICfg;
    function GetLog : ISANLog;
    function GetIsActive : boolean;
    function GetStatusStr : string;
    {$ENDREGION}

    /// <summary>
    ///   Активация подсистемы.
    /// </summary>
    /// <exception cref="ESubSystemActivationError">
    ///   При неудачной активации.
    /// </sexception>
    procedure Activate();

    /// <summary>
    ///   Деактивация подсистемы.
    /// </summary>
    procedure Deactivate();

    /// <summary>
    ///   Строковый отчет о внутренних данных.
    /// </summary>
    function Report(const ADelimiter : string) : string;

    /// <summary>
    ///   Конфигурация подсистемы
    /// </summary>
    property Config : ICfg  read GetConfig;

    /// <summary>
    ///   Лог, в который подсистема может вести запись.
    /// </summary>
    property Log : ISANLog  read GetLog;

    /// <summary>
    ///   Активна ли подсистема?
    /// </summary>
    property IsActive : boolean  read GetIsActive;

    /// <summary>
    ///   Текстовая строка состояния подсистемы.
    /// </summary>
    property StatusStr : string  read GetStatusStr;
  end;
  {$ENDIF}

  /// <summary>
  ///   Подсистема приложения, которую можно заблокировать.
  /// </summary>
  ISubSystemCanDisable =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Intf.ISubSystemCanDisable;
  {$ELSE}
  interface(ISubSystem)
  ['{87C5E101-19B7-44F9-879A-2E7D4016F93A}']
    {$REGION 'Gets&Sets'}
    function GetEnabled : boolean;
    {$ENDREGION}

    /// <summary>
    ///   Доступна ли система?
    /// </summary>
    /// <summary>
    ///   Если систем не доступна, активация не произойдет
    /// </summary>
    property Enabled : boolean  read GetEnabled;
  end;
  {$ENDIF}

  /// <summary>
  ///   Подсистема приложения, которая может перезаписывать конфигурацию.
  /// </summary>
  ISubSystemCanWriteCfg =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Intf.ISubSystemCanWriteCfg;
  {$ELSE}
  interface(ISubSystem)
  ['{0E2A006B-CBC0-4141-8A44-2AA095CA995F}']
  end;
  {$ENDIF}

  /// <summary>
  ///   Управление подсистемой. Генерик.
  /// </summary>
  TSubSystemControl<T : ISubSystem> = record
  strict private
    class function GetMeta: TSubSystemMetadata; static;
    class function GetInstance: T; static;
    class function GetState: TSubSystemState; static;
    class function GetIsActive: boolean; static;
    class function GetEnabled: boolean; static;


  public
    class operator Implicit(const AControl : TSubSystemControl<T>): T;

    /// <summary>
    ///   Активация подсистемы.
    /// </summary>
    class procedure Activate(out AInstance : T); overload; static;

    /// <summary>
    ///   Активация подсистемы.
    /// </summary>
    /// <param name="AIfNeeded">
    ///   <para>
    ///     <c>True</c> — активация, только если подсистема деактивирована
    ///   </para>
    ///   <para>
    ///     <c>False</c> — реактивация (дизактивация + активация)
    ///   </para>
    /// </param>
    class procedure Activate(const AIfNeeded : boolean; out AInstance : T); overload; static;

    /// <summary>
    ///   Активация подсистемы.
    /// </summary>
    /// <param name="AIfNeeded">
    ///   <para>
    ///     <c>True</c> — активация, только если подсистема деактивирована
    ///   </para>
    ///   <para>
    ///     <c>False</c> — реактивация (дизактивация + активация)
    ///   </para>
    /// </param>
    class procedure Activate(const AIfNeeded : boolean = False); overload; static;

    /// <summary>
    ///   Деактивация подсистемы.
    /// </summary>
    class procedure Deactivate(); static;

    /// <summary>
    ///   Экземпляр подсистемы.
    /// </summary>
    class function TryGet(out AInstance : T; const AIfActive : boolean = False) : boolean; static;

    /// <summary>
    ///   Метаданные подсистемы.
    /// </summary>
    class property Meta : TSubSystemMetadata  read GetMeta;

    /// <summary>
    ///   Экземпляр подсистемы.
    /// </summary>
    class property Instance : T  read GetInstance;

    /// <summary>
    ///   Состояние подсистемы.
    /// </summary>
    class property State : TSubSystemState  read GetState;

    /// <summary>
    ///   Активность подсистемы.
    /// </summary>
    class property IsActive : boolean  read GetIsActive;

    /// <summary>
    ///   Доступность подсистемы.
    /// </summary>
    /// <returns>
    ///   <c>ISubSystemCanDisable(Instance).Enabled</c>.
    ///   <para>
    ///     Если система не существует или не поддержтвает <c>ISubSystemCanDisable</c>,
    ///     возвращает <c>True</c>
    ///   </para>
    /// </returns>
    class property Enabled : boolean  read GetEnabled;
  end;

  /// <summary>
  ///   Пара: метаданные — подсистема.
  /// </summary>
  TSubSystemPair = TPair<TSubSystemMetadata, ISubSystem>;

{$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Управление базовыми интерфейсами подсистем.
  /// </summary>
  Api = record
  strict private
    type TSubSystemsDict = TObjectDictionary<TSubSystemMetadata, ISubSystem>;

  private
    class var FDict : TSubSystemsDict;
    class var FHolder: IInterface;
    class procedure Initialize(); static;
    class procedure Finalize(); static;

  public

    /// <summary>
    ///   Зарегистрировать метаданные подсистемы.
    /// </summary>
    class function Reg(const ATypeInfo : PTypeInfo; const AIsCritical, AInDLL: boolean; const ARequirements : array of PTypeInfo) : TSubSystemMetadata; overload; static;

      /// <summary>
      ///   Зарегистрировать метаданные подсистемы.
      /// </summary>
    class function Reg<TSubSystem : ISubSystem>(const AIsCritical, AInDLL: boolean; const ARequirements : array of PTypeInfo) : TSubSystemMetadata; overload; static;

    /// <summary>
    ///   Зарегистрировать метаданные подсистемы.
    /// </summary>
    class function Reg(const ATypeInfo : PTypeInfo; const AIsCritical: boolean; const ARequirements : array of PTypeInfo) : TSubSystemMetadata; overload; static;

      /// <summary>
      ///   Зарегистрировать метаданные подсистемы.
      /// </summary>
    class function Reg<TSubSystem : ISubSystem>(const AIsCritical: boolean; const ARequirements : array of PTypeInfo) : TSubSystemMetadata; overload; static;

    /// <summary>
    ///   Зарегистрировать метаданные подсистемы.
    /// </summary>
    class function Reg(const ATypeInfo : PTypeInfo; const AIsCritical: boolean = True; const AInDLL: boolean = False) : TSubSystemMetadata; overload; static;

      /// <summary>
      ///   Зарегистрировать метаданные подсистемы.
      /// </summary>
    class function Reg<TSubSystem : ISubSystem>(const AIsCritical: boolean = True; const AInDLL: boolean = False) : TSubSystemMetadata; overload; static;


    /// <summary>
    ///   Зарегистрировать метаданные подсистемы.
    /// </summary>
    class procedure RegMaker(const ATypeInfo : PTypeInfo; const AMaker: TMakerSubSystem); overload; static;

      /// <summary>
      ///   Зарегистрировать метаданные подсистемы.
      /// </summary>
    class procedure RegMaker<TSubSystem : ISubSystem>(const AMaker: TMakerSubSystem); overload; static;


    /// <summary>
    ///   Разрегистрировать метаданные подсистемы.
    /// </summary>
    class function UnReg<TSubSystem : ISubSystem>() : boolean; overload; static;

    /// <summary>
    ///   Разрегистрировать метаданные подсистемы.
    /// </summary>
    class function UnReg(const ATypeInfo : PTypeInfo) : boolean; overload; static;


    /// <summary>
    ///   Метаданные подсистемы.
    /// </summary>
    class function Get<TSubSystem : ISubSystem> : TSubSystemMetadata; overload; static;

    /// <summary>
    ///   Метаданные подсистемы.
    /// </summary>
    class function Get(const ATypeInfo : PTypeInfo) : TSubSystemMetadata; overload; static;

    /// <summary>
    ///   Метаданные подсистемы.
    /// </summary>
    class function Get(const AGUID : TGUID) : TSubSystemMetadata; overload; static;


    /// <summary>
    ///   Состояние подсистемы.
    /// </summary>
    class function State(const AMetadata : TSubSystemMetadata) : TSubSystemState; static;


    /// <summary>
    ///   Все метаданные подсистем.
    /// </summary>
    class function All : TSubSystemsDict.TKeyCollection; static;

    /// <summary>
    ///   Создать все зарегистрированные подсистемы.
    /// </summary>
    class procedure MakeAll; static;

    /// <summary>
    ///   Очистить все экземпляры подсистем.
    /// </summary>
    class procedure DoneAll; static;

    /// <summary>
    ///   Отчет о состоянии подсистем.
    /// </summary>
    class procedure Report(const AReporter : TReporter); static;

    /// <summary>
    ///   Словарь подсистем по метаданным.
    /// </summary>
    class property SubSystems : TSubSystemsDict  read FDict;
  end;

implementation

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.Math, System.StrUtils,
  {$ENDREGION}

  {$REGION 'San'}
  San.Intfs,
  San.Intfs.Holders,
  San.SubSystems.Messages,
  San.SubSystems.Exceptions;
  {$ENDREGION}
{$ENDREGION}

{$REGION 'TSubSystemMetadata'}
function TSubSystemMetadata.GetDLLFileName: string;
begin
  Result := Format('SS%s.dll', [Name]);
end;

function TSubSystemMetadata.GetGUID: TGUID;
begin
  Result := FTypeInfo^.TypeData^.Guid;
end;

function TSubSystemMetadata.GetName: string;
begin
  Result := Copy(FTypeInfo^.NameFld.ToString, 2);
end;
{$ENDREGION}

{$I San.SubSystems.Intf.GenImpl.inc}

{$REGION 'Api'}
class procedure Api.Initialize;
begin
  FDict := TSubSystemsDict.Create([doOwnsKeys]);
end;

class procedure Api.Finalize;
begin
  FreeAndNil(FDict);
end;

class function Api.Reg(const ATypeInfo: PTypeInfo; const AIsCritical,
  AInDLL: boolean; const ARequirements: array of PTypeInfo): TSubSystemMetadata;
var
  i : integer;
begin
  Result := TSubSystemMetadata.Create();
  try
    Result.FTypeInfo     := ATypeInfo;
    Result.FIsCritical   := AIsCritical;
    Result.FInDLL        := AInDLL;

    SetLength(Result.FRequirements, Length(ARequirements));
    for i := Low(ARequirements) to High(ARequirements) do
    try
      Result.FRequirements[i] := Api.Get(ARequirements[i]);
    except
      ESubSystemMeta.CreateResFmt(@sSubSystemRequirementNotRegistredErrorFmt, [Result.Name, ARequirements[i]^.NameFld.ToString()]).RaiseOuter;
    end;
      //
    FDict.Add(Result, nil);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

class function Api.Reg<TSubSystem>(const AIsCritical, AInDLL: boolean;
  const ARequirements: array of PTypeInfo): TSubSystemMetadata;
begin
  Result := Reg(TypeInfo(TSubSystem), AIsCritical, AInDLL, ARequirements);
end;

class function Api.Reg(const ATypeInfo: PTypeInfo; const AIsCritical: boolean;
  const ARequirements: array of PTypeInfo): TSubSystemMetadata;
begin
  Result := Reg(ATypeInfo, AIsCritical, False, ARequirements);
end;

class function Api.Reg<TSubSystem>(const AIsCritical: boolean;
  const ARequirements: array of PTypeInfo): TSubSystemMetadata;
begin
  Result := Reg(TypeInfo(TSubSystem), AIsCritical, False, ARequirements);
end;

class function Api.Reg(const ATypeInfo: PTypeInfo; const AIsCritical,
  AInDLL: boolean): TSubSystemMetadata;
begin
  Result := Reg(ATypeInfo, AIsCritical, AInDLL, []);
end;

class function Api.Reg<TSubSystem>(const AIsCritical,
  AInDLL: boolean): TSubSystemMetadata;
begin
  Result := Reg(TypeInfo(TSubSystem), AIsCritical, AInDLL, []);
end;

class procedure Api.RegMaker(const ATypeInfo: PTypeInfo;
  const AMaker: TMakerSubSystem);
begin
  Get(ATypeInfo).FMaker := AMaker;
end;

class procedure Api.RegMaker<TSubSystem>(const AMaker: TMakerSubSystem);
begin
  RegMaker(TypeInfo(TSubSystem), AMaker);
end;

class function Api.UnReg(const ATypeInfo: PTypeInfo): boolean;
var
  M : TSubSystemMetadata;
begin
  for M in All do
    if (M.TypeInfo = ATypeInfo) then
    begin
      FDict.Remove(M);
      Exit(True);
    end;

  Result := False;
end;

class function Api.UnReg<TSubSystem>: boolean;
begin
  Result := UnReg(TypeInfo(TSubSystem));
end;

class procedure Api.MakeAll;
var
  M : TSubSystemMetadata;
  ssG  : TGUID;
  SS : ISubSystem;
begin
  Api.DoneAll;

  for M in All do
  try
    ssG := M.TypeInfo^.TypeData^.Guid;

    if M.InDLL then
      raise ENotSupportedException.Create('Meta.InDLL')
    else
    if Assigned(M.Maker) then
      SS := M.Maker()

    else
      raise ESubSystemMakeError.CreateRes(@sMakerIsNil);

    if not Assigned(SS) then
      raise ESubSystemMakeError.CreateRes(@sSubSystemIsNil);

    FDict[M] := SS;
  except
    ESubSystemMakeError.CreateResFmt(@sFailsMakeSubSystemFmt, [M.Name]).RaiseOuter;
  end;
end;

class procedure Api.DoneAll;
var
  M : TSubSystemMetadata;
begin
  for M in All do
    FDict[M] := nil;
end;

class function Api.All: TSubSystemsDict.TKeyCollection;
begin
  Result := FDict.Keys;
end;

class function Api.Get(const AGUID: TGUID): TSubSystemMetadata;
var
  M : TSubSystemMetadata;
  P : PTypeInfo;
begin
  for M in All do
  begin
    P := M.TypeInfo;

    repeat

      if (P^.TypeData^.Guid = AGUID) then
        Exit(M);

      P := P^.TypeData^.IntfParent^;
    until P = TypeInfo(ISubSystem);
  end;

  raise ENotSupportedException.Create(TSubSystemMetadata.UnitName + '.Get for ' + GUIDToString(AGUID));
end;

class function Api.Get(const ATypeInfo: PTypeInfo): TSubSystemMetadata;
begin
  try
    Result := Get(ATypeInfo^.TypeData^.Guid);
  except
    ESubSystemMeta.CreateResFmt(@sInternalErrorForFmt, [ATypeInfo^.Name]).RaiseOuter();
    Result := nil;
  end;
end;

class function Api.Get<TSubSystem>: TSubSystemMetadata;
begin
  Result := Get(TypeInfo(TSubSystem));
end;

class function Api.State(const AMetadata: TSubSystemMetadata): TSubSystemState;
var
  SS : ISubSystem;
begin
  if not (FDict.TryGetValue(AMetadata, SS) and Assigned(SS)) then
    Result := TSubSystemState.sssUnassigned
  else if SS.IsActive then
    Result := TSubSystemState.sssActive
  else
    Result := TSubSystemState.sssInactive;
end;

class procedure Api.Report(const AReporter: TReporter);

  function StateToStr(const AState : TSubSystemState) : string;
  begin
    Result := Copy
    (
      GetEnumName(TypeInfo(TSubSystemState), Ord(AState)),
      4
    );
  end;

var
  L : integer;
  M : TSubSystemMetadata;
begin
  L := 0;
  for M in All do
    L := Max(L, Length(M.Name));

  for M in All do
    AReporter.Rep([M.Name, DupeString(' ', L - Length(M.Name)), ' — ', StateToStr(State(M))]);
end;
{$ENDREGION}

initialization
  THolderApi.Init(Api.FHolder, Api.Initialize, Api.Finalize);

end{$WARNINGS OFF}.
{$ENDIF}
