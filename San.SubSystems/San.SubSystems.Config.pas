{*******************************************************}
{                                                       }
{       Управление конфигурацией                        }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Управление конфигурацией
/// </summary>
unit San.SubSystems.Config;
{$I San.SubSystems.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Classes, System.Generics.Collections, System.TypInfo,
  System.StrUtils, System.IOUtils,
  {$ENDREGION}

  {$REGION 'San'}
  San.SubSystems.Reporter,
  San.SubSystems.Config.Storage.Intf,
  San.SubSystems.Config.Storage.Ini,
  San.SubSystems.Config.Storage,
  San.SubSystems.Config.Intf
  {$ENDREGION}
;
{$ENDREGION}

{$ENDIF}

{$IFNDEF USE_ALIASES}
type
  /// <summary>
  ///   Управление конфигурацией подсистем
  /// </summary>
  Api = record
  private
    class procedure Initialize(); static;

  strict private
    class function GetDefaultStorage: TCfgLocationStorageMeta; static;
    class function GetUniqueStr: string; static;
    class procedure SetUniqueStr(const AValue: string); static;
    class function GetProgramData: string; static;
    class procedure SetProgramData(const AValue: string); static;
    class function GetMainLogName: string; static;
    class procedure SetMainLogName(const AValue: string); static;

  public const

    /// <summary>
    ///   Управление хранилищем
    /// </summary>
    Storage : San.SubSystems.Config.Storage.Api = ();

    /// <summary>
    ///   Презагрузка конфигурации
    /// </summary>
    /// <param name="AStorage">
    ///   Хранилище. nil — хранилище по-умолчанию.
    /// </param>
    class procedure ReLoad(AStorage : ICfgStorage = nil); static;

    /// <summary>
    ///   Перезапись конфигурации
    /// </summary>
    /// <param name="AStorage">
    ///   Хранилище. nil — хранилище по-умолчанию.
    /// </param>
    class procedure ReWrite(AStorage : ICfgStorage = nil); static;

    /// <summary>
    ///   Сохранить отладочный файл в ProgramData
    /// </summary>
    class procedure DebugWriteFile(const ADoWrite : TProc<string>;
      const AFolderInProgramData, AFileExt : string;
      const APrefix1 : string = ''; const APrefix2 : string = ''); overload; static;

    /// <summary>
    ///   Сохранить отладочный файл в ProgramData
    /// </summary>
    class procedure DebugWriteFile(const ATextData : string;
      const AFolderInProgramData, AFileExt : string;
      const APrefix1 : string = ''; const APrefix2 : string = ''); overload; static;

    /// <summary>
    ///   Метаданные хранилища по-умолчанию.
    /// </summary>
    class property DefaultStorage : TCfgLocationStorageMeta  read GetDefaultStorage;

    /// <summary>
    ///   Уникальная строка которая задается в дочернем коде
    ///   и позволяет избежать колизий с путями и других подобных проблем.
    /// </summary>
    class property UniqueStr : string  read GetUniqueStr  write SetUniqueStr;

    /// <summary>
    ///   Путь к папке, на которую у приложения есть права на запись.
    /// </summary>
    class property ProgramData : string  read GetProgramData  write SetProgramData;

    /// <summary>
    ///   Имя основного лога.
    /// </summary>
    class property MainLogName : string  read GetMainLogName  write SetMainLogName;
  end;

implementation

{$REGION 'uses'}
uses
  {$REGION 'San'}
  San.SubSystems.Intf, San.SubSystems.Impl
  {$ENDREGION}
  ;
{$ENDREGION}

type
  /// <summary>
  ///   Статическая часть конфигурации. Реализация.
  /// </summary>
  TCfgStatic = class(TSubSystemCfg, ICfgStatic)
  strict private
    FProgramData, FMainLogName : string;
  protected
    {$REGION 'ICfgStatic'}
    function GetProgramData : string;
    procedure SetProgramData(const AValue : string);
    function GetMainLogName : string;
    procedure SetMainLogName(const AValue : string);
    {$ENDREGION}

    function GetSection : string; override;
    procedure DoReadFromStorage(const AStorage : TCfgSectionOfStorage); override;
    procedure DoReport(const AReporter : TReporter); override;
  end;

{$REGION 'TCfgStatic'}
procedure TCfgStatic.DoReadFromStorage(const AStorage: TCfgSectionOfStorage);
begin
  inherited;
  FProgramData := AStorage.Read('ProgramData', ExtractFilePath(ParamStr(0)));
  FMainLogName := AStorage.Read('Log', Api.UniqueStr + '|yyyy_mm_dd|.log');
end;

procedure TCfgStatic.DoReport(const AReporter: TReporter);
begin
  inherited;
  AReporter.Rep('ProgramData: ' + FProgramData);
  AReporter.Rep('MainLogName: ' + FMainLogName);
end;

function TCfgStatic.GetSection: string;
begin
  Result := 'Main'
end;

function TCfgStatic.GetMainLogName: string;
begin
  Result := FMainLogName;
end;

function TCfgStatic.GetProgramData: string;
begin
  Result := FProgramData;
end;

procedure TCfgStatic.SetMainLogName(const AValue: string);
begin
  FMainLogName := AValue;
end;

procedure TCfgStatic.SetProgramData(const AValue: string);
begin
  FProgramData := AValue;
end;
{$ENDREGION}

var
  API_UniqueStr : string;
  API_DefaultLocationStorageMeta : TCfgLocationStorageMeta;
  API_FConfig: ICfgStatic;

{$REGION 'Api'}
class procedure Api.Initialize;
begin
  API_DefaultLocationStorageMeta := TCfgLocationStorageMeta.Create(function : ICfgMetaStorage
    begin
      Result := San.SubSystems.Config.Storage.Intf.Api.DefaultMeta;
    end
  );

  API_FConfig := TCfgStatic.Create(nil);
end;

class procedure Api.DebugWriteFile(const ADoWrite: TProc<string>;
  const AFolderInProgramData, AFileExt, APrefix1, APrefix2: string);
var
  FilePath, FileName : string;
begin
  FilePath := ProgramData + '\' + AFolderInProgramData + '\';

  FileName := IfThen(APrefix1 <> '', APrefix1 + '_') + IfThen(APrefix2 <> '', APrefix2 + '_') + FormatDateTime('yyyy_mm_dd__hh_mm_ss_zzz', Now()) + AFileExt;

  ForceDirectories(FilePath);

  ADoWrite(FilePath + FileName);
end;

class procedure Api.DebugWriteFile(const ATextData, AFolderInProgramData,
  AFileExt, APrefix1, APrefix2: string);
begin
  DebugWriteFile(
    procedure (AFileName : string)
    begin
      TFile.WriteAllText(AFileName, ATextData);
    end,
    AFolderInProgramData,
    AFileExt,
    APrefix1,
    APrefix2
  );
end;

class function Api.GetDefaultStorage: TCfgLocationStorageMeta;
begin
  Result := API_DefaultLocationStorageMeta;
end;

class function Api.GetMainLogName: string;
begin
  Result := API_FConfig.MainLogName;
end;

class function Api.GetProgramData: string;
begin
  Result := API_FConfig.ProgramData;
end;

class function Api.GetUniqueStr: string;
begin
  Result := API_UniqueStr;
end;

class procedure Api.SetMainLogName(const AValue: string);
begin
  API_FConfig.MainLogName := AValue;
end;

class procedure Api.SetProgramData(const AValue: string);
begin
  API_FConfig.ProgramData := AValue;
end;

class procedure Api.SetUniqueStr(const AValue: string);
begin
  API_UniqueStr := AValue;
end;

class procedure Api.ReLoad(AStorage: ICfgStorage);
var
  SS : ISubSystem;
begin
  if not Assigned(AStorage) then
    AStorage := DefaultStorage.MakeStorageInstance;

  for SS in San.SubSystems.Intf.Api.SubSystems.Values do
    if Assigned(SS) then
      SS.Config.ReadFromStorage(AStorage);

  API_FConfig.ReadFromStorage(AStorage);

  if AStorage.IsWriteActionsDetected then
    AStorage.Update(True);
end;

class procedure Api.ReWrite(AStorage: ICfgStorage);
var
  SS : ISubSystem;
begin
  if not Assigned(AStorage) then
    AStorage := DefaultStorage.MakeStorageInstance;

  for SS in San.SubSystems.Intf.Api.SubSystems.Values do
    if Assigned(SS) then
      SS.Config.WriteToStorage(AStorage);

  API_FConfig.WriteToStorage(AStorage);

  AStorage.Update();
end;
{$ENDREGION}

initialization
  Api.Initialize();

end{$WARNINGS OFF}.
{$ENDIF}
