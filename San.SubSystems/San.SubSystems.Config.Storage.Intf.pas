{*******************************************************}
{                                                       }
{       Tипы данных: Интерфейсы хранилища конфигурации  }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Tипы данных: Интерфейсы хранилища конфигурации
/// </summary>
unit San.SubSystems.Config.Storage.Intf;
{$I San.SubSystems.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Types, System.Generics.Collections,
  {$ENDREGION}

  {$REGION 'San'}
  San.SubSystems.Reporter, San.StrUtils
  {$ENDREGION}
  ;
{$ENDREGION}

{$ENDIF}

type
  /// <summary>
  ///   Хранилище конфигурации.
  /// </summary>
  ICfgStorage =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Config.Storage.Intf.ICfgStorage;
  {$ELSE}
  interface(IInterface)
  ['{AD8E397E-ADC5-4522-8698-D564CCA319BB}']
    {$REGION 'Gets&Sets'}
    function GetLocation : string;
    function GetIsWriteActionsDetected : boolean;
    {$ENDREGION}

    /// <summary>
    ///   Обновить хранилище записанными данными.
    /// </summary>
    /// <param name="ASuppressErrors">
    ///   Подавлять ошибки (например может не быть прав на запись в хранилище)
    /// </param>
    /// <returns>
    ///   <c>True</c> — успешно, <c>False</c> — была подавлена ошибка
    /// </returns>
    function Update(const ASuppressErrors : boolean = False) : boolean;

    /// <summary>
    ///   Расположение хранилища.
    /// </summary>
    property Location : string read GetLocation;

    /// <summary>
    ///   При работе с хранилищем зафиксированы действия записи
    /// </summary>
    property IsWriteActionsDetected : boolean  read GetIsWriteActionsDetected;
  end;
  {$ENDIF}

  /// <summary>
  ///   Хранилище конфигурации, разделенное на секции.
  /// </summary>
  ICfgSectionedStorage =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Config.Storage.Intf.ICfgSectionedStorage;
  {$ELSE}
  interface(ICfgStorage)
  ['{EA290394-9FD1-4513-968D-8519C37B5E5D}']
    function ExistsSection(const ASection : string) : boolean;
    function ExistsValue(const ASection : string; const AIdent : string) : boolean;
    function Read(const ASection : string; const AIdent : string; ADefault : Variant) : Variant;
    function ReadSectionValues(const ASection : string) : TStringDynArray;

    procedure Write(const ASection : string; const AIdent : string; AValue : Variant);
  end;
  {$ENDIF}

  /// <summary>
  ///   Хранилище конфигурации, разделенное на секции. Обертка.
  /// </summary>
  TCfgSectionedStorage =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Config.Storage.Intf.TCfgSectionedStorage;
  {$ELSE}
  record
  strict private
    function GetIsWriteActionsDetected: boolean;
    function GetLocation: string;

  private
    FInnerData : ICfgSectionedStorage;

  public
    class operator Implicit(const A : TCfgSectionedStorage): ICfgSectionedStorage;
    class operator Implicit(const A : ICfgSectionedStorage): TCfgSectionedStorage;
    class operator Implicit(const A : ICfgStorage): TCfgSectionedStorage;

    procedure Update();
    function ExistsSection(const ASection : string) : boolean;
    function ExistsValue(const ASection : string; const AIdent : string) : boolean;
    function Read(const ASection : string; const AIdent : string; ADefault : Variant) : Variant;
    function ReadSectionValues(const ASection : string) : TStringDynArray;
    function ReadSectionNamesAndValues(const ASection : string) : TArray<TPairStrStr>;
    function ReadBool(const ASection : string; const AIdent : string; const ADefault : boolean) : boolean;

    procedure Write(const ASection : string; const AIdent : string; AValue : Variant);
    procedure WriteBool(const ASection : string; const AIdent : string; const AValue : boolean);

    property Location : string read GetLocation;
    property IsWriteActionsDetected : boolean  read GetIsWriteActionsDetected;
  end;
  {$ENDIF}

  /// <summary>
  ///   Обертка над секционным хранилищем в рамках одной секции.
  /// </summary>
  TCfgSectionOfStorage =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Config.Storage.Intf.TCfgSectionOfStorage;
  {$ELSE}
  record
  strict private
    function GetIsWriteActionsDetected: boolean;
    function GetLocation: string;

  private
    FInternal : TCfgSectionedStorage;
    FSection : string;

  public
    class function Create (const AInternalStorage : TCfgSectionedStorage;
      const ASection : string) : TCfgSectionOfStorage; static;

    procedure Update();
    function ExistsSection() : boolean;
    function ExistsValue(const AIdent : string) : boolean;
    function Read(const AIdent : string; ADefault : Variant) : Variant;
    function ReadSectionValues() : TStringDynArray;
    function ReadSectionNamesAndValues() : TArray<TPairStrStr>;
    function ReadBool(const AIdent : string; const ADefault : boolean) : boolean;

    procedure Write(const AIdent : string; AValue : Variant);
    procedure WriteBool(const AIdent : string; const AValue : boolean);

    property Location : string read GetLocation;
    property IsWriteActionsDetected : boolean  read GetIsWriteActionsDetected;
    property InternalStorage : TCfgSectionedStorage  read FInternal;
    property Section : string  read FSection;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
type
  /// <summary>
  ///   Метаданные хранилища конфигураций
  /// </summary>
  ICfgMetaStorage = interface
  ['{3FAF4B6A-4F44-40DB-B722-0A8CDDFD380E}']
    /// <summary>
    ///   Создать экземпляр хранилища.
    /// </summary>
    function MakeStorageInstance (const ALocation : string) : ICfgStorage;

    /// <summary>
    ///   Проверить возможность записи в хранилище.
    /// </summary>
    function IsWritableStorage (const ALocation : string) : boolean;

    /// <summary>
    ///   Отчет о хранилище.
    /// </summary>
    /// <param name="ARaw">
    ///   <c>True</c> — низкоуровнивые данные хранилища
    /// </param>
    procedure Report(const ALocation : string; const AReporter : TReporter;
      const ARaw : boolean = False);
  end;

  /// <summary>
  ///   Метаданные о расположении через динамическое получение метаданных хранилища
  /// </summary>
  TCfgLocationStorageMeta = record
  private
    FInnerData : IInterface;

  strict private
    function GetMeta: ICfgMetaStorage;
    function GetLocation: string;
    procedure SetLocation(const AValue: string);

  public
    class operator Implicit(const A : TCfgLocationStorageMeta): ICfgStorage;
    class operator Implicit(const A : TCfgLocationStorageMeta): TCfgSectionedStorage;

    /// <summary>
    ///   Создать экземпляр хранилища.
    /// </summary>
    function MakeStorageInstance () : ICfgStorage;

    /// <summary>
    ///   Проверить возможность записи в хранилище.
    /// </summary>
    function IsWritableStorage () : boolean;

    /// <summary>
    ///   Отчет о хранилище.
    /// </summary>
    /// <param name="ARaw">
    ///   <c>True</c> — низкоуровнивые данные хранилища
    /// </param>
    procedure Report(const AReporter : TReporter;
      const ARaw : boolean = False);

    /// <summary>
    ///   Инициализация
    /// </summary>
    class function Create(const AGetMeta : TFunc<ICfgMetaStorage>) : TCfgLocationStorageMeta; static;

    /// <summary>
    ///   Расположение
    /// </summary>
    property Location : string  read GetLocation  write SetLocation;

    /// <summary>
    ///   Метаданные хранилища
    /// </summary>
    property Meta : ICfgMetaStorage  read GetMeta;
  end;

  /// <summary>
  ///   Управление хранилищами конфигурации
  /// </summary>
  Api = record
  strict private
    class var FDefaultMeta : ICfgMetaStorage;

  public
    /// <summary>
    ///   Метаданные хранилища конфигураций по-умолчанию.
    /// </summary>
    class property DefaultMeta : ICfgMetaStorage  read FDefaultMeta  write FDefaultMeta;
  end;

implementation

{$REGION 'uses'}
uses
  {$REGION 'San'}
  San.Intfs, San.Intfs.Basic
  {$ENDREGION}
  ;
{$ENDREGION}

type
  TCfgLocationStorageMetaInnerData = class(TInterfacedObject, IObjRef)
  private
    FGetMeta : TFunc<ICfgMetaStorage>;
    FLocation: string;

  protected
    {$REGION 'IObjRef'}
    function GetObjRef : TObject;
    {$ENDREGION}
  end;

  TCfgLocationStorageMetaHelper = record helper for TCfgLocationStorageMeta
    function InnerData : TCfgLocationStorageMetaInnerData;
  end;

{$REGION 'TCfgLocationStorageMetaInnerData'}
function TCfgLocationStorageMetaInnerData.GetObjRef: TObject;
begin
  Result := Self;
end;
{$ENDREGION}

{$REGION 'TCfgLocationStorageMetaHelper'}
function TCfgLocationStorageMetaHelper.InnerData: TCfgLocationStorageMetaInnerData;
begin
  Result := (FInnerData as IObjRef).ObjRef as TCfgLocationStorageMetaInnerData;
end;
{$ENDREGION}

{$REGION 'TCfgSectionedStorage'}
function TCfgSectionedStorage.ExistsSection(
  const ASection: string): boolean;
begin
  Result := FInnerData.ExistsSection(ASection);
end;

function TCfgSectionedStorage.ExistsValue(const ASection,
  AIdent: string): boolean;
begin
  Result := FInnerData.ExistsValue(ASection, AIdent);
end;

function TCfgSectionedStorage.GetIsWriteActionsDetected: boolean;
begin
  Result := FInnerData.IsWriteActionsDetected;
end;

function TCfgSectionedStorage.GetLocation: string;
begin
  Result := FInnerData.Location;
end;

class operator TCfgSectionedStorage.Implicit(
  const A: TCfgSectionedStorage): ICfgSectionedStorage;
begin
  Result := A.FInnerData;
end;

class operator TCfgSectionedStorage.Implicit(
  const A: ICfgSectionedStorage): TCfgSectionedStorage;
begin
  Result.FInnerData := A;
end;

class operator TCfgSectionedStorage.Implicit(
  const A: ICfgStorage): TCfgSectionedStorage;
begin
  San.Intfs.Api.MustSupports<ICfgSectionedStorage>(A, Result.FInnerData);
end;

function TCfgSectionedStorage.Read(const ASection, AIdent: string;
  ADefault: Variant): Variant;
begin
  Result := FInnerData.Read(ASection, AIdent, ADefault);
end;

function TCfgSectionedStorage.ReadSectionValues(
  const ASection: string): TStringDynArray;
begin
  Result := FInnerData.ReadSectionValues(ASection);
end;

procedure TCfgSectionedStorage.Update;
begin
  FInnerData.Update();
end;

function TCfgSectionedStorage.ReadSectionNamesAndValues(const ASection : string) : TArray<TPairStrStr>;
var
  Values : TStringDynArray;
  i : integer;
begin
  Values := FInnerData.ReadSectionValues(ASection);
  SetLength(Result, Length(Values));

  for i := Low(Values) to High(Values) do
    Result[i] := San.StrUtils.Api.NameValue(Values[i]);
end;

procedure TCfgSectionedStorage.Write(const ASection, AIdent: string;
  AValue: Variant);
begin
  FInnerData.Write(ASection, AIdent, AValue);
end;

function TCfgSectionedStorage.ReadBool(const ASection : string; const AIdent: string;
  const ADefault: boolean): boolean;
var
  S : string;
  i : integer;
begin
  if not FInnerData.ExistsValue(ASection, AIdent) then
    Exit(ADefault);

   S := FInnerData.Read(ASection, AIdent, '');

  if TryStrToBool(S, Result) then
    Exit;

  if TryStrToInt(S, i) then
    Exit(i <> 0);

  Result := StrToBool(S);
end;

procedure TCfgSectionedStorage.WriteBool(const ASection : string; const AIdent: string;
  const AValue: boolean);
begin
  FInnerData.Write(ASection, AIdent, BoolToStr(AValue, True));
end;
{$ENDREGION}

{$REGION 'TCfgSectionOfStorage'}
class function TCfgSectionOfStorage.Create(
  const AInternalStorage: TCfgSectionedStorage;
  const ASection: string): TCfgSectionOfStorage;
begin
  Result.FInternal  := AInternalStorage;
  Result.FSection   := ASection;
end;

function TCfgSectionOfStorage.ExistsSection: boolean;
begin
  Result := FInternal.ExistsSection(FSection);
end;

function TCfgSectionOfStorage.ExistsValue(const AIdent: string): boolean;
begin
  Result := FInternal.ExistsValue(FSection, AIdent);
end;

function TCfgSectionOfStorage.GetIsWriteActionsDetected: boolean;
begin
  Result := FInternal.IsWriteActionsDetected;
end;

function TCfgSectionOfStorage.GetLocation: string;
begin
  Result := FInternal.Location;
end;

function TCfgSectionOfStorage.Read(const AIdent: string;
  ADefault: Variant): Variant;
begin
  Result := FInternal.Read(FSection, AIdent, ADefault);
end;

function TCfgSectionOfStorage.ReadBool(const AIdent: string;
  const ADefault: boolean): boolean;
begin
  Result := FInternal.ReadBool(FSection, AIdent, ADefault);
end;

function TCfgSectionOfStorage.ReadSectionValues: TStringDynArray;
begin
  Result := FInternal.ReadSectionValues(FSection);
end;

procedure TCfgSectionOfStorage.Update;
begin
  FInternal.Update();
end;

function TCfgSectionOfStorage.ReadSectionNamesAndValues() : TArray<TPairStrStr>;
begin
  Result := FInternal.ReadSectionNamesAndValues(FSection);
end;

procedure TCfgSectionOfStorage.Write(const AIdent: string; AValue: Variant);
begin
  FInternal.Write(FSection, AIdent, AValue);
end;

procedure TCfgSectionOfStorage.WriteBool(const AIdent: string;
  const AValue: boolean);
begin
  FInternal.Write(FSection, AIdent, AValue);
end;
{$ENDREGION}

{$REGION 'TCfgLocationStorageMeta'}
class function TCfgLocationStorageMeta.Create(
  const AGetMeta: TFunc<ICfgMetaStorage>): TCfgLocationStorageMeta;
var
  InnerData : TCfgLocationStorageMetaInnerData;
begin
  InnerData := TCfgLocationStorageMetaInnerData.Create();
  Result.FInnerData := InnerData;

  InnerData.FGetMeta := AGetMeta;
end;

class operator TCfgLocationStorageMeta.Implicit(
  const A: TCfgLocationStorageMeta): ICfgStorage;
begin
  Result := A.MakeStorageInstance();
end;

function TCfgLocationStorageMeta.GetMeta: ICfgMetaStorage;
begin
  Result := InnerData.FGetMeta();
end;

function TCfgLocationStorageMeta.GetLocation: string;
begin
  Result := InnerData.FLocation;
end;

procedure TCfgLocationStorageMeta.SetLocation(const AValue: string);
begin
  InnerData.FLocation := AValue;
end;

class operator TCfgLocationStorageMeta.Implicit(
  const A: TCfgLocationStorageMeta): TCfgSectionedStorage;
begin
  San.Intfs.Api.MustSupports<ICfgSectionedStorage>(A.MakeStorageInstance(), Result.FInnerData);
end;

function TCfgLocationStorageMeta.IsWritableStorage: boolean;
begin
  Result := Meta.IsWritableStorage(Location);
end;

function TCfgLocationStorageMeta.MakeStorageInstance: ICfgStorage;
begin
  Result := Meta.MakeStorageInstance(Location);
end;

procedure TCfgLocationStorageMeta.Report(const AReporter: TReporter;
  const ARaw : boolean = False);
begin
  Meta.Report(Location, AReporter, ARaw);
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
