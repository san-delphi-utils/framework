{*******************************************************}
{                                                       }
{       Управление подсистемами                         }
{       Публичная часть Api, для встраивания            }
{       в унаследованные библиотеки                     }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
const
  /// <summary>
  ///   Метаданные.
  /// </summary>
  Meta : San.SubSystems.Intf.Api = ();

  /// <summary>
  ///   Управление конфигурацией.
  /// </summary>
  Config : San.SubSystems.Config.Api = ();

  /// <summary>
  ///   Управление подсистемой.
  /// </summary>
  class function Control<T : ISubSystem> : TSubSystemControl<T>; static;
