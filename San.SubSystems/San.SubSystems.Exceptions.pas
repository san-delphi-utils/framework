{*******************************************************}
{                                                       }
{       Базовые исключения                              }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Базовые исключения
/// </summary>
unit San.SubSystems.Exceptions;
{$I San.SubSystems.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils,
  {$ENDREGION}

  {$REGION 'San'}
  San.Excps
  {$ENDREGION}
;
{$ENDREGION}

{$ENDIF}

type
  /// <summary>
  ///   Ошибка подсистемы
  /// </summary>
  ESubSystem =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Exceptions.ESubSystem;
  {$ELSE}
  class(ESanBasic)
  end;
  {$ENDIF}

  /// <summary>
  ///   Ошибка метаданных подсистемы
  /// </summary>
  ESubSystemMeta =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Exceptions.ESubSystemMeta;
  {$ELSE}
  class(ESubSystem)
  end;
  {$ENDIF}

  /// <summary>
  ///   Ошибка: при создании подсистемы произошла ошибка.
  /// </summary>
  ESubSystemMakeError =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Exceptions.ESubSystemMakeError;
  {$ELSE}
  class(ESubSystem);
  {$ENDIF}

  /// <summary>
  ///   Ошибка: при активации подсистемы произошла ошибка.
  /// </summary>
  ESubSystemActivationError =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Exceptions.ESubSystemActivationError;
  {$ELSE}
  class(ESubSystem);
  {$ENDIF}

{$IFNDEF USE_ALIASES}
implementation

end{$WARNINGS OFF}.
{$ENDIF}
