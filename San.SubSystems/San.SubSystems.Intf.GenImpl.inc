{*******************************************************}
{                                                       }
{       Базовые интерфейсы подсистем.                   }
{       Реализация контроля.                            }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$REGION 'TSubSystemControl<T>'}
class operator TSubSystemControl<T>.Implicit(
  const AControl: TSubSystemControl<T>): T;
begin
  AControl.TryGet(Result);
end;

class function TSubSystemControl<T>.GetMeta: TSubSystemMetadata;
begin
  Result := San.SubSystems.Intf.Api.Get<T>;
end;

class function TSubSystemControl<T>.TryGet(out AInstance: T;
  const AIfActive: boolean): boolean;
var
  M : TSubSystemMetadata;
  SS : ISubSystem;
begin
  M := Meta;
  Result := San.SubSystems.Intf.Api.SubSystems.TryGetValue(M, SS)
        and Supports(SS, M.GUID, AInstance)
        and (not AIfActive or AInstance.IsActive);
end;

class function TSubSystemControl<T>.GetInstance: T;
var
  M : TSubSystemMetadata;
  SS : ISubSystem;
begin
  M := Meta;
  SS := San.SubSystems.Intf.Api.SubSystems[M];
  San.Intfs.Api.MustSupports<T>(SS, Result);
end;

class function TSubSystemControl<T>.GetIsActive: boolean;
var
  SS : T;
begin
  Result := TryGet(SS) and SS.IsActive;
end;

class function TSubSystemControl<T>.GetState: TSubSystemState;
begin
  Result := San.SubSystems.Intf.Api.State(Meta);
end;

class function TSubSystemControl<T>.GetEnabled: boolean;
var
  SS : T;
  SSD : ISubSystemCanDisable;
begin
  Result := not TryGet(SS) or not Supports(SS, ISubSystemCanDisable, SSD) or SSD.Enabled;
end;

class procedure TSubSystemControl<T>.Activate(out AInstance: T);
var
  SS : T;
begin
  SS := Instance;
  SS.Activate();
  AInstance := SS;
end;

class procedure TSubSystemControl<T>.Activate(const AIfNeeded : boolean; out AInstance : T);
var
  SS : T;
begin
  SS := Instance;

  if not AIfNeeded or AIfNeeded and not SS.IsActive then
    SS.Activate();

  AInstance := SS;
end;

class procedure TSubSystemControl<T>.Activate(const AIfNeeded : boolean);
var
  Dummy : T;
begin
  Activate(AIfNeeded, Dummy);
end;

class procedure TSubSystemControl<T>.Deactivate;
var
  Inst : T;
begin
  if TryGet(Inst, True) then
    Inst.Deactivate();
end;
{$ENDREGION}
