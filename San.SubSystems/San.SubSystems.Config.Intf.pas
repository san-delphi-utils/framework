{*******************************************************}
{                                                       }
{       Интерфейсы конфигурации                         }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Tипы данных: Интерфейсы конфигурации
/// </summary>
unit San.SubSystems.Config.Intf;
{$I San.SubSystems.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.Types,
  {$ENDREGION}

  {$REGION 'San'}
  San.SubSystems.Config.Storage.Intf
  {$ENDREGION}
;
{$ENDREGION}

{$ENDIF}

type
  /// <summary>
  ///   Блок параметров конфигурации.
  /// </summary>
  ICfg =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Config.Intf.ICfg;
  {$ELSE}
  interface(IInterface)
  ['{223D2F5A-BB8B-481E-AC81-B81EA299A4F1}']

    /// <summary>
    ///   Загрузка из хранилища.
    /// </summary>
    procedure ReadFromStorage(AStorage : ICfgStorage);

    /// <summary>
    ///   Сохранение в хранилище.
    /// </summary>
    procedure WriteToStorage(AStorage : ICfgStorage);

    /// <summary>
    ///   Строковый отчет о внутренних данных.
    /// </summary>
    function Report(const ADelimiter : string; const APrefix : string = '';
      const APostfix : string = '') : string;

  end;
  {$ENDIF}

  /// <summary>
  ///   Блок параметров логгирования.
  /// </summary>
  ICfgLog =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Config.Intf.ICfgLog;
  {$ELSE}
  interface(ICfg)
  ['{A4471CC8-E878-4867-8920-49071356CCCF}']
    {$REGION 'Gets&Sets'}
    function GetLog : string;
    function GetLogLevel : integer;
    {$ENDREGION}

    /// <summary>
    ///   Расположение лога. Если не задано — используется общий лог.
    /// </summary>
    property Log : string  read GetLog;

    /// <summary>
    ///   Уровень логирования. 0 — ничего не логировать
    /// </summary>
    property LogLevel : integer  read GetLogLevel;
  end;
  {$ENDIF}

  /// <summary>
  ///   Блок параметров сетевого клиента или сервера.
  /// </summary>
  ICfgTCP =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Config.Intf.ICfgTCP;
  {$ELSE}
  interface(ICfg)
  ['{5D2487B3-D7EF-431A-89D3-97A33C809D9F}']
    {$REGION 'Gets&Sets'}
    function GetPort : word;
    function GetUseTLS : boolean;
    {$ENDREGION}

    /// <summary>
    ///   Порт.
    /// </summary>
    property Port : word  read GetPort;

    /// <summary>
    ///   Использовать TLS.
    /// </summary>
    property UseTLS : boolean  read GetUseTLS;
  end;
  {$ENDIF}

  /// <summary>
  ///   Блок параметров сетевого клиента.
  /// </summary>
  ICfgTCPClient =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Config.Intf.ICfgTCPClient;
  {$ELSE}
  interface(ICfgTCP)
  ['{C42E5796-C96E-45A7-8182-E3B6CFA78124}']
    {$REGION 'Gets&Sets'}
    function GetHost : string;
    {$ENDREGION}

    /// <summary>
    ///   Хост.
    /// </summary>
    property Host : string  read GetHost;
  end;
  {$ENDIF}

  /// <summary>
  ///   Блок параметров HTTP клиента.
  /// </summary>
  ICfgHTTPClient =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Config.Intf.ICfgHTTPClient;
  {$ELSE}
  interface(ICfgTCPClient)
  ['{8B215838-6BC7-4EAB-9521-A34D7AD19F13}']
    {$REGION 'Gets&Sets'}
    function GetProtocol : string;
    function GetDocument : string;
    function GetConnectTimeout : integer;
    {$ENDREGION}

    /// <summary>
    ///   Протокол.
    /// </summary>
    property Protocol : string  read GetProtocol;

    /// <summary>
    ///   Документ (Url после хоста и порта).
    /// </summary>
    property Document : string  read GetDocument;

    /// <summary>
    ///   Таймаут подключения.
    /// </summary>
    property ConnectTimeout : integer  read GetConnectTimeout;
  end;
  {$ENDIF}

  /// <summary>
  ///   Блок параметров статической конфигурации.
  /// </summary>
  ICfgStatic =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Config.Intf.ICfgStatic;
  {$ELSE}
  interface(ICfg)
  ['{7E9112B8-5724-4F5F-96E4-619BB5E565AB}']
    {$REGION 'Gets&Sets'}
    function GetProgramData : string;
    procedure SetProgramData(const AValue : string);
    function GetMainLogName : string;
    procedure SetMainLogName(const AValue : string);
    {$ENDREGION}

    /// <summary>
    ///   Путь к папке, на которую у приложения есть права на запись.
    /// </summary>
    property ProgramData : string  read GetProgramData  write SetProgramData;

    /// <summary>
    ///   Имя основного лога.
    /// </summary>
    property MainLogName : string  read GetMainLogName  write SetMainLogName;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
implementation

end{$WARNINGS OFF}.
{$ENDIF}
