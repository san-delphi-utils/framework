{*******************************************************}
{                                                       }
{       Управление подсистемами                         }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Управление подсистемами
/// </summary>
unit San.SubSystems;
{$I San.SubSystems.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Classes, System.Generics.Collections, System.TypInfo,
  {$ENDREGION}

  {$REGION 'San'}
  San.Excps,

  San.SubSystems.Messages,
  San.SubSystems.Exceptions,
  San.SubSystems.Reporter,
  San.SubSystems.Config.Storage.Intf,
  San.SubSystems.Config.Storage.Ini,
  San.SubSystems.Config.Storage,
  San.SubSystems.Config.Intf,
  San.SubSystems.Config,
  San.SubSystems.Intf,
  San.SubSystems.Impl
  {$ENDREGION}
;
{$ENDREGION}

  {$DEFINE USE_ALIASES}
  {$I San.SubSystems.Inc.inc}
  {$UNDEF USE_ALIASES}
{$ELSE}
  {$I San.SubSystems.Inc.inc}
{$ENDIF}

{$IFNDEF USE_ALIASES}
type
  /// <summary>
  ///   Управление подсистемами
  /// </summary>
  Api = record
  public
    {$I San.SubSystems.Api.Intf.Public.Partial.inc}

  end;

implementation

{$REGION 'uses'}
uses
  {$REGION 'San.Intfs'}
  San.Intfs
  {$ENDREGION}
  ;
{$ENDREGION}

{$I San.SubSystems.Intf.GenImpl.inc}

{$REGION 'Api'}
class function Api.Control<T>: TSubSystemControl<T>;
begin
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
