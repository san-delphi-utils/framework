{*******************************************************}
{                                                       }
{       Базовая реализация подсистем.                   }
{       Реализация генерика.                            }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$REGION 'TCustomSubSystem<TIntf>'}
function TCustomSubSystem<TIntf, TCfg>.GetConfig: TCfg;
begin
  Result := TCfg(inherited Config);
end;

class function TCustomSubSystem<TIntf, TCfg>.SubSystemTypeInfo: PTypeInfo;
begin
  Result := PTypeInfo(TypeInfo(TIntf));
end;

class function TCustomSubSystem<TIntf, TCfg>.CfgClass : TSubSystemCfgClass;
begin
  San.Api.Objs.ValidateGen<TCfg,TSubSystemCfg>();
  Result := TSubSystemCfgClass(PTypeInfo(TypeInfo(TCfg)).TypeData^.ClassType);
end;
{$ENDREGION}

