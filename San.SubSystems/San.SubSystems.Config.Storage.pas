{*******************************************************}
{                                                       }
{       Tипы данных конфигурации.                       }
{       Хранилища                                       }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Tипы данных конфигурации.
///   Хранилища.
/// </summary>
unit San.SubSystems.Config.Storage;
{$I San.SubSystems.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Types, System.IniFiles,
  {$ENDREGION}

  {$REGION 'San'}
  San.SubSystems.Config.Storage.Intf,
  San.SubSystems.Config.Storage.Ini
  ;
  {$ENDREGION}
{$ENDREGION}

  {$DEFINE USE_ALIASES}
    {$I San.SubSystems.Config.Storage.Intf.pas}
  {$UNDEF USE_ALIASES}
{$ELSE}
  {$I San.SubSystems.Config.Storage.Intf.pas}
{$ENDIF}

{$IFNDEF USE_ALIASES}
type
  /// <summary>
  ///   Управление хранилищами конфигурации
  /// </summary>
  Api = record
  public const
    /// <summary>
    ///   Метаданные хранилищ
    /// </summary>
    Meta : San.SubSystems.Config.Storage.Intf.Api = ();

    /// <summary>
    ///   Хранилище в Ini-файле.
    /// </summary>
    Ini : San.SubSystems.Config.Storage.Ini.Api = ();
  end;

implementation

end{$WARNINGS OFF}.
{$ENDIF}
