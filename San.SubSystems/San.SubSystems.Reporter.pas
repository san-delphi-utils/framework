{*******************************************************}
{                                                       }
{       Построитель отчета                              }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Tипы данных: Построитель отчета
/// </summary>
unit San.SubSystems.Reporter;
{$I San.SubSystems.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Types
  {$ENDREGION}
;
{$ENDREGION}

type
  TCfgReporterDelegate = reference to procedure (const AData : array of string);
{$ELSE}
type
{$ENDIF}

  /// <summary>
  ///   Построитель отчета
  /// </summary>
  TReporter =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Reporter.TReporter;
  {$ELSE}
  record
  private
    FReporter : TCfgReporterDelegate;
  public
    procedure Rep(const AData : array of string); overload;
    procedure Rep(const AData : string); overload;
    procedure RepFmt(const AData : string; const AArgs : array of const);

    class function Create(ADelegate : TCfgReporterDelegate) : TReporter; static;
  end;
  {$ENDIF}


{$IFNDEF USE_ALIASES}
implementation

{$REGION 'TReporter'}
class function TReporter.Create(ADelegate: TCfgReporterDelegate): TReporter;
begin
  Result.FReporter := ADelegate;
end;

procedure TReporter.Rep(const AData: array of string);
begin
  FReporter(AData);
end;

procedure TReporter.Rep(const AData: string);
begin
  FReporter([AData]);
end;

procedure TReporter.RepFmt(const AData: string; const AArgs: array of const);
begin
  FReporter([ Format(AData, AArgs) ]);
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
