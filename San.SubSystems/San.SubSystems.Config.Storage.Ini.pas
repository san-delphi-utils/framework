{*******************************************************}
{                                                       }
{       Tипы данных конфигурации.                       }
{       Секционное хранилище в INI                      }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Tипы данных конфигурации.
///   Секционное хранилище в INI
/// </summary>
unit San.SubSystems.Config.Storage.Ini;
{$I San.SubSystems.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Types, System.IniFiles,
  {$ENDREGION}

  {$REGION 'San'}
  San.SubSystems.Reporter,
  San.SubSystems.Config.Storage.Intf
  {$ENDREGION}
  ;
{$ENDREGION}

{$ENDIF}

type
  /// <summary>
  ///   Ридер/Райтер элемента конфигурации с разделением на секции.
  /// </summary>
  TCfgSectionedStorageIni =
  {$IFDEF USE_ALIASES}
    San.SubSystems.Config.Storage.Ini.TCfgSectionedStorageIni;
  {$ELSE}
  class (TInterfacedObject, ICfgStorage, ICfgSectionedStorage)
  strict private
    FIniFile : TMemIniFile;
    FIsWriteActionsDetected : boolean;

  protected
    {$REGION 'ICfgStorage'}
    function GetLocation : string;
    function GetIsWriteActionsDetected : boolean;
    function Update(const ASuppressErrors : boolean = False) : boolean;
    {$ENDREGION}

    {$REGION 'ICfgSectionedStorage'}
    function ExistsSection(const ASection : string) : boolean;
    function ExistsValue(const ASection : string; const AIdent : string) : boolean;
    function Read(const ASection : string; const AIdent : string; ADefault : Variant) : Variant;
    function ReadSectionValues(const ASection : string) : TStringDynArray;
    procedure Write(const ASection : string; const AIdent : string; AValue : Variant);
    {$ENDREGION}

  public
    constructor Create(const AFileName : string);
    destructor Destroy; override;

  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
type
  /// <summary>
  ///   Tипы данных конфигурации. Секционное хранилище в INI
  /// </summary>
  Api = record
  private
    class var FMeta : ICfgMetaStorage;

  public
    /// <summary>
    ///   Выбрать, как хранилище по-умолчанию
    /// </summary>
    class procedure SelectAsDefault(); static;

    /// <summary>
    ///   Метаданные хранилища конфигураций
    /// </summary>
    class property Meta : ICfgMetaStorage  read FMeta;
  end;

implementation

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.Classes, System.IOUtils
  {$ENDREGION}
  ;
{$ENDREGION}

type
  /// <summary>
  ///   Метаданные хранилища конфигураций
  /// </summary>
  TCfgMetaStorageIni = class(TInterfacedObject, ICfgMetaStorage)
  protected
    {$REGION 'ICfgMetaStorage'}
    function MakeStorageInstance (const ALocation : string) : ICfgStorage;
    function IsWritableStorage (const ALocation : string) : boolean;
    procedure Report(const ALocation : string; const AReporter : TReporter;
      const ARaw : boolean = False);
    {$ENDREGION}
  end;

{$REGION 'TCfgMetaStorageIni'}
function TCfgMetaStorageIni.IsWritableStorage(const ALocation: string): boolean;
var
  FS : TFileStream;
begin
  Result := True;

  FS := nil;
  try

    try
      FS := TFile.OpenWrite(ALocation);
    except
      on EInOutError do
        Result := False;
    end;

  finally
    FreeAndNil(FS);
  end;
end;

function TCfgMetaStorageIni.MakeStorageInstance(
  const ALocation: string): ICfgStorage;
begin
  Result := TCfgSectionedStorageIni.Create(ALocation);
end;

procedure TCfgMetaStorageIni.Report(const ALocation: string;
  const AReporter: TReporter; const ARaw : boolean = False);

  procedure Raw();
  begin
    AReporter.Rep(TFile.ReadAllLines(ALocation));
  end;

  procedure NotRaw();
  var
    Ini : TMemIniFile;
    Sections, Values : TStrings;
    i : integer;
  begin
    Ini := TMemIniFile.Create(ALocation);
    try

      Sections := TStringList.Create();
      try
        Ini.ReadSections(Sections);

        if Sections.Count > 0 then
        begin
          Values := TStringList.Create();
          try

            for I := 0 to Sections.Count - 1 do
            begin
              AReporter.RepFmt('[%s]', [Sections[i]]);

              Ini.ReadSectionValues(Sections[i], Values);
              AReporter.Rep(Values.ToStringArray);
            end;

          finally
            FreeAndNil(Values);
          end;
        end;

      finally
        FreeAndNil(Sections);
      end;

    finally
      FreeAndNil(Ini);
    end;
  end;

begin
  if ARaw then
    Raw()
  else
    NotRaw();
end;
{$ENDREGION}

{$REGION 'TCfgSectionedStorageIni'}
constructor TCfgSectionedStorageIni.Create(const AFileName: string);
begin
  inherited Create();
  FIniFile := TMemIniFile.Create(AFileName);
end;

destructor TCfgSectionedStorageIni.Destroy;
begin
  FreeAndNil(FIniFile);
  inherited;
end;

function TCfgSectionedStorageIni.GetIsWriteActionsDetected: boolean;
begin
  Result := FIsWriteActionsDetected;
end;

function TCfgSectionedStorageIni.GetLocation: string;
begin
  Result := FIniFile.FileName;
end;

function TCfgSectionedStorageIni.Update(const ASuppressErrors : boolean = False) : boolean;
begin
  Result := True;

  if ASuppressErrors then
  try
    FIniFile.UpdateFile();
  except
    on EFileStreamError do
      Result := False;
  end

  else
    FIniFile.UpdateFile();
end;

function TCfgSectionedStorageIni.ExistsSection(const ASection: string): boolean;
begin
  Result := FIniFile.SectionExists(ASection);
end;

function TCfgSectionedStorageIni.ExistsValue(const ASection,
  AIdent: string): boolean;
begin
  Result := FIniFile.ValueExists(ASection, AIdent);
end;

function TCfgSectionedStorageIni.Read(const ASection, AIdent: string;
  ADefault: Variant): Variant;
begin
  if not FIniFile.ValueExists(ASection, AIdent) then
    Result := ADefault
  else
    Result := FIniFile.ReadString(ASection, AIdent, '');
end;

function TCfgSectionedStorageIni.ReadSectionValues(
  const ASection: string): TStringDynArray;
var
  SS : TStrings;
  i : integer;
begin
  SS := TStringList.Create();
  try
    FIniFile.ReadSectionValues(ASection, SS);

    SetLength(Result, SS.Count);
    for i := 0 to SS.Count - 1 do
      Result[i] := SS[i];

  finally
    FreeAndNil(SS);
  end;

end;

procedure TCfgSectionedStorageIni.Write(const ASection, AIdent: string;
  AValue: Variant);
begin
  FIniFile.WriteString(ASection, AIdent, AValue);
  FIsWriteActionsDetected := True;
end;
{$ENDREGION}

{$REGION 'Api'}
class procedure Api.SelectAsDefault();
begin
  San.SubSystems.Config.Storage.Intf.Api.DefaultMeta := FMeta;
end;
{$ENDREGION}

initialization
  Api.FMeta := TCfgMetaStorageIni.Create;
  Api.SelectAsDefault();

end{$WARNINGS OFF}.
{$ENDIF}
