{*******************************************************}
{                                                       }
{       Logger                                          }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2013-2016         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Логгер и его внутренние типы.
/// </summary>
unit San.Logger;
{$I San.inc}
{.$DEFINE WIDE_ONLY} //оставляет только методы и типы для widestring
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Classes, {$IFDEF WIDE_ONLY}System.WideStrings,{$ENDIF}
  {$ENDREGION}

  {$REGION 'San'}
  San.Excps, San.UpperString
  {$ENDREGION}
;
{$ENDREGION}

{$ENDIF}

const
  /// <summary>
  ///   Признак новой записи.
  /// </summary>
  LOG_REC_MARKER =
  {$IFDEF USE_ALIASES}
     San.Logger.LOG_REC_MARKER;
  {$ELSE}
     '#@$';
  {$ENDIF}

{$REGION 'LogRecordKind constants'}
const
  /// <summary>
  ///   Разновидность записи в логе: информация.
  /// </summary>
  LRK_INFO     =
  {$IFDEF USE_ALIASES}
    San.Logger.LRK_INFO;
  {$ELSE}
    0;
  {$ENDIF}

  /// <summary>
  ///   Разновидность записи в логе: предупреждение.
  /// </summary>
  LRK_WARNING  =
  {$IFDEF USE_ALIASES}
    San.Logger.LRK_WARNING;
  {$ELSE}
    1;
  {$ENDIF}

  /// <summary>
  ///   Разновидность записи в логе: ошибка.
  /// </summary>
  LRK_ERROR    =
  {$IFDEF USE_ALIASES}
    San.Logger.LRK_ERROR;
  {$ELSE}
    2;
  {$ENDIF}

  /// <summary>
  ///   Разновидность записи в логе: критическая ошибка.
  /// </summary>
  LRK_FATAL    =
  {$IFDEF USE_ALIASES}
    San.Logger.LRK_FATAL;
  {$ELSE}
    3;
  {$ENDIF}
{$ENDREGION}

{$REGION 'resourcestring'}
{$IFNDEF USE_ALIASES}
resourcestring
  sSANLoggerRecordClosedErr =
    'Запись закрыта. Действия над записью запрещены';

  sSANLoggerTargetReccordMissingErr =
    'Не задан экземпляр записи для передачи данных';

  sSANLoggerLogNotFoundFmtErr =
    'Лог "%s" не найден';

  sSANLoggerLogAlreadyExistsFmtErr =
    'Лог "%s" уже существует';

  sSANLoggerStorageNotFoundFmtErr =
    'Хранилище "%s" не найдено';
{$ENDIF}
{$ENDREGION}

type
  {$IFNDEF USE_ALIASES}
  {$REGION 'Forwards'}
  ISANLogStorage = interface;
  {$ENDREGION}
  {$ENDIF}

  /// <summary>
  ///   Базовое исключение для лога.
  /// </summary>
  ESANLogError =
  {$IFDEF USE_ALIASES}
     San.Logger.ESANLogError;
  {$ELSE}
     class(San.Excps.ESanBasic);
  {$ENDIF}

  {$REGION 'BaseTypes'}
  /// <summary>
  ///   Тип данных типа лога.
  /// </summary>
  TSANLogRecordKind =
  {$IFDEF USE_ALIASES}
     San.Logger.TSANLogRecordKind;
  {$ELSE}
     Longword;
  {$ENDIF}

  /// <summary>
  ///   Тип данных текстового подтипа лога.
  /// </summary>
  TSANLogRecordSubType =
  {$IFDEF USE_ALIASES}
     San.Logger.TSANLogRecordSubType;
  {$ELSE}
    {$IFDEF WIDE_ONLY}
      WideString;
    {$ELSE}
      string;
    {$ENDIF}
  {$ENDIF}

  /// <summary>
  ///   Тип данных текста лога.
  /// </summary>
  TSANLogRecordText =
  {$IFDEF USE_ALIASES}
     San.Logger.TSANLogRecordText;
  {$ELSE}
    {$IFDEF WIDE_ONLY}
      WideString;
    {$ELSE}
      string;
    {$ENDIF}
  {$ENDIF}

  /// <summary>
  ///   Тип данных имен.
  /// </summary>
  TSANLogName =
  {$IFDEF USE_ALIASES}
     San.Logger.TSANLogName;
  {$ELSE}
    {$IFDEF WIDE_ONLY}
      WideString;
    {$ELSE}
      string;
    {$ENDIF}
  {$ENDIF}

  /// <summary>
  ///   Тип данных расположения.
  /// </summary>
  TSANLocation =
  {$IFDEF USE_ALIASES}
     San.Logger.TSANLocation;
  {$ELSE}
    {$IFDEF WIDE_ONLY}
      WideString;
    {$ELSE}
      string;
    {$ENDIF}
  {$ENDIF}

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Тип абстрактного списка строк.
  /// </summary>
  TSANStrings =
    {$IFDEF WIDE_ONLY}
      TWideStrings;
    {$ELSE}
      TStrings;
    {$ENDIF}

  /// <summary>
  ///   Тип списка строк.
  /// </summary>
  TSANStringList =
    {$IFDEF WIDE_ONLY}
      TWideStringList;
    {$ELSE}
      TStringList;
    {$ENDIF}
  {$ENDIF}
  {$ENDREGION}

  {$REGION 'Record'}
  /// <summary>
  ///   Интерфейс вывода лога.
  /// </summary>
  ISANLogOutput =
  {$IFDEF USE_ALIASES}
     San.Logger.ISANLogOutput;
  {$ELSE}
    interface(IInterface)
  ['{03E130F6-AA97-4E08-8F0A-4AB03169495F}']
    {$REGION 'Gets&Sets'}
    function GetDateTimeFormat : TSANLogRecordText;
    {$ENDREGION}
    /// <summary>
    ///   Заголовок записи.
    /// </summary>
    /// <param name="AKind">
    ///   Целочисленный тип записи.
    /// </param>
    /// <param name="ASubType">
    ///   Строка подтипа записи.
    /// </param>
    /// <param name="ADateTime">
    ///   Дата и время записи. Если ADateTime = 0, берется Now.
    /// </param>
    procedure Header(const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      ADateTime : TDateTime = 0);

    /// <summary>
    ///   Добавить строку в буфер.
    /// </summary>
    /// <param name="AText">
    ///   Добавляемый текст.
    /// </param>
    procedure Write(const AText : TSANLogRecordText);

    /// <summary>
    ///   Добавить строку в буфер с добавлением #13#10.
    /// </summary>
    /// <param name="AText">
    ///   Добавляемый текст.
    /// </param>
    procedure Writeln(const AText : TSANLogRecordText);

    {$IFNDEF WIDE_ONLY}
    /// <summary>
    ///   Добавить форматированную строку в буфер.
    /// </summary>
    /// <param name="AText">
    ///   Добавляемый текст.
    /// </param>
    /// <param name="AArgs">
    ///   Аргументы формата.
    /// </param>
    procedure WriteFmt(const AText : TSANLogRecordText; const AArgs : array of const);

    /// <summary>
    ///   Добавить форматированную строку в буфер с добавлением #13#10.
    /// </summary>
    /// <param name="AText">
    ///   Добавляемый текст.
    /// </param>
    /// <param name="AArgs">
    ///   Аргументы формата.
    /// </param>
    procedure WritelnFmt(const AText : TSANLogRecordText; const AArgs : array of const);

    /// <summary>
    ///   Добавить строку в буфер.
    /// </summary>
    /// <param name="AIdent">
    ///   Идентификатор.
    /// </param>
    procedure WriteRes(const AIdent: NativeUInt); overload;

    /// <summary>
    ///   Добавить строку в буфер.
    /// </summary>
    /// <param name="AResStringRec">
    ///   Ссылка на ресурсную строку.
    /// </param>
    procedure WriteRes(const AResStringRec: PResStringRec); overload;

    /// <summary>
    ///   Добавить строку в буфер с добавлением #13#10.
    /// </summary>
    /// <param name="AIdent">
    ///   Идентификатор.
    /// </param>
    procedure WritelnRes(const AIdent: NativeUInt); overload;

    /// <summary>
    ///   Добавить строку в буфер с добавлением #13#10.
    /// </summary>
    /// <param name="AResStringRec">
    ///   Ссылка на ресурсную строку.
    /// </param>
    procedure WritelnRes(const AResStringRec: PResStringRec); overload;

    /// <summary>
    ///   Добавить строку в буфер.
    /// </summary>
    /// <param name="AIdent">
    ///   Идентификатор.
    /// </param>
    procedure WriteResFmt(const AIdent: NativeUInt; const AArgs : array of const); overload;

    /// <summary>
    ///   Добавить строку в буфер.
    /// </summary>
    /// <param name="AResStringRec">
    ///   Ссылка на ресурсную строку.
    /// </param>
    procedure WriteResFmt(const AResStringRec: PResStringRec; const AArgs : array of const); overload;

    /// <summary>
    ///   Добавить строку в буфер с добавлением #13#10.
    /// </summary>
    /// <param name="AIdent">
    ///   Идентификатор.
    /// </param>
    procedure WritelnResFmt(const AIdent: NativeUInt; const AArgs : array of const); overload;

    /// <summary>
    ///   Добавить строку в буфер с добавлением #13#10.
    /// </summary>
    /// <param name="AResStringRec">
    ///   Ссылка на ресурсную строку.
    /// </param>
    procedure WritelnResFmt(const AResStringRec: PResStringRec; const AArgs : array of const); overload;
    {$ENDIF}

    /// <summary>
    ///   Добавить отчеркивание.
    /// </summary>
    /// <param name="ALineItem">
    ///   Элементарная строка, из которых строится линия.
    /// </param>
    /// <param name="ALineItemsCount">
    ///   Количество элементарных строк в линии.
    /// </param>
    /// <param name="AUseLn">
    ///   Производится ли перевод строки после линии.
    /// </param>
    procedure Line(const ALineItem : TSANLogRecordText; const ALineItemsCount : Word = 255;
      const AUseLn : boolean = True);

    /// <summary>
    ///   Формат даты и времени.
    /// </summary>
    property DateTimeFormat : TSANLogRecordText  read GetDateTimeFormat;
  end;
  {$ENDIF}

  /// <summary>
  ///   Интерфейс одной записи лога.
  /// </summary>
  ISANLogRecord =
  {$IFDEF USE_ALIASES}
     San.Logger.ISANLogRecord;
  {$ELSE}
    interface(ISANLogOutput)
  ['{AA68A6F1-BD10-4A89-A107-719A4F992A7F}']
    {$REGION 'Gets&Sets'}
    function GetBuffer : TSANLogRecordText;
    procedure SetBuffer(const ABuffer : TSANLogRecordText);
    function GetAutoFlush : boolean;
    procedure SetAutoFlush(const AAutoFlush : boolean);
    function GetClosed : boolean;
    {$ENDREGION}

    /// <summary>
    ///   Вписать все накопленные данные в файл.
    /// </summary>
    procedure Flush;

    /// <summary>
    ///   Закончить работу с записью.
    /// </summary>
    procedure Close;

    /// <summary>
    ///   Накопленные данные.
    /// </summary>
    property Buffer : TSANLogRecordText  read GetBuffer  write SetBuffer;

    /// <summary>
    ///   Выполнять команду Flush автоматически при каждом изменении Buffer.
    /// </summary>
    property AutoFlush : boolean  read GetAutoFlush  write SetAutoFlush;

    /// <summary>
    ///   Признак закрытой записи.
    /// </summary>
    property Closed : boolean  read GetClosed;
  end;
  {$ENDIF}

  /// <summary>
  ///   Интерфейс с перенаправляемым вводом.
  /// </summary>
  ISANLogRecordTargetProvider =
  {$IFDEF USE_ALIASES}
     San.Logger.ISANLogRecordTargetProvider;
  {$ELSE}
    interface(IInterface)
  ['{906DD15D-7CE9-4256-A4DB-3419851E495A}']
    {$REGION 'Gets&Sets'}
    function GetTargetRec : ISANLogRecord;
    procedure SetTargetRec(const ATargetRec : ISANLogRecord);
    {$ENDREGION}

    /// <summary>
    ///   Запись, в которую перенаправляется ввод.
    /// </summary>
    property TargetRec : ISANLogRecord  read GetTargetRec  write SetTargetRec;
  end;
  {$ENDIF}
  {$ENDREGION}

  {$REGION 'Log'}
  /// <summary>
  ///   Полноценный лог.
  /// </summary>
  ISANLog =
  {$IFDEF USE_ALIASES}
     San.Logger.ISANLog;
  {$ELSE}
    interface(ISANLogOutput)
  ['{C3BF5BEE-84A2-4BD2-8720-36FB3F6E89A1}']
    {$REGION 'Gets&Sets'}
    function GetName : TSANLogName;
    function GetLocation : TSANLocation;
    procedure SetLocation(const ALocation : TSANLocation);
    function GetExpandedLocation : TSANLocation;
    function GetDateTimeFormat : TSANLogRecordText;
    procedure SetDateTimeFormat(const ADateTimeFormat : TSANLogRecordText);
    function GetStorage : ISANLogStorage;
    procedure SetStorage(const AStorage : ISANLogStorage);
    {$ENDREGION}

    /// <summary>
    ///   Создать запись.
    /// </summary>
    /// <param name="ATags">
    ///   Набор тегов для возможной фильтрации
    /// </param>
    /// <returns>
    ///   Открытая запись.
    /// </returns>
    function NewRecord(const ATags : TSANLocation = '') : ISANLogRecord;

    /// <summary>
    ///   Уникальное имя лога, однозначно определяющее его
    ///   в списке логов.
    /// </summary>
    property Name : TSANLogName  read GetName;

    /// <summary>
    ///   Расположение лога.
    /// </summary>
    property Location : TSANLocation  read GetLocation  write SetLocation;

    /// <summary>
    ///   Расположение лога с раскрытым форматом.
    /// </summary>
    property ExpandedLocation : TSANLocation  read GetExpandedLocation;

    /// <summary>
    ///   Формат даты и времени для каждой записи в логе. По умолчанию: yyyy.mm.dd hh:nn:ss.zzz
    /// </summary>
    property DateTimeFormat : TSANLogRecordText  read GetDateTimeFormat  write SetDateTimeFormat;

    /// <summary>
    ///   Ссылка на хранилище лога
    /// </summary>
    property Storage : ISANLogStorage  read GetStorage  write SetStorage;
  end;
  {$ENDIF}
  {$ENDREGION}

  {$REGION 'Table'}
  /// <summary>
  ///   Колонка таблицы лога.
  /// </summary>
  ISANLogTableColumn =
  {$IFDEF USE_ALIASES}
     San.Logger.ISANLogTableColumn;
  {$ELSE}
    interface(IInterface)
  ['{F65F05C7-AF2D-42E6-AA01-326C0B5392D6}']
    {$REGION 'Gets&Sets'}
    function GetWidth : Word;
    procedure SetWidth(const AWidth : Word);
    function GetAlignment : TAlignment;
    procedure SetAlignment(const AAlignment : TAlignment);
    function GetStrings : TSANStrings;
    {$ENDREGION}

    /// <summary>
    ///   Создать запись.
    /// </summary>
    /// <returns>
    ///   Открытая запись.
    /// </returns>
    function NewRecord : ISANLogRecord;

    /// <summary>
    ///   Ширина колонки
    /// </summary>
    property Width      : Word          read GetWidth      write SetWidth;

    /// <summary>
    ///   Выравнивание текста колонки
    /// </summary>
    property Alignment  : TAlignment    read GetAlignment  write SetAlignment;
    /// <summary>
    ///   Буфер позволяющий сначала назначить текст каждой колонки,
    ///   а затем вывести их все
    /// </summary>
    property Strings    : TSANStrings  read GetStrings;
  end;
  {$ENDIF}

  /// <summary>
  ///   Колонки таблицы лога.
  /// </summary>
  ISANLogTableColumns =
  {$IFDEF USE_ALIASES}
     San.Logger.ISANLogTableColumns;
  {$ELSE}
    interface(IInterface)
  ['{C9CEF962-F56C-47F6-9D37-1E18A32B09F9}']
    {$REGION 'Gets&Sets'}
    function GetCount : integer;
    function GetItems(const AIndex: Integer): ISANLogTableColumn;
    function GetMaxRowCount : integer;
    function GetTotalWidth : integer;
    {$ENDREGION}

    /// <summary>
    ///   Заблокировать доступ других потоков.
    /// </summary>
    procedure Lock;

    /// <summary>
    ///   Разблокировать доступ других потоков.
    /// </summary>
    procedure UnLock;

    /// <summary>
    ///   Добавить колонку.
    /// </summary>
    function Add : ISANLogTableColumn;

    /// <summary>
    ///   Первая колонка.
    /// </summary>
    function First: ISANLogTableColumn;

    /// <summary>
    ///   Последняя колонка.
    /// </summary>
    function Last: ISANLogTableColumn;

    /// <summary>
    ///   Задать кол-во и ширину колонок таблицы.
    /// </summary>
    /// <param name="AColWidths">
    ///   Массив ширин колонок.
    /// </param>
    procedure InitColsByWidth(const AColWidths : array of Word);

    /// <summary>
    ///   Очистить текст всех колонок.
    /// </summary>
    procedure AllStringsClear;

    /// <summary>
    ///   Количество колонок.
    /// </summary>
    property Count : integer  read GetCount;

    /// <summary>
    ///   Колонки по индексу.
    /// </summary>
    property Items[const AIndex: Integer]: ISANLogTableColumn read GetItems; default;

    /// <summary>
    ///   Максимальное кол-во строк в тексте среди всех колонок таблицы.
    /// </summary>
    property MaxRowCount : integer  read GetMaxRowCount;

    /// <summary>
    ///   Полная ширина одной строки таблицы с текущими настройками.
    /// </summary>
    property TotalWidth : integer  read GetTotalWidth;
  end;
  {$ENDIF}

  /// <summary>
  ///   Таблица лога.
  /// </summary>
  ISANLogTable =
  {$IFDEF USE_ALIASES}
     San.Logger.ISANLogTable;
  {$ELSE}
    interface(ISANLogRecordTargetProvider)
  ['{DCEC4894-8F66-489C-BBD4-4B9E6279BB5D}']
    {$REGION 'Gets&Sets'}
    function GetColumns : ISANLogTableColumns;
    function GetVLineChar : Char;
    procedure SetVLineChar(const AVLineChar : Char);
    function GetHLineChar : Char;
    procedure SetHLineChar(const AHLineChar : Char);
    function GetCrossChar : Char;
    procedure SetCrossChar(const ACrossChar : Char);
    {$ENDREGION}

    /// <summary>
    ///   Вывести горизонтальную линию таблицы.
    /// </summary>
    /// <param name="AShowColsSep">
    ///   True, показывать разделители колонок.
    /// </param>
    procedure WriteHrzLine(const AShowColsSep : boolean = True);

    /// <summary>
    ///   Вывод многострочного заголовка таблицы (без деления на колонки)
    ///   из массива строк.
    /// </summary>
    /// <param name="AAlignment">
    ///   Выравнивание заголовка.
    /// </param>
    /// <param name="AStrArr">
    ///   Массив строк заголовка. Каждая строка массива выводится с новой строки.
    /// </param>
    procedure WriteHeader(const AAlignment : TAlignment; const AStrArr : array of TSANLogRecordText); overload;

    /// <summary>
    ///   Вывод многострочного заголовка таблицы (без деления на колонки)
    ///   из списка строк
    /// </summary>
    /// <param name="AAlignment">
    ///   Выравнивание заголовка.
    /// </param>
    /// <param name="AStrings">
    ///   Список строк заголовка. Каждая строка списка выводится с новой строки.
    /// </param>
    procedure WriteHeader(const AAlignment : TAlignment; const AStrings : TSANStrings); overload;

    /// <summary>
    ///   Вывод многострочного заголовка таблицы (без деления на колонки)
    ///   из строки разделением по столбцам строки строки ASeparatedStr
    ///   подстрокой ASeparator
    /// </summary>
    /// <param name="AAlignment">
    ///   Выравнивание заголовка.
    /// </param>
    /// <param name="ASeparatedStr">
    ///   Строки заголовка в виде единой строки.
    /// </param>
    /// <param name="ASeparator">
    ///   Разделитель строк, обеспечивающий перенос при выводе.
    /// </param>
    procedure WriteHeader(const AAlignment : TAlignment; const ASeparatedStr : TSANLogRecordText;
      const ASeparator : TSANLogRecordText = sLineBreak); overload;

    /// <summary>
    ///   Вывод номеров колонок.
    /// </summary>
    /// <param name="AAlignment">
    ///   Выравнивание.
    /// </param>
    procedure WriteColsNumbers(const AAlignment : TAlignment = taLeftJustify);

    /// <summary>
    ///   Вывод строки таблицы из массива строк.
    /// </summary>
    procedure WriteRow(const AStrArr : array of TSANLogRecordText); overload;

    /// <summary>
    ///   Вывод строки таблицы из списка строк.
    /// </summary>
    /// <param name="AStrings">
    ///   Текст колонок по строкам.
    /// </param>
    procedure WriteRow(const AStrings : TStrings); overload;

    /// <summary>
    ///   Вывод строки таблицы, c разделением по столбцам строки ASeparatedStr
    ///   подстрокой ASeparator.
    /// </summary>
    /// <param name="ASeparatedStr">
    ///   Строки колонок в виде единой строки.
    /// </param>
    /// <param name="ASeparator">
    ///   Разделитель строк, обеспечивающий деление на колонки.
    /// </param>
    procedure WriteRow(const ASeparatedStr : TSANLogRecordText; const ASeparator : TSANLogRecordText = '|'); overload;

    /// <summary>
    ///   Вывод строки таблицы из списков строк колонок.
    /// </summary>
    procedure WriteColumnsText;

    /// <summary>
    ///   Список колонок таблицы
    /// </summary>
    property Columns : ISANLogTableColumns  read GetColumns;

    /// <summary>
    ///   Символ, вертикальной линии. По умолчанию "|"
    /// </summary>
    property VLineChar : Char  read GetVLineChar  write SetVLineChar;
    /// <summary>
    ///   Символ, горизонтальной линии. По умолчанию "-"
    /// </summary>
    property HLineChar : Char  read GetHLineChar  write SetHLineChar;
    /// <summary>
    ///   Символ, пересечения линиий. По умолчанию "+"
    /// </summary>
    property CrossChar : Char  read GetCrossChar  write SetCrossChar;
  end;
  {$ENDIF}
  {$ENDREGION}

  {$REGION 'Tree'}
  /// <summary>
  ///   Вывод в виде дерева.
  /// </summary>
  ISANLogTree =
  {$IFDEF USE_ALIASES}
     San.Logger.ISANLogTree;
  {$ELSE}
    interface(ISANLogRecordTargetProvider)
  ['{CF91216E-E8B2-4D94-8438-448ACAA8C326}']
    {$REGION 'Gets&Sets'}
    function GetLevelsCount : integer;
    function GetLevelsNext(const ALevel : integer) : boolean;
    function GetVLineChar : Char;
    procedure SetVLineChar(const AVLineChar : Char);
    function GetHLineChar : Char;
    procedure SetHLineChar(const AHLineChar : Char);
    function GetCrossChar : Char;
    procedure SetCrossChar(const ACrossChar : Char);
    function GetVIndent : byte;
    procedure SetVIndent(const AVIndent : byte);
    function GetHIndent : byte;
    procedure SetHIndent(const AHIndent : byte);
    function GetSubIndent : byte;
    procedure SetSubIndent(const ASubIndent : byte);
    function GetLeftIndent : byte;
    procedure SetLeftIndent(const ALeftIndent : byte);
    function GetVIndentIfEmpty : boolean;
    procedure SetVIndentIfEmpty(const AVIndentIfEmpty : boolean);
    {$ENDREGION}

    /// <summary>
    ///   Начать новый узел
    ///   Св-во LevelsNext[] показывает какие ветки продолжаются (при True)
    ///   Если ALevel в пределах [0, LevelsCount - 1] и LevelsNext[ALevel] = True,
    ///   то добавляется подузел уровня ALevel, иначе подузел уровня LevelsCount
    ///   AHasNextSibling - новое значение для LevelsNext[ALevel]
    /// </summary>
    function NewNode(const AHasNextSibling : boolean; const ALevel :integer = -1) : ISANLogRecord;

    /// <summary>
    ///   Завершить текущий узел, отрисовать линии старших узлов (если они есть)
    /// </summary>
    procedure EndNode;

    /// <summary>
    ///   Простой узел с текстом
    /// </summary>
    procedure SimpleNode(const ANodeText : TSANLogRecordText; const AHasNextSibling : boolean;
      const ALevel : Integer = -1);

    /// <summary>
    ///   Кол-во уровней открытых в данный момент
    /// </summary>
    property LevelsCount : integer  read GetLevelsCount;

    /// <summary>
    ///   True - для уровней на которых ожидается следующий,
    ///   False - для уровней на нет следующего узла
    /// </summary>
    property LevelsNext [const ALevel : integer] : boolean  read GetLevelsNext;

    /// <summary>
    /// Символ, вертикальной линии. По умолчанию "|"
    /// </summary>
    property VLineChar : Char  read GetVLineChar  write SetVLineChar;
    /// <summary>
    /// Символ, горизонтальной линии. По умолчанию "-"
    /// </summary>
    property HLineChar : Char  read GetHLineChar  write SetHLineChar;
    /// <summary>
    /// Символ, пересечения линиий. По умолчанию "+"
    /// </summary>
    property CrossChar : Char  read GetCrossChar  write SetCrossChar;

    /// <summary>
    /// Вертикальный отступ.
    /// </summary>
    property VIndent     : byte  read GetVIndent     write SetVIndent;
    /// <summary>
    /// Горизогтальный отступ.
    /// </summary>
    property HIndent     : byte  read GetHIndent     write SetHIndent;
    /// <summary>
    /// Отступ для подэлемента.
    /// </summary>
    property SubIndent   : byte  read GetSubIndent   write SetSubIndent;
    /// <summary>
    /// Отступ слева.
    /// </summary>
    property LeftIndent  : byte  read GetLeftIndent  write SetLeftIndent;

    /// <summary>
    /// Применять ли вертикальный отступ для пустого дерева.
    /// </summary>
    property VIndentIfEmpty : boolean  read GetVIndentIfEmpty  write SetVIndentIfEmpty;
  end;
  {$ENDIF}
  {$ENDREGION}

  {$REGION 'Storage'}
  /// <summary>
  ///   Хранилище логов (файл, консоль, что-то еще)
  /// </summary>
  ISANLogStorage =
  {$IFDEF USE_ALIASES}
     San.Logger.ISANLogStorage;
  {$ELSE}
  interface(IInterface)
  ['{0CB5D0EF-A6C2-4DC0-ABE2-13EE499F6773}']
    {$REGION 'Gets&Sets'}
    function GetName : TSANLogName;
    {$ENDREGION}

    /// <summary>
    ///   Запись
    /// </summary>
    procedure Write(var AContext : Pointer; const AText : TSANLogRecordText; const AWeight : integer);

    /// <summary>
    ///   Имя хранилища
    /// </summary>
    property Name : TSANLogName  read GetName;
  end;
  {$ENDIF}

  /// <summary>
  ///   Хранилище логов, открываемое
  /// </summary>
  ISANLogStorageOpenable =
  {$IFDEF USE_ALIASES}
     San.Logger.ISANLogStorageOpenable;
  {$ELSE}
  interface(ISANLogStorage)
  ['{A8976A3F-D1A4-48CC-A6F2-1C86C8CB5E74}']
    /// <summary>
    ///   Открытие хранилища для записи
    /// </summary>
    function Open(const ALog : ISANLog; const AWeight : integer; out AContext : Pointer) : boolean;

    /// <summary>
    ///   Закрытие хранилища для записи
    /// </summary>
    procedure Close(const AWeight : integer; var AContext : Pointer);
  end;
  {$ENDIF}
  {$ENDREGION}

  {$REGION 'Controller'}
  /// <summary>
  ///   Тег контроллера логов
  /// </summary>
  /// <remarks>
  ///    Управление выводом
  /// </remarks>
  TSANLogControllerTag =
  {$IFDEF USE_ALIASES}
     San.Logger.TSANLogControllerTag;
  {$ELSE}
  record
  private
    FName : TSANLogName;
    FWeight : integer;

  public
    class function Create(const AName : TSANLogName; const AWeight : integer = 0) : TSANLogControllerTag; static;

    /// <summary>
    ///   Имя тега
    /// </summary>
    property Name : TSANLogName  read FName;

    /// <summary>
    ///   Вес тега
    /// </summary>
    /// <remarks>
    ///   <para>
    ///    <c>Weight&lt;0</c>, вывод скрывается
    ///   </para>
    ///   <para>
    ///     иначе, вывод производится
    ///   </para>
    /// </remarks>
    property Weight : integer  read FWeight;
  end;
  {$ENDIF}
  {$ENDREGION}

{$IFNDEF USE_ALIASES}
type

  /// <summary>
  ///   Логгер.
  /// </summary>
  Api = record
  public type
    /// <summary>
    ///   Хранилища
    /// </summary>
    TStorages = record
    strict private
      class function InitIfNeeded : IInterfaceList; static;
      class function GetCount: integer; static;
      class function GetStorages(const AIndex: integer): ISANLogStorage; static;
      class function GetStorageByName(const AName : TSANLogName): ISANLogStorage; static;

    public
      /// <summary>
      ///   Попытка поиск лога по имени.
      /// </summary>
      /// <param name="AStorageName">
      ///   Имя хранилища.
      /// </param>
      /// <param name="AStorage">
      ///   Найденное хранилище, если поиск успешен.
      /// </param>
      /// <param name="AIndexPtr">
      ///   В случае успеха, если не nil - в значение записиывается индекс элемента.
      /// </param>
      /// <returns>
      ///   True, поиск успешен.
      /// </returns>
      class function TryFindByName(const AStorageName : TSANLogName; out AStorage : ISANLogStorage;
        const AIndexPtr : PInteger = nil) : boolean; static;

      /// <summary>
      ///   Проверка существования лога по имени.
      /// </summary>
      /// <param name="AName">
      ///   Имя лога.
      /// </param>
      /// <returns>
      ///   True, лог cуществует.
      /// </returns>
      class function ExistsByName(const AStorageName : TSANLogName) : boolean; static;

      /// <summary>
      ///   Добавить хранилище
      /// </summary>
      class function Add(AStorage : ISANLogStorage) : ISANLogStorage; static;

      /// <summary>
      ///   Удалить хранилище
      /// </summary>
      class function Remove(AStorage : ISANLogStorage) : boolean; overload; static;

      /// <summary>
      ///   Удалить хранилище
      /// </summary>
      class function Remove(const AStorageName : TSANLogName) : boolean; overload; static;

      /// <summary>
      ///   Количество
      /// </summary>
      class property Count : integer  read GetCount;

      /// <summary>
      ///   Хранилище по индексу
      /// </summary>
      class property Items[const AIndex : integer] : ISANLogStorage  read GetStorages; default;

      /// <summary>
      ///   Хранилище  по имени
      /// </summary>
      class property Items[const AName : TSANLogName] : ISANLogStorage  read GetStorageByName; default;
    end;

  strict private
    class function InitIfNeeded : IInterfaceList; static;
    class function GetLogsCount : integer; static;
    class function GetLogs(const AIndex : integer) : ISANLog; static;
    class function GetLogByName(const AName : TSANLogName) : ISANLog; static;

  public
    /// <summary>
    ///   Заблокировать доступ других потоков.
    /// </summary>
    class procedure Lock; static;

    /// <summary>
    ///   Разблокировать доступ других потоков.
    /// </summary>
    class procedure UnLock; static;

    /// <summary>
    ///   Попытка поиск лога по имени.
    /// </summary>
    /// <param name="AName">
    ///   Имя лога.
    /// </param>
    /// <param name="ALog">
    ///   Найденый лог, если поиск успешен.
    /// </param>
    /// <param name="AIndexPtr">
    ///   В случае успеха, если не nil - в значение записиывается индекс элемента.
    /// </param>
    /// <returns>
    ///   True, поиск успешен.
    /// </returns>
    class function TryFindByName(const AName : TSANLogName; out ALog : ISANLog;
      const AIndexPtr : PInteger = nil) : boolean; static;

    /// <summary>
    ///   Проверка существования лога по имени.
    /// </summary>
    /// <param name="AName">
    ///   Имя лога.
    /// </param>
    /// <returns>
    ///   True, лог cуществует.
    /// </returns>
    class function ExistsByName(const AName : TSANLogName) : boolean; static;

    /// <summary>
    ///   Добавить лог.
    /// </summary>
    /// <param name="AName">
    ///   Имя лога.
    /// </param>
    /// <param name="ALocation">
    ///   Расположение лога.
    /// </param>
    /// <param name="AStorage">
    ///   Хранилище
    /// </param>
    /// <returns>
    ///   Экземпляр нового лога.
    /// </returns>
    class function Add(const AName : TSANLogName; const ALocation : TSANLocation; AStorage : ISANLogStorage = nil) : ISANLog; static;

    /// <summary>
    ///   Добавить лог если его нет или взять существующий.
    /// </summary>
    /// <param name="AName">
    ///   Имя лога.
    /// </param>
    /// <param name="ALocation">
    ///   Расположение лога.
    /// </param>
    /// <param name="AStorage">
    ///   Хранилище
    /// </param>
    /// <returns>
    ///   Экземпляр нового лога.
    /// </returns>
    class function AddOrGet(const AName : TSANLogName; const ALocation : TSANLocation; AStorage : ISANLogStorage = nil) : ISANLog; static;

    /// <summary>
    ///   Удалить информацию о логе по имени.
    /// </summary>
    /// <param name="AName">
    ///   Имя лога.
    /// </param>
    /// <returns>
    ///   True, лог удален.
    /// </returns>
    class function Remove(const AName : TSANLogName) : boolean; overload; static;

    /// <summary>
    ///   Удалить информацию о логе по интерфейсу лога.
    /// </summary>
    /// <param name="ALog">
    ///   Имя лога.
    /// </param>
    /// <returns>
    ///   True, лог удален.
    /// </returns>
    class function Remove(const ALog : ISANLog) : boolean; overload; static;

    /// <summary>
    ///   Создать экземпляр табличного вывода.
    /// </summary>
    class function MakeTable : ISANLogTable; static;

    /// <summary>
    ///   Создать экземпляр древовидного вывода.
    /// </summary>
    class function MakeTree : ISANLogTree; static;

    /// <summary>
    ///    Строковое представление типа записи лога
    /// </summary>
    class function LogRecordKindToStr(const AKind : TSANLogRecordKind) : string; static;

    /// <summary>
    ///    Получить из строки тип записи лога
    /// </summary>
    class function StrToLogRecordKind(const AStr : string) : TSANLogRecordKind; static;

    /// <summary>
    ///   Раcсчитать вес тегов
    /// </summary>
    class function CalcTagsWeight(const  ATags : TSANLocation) : integer; static;

    /// <summary>
    ///   Сохранить веса тегов
    /// </summary>
    class procedure TagsWeightsSave(const AFileName : TSANLocation); static;

    /// <summary>
    ///   Загрузить веса тегов
    /// </summary>
    class procedure TagsWeightsLoad(const AFileName : TSANLocation); static;

    /// <summary>
    ///   Количество логов в наборе.
    /// </summary>
    class property LogsCount : integer  read GetLogsCount;

    /// <summary>
    ///   Лог по индексу.
    /// </summary>
    class property Logs[const AIndex : integer] : ISANLog  read GetLogs; default;

    /// <summary>
    ///   Лог по имени.
    /// </summary>
    class property Logs[const AName : TSANLogName] : ISANLog  read GetLogByName; default;

    /// <summary>
    ///   Хранилища
    /// </summary>
    const Storages : TStorages = ();
  end;

implementation

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.StrUtils, System.Math, System.DateUtils,
  System.SyncObjs, System.Generics.Collections
  {$ENDREGION}
;
{$ENDREGION}

{$IFDEF NEXTGEN}
function LoadStr(Ident: NativeUInt): string;
begin
  raise ENotSupportedException.Create('LoadStr');
end;
{$ENDIF !NEXTGEN}


{$REGION 'BaseClasses'}
type
  /// <summary>
  ///   Интерфейс расширенной записи лога.
  /// </summary>
  TSANLogOutput = class abstract(TInterfacedObject, ISANLogOutput)
  protected
    {$REGION 'ISANLogOutput'}
    function GetDateTimeFormat : TSANLogRecordText;
    procedure Header(const AKind : TSANLogRecordKind; const ASubType : TSANLogRecordSubType;
      ADateTime : TDateTime = 0);
    procedure Write(const AText : TSANLogRecordText);
    procedure Writeln(const AText : TSANLogRecordText);
    procedure Line(const ALineItem : TSANLogRecordText; const ALineItemsCount : Word = 255;
      const AUseLn : boolean = True);

    {$IFNDEF WIDE_ONLY}
    procedure WriteFmt(const AText : TSANLogRecordText; const AArgs : array of const);
    procedure WritelnFmt(const AText : TSANLogRecordText; const AArgs : array of const);
    procedure WriteRes(const AIdent: NativeUInt); overload;
    procedure WriteRes(const AResStringRec: PResStringRec); overload;
    procedure WritelnRes(const AIdent: NativeUInt); overload;
    procedure WritelnRes(const AResStringRec: PResStringRec); overload;
    procedure WriteResFmt(const AIdent: NativeUInt; const AArgs : array of const); overload;
    procedure WriteResFmt(const AResStringRec: PResStringRec; const AArgs : array of const); overload;
    procedure WritelnResFmt(const AIdent: NativeUInt; const AArgs : array of const); overload;
    procedure WritelnResFmt(const AResStringRec: PResStringRec; const AArgs : array of const); overload;
    {$ENDIF}

    {$ENDREGION}

    /// <summary>
    ///   Считать формат даты и времени заголовка.
    /// </summary>
    function DoGetDateTimeFormat : string; virtual; abstract;

    /// <summary>
    ///   Непосредственная запись.
    /// </summary>
    procedure DoWrite(const AText : string); virtual; abstract;

  public
    property DateTimeFormat : TSANLogRecordText  read GetDateTimeFormat;
  end;

  /// <summary>
  ///   Абстрактная запись лога.
  /// </summary>
  TSANLogCustomRecord = class abstract(TSANLogOutput, ISANLogRecord)
  private
    FAutoFlush : boolean;
    FBufferCS : TCriticalSection;
    FClosed : boolean;

  protected
    {$REGION 'ISANLogRecord'}
    function GetBuffer : TSANLogRecordText;
    procedure SetBuffer(const ABuffer : TSANLogRecordText);
    function GetAutoFlush : boolean;
    procedure SetAutoFlush(const AAutoFlush : boolean);
    function GetClosed : boolean;
    procedure Flush;
    procedure Close;
    {$ENDREGION}

    procedure DoWrite(const AText : string); override;
    procedure ValidateClosed;
    procedure DoFlush; virtual; abstract;
    procedure DoClose; virtual;
    function DoGetBuffer : TSANLogRecordText; virtual; abstract;
    procedure DoSetBuffer(const ABuffer : TSANLogRecordText); virtual; abstract;

  public
    constructor Create;
    destructor Destroy; override;

    property Buffer : TSANLogRecordText  read GetBuffer  write SetBuffer;
    property AutoFlush : boolean  read GetAutoFlush  write SetAutoFlush;
  end;

{$REGION 'TSANLogOutput'}
function TSANLogOutput.GetDateTimeFormat: TSANLogRecordText;
begin
  Result := DoGetDateTimeFormat;
end;

procedure TSANLogOutput.Header(const AKind: TSANLogRecordKind;
  const ASubType: TSANLogRecordSubType; ADateTime: TDateTime);
begin

  if SameValue(ADateTime, 0, OneMillisecond) then
    ADateTime := Now();

  DoWrite
  (
    Format
    (
      '%s %s %s %s',
      [
        LOG_REC_MARKER,
        FormatDateTime(DateTimeFormat, ADateTime),
        Api.LogRecordKindToStr(AKind),
        ASubType
      ]
    ) + sLineBreak
  );

end;

procedure TSANLogOutput.Line(const ALineItem: TSANLogRecordText;
  const ALineItemsCount: Word; const AUseLn: boolean);
begin
  DoWrite(DupeString(ALineItem, ALineItemsCount) + IfThen(AUseLn, sLineBreak));
end;

procedure TSANLogOutput.Write(const AText: TSANLogRecordText);
begin
  DoWrite(AText);
end;

procedure TSANLogOutput.Writeln(const AText: TSANLogRecordText);
begin
  DoWrite(AText + sLineBreak);
end;

{$IFNDEF WIDE_ONLY}
procedure TSANLogOutput.WriteFmt(const AText : TSANLogRecordText; const AArgs : array of const);
begin
  DoWrite( Format(AText, AArgs) );
end;

procedure TSANLogOutput.WritelnFmt(const AText : TSANLogRecordText; const AArgs : array of const);
begin
  DoWrite(Format(AText, AArgs) + sLineBreak);
end;

procedure TSANLogOutput.WriteRes(const AIdent: NativeUInt);
begin
  DoWrite(LoadStr(AIdent));
end;

procedure TSANLogOutput.WriteRes(const AResStringRec: PResStringRec);
begin
  DoWrite(LoadResString(AResStringRec));
end;

procedure TSANLogOutput.WritelnRes(const AIdent: NativeUInt);
begin
  DoWrite(LoadStr(AIdent) + sLineBreak);
end;

procedure TSANLogOutput.WritelnRes(const AResStringRec: PResStringRec);
begin
  DoWrite(LoadResString(AResStringRec) + sLineBreak);
end;

procedure TSANLogOutput.WriteResFmt(const AIdent: NativeUInt; const AArgs : array of const);
begin
  DoWrite(Format(LoadStr(AIdent), AArgs));
end;

procedure TSANLogOutput.WriteResFmt(const AResStringRec: PResStringRec; const AArgs : array of const);
begin
  DoWrite(Format(LoadResString(AResStringRec), AArgs));
end;

procedure TSANLogOutput.WritelnResFmt(const AIdent: NativeUInt; const AArgs : array of const);
begin
  DoWrite(Format(LoadStr(AIdent), AArgs) + sLineBreak);
end;

procedure TSANLogOutput.WritelnResFmt(const AResStringRec: PResStringRec; const AArgs : array of const);
begin
  DoWrite(Format(LoadResString(AResStringRec), AArgs) + sLineBreak);
end;
{$ENDIF}

{$ENDREGION}

{$REGION 'TSANLogCustomRecord'}
procedure TSANLogCustomRecord.Close;
begin
  ValidateClosed;

  DoClose;

  FClosed := True;
end;

constructor TSANLogCustomRecord.Create;
begin
  inherited;

  FBufferCS := TCriticalSection.Create;
end;

destructor TSANLogCustomRecord.Destroy;
begin

  if not FClosed then
    Close;

  FreeANdNil(FBufferCS);

  inherited;
end;

procedure TSANLogCustomRecord.DoClose;
begin
end;

procedure TSANLogCustomRecord.DoWrite(const AText: string);
begin
  Buffer := Buffer + AText;
end;

procedure TSANLogCustomRecord.Flush;
begin
  ValidateClosed;

  FBufferCS.Enter;
  try

    DoFlush;

  finally
    FBufferCS.Leave;
  end;
end;

function TSANLogCustomRecord.GetAutoFlush: boolean;
begin
  Result := FAutoFlush;
end;

function TSANLogCustomRecord.GetBuffer: TSANLogRecordText;
begin
  FBufferCS.Enter;
  try

    Result := DoGetBuffer;

  finally
    FBufferCS.Leave;
  end;
end;

function TSANLogCustomRecord.GetClosed: boolean;
begin
  Result := FClosed;
end;

procedure TSANLogCustomRecord.SetAutoFlush(const AAutoFlush: boolean);
begin
  FAutoFlush := AAutoFlush;
end;

procedure TSANLogCustomRecord.SetBuffer(const ABuffer: TSANLogRecordText);
begin
  ValidateClosed;

  FBufferCS.Enter;
  try

    DoSetBuffer(ABuffer);

    if FAutoFlush then
    begin
      DoFlush;
      DoSetBuffer('');
    end;

  finally
    FBufferCS.Leave;
  end;

end;

procedure TSANLogCustomRecord.ValidateClosed;
begin
  if FClosed then
    raise ESANLogError.CreateRes(@sSANLoggerRecordClosedErr);
end;
{$ENDREGION}
{$ENDREGION}



{$REGION 'Table'}
type
  TSANLogTableColumn  = class;
  TSANLogTable        = class;

  /// <summary>
  ///   Одна запись колонки таблицы лога.
  /// </summary>
  TSANLogTableColumnRecord = class(TSANLogCustomRecord)
  private
    FColumn : TSANLogTableColumn;

  protected
    function DoGetBuffer : TSANLogRecordText; override;
    procedure DoSetBuffer(const ABuffer : TSANLogRecordText); override;
    procedure DoFlush; override;
    procedure DoClose; override;
    function DoGetDateTimeFormat : string; override;

  public
    constructor Create(const AColumn : TSANLogTableColumn);
  end;

  /// <summary>
  ///   Колонка таблицы лога.
  /// </summary>
  TSANLogTableColumn = class(TInterfacedObject, ISANLogTableColumn)
  private
    FTable       : TSANLogTable;
    FOpenRecord  : TSANLogTableColumnRecord;
    FWidth       : Word;
    FAlignment   : TAlignment;
    FStrings     : TSANStrings;

  protected
    {$REGION 'ISANLogTableColumn'}
    function GetWidth : Word;
    procedure SetWidth(const AWidth : Word);
    function GetAlignment : TAlignment;
    procedure SetAlignment(const AAlignment : TAlignment);
    function GetStrings : TSANStrings;
    function NewRecord : ISANLogRecord;
    {$ENDREGION}

  public
    constructor Create(const ATable : TSANLogTable; const AWidth : Word = 0;
      const AAlignment : TAlignment = Low(TAlignment));
    destructor Destroy; override;

    /// <summary>
    ///   Ширина колонки
    /// </summary>
    property Width      : Word          read GetWidth      write SetWidth;

    /// <summary>
    ///   Выравнивание текста колонки
    /// </summary>
    property Alignment  : TAlignment    read GetAlignment  write SetAlignment;
    /// <summary>
    ///   Буфер позволяющий сначала назначить текст каждой колонки,
    ///   а затем вывести их все
    /// </summary>
    property Strings    : TSANStrings  read GetStrings;
  end;

  /// <summary>
  ///   Колонки таблицы лога.
  /// </summary>
  TSANLogTableColumns = class(TInterfaceList, ISANLogTableColumns)
  private
    FTable : TSANLogTable;

  protected
    {$REGION 'ISANLogTableColumns'}
    function GetItems(const AIndex: Integer): ISANLogTableColumn;
    function GetMaxRowCount : integer;
    function GetTotalWidth : integer;
    function Add : ISANLogTableColumn;
    function First: ISANLogTableColumn;
    function Last: ISANLogTableColumn;
    procedure InitColsByWidth(const AColWidths : array of Word);
    procedure AllStringsClear;
    {$ENDREGION}

  public
    constructor Create(const ATable : TSANLogTable);
    property Items[const AIndex: Integer]: ISANLogTableColumn read GetItems; default;
    property MaxRowCount : integer  read GetMaxRowCount;
  end;

  /// <summary>
  ///   Таблица лога.
  /// </summary>
  TSANLogTable = class(TInterfacedObject, ISANLogRecordTargetProvider, ISANLogTable)
  private
    FColumns : ISANLogTableColumns;
    FVLineChar, FHLineChar, FCrossChar : Char;
    FTargetRec : ISANLogRecord;

  protected
    {$REGION 'ISANLogRecordTargetProvider'}
    function GetTargetRec : ISANLogRecord;
    procedure SetTargetRec(const ATargetRec : ISANLogRecord);
    {$ENDREGION}

    {$REGION 'ISANLogTable'}
    function GetColumns : ISANLogTableColumns;
    function GetVLineChar : Char;
    procedure SetVLineChar(const AVLineChar : Char);
    function GetHLineChar : Char;
    procedure SetHLineChar(const AHLineChar : Char);
    function GetCrossChar : Char;
    procedure SetCrossChar(const ACrossChar : Char);
    procedure WriteHrzLine(const AShowColsSep : boolean = True);
    procedure WriteHeader(const AAlignment : TAlignment; const AStrArr : array of TSANLogRecordText); overload;
    procedure WriteHeader(const AAlignment : TAlignment; const AStrings : TSANStrings); overload;
    procedure WriteHeader(const AAlignment : TAlignment; const ASeparatedStr : TSANLogRecordText;
      const ASeparator : TSANLogRecordText = sLineBreak); overload;
    procedure WriteColsNumbers(const AAlignment : TAlignment = taLeftJustify);
    procedure WriteRow(const AStrArr : array of TSANLogRecordText); overload;
    procedure WriteRow(const AStrings : TSANStrings); overload;
    procedure WriteRow(const ASeparatedStr : TSANLogRecordText; const ASeparator : TSANLogRecordText = '|'); overload;
    procedure WriteColumnsText;
    {$ENDREGION}

    /// <summary>
    ///   Форматировать текст AFullCellText в соответствии с параметрами столбца с индексом AColIndex,
    ///   игнорировать выравнивание колонок.
    /// </summary>
    function CellText(const AFullCellText : string; const AColIndex : integer; const AAlignment : TAlignment) : string; overload;

    /// <summary>
    ///   Форматировать текст AFullCellText в соответствии с параметрами столбца с индексом AColIndex.
    /// </summary>
    function CellText(const AFullCellText : string; const AColIndex : integer) : string; overload;

    /// <summary>
    ///   Проверить целевую запись.
    /// </summary>
    procedure ValidateTargetRec;

    /// <summary>
    ///   Передача текста в целевую запись.
    /// </summary>
    procedure WriteToTargetRec(const AText : string);

    /// <summary>
    ///   Передача одной строки заголовка в целевую запись.
    /// </summary>
    procedure WriteHeaderRowToTargetRec(const AAlignment : TAlignment;
      const AHeaderRow : string; const AWidth : integer);

  public
    constructor Create;

    property TargetRec : ISANLogRecord  read GetTargetRec  write SetTargetRec;

    property Columns : ISANLogTableColumns  read GetColumns;

    property VLineChar : Char  read GetVLineChar  write SetVLineChar;
    property HLineChar : Char  read GetHLineChar  write SetHLineChar;
    property CrossChar : Char  read GetCrossChar  write SetCrossChar;
  end;

{$REGION 'TSANLogTableColumnRecord'}
constructor TSANLogTableColumnRecord.Create(const AColumn: TSANLogTableColumn);
begin
  inherited Create;

  FColumn := AColumn;
end;

procedure TSANLogTableColumnRecord.DoClose;
begin
  FColumn.FOpenRecord := nil;
end;

procedure TSANLogTableColumnRecord.DoFlush;
begin
  FColumn.FTable.WriteColumnsText;
end;

function TSANLogTableColumnRecord.DoGetBuffer: TSANLogRecordText;
begin
  Result := FColumn.FStrings.Text;
end;

function TSANLogTableColumnRecord.DoGetDateTimeFormat: string;
begin
  FColumn.FTable.ValidateTargetRec;
  Result := FColumn.FTable.TargetRec.DateTimeFormat;
end;

procedure TSANLogTableColumnRecord.DoSetBuffer(
  const ABuffer: TSANLogRecordText);
begin
  FColumn.Strings.Text := ABuffer;
end;
{$ENDREGION}

{$REGION 'TSANLogTableColumn'}
constructor TSANLogTableColumn.Create(const ATable : TSANLogTable;
  const AWidth : Word; const AAlignment : TAlignment);
begin
  inherited Create;

  FTable      := ATable;
  FWidth      := AWidth;
  FAlignment  := AAlignment;

  FStrings    := TSANStringList.Create;
end;

destructor TSANLogTableColumn.Destroy;
begin
  FreeAndNil(FStrings);

  inherited;
end;

function TSANLogTableColumn.GetAlignment: TAlignment;
begin
  Result := FAlignment;
end;

function TSANLogTableColumn.GetStrings: TSANStrings;
begin
  Result := FStrings;
end;

function TSANLogTableColumn.GetWidth: Word;
begin
  Result := FWidth;
end;

function TSANLogTableColumn.NewRecord: ISANLogRecord;
begin
  if Assigned(FOpenRecord) then
    FOpenRecord.Close;

  FOpenRecord := TSANLogTableColumnRecord.Create(Self);
  Result := FOpenRecord;
end;

procedure TSANLogTableColumn.SetAlignment(const AAlignment: TAlignment);
begin
  FAlignment := AAlignment;
end;

procedure TSANLogTableColumn.SetWidth(const AWidth: Word);
begin
  FWidth := AWidth;
end;
{$ENDREGION}

{$REGION 'TSANLogTableColumns'}
function TSANLogTableColumns.Add: ISANLogTableColumn;
begin
  Result := TSANLogTableColumn.Create(FTable);
  inherited Add(Result);
end;

procedure TSANLogTableColumns.AllStringsClear;
var
  i : integer;
begin

  Lock;
  try

    for i := 0 to Count - 1 do
      Items[i].Strings.Clear;

  finally
    Unlock;
  end;

end;

constructor TSANLogTableColumns.Create(const ATable: TSANLogTable);
begin
  inherited Create;

  FTable := ATable;
end;

function TSANLogTableColumns.First: ISANLogTableColumn;
begin
  Result := ISANLogTableColumn(inherited First);
end;

function TSANLogTableColumns.GetItems(const AIndex: Integer): ISANLogTableColumn;
begin
  Result := ISANLogTableColumn(inherited Items[AIndex]);
end;

function TSANLogTableColumns.GetMaxRowCount: integer;
var
  i : integer;
begin

  Lock;
  try

    Result := 0;

    for i := 0 to Count - 1 do
      Result := MAX(Result, Items[i].Strings.Count);

  finally
    Unlock;
  end;

end;

function TSANLogTableColumns.GetTotalWidth: integer;
var
  i : integer;
begin

  Lock;
  try

    Result := 0;

    for i := 0 to Count - 1 do
      Inc(Result, Items[i].Width);

    Inc(Result, Count*3 + 1);

  finally
    Unlock;
  end;

end;

procedure TSANLogTableColumns.InitColsByWidth(const AColWidths: array of Word);
var
  i : integer;
begin
  Lock;
  try

    for i := Low(AColWidths) to High(AColWidths) do
      if i < Count then
        Items[i].Width := AColWidths[i]
      else
        Add.Width := AColWidths[i];

    for i := Count - 1 downto High(AColWidths) + 1 do
      Delete(i);

  finally
    Unlock;
  end;

end;

function TSANLogTableColumns.Last: ISANLogTableColumn;
begin
  Result := ISANLogTableColumn(inherited Last);
end;
{$ENDREGION}

{$REGION 'TSANLogTable'}
function TSANLogTable.CellText(const AFullCellText: string;
  const AColIndex: integer; const AAlignment: TAlignment): string;
var
  Column : ISANLogTableColumn;
  DLgth : integer;
begin
  Column := Columns[AColIndex];
  DLgth := Column.Width - Length(AFullCellText);

  if DLgth <= 0 then
    Result := Copy(AFullCellText, 1, Column.Width)

  else
    case AAlignment of
      taLeftJustify:
        Result := AFullCellText + DupeString(' ', DLgth);

      taRightJustify:
        Result := DupeString(' ', DLgth) + AFullCellText;

      taCenter:
        Result := DupeString(' ', DLgth div 2) +
                  AFullCellText +
                  DupeString(' ', DLgth div 2) +
                  IfThen(Odd(DLgth), ' ');
    end;

  Result := ' ' + Result + ' ' + VLineChar;
end;

function TSANLogTable.CellText(const AFullCellText: string;
  const AColIndex: integer): string;
begin
  Result := CellText(AFullCellText, AColIndex, Columns[AColIndex].Alignment);
end;

constructor TSANLogTable.Create;
begin
  inherited Create;

  FColumns := TSANLogTableColumns.Create(Self);

  FVLineChar := '|';
  FHLineChar := '-';
  FCrossChar := '+';
end;

function TSANLogTable.GetColumns: ISANLogTableColumns;
begin
  Result := FColumns;
end;

function TSANLogTable.GetCrossChar: Char;
begin
  Result := FCrossChar;
end;

function TSANLogTable.GetHLineChar: Char;
begin
  Result := FHLineChar;
end;

function TSANLogTable.GetTargetRec: ISANLogRecord;
begin
  Result := FTargetRec;
end;

function TSANLogTable.GetVLineChar: Char;
begin
  Result := FVLineChar;
end;

procedure TSANLogTable.SetCrossChar(const ACrossChar: Char);
begin
  FCrossChar := ACrossChar;
end;

procedure TSANLogTable.SetHLineChar(const AHLineChar: Char);
begin
  FHLineChar := AHLineChar;
end;

procedure TSANLogTable.SetTargetRec(const ATargetRec: ISANLogRecord);
begin
  FTargetRec := ATargetRec;
end;

procedure TSANLogTable.SetVLineChar(const AVLineChar: Char);
begin
  FVLineChar := AVLineChar;
end;

procedure TSANLogTable.ValidateTargetRec;
begin
  if not Assigned(FTargetRec) then
    raise ESANLogError.CreateRes(@sSANLoggerTargetReccordMissingErr);
end;

procedure TSANLogTable.WriteHeaderRowToTargetRec(const AAlignment : TAlignment;
  const AHeaderRow: string; const AWidth: integer);
var
  dWidth : integer;
  S : string;
begin
  dWidth := AWidth - Length(AHeaderRow);

  case AAlignment of
    taLeftJustify:
      S := AHeaderRow + DupeString(' ', dWidth);

    taRightJustify:
      S := DupeString(' ', dWidth) + AHeaderRow;

    taCenter:
      S := DupeString(' ', dWidth div 2) + AHeaderRow + DupeString(' ', dWidth div 2 + ORD(Odd(dWidth)));

  end;

  WriteToTargetRec(VLineChar + ' ' + S + ' ' + VLineChar);
end;

procedure TSANLogTable.WriteToTargetRec(const AText: string);
begin
  ValidateTargetRec;

  FTargetRec.Writeln(AText);
end;

procedure TSANLogTable.WriteColsNumbers(const AAlignment: TAlignment);
var
  i : integer;
  Row : string;
begin
  Row := VLineChar;

  for i := 0 to Columns.Count - 1 do
    Row := Row + CellText(IntToStr(i + 1), i, AAlignment);

  WriteToTargetRec(Row);
end;

procedure TSANLogTable.WriteColumnsText;
var
  i, RowIndex, RowCount : integer;
  Row : string;
begin
  Columns.Lock;
  try

    RowCount := Columns.MaxRowCount;

    for RowIndex := 0 to RowCount - 1 do
    begin
      Row := VLineChar;

      for i := 0 to Columns.Count - 1 do
        with Columns[i].Strings do
          if RowIndex < Count then
            Row := Row + CellText(Strings[RowIndex], i)
          else
            Row := Row + CellText('', i);

      WriteToTargetRec(Row);
    end;

    Columns.AllStringsClear;

  finally
    Columns.UnLock;
  end;
end;

procedure TSANLogTable.WriteHeader(const AAlignment: TAlignment;
  const AStrings: TSANStrings);
var
  i, Width : integer;
begin

  if AStrings.Count = 0 then
    Exit;

  Width := Columns.TotalWidth - 4;

  for i := 0 to AStrings.Count - 1 do
    WriteHeaderRowToTargetRec(AAlignment, AStrings[i], Width);
end;

procedure TSANLogTable.WriteHeader(const AAlignment: TAlignment;
  const AStrArr: array of TSANLogRecordText);
var
  i, Width : integer;
begin

  if Length(AStrArr) = 0 then
    Exit;

  Width := Columns.TotalWidth - 4;

  for i := Low(AStrArr) to High(AStrArr) do
    WriteHeaderRowToTargetRec(AAlignment, AStrArr[i], Width);
end;

procedure TSANLogTable.WriteHeader(const AAlignment: TAlignment;
  const ASeparatedStr, ASeparator: TSANLogRecordText);
var
  Width, Pos, Offset, L : integer;
begin
  Width := Columns.TotalWidth - 4;
  L := Length(ASeparator);

  Offset  := 1;

  repeat
    Pos := PosEx(ASeparator, ASeparatedStr, Offset);

    if Pos = 0 then
      Break;

    WriteHeaderRowToTargetRec(AAlignment, Copy(ASeparatedStr, Offset, Pos - Offset - L + 1), Width);

    Offset := Pos + L;
  until False;

  WriteHeaderRowToTargetRec(AAlignment, Copy(ASeparatedStr, Offset, Length(ASeparatedStr) - Offset + 1), Width);
end;

procedure TSANLogTable.WriteHrzLine(const AShowColsSep: boolean);
var
  S : string;
//------------------------------------------------------------------------------
  procedure AddColumnChars(const AColIndex : integer; const AEndChar : Char);
  begin
    S := S +
         DupeString(HLineChar, Columns[AColIndex].Width + 2) +
         AEndChar;
  end;
//------------------------------------------------------------------------------
var
  i : integer;
  ColsSep : Char;
begin
  S := CrossChar;

  if AShowColsSep then
    ColsSep := CrossChar
  else
    ColsSep := HLineChar;

  Columns.Lock;
  try

    for i := 0 to Columns.Count - 2 do
      AddColumnChars(i, ColsSep);

    AddColumnChars(Columns.Count - 1, CrossChar);

  finally
    Columns.UnLock;
  end;

  WriteToTargetRec(S);
end;

procedure TSANLogTable.WriteRow(const AStrings: TSANStrings);
var
  i : integer;
  Row : string;
begin
  Row := VLineChar;

  for i := 0 to MIN(AStrings.Count - 1, Columns.Count - 1) do
    Row := Row + CellText(AStrings[i], i);

  for i := AStrings.Count to Columns.Count - 1 do
    Row := Row + CellText('', i);

  WriteToTargetRec(Row);
end;

procedure TSANLogTable.WriteRow(const ASeparatedStr, ASeparator: TSANLogRecordText);
var
  Pos, Offset, L, Cols, i : integer;
  Row : string;
begin
  Offset  := 1;
  Cols    := 0;
  L       := Length(ASeparator);
  Row     := VLineChar;

  repeat
    Pos := PosEx(ASeparator, ASeparatedStr, Offset);

    if Pos = 0 then
      Break;

    Row := Row + CellText(Copy(ASeparatedStr, Offset, Pos - Offset - L + 1), Cols);

    Offset := Pos + L;
    Inc(Cols);
  until Cols >= Columns.Count;

  if Cols < Columns.Count then
    Row := Row + CellText(Copy(ASeparatedStr, Offset, Length(ASeparatedStr) - Offset + 1), Cols);

  for I := Cols + 1 to Columns.Count - 1 do
    Row := Row + CellText('', i);

  WriteToTargetRec(Row);
end;

procedure TSANLogTable.WriteRow(const AStrArr: array of TSANLogRecordText);
var
  i : integer;
  Row : string;
begin
  Row := VLineChar;

  for i := 0 to MIN(High(AStrArr), Columns.Count - 1) do
    Row := Row + CellText(AStrArr[i], i);

  for i := Length(AStrArr) to Columns.Count - 1 do
    Row := Row + CellText('', i);

  WriteToTargetRec(Row);
end;
{$ENDREGION}
//------------------------------------------------------------------------------
{$ENDREGION}



{$REGION 'Tree'}
type
  TSANLogTree = class;

  /// <summary>
  ///   Одна запись дерева лога.
  /// </summary>
  TSANLogTreeRecord = class(TSANLogCustomRecord, ISANLogRecord)
  private
    FTree : TSANLogTree;
    FBuffer : TSANLogRecordText;

  protected
    {$REGION 'ISANLogRecord'}
    procedure SetAutoFlush(const AAutoFlush : boolean);
    {$ENDREGION}

    function DoGetBuffer : TSANLogRecordText; override;
    procedure DoSetBuffer(const ABuffer : TSANLogRecordText); override;
    procedure DoFlush; override;
    procedure DoClose; override;
    function DoGetDateTimeFormat : string; override;

  public
    constructor Create(const ATree : TSANLogTree);
  end;

  /// <summary>
  ///   Таблица лога.
  /// </summary>
  TSANLogTree = class(TInterfacedObject, ISANLogRecordTargetProvider, ISANLogTree)
  private
    FTargetRec : ISANLogRecord;
    FOpenRecord  : TSANLogTreeRecord;

    FLevels : TList<Boolean>;

    FVLineChar, FHLineChar, FCrossChar : Char;
    FVIndent, FHIndent, FSubIndent, FLeftIndent : byte;

    FFirstLine, FNotAddIndentStr, FVIndentIfEmpty : boolean;

  strict private
    /// <summary>
    ///   Внутрення передача текста в целевую запись.
    /// </summary>
    procedure InnerWriteToTargetRec(const AText: string; const AIndex, ACount : Integer); //

  protected
    {$REGION 'ISANLogRecordTargetProvider'}
    function GetTargetRec : ISANLogRecord;
    procedure SetTargetRec(const ATargetRec : ISANLogRecord);
    {$ENDREGION}

    {$REGION 'ISANLogTree'}
    function GetLevelsCount : integer;
    function GetLevelsNext(const ALevel : integer) : boolean;
    function GetVLineChar : Char;
    procedure SetVLineChar(const AVLineChar : Char);
    function GetHLineChar : Char;
    procedure SetHLineChar(const AHLineChar : Char);
    function GetCrossChar : Char;
    procedure SetCrossChar(const ACrossChar : Char);
    function GetVIndent : byte;
    procedure SetVIndent(const AVIndent : byte);
    function GetHIndent : byte;
    procedure SetHIndent(const AHIndent : byte);
    function GetSubIndent : byte;
    procedure SetSubIndent(const ASubIndent : byte);
    function GetLeftIndent : byte;
    procedure SetLeftIndent(const ALeftIndent : byte);
    function GetVIndentIfEmpty : boolean;
    procedure SetVIndentIfEmpty(const AVIndentIfEmpty : boolean);
    function NewNode(const AHasNextSibling : boolean; const ALevel :integer = -1) : ISANLogRecord;
    procedure EndNode;
    procedure SimpleNode(const ANodeText : TSANLogRecordText; const AHasNextSibling : boolean;
      const ALevel : Integer = -1);
    {$ENDREGION}

    /// <summary>
    ///   Проверить целевую запись.
    /// </summary>
    procedure ValidateTargetRec;

    /// <summary>
    ///   Передача текста в целевую запись.
    /// </summary>
    procedure WriteToTargetRec(const AText : string);

    /// <summary>
    ///   Cформировать строку отступа.
    /// </summary>
    function IndentStr(const AForceVrtLine : boolean = False) : string;

  public
    constructor Create;
    destructor Destroy; override;

    property TargetRec : ISANLogRecord  read GetTargetRec  write SetTargetRec;

    property LevelsCount : integer  read GetLevelsCount;
    property LevelsNext [const ALevel : integer] : boolean  read GetLevelsNext;

    property VLineChar : Char  read GetVLineChar  write SetVLineChar;
    property HLineChar : Char  read GetHLineChar  write SetHLineChar;
    property CrossChar : Char  read GetCrossChar  write SetCrossChar;

    property VIndent     : byte  read GetVIndent     write SetVIndent;
    property HIndent     : byte  read GetHIndent     write SetHIndent;
    property SubIndent   : byte  read GetSubIndent   write SetSubIndent;
    property LeftIndent  : byte  read GetLeftIndent  write SetLeftIndent;
    property VIndentIfEmpty : boolean  read GetVIndentIfEmpty  write SetVIndentIfEmpty;
  end;

{$REGION 'TSANLogTreeRecord'}
constructor TSANLogTreeRecord.Create(const ATree: TSANLogTree);
begin
  inherited Create();

  FAutoFlush := True;
  FTree := ATree;
end;

procedure TSANLogTreeRecord.SetAutoFlush(const AAutoFlush: boolean);
begin
end;

procedure TSANLogTreeRecord.DoClose;
begin
  FTree.FOpenRecord := nil;
end;

procedure TSANLogTreeRecord.DoFlush;
begin
  FTree.WriteToTargetRec(FBuffer);
end;

function TSANLogTreeRecord.DoGetBuffer: TSANLogRecordText;
begin
  Result := FBuffer;
end;

function TSANLogTreeRecord.DoGetDateTimeFormat: string;
begin
  FTree.ValidateTargetRec;
  Result := FTree.TargetRec.DateTimeFormat;
end;

procedure TSANLogTreeRecord.DoSetBuffer(const ABuffer: TSANLogRecordText);
begin
  FBuffer := ABuffer;
end;
{$ENDREGION}

{$REGION 'TSANLogTree'}
constructor TSANLogTree.Create;
begin
  inherited Create;

  FLevels := TList<Boolean>.Create;

  FVLineChar := '|';
  FHLineChar := '-';
  FCrossChar := '+';

  FHIndent     := 2;
  FVIndent     := 1;
  FSubIndent   := 2;
  FLeftIndent  := 2;
end;

destructor TSANLogTree.Destroy;
begin
  FreeAndNil(FLevels);

  inherited;
end;

function TSANLogTree.GetCrossChar: Char;
begin
  Result := FCrossChar;
end;

function TSANLogTree.GetHIndent: byte;
begin
  Result := FHIndent;
end;

function TSANLogTree.GetHLineChar: Char;
begin
  Result := FHLineChar;
end;

function TSANLogTree.GetLeftIndent: byte;
begin
  Result := FLeftIndent;
end;

function TSANLogTree.GetLevelsCount: integer;
begin
  Result := FLevels.Count;
end;

function TSANLogTree.GetLevelsNext(const ALevel: integer): boolean;
begin
  Result := FLevels[ALevel];
end;

function TSANLogTree.GetSubIndent: byte;
begin
  Result := FSubIndent;
end;

function TSANLogTree.GetTargetRec: ISANLogRecord;
begin
  Result := FTargetRec;
end;

function TSANLogTree.GetVIndent: byte;
begin
  Result := FVIndent;
end;

function TSANLogTree.GetVIndentIfEmpty: boolean;
begin
  Result := FVIndentIfEmpty;
end;

function TSANLogTree.GetVLineChar: Char;
begin
  Result := FVLineChar;
end;

procedure TSANLogTree.SetCrossChar(const ACrossChar: Char);
begin
  FCrossChar := ACrossChar;
end;

procedure TSANLogTree.SetHIndent(const AHIndent: byte);
begin
  FHIndent := AHIndent;
end;

procedure TSANLogTree.SetHLineChar(const AHLineChar: Char);
begin
  FHLineChar := AHLineChar;
end;

procedure TSANLogTree.SetLeftIndent(const ALeftIndent: byte);
begin
  FLeftIndent := ALeftIndent;
end;

procedure TSANLogTree.SetSubIndent(const ASubIndent: byte);
begin
  FSubIndent := ASubIndent;
end;

procedure TSANLogTree.SetTargetRec(const ATargetRec: ISANLogRecord);
begin
  FTargetRec := ATargetRec;
end;

procedure TSANLogTree.SetVIndent(const AVIndent: byte);
begin
  FVIndent := AVIndent;
end;

procedure TSANLogTree.SetVIndentIfEmpty(const AVIndentIfEmpty: boolean);
begin
  FVIndentIfEmpty := AVIndentIfEmpty;
end;

procedure TSANLogTree.SetVLineChar(const AVLineChar: Char);
begin
  FVLineChar := AVLineChar;
end;

function TSANLogTree.IndentStr(const AForceVrtLine: boolean): string;
var
  i : integer;
begin

  if LevelsCount > 0 then
    Result := DupeString(' ', FLeftIndent)
  else
    Result := '';

  for i := 0 to LevelsCount - 2 do
    Result := Result
            + IfThen(LevelsNext[i], VLineChar, ' ')
            + DupeString(' ', FHIndent + FSubIndent);

  Result := Result
          + IfThen(FFirstLine, CrossChar, IfThen(FLevels.Last or AForceVrtLine, VLineChar, ' ') )
          + DupeString( IfThen(FFirstLine, HLineChar, ' '), FHIndent);
end;

function TSANLogTree.NewNode(const AHasNextSibling : boolean;
  const ALevel :integer = -1) : ISANLogRecord;
var
  RowIndex : byte;
  EmptyTree : boolean;
begin
  EmptyTree := (LevelsCount = 0);

  if (ALevel < 0) or (ALevel > LevelsCount - 1) or (not LevelsNext[ALevel]) then
    FLevels.Add(AHasNextSibling)

  else
  begin
    FLevels[ALevel] := AHasNextSibling;
    FLevels.Count := MIN(FLevels.Count, ALevel + 1)
  end;


  if not Assigned(FOpenRecord) then
    FOpenRecord := TSANLogTreeRecord.Create(Self);

  Result := FOpenRecord;


  if (not EmptyTree) or (FVIndentIfEmpty) then
  begin
    ValidateTargetRec;

    for RowIndex := 1 to VIndent do
    begin
      FNotAddIndentStr := True;
      FOpenRecord.Writeln(IndentStr(True));
    end;
  end;

  FFirstLine := True;
end;

procedure TSANLogTree.EndNode;
begin

  with FLevels do
    while (Count > 0) and not FLevels.Last do
      Count := Count - 1;

  if Assigned(FOpenRecord) and (FLevels.Count = 0) then
    FOpenRecord.Close;
end;

procedure TSANLogTree.SimpleNode(const ANodeText: TSANLogRecordText;
  const AHasNextSibling: boolean; const ALevel: Integer);
begin
  NewNode(AHasNextSibling, ALevel);
  try
    FOpenRecord.Writeln(ANodeText);
  finally
    EndNode;
  end;
end;

procedure TSANLogTree.ValidateTargetRec;
begin
  if not Assigned(FTargetRec) then
    raise ESANLogError.CreateRes(@sSANLoggerTargetReccordMissingErr);
end;

procedure TSANLogTree.InnerWriteToTargetRec(const AText: string; const AIndex,
  ACount: Integer);
begin
  if FNotAddIndentStr then
    FTargetRec.Write(Copy(AText, AIndex, ACount))

  else
  begin
    FTargetRec.Write(IndentStr + Copy(AText, AIndex, ACount));
    FNotAddIndentStr := True;
  end;

  FFirstLine := False;
end;

procedure TSANLogTree.WriteToTargetRec(const AText: string);
var
  Pos, Offset : integer;
begin
  ValidateTargetRec;

  Offset := 1;
  repeat
    Pos := PosEx(sLineBreak, AText, Offset);

    if Pos = 0 then
      Break;

    InnerWriteToTargetRec(AText, Offset, Pos - Offset + 2);
    FNotAddIndentStr := False;

    Offset := Pos + 2;
  until FALSE;

  if Length(AText) > Offset + 1 then
    InnerWriteToTargetRec(AText, Offset, Length(AText) - Offset + 1);

  FFirstLine := False;
end;
{$ENDREGION}

{$ENDREGION}



{$REGION 'Log'}
type
  TSANLog = class;

  /// <summary>
  ///   Одна запись лога.
  /// </summary>
  TSANLogRecord = class(TSANLogCustomRecord)
  private
    FBuffer : TSANLogRecordText;
    FStorageContext : Pointer;
    FLog : TSANLog;
    FStorage : ISANLogStorage;
    FWeight : integer;
    FOpenSuccessfully : boolean;

  protected
    function DoGetBuffer : TSANLogRecordText; override;
    procedure DoSetBuffer(const ABuffer : TSANLogRecordText); override;
    procedure DoFlush; override;
    procedure DoClose; override;
    function DoGetDateTimeFormat : string; override;

  public
    constructor Create(const ALog : TSANLog; const ATags : TSANLocation);
  end;

  /// <summary>
  ///   Лог.
  /// </summary>
  TSANLog = class(TSANLogOutput, ISANLog)
  private
    FWriteRecCS : TCriticalSection;
    FName : TSANLogName;
    FDateTimeFormat : TSANLogRecordText;
    FLocation : TSANLocation;
    FOpenRecord : TSANLogRecord;
    FStorage : ISANLogStorage;

  protected
    {$REGION 'ISANLog'}
    function GetName : TSANLogName;
    function GetLocation : TSANLocation;
    procedure SetLocation(const ALocation : TSANLocation);
    function GetExpandedLocation : TSANLocation;
    function GetDateTimeFormat : TSANLogRecordText;
    procedure SetDateTimeFormat(const ADateTimeFormat : TSANLogRecordText);
    function GetStorage : ISANLogStorage;
    procedure SetStorage(const AStorage : ISANLogStorage);
    function NewRecord(const ATags : TSANLocation = '') : ISANLogRecord;
    {$ENDREGION}

    /// <summary>
    ///   Развернуть Location c учетом форматированных дат внутри '|'.
    /// </summary>
    function ExpandLocation : string;

    function DoGetDateTimeFormat : string; override;
    procedure DoWrite(const AText : string); override;

  public
    constructor Create(const AName : TSANLogName; AStorage : ISANLogStorage);
    destructor Destroy; override;

    property Name : TSANLogName  read GetName;
    property Location : TSANLocation  read GetLocation  write SetLocation;

  end;

{$REGION 'TSANLogRecord'}
constructor TSANLogRecord.Create(const ALog : TSANLog; const ATags : TSANLocation);
var
  SO : ISANLogStorageOpenable;
begin
  FLog := ALog;

  inherited Create;

  FAutoFlush := True;

  FStorage := FLog.GetStorage();

  FWeight := Api.CalcTagsWeight(ATags);

  FOpenSuccessfully := not Supports(FStorage, ISANLogStorageOpenable, SO)
                    or SO.Open(ALog, FWeight, FStorageContext);
end;

procedure TSANLogRecord.DoClose;
var
  SO : ISANLogStorageOpenable;
begin

  if FOpenSuccessfully and Supports(FStorage, ISANLogStorageOpenable, SO) then
    SO.Close(FWeight, FStorageContext);

  FLog.FOpenRecord := nil;

  FLog.FWriteRecCS.Leave;
end;

procedure TSANLogRecord.DoFlush;
begin
  FStorage.Write(FStorageContext, FBuffer, FWeight);
end;

function TSANLogRecord.DoGetBuffer: TSANLogRecordText;
begin
  Result := FBuffer;
end;

function TSANLogRecord.DoGetDateTimeFormat: string;
begin
  Result := FLog.FDateTimeFormat;
end;

procedure TSANLogRecord.DoSetBuffer(const ABuffer: TSANLogRecordText);
begin
  FBuffer := ABuffer;
end;
{$ENDREGION}

{$REGION 'TSANLog'}
constructor TSANLog.Create(const AName : TSANLogName; AStorage : ISANLogStorage);
begin
  inherited Create;

  FDateTimeFormat := 'yyyy.mm.dd hh:nn:ss.zzz';

  FName := AName;

  FWriteRecCS := TCriticalSection.Create;

  FStorage := AStorage;
end;

destructor TSANLog.Destroy;
begin
  FreeAndNil(FWriteRecCS);

  inherited;
end;

function TSANLog.DoGetDateTimeFormat: string;
begin
  Result := FDateTimeFormat;
end;

procedure TSANLog.DoWrite(const AText: string);
begin
  NewRecord.Buffer := AText;
end;

function TSANLog.GetName: TSANLogName;
begin
  Result := FName;
end;

function TSANLog.GetStorage: ISANLogStorage;
begin
  Result := FStorage;
end;

function TSANLog.NewRecord(const ATags : TSANLocation): ISANLogRecord;
begin
  FWriteRecCS.Enter;

  if Assigned(FOpenRecord) then
    FOpenRecord.Close;

  FOpenRecord := TSANLogRecord.Create(Self, ATags);
  Result := FOpenRecord;
end;

function TSANLog.GetDateTimeFormat: TSANLogRecordText;
begin
  Result := FDateTimeFormat;
end;

procedure TSANLog.SetDateTimeFormat(const ADateTimeFormat: TSANLogRecordText);
begin
  FDateTimeFormat := ADateTimeFormat;
end;

function TSANLog.GetLocation: TSANLocation;
begin
  Result := FLocation;
end;

procedure TSANLog.SetLocation(const ALocation: TSANLocation);
begin
  FLocation := ALocation;
end;

procedure TSANLog.SetStorage(const AStorage: ISANLogStorage);
begin
  FStorage := AStorage;
end;

function TSANLog.GetExpandedLocation: TSANLocation;
begin
  Result := ExpandLocation;
end;

function TSANLog.ExpandLocation: string;
var
  Now : TDateTime;
  S, DS : string;
  UseFmt : boolean;
  Pos, Offset : integer;
begin
  Result  := '';
  S       := FLocation;
  UseFmt  := False;
  Offset  := 1;
  Now     := System.SysUtils.Now();

  repeat
    Pos := PosEx('|', S, Offset);

    if Pos = 0 then
      Break;

    DS := Copy(S, Offset, Pos - Offset);

    if UseFmt then
      Result := Result + FormatDateTime(DS, Now)
    else
      Result := Result + DS;

    UseFmt := not UseFmt;
    Offset := Pos + 1;
  until False;

  Result := Result + Copy(S, Offset, Length(S) - Offset + 1);
end;
{$ENDREGION}
{$ENDREGION}



{$REGION 'Storages'}
type
  /// <summary>
  ///   Хранилище в текстовом файле
  /// </summary>
  TSANLogStorageTextFile = class(TInterfacedObject, ISANLogStorage, ISANLogStorageOpenable)
  public type
    PText = ^System.TextFile;

  protected
    {$REGION 'ISANLogStorage'}
    function GetName : TSANLogName;
    function GetLimWeight : integer;
    procedure Write(var AContext : Pointer; const AText : TSANLogRecordText; const AWeight : integer);
    {$ENDREGION}

    {$REGION 'ISANLogStorageOpenable'}
    function Open(const ALog : ISANLog; const AWeight : integer; out AContext : Pointer) : boolean;
    procedure Close(const AWeight : integer; var AContext : Pointer);
    {$ENDREGION}
  end;

  /// <summary>
  ///   Хранилище в консоли
  /// </summary>
  TSANLogStorageConsole = class(TInterfacedObject, ISANLogStorage)
  protected
    {$REGION 'ISANLogStorage'}
    function GetName : TSANLogName;
    function GetLimWeight : integer;
    procedure Write(var AContext : Pointer; const AText : TSANLogRecordText; const AWeight : integer);
    {$ENDREGION}
  end;

{$REGION 'TSANLogStorageTextFile'}
function TSANLogStorageTextFile.GetName: TSANLogName;
begin
  Result := 'TextFile';
end;

function TSANLogStorageTextFile.GetLimWeight: integer;
begin
  Result := 0;
end;

function TSANLogStorageTextFile.Open(const ALog: ISANLog;
  const AWeight : integer; out AContext: Pointer): boolean;
var
  PF : PText;
begin
  if AWeight < 0 then
    Exit(False);

  New(PF);
  try
    AssignFile(PF^, ALog.ExpandedLocation);

    {$I-}
    Append(PF^);

    if IOResult <> 0 then
    {$I+}
    try
      Rewrite(PF^);
    except
      on E : Exception do
        Exception.RaiseOuterException( ESANLogError.CreateFmt('Error for location: %s', [ALog.Location]) );
    end;

    Result := True;
    AContext := PF;

  except
    Dispose(PF);
    raise;
  end;
end;

procedure TSANLogStorageTextFile.Close(const AWeight : integer; var AContext: Pointer);
begin
  if AWeight < 0 then
    Exit;

  CloseFile(PText(AContext)^);
  Dispose(AContext);
end;

procedure TSANLogStorageTextFile.Write(var AContext: Pointer;
  const AText: TSANLogRecordText; const AWeight : integer);
begin
  if AWeight >= 0 then
    System.Write(PText(AContext)^, AText);
end;
{$ENDREGION}

{$REGION 'TSANLogStorageConsole'}
function TSANLogStorageConsole.GetName: TSANLogName;
begin
  Result := 'Console';
end;

function TSANLogStorageConsole.GetLimWeight: integer;
begin
  Result := 0;
end;

procedure TSANLogStorageConsole.Write(var AContext: Pointer;
  const AText: TSANLogRecordText; const AWeight : integer);
begin
  if AWeight >= 0 then
    System.Write(AText);
end;
{$ENDREGION}

{$ENDREGION}

{$REGION 'TSANLogControllerTag'}
class function TSANLogControllerTag.Create(
  const AName: TSANLogName; const AWeight : integer): TSANLogControllerTag;
begin
  Result.FName    := AName;
  Result.FWeight  := AWeight;
end;
{$ENDREGION}

const
  LRK_STRINGS : array [0..3] of string =
  (
    'INFO',
    'WARNING',
    'ERROR',
    'FATAL'
  );
var
  Api_Logs, Api_Storages : IInterfaceList;
  Api_Tags : TDictionary<TUpperString,TSANLogControllerTag>;

{$REGION 'Api'}
class function Api.LogRecordKindToStr(const AKind : TSANLogRecordKind) : string;
begin
  case AKind of
    LRK_INFO, LRK_WARNING, LRK_ERROR, LRK_FATAL: Result := LRK_STRINGS[AKind];

  else
    Result := Format('KIND_%d', [AKind]);
  end;
end;

class function Api.StrToLogRecordKind(const AStr : string) : TSANLogRecordKind;
var
  Idx : integer;
begin
  Idx := AnsiIndexText(AStr, LRK_STRINGS);
  if Idx >= 0 then
    Result := Idx
  else
    Result := 4; // <- UNKNOWN
end;

class function Api.InitIfNeeded : IInterfaceList;
begin
  if not Assigned(Api_Logs) then
    Api_Logs := TInterfaceList.Create();

  Result := Api_Logs;
end;

class function Api.Add(const AName: TSANLogName;
  const ALocation: TSANLocation; AStorage : ISANLogStorage): ISANLog;
begin
  if ExistsByName(AName) then
    raise ESANLogError.CreateResFmt(@sSANLoggerLogAlreadyExistsFmtErr, [AName]);

  if not Assigned(AStorage) then
    AStorage := Storages.Items[0];

  Result := TSANLog.Create(AName, AStorage);

  Result.Location := ALocation;

  InitIfNeeded().Add(Result);
end;

class function Api.AddOrGet(const AName: TSANLogName;
  const ALocation: TSANLocation; AStorage : ISANLogStorage): ISANLog;
begin
  if TryFindByName(AName, Result) then
    Exit;

  if not Assigned(AStorage) then
    AStorage := Storages.Items[0];

  Result := TSANLog.Create(AName, AStorage);

  Result.Location := ALocation;

  InitIfNeeded().Add(Result);
end;

class function Api.ExistsByName(const AName: TSANLogName): boolean;
var
  Dummy : ISANLog;
begin
  Result := TryFindByName(AName, Dummy);
end;

class function Api.Remove(const AName : TSANLogName) : boolean;
var
  Dummy : ISANLog;
  i : integer;
begin
  Result := TryFindByName(AName, Dummy, @i);
  if Result then
    Api_Logs.Delete(i);
end;

class function Api.Remove(const ALog : ISANLog) : boolean;
begin
  Result := Api.Remove(ALog.Location);
end;

class function Api.GetLogByName(const AName: TSANLogName): ISANLog;
begin
  if not TryFindByName(AName, Result) then
    raise ESANLogError.CreateResFmt(@sSANLoggerLogNotFoundFmtErr, [AName]);
end;

class function Api.GetLogs(const AIndex: integer): ISANLog;
begin
  Result := ISANLog(InitIfNeeded().Items[AIndex]);
end;

class function Api.GetLogsCount: integer;
begin
  if not Assigned(Api_Logs) then
    Result := 0
  else
    Result := Api_Logs.Count;
end;

class procedure Api.Lock;
begin
  InitIfNeeded().Lock();
end;

class procedure Api.UnLock;
begin
  InitIfNeeded().Lock();
end;

class function Api.MakeTable: ISANLogTable;
begin
  Result := TSANLogTable.Create;
end;

class function Api.MakeTree: ISANLogTree;
begin
  Result := TSANLogTree.Create;
end;

class function Api.TryFindByName(const AName: TSANLogName; out ALog: ISANLog;
  const AIndexPtr: PInteger): boolean;
var
  i : integer;
begin
  ALog    := nil;
  Result  := False;

  if not Assigned(Api_Logs) then
    Exit;

  Api_Logs.Lock();
  try

    for i := 0 to Api_Logs.Count - 1 do
      if {$IF Defined(NEXTGEN) OR not Defined(WIDE_ONLY)}SameText{$ELSE}WideSameText{$ENDIF}(AName, Logs[i].Name) then
      begin
        ALog := ISANLog(Api_Logs[i]);

        if Assigned(AIndexPtr) then
          AIndexPtr^ := i;

        Exit(True);
      end;

  finally
    Api_Logs.UnLock();
  end;

end;

class function Api.CalcTagsWeight(const ATags: TSANLocation): integer;
var
  S, TagStr : string;
  TagUStr : TUpperString;
  Tag : TSANLogControllerTag;
begin
  Result := 0;

  if ATags = '' then
    Exit;

  if not Assigned(Api_Tags) then
    Api_Tags := TDictionary<TUpperString,TSANLogControllerTag>.Create(TUpperStringEqualityComparer.Create());

  for S in ATags.Split(['#']) do
  begin
    TagStr  := Trim(S);

    if TagStr.IsEmpty then
      Continue;

    TagUStr := TagStr;

    if Api_Tags.TryGetValue(TagUStr, Tag) then
      Inc(Result, Tag.Weight)
    else
       Api_Tags.Add(TagUStr, TSANLogControllerTag.Create(TagStr));
  end;
end;

class procedure Api.TagsWeightsSave(const AFileName: TSANLocation);
var
  SS : TStringList;
  Tag : TSANLogControllerTag;
begin
  if not Assigned(Api_Tags) then
    Exit;

  SS := TStringList.Create;
  try

    for Tag in Api_Tags.Values do
      SS.AddPair(Tag.Name, Tag.Weight.ToString);

    SS.Sort();

    SS.SaveToFile(AFileName);

  finally
    FreeAndNil(SS)
  end;
end;

class procedure Api.TagsWeightsLoad(const AFileName: TSANLocation);
var
  SS : TStringList;
  i, W : integer;
  N, V : string;
begin
  SS := TStringList.Create;
  try
    SS.LoadFromFile(AFileName);

    if SS.Count > 0 then
    begin

      if not Assigned(Api_Tags) then
        Api_Tags := TDictionary<TUpperString,TSANLogControllerTag>.Create(TUpperStringEqualityComparer.Create());

      for i := 0 to SS.Count - 1 do
      begin
        N := SS.Names[i];
        V := SS.ValueFromIndex[i];
        if TryStrToInt(V, W) then
          Api_Tags.AddOrSetValue(N, TSANLogControllerTag.Create(N, W));
      end;
    end;

  finally
    FreeAndNil(SS)
  end;

end;
{$ENDREGION}

{$REGION 'Api.TStorages'}
class function Api.TStorages.InitIfNeeded: IInterfaceList;
begin
  if not Assigned(Api_Storages) then
  begin
    Api_Storages := TInterfaceList.Create();
    Api_Storages.Add(TSANLogStorageTextFile.Create);
    Api_Storages.Add(TSANLogStorageConsole.Create);
  end;

  Result := Api_Storages;
end;

class function Api.TStorages.TryFindByName(const AStorageName: TSANLogName;
  out AStorage: ISANLogStorage; const AIndexPtr: PInteger): boolean;
var
  i : integer;
  S : ISANLogStorage;
begin
  if Assigned(Api_Storages) then
  begin
    Api_Storages.Lock;
    try

      for i := 0 to Api_Storages.Count - 1 do
      begin
        S := Api_Storages[i] as ISANLogStorage;

        if AnsiSameText(S.Name, AStorageName) then
        begin
          AStorage := S;

          if Assigned(AIndexPtr) then
            AIndexPtr^ := i;

          Exit(True);
        end;
      end;

    finally
      Api_Storages.Unlock;
    end;
  end;


  AStorage := nil;

  if Assigned(AIndexPtr) then
    AIndexPtr^ := -1;

  Result := False;
end;

class function Api.TStorages.ExistsByName(
  const AStorageName: TSANLogName): boolean;
var
  Dummy : ISANLogStorage;
begin
  Result := TryFindByName(AStorageName, Dummy);
end;

class function Api.TStorages.GetCount: integer;
begin
  if Assigned(Api_Storages) then
    Result := Api_Storages.Count
  else
    Result := 0;
end;

class function Api.TStorages.GetStorages(const AIndex: integer): ISANLogStorage;
begin
  Result := InitIfNeeded[AIndex] as ISANLogStorage;
end;

class function Api.TStorages.GetStorageByName(
  const AName: TSANLogName): ISANLogStorage;
begin
  if not TryFindByName(AName, Result) then
    raise ESANLogError.CreateResFmt(@sSANLoggerStorageNotFoundFmtErr, [AName]);
end;

class function Api.TStorages.Add(AStorage: ISANLogStorage): ISANLogStorage;
begin
  InitIfNeeded.Add(AStorage);
  Result := AStorage;
end;

class function Api.TStorages.Remove(AStorage: ISANLogStorage): boolean;
begin
  Result := Remove(AStorage.Name);
end;

class function Api.TStorages.Remove(const AStorageName: TSANLogName): boolean;
var
  Dummy : ISANLogStorage;
  idx : integer;
begin
  Result := TryFindByName(AStorageName, Dummy, @idx);
  if Result then
    Api_Storages.Delete(idx);
end;
{$ENDREGION}

initialization

finalization
  FreeAndNil(Api_Tags);

end{$WARNINGS OFF}.
{$ENDIF}

