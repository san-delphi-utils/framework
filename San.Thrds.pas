{*******************************************************}
{                                                       }
{       Работа с потоками.                              }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2013-2018         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Работа с потоками.
/// </summary>
unit San.Thrds;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$IFDEF MSWINDOWS}
  WinApi.Windows,
  {$ENDIF}

  System.SysUtils, System.Classes;
{$ENDREGION}
{$ENDIF}

{$IFDEF MSWINDOWS}
const
  /// <summary>
  ///   Таймаут ожидания событий в потоке.
  /// </summary>
  THREAD_WAIT_TIMEOUT  =
  {$IFDEF USE_ALIASES}
    San.Thrds.THREAD_WAIT_TIMEOUT;
  {$ELSE}
    1000;
  {$ENDIF}

  /// <summary>
  ///   Фиксированный таймаут бездействия одного шага потока.
  /// </summary>
  STEP_SLEEP_TIMEOUT   =
  {$IFDEF USE_ALIASES}
    San.Thrds.STEP_SLEEP_TIMEOUT;
  {$ELSE}
    1;
  {$ENDIF}

type
  /// <summary>
  ///   Поток, имеющий системное событие своего завершения
  /// </summary>
  /// <remarks>
  ///   При ожидании в таком потоке других событий можно моментально отловить завершение.
  /// </remarks>
  TEventedThread =
  {$IFDEF USE_ALIASES}
    San.Thrds.TEventedThread;
  {$ELSE}
    class(TThread)
  strict private
    FTerminateEvent : THandle;

  protected
    procedure TerminatedSet; override;

  public
    constructor Create; overload;
    constructor Create(CreateSuspended: Boolean); overload;
    destructor Destroy; override;

    /// <summary>
    ///   Системное событие, сигнализирующее о завершении потока.
    /// </summary>
    property TerminateEvent : THandle  read FTerminateEvent;
  end;
  {$ENDIF}

  /// <summary>
  ///   Массив из двух описателей.
  /// </summary>
  THandles2 =
  {$IFDEF USE_ALIASES}
    San.Thrds.THandles2;
  {$ELSE}
    array [0..1] of THandle;
  {$ENDIF}

  /// <summary>
  ///   Поток вызова события по таймауту.
  /// </summary>
  TTimerThread =
  {$IFDEF USE_ALIASES}
    San.Thrds.TTimerThread;
  {$ELSE}
  class(TEventedThread)
  strict private
    FTimeOutEvent, FRestartEvent : THandle;
    FTimeOut : integer;
    FRestartHandles : THandles2;

    function WaitForRestart : boolean;

  protected
    procedure Execute(); override;

  public
    constructor Create(const ATimeOut : integer = 1; const AInitialState : boolean = False); overload;
    constructor Create(const ACreateSuspended: Boolean; const ATimeOut : integer = 1;
       const AInitialState : boolean = False); overload;
    destructor Destroy; override;

    /// <summary>
    ///   Продолжить ожидание.
    /// </summary>
    procedure Restart;

    /// <summary>
    ///   Событие по таймауту. Пердоставляется во вне.
    /// </summary>
    property TimeOutEvent : THandle  read FTimeOutEvent;

    /// <summary>
    ///   Таймаут события.
    /// </summary>
    property TimeOut : integer  read FTimeOut  write FTimeOut;
  end;
  {$ENDIF}

{$ENDIF}

type
  /// <summary>
  ///   Фасад многопоточного блокировщика
  /// </summary>
  TMultithreadLockerFacade =
  {$IFDEF USE_ALIASES}
    San.Thrds.TMultithreadLockerFacade;
  {$ELSE}
  record
  public type
    {$REGION 'types'}
    /// <summary>
    ///   Информация о блокировках на чтение для данного потока
    /// </summary>
    TThreadReadLocks = record
    private
      FThread : TThread;
      FLocks : NativeInt;
    end;

    /// <summary>
    ///   Данные о блокировках
    /// </summary>
    TMultithreadLockerData = class(TObject)
    private
      FReadLocks : TArray<TThreadReadLocks>;
      FWriteThread : TThread;

    public
      /// <summary>
      ///   Изменить счетчик для текущего потока
      /// </summary>
      /// <param name="AStep">
      ///   Шаг изменения.
      /// </param>
      /// <remarks>
      ///   Добавление записей в массив только при полжительных <c>AStep</c>
      /// </remarks>
      function UpdateReadLocks(const AStep : integer) : integer;

      /// <summary>
      ///   Проверить, есть ли блокировки на чтение от других потоков
      /// </summary>
      function HasOtherReadLocks() : boolean;

      /// <summary>
      ///   Завершение использования сущностей текущем потоком
      /// </summary>
      procedure DetachCurrentThread();

      /// <summary>
      ///   Проверка на наличие блокировок
      /// </summary>
      function HasLocks : boolean;
    end;

    ITarget = interface(IInterface)
    ['{EA639E12-A7C9-4AE7-B00F-010BDD0A1C1F}']
      /// <summary>
      ///   Выдает указатель на данные блокировки <c>TMultithreadLockerData</c>
      /// </summary>
      /// <returns>
      ///   <para>
      ///     <c>nil</c> — значит объект не поддерживает блокировки, методы фасада работают вхолостую
      ///   </para>
      ///   <para>
      ///     Иначе — поддеживаются блокировки
      ///   </para>
      /// </returns>
      function GetLockerData() : TMultithreadLockerFacade.TMultithreadLockerData;

      /// <summary>
      ///   Попытка получения блокировки на чтение для внешних объектов
      ///   в течение указанного времени
      /// </summary>
      /// <param name="AStartTicks">
      ///   Количество тиков в начале запроса
      /// </param>
      /// <param name="ATimeOut">
      ///   Таймаут
      /// </param>
      /// <param name="AFailedToLock">
      ///   При успехе — <c>nil</c>, при неудаче — объект, который не удалось заблокировать
      /// </param>
      /// <returns>
      ///   <c>True</c> — все необходимые блокировки получены / <c>False</c> — таймаут
      /// </returns>
      function ExternalTryEnterRead(const AStartTicks, ATimeOut : Cardinal; out AFailedToLock : TObject) : boolean;

      /// <summary>
      ///   Отпустить блокировки на чтение для внешних объектов
      /// </summary>
      /// <param name="ALastLocked">
      ///   Получен от <c>ExternalTryEnterRead</c> при неудаче,
      ///   этот объект и следующие за ним НЕ нуждаются в разблокировке
      /// </param>
      procedure ExternalLeaveRead(const AFailedToLock : TObject);
    end;
    {$ENDREGION}

  strict private
    FTarget : ITarget;

    /// <summary>
    ///   Ожидание снятия блокировки на запись
    /// </summary>
    function WaitForWriteUnlocked(const AData : TMultithreadLockerData;
      const AForWrite : boolean;
      const AStartTicks, ATimeOut : Cardinal) : boolean;

  private
    /// <summary>
    ///   Общая обработка Enter/Leave
    /// </summary>
    /// <remarks>
    ///   Реализует:
    ///   <para>
    ///     <c>TryEnterRead</c>
    ///   </para>
    ///   <para>
    ///     <c>EnterRead</c>
    ///   </para>
    ///   <para>
    ///     <c>LeaveRead</c>
    ///   </para>
    ///   <para>
    ///     <c>TryEnterWrite</c>
    ///   </para>
    ///   <para>
    ///     <c>EnterWrite</c>
    ///   </para>
    ///   <para>
    ///     <c>LeaveWrite</c>
    ///   </para>
    ///   Может вызываться напрямую из:
    ///   <para>
    ///     <c>TEntitiesObject.ParentLockerTryEnterRead</c>
    ///   </para>
    ///   <para>
    ///     <c>TEntitiesObject.ParentLockerLeaveRead</c>
    ///   </para>
    /// </remarks>
    function DoAction(const AForWrite, AIsEnter : boolean;
      const AStartTicks, ATimeout : Cardinal) : boolean;

  public
    constructor Create(ATarget : ITarget);

    /// <summary>
    ///   Попытка получения блокировки на чтение в течение указанного времени
    /// </summary>
    /// <returns>
    ///   <c>True</c> — блокировка получена / <c>False</c> — таймаут
    /// </returns>
    function TryEnterRead(const ATimeOut : Cardinal = 0) : boolean; overload;

    /// <summary>
    ///   Попытка получения блокировки на чтение в течение указанного времени.
    ///   Начиная с указанного момента
    /// </summary>
    /// <returns>
    ///   <c>True</c> — блокировка получена / <c>False</c> — таймаут
    /// </returns>
    function TryEnterRead(const AStartTicks, ATimeOut : Cardinal) : boolean; overload;

    /// <summary>
    ///   Получения блокировки на чтение с ожиданием
    /// </summary>
    procedure EnterRead;

    /// <summary>
    ///   Освобождение блокировки на чтение
    /// </summary>
    procedure LeaveRead;

    /// <summary>
    ///   Попытка немедленного получения блокировки на запись
    /// </summary>
    /// <returns>
    ///   True - блокировка получена
    /// </returns>
    function TryEnterWrite(const ATimeOut : Cardinal = 0) : boolean; overload;

    /// <summary>
    ///   Попытка немедленного получения блокировки на запись
    ///   Начиная с указанного момента
    /// </summary>
    /// <returns>
    ///   True - блокировка получена
    /// </returns>
    function TryEnterWrite(const AStartTicks, ATimeOut : Cardinal) : boolean; overload;

    /// <summary>
    ///   Получения блокировки на запись с ожиданием
    /// </summary>
    procedure EnterWrite;

    /// <summary>
    ///   Освобождение блокировки на запись
    /// </summary>
    procedure LeaveWrite;

    /// <summary>
    ///   Освободить внутренние данные
    /// </summary>
    procedure Free;
  end;
  {$ENDIF}


{$IFNDEF USE_ALIASES}
implementation

uses
  {$REGION 'System'}
  System.Math,
  {$ENDREGION}

  {$REGION 'San'}
  San.Messages;
  {$ENDREGION}

{$IFDEF MSWINDOWS}
{$REGION 'TEventedThread'}
constructor TEventedThread.Create;
begin
  Create(False);
end;

constructor TEventedThread.Create(CreateSuspended: Boolean);
begin

  FTerminateEvent := CreateEvent(nil, True, False, nil);
  if FTerminateEvent = 0 then
    RaiseLastOSError;

  inherited Create(CreateSuspended);
end;

destructor TEventedThread.Destroy;
begin
  CloseHandle(FTerminateEvent);
  inherited;
end;

procedure TEventedThread.TerminatedSet;
begin
  SetEvent(FTerminateEvent);
end;
{$ENDREGION}

{$REGION 'TTimerThread'}
function TTimerThread.WaitForRestart: boolean;
var
  WaitRes : Cardinal;
begin

  repeat

    WaitRes := WaitForMultipleObjects(High(FRestartHandles) + 1, @FRestartHandles, False, THREAD_WAIT_TIMEOUT);
    case WaitRes of
      WAIT_OBJECT_0 + 0:
        Exit(False);

      WAIT_OBJECT_0 + 1:
      begin
        if not ResetEvent(FRestartEvent) then
          RaiseLastOSError;

        Exit(True);
      end;

      WAIT_TIMEOUT:
        Sleep(STEP_SLEEP_TIMEOUT);

      WAIT_FAILED:
        RaiseLastOSError;

    else
      raise EThread.CreateResFmt(@sUnexpectedWaitResultFmt, [WaitRes]);
    end;

  until False;

  Result := False;
end;

procedure TTimerThread.Execute;
var
  WaitRes : Cardinal;
begin
  FRestartHandles[0] := TerminateEvent;
  FRestartHandles[1] := FRestartEvent;

  repeat

    WaitRes := WaitForSingleObject(TerminateEvent, FTimeOut);
    case WaitRes of
      WAIT_OBJECT_0 + 0:
        Break;

      WAIT_TIMEOUT:
      begin
        if not SetEvent(FTimeOutEvent) then
          RaiseLastOSError;

        if not WaitForRestart() then
          Break;
      end;

      WAIT_FAILED:
        RaiseLastOSError;

    else
      raise EThread.CreateResFmt(@sUnexpectedWaitResultFmt, [WaitRes]);
    end;

  until False;

end;

procedure TTimerThread.Restart;
begin
  if not ResetEvent(FTimeOutEvent) then
    RaiseLastOSError;

  if not SetEvent(FRestartEvent) then
    RaiseLastOSError;
end;

constructor TTimerThread.Create(const ATimeOut: integer; const AInitialState : boolean);
begin
  Create(False, ATimeOut, AInitialState);
end;

constructor TTimerThread.Create(const ACreateSuspended: Boolean; const ATimeOut: integer; const AInitialState : boolean);
begin
  FTimeOut := ATimeOut;

  FTimeOutEvent := CreateEvent(nil, True, AInitialState, nil);
  if FTimeOutEvent = 0 then
    RaiseLastOSError;

  FRestartEvent := CreateEvent(nil, True, False, nil);
  if FRestartEvent = 0 then
    RaiseLastOSError;

  inherited Create(False);
end;

destructor TTimerThread.Destroy;
begin
  CloseHandle(FTimeOutEvent);
  CloseHandle(FRestartEvent);

  inherited;
end;
{$ENDREGION}

{$ENDIF}

{$REGION 'TMultithreadLockerFacade'}

{$REGION 'TMultithreadLockerFacade.TMultithreadLockerData'}
function TMultithreadLockerFacade.TMultithreadLockerData.UpdateReadLocks(
  const AStep: integer): integer;
var
  left, right, center, i: integer;
  x : IntPtr;
  v : TThreadReadLocks;
begin
  v.FThread := TThread.Current;

  x := IntPtr(TThread.Current);
  left := 0;
  right := Length(FReadLocks) - 1;

  if (right < left) or (x > IntPtr(FReadLocks[right].FThread)) then
  begin
    if AStep < 1 then
      Exit(0);

    v.FLocks := 1;
    Insert(v, FReadLocks, right + 1);
  end

  else
  if x < IntPtr(FReadLocks[left].FThread) then
  begin
    if AStep < 1 then
      Exit(0);

    v.FLocks := 1;
    Insert(v, FReadLocks, 0);
  end

  else
  begin

    // сужаем диапазон поиска до одного индекса
    while((right - left) > 1)do
    begin
      center := (left + right) div 2;
      if x <= IntPtr(FReadLocks[left].FThread) then
        right := center
      else
        left := center;
    end;

    // проверяем что осталось
    if x = IntPtr(FReadLocks[left].FThread) then
      i := left

    else
    if x = IntPtr(FReadLocks[right].FThread) then
      i := right

    else
    begin
      if AStep < 1 then
        Exit(0);

      v.FLocks := 1;
      Insert(v, FReadLocks, right);
      Exit(right);
    end;

    v.FLocks := FReadLocks[i].FLocks + AStep;

    Assert(v.FLocks >= 0);

    if AStep <> 0 then
      FReadLocks[i] := v;
  end;

  Result := v.FLocks;
end;

function TMultithreadLockerFacade.TMultithreadLockerData.HasOtherReadLocks: boolean;
var
  i : integer;
begin
  for i := 0 to High(FReadLocks) do
    if (FReadLocks[i].FThread <> TThread.Current) and (FReadLocks[i].FLocks > 0) then
      Exit(True);

  Result := False;
end;

procedure TMultithreadLockerFacade.TMultithreadLockerData.DetachCurrentThread;
var
  i : integer;
begin
  for i := 0 to High(FReadLocks) do
    if (FReadLocks[i].FThread = TThread.Current) then
    begin
      System.Delete(FReadLocks, i, 1);
      Exit;
    end;
end;

function TMultithreadLockerFacade.TMultithreadLockerData.HasLocks: boolean;
var
  i : integer;
begin
  if Assigned(FWriteThread) then
    Exit(True);

  for i := 0 to High(FReadLocks) do
    if (FReadLocks[i].FLocks > 0) then
      Exit(True);

  Result := False;
end;
{$ENDREGION}

constructor TMultithreadLockerFacade.Create(ATarget : ITarget);
begin
  FTarget := ATarget;
end;

function TMultithreadLockerFacade.WaitForWriteUnlocked(
  const AData: TMultithreadLockerData; const AForWrite: boolean;
  const AStartTicks, ATimeOut: Cardinal): boolean;
var
  CurTicks, TimeOut : Cardinal;
begin
  Result := False;
  TimeOut := ATimeOut;

  repeat

    if not TMonitor.Enter(AData, TimeOut) then
      Exit(False);
    try
      Result := (not Assigned(AData.FWriteThread) or (AData.FWriteThread = TThread.Current))
                and (not AForWrite or AForWrite and not AData.HasOtherReadLocks());
    finally
      if not Result then
        TMonitor.Exit(AData);
    end;

    if Result then
      Break;

    CurTicks := TThread.GetTickCount();
    if CurTicks - AStartTicks > ATimeOut then
      Exit(False);

    TimeOut := ATimeOut - (CurTicks - AStartTicks);
  until False;
end;

function TMultithreadLockerFacade.DoAction(const AForWrite, AIsEnter: boolean;
  const AStartTicks, ATimeout: Cardinal): boolean;
var
  Data : TMultithreadLockerData;
  LastLocked : TObject;
begin
  Data := FTarget.GetLockerData();
  if not Assigned(Data) then
    Exit(True);

  LastLocked := nil;

  Result := not AIsEnter
         or     AIsEnter and FTarget.ExternalTryEnterRead(AStartTicks, ATimeout, LastLocked);
  if Result then
  try

    Result := WaitForWriteUnlocked(Data, AForWrite, AStartTicks, ATimeout);
    if Result then
    try

      if not AForWrite then
        Data.UpdateReadLocks( IfThen(AIsEnter, +1, -1) )

      else
      if AIsEnter then
        Data.FWriteThread := TThread.Current

      else
        Data.FWriteThread := nil;

    finally
      TMonitor.Exit(Data);
    end;

  finally
    if not (Result and AIsEnter) then
      FTarget.ExternalLeaveRead(LastLocked);
  end;

end;

function TMultithreadLockerFacade.TryEnterRead(const ATimeOut : Cardinal): boolean;
begin
  Result := DoAction(False, True, TThread.GetTickCount, ATimeOut);
end;

function TMultithreadLockerFacade.TryEnterRead(const AStartTicks,
  ATimeOut: Cardinal): boolean;
begin
  Result := DoAction(False, True, AStartTicks, ATimeOut);
end;

procedure TMultithreadLockerFacade.EnterRead;
begin
  DoAction(False, True, TThread.GetTickCount, INFINITE);
end;

procedure TMultithreadLockerFacade.LeaveRead;
begin
  DoAction(False, False, TThread.GetTickCount, INFINITE);
end;

function TMultithreadLockerFacade.TryEnterWrite(const ATimeOut : Cardinal): boolean;
begin
  Result := DoAction(True, True, TThread.GetTickCount, ATimeOut);
end;

function TMultithreadLockerFacade.TryEnterWrite(const AStartTicks,
  ATimeOut: Cardinal): boolean;
begin
  Result := DoAction(True, True, AStartTicks, ATimeOut);
end;

procedure TMultithreadLockerFacade.EnterWrite;
begin
  DoAction(True, True, TThread.GetTickCount, INFINITE);
end;

procedure TMultithreadLockerFacade.LeaveWrite;
begin
  DoAction(True, False, TThread.GetTickCount, INFINITE);
end;

procedure TMultithreadLockerFacade.Free;
begin
  FTarget := nil;
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
