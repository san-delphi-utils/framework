{*******************************************************}
{                                                       }
{       Модуль псевдонимов библиотеки San.              }
{       Публичная часть Api, для встраивания            }
{       в унаследованные библиотеки                     }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2013-2016         Станцо А.В.     }
{                                                       }
{*******************************************************}
const
  /// <summary>
  ///   Rtti.
  /// </summary>
  Rtti : San.Rtti.Api = ();

  /// <summary>
  ///   Интерфейсы.
  /// </summary>
  Intfs : San.Intfs.Api = ();

  /// <summary>
  ///   Объекты.
  /// </summary>
  Objs : San.Objs.Api = ();

  /// <summary>
  ///   Исключения.
  /// </summary>
  Excps : San.Excps.Api = ();

  /// <summary>
  ///   Логгер.
  /// </summary>
  Logger : San.Logger.Api = ();

  /// <summary>
  ///   Постпроцессор.
  /// </summary>
  PostProcessor : San.PostProcessor.Api = ();

  /// <summary>
  ///   Замена в тексте.
  /// </summary>
  Replacement : San.Replacement.Api = ();

  /// <summary>
  ///   Работа со строками.
  /// </summary>
  StrUtils : San.StrUtils.Api = ();

  /// <summary>
  ///   Доступ к методам работы со средой разработки
  /// </summary>
  IDE : San.IDE.Api = ();
