  State := Ord(FLeft is TPathItemStatic) + 2 * Ord(FRight is TPathItemStatic);
  case State of
    // !StaticLeft && !StaticRight
    0:
      Result :=
        function (const AContext : TDataEntity; const AThis: TDataNode) : boolean
        var
          DFldL, DFldR : TDataField;
        begin
          DFldL := LeftDataField[ AContext, AThis];
          DFldR := RightDataField[AContext, AThis];
          Result := DFldL.CompareDataFieldTo(DFldR, CompareOperation);
        end;

    // StaticLeft && !StaticRight
    1:
      Result :=
        function (const AContext : TDataEntity; const AThis: TDataNode) : boolean
        var
          DFldR : TDataField;
        begin
          DFldR := RightDataField[AContext, AThis];
          Result := DFldR.CompareDataValueTo(TPathItemStatic(FLeft).Value, CompareOperation.Flip);
        end;

    // !StaticLeft && StaticRight
    2:
      Result :=
        function (const AContext : TDataEntity; const AThis: TDataNode) : boolean
        var
          DFldL : TDataField;
        begin
          DFldL := LeftDataField[ AContext, AThis];
          Result := DFldL.CompareDataValueTo(TPathItemStatic(FRight).Value, CompareOperation);
        end;

    // StaticLeft && StaticRight
    3:
    begin
      Result :=
        function (const AContext : TDataEntity; const AThis: TDataNode) : boolean
        begin
          Result := TPathItemStatic(FLeft).CompareDataTo(TPathItemStatic(FRight), FCompareOperation);
        end;
     end;
  else
    San.Api.Excps.ItsImpossible(Result);
  end;
