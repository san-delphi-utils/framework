{*******************************************************}
{                                                       }
{       Динамическая структура данных и метаданных.     }
{       Константы                                       }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2017              Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   Сообщения.
/// </summary>
unit San.Entities.Consts;
{$I San.Entities.inc}
interface

const
  sLink        = 'link';
  sRecursion   = 'recursion';
  sFields      = 'fields';
  sAncestor    = 'ancestor';
  sIndexes     = 'indexes';
  sUnique      = 'unique';
  sField       = 'field';
  sName        = 'name';
  sType        = 'type';
  sVersion     = 'version';
  sRoot        = 'root';
  sEntity      = 'entity';
  sCollection  = 'collection';
  sDefault     = 'default';

implementation

end.
