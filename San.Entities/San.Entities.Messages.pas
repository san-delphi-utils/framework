{*******************************************************}
{                                                       }
{       Динамическая структура данных и метаданных.     }
{       Сообщения                                       }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2017              Станцо А.В.     }
{                                                       }
{*******************************************************}
/// <summary>
///   Сообщения.
/// </summary>
unit San.Entities.Messages;
{$I San.Entities.inc}
interface

resourcestring
  {$REGION 'San.Entities'}
  s_San_Entities_MetaFieldNotFoundFmt =
    'Метаданные поля с именем "%s" не найдены для метасущности "%s"';

  s_San_Entities_MetaFieldClassNotFoundFmt =
    'Класс поля метаданных не найден для типа "%s"';

  s_San_Entities_DataClassNotImplementedForFmt =
    'Метод %s не реализован для класса "%s"';

  s_San_Entities_ForbiddenCreateDataDirectlyFmt =
    'Запрещено напрямую создать экземпляры данных (в частности "%s").'#13#10 +
    'Исползуйте MakeData объектов метаданных';

  s_San_Entities_DataFieldNotFoundFmt =
    'Данные поля с именем "%s" не найдены для сущности "%s"';

  s_San_Entities_DataFieldReadCastErrorFmt =
    'Не возможно преобразовать значение типа %s в значение типа %s при чтении';

  s_San_Entities_DataFieldWriteCastErrorFmt =
    'Не возможно преобразовать значение типа %s в значение типа %s при записи';

  s_San_Entities_OtherRootError =
    'Владеющий узел принадлежит к другому корню';

  s_San_Entities_SingleIndexClassNotAssignedErrorFmt =
    'Класс индекса для типа метаполя "%s" не задан';

  s_San_Entities_InvalidMetaOwnerClassErrorFmt =
    'Класс владельца метаданных "%s" не допустим для класса метаданных "%s"';

  s_San_Entities_InvalidMetaClassErrorFmt =
    'Класс метаданных "%s" не допустим для класса данных "%s"';

  s_San_Entities_InvalidDataOwnerClassErrorFmt =
    'Классданных "%s" не допустим для класса данных "%s"';

  s_San_Entities_SetNameNotSupportedErrorFmt =
    'Задание имени не поддерживается классом "%s"';

  s_San_Entities_RichClassMustBeInherithFromForMetaClassFmt =
    'Класс "%s" должен быть унаследован от "%s" для метакласса "%s"';

  s_San_Entities_MetaPropsChangingForbiddenWithExistingData =
    'Изменение свойств метаданных запрещено, уже созданы данные';

  s_San_Entities_MetaCanNotAssignAncestorForLink =
    'Нельзя назначать предка для ссылки на сущность';

  s_San_Entities_MetaAncestorIsSetOnceAndCanNotBeChanged =
    'Предок метасущности задается один раз и не может быть изменен';

  s_San_Entities_MetaCircularInheritanceAttempt =
    'Попытка циклического наследования метасущности';

  s_San_Entities_MetaSetAncestorInternalErrorFmt =
    'Внутренняя ошибка задания предка наследования метасущности'#13#10'Наслкдник "%s".'#13#10'Старый предок "%s"'#13#10'Новый предок "%s"';

  s_San_Entities_MetaCollectionCanNotBeRecursiveFmt =
    'Метаколлекции не могут быть рекурсивными. Коллекция "%s"';

  s_San_Entities_MetaRootCanNotBeLinkTypeFmt =
    'Корневые метасущности не могут быть ссылками. Корень "%s"';

  s_San_Entities_MetaInternalDataMustBeInitializedBeforeDataCreatingFmt =
    'Внутренние данные метасущности/метаколлекции должны быть инициализированы'#13#10'перед созданием экземпляра данных по этим метаданным.'#13#10'Метаданные "%s"';

  s_San_Entities_CanNotResolvingPathFmt =
    'Не возможно разрешить путь "%s"';

  s_San_Entities_ResolvingPathUnexpectedEndFmt =
    'Неожиданное завершение текста при разрешении пути для лексемы "%s"';

  s_San_Entities_ResolvingPathUnexpectedCharFmt =
    'Неожиданный символ при разрешении пути "%s"';

  s_San_Entities_ResolvingInternalErrorFmt =
    'Внутренняя ошибка разрешения пути "%s" в позиции %d';

  s_San_Entities_ResolvingParentIsNilFmt =
    'При разрешении пути родитель для узла "%s" не существует (nil)';

  s_San_Entities_ResolvingChildNotFoundFmt =
    'При разрешении пути дочерний узел с именем "%s" не найден для узла "%s"';

  s_San_Entities_ResolvingFilterMultiTargetsDataFmt =
    'При разрешении пути фильтром, получено несколько целевых объектов данных для Context "%s" и This "%s"';

  s_San_Entities_ResolvingFilterTargetMustBeCollectionFmt =
    'При разрешении пути фильтром, целевой объект должен быть коллекцией (а он %s.%s) для узла "%s"';

  s_San_Entities_ResolvingFieldsConditionTargetMustBeFieldFmt =
    'При разрешении пути условием, целевой объект должен быть полем (а он %s.%s) для узла "%s"';

  s_San_Entities_ResolvingFieldsConditionLeftTargetMustBeFieldFmt =
    'При разрешении пути условием, целевой объект левой части должен быть полем (а он %s.%s) для узла "%s"';

  s_San_Entities_ResolvingFieldsConditionRightTargetMustBeFieldFmt =
    'При разрешении пути условием, целевой объект правой части должен быть полем (а он %s.%s) для узла "%s"';

  s_San_Entities_ResolvingUnexpectedLexeme =
    'Неожиданная лексемма';

  s_San_Entities_ResolvingPathSintaxUnexpectedEnd =
    'Неожиданное завершение списка лексем';

  s_San_Entities_ResolvingPathSintaxStaticPathItemFmt =
    'Неожиданный статический элемент пути "%s"';

  s_San_Entities_ResolvingSintaxInternalErrorFmt =
    'Внутренняя ошибка синтаксического анализа при разрешении пути "%s" для %d-й лексемы'#13#10'Kind: %s'#13#10'Start: %d'#13#10'Length: %d'#13#10'Value : %s';

  s_San_Entities_ResolvingRichClassPathUnexpectedLexemeFmt =
    'Неожиданная лексема при разрешении пути лучшего класса "%s" для пути "%s" в позиции %d';

  s_San_Entities_RichClassUnexpectedNodeForFmt =
    'Неожиданный узел метаданных "%s" для пути "%s"';

  s_San_Entities_ItemsNotFoundFmt =
    'Элементы не найдены в коллекции "%s"';

  s_San_Entities_DataFieldRefreshInternalErrorFmt =
    'Внутренняя ошибка обновления иформации для поля "%s"';

  s_San_Entities_JSONImportPairNotFoundFmt =
    'При импорте из JSON не найдена пара значений "%s"';

  s_San_Entities_JSONImportUnexpectedValueFmt =
    'Неожиданный тип значения (%s: %s) при импорте из JSON';

  s_San_Entities_JSONImportInternalErrorForFieldFmt =
    'Внутренняя ошибка при импорте из JSON поля с индексом %d и именем "%s"';

  s_San_Entities_JSONImportInternalErrorFmt =
    'Внутренняя ошибка при импорте из JSON:'#13#10'%s';

  s_San_Entities_JSONExportInternalErrorForNodeFmt =
    'Внутренняя ошибка при экспорте в JSON для узла %s "%s"';

  s_San_Entities_InternalPropIdxGetErrorForFieldFmt =
    'Внутренняя ошибка при получении (Get) значения индексного свойства (индекс %d) для поля "%s"';

  s_San_Entities_InternalPropIdxSetErrorForFieldFmt =
    'Внутренняя ошибка при установке (Set) значения индексного свойства (индекс %d) для поля "%s"';

  s_San_Entities_ValueCastErrorFromToFmt =
    'Ошибка преобразования типа TValue.Cast из типа "%s" в тип "%s"';

  s_San_Entities_FindByFieldInternalErrorXXXYYYFmt =
    'Внутренняя ошибка для поиска по полю "%s" со значением "%s"';

  s_San_Entities_FieldCompareDataValueToInternalErrorFmt =
    'Внутренняя ошибка сравнения значения поля "%s" (тип значения "%s") с фиксированным значением типа "%s"';

  s_San_Entities_SetDefaultValueIsForbiddenForNodeFieldXXXFmt =
    'Запрещается установливать значение по умолчанию для поля узла ("%s")';

  s_San_Entities_ValueToJSONConvertErrorForXXXFmt =
    'Не возможно преобразовать значение типа %s (%s) в JSON';

  s_San_Entities_JSONToValueConvertErrorForXXXFmt =
    'Не возможно преобразовать JSON %s (%s) в значение типа %s (%s)';

  {$ENDREGION}


implementation

end.

