{*******************************************************}
{                                                       }
{       Динамическая структура данных и метаданных.     }
{       Чтение и запись в XML.                          }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Динамическая структура данных и метаданных.
///   Чтение и запись в XML.
/// </summary>
unit San.Entities.RW.XML;
{$I San.Entities.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Classes, System.Variants,
  {$ENDREGION}

  {$REGION 'Xml'}
  Xml.xmldom, Xml.XMLIntf, {$IFDEF MSWINDOWS}Xml.Win.msxmldom,{$ENDIF} Xml.XMLDoc,
  {$ENDREGION}

  {$REGION 'San'}
  San.Entities
  {$ENDREGION}
;
{$ENDREGION}

{$ENDIF}

type
  /// <summary>
  ///   Ридер метаданных.
  /// </summary>
  TMetaReaderXML =
  {$IFDEF USE_ALIASES}
    San.Entities.RW.XML.TMetaReaderXML;
  {$ELSE}
    class (TMetaReader)
  strict private
    FXMLDocument : IXMLDocument;

  public
    constructor Create(AXMLDocument : IXMLDocument);

    /// <summary>
    ///   Заполнить метаданные сущности с потерей предидущих данных из внешнего источника.
    /// </summary>
    procedure Read(const AMetaEntityRoot : TMetaEntityRoot); override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Райтер метаданных.
  /// </summary>
  TMetaWriterXML =
  {$IFDEF USE_ALIASES}
    San.Entities.RW.XML.TMetaWriterXML;
  {$ELSE}
    class (TMetaWriter)
  strict private
    FXMLDocumentOptions : TXMLDocOptions;
    FXMLDocument : IXMLDocument;

    function CreateXMLDocument : IXMLDocument;

  public
    constructor Create(const AXMLDocumentOptions : TXMLDocOptions =
      [doNodeAutoCreate,doAttrNull,doAutoPrefix,doNamespaceDecl]);

    /// <summary>
    ///   Записать метаданные сущности во внешний источник.
    /// </summary>
    procedure Write(const AMetaEntityRoot : TMetaEntityRoot); override;

    /// <summary>
    ///   Документ полученый после записи.
    /// </summary>
    property XMLDocument : IXMLDocument  read FXMLDocument;
  end;
  {$ENDIF}

  /// <summary>
  ///   Ридер данных.
  /// </summary>
  TDataReaderXML =
  {$IFDEF USE_ALIASES}
    San.Entities.RW.XML.TDataReaderXML;
  {$ELSE}
    class (TDataReader)
  strict private
    FXMLDocument : IXMLDocument;

  public
    constructor Create(AXMLDocument : IXMLDocument);

    /// <summary>
    ///   Заполнить данные сущности с потерей предидущих данных из внешнего источника.
    /// </summary>
    procedure Read(const ADataEntityRoot : TDataEntityRoot); override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Райтер данных.
  /// </summary>
  TDataWriterXML =
  {$IFDEF USE_ALIASES}
    San.Entities.RW.XML.TDataWriterXML;
  {$ELSE}
    class (TDataWriter)
  strict private
    FXMLDocumentOptions : TXMLDocOptions;
    FXMLDocument : IXMLDocument;

    function CreateXMLDocument : IXMLDocument;

  public
    constructor Create(const AXMLDocumentOptions : TXMLDocOptions =
      [doNodeAutoCreate,doAttrNull,doAutoPrefix,doNamespaceDecl]);

    /// <summary>
    ///   Записать данные сущности во внешний источник.
    /// </summary>
    procedure Write(const ADataEntityRoot : TDataEntityRoot); override;

    /// <summary>
    ///   Документ полученый после записи.
    /// </summary>
    property XMLDocument : IXMLDocument  read FXMLDocument;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Доступ к методам работы с интерфейсами.
  /// </summary>
  Api = record
  public type
    TMeta = record
      class procedure ReadFromFile(const AMetaEntityRoot : TMetaEntityRoot; const AFileName : string); static;
      class procedure ReadFromStr(const AMetaEntityRoot : TMetaEntityRoot; const AXMLData : string); static;

      class procedure WriteToFile(const AMetaEntityRoot : TMetaEntityRoot; const AFileName : string); static;
      class function WriteToStr(const AMetaEntityRoot : TMetaEntityRoot) : string; static;

    end;

    TData = record
      class procedure ReadFromFile(const ADataEntityRoot : TDataEntityRoot; const AFileName : string); static;
      class procedure ReadFromStr(const ADataEntityRoot : TDataEntityRoot; const AXMLData : string); static;

      class procedure WriteToFile(const ADataEntityRoot : TDataEntityRoot; const AFileName : string); static;
      class function WriteToStr(const ADataEntityRoot : TDataEntityRoot) : string; static;

    end;

  public const
    Meta : San.Entities.RW.XML.Api.TMeta = ();
    Data : San.Entities.RW.XML.Api.TData = ();

  end;


implementation

{$REGION 'uses'}
uses
  San.Messages
;
{$ENDREGION}

{$REGION 'TMetaReaderXML'}
constructor TMetaReaderXML.Create(AXMLDocument: IXMLDocument);
begin
  inherited Create();

  FXMLDocument := AXMLDocument;
end;

procedure TMetaReaderXML.Read(const AMetaEntityRoot: TMetaEntityRoot);

  procedure ReadEntities(AXMLNode : IXMLNode; const AEntities : TMetaEntities); forward;

  procedure ReadEntity(AXMLNode : IXMLNode; const AEntity : TMetaEntity);
  var
    FieldsNode, FieldNode : IXMLNode;
    i : integer;
    MetaFieldClass : TMetaFieldClass;
    MetaField : TMetaField;
  begin
    try
      AEntity.RichDataClassName := VarToStr(AXMLNode.Attributes['rich']);

      FieldsNode := AXMLNode;// AXMLNode.ChildNodes.FindNode('fields');

      for i := 0 to FieldsNode.ChildNodes.Count - 1 do
      begin
        FieldNode := FieldsNode.ChildNodes[i];

        try
          if not TMetaField.FieldsByStrIDRegistry.TryGetValue(FieldNode.NodeName, MetaFieldClass) then
            raise ENotSupportedException.Create(FieldNode.NodeName);

          MetaField := MetaFieldClass.Create(AEntity);
          MetaField.Name := FieldNode.Attributes['name'];

          AEntity.Fields.Add(MetaField);

          if not MetaField.IsSimpleField then
            if MetaField.IsEntityField then
              ReadEntity(FieldNode, (MetaField as TMetaFieldEntity).Entity)

            else
            if MetaField.IsEntitiesField then
              ReadEntities(FieldNode, (MetaField as TMetaFieldEntities).Entities)

            else
              raise ENotSupportedException.Create(MetaField.ClassName);
        except
          EEntitiesError.CreateResFmt(@s_San_InternalErrorForWithDetailsFmt, [FieldNode.NodeName, FieldNode.XML]).RaiseOuter();
        end;
      end;

    except
      EEntitiesError.CreateResFmt(@s_San_InternalErrorForWithDetailsFmt, [AXMLNode.NodeName, AXMLNode.XML]).RaiseOuter();
    end;
  end;

  procedure ReadEntities(AXMLNode : IXMLNode; const AEntities : TMetaEntities);
  var
    EntityNode : IXMLNode;
  begin
    try
      AEntities.RichDataClassName := VarToStr(AXMLNode.Attributes['rich']);
      AEntities.IsLinks := StrToBool(AXMLNode.Attributes['isLinks']);
      AEntities.IsRecursive := StrToBool(AXMLNode.Attributes['isRecursive']);

      if not AEntities.IsRecursive then
      begin
        EntityNode := AXMLNode.ChildNodes.FindNode('entity');
        ReadEntity(EntityNode, AEntities.ItemMeta);
      end;

    except
      EEntitiesError.CreateResFmt(@s_San_InternalErrorForWithDetailsFmt, [AXMLNode.NodeName, AXMLNode.XML]).RaiseOuter();
    end;
  end;

var
  RootNode : IXMLNode;
begin
  if FXMLDocument.DocumentElement.Attributes['version'] <> 1 then
    raise ENotSupportedException.Create('Invalid version ' + FXMLDocument.DocumentElement.Attributes['version']);

  RootNode := FXMLDocument.DocumentElement.ChildNodes.FindNode('root');

  AMetaEntityRoot.Name     := VarToStr(RootNode.Attributes['name']);
  AMetaEntityRoot.Version  := RootNode.Attributes['version'];

  ReadEntity(RootNode, AMetaEntityRoot);
end;
{$ENDREGION}

{$REGION 'TMetaWriterXML'}
constructor TMetaWriterXML.Create(const AXMLDocumentOptions: TXMLDocOptions);
begin
  inherited Create();

  FXMLDocumentOptions := AXMLDocumentOptions;
end;

function TMetaWriterXML.CreateXMLDocument: IXMLDocument;
var
  xml : TXMLDocument;
begin
  xml := TXMLDocument.Create(nil);
  Result := xml;
  xml.Options := FXMLDocumentOptions;

  xml.Active := True;

  xml.Version     := '1.0';
  xml.Encoding    := 'windows-1251';
  xml.StandAlone  := 'yes';
end;

procedure TMetaWriterXML.Write(const AMetaEntityRoot: TMetaEntityRoot);

  procedure WriteEntities(AXMLNode : IXMLNode; const AEntities : TMetaEntities); forward;

  procedure WriteEntity(AXMLNode : IXMLNode; const AEntity : TMetaEntity);
  var
    FieldsNode, FieldNode : IXMLNode;
    i : integer;
    MetaField : TMetaField;
  begin
    try
      AXMLNode.Attributes['rich'] := AEntity.RichDataClassName;

      FieldsNode := AXMLNode; //AXMLNode.AddChild('fields');

      for i := 0 to AEntity.Fields.Count - 1 do
      begin
        MetaField := AEntity.Fields[i];

        FieldNode := FieldsNode.AddChild(MetaField.FieldStrID);
        FieldNode.Attributes['name'] := MetaField.Name;

        if not MetaField.IsSimpleField then
          if MetaField.IsEntityField then
            WriteEntity(FieldNode, (MetaField as TMetaFieldEntity).Entity)

          else
          if MetaField.IsEntitiesField then
            WriteEntities(FieldNode, (MetaField as TMetaFieldEntities).Entities)

          else
            raise ENotSupportedException.Create(MetaField.ClassName);
      end;

    except
      EEntitiesError.CreateResFmt(@s_San_InternalErrorForWithDetailsFmt, [AXMLNode.NodeName, AXMLNode.XML]).RaiseOuter();
    end;
  end;

  procedure WriteEntities(AXMLNode : IXMLNode; const AEntities : TMetaEntities);
  var
    EntityNode : IXMLNode;
  begin
    try
      AXMLNode.Attributes['rich'] := AEntities.RichDataClassName;
      AXMLNode.Attributes['isLinks'] := BoolToStr(AEntities.IsLinks, True);
      AXMLNode.Attributes['isRecursive'] := BoolToStr(AEntities.IsRecursive, True);

      if not AEntities.IsRecursive then
      begin
        EntityNode := AXMLNode.AddChild('entity');
        WriteEntity(EntityNode, AEntities.ItemMeta);
      end;

    except
      EEntitiesError.CreateResFmt(@s_San_InternalErrorForWithDetailsFmt, [AXMLNode.NodeName, AXMLNode.XML]).RaiseOuter();
    end;
  end;

var
  MetaNode, RootNode : IXMLNode;
begin
  FXMLDocument := CreateXMLDocument;

  MetaNode := FXMLDocument.AddChild('meta');
  MetaNode.Attributes['version'] := 1;

  RootNode := MetaNode.AddChild('root');
  RootNode.Attributes['name']     := AMetaEntityRoot.Name;
  RootNode.Attributes['version']  := AMetaEntityRoot.Version;

  WriteEntity(RootNode, AMetaEntityRoot);
end;
{$ENDREGION}

{$REGION 'TDataReaderXML'}
constructor TDataReaderXML.Create(AXMLDocument: IXMLDocument);
begin
  inherited Create();

  FXMLDocument := AXMLDocument;
end;

procedure TDataReaderXML.Read(const ADataEntityRoot: TDataEntityRoot);

  procedure ReadEntities(AXMLNode : IXMLNode; const AEntities : TDataEntities); forward;

  procedure ReadEntity(AXMLNode : IXMLNode; const AEntity : TDataEntity);

    function GetFieldByNode(AXMLNode : IXMLNode) : TDataField;
    var
      MetaFieldClass : TMetaFieldClass;
      Name : string;
    begin
      if AnsiSameText(AXMLNode.NodeName, 'field')
      or TMetaField.FieldsByStrIDRegistry.TryGetValue(AXMLNode.NodeName, MetaFieldClass)
      and ((MetaFieldClass = TMetaFieldEntity) or (MetaFieldClass = TMetaFieldLinkEntity) or (MetaFieldClass = TMetaFieldEntities))
      then
      begin
        Name := VarToStr(AXMLNode.Attributes['name']);
        Result := AEntity.Fields[Name];
      end
      else
        raise ENotSupportedException.Create(AXMLNode.NodeName);
    end;

  var
    FieldsNode, FieldNode : IXMLNode;
    i : integer;
    DataField : TDataField;
    Value : OleVariant;
  begin
    try
      FieldsNode := AXMLNode; //AXMLNode.ChildNodes.FindNode('fields');

      for i := 0 to FieldsNode.ChildNodes.Count - 1 do
      begin
        FieldNode := FieldsNode.ChildNodes[i];

        try
          DataField := GetFieldByNode(FieldNode);

          if DataField.Meta.IsSimpleField then
          begin
            Value := FieldNode.Attributes['value'];
            DataField.Value := Value;
          end

          else
          if DataField.Meta.IsEntityField then
            ReadEntity(FieldNode, DataField.AsEntity)

          else
          if DataField.Meta.IsEntitiesField then
            ReadEntities(FieldNode, DataField.AsItems)

          else
          if DataField.Meta.IsLinkField then
            raise ENotSupportedException.Create(FieldNode.NodeName)

          else
            raise ENotSupportedException.Create(FieldNode.NodeName);
        except
          EEntitiesError.CreateResFmt(@s_San_InternalErrorForWithDetailsFmt, [FieldNode.NodeName, FieldNode.XML]).RaiseOuter();
        end;
      end;

    except
      EEntitiesError.CreateResFmt(@s_San_InternalErrorForWithDetailsFmt, [AXMLNode.NodeName, AXMLNode.XML]).RaiseOuter();
    end;
  end;

  procedure ReadEntities(AXMLNode : IXMLNode; const AEntities : TDataEntities);
  var
    i : integer;
    ChildNode : IXMLNode;
  begin
    try

      for i := 0 to AXMLNode.ChildNodes.Count - 1 do
      begin
        ChildNode := AXMLNode.ChildNodes[i];

        if AnsiSameText(ChildNode.NodeName, TMetaFieldEntity.FieldStrID) then
          ReadEntity(ChildNode, AEntities.Add());
      end;

    except
      EEntitiesError.CreateResFmt(@s_San_InternalErrorForWithDetailsFmt, [AXMLNode.NodeName, AXMLNode.XML]).RaiseOuter();
    end;
  end;

var
  RootNode : IXMLNode;
begin
  if FXMLDocument.DocumentElement.Attributes['version'] <> 1 then
    raise ENotSupportedException.Create('Invalid version ' + FXMLDocument.DocumentElement.Attributes['version']);

  RootNode := FXMLDocument.DocumentElement.ChildNodes.FindNode('root');

  ADataEntityRoot.Version  := RootNode.Attributes['version'];

  ReadEntity(RootNode, ADataEntityRoot);
end;
{$ENDREGION}

{$REGION 'TDataWriterXML'}
constructor TDataWriterXML.Create(const AXMLDocumentOptions: TXMLDocOptions);
begin
  inherited Create();

  FXMLDocumentOptions := AXMLDocumentOptions;
end;

function TDataWriterXML.CreateXMLDocument: IXMLDocument;
var
  xml : TXMLDocument;
begin
  xml := TXMLDocument.Create(nil);
  Result := xml;
  xml.Options := FXMLDocumentOptions;

  xml.Active := True;

  xml.Version     := '1.0';
  xml.Encoding    := 'windows-1251';
  xml.StandAlone  := 'yes';
end;

procedure TDataWriterXML.Write(const ADataEntityRoot: TDataEntityRoot);
begin
  FXMLDocument := CreateXMLDocument;

  raise ENotImplemented.Create(ClassName + '.Write');
end;
{$ENDREGION}

{$REGION 'Api.TMeta'}
class procedure Api.TMeta.ReadFromFile(const AMetaEntityRoot: TMetaEntityRoot;
  const AFileName: string);
var
  Reader : TMetaReaderXML;
begin
  Reader := TMetaReaderXML.Create(LoadXMLDocument(AFileName));
  try
    Reader.Read(AMetaEntityRoot);
  finally
    FreeAndNil(Reader);
  end;
end;

class procedure Api.TMeta.ReadFromStr(const AMetaEntityRoot: TMetaEntityRoot;
  const AXMLData: string);
var
  Reader : TMetaReaderXML;
begin
  Reader := TMetaReaderXML.Create(LoadXMLData(AXMLData));
  try
    Reader.Read(AMetaEntityRoot);
  finally
    FreeAndNil(Reader);
  end;
end;

class procedure Api.TMeta.WriteToFile(const AMetaEntityRoot: TMetaEntityRoot;
  const AFileName: string);
var
  Writer : TMetaWriterXML;
begin
  Writer := TMetaWriterXML.Create();
  try
    Writer.Write(AMetaEntityRoot);
    Writer.XMLDocument.SaveToFile(AFileName);
  finally
    FreeAndNil(Writer);
  end;
end;

class function Api.TMeta.WriteToStr(
  const AMetaEntityRoot: TMetaEntityRoot): string;
var
  Writer : TMetaWriterXML;
begin
  Writer := TMetaWriterXML.Create();
  try
    Writer.Write(AMetaEntityRoot);
    Result := Writer.XMLDocument.XML.Text;
  finally
    FreeAndNil(Writer);
  end;
end;
{$ENDREGION}

{$REGION 'Api.TData'}
class procedure Api.TData.ReadFromFile(const ADataEntityRoot: TDataEntityRoot;
  const AFileName: string);
var
  Reader : TDataReaderXML;
begin
  Reader := TDataReaderXML.Create(LoadXMLDocument(AFileName));
  try
    Reader.Read(ADataEntityRoot);
  finally
    FreeAndNil(Reader);
  end;
end;

class procedure Api.TData.ReadFromStr(const ADataEntityRoot: TDataEntityRoot;
  const AXMLData: string);
var
  Reader : TDataReaderXML;
begin
  Reader := TDataReaderXML.Create(LoadXMLData(AXMLData));
  try
    Reader.Read(ADataEntityRoot);
  finally
    FreeAndNil(Reader);
  end;
end;

class procedure Api.TData.WriteToFile(const ADataEntityRoot: TDataEntityRoot;
  const AFileName: string);
var
  Writer : TDataWriterXML;
begin
  Writer := TDataWriterXML.Create();
  try
    Writer.Write(ADataEntityRoot);
    Writer.XMLDocument.SaveToFile(AFileName);
  finally
    FreeAndNil(Writer);
  end;
end;

class function Api.TData.WriteToStr(
  const ADataEntityRoot: TDataEntityRoot): string;
var
  Writer : TDataWriterXML;
begin
  Writer := TDataWriterXML.Create();
  try
    Writer.Write(ADataEntityRoot);
    Result := Writer.XMLDocument.XML.Text;
  finally
    FreeAndNil(Writer);
  end;
end;
{$ENDREGION}

{$REGION 'Api'}
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
