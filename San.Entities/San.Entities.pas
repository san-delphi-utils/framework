{*******************************************************}
{                                                       }
{       Динамическая структура данных и метаданных      }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2017-2018         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Динамическая структура данных и метаданных
/// </summary>
unit San.Entities;
{$I San.Entities.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Classes, System.Variants, System.Generics.Collections,
  System.Generics.Defaults, System.TypInfo, System.Rtti, System.Types,
  System.SyncObjs, System.StrUtils, System.Math, System.JSON,
  {$ENDREGION}

  {$REGION 'San'}
  San, San.UpperString, San.Intfs.Basic, San.Events, San.Bool3,
  San.Entities.Consts
  {$ENDREGION}

;
{$ENDREGION}

const
  /// <summary>
  ///   Разделитель путей
  /// </summary>
  PATH_SEPARATOR                  = '.';

  /// <summary>
  ///   Указание на корень в пути
  /// </summary>
  PATH_ROOT                       = '@root';

  /// <summary>
  ///   Указание на родителя в пути
  /// </summary>
  PATH_PARENT                     = '@parent';

  /// <summary>
  ///   Указание на текущий узел в пути
  /// </summary>
  PATH_THIS                       = '@this';

  /// <summary>
  ///   Указание на метасущность в коллекции
  /// </summary>
  PATH_META_ENTITY_IN_COLLECTION  = '[]';

type
  {$REGION 'Forwards'}
    TEntitiesObject = class;

    {$REGION 'Meta'}
    TMetaObject      = class;
    TMetaNode        = class;
    TMetaField       = class;
    TMetaEntity      = class;
    TMetaCollection  = class;
    TMetaEntityRoot  = class;
    {$ENDREGION}

    {$REGION 'Data'}
    TMEEntities                        = class;

    TDataObjects                       = class;
    TDataObject                        = class;
    TDataNode                          = class;
    TDataField                         = class;

    TDataFieldInteger                  = class;
    TDataFieldInt64                    = class;
    TDataFieldString                   = class;
    TDataFieldChar                     = class;
    TDataFieldExtended                 = class;
    TDataFieldDouble                   = class;
    TDataFieldDateTime                 = class;
    TDataFieldDate                     = class;
    TDataFieldTime                     = class;
    TDataFieldBoolean                  = class;
    TDataFieldGUID                     = class;
    TDataFieldEntity                   = class;
    TDataFieldCollection               = class;

    TDataNodeEntityOrCollection        = class;
    TDataEntity                        = class;
    TDataCollectionIndex               = class;
    TDataCollectionIndexSingle         = class;

    TDataCollectionIndexSingleInteger  = class;
    TDataCollectionIndexSingleInt64    = class;
    TDataCollectionIndexSingleString   = class;
    TDataCollectionIndexSingleGUID     = class;

    TDataCollection                    = class;
    TDataEntityRoot                    = class;
    {$ENDREGION}

  {$ENDREGION}

{$ELSE}
type
{$ENDIF}

  {$REGION 'classes'}
  /// <summary>
  ///   Класс узла метаданных
  /// </summary>
  TMetaNodeClass =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaNodeClass;
  {$ELSE}
    class of TMetaNode;
  {$ENDIF}

  /// <summary>
  ///   Класс поля метаданных
  /// </summary>
  TMetaFieldClass =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldClass;
  {$ELSE}
    class of TMetaField;
  {$ENDIF}

  /// <summary>
  ///   Класс индекса метаданных
  /// </summary>
  TMetaCollectionIndexClass =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaCollectionIndexClass;
  {$ELSE}
    class of TMetaCollectionIndex;
  {$ENDIF}

  /// <summary>
  ///   Класс объекта данных
  /// </summary>
  TDataObjectClass =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataObjectClass;
  {$ELSE}
    class of TDataObject;
  {$ENDIF}

  /// <summary>
  ///   Класс узла данных
  /// </summary>
  TDataNodeClass =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataNodeClass;
  {$ELSE}
    class of TDataNode;
  {$ENDIF}

  /// <summary>
  ///   Класс поля данных
  /// </summary>
  TDataFieldClass =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldClass;
  {$ELSE}
    class of TDataField;
  {$ENDIF}

  /// <summary>
  ///   Класс данных индекса списка сущностей по одному полю
  /// </summary>
  TDataCollectionIndexSingleClass =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataCollectionIndexSingleClass;
  {$ELSE}
    class of TDataCollectionIndexSingle;
  {$ENDIF}

  /// <summary>
  ///   Класс сущности данных
  /// </summary>
  TDataEntityClass =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataEntityClass;
  {$ELSE}
    class of TDataEntity;
  {$ENDIF}

    /// <summary>
  ///   Класс коллекции данных
  /// </summary>
  TDataCollectionClass =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataCollectionClass;
  {$ELSE}
    class of TDataCollection;
  {$ENDIF}
  {$ENDREGION}

  {$REGION 'exception'}

  /// <summary>
  ///   Базовое исключение сущностей
  /// </summary>
  EEntities =
  {$IFDEF USE_ALIASES}
    San.Entities.EEntities;
  {$ELSE}
    class(ESanBasic)
  end;
  {$ENDIF}

  /// <summary>
  ///   Базовая ошибка сущностей
  /// </summary>
  EEntitiesError =
  {$IFDEF USE_ALIASES}
    San.Entities.EEntitiesError;
  {$ELSE}
    class(EEntities)
  end;
  {$ENDIF}

  /// <summary>
  ///   Ошибка изменения метаданных при существующих данных
  /// </summary>
  EEntitiesMetaChangeForbidden =
  {$IFDEF USE_ALIASES}
    San.Entities.EEntitiesMetaChangeForbidden;
  {$ELSE}
    class(EEntitiesError)
  end;
  {$ENDIF}

  /// <summary>
  ///   Ошибка наследования метаданных
  /// </summary>
  EEntitiesMetaInheritance =
  {$IFDEF USE_ALIASES}
    San.Entities.EEntitiesMetaInheritance;
  {$ELSE}
    class(EEntitiesError)
  end;
  {$ENDIF}

  /// <summary>
  ///   Ошибка разрешения пути
  /// </summary>
  EEntitiesResolvingError =
  {$IFDEF USE_ALIASES}
    San.Entities.EEntitiesResolvingError;
  {$ELSE}
    class(EEntitiesError)
  end;
  {$ENDIF}

  /// <summary>
  ///   Ошибка поиска
  /// </summary>
  EEntitiesFinderError =
  {$IFDEF USE_ALIASES}
    San.Entities.EEntitiesFinderError;
  {$ELSE}
    class(EEntitiesError)
  end;
  {$ENDIF}

  /// <summary>
  ///   Ошибка чтения ридером или записи райтером сущностей
  /// </summary>
  EEntitiesRWError =
  {$IFDEF USE_ALIASES}
    San.Entities.EEntitiesRWError;
  {$ELSE}
    class(EEntitiesError)
  end;
  {$ENDIF}

  {$ENDREGION}

  {$REGION 'simple'}
  /// <summary>
  ///   Тип узла
  /// </summary>
  TEntitiesNodeType =
  {$IFDEF USE_ALIASES}
    San.Entities.TEntitiesNodeType;
  {$ELSE}
    (enntField, enntEntity, enntCollection);
  {$ENDIF}

  TEntitiesNodeTypeHelper = record helper for TEntitiesNodeType
  public
    function ToString() : string;
  end;

  /// <summary>
  ///   Тип события
  /// </summary>
  TEntitiesEventKind =
  {$IFDEF USE_ALIASES}
    San.Entities.TEntitiesEventKind;
  {$ELSE}
    (enevkUnknown, enevkInsert, enevkDelete, enevkMove, enevkChange);
  {$ENDIF}

  TEntitiesEventKindHelper = record helper for TEntitiesEventKind
  public
    function ToString() : string;
  end;

  /// <summary>
  ///   Тип сравнения
  /// </summary>
  TEntitiesCompareOperation =
  {$IFDEF USE_ALIASES}
    San.Entities.TEntitiesCompareOperation;
  {$ELSE}
    (
      ecoUnknown,
      ecoNotEquals,
      ecoLessThan,
      ecoLessThanOrEquals,
      ecoEquals,
      ecoGreaterThanOrEquals,
      ecoGreaterThan
    );
  {$ENDIF}

  TEntitiesCompareOperationHelper = record helper for TEntitiesCompareOperation
  public
    function ToString() : string;

    /// <summary>
    ///   Перевернуть слева на право
    /// </summary>
    function Flip() : TEntitiesCompareOperation;
  end;
  {$ENDREGION}

  {$REGION 'events'}
  /// <summary>
  ///   Базовая информация о событии
  /// </summary>
  TEntitiesEvent =
  {$IFDEF USE_ALIASES}
    San.Entities.TEntitiesEvent;
  {$ELSE}
    class abstract (TObject)
  strict private
    FSource  : TEntitiesObject;

  protected
    function GetKind() : TEntitiesEventKind; virtual; abstract;

  public
    constructor Create(const ASource  : TEntitiesObject);

    /// <summary>
    ///   Объект-источник события
    /// </summary>
    property Source  : TEntitiesObject  read FSource;

    /// <summary>
    ///   Тип события
    /// </summary>
    property Kind    : TEntitiesEventKind  read GetKind;
  end;
  {$ENDIF}

  /// <summary>
  ///   Информация о событии вставки
  /// </summary>
  TEntitiesInsertEvent =
  {$IFDEF USE_ALIASES}
    San.Entities.TEntitiesInsertEvent;
  {$ELSE}
    class(TEntitiesEvent)
  private
    FItem  : TEntitiesObject;
    FIndex  : integer;

  protected
    function GetKind() : TEntitiesEventKind; override;

  public
    constructor Create(const ASource  : TEntitiesObject; const AItem  : TEntitiesObject;
      const AIndex  : integer);

    /// <summary>
    ///   Добавляемый элемент
    /// </summary>
    property Item : TEntitiesObject  read FItem;

    /// <summary>
    ///   Индекс добавляемого элемента
    /// </summary>
    property Index : integer  read FIndex;
  end;
  {$ENDIF}

  /// <summary>
  ///   Информация о событии удаления
  /// </summary>
  TEntitiesDeleteEvent =
  {$IFDEF USE_ALIASES}
    San.Entities.TEntitiesDeleteEvent;
  {$ELSE}
    class(TEntitiesEvent)
  private
    FItem  : TEntitiesObject;
    FIndex  : integer;

  protected
    function GetKind() : TEntitiesEventKind; override;

  public
    constructor Create(const ASource  : TEntitiesObject; const AItem  : TEntitiesObject;
      const AIndex  : integer);

    /// <summary>
    ///   Проверить, совпадает ли указатель на объект с удаляемым объектом
    /// </summary>
    function IsDeletedItem(const AItem  : TEntitiesObject) : boolean;

    /// <summary>
    ///   Индекс удаленного элемента
    /// </summary>
    property Index : integer  read FIndex;
  end;
  {$ENDIF}

  /// <summary>
  ///   Информация о событии перемещения
  /// </summary>
  TEntitiesMoveEvent =
  {$IFDEF USE_ALIASES}
    San.Entities.TEntitiesMoveEvent;
  {$ELSE}
    class(TEntitiesEvent)
  private
    FItem  : TEntitiesObject;
    FOldIndex, FNewIndex : integer;

  protected
    function GetKind() : TEntitiesEventKind; override;

  public
    constructor Create(const ASource  : TEntitiesObject; const AItem  : TEntitiesObject;
      const AOldIndex, ANewIndex : integer);

    /// <summary>
    ///   Перемещенный элемента Объект-источник события
    /// </summary>
    property Item : TEntitiesObject  read FItem;

    /// <summary>
    ///   Старый индекс элемента
    /// </summary>
    property OldIndex : integer  read FOldIndex;

    /// <summary>
    ///   Новый индекс элемента
    /// </summary>
    property NewIndex : integer  read FNewIndex;
  end;
  {$ENDIF}

  /// <summary>
  ///   Информация о событии изменения
  /// </summary>
  TEntitiesChangeEvent =
  {$IFDEF USE_ALIASES}
    San.Entities.TEntitiesChangeEvent;
  {$ELSE}
    class(TEntitiesEvent)
  private
    FField  : TDataField;
    FOldValue, FNewValue : TValue;

  protected
    function GetKind() : TEntitiesEventKind; override;

  public
    constructor Create(const ASource  : TEntitiesObject; const AField  : TDataField;
      const AOldValue, ANewValue : TValue);

    /// <summary>
    ///   Изменяемое поле
    /// </summary>
    property Field  : TDataField  read FField;

    /// <summary>
    ///   Старое значение
    /// </summary>
    property OldValue : TValue  read FOldValue;

    /// <summary>
    ///   Новое значение
    /// </summary>
    property NewValue : TValue  read FNewValue;
  end;
  {$ENDIF}

  /// <summary>
  ///   Событие изменения данных. Атрибут события
  /// </summary>
  EvMEEntities =
  {$IFDEF USE_ALIASES}
    San.Entities.EvMEDataChange;
  {$ELSE}
    class(TMetaEvent.Subtypes.SubscribeAttribute);
  {$ENDIF}

  /// <summary>
  ///   Событие изменения данных
  /// </summary>
  TMEEntities =
  {$IFDEF USE_ALIASES}
    San.Entities.TMEEntities;
  {$ELSE}
    class(TMetaEventSeparate)
  protected
    function MatchSubscribeAttr(const AAttr : TMetaEvent.Subtypes.SubscribeAttribute) : boolean; override;

  public
    {$REGION 'types'}
    type
      /// <summary>
      ///   Контекст события данных
      /// </summary>
      Context = class(TObject)
      private
        FEvents : TArray<TEntitiesEvent>;

      public
        constructor Create(const AEvents : TArray<TEntitiesEvent>);

        /// <summary>
        ///   Попытка получить событие для конкретного поля
        /// </summary>
        function TryGet(const AFieldName : string; out AEvent : TEntitiesChangeEvent) : boolean; overload;

        /// <summary>
        ///   Попытка получить события для конкретного поля
        /// </summary>
        function TryGet(const AFieldName : string; out AEvents : TArray<TEntitiesChangeEvent>) : boolean; overload;

        /// <summary>
        ///   Попытка получить событие для конкретных полей
        /// </summary>
        function TryGet(const AFieldNames : array of string; out AEvent : TEntitiesChangeEvent) : boolean; overload;

        /// <summary>
        ///   Попытка получить события для конкретных полей
        /// </summary>
        function TryGet(const AFieldNames : array of string; out AEvents : TArray<TEntitiesChangeEvent>) : boolean; overload;

        /// <summary>
        ///   Произошедшие события
        /// </summary>
        property Events : TArray<TEntitiesEvent>  read FEvents;
      end;

      Event = TMetaEvent.Event<TMEEntities.Context>;
    {$ENDREGION}

    class function EventName : string; override;
    class function Description : string; override;

    function Fire(const ASender : TObject; const AEvents : TArray<TEntitiesEvent>) : TMetaEvent.Subtypes.TFiring;
  end;
  {$ENDIF}

  {$ENDREGION}

  {$REGION 'base'}
  /// <summary>
  ///   Перечислитель массива
  /// </summary>
  TArrayEnumerator<T> = class
  strict private
    FArray : TArray<T>;
    FIndex: Integer;
    function GetCurrent: T;

  public
    constructor Create(const AArray : TArray<T>);

    property Current: T read GetCurrent;
    function MoveNext: Boolean;
  end;

  /// <summary>
  ///   Делегат перечисления
  /// </summary>
  TEnumeratorRef<T> = reference to function (out AItem : T; const ARestart : boolean) : boolean;

  /// <summary>
  ///   Разрешитель путей
  /// </summary>
  TPathResolver =
  {$IFDEF USE_ALIASES}
    San.Entities.TPathResolver;
  {$ELSE}
    record
  public type
    {$REGION 'types'}
    /// <summary>
    ///   Разновидности лексем
    /// </summary>
    TLexemeKind =
      (
        lxkUnknown,              // Изначальное состояние. Не должно встречвться!

        lxkSysIdentifierRoot,    // @root
        lxkSysIdentifierParent,  // @parent
        lxkSysIdentifierThis,    // @this
        lxkDot,                  // .
        lxkSquareBracketOpen,    // [
        lxkSquareBracketClose,   // ]
        lxkRoundBracketOpen,     // (
        lxkRoundBracketClose,    // )
        lxkNot,                  // !
        lxkMinus,                // -
        lxkNotEquals,            // !=
        lxkLessThan,             // <
        lxkLessThanOrEquals,     // <=
        lxkEquals,               // ==
        lxkGreaterThanOrEquals,  // >=
        lxkGreaterThan,          // >
        lxkAnd,                  // &&
        lxkOr,                   // ||
        lxkNumber,               // Число[0-9]+
        lxkString,               // Строка '.*'
        lxkIdentifier            // Строка [A-Za-z_][A-Za-z_0-9]* <MyName>
      );

    TLexemeKindHelper = record helper for TLexemeKind
    public
      function ToString() : string;
      function ToCompareKind() : TEntitiesCompareOperation;
    end;

    /// <summary>
    ///   Множество разновидностей лексем
    /// </summary>
    TLexemesKinds = set of TLexemeKind;

    PLexeme = ^TLexeme;

    /// <summary>
    ///   Лексема
    /// </summary>
    TLexeme = record
    strict private
      FKind : TLexemeKind;
      FStart, FLength : integer;
      private
        function GetValue(const AText: string): string;

    public
      class operator Equal(const A, B : TLexeme) : boolean;
      class operator NotEqual(const A, B : TLexeme) : boolean;
      class operator Equal(const A : TLexeme; const B : TLexemeKind) : boolean;
      class operator NotEqual(const A : TLexeme; const B : TLexemeKind) : boolean;
      class operator Implicit(const A : TLexeme) :  TLexemeKind;
      class operator In(const A : TLexeme; const B: TLexemesKinds) : Boolean;

      constructor Create(const AKind : TLexemeKind; const AStart, ALength : integer);

      /// <summary>
      ///   Разновидности лексемы
      /// </summary>
      property Kind    : TLexemeKind  read FKind;

      /// <summary>
      ///   Начало в тексте
      /// </summary>
      property Start   : integer      read FStart;

      /// <summary>
      ///   Длина
      /// </summary>
      property Length  : integer      read FLength;

      /// <summary>
      ///   Значение лексемы в указанном тексте
      /// </summary>
      property Value[const AText : string] : string  read GetValue;
    end;

    /// <summary>
    ///   Элемент пути
    /// </summary>
    TPathItem = class abstract (TObject)
    strict private
      function GetUltimateChild: TPathItem;

    private
      FOwner: TObject;
      FChild: TPathItem;

    protected
      procedure DoIsTargetStatic(var AResult : Bool3); virtual;
      function GetCanHasMultiTargets : boolean; virtual;
      function GetTargetMeta(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TMetaNode): TMetaNode; virtual; abstract;
      function GetTargetData(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TDataNode): TDataNode; virtual; abstract;
      function GetTargetsData(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TDataNode): TArray<TDataNode>; virtual;

    public
      constructor Create(const AOwner : TObject);
      destructor Destroy; override;

      /// <summary>
      ///   Весь путь со всеми дочерними элементами не зависит от контекста <c>AContext</c>
      /// </summary>
      /// <returns>
      ///   <para>
      ///     <c>False3</c>  — зависит
      ///   </para>
      ///   <para>
      ///     <c>Null3</c>   — не известно
      ///   </para>
      ///   <para>
      ///     <c>True3</c>   — не зависит
      ///   </para>
      /// </returns>
      function IsEndTargetStatic : Bool3;

      /// <summary>
      ///   Найти конечную цель пути метаданных
      /// </summary>
      function FindEndTargetMeta(const ANodeType : TEntitiesNodeType;
        const AContext, AThis : TMetaNode) : TMetaNode;

      /// <summary>
      ///   Найти конечную цель пути данных
      /// </summary>
      function FindEndTargetData(const ANodeType : TEntitiesNodeType;
        const AContext, AThis : TDataNode) : TDataNode;

      /// <summary>
      ///   Найти конечные цели пути данных
      /// </summary>
      function FindEndTargetsData(const ANodeType : TEntitiesNodeType;
        const AContext, AThis : TDataNode) : TArray<TDataNode>;

      /// <summary>
      ///   Владеющий объект
      /// </summary>
      property Owner : TObject  read FOwner;

      /// <summary>
      ///   Дочерний элемент
      /// </summary>
      property Child : TPathItem  read FChild;

      /// <summary>
      ///   Последний дочерний элемент
      /// </summary>
      property UltimateChild : TPathItem  read GetUltimateChild;

      /// <summary>
      ///   Может имет несколько целей
      /// </summary>
      property CanHasMultiTargets : boolean  read GetCanHasMultiTargets;

      /// <summary>
      ///   Целевой объект метаданных
      /// </summary>
      property TargetMeta[const ANodeType : TEntitiesNodeType;
        const AContext, AThis : TMetaNode] : TMetaNode  read GetTargetMeta;

      /// <summary>
      ///   Целевой объект данных
      /// </summary>
      property TargetData[const ANodeType : TEntitiesNodeType;
        const AContext, AThis : TDataNode] : TDataNode  read GetTargetData;

      /// <summary>
      ///   Целевые объекты данных
      /// </summary>
      property TargetsData[const ANodeType : TEntitiesNodeType;
        const AContext, AThis : TDataNode] : TArray<TDataNode>  read GetTargetsData;
    end;

    /// <summary>
    ///   Элемент пути: @root
    /// </summary>
    TPathItemSysRoot = class (TPathItem)
    protected
      procedure DoIsTargetStatic(var AResult : Bool3); override;
      function GetTargetMeta(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TMetaNode): TMetaNode; override;
      function GetTargetData(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TDataNode): TDataNode; override;
    end;

    /// <summary>
    ///   Элемент пути: @parent
    /// </summary>
    TPathItemSysParent = class (TPathItem)
    protected
      procedure DoIsTargetStatic(var AResult : Bool3); override;
      function GetTargetMeta(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TMetaNode): TMetaNode; override;
      function GetTargetData(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TDataNode): TDataNode; override;
    end;

    /// <summary>
    ///   Элемент пути: @this
    /// </summary>
    TPathItemSysThis = class (TPathItem)
    protected
      procedure DoIsTargetStatic(var AResult : Bool3); override;
      function GetTargetMeta(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TMetaNode): TMetaNode; override;
      function GetTargetData(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TDataNode): TDataNode; override;
    end;

    /// <summary>
    ///   Элемент пути: фиксированное значение
    /// </summary>
    TPathItemStatic = class abstract (TPathItem)
    protected
      procedure DoIsTargetStatic(var AResult : Bool3); override;
      function GetTargetMeta(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TMetaNode): TMetaNode; override;
      function GetTargetData(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TDataNode): TDataNode; override;
      function GetValue : TValue; virtual; abstract;

    public
      /// <summary>
      ///   Сравнение двух статических значений
      /// </summary>
      function CompareDataTo(const AOther : TPathItemStatic; const ACompareOperation : TEntitiesCompareOperation) : Bool3; virtual; abstract;

      /// <summary>
      ///   Фиксированное значение
      /// </summary>
      property Value : TValue  read GetValue;
    end;

    /// <summary>
    ///   Элемент пути: фиксированное значение. Генерик
    /// </summary>
    TPathItemStatic<T> = class (TPathItemStatic)
    private
      FValue : T;

    protected
      function GetValue : TValue; override;

    public
      function CompareDataTo(const AOther : TPathItemStatic; const ACompareOperation : TEntitiesCompareOperation) : Bool3; override;

      /// <summary>
      ///   Фиксированное значение
      /// </summary>
      property Value : T  read FValue;
    end;

    /// <summary>
    ///   Элемент пути: фиксированное значение Int64
    /// </summary>
    TPathItemInt64 = class(TPathItemStatic<Int64>);

    /// <summary>
    ///   Элемент пути: фиксированное значение Extended
    /// </summary>
    TPathItemExtended = class(TPathItemStatic<Extended>);

    /// <summary>
    ///   Элемент пути: фиксированное значение String
    /// </summary>
    TPathItemString = class(TPathItemStatic<string>);

    /// <summary>
    ///   Элемент пути: идентификатор
    /// </summary>
    TPathItemIdentifier = class (TPathItem)
    private
      FName : string;

    protected
      function GetTargetMeta(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TMetaNode): TMetaNode; override;
      function GetTargetData(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TDataNode): TDataNode; override;

    public
      /// <summary>
      ///   Имя идентификатора
      /// </summary>
      property Name : string  read FName;
    end;

    /// <summary>
    ///   Условие фильтрации
    /// </summary>
    TPathFilterCondition = class abstract (TObject)
    private
      FIsNegative : boolean;

    strict protected
      /// <summary>
      ///   Проверка условия
      /// </summary>
      function DoCheck(const AContext, AThis: TDataNode) : boolean; virtual; abstract;

    protected
      /// <summary>
      ///   Получить негативный результат
      /// </summary>
      /// <param name="ACollection">
      ///   Фильтруемая коллекция
      /// </param>
      /// <param name="APositiveResults">
      ///   Положительные результаты
      /// </param>
      function Negative(const ACollection : TDataCollection; const APositiveResults : TArray<TDataNode>) : TArray<TDataNode>;

      /// <summary>
      ///   Фильтрация с перебором всей коллекции и проверкой данным условием
      /// </summary>
      function FilterByCheckAll(const ACollection : TDataCollection; const AThis: TDataNode) : TArray<TDataNode>;

      /// <summary>
      ///   Фильтрация условием
      /// </summary>
      /// <param name="ACollection">
      ///   Фильтруемая коллекция
      /// </param>
      /// <param name="AThis">
      ///   Узел, запрашивающий фильтрацию
      /// </param>
      function DoFilter(const ACollection : TDataCollection; const AThis: TDataNode) : TArray<TDataNode>; virtual; abstract;

    public
      /// <summary>
      ///   Все внутренности условия не зависит от контекста <c>AContext</c>
      /// </summary>
      /// <returns>
      ///   <para>
      ///     <c>False3</c>  — зависит
      ///   </para>
      ///   <para>
      ///     <c>Null3</c>   — не известно
      ///   </para>
      ///   <para>
      ///     <c>True3</c>   — не зависит
      ///   </para>
      /// </returns>
      function IsStatic : Bool3; virtual;

      /// <summary>
      ///   Проверка условия
      /// </summary>
      function Check(const AContext, AThis: TDataNode) : boolean;

      /// <summary>
      ///   Фильтрация условием
      /// </summary>
      function Filter(const ACollection : TDataCollection; const AThis: TDataNode) : TArray<TDataNode>;

      /// <summary>
      ///   Негативное условие. NOT
      /// </summary>
      property IsNegative : boolean  read FIsNegative;

    end;

    /// <summary>
    ///   Условие фильтрации: набор из подусловий объединенных одной логической операцией
    /// </summary>
    TPathFilterMultiCondition = class abstract (TPathFilterCondition)
    private
      FSubConditions : TArray<TPathFilterCondition>;

    protected

    public
      constructor Create(const ASubConditions : array of TPathFilterCondition);
      destructor Destroy; override;

      function IsStatic : Bool3; override;

      /// <summary>
      ///   Подусловия
      /// </summary>
      property SubConditions : TArray<TPathFilterCondition>  read FSubConditions;
    end;

    /// <summary>
    ///   Условие фильтрации: логическое И
    /// </summary>
    TPathFilterAndCondition = class (TPathFilterMultiCondition)
    strict protected
      function DoCheck(const AContext, AThis: TDataNode) : boolean; override;

    protected
      function DoFilter(const ACollection : TDataCollection; const AThis: TDataNode) : TArray<TDataNode>; override;

    end;

    /// <summary>
    ///   Условие фильтрации: логическое ИЛИ
    /// </summary>
    TPathFilterOrCondition = class (TPathFilterMultiCondition)
    strict protected
      function DoCheck(const AContext, AThis: TDataNode) : boolean; override;

    protected
      function DoFilter(const ACollection : TDataCollection; const AThis: TDataNode) : TArray<TDataNode>; override;

    end;

    /// <summary>
    ///   Условие фильтрации проверяющее логическое значение поля
    /// </summary>
    TPathFilterBoolFieldCheckCondition = class (TPathFilterCondition)
    strict private

    private
      FFieldPath : TPathItem;
      function GetDataField(const AContext, AThis: TDataNode): TDataField;
      function GetMetaField(const AContext, AThis: TMetaNode): TMetaField;

    strict protected
      function DoCheck(const AContext, AThis: TDataNode) : boolean; override;

    protected
      function DoFilter(const ACollection : TDataCollection; const AThis: TDataNode) : TArray<TDataNode>; override;

    public
      constructor Create(const AFieldPath : TPathItem);
      destructor Destroy; override;

      function IsStatic : Bool3; override;

      /// <summary>
      ///   Путь к проверяемому полю
      /// </summary>
      property FieldPath : TPathItem  read FFieldPath;

      /// <summary>
      ///   Поле, на которое указывает правая часть выражения
      /// </summary>
      property MetaField[const AContext, AThis: TMetaNode] : TMetaField  read GetMetaField;

      /// <summary>
      ///   Поле, на которое указывает правая часть выражения
      /// </summary>
      property DataField[const AContext, AThis: TDataNode] : TDataField  read GetDataField;
    end;

    /// <summary>
    ///   Условие фильтрации сравнивающее два поля
    /// </summary>
    TPathFilterFieldsCompareCondition = class (TPathFilterCondition)
    strict private
      function GetLeftMetaField(const AContext, AThis: TMetaNode): TMetaField;
      function GetRightMetaField(const AContext, AThis: TMetaNode): TMetaField;

      function GetLeftDataField(const AContext, AThis: TDataNode): TDataField;
      function GetRightDataField(const AContext, AThis: TDataNode): TDataField;

    private
      FLeft, FRight : TPathItem;
      FCompareOperation : TEntitiesCompareOperation;

    strict protected
      function DoCheck(const AContext, AThis: TDataNode) : boolean; override;

    protected
      class function GetMetaField(const APathItem : TPathItem; const AContext, AThis: TMetaNode;
        const AErrMsg : PResStringRec): TMetaField; static;
      class function GetDataField(const APathItem : TPathItem; const AContext, AThis: TDataNode;
        const AErrMsg : PResStringRec): TDataField; static;

      function DoFilter(const ACollection : TDataCollection; const AThis: TDataNode) : TArray<TDataNode>; override;

    public
      constructor Create(const ALeft, ARight : TPathItem; const ACompareOperation : TEntitiesCompareOperation);
      destructor Destroy; override;

      function IsStatic : Bool3; override;

      /// <summary>
      ///   Левая часть для сравнения
      /// </summary>
      property Left : TPathItem  read FLeft;

      /// <summary>
      ///   Правая часть для сравнения
      /// </summary>
      property Right : TPathItem  read FRight;

      /// <summary>
      ///   Поле, на которое указывает левая часть выражения
      /// </summary>
      property LeftMetaField[const AContext, AThis: TMetaNode] : TMetaField  read GetLeftMetaField;

      /// <summary>
      ///   Поле, на которое указывает правая часть выражения
      /// </summary>
      property RightMetaField[const AContext, AThis: TMetaNode] : TMetaField  read GetRightMetaField;

      /// <summary>
      ///   Поле, на которое указывает левая часть выражения
      /// </summary>
      property LeftDataField[const AContext, AThis: TDataNode] : TDataField  read GetLeftDataField;

      /// <summary>
      ///   Поле, на которое указывает правая часть выражения
      /// </summary>
      property RightDataField[const AContext, AThis: TDataNode] : TDataField  read GetRightDataField;

      /// <summary>
      ///   Операция сравнения
      /// </summary>
      property CompareOperation : TEntitiesCompareOperation  read FCompareOperation;
    end;

    /// <summary>
    ///   Элемент пути: фильтр
    /// </summary>
    TPathItemFilter = class (TPathItemIdentifier)
    private
      FCondition : TPathFilterCondition;

    protected
      procedure DoIsTargetStatic(var AResult : Bool3); override;
      function GetCanHasMultiTargets : boolean; override;
      function GetTargetData(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TDataNode): TDataNode; override;
      function GetTargetsData(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TDataNode): TArray<TDataNode>; override;

    public
      destructor Destroy; override;

      /// <summary>
      ///   Условие фильтрации
      /// </summary>
      property Condition : TPathFilterCondition  read FCondition;
    end;

    /// <summary>
    ///   Лексический анализатор
    /// </summary>
    TLexicalAnalizer = record
    strict private
      FText : string;

    public
      constructor Create(const AText : string);

      /// <summary>
      ///   Провести анализ
      /// </summary>
      /// <returns>
      ///   Массив лексем
      /// </returns>
      function Parse : TArray<TLexeme>;
    end;

    /// <summary>
    ///   Синтаксический анализатор
    /// </summary>
    TSyntaxAnalizer = record
    strict private
      FText : string;
      FLexemes : TArray<TLexeme>;
      FIndex : integer;
      function GetCurLexeme: PLexeme;

    private
      function PeekNext(const AExpectedLexemes : TLexemesKinds = []) : boolean;
      function Next(const AExpectedLexemes : TLexemesKinds = []) : boolean;
      function TryNext(const AExpectedLexemes : TLexemesKinds = []) : boolean;

      procedure RaiseUnxpextedEnd();
      procedure RaiseUnxpextedStatic(var AStaticPathItem);
      function ParseNumber(const AOwner : TPathItem; out AItem : TPathItem) : boolean;
      function ParseString(const AOwner : TPathItem; out AItem : TPathItem) : boolean;
      function ParseCondition(out ACondition : TPathFilterCondition) : boolean;
      function ParseIdentifier(const AOwner : TPathItem; out AItem : TPathItem) : boolean;

      property CurLexeme : PLexeme  read GetCurLexeme;

    public
      constructor Create(const AText : string; const ALexemes : TArray<TLexeme>);

      /// <summary>
      ///   Провести анализ
      /// </summary>
      /// <returns>
      ///   Верхний элемент пути
      /// </returns>
      function Parse : TPathItem;
    end;
    {$ENDREGION}

  public
    /// <summary>
    ///   Лексический анализ текста
    /// </summary>
    /// <param name="AText">
    ///   Анализируемый текст
    /// </param>
    /// <returns>
    ///   Массив лексем
    /// </returns>
    class function LexicalAnalize(const AText : string) : TArray<TLexeme>; static;

    /// <summary>
    ///   Синтаксический анализ текста
    /// </summary>
    /// <param name="AText">
    ///   Анализируемый текст
    /// </param>
    /// <param name="ALexemes">
    ///   Массив лексем, сформированный <c>LexicalAnalize</c>
    /// </param>
    /// <returns>
    ///   Верхний элемент пути
    /// </returns>
    class function SintaxAnalize(const AText : string; const ALexemes : TArray<TLexeme>) : TPathItem; overload; static;

    /// <summary>
    ///   Синтаксический анализ текста, совмещенный с лексическим
    /// </summary>
    /// <param name="AText">
    ///   Анализируемый текст
    /// </param>
    /// <returns>
    ///   Верхний элемент пути
    /// </returns>
    class function SintaxAnalize(const AText : string) : TPathItem; overload; static;
  end;
  {$ENDIF}

  /// <summary>
  ///   Базовый объект динамических данных
  /// </summary>
  TEntitiesObject =
  {$IFDEF USE_ALIASES}
    San.Entities.TEntitiesObject;
  {$ELSE}
    class
    (
      TObject,
      IInterface,
      IProvideUpdates, IProvideUpdates2,
      TMultithreadLockerFacade.ITarget
    )
  strict private
    {$IFDEF IS_ALIVE_SUPPORTS}
    FAlive : IInterface;
    {$ENDIF}

    function GetLocker: TMultithreadLockerFacade;
    function GetIsAlive: boolean;
    function GetUpdates: TUpdates2;

  private
    FUpdates : TUpdates2;

  protected
    {$REGION 'IInterface'}
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    {$ENDREGION}

    {$REGION 'IProvideUpdates'}
    function IProvideUpdates.GetUpdates    = GetProvideUpdates;
    function GetProvideUpdates : TUpdates;
    {$ENDREGION}

    {$REGION 'IProvideUpdates2'}
    function IProvideUpdates2.GetUpdates   = GetProvideUpdates;
    function IProvideUpdates2.GetUpdates2  = GetProvideUpdates2;
    function GetProvideUpdates2 : TUpdates2;
    {$ENDREGION}

    {$REGION 'TMultithreadLockerFacade.ITarget'}
    function GetLockerData() : TMultithreadLockerFacade.TMultithreadLockerData; virtual;
    function ExternalTryEnterRead(const AStartTicks, ATimeOut : Cardinal;
      out AFailedToLock : TObject) : boolean; virtual;
    procedure ExternalLeaveRead(const AFailedToLock : TObject); virtual;
    {$ENDREGION}

    /// <summary>
    ///   Действия при завершении использования сущностей текущем потоком
    /// </summary>
    procedure LockerDetachCurrentThread(); virtual;

    /// <summary>
    ///   Имеет подписчиков
    /// </summary>
    function HasEventSubscribers : boolean; virtual;
    /// <summary>
    ///   Породить событие
    /// </summary>
    procedure DoFireEvent(const AEvent : TEntitiesEvent); virtual;
    /// <summary>
    ///   Породить событие вставки
    /// </summary>
    procedure FireEventInsert(const ASource : TEntitiesObject; const AIndex : integer;
      const AItem : TEntitiesObject); virtual;
    /// <summary>
    ///   Породить событие удаления
    /// </summary>
    procedure FireEventDelete(const ASource : TEntitiesObject; const AIndex : integer;
      const AItem : TEntitiesObject); virtual;
    /// <summary>
    ///   Породить событие перестановки
    /// </summary>
    procedure FireEventMove(const ASource : TEntitiesObject; const AOldIndex, ANewIndex : integer;
      const AItem : TEntitiesObject); virtual;
    /// <summary>
    ///   Породить событие изменения
    /// </summary>
    procedure FireEventChange(const ASource : TEntitiesObject; const AField : TDataField;
      const AOldValue, ANewValue : TValue); virtual;


    /// <summary>
    ///   Обработка оповещений от механизма серийных изменений
    /// </summary>
    procedure DoUpdate(); virtual;

    /// <summary>
    ///   Экспорт в JSON
    /// </summary>
    /// <param name="ACompact">
    ///   <para>
    ///     <c>True</c>  — cодержит только поля отличные от DEFAULT, НЕ пустые сущности и коллекции
    ///   </para>
    ///   <para>
    ///     <c>False</c> — cодержит всё
    ///   </para>
    /// </param>
    function DoExportToJSON(const ACompact : boolean) : TJSONValue; virtual;

    /// <summary>
    ///   Экспорт отладочной информации в JSON
    /// </summary>
    procedure DoDebugExportToJSON(const AJSONObject : TJSONObject); virtual;

    /// <summary>
    ///   Импорт из JSON
    /// </summary>
    /// <param name="AJSON">
    ///   Импортируемый JSON
    /// </param>
    /// <param name="AReWrite">
    ///   <para>
    ///     <c>True</c>  — c потерей старых данных
    ///   </para>
    ///   <para>
    ///     <c>False</c> — c сохранением старых данных
    ///   </para>
    /// </param>
    procedure DoImportFromJSON(const AJSON : TJSONValue; const AReWrite : boolean); virtual;

    /// <summary>
    ///   Ошибка импорта из JSON. Не найдена пара с именем
    /// </summary>
    class procedure RaiseJSONImportPairNotFound(const AName : string); static;

  strict protected
    procedure AssignError(const ASource: TEntitiesObject);
    function AssignCheck(const ADest: TEntitiesObject) : boolean; virtual;
    procedure AssignTo(const ADest: TEntitiesObject); virtual;

  public
    constructor Create();
    destructor Destroy; override;

    /// <summary>
    ///   Копирование значений
    /// </summary>
    procedure Assign(const ASource : TEntitiesObject); virtual;

    /// <summary>
    ///   Экспорт в JSON
    /// </summary>
    /// <param name="ACompact">
    ///   <para>
    ///     <c>True</c>  — cодержит только поля отличные от DEFAULT, НЕ пустые сущности и коллекции
    ///   </para>
    ///   <para>
    ///     <c>False</c> — cодержит всё
    ///   </para>
    /// </param>
    function ExportToJSON(const ACompact : boolean = True) : TJSONValue; virtual;

    /// <summary>
    ///   Импорт из JSON
    /// </summary>
    /// <param name="AJSON">
    ///   Импортируемый JSON
    /// </param>
    /// <param name="AReWrite">
    ///   <para>
    ///     <c>True</c>  — c потерей старых данных
    ///   </para>
    ///   <para>
    ///     <c>False</c> — c сохранением старых данных
    ///   </para>
    /// </param>
    procedure ImportFromJSON(const AJSON : TJSONValue; const AReWrite : boolean); virtual;

    /// <summary>
    ///   Управление блокировками
    /// </summary>
    property Locker : TMultithreadLockerFacade  read GetLocker;

    /// <summary>
    ///   Механизм серийных изменений
    /// </summary>
    /// <remarks>
    ///   Поведение при оповещении определяется виртульным методом <c>DoUpdate</c>
    /// </remarks>
    property Updates : TUpdates2  read GetUpdates;

    /// <summary>
    ///   Проверка, жив ли объект
    /// </summary>
    /// <remarks>
    ///   При <c>True</c> — 100% гарантии не дает. При <c>False</c> — точно мусор
    /// </remarks>
    property IsAlive : boolean  read GetIsAlive;

  end;
  {$ENDIF}
  {$ENDREGION}

  {$REGION 'Meta'}
  /// <summary>
  ///   Базовый объект метаданных
  /// </summary>
  TMetaObject =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaObject;
  {$ELSE}
    class abstract(TEntitiesObject)
  private
    FDataObjects : TDataObjects;

  protected
    /// <summary>
    ///   Базовый класс данных
    /// </summary>
    class function GetBaseDataClass : TDataObjectClass; virtual;

    /// <summary>
    ///   Возможно, унаследованный класс данных
    /// </summary>
    function GetRichDataClass : TDataObjectClass; virtual;

    procedure DataCreating(const ADataObject : TDataObject); virtual;
    procedure DataDestroing(const ADataObject : TDataObject); virtual;

    /// <summary>
    ///   Проверка отсутствия данных
    /// </summary>
    /// <remarks>
    ///   Позволяет предотвратить изменение метаданных, когда данные уже созданы
    /// </remarks>
    procedure ValidateDataEmpty();

    /// <summary>
    ///   Экспорт в JSON
    /// </summary>
    /// <param name="ACompact">
    ///   <para>
    ///     <c>True</c>  — cодержит только поля отличные от DEFAULT, НЕ пустые сущности и коллекции
    ///   </para>
    ///   <para>
    ///     <c>False</c> — cодержит всё
    ///   </para>
    /// </param>
    procedure DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean = True); reintroduce; overload; virtual;

    /// <summary>
    ///   Импорт из JSON
    /// </summary>
    /// <param name="AJSON">
    ///   Импортируемый JSON
    /// </param>
    /// <param name="AReWrite">
    ///   <para>
    ///     <c>True</c>  — c потерей старых данных
    ///   </para>
    ///   <para>
    ///     <c>False</c> — c сохранением старых данных
    ///   </para>
    /// </param>
    procedure DoImportFromJSON(const AJSON : TJSONObject; const AReWrite : boolean); reintroduce; overload; virtual;

    function DoExportToJSON(const ACompact : boolean) : TJSONValue; overload; override;
    procedure DoImportFromJSON(const AJSON : TJSONValue; const AReWrite : boolean); overload; override;

  public
    procedure BeforeDestruction; override;
    destructor Destroy; override;

    /// <summary>
    ///   Копирование значений
    /// </summary>
    procedure Assign(const ASource : TEntitiesObject); override;

    /// <summary>
    ///   Создать экземпляр данных
    /// </summary>
    /// <param name="AOwner">
    ///   Владеющий объект данных
    /// </param>
    /// <param name="AData">
    ///   Созданый объект данных
    /// </param>
    procedure MakeDataRaw(const AOwner : TDataObject; out AData);

    /// <summary>
    ///   Создать экземпляр данных
    /// </summary>
    /// <param name="AOwner">
    ///   Владеющий объект данных
    /// </param>
    /// <returns>
    ///   Созданый объект данных
    /// </returns>
    function MakeData(const AOwner : TDataObject) : TDataObject;

    /// <summary>
    ///   Экспорт в JSON
    /// </summary>
    /// <param name="ACompact">
    ///   <para>
    ///     <c>True</c>  — cодержит только поля отличные от DEFAULT, НЕ пустые сущности и коллекции
    ///   </para>
    ///   <para>
    ///     <c>False</c> — cодержит всё
    ///   </para>
    /// </param>
    function ExportToJSON(const ACompact : boolean = True) : TJSONObject; reintroduce; virtual;

    /// <summary>
    ///   Импорт из JSON
    /// </summary>
    /// <param name="AJSON">
    ///   Импортируемый JSON
    /// </param>
    /// <param name="AReWrite">
    ///   <para>
    ///     <c>True</c>  — c потерей старых данных
    ///   </para>
    ///   <para>
    ///     <c>False</c> — c сохранением старых данных
    ///   </para>
    /// </param>
    procedure ImportFromJSON(const AJSON : TJSONObject; const AReWrite : boolean); reintroduce; virtual;
  end;
  {$ENDIF}

  /// <summary>
  ///   Метаданные с сылкой на владельца
  /// </summary>
  TMetaNode =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaNode;
  {$ELSE}
    class abstract(TMetaObject, IEventsSubscriber)
  strict private
    FOwner : TMetaNode;
    function GetParent: TMetaEntity;
    function GetPath : string;

  private
    FRoot : TMetaEntityRoot;

  strict protected
    function GetName : string; virtual; abstract;
    procedure SetName (const AValue : string); virtual;
    function DoGetPath : string; virtual; abstract;

  protected
    class function CheckMetaOwnerClass(const AOwner: TMetaNode) : boolean; virtual;
    function GetNodeType : TEntitiesNodeType; virtual; abstract;
    function DoFindChildNode(const ANodeType : TEntitiesNodeType; const AName : string; out AMetaNode : TMetaNode) : boolean; virtual;
    function GetInitialOwner() : TMetaNode; inline;

    function GetLockerData() : TMultithreadLockerFacade.TMultithreadLockerData; override;
    function ExternalTryEnterRead(const AStartTicks, ATimeOut : Cardinal;
      out AFailedToLock : TObject) : boolean; override;
    procedure ExternalLeaveRead(const AFailedToLock : TObject); override;

    {$REGION 'IEventsSubscriber'}
    function GetGuard : IEventsSubscribeGuard;
    {$ENDREGION}


  public
    constructor Create(const AOwner: TMetaNode); virtual;

    function ExportToJSON(const ACompact : boolean = True) : TJSONObject; override;

    /// <summary>
    ///   Создать копию объекта с указанным владельцем
    /// </summary>
    procedure Clone(const AOwner: TMetaNode; out AClone);

    /// <summary>
    ///   Метаданные корневой сушности
    /// </summary>
    property Root : TMetaEntityRoot  read FRoot;

    /// <summary>
    ///   Прямой владелец (поле или список сущностей)
    /// </summary>
    property Owner : TMetaNode  read FOwner;

    /// <summary>
    ///   Владеющая сушность
    /// </summary>
    property Parent : TMetaEntity  read GetParent;

    /// <summary>
    ///   Тип именованного узла
    /// </summary>
    property NodeType : TEntitiesNodeType  read GetNodeType;

    /// <summary>
    ///   Имя метаданных сущности
    /// </summary>
    property Name : string  read GetName  write SetName;

    /// <summary>
    ///   Имена метаданных сущности от корня
    /// </summary>
    /// <remarks>
    ///   Для <c>Self=nil</c> возвращает строку <c>'nil'</c>
    /// </remarks>
    property Path : string  read GetPath;
  end;
  {$ENDIF}

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Метаданные с сылкой на владельца. Генерик
  /// </summary>
  TMetaNode<TOwner, TData : class> = class abstract(TMetaNode)
  strict private
    function GetOwner: TOwner;

  protected
    class function GetBaseDataClass : TDataObjectClass; override;
    class function CheckMetaOwnerClass(const AOwner: TMetaNode) : boolean; override;

  public

    /// <summary>
    ///   Прямой владелец (поле или список сущностей)
    /// </summary>
    property Owner : TOwner  read GetOwner;
  end;
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности
  /// </summary>
  TMetaField =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaField;
  {$ELSE}
    class abstract(TMetaNode<TMetaEntity, TDataField>)
  strict private
    FName : string;
    class var FFieldsByStrIDRegistry : TDictionary<TUpperString,TMetaFieldClass>;

  private
    FIndex : integer;

  strict protected
    function AssignCheck(const ADest: TEntitiesObject) : boolean; override;
    procedure AssignTo(const ADest: TEntitiesObject); override;
    function GetName : string; override;
    function DoGetPath : string; override;
    procedure SetName (const AValue : string); override;
    function GetIsCollectionField: boolean; virtual;
    function GetIsEntityField: boolean; virtual;
    function GetIsSimpleField: boolean; virtual;

    function GetNodeType : TEntitiesNodeType; override;

  protected
    function GetIndex : integer;
    procedure DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean); override;
    procedure DoImportFromJSON(const AJSON : TJSONObject; const AReWrite : boolean); override;

    /// <summary>
    ///   Соответствующий класс индекса данных
    /// </summary>
    class function DataEntitiesIndexSingleClass : TDataCollectionIndexSingleClass; virtual;

  public
    class constructor Create;
    class destructor Destroy;

    constructor Create(const AOwner: TMetaNode); override;

    /// <summary>
    ///   Тип значения полей порожденных данным метаполем
    /// </summary>
    class function ValueType() : PTypeInfo;

    /// <summary>
    ///   Строковый идентификатор поля
    /// </summary>
    class function FieldStrID : string; virtual;

    /// <summary>
    ///   Классы полей по имени типа
    /// </summary>
    class property FieldsByStrIDRegistry : TDictionary<TUpperString,TMetaFieldClass>  read FFieldsByStrIDRegistry;

    /// <summary>
    ///   Индекс поля
    /// </summary>
    property Index : integer  read GetIndex;

    /// <summary>
    ///   Простое поле (не подобъект, не список, не ссылка)
    /// </summary>
    property IsSimpleField : boolean  read GetIsSimpleField;

    /// <summary>
    ///   Поле, содержащее сущность
    /// </summary>
    property IsEntityField : boolean  read GetIsEntityField;

    /// <summary>
    ///   Поле, содержащее список сущностей
    /// </summary>
    property IsCollectionField : boolean  read GetIsCollectionField;
  end;
  {$ENDIF}

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Метаданные поля сущности. Значение по умолчанию. Генерик.
  /// </summary>
  TMetaField<T> = class(TMetaField)
  strict private
    FDefaultValue : T;

  protected
    procedure DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean); override;
    procedure DoImportFromJSON(const AJSON : TJSONObject; const AReWrite : boolean); override;
    procedure SetDefaultValue(const AValue: T); virtual;

  public
    {$IFDEF DEBUG}
    class constructor Create();
    {$ENDIF}

    /// <summary>
    ///   Значение по умолчанию
    /// </summary>
    property DefaultValue : T  read FDefaultValue  write SetDefaultValue;
  end;

  /// <summary>
  ///   Метаданные поля сущности. Генерик
  /// </summary>
  TMetaField<TDataFieldType, TDataCollectionIndexSingleType : class; T> = class(TMetaField<T>)
  protected
    class function GetBaseDataClass() : TDataObjectClass; override;
    class function DataEntitiesIndexSingleClass : TDataCollectionIndexSingleClass; override;

  end;

  /// <summary>
  ///   Метаданные поля сущности, содержащего дочерний узел
  /// </summary>
  TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType : class> =
    class abstract(TMetaField<TDataFieldType, TDataCollectionIndexSingleType, TDataNode>)
  strict private
    function GetNode: TMetaDataType;

  private
    FNode : TMetaNode;

  strict protected
    function AssignCheck(const ADest: TEntitiesObject) : boolean; override;
    procedure AssignTo(const ADest: TEntitiesObject); override;

  protected
    class function SubNodeClass : TMetaNodeClass; virtual;
    function DoFindChildNode(const ANodeType : TEntitiesNodeType;
      const AName : string; out AMetaNode : TMetaNode) : boolean; override;
    function GetIsSimpleField: boolean; override;
    procedure DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean); override;
    procedure DoImportFromJSON(const AJSON : TJSONObject; const AReWrite : boolean); override;
    procedure SetDefaultValue(const AValue: TDataNode); override;

    /// <summary>
    ///   Метаданные узла
    /// </summary>
    property Node : TMetaDataType  read GetNode;

  public
    constructor Create(const AOwner: TMetaNode); override;
    destructor Destroy; override;

  end;
  {$ENDIF}

  {$REGION 'Typed fields'}
  /// <summary>
  ///   Метаданные поля сущности. Integer
  /// </summary>
  TMetaFieldInteger =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldInteger;
  {$ELSE}
    class(TMetaField<TDataFieldInteger, TDataCollectionIndexSingleInteger, Integer>);
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности. Integer
  /// </summary>
  TMetaFieldInt64 =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldInt64;
  {$ELSE}
    class(TMetaField<TDataFieldInt64, TDataCollectionIndexSingleInt64, Int64>);
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности. String
  /// </summary>
  TMetaFieldString =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldString;
  {$ELSE}
    class(TMetaField<TDataFieldString, TDataCollectionIndexSingleString, String>);
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности. Char
  /// </summary>
  TMetaFieldChar =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldChar;
  {$ELSE}
    class(TMetaField<TDataFieldChar, TDataCollectionIndexSingleString, Char>);
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности. Extended
  /// </summary>
  TMetaFieldExtended =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldExtended;
  {$ELSE}
    class(TMetaField<TDataFieldExtended, TDataCollectionIndexSingle, Extended>);
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности. Double
  /// </summary>
  TMetaFieldDouble =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldDouble;
  {$ELSE}
    class(TMetaField<TDataFieldDouble, TDataCollectionIndexSingle, Double>);
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности.TDateTime
  /// </summary>
  TMetaFieldDateTime =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldDateTime;
  {$ELSE}
    class(TMetaField<TDataFieldDateTime, TDataCollectionIndexSingle, TDateTime>);
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности.TDate
  /// </summary>
  TMetaFieldDate =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldDate;
  {$ELSE}
    class(TMetaField<TDataFieldDate, TDataCollectionIndexSingle, TDate>);
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности.TTime
  /// </summary>
  TMetaFieldTime =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldTime;
  {$ELSE}
    class(TMetaField<TDataFieldTime, TDataCollectionIndexSingle, TTime>);
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности. Boolean
  /// </summary>
  TMetaFieldBoolean =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldBoolean;
  {$ELSE}
    class(TMetaField<TDataFieldBoolean, TDataCollectionIndexSingle, Boolean>);
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности. GUID
  /// </summary>
  TMetaFieldGUID =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldGUID;
  {$ELSE}
    class(TMetaField<TDataFieldGUID, TDataCollectionIndexSingleGUID, TGUID>);
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности. Entity
  /// </summary>
  TMetaFieldEntity =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldEntity;
  {$ELSE}
    class(TMetaFieldNode<TMetaEntity, TDataFieldEntity, TDataCollectionIndexSingle>)
  strict private
    function GetEntity: TMetaEntity;

  protected
    function GetIsEntityField: boolean; override;

  public
    class function FieldStrID : string; override;

    /// <summary>
    ///   Метаданные сущности
    /// </summary>
    property Entity : TMetaEntity  read GetEntity;
  end;
  {$ENDIF}

  /// <summary>
  ///   Метаданные поля сущности. Collection
  /// </summary>
  TMetaFieldCollection =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaFieldCollection;
  {$ELSE}
    class(TMetaFieldNode<TMetaCollection, TDataFieldCollection, TDataCollectionIndexSingle>)
  strict private
    function GetCollection: TMetaCollection;

  protected
    function GetIsCollectionField: boolean; override;

  public
    class function FieldStrID : string; override;

    /// <summary>
    ///   Метаданные списка сущностей
    /// </summary>
    property Collection : TMetaCollection  read GetCollection;
  end;
  {$ENDIF}
  {$ENDREGION}

  /// <summary>
  ///   Фасад метаданных полей
  /// </summary>
  TMetaFieldsFacade = record
  strict private
    function GetCount: integer;
    function GetByIndex(const AIndex: integer): TMetaField;
    function GetByName(const AName: string): TMetaField;

  private
    /// <summary>
    ///   Непосредственный источник полей (может быть ссылка)
    /// </summary>
    FTarget : TMetaEntity;

  public
    constructor Create(const ATarget : TMetaEntity);

    /// <summary>
    ///   Перечисление полей
    /// </summary>
    function GetEnumerator: TArrayEnumerator<TMetaField>;

    /// <summary>
    ///   Попытка поиска по имени
    /// </summary>
    /// <param name="AName">
    ///   Имя поля
    /// </param>
    /// <param name="AMetaField">
    ///   Найденное поле
    /// </param>
    /// <param name="AIndexPtr">
    ///   Если не <c>nil</c> — индекс найденного элемента
    /// </param>
    /// <returns>
    ///   Успешно
    /// </returns>
    function TryFindByName(const AName : string; out AMetaField : TMetaField;
      const AIndexPtr : PInteger = nil) : boolean; overload;

    /// <summary>
    ///   Попытка поиска по имени
    /// </summary>
    /// <param name="AName">
    ///   Имя поля
    /// </param>
    /// <param name="AMetaField">
    ///   Найденное поле
    /// </param>
    /// <param name="AIndexPtr">
    ///   Если не <c>nil</c> — индекс найденного элемента
    /// </param>
    /// <returns>
    ///   Успешно
    /// </returns>
    function TryFindByName<TField : class>(const AName : string; out AMetaField : TField;
      const AIndexPtr : PInteger = nil) : boolean; overload;

    /// <summary>
    ///   Проверка существования по имени
    /// </summary>
    /// <param name="AName">
    ///   Имя поля
    /// </param>
    /// <returns>
    ///   Успешно
    /// </returns>
    function ExistsByName(const AName : string) : boolean;

    /// <summary>
    ///   Добавление поля
    /// </summary>
    /// <param name="AMetaField">
    ///   Добавляемое поле
    /// </param>
    /// <returns>
    ///   Добавленное поле
    /// </returns>
    function Add(const AMetaField : TMetaField) : TMetaField; overload;

    /// <summary>
    ///   Создание и добавление поля указанного типа
    /// </summary>
    /// <param name="TField">
    ///   Наследник <c>TMetaField</c>
    /// </param>
    /// <param name="AName">
    ///   Имя поля
    /// </param>
    /// <returns>
    ///   Добавленное поле
    /// </returns>
    function Add<TField : class>(const AName : string) : TField; overload;

    /// <summary>
    ///   Вставка поля
    /// </summary>
    /// <param name="AIndex">
    ///   Индекс поля
    /// </param>
    /// <param name="AMetaField">
    ///   Добавляемое поле
    /// </param>
    /// <returns>
    ///   Добавленное поле
    /// </returns>
    function Insert(const AIndex : integer; AMetaField : TMetaField) : TMetaField;

    /// <summary>
    ///   Перестановка полей местами
    /// </summary>
    /// <param name="AIndex1">
    ///   Первый индекс
    /// </param>
    /// <param name="AIndex2">
    ///   Второй индекс
    /// </param>
    procedure Exchange(const AIndex1, AIndex2 : integer);

    /// <summary>
    ///   Удаление поля
    /// </summary>
    /// <param name="AIndex">
    ///   Добавляемое поле
    /// </param>
    procedure Delete(const AIndex : integer);

    /// <summary>
    ///   Количество полей
    /// </summary>
    property Count : integer  read GetCount;

    /// <summary>
    ///   Элементы — поля
    /// </summary>
    property Items[const AIndex : integer] : TMetaField  read GetByIndex; default;

    /// <summary>
    ///   Поля по имени
    /// </summary>
    /// <remarks>
    ///   Исключение, если не найдено
    /// </remarks>
    property Items[const AName : string] : TMetaField  read GetByName; default;

  end;

  /// <summary>
  ///   Метаданные узла сущности или коллекции
  /// </summary>
  TMetaNodeEntityOrCollection = class abstract(TMetaNode<TMetaNode, TDataNodeEntityOrCollection>)
  protected type
    {$REGION 'types'}
    TMetaNodeEntityOrCollectionInternalClass = class of TMetaNodeEntityOrCollectionInternal;

    /// <summary>
    ///   Базовый класс внутренних данных коллекции или сущности
    /// </summary>
    TMetaNodeEntityOrCollectionInternal = class abstract(TObject)
    strict private
      FRichDataClass : TDataNodeClass;

    protected
      function GetTarget(const AOwner : TMetaNodeEntityOrCollection) : TMetaNodeEntityOrCollection; virtual;
      procedure DoExportToJSON(const AOwner : TMetaNodeEntityOrCollection;
        const AJSON : TJSONObject; const ACompact : boolean); virtual; abstract;

    public
      constructor Create(); virtual;
      procedure ClearBeforeFree(); virtual;

      /// <summary>
      ///   Обогащенный класс наследника
      /// </summary>
      property RichDataClass : TDataNodeClass  read FRichDataClass  write  FRichDataClass;

    end;

    /// <summary>
    ///   Внутренние данные ссылки на коллекцию или сущность
    /// </summary>
    TMetaNodeEntityOrCollectionInternalCustomLink = class abstract(TMetaNodeEntityOrCollectionInternal)
    strict private
      FLink: string;
      FResolvedLink : TPathResolver.TPathItem;
      procedure SetLink(const AValue: string);

    protected
      function GetTarget(const AOwner : TMetaNodeEntityOrCollection) : TMetaNodeEntityOrCollection; override;

    public
      constructor Create(const ALink : string); reintroduce;
      procedure ClearBeforeFree(); override;

      /// <summary>
      ///   Ссылка на целевую сущность или коллекцию
      /// </summary>
      property Link : string  read FLink  write SetLink;

      /// <summary>
      ///   Разрешенная ссылка на целевую сущность или коллекцию
      /// </summary>
      property ResolvedLink : TPathResolver.TPathItem  read FResolvedLink;

    end;

    /// <summary>
    ///   Внутренние данные ссылки на коллекцию или сущность
    /// </summary>
    TMetaNodeEntityOrCollectionInternalLink = class (TMetaNodeEntityOrCollectionInternalCustomLink)
    protected
      procedure DoExportToJSON(const AOwner : TMetaNodeEntityOrCollection;
        const AJSON : TJSONObject; const ACompact : boolean); override;
    end;

    /// <summary>
    ///   Внутренние данные рекурсивной сущности
    /// </summary>
    /// <remarks>
    ///   Поведение метаданных похоже на ссылку, но поведение данных похоже на сущность без ссылки
    /// </remarks>
    TMetaNodeEntityOrCollectionInternalRecursion = class (TMetaNodeEntityOrCollectionInternalCustomLink)
    protected
      procedure DoExportToJSON(const AOwner : TMetaNodeEntityOrCollection;
        const AJSON : TJSONObject; const ACompact : boolean); override;
    end;

    /// <summary>
    ///   Внутренние данные коллекции или сущности
    /// </summary>
    TMetaNodeEntityOrCollectionInternalData = class abstract(TMetaNodeEntityOrCollectionInternal)

    protected

    public

    end;
    {$ENDREGION}

  strict private
    FLockerData : TMultithreadLockerFacade.TMultithreadLockerData;
    function GetTarget: TMetaNodeEntityOrCollection;
    function GetIsLink: boolean;
    function GetLink: string;
    procedure SetLink(const AValue: string);
    function GetIsRecursive: boolean;
    function GetRecursion: string;
    procedure SetRecursion(const AValue: string);

  private
    FInternalData : TMetaNodeEntityOrCollectionInternal;

  strict protected
    class function GetInternalDataClass : TMetaNodeEntityOrCollectionInternalClass; virtual; abstract;
    function AssignCheck(const ADest: TEntitiesObject) : boolean; override;
    procedure AssignTo(const ADest: TEntitiesObject); override;
    procedure AssignNoLinkTo(const ADest: TMetaNodeEntityOrCollection); virtual;
    function GetRichDataClass : TDataObjectClass; override;
    function GetLockerData() : TMultithreadLockerFacade.TMultithreadLockerData; override;
    procedure LockerDetachCurrentThread(); override;
    function GetFields: TMetaFieldsFacade; virtual; abstract;

    /// <summary>
    ///   Пересоздать внутренние данные
    /// </summary>
    /// <param name="ALink">
    ///   Непустая ссылка создает ссылочный тип
    /// </param>
    /// <param name="AIsRecursion">
    ///   Для ссылочного типа определяется: является тип сысылкой или рекурсией
    /// </param>
    procedure DoRecreateInternal(const ALink : string = ''; const AIsRecursion : boolean = False); virtual;

  protected
    procedure DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean); override;

    property InternalData : TMetaNodeEntityOrCollectionInternal  read FInternalData;

    /// <summary>
    ///   Ссылка рекурсивного типа на предыдущую итерацию
    /// </summary>
    /// <remarks>
    ///   Предотвращает зацикливание при создании метаданных
    /// </remarks>
    property Recursion : string  read GetRecursion  write SetRecursion;

    /// <summary>
    ///   Тип является рекурсивным
    /// </summary>
    property IsRecursive : boolean  read GetIsRecursive;

  public
    constructor Create(const AOwner: TMetaNode); override;
    destructor Destroy; override;

    /// <summary>
    ///   Ссылка на целевую сущность или коллекцию
    /// </summary>
    /// <remarks>
    ///   Если ссылка пуста, сущность или коллекция предоставляет свои данные
    /// </remarks>
    property Link : string  read GetLink  write SetLink;

    /// <summary>
    ///   Является ссылкой
    /// </summary>
    property IsLink : boolean  read GetIsLink;

    /// <summary>
    ///   Целевой объект.
    /// </summary>
    /// <remarks>
    ///   Если заполнена ссылка <c>Link</c> — соответствует ссылке,
    ///   иначе — <c>=Self</c>
    /// </remarks>
    property Target : TMetaNodeEntityOrCollection  read GetTarget;

    /// <summary>
    ///   Метаданные полей сущности или элемента списка сущностей
    /// </summary>
    /// <remarks>
    ///   <c>=ItemMeta.Fields</c>
    /// </remarks>
    property Fields : TMetaFieldsFacade  read GetFields;

  end;

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Метаданные узла сущности или коллекции. Генерик
  /// </summary>
  /// <remarks>
  ///   Предок для
  ///   <para>
  ///     <c>TMetaEntity</c>
  ///   </para>
  ///   и
  ///   <para>
  ///     <c>TMetaCollection</c>
  ///   </para>
  /// </remarks>
  TMetaNodeEntityOrCollection<TOwner, TData : class> = class abstract(TMetaNodeEntityOrCollection)
  strict private
    function GetOwner: TOwner;

  protected
    class function GetBaseDataClass : TDataObjectClass; override;
    class function CheckMetaOwnerClass(const AOwner: TMetaNode) : boolean; override;

  public
    /// <summary>
    ///   Прямой владелец (поле или список сущностей)
    /// </summary>
    property Owner : TOwner  read GetOwner;
  end;
  {$ENDIF}

  /// <summary>
  ///   Метаданные сущности
  /// </summary>
  TMetaEntity =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaEntity;
  {$ELSE}
    class(TMetaNodeEntityOrCollection<TMetaNode, TDataEntity>)
  protected type
    {$REGION 'protected types'}
    /// <summary>
    ///   Внутренние данные сущности
    /// </summary>
    TMetaEntityInternalData = class (TMetaNodeEntityOrCollectionInternalData)
    strict private

    private
      FFields : TArray<TMetaField>;
      FAncestor: TMetaEntity;

    protected
      procedure DoExportToJSON(const AOwner : TMetaNodeEntityOrCollection;
        const AJSON : TJSONObject; const ACompact : boolean); override;

      procedure CloneFields(const ASource, ADest: TMetaEntity; const ACheckExists : Boolean);

      /// <summary>
      ///   Попытка поиска по имени
      /// </summary>
      /// <param name="AName">
      ///   Имя поля
      /// </param>
      /// <param name="AMetaField">
      ///   Найденное поле
      /// </param>
      /// <param name="AIndexPtr">
      ///   Если не <c>nil</c> — индекс найденного элемента
      /// </param>
      /// <returns>
      ///   Успешно
      /// </returns>
      function TryFindFieldByName(const AName : string; out AMetaField : TMetaField;
        const AIndexPtr : PInteger = nil) : boolean;

      /// <summary>
      ///   Проверка существования по имени
      /// </summary>
      /// <param name="AName">
      ///   Имя поля
      /// </param>
      /// <returns>
      ///   Успешно
      /// </returns>
      function ExistsFieldByName(const AName : string) : boolean;

      /// <summary>
      ///   Добавить поле
      /// </summary>
      /// <param name="AMetaField">
      ///   Поле
      /// </param>
      procedure AddField(const AMetaField : TMetaField);

    public
      procedure ClearBeforeFree(); override;

    end;

    /// <summary>
    ///   Фасад метаданных полей. Хелпер
    /// </summary>
    TMetaFieldsFacadeHelper = record helper for TMetaFieldsFacade
    strict private
      function GetInternalData: TMetaEntity.TMetaEntityInternalData; inline;

    private
      property InternalData : TMetaEntity.TMetaEntityInternalData  read GetInternalData;
    end;
    {$ENDREGION}

  strict private
    function GetTarget: TMetaEntity; inline;
    procedure DoSetAncestor(const AValue: TMetaEntity; const AUpdateFields : boolean);
    procedure SetAncestor(const AValue: TMetaEntity);
    function GetAncestor: TMetaEntity;

  strict protected
    class function GetInternalDataClass : TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalClass; override;
    function AssignCheck(const ADest: TEntitiesObject) : boolean; override;
    procedure AssignNoLinkTo(const ADest: TMetaNodeEntityOrCollection); override;
    function GetName : string; override;
    function DoGetPath : string; override;
    function GetFields: TMetaFieldsFacade; override;

  protected
    function GetRichDataClass : TDataObjectClass; override;
    function GetNodeType : TEntitiesNodeType; override;
    function DoFindChildNode(const ANodeType : TEntitiesNodeType;
      const AName : string; out AMetaNode : TMetaNode) : boolean; override;
    procedure DoImportFromJSON(const AJSON : TJSONObject; const AReWrite : boolean); override;

  public
    /// <summary>
    ///   Целевой объект.
    /// </summary>
    /// <remarks>
    ///   Если заполнена ссылка <c>Link</c> — соответствует ссылке,
    ///   иначе — <c>=Self</c>
    /// </remarks>
    property Target : TMetaEntity  read GetTarget;

    /// <summary>
    ///   Предковая метасущность
    /// </summary>
    /// <remarks>
    ///   Если задана, из нее копируются все описания полей
    /// </remarks>
    /// <exceptions cref="EEntitiesMetaChangeForbidden">
    ///   При попытке изменения, если существуют данные
    /// </exceptions>
    property Ancestor : TMetaEntity  read GetAncestor  write SetAncestor  default nil;

    /// <summary>
    ///   Метаданные полей сущности
    /// </summary>
    property Fields;

    /// <summary>
    ///   Ссылка рекурсивного типа на предыдущую итерацию
    /// </summary>
    /// <remarks>
    ///   Предотвращает зацикливание при создании метаданных
    /// </remarks>
    property Recursion;

    /// <summary>
    ///   Тип является рекурсивным
    /// </summary>
    property IsRecursive;
  end;
  {$ENDIF}

  /// <summary>
  ///   Метаданные индекса списка сущностей
  /// </summary>
  TMetaCollectionIndex =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaCollectionIndex;
  {$ELSE}
    class(TMetaObject)
  strict private
    FOwner: TMetaCollection;
    FIsUnique: boolean;

  protected
    procedure DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean); override;

  public
    constructor Create(const AOwner : TMetaCollection;
      const AIsUnique : boolean); virtual;

    /// <summary>
    ///   Создать копию индекса с указанным владельцем
    /// </summary>
    procedure Clone(const AOwner : TMetaCollection; out AClone);

    /// <summary>
    ///   Метаданные владеющего списка сущностей
    /// </summary>
    property Owner : TMetaCollection  read FOwner;

    /// <summary>
    ///   Уникальность индекса
    /// </summary>
    property IsUnique : boolean  read FIsUnique;
  end;
  {$ENDIF}

  /// <summary>
  ///   Метаданные индекса списка сущностей по одному полю
  /// </summary>
  TMetaCollectionIndexSingle =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaCollectionIndexSingle;
  {$ELSE}
    class(TMetaCollectionIndex)
  strict private
    FField: TMetaField;

  strict protected
    function AssignCheck(const ADest: TEntitiesObject) : boolean; override;
    procedure AssignTo(const ADest: TEntitiesObject); override;
    procedure DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean); override;

  protected
    function GetRichDataClass : TDataObjectClass; override;

  public
    constructor Create(const AOwner : TMetaCollection;
      const AUnique : boolean; const AField: TMetaField); reintroduce;

    /// <summary>
    ///   Метаданные поля, по которому построен индекс
    /// </summary>
    property Field : TMetaField  read FField;

  end;
  {$ENDIF}

  /// <summary>
  ///   Метаданные списка сущностей
  /// </summary>
  TMetaCollection =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaCollection;
  {$ELSE}
    class(TMetaNodeEntityOrCollection<TMetaFieldCollection, TDataCollection>)
  protected type
    {$REGION 'protected types'}
    /// <summary>
    ///   Внутренние данные коллекции
    /// </summary>
    TMetaCollectionInternalData = class (TMetaNodeEntityOrCollectionInternalData)
    strict private

    private
      FItemMeta : TMetaEntity;
      FIndexes : TArray<TMetaCollectionIndex>;

    protected
      procedure CloneIndexes(const ASource, ADest : TMetaCollection);
      procedure AddIndex(const AIndex : TMetaCollectionIndex);
      procedure DoExportToJSON(const AOwner : TMetaNodeEntityOrCollection;
        const AJSON : TJSONObject; const ACompact : boolean); override;

    public
      procedure ClearBeforeFree(); override;

    end;
    {$ENDREGION}

  public type
    {$REGION 'public types'}
    /// <summary>
    ///   Фасад метаданных индексов списка сущностей
    /// </summary>
    TMetaCollectionIndexesFacade = record
    strict private
      FTarget : TMetaCollection;
      function GetInternalData: TMetaCollectionInternalData;
      function GetCount : integer;
      function GetItems(const AIndex: integer): TMetaCollectionIndex;

    private
      constructor Create(const ATarget : TMetaCollection);

      property InternalData : TMetaCollectionInternalData  read GetInternalData;

    public
      /// <summary>
      ///   Перечисление индексов
      /// </summary>
      function GetEnumerator: TArrayEnumerator<TMetaCollectionIndex>;

      /// <summary>
      ///   Попытка поиска метаиндекса по одному полю
      /// </summary>
      /// <param name="AMetaField">
      ///   Поле, по которому строится индекс
      /// </param>
      /// <param name="AMetaEntitiesIndexSingle">
      ///   Найденый индекс
      /// </param>
      /// <param name="AIndexPtr">
      ///   Если не <c>nil</c> — индекс найденного элемента
      /// </param>
      /// <returns>
      ///   True - успешно
      /// </returns>
      function TryFindSingleIndex(const AMetaField : TMetaField;
        out AMetaEntitiesIndexSingle : TMetaCollectionIndexSingle;
        const AIndexPtr : PInteger = nil) : boolean;

      /// <summary>
      ///   Добавление индекса по одному полю
      /// </summary>
      /// <param name="AMetaField">
      ///   Поле, по которому строится индекс
      /// </param>
      /// <param name="AUnique">
      ///   Уникальность индекса
      /// </param>
      /// <returns>
      ///   Добавленный индекс
      /// </returns>
      function AddSingleIndex(const AMetaField : TMetaField; const AUnique : boolean) : TMetaCollectionIndexSingle;

      /// <summary>
      ///   Удаление индекса по одному полю
      /// </summary>
      /// <param name="AMetaField">
      ///   Поле, по которому строится индекс
      /// </param>
      /// <returns>
      ///   True - успешно
      /// </returns>
      function RemoveSingleIndex(const AMetaField : TMetaField) : boolean;

      /// <summary>
      ///   Количество полей
      /// </summary>
      property Count : integer  read GetCount;

      /// <summary>
      ///   Элементы — сущности
      /// </summary>
      property Items[const AIndex : integer] : TMetaCollectionIndex  read GetItems; default;
    end;
    {$ENDREGION}

  strict private
    function GetTarget: TMetaCollection; inline;
    function GetItemMeta: TMetaEntity;
    function GetIndexes: TMetaCollectionIndexesFacade;

  strict protected
    class function GetInternalDataClass : TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalClass; override;
    function AssignCheck(const ADest: TEntitiesObject) : boolean; override;
    procedure AssignNoLinkTo(const ADest: TMetaNodeEntityOrCollection); override;
    function GetName : string; override;
    function DoGetPath : string; override;
    procedure DoRecreateInternal(const ALink : string = ''; const AIsRecursion : boolean = False); override;
    function GetFields: TMetaFieldsFacade; override;

  protected
    function GetRichDataClass : TDataObjectClass; override;
    function GetNodeType : TEntitiesNodeType; override;
    procedure DoImportFromJSON(const AJSON : TJSONObject; const AReWrite : boolean); override;

  public
    /// <summary>
    ///   Целевой объект.
    /// </summary>
    /// <remarks>
    ///   Если заполнена ссылка <c>Link</c> — соответствует ссылке,
    ///   иначе — <c>=Self</c>
    /// </remarks>
    property Target : TMetaCollection  read GetTarget;

    /// <summary>
    ///   Метаданные элемента списка сущностей
    /// </summary>
    property ItemMeta : TMetaEntity  read GetItemMeta;

    /// <summary>
    ///   Метаданные индексов списка сущностей
    /// </summary>
    property Indexes : TMetaCollectionIndexesFacade  read GetIndexes;

    /// <summary>
    ///   Метаданные полей элемента списка сущностей
    /// </summary>
    /// <remarks>
    ///   <c>=ItemMeta.Fields</c>
    /// </remarks>
    property Fields;

  end;
  {$ENDIF}

  /// <summary>
  ///   Атрибут для регистрации класса данных методом <c>RichDataClassesRegRTTI</c>
  /// </summary>
  RichData =
  {$IFDEF USE_ALIASES}
    San.Entities.RichData;
  {$ELSE}
    class(TCustomAttribute)
  strict private
    FPath : string;
  public
    constructor Create(const APath : string);

    /// <summary>
    ///   Путь к метаданным
    /// </summary>
    property Path : string  read FPath;
  end;
  {$ENDIF}

  /// <summary>
  ///   Метаданные корневой сущности
  /// </summary>
  TMetaEntityRoot =
  {$IFDEF USE_ALIASES}
    San.Entities.TMetaEntityRoot;
  {$ELSE}
    class(TMetaEntity)
  strict private
    FName : string;
    FVersion : integer;
    FEventSpace : TEventSpace;
    FIsEventsEnabled : boolean;
    FDataEntityBaseClass : TDataEntityClass;
    FDataCollectionBaseClass : TDataCollectionClass;
    procedure SetVersion(const AValue: integer);
    function GetMetaEvent: TMEEntities;

  private
    FGuard : IEventsSubscribeGuard;
    FMEEntities : TMEEntities;

  strict private
    function GetDataRootsCount: integer;
    function GetDataRoots(const AIndex: integer): TDataEntityRoot;

  strict protected
    function AssignCheck(const ADest: TEntitiesObject) : boolean; override;
    procedure AssignTo(const ADest: TEntitiesObject); override;
    function GetName : string; override;
    procedure SetName (const AValue : string); override;
    function DoGetPath : string; override;
    procedure DoRecreateInternal(const ALink : string = ''; const AIsRecursion : boolean = False); override;

  protected
    class function GetBaseDataClass : TDataObjectClass; override;
    class function CheckMetaOwnerClass(const AOwner: TMetaNode) : boolean; override;
    procedure DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean); override;
    procedure DoImportFromJSON(const AJSON : TJSONObject; const AReWrite : boolean); override;

  public
    constructor Create(); reintroduce;
    destructor Destroy; override;

    /// <summary>
    ///   Создать экземпляр данных
    /// </summary>
    /// <param name="ADataRoot">
    ///   Созданый объект данных
    /// </param>
    procedure MakeDataRaw(out ADataRoot);

    /// <summary>
    ///   Создать экземпляр данных
    /// </summary>
    /// <returns>
    ///   Созданый объект данных
    /// </returns>
    function MakeData() : TDataEntityRoot;

    /// <summary>
    ///   Регистрация класса (-ов) данных для указанного пути
    /// </summary>
    /// <param name="APath">
    ///   Путь к узлу
    /// </param>
    /// <param name="ADataEntityClass">
    ///   Класс сущности
    /// </param>
    /// <param name="ADataCollectionClass">
    ///   Класс коллекции
    /// </param>
    procedure RichDataClassesReg(const APath : string;
      const ADataEntityClass : TDataEntityClass;
      const ADataCollectionClass : TDataCollectionClass = nil);

    /// <summary>
    ///   Регистрация в <c>RichDataClassesRegistry</c> по атрибутам <c>RichData</c>
    /// </summary>
    procedure RichDataClassesRegRTTI();

    /// <summary>
    ///   Количество полей
    /// </summary>
    property DataRootsCount : integer  read GetDataRootsCount;

    /// <summary>
    ///   Элементы — поля
    /// </summary>
    property DataRoots[const AIndex : integer] : TDataEntityRoot  read GetDataRoots;

    /// <summary>
    ///   Актуальная версия метаданных
    /// </summary>
    property Version : integer  read FVersion  write SetVersion;

    /// <summary>
    ///   Пространство событий корня метаданных
    /// </summary>
    property EventSpace : TEventSpace  read FEventSpace;

    /// <summary>
    ///   Метасобытие изменения данных
    /// </summary>
    property MetaEvent: TMEEntities  read GetMetaEvent;

    /// <summary>
    ///   Активность событий
    /// </summary>
    /// <remarks>
    ///   При False события не вызываются
    /// </remarks>
    property IsEventsEnabled : boolean  read FIsEventsEnabled  write FIsEventsEnabled;

    /// <summary>
    ///   Базовый класс данных сущности
    /// </summary>
    property DataEntityBaseClass      : TDataEntityClass      read FDataEntityBaseClass      write  FDataEntityBaseClass;

    /// <summary>
    ///   Базовый класс данных коллекции
    /// </summary>
    property DataCollectionBaseClass  : TDataCollectionClass  read FDataCollectionBaseClass  write FDataCollectionBaseClass;
  end;
  {$ENDIF}

  {$ENDREGION}

  {$REGION 'Data'}
  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Базовый объект данных
  /// </summary>
  TDataObjects = class(TObjectList<TDataObject>);
  {$ENDIF}

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Слабая ссылка на объект данных
  /// </summary>
  /// <remarks>
  ///   Для использования в унаследованном коде
  /// </remarks>
  TDataWeakRef<T : class> = record
  strict private
    function GetIsAlive: boolean;
    function GetTarget: T;
    procedure SetTarget(const AValue: T);

  private
    FInternalData : IObjRef;

  public
    class operator Implicit(const ARef : TDataWeakRef<T>) : T;
    class operator Implicit(const ATarget : T) : TDataWeakRef<T>;
    class operator Implicit(const ARef : TDataWeakRef<T>) : boolean;
    class operator Equal(const A : TDataWeakRef<T>; const B : T) : boolean;
    class operator Equal(const A : T; const B : TDataWeakRef<T>) : boolean;
    class operator NotEqual(const A : TDataWeakRef<T>; const B : T) : boolean;
    class operator NotEqual(const A : T; const B : TDataWeakRef<T>) : boolean;

    class function Create(const ATarget : T) : TDataWeakRef<T>; static;

    /// <summary>
    ///   Создать копию ссылки
    /// </summary>
    procedure Assign(const ASource : TDataWeakRef<T>);

    /// <summary>
    ///   Целевой объект, на который ссылаемся
    /// </summary>
    /// <remarks>
    ///   При задании, ссылаемся на новый объект
    /// </remarks>
    property Target : T  read GetTarget  write SetTarget;

    /// <summary>
    ///   Объект, на который ссылаемся, жив
    /// </summary>
    property IsAlive : boolean  read GetIsAlive;

  end;
  {$ENDIF}

  /// <summary>
  ///   Базовый объект данных
  /// </summary>
  TDataObject =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataObject;
  {$ELSE}
    class abstract(TEntitiesObject)
  protected type
    {$REGION 'protected types'}
    /// <summary>
    ///   Тип дествий внутреннего оповещения
    /// </summary>
    TInternalNotificationAction = (inaClear, inaChanged, inaAdded, inaRemoved);

    /// <summary>
    ///   Контекст внутреннего оповещения
    /// </summary>
    TInternalNotificationContext = record
    public
      Sender, Item : TDataObject;
      Action : TInternalNotificationAction;

      class operator Equal(const A, B : TInternalNotificationContext) : boolean;
      class operator NotEqual(const A, B : TInternalNotificationContext) : boolean;

      constructor Create(const ASender : TDataObject; const AAction : TInternalNotificationAction); overload;
      constructor Create(const ASender, AItem : TDataObject; const AAction : TInternalNotificationAction); overload;
    end;

    /// <summary>
    ///   Контекст внутреннего оповещения
    /// </summary>
    TInNtfCtx = TInternalNotificationContext;

    /// <summary>
    ///   Обработчик внутреннего оповещения
    /// </summary>
    TInternalNotifier = procedure (const AContext : TDataObject.TInNtfCtx) of object;

    /// <summary>
    ///   Данные оповещения
    /// </summary>
    TInternalNotifierData = class
    strict private
      FNotifier : TInternalNotifier;

    private
      FHost : TDataObject;

      function ExportToJSON() : TJSONValue;

    public
      constructor Create(const ANotifier : TInternalNotifier; const AHost : TDataObject);
      destructor Destroy; override;

      /// <summary>
      ///   Вызвать внутренний обработчик <c>FNotifier</c>
      /// </summary>
      procedure Notify(const AContext : TDataObject.TInNtfCtx);
    end;
    {$ENDREGION}

  public type
    {$REGION 'public types'}
    /// <summary>
    ///   Контекст создания экземпляра данных
    /// </summary>
    TCreateContext = record
      /// <summary>
      ///   Метаданные
      /// </summary>
      Meta : TMetaObject;

      /// <summary>
      ///   Владелец
      /// </summary>
      Owner : TDataObject;
    end;
    {$ENDREGION}

  strict private
    FMeta: TMetaObject;
    FInternalNotification : TArray<TInternalNotifierData>;
    FUpdatesNotification : TArray<TInternalNotificationContext>;
    function GetInternalNotificationCount: integer;

  strict protected
     procedure DoUpdate(); override;

  protected
    /// <summary>
    ///   Проверки контекста с исключением
    /// </summary>
    class procedure ValidateCreateContext(const AContext : TDataObject.TCreateContext); virtual;

    procedure DoDebugExportToJSON(const AJSONObject : TJSONObject); override;

    /// <summary>
    ///   Внутренний конструктор вызывается через Meta.MakeData
    /// </summary>
    constructor InternalCreate(const AContext : TDataObject.TCreateContext); virtual;

    function GetInitialMeta : TMetaObject; inline;

    /// <summary>
    ///   Оповестить
    /// </summary>
    procedure DoInternalNotificationNotify(const AContext : TDataObject.TInNtfCtx);

  public
    /// <summary>
    ///   Публичный конструктор запрещен для вызова
    /// </summary>
    constructor Create; dynamic; deprecated;
    procedure BeforeDestruction; override;
    destructor Destroy; override;

    /// <summary>
    ///   Обновить данные
    /// </summary>
    procedure Refresh; virtual;

    /// <summary>
    ///   Добавить внутренний обработчик
    /// </summary>
    /// <returns>
    ///   Объект данных подписки. Должен быть сохранен до окончания потребности в подписке
    /// </returns>
    function InternalNotificationAdd(const ANotifier : TInternalNotifier) : TInternalNotifierData;

    /// <summary>
    ///   Убрать внутренний обработчик
    /// </summary>
    procedure InternalNotificationRemove(const ANotifierData : TInternalNotifierData);

    /// <summary>
    ///   Оповестить или запомнить до <c>Updates.EndUpdate</c>
    /// </summary>
    procedure InternalNotificationNotify(const AContext : TDataObject.TInNtfCtx);

    /// <summary>
    ///   Создать слабую ссылку
    /// </summary>
    procedure WeakRef(out ARef);

    /// <summary>
    ///   Количество элементов внутренней подписки
    /// </summary>
    property InternalNotificationCount : integer  read GetInternalNotificationCount;

    /// <summary>
    ///   Метаданне
    /// </summary>
    property Meta : TMetaObject  read FMeta;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные с сылкой на владельца
  /// </summary>
  TDataNode =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataNode;
  {$ELSE}
    class abstract(TDataObject, IEventsSubscriber)
  strict private
    FOwner: TDataNode;
    function GetMeta: TMetaNode;
    function GetParent: TDataEntity;
    function GetNodeType : TEntitiesNodeType;
    function GetName : string;
    function GetPath : string;

  private
    FRoot: TDataEntityRoot;

  protected
    class function GetMetaClass : TMetaNodeClass; virtual;
    class function GetDataOwnerClass : TDataObjectClass; virtual;
    class procedure ValidateCreateContext(const AContext : TDataObject.TCreateContext); override;
    class function CheckMetaClass(const AMeta : TMetaNode) : boolean; virtual;
    class function CheckOwnerClass(const AOwner : TDataNode) : boolean; virtual;

    constructor InternalCreate(const AContext : TDataObject.TCreateContext); override;

    function GetLockerData() : TMultithreadLockerFacade.TMultithreadLockerData; override;
    function ExternalTryEnterRead(const AStartTicks, ATimeOut : Cardinal;
      out AFailedToLock : TObject) : boolean; override;
    procedure ExternalLeaveRead(const AFailedToLock : TObject); override;

    procedure OwnedDestroing(const AOwned : TDataObject); virtual;
    function DoFindChildNode(const ANodeType : TEntitiesNodeType; const AName : string; out ADataNode : TDataNode) : boolean; virtual;
    function GetInitialOwner() : TDataNode; inline;

    {$REGION 'IEventsSubscriber'}
    function GetGuard : IEventsSubscribeGuard;
    {$ENDREGION}

    /// <summary>
    ///   Имя метаданных
    /// </summary>
    /// <remarks>
    ///   = Meta.Name
    /// </remarks>
    property Name : string  read GetName;

    /// <summary>
    ///   Имена метаданных корня и индексы сущностей
    /// </summary>
    /// <remarks>
    ///   != Meta.Path
    /// </remarks>
    property Path : string  read GetPath;

  public
    destructor Destroy; override;

    function ExportToJSON(const ACompact : boolean = True) : TJSONValue; override;

    /// <summary>
    ///   Создать копию объекта с указанным владельцем
    /// </summary>
    procedure Clone(const AOwner: TDataNode; out AClone);

    /// <summary>
    ///   Метаданне
    /// </summary>
    property Meta : TMetaNode  read GetMeta;

    /// <summary>
    ///   Метаданные корневой сушности
    /// </summary>
    property Root : TDataEntityRoot  read FRoot;

    /// <summary>
    ///   Прямой владелец (поле, список полей, сущность или список сущностей)
    /// </summary>
    property Owner : TDataNode  read FOwner;

    /// <summary>
    ///   Владеющая сушность
    /// </summary>
    property Parent : TDataEntity  read GetParent;

    /// <summary>
    ///   Тип узла
    /// </summary>
    property NodeType : TEntitiesNodeType  read GetNodeType;
  end;
  {$ENDIF}

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Данные с сылкой на владельца. Генерик
  /// </summary>
  TDataNode<TMeta, TOwner : class> =
    class abstract(TDataNode)
  strict private
    function GetMeta: TMeta;
    function GetOwner: TOwner;

  protected
    class function GetMetaClass : TMetaNodeClass; override;
    class function GetDataOwnerClass : TDataObjectClass; override;

  public
    /// <summary>
    ///   Метаданне
    /// </summary>
    /// <remarks>
    ///   Weak
    /// </remarks>
    property Meta : TMeta  read GetMeta;

    /// <summary>
    ///   Прямой владелец (поле, список полей, сущность или список сущностей)
    /// </summary>
    property Owner : TOwner  read GetOwner;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности
  /// </summary>
  TDataField =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataField;
  {$ELSE}
    class abstract(TDataNode<TMetaField, TDataEntity>)
  protected
    function GetIndex : Integer;
    function GetValue : Variant; virtual; abstract;
    procedure SetValue(const AValue : Variant); virtual; abstract;
    function GetAsValue: TValue; virtual; abstract;
    procedure SetAsValue(const AValue: TValue); virtual; abstract;
    function GetAsInteger : Integer; virtual; abstract;
    procedure SetAsInteger(const AValue : Integer); virtual; abstract;
    function GetAsInt64 : Int64; virtual; abstract;
    procedure SetAsInt64(const AValue : Int64); virtual; abstract;
    function GetAsString : string; virtual; abstract;
    procedure SetAsString(const AValue : string); virtual; abstract;
    function GetAsChar : Char; virtual; abstract;
    procedure SetAsChar(const AValue : Char); virtual; abstract;
    function GetAsExtended : Extended; virtual; abstract;
    procedure SetAsExtended(const AValue : Extended); virtual; abstract;
    function GetAsDouble: Double; virtual; abstract;
    procedure SetAsDouble(const AValue: Double); virtual; abstract;
    function GetAsBoolean : Boolean; virtual; abstract;
    procedure SetAsBoolean(const AValue : Boolean); virtual; abstract;
    function GetAsGUID : TGUID; virtual; abstract;
    procedure SetAsGUID(const AValue : TGUID); virtual; abstract;
    function GetAsNode: TDataNode; virtual; abstract;
    function GetAsEntity: TDataEntity; virtual; abstract;
    function GetAsItems: TDataCollection; virtual; abstract;
    procedure GetAsType(const ATypeInfo : PTypeInfo; out AValue); virtual;
    procedure GetAsTypeWithCast(const ATypeInfo : PTypeInfo; out AValue); virtual;
    procedure SetAsType(const ATypeInfo : PTypeInfo; const AValue); virtual;
    procedure SetAsTypeWithCast(const ATypeInfo : PTypeInfo; const AValue); virtual;

    /// <summary>
    ///   Ошибка приобразования типов при чтении
    /// </summary>
    /// <param name="TFrom">
    ///   Из типа
    /// </param>
    /// <param name="TTo">
    ///   В тип
    /// </param>
    /// <param name="AResult">
    ///   nil результат
    /// </param>
    procedure CastReadError<TFrom, TTo>(out AResult);

    /// <summary>
    ///   Ошибка приобразования типов при записи
    /// </summary>
    /// <param name="TFrom">
    ///   Из типа
    /// </param>
    /// <param name="TTo">
    ///   В тип
    /// </param>
    procedure CastWriteError<TFrom, TTo>;

    /// <summary>
    ///   Проверить возможность редактирования
    /// </summary>
    procedure CheckCanEdit;

    /// <summary>
    ///   Попытка получить индекс по данному полю, если он есть
    /// </summary>
    /// <param name="AIndex">
    ///   Существующий индекс
    /// </param>
    /// <returns>
    ///   True — найден
    /// </returns>
    function TryGetSingleIndex(out AIndex : TDataCollectionIndexSingle) : boolean;

  public
    /// <summary>
    ///   Тип значения
    /// </summary>
    class function ValueType() : PTypeInfo; virtual;

    /// <summary>
    ///   Сравнить данные с данными другого поля
    /// </summary>
    function CompareDataFieldTo(const AOtherField : TDataField; const AOperation : TEntitiesCompareOperation) : Bool3; virtual; abstract;

    /// <summary>
    ///   Сравнить данные с другими данными
    /// </summary>
    function CompareDataValueTo(const AOtherData : TValue; const AOperation : TEntitiesCompareOperation) : Bool3; virtual; abstract;

    /// <summary>
    ///   Получить значение указанного типа
    /// </summary>
    function AsType<TValueType> : TValueType;

    /// <summary>
    ///   Задать значение указанного типа
    /// </summary>
    procedure From<TValueType>(const AValue : TValueType);

    /// <summary>
    ///   Имя метаданных поля
    /// </summary>
    /// <remarks>
    ///   = Meta.MetaName
    /// </remarks>
    property Name;

    /// <summary>
    ///   Зачение. Вариант
    /// </summary>
    property Value : Variant             read GetValue         write SetValue;

    /// <summary>
    ///   Зачение
    /// </summary>
    property AsValue : TValue            read GetAsValue       write SetAsValue;

    /// <summary>
    ///   Зачение. Целое
    /// </summary>
    property AsInteger : Integer         read GetAsInteger     write SetAsInteger;

    /// <summary>
    ///   Зачение. Целое
    /// </summary>
    property AsInt : Integer             read GetAsInteger     write SetAsInteger;

    /// <summary>
    ///   Зачение. Целое x64
    /// </summary>
    property AsInt64 : Int64             read GetAsInt64       write SetAsInt64;

    /// <summary>
    ///   Зачение. Строка
    /// </summary>
    property AsString : string           read GetAsString      write SetAsString;

    /// <summary>
    ///   Зачение. Строка
    /// </summary>
    property AsStr : string              read GetAsString      write SetAsString;

    /// <summary>
    ///   Зачение. Символ
    /// </summary>
    property AsChar : Char               read GetAsChar        write SetAsChar;

    /// <summary>
    ///   Зачение. Действилельное число
    /// </summary>
    property AsExtended : Extended       read GetAsExtended    write SetAsExtended;

    /// <summary>
    ///   Зачение. Действилельное число
    /// </summary>
    property AsDouble : Double           read GetAsDouble      write SetAsDouble;

    /// <summary>
    ///   Зачение. Логическое
    /// </summary>
    property AsBoolean : Boolean         read GetAsBoolean     write SetAsBoolean;

    /// <summary>
    ///   Зачение. GUID
    /// </summary>
    property AsGUID : TGUID              read GetAsGUID        write SetAsGUID;


    /// <summary>
    ///   Зачение. Узел
    /// </summary>
    property AsNode : TDataNode          read GetAsNode;

    /// <summary>
    ///   Зачение. Сущность
    /// </summary>
    property AsEntity : TDataEntity      read GetAsEntity;

    /// <summary>
    ///   Зачение. Список сущностей
    /// </summary>
    /// <remarks>
    ///   Синоним <c>AsCollection</c>
    /// </remarks>
    property AsItems : TDataCollection   read GetAsItems;

    /// <summary>
    ///   Зачение. Список сущностей
    /// </summary>
    /// <remarks>
    ///   Синоним <c>AsItems</c>
    /// </remarks>
    property AsCollection : TDataCollection   read GetAsItems;
  end;
  {$ENDIF}

  {$REGION 'typed data fields'}
  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Данные поля сущности. Генерик
  /// </summary>
  TDataField<TMeta : class; T> = class abstract(TDataField)
  strict private
    function GetMeta: TMeta;
    function GetDefaultValue: T;

  private
    FValue : T;

  protected
    class function GetMetaClass : TMetaNodeClass; override;
    constructor InternalCreate(const AContext : TDataObject.TCreateContext); override;

    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsValue: TValue; override;
    procedure SetAsValue(const AValue: TValue); override;
    function GetAsInteger : Integer; override;
    procedure SetAsInteger(const AValue : Integer); override;
    function GetAsInt64 : Int64; override;
    procedure SetAsInt64(const AValue : Int64); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsChar : Char; override;
    procedure SetAsChar(const AValue : Char); override;
    function GetAsExtended : Extended; override;
    procedure SetAsExtended(const AValue : Extended); override;
    function GetAsDouble: Double; override;
    procedure SetAsDouble(const AValue: Double); override;
    function GetAsBoolean : Boolean; override;
    procedure SetAsBoolean(const AValue : Boolean); override;
    function GetAsGUID : TGUID; override;
    procedure SetAsGUID(const AValue : TGUID); override;
    function GetAsNode: TDataNode; override;
    function GetAsEntity: TDataEntity; override;
    function GetAsItems: TDataCollection; override;
    procedure GetAsType(const ATypeInfo : PTypeInfo; out AValue); override;

    function DoExportToJSON(const ACompact : boolean = True) : TJSONValue; override;
    procedure DoImportFromJSON(const AJSON : TJSONValue; const AReWrite : boolean); override;

    /// <summary>
    ///   Задать новое значение
    /// </summary>
    /// <param name="TFrom">
    ///   Тип значения
    /// </param>
    /// <param name="AValue">
    ///   Новое значение
    /// </param>
    procedure SetNewValue<TFrom>(const AValue : T);

    /// <summary>
    ///   Равенство значений
    /// </summary>
    /// <param name="AValue">
    ///   Новое значение
    /// </param>
    function IsEqualValue(const AValue : T) : boolean; virtual;

    /// <summary>
    ///   Ошибка приобразования типов при чтении
    /// </summary>
    /// <param name="TTo">
    ///   В тип
    /// </param>
    /// <param name="AResult">
    ///   nil результат
    /// </param>
    procedure CastReadError<TTo>(out AResult);

    /// <summary>
    ///   Ошибка приобразования типов при записи
    /// </summary>
    /// <param name="TTo">
    ///   В тип
    /// </param>
    procedure CastWriteError<TTo>;

  public
    class function ValueType() : PTypeInfo; override;

    /// <summary>
    ///   Сравнить данные с данными другого поля
    /// </summary>
    function CompareDataFieldTo(const AOtherField : TDataField; const AOperation : TEntitiesCompareOperation) : Bool3; override;

    /// <summary>
    ///   Сравнить данные с другими данными
    /// </summary>
    function CompareDataValueTo(const AOtherData : TValue; const AOperation : TEntitiesCompareOperation) : Bool3; override;

    /// <summary>
    ///   Метаданне
    /// </summary>
    /// <remarks>
    ///   Weak
    /// </remarks>
    property Meta : TMeta  read GetMeta;

    /// <summary>
    ///   Значение по умолчанию
    /// </summary>
    property DefaultValue : T  read GetDefaultValue;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. Integer
  /// </summary>
  TDataFieldInteger =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldInteger;
  {$ELSE}
    class(TDataField<TMetaFieldInteger, Integer>)
  protected
    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsInteger : Integer; override;
    procedure SetAsInteger(const AValue : Integer); override;
    function GetAsInt64 : Int64; override;
    procedure SetAsInt64(const AValue : Int64); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsChar : Char; override;
    procedure SetAsChar(const AValue : Char); override;
    function GetAsExtended : Extended; override;
    procedure SetAsExtended(const AValue : Extended); override;
    function GetAsDouble: Double; override;
    procedure SetAsDouble(const AValue: Double); override;
    function GetAsBoolean : Boolean; override;
    procedure SetAsBoolean(const AValue : Boolean); override;
    procedure GetAsTypeWithCast(const ATypeInfo : PTypeInfo; out AValue); override;
    procedure SetAsTypeWithCast(const ATypeInfo : PTypeInfo; const AValue); override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. Integer
  /// </summary>
  TDataFieldInt64 =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldInteger;
  {$ELSE}
    class(TDataField<TMetaFieldInt64, Int64>)
  protected
    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsInteger : Integer; override;
    procedure SetAsInteger(const AValue : Integer); override;
    function GetAsInt64 : Int64; override;
    procedure SetAsInt64(const AValue : Int64); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsChar : Char; override;
    procedure SetAsChar(const AValue : Char); override;
    function GetAsExtended : Extended; override;
    procedure SetAsExtended(const AValue : Extended); override;
    function GetAsDouble: Double; override;
    procedure SetAsDouble(const AValue: Double); override;
    function GetAsBoolean : Boolean; override;
    procedure SetAsBoolean(const AValue : Boolean); override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. String
  /// </summary>
  TDataFieldString =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldString;
  {$ELSE}
    class(TDataField<TMetaFieldString, String>)
  protected
    function IsEqualValue(const AValue : string) : boolean; override;

    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsInteger : Integer; override;
    procedure SetAsInteger(const AValue : Integer); override;
    function GetAsInt64 : Int64; override;
    procedure SetAsInt64(const AValue : Int64); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsChar : Char; override;
    procedure SetAsChar(const AValue : Char); override;
    function GetAsExtended : Extended; override;
    procedure SetAsExtended(const AValue : Extended); override;
    function GetAsDouble: Double; override;
    procedure SetAsDouble(const AValue: Double); override;
    function GetAsBoolean : Boolean; override;
    procedure SetAsBoolean(const AValue : Boolean); override;
    function GetAsGUID : TGUID; override;
    procedure SetAsGUID(const AValue : TGUID); override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. Char
  /// </summary>
  TDataFieldChar =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldChar;
  {$ELSE}
    class(TDataField<TMetaFieldChar, Char>)
  protected
    function IsEqualValue(const AValue : Char) : boolean; override;

    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsInteger : Integer; override;
    procedure SetAsInteger(const AValue : Integer); override;
    function GetAsInt64 : Int64; override;
    procedure SetAsInt64(const AValue : Int64); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsChar : Char; override;
    procedure SetAsChar(const AValue : Char); override;
    function GetAsBoolean : Boolean; override;
    procedure SetAsBoolean(const AValue : Boolean); override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. Extended
  /// </summary>
  TDataFieldExtended =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldExtended;
  {$ELSE}
    class(TDataField<TMetaFieldExtended, Extended>)
  protected
    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsInteger : Integer; override;
    procedure SetAsInteger(const AValue : Integer); override;
    function GetAsInt64 : Int64; override;
    procedure SetAsInt64(const AValue : Int64); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsExtended : Extended; override;
    procedure SetAsExtended(const AValue : Extended); override;
    function GetAsDouble: Double; override;
    procedure SetAsDouble(const AValue: Double); override;
    function GetAsBoolean : Boolean; override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. Double
  /// </summary>
  TDataFieldDouble =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldDouble;
  {$ELSE}
    class(TDataField<TMetaFieldDouble, Double>)
  protected
    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsInteger : Integer; override;
    procedure SetAsInteger(const AValue : Integer); override;
    function GetAsInt64 : Int64; override;
    procedure SetAsInt64(const AValue : Int64); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsExtended : Extended; override;
    procedure SetAsExtended(const AValue : Extended); override;
    function GetAsDouble: Double; override;
    procedure SetAsDouble(const AValue: Double); override;
    function GetAsBoolean : Boolean; override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. TDateTime
  /// </summary>
  TDataFieldDateTime =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldDateTime;
  {$ELSE}
    class(TDataField<TMetaFieldDateTime, TDateTime>)
  protected
    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsInteger : Integer; override;
    procedure SetAsInteger(const AValue : Integer); override;
    function GetAsInt64 : Int64; override;
    procedure SetAsInt64(const AValue : Int64); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsExtended : Extended; override;
    procedure SetAsExtended(const AValue : Extended); override;
    function GetAsDouble: Double; override;
    procedure SetAsDouble(const AValue: Double); override;
    function GetAsBoolean : Boolean; override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. TDate
  /// </summary>
  TDataFieldDate =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldDate;
  {$ELSE}
    class(TDataField<TMetaFieldDate, TDate>)
  protected
    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsInteger : Integer; override;
    procedure SetAsInteger(const AValue : Integer); override;
    function GetAsInt64 : Int64; override;
    procedure SetAsInt64(const AValue : Int64); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsExtended : Extended; override;
    procedure SetAsExtended(const AValue : Extended); override;
    function GetAsDouble: Double; override;
    procedure SetAsDouble(const AValue: Double); override;
    function GetAsBoolean : Boolean; override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. TTime
  /// </summary>
  TDataFieldTime =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldTime;
  {$ELSE}
    class(TDataField<TMetaFieldTime, TTime>)
  protected
    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsInteger : Integer; override;
    procedure SetAsInteger(const AValue : Integer); override;
    function GetAsInt64 : Int64; override;
    procedure SetAsInt64(const AValue : Int64); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsExtended : Extended; override;
    procedure SetAsExtended(const AValue : Extended); override;
    function GetAsDouble: Double; override;
    procedure SetAsDouble(const AValue: Double); override;
    function GetAsBoolean : Boolean; override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. Boolean
  /// </summary>
  TDataFieldBoolean =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldBoolean;
  {$ELSE}
    class(TDataField<TMetaFieldBoolean, Boolean>)
  protected
    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsInteger : Integer; override;
    procedure SetAsInteger(const AValue : Integer); override;
    function GetAsInt64 : Int64; override;
    procedure SetAsInt64(const AValue : Int64); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsExtended : Extended; override;
    function GetAsDouble: Double; override;
    function GetAsBoolean : Boolean; override;
    procedure SetAsBoolean(const AValue : Boolean); override;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. GUID
  /// </summary>
  TDataFieldGUID =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldGUID;
  {$ELSE}
    class(TDataField<TMetaFieldGUID, TGUID>)
  protected
    function GetValue : Variant; override;
    procedure SetValue(const AValue : Variant); override;
    function GetAsString : string; override;
    procedure SetAsString(const AValue : string); override;
    function GetAsGUID : TGUID; override;
    procedure SetAsGUID(const AValue : TGUID); override;
  end;
  {$ENDIF}

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Данные поля сущности. TDataNode
  /// </summary>
  TDataFieldNode<TMeta, T : class> = class(TDataField<TMeta, T>)
  protected
    procedure OwnedDestroing(const AOwned : TDataObject); override;
    function GetAsNode: TDataNode; override;
    function GetAsString : string; override;
    function DoFindChildNode(const ANodeType : TEntitiesNodeType;
      const AName : string; out ADataNode : TDataNode) : boolean; override;
    function DoExportToJSON(const ACompact : boolean) : TJSONValue; override;
    procedure DoImportFromJSON(const AJSON : TJSONValue; const AReWrite : boolean); override;

  public
    destructor Destroy; override;
    procedure Refresh; override;

  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. Entity
  /// </summary>
  TDataFieldEntity =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldEntity;
  {$ELSE}
    class(TDataFieldNode<TMetaFieldEntity, TDataEntity>)
  protected
    function GetAsNode: TDataNode; override;
    function GetAsEntity: TDataEntity; override;

  end;
  {$ENDIF}

  /// <summary>
  ///   Данные поля сущности. Entities
  /// </summary>
  TDataFieldCollection =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataFieldCollection;
  {$ELSE}
    class(TDataFieldNode<TMetaFieldCollection, TDataCollection>)
  protected
    function GetAsNode: TDataNode; override;
    function GetAsItems: TDataCollection; override;

  public

  end;
  {$ENDIF}

  {$ENDREGION}

  /// <summary>
  ///   Данные узла сущности или коллекции
  /// </summary>
  TDataNodeEntityOrCollection = class abstract(TDataNode<TMetaNodeEntityOrCollection, TDataNode>)
  protected type
    {$REGION 'types'}
    TDataNodeEntityOrCollectionInternalClass = class of TDataNodeEntityOrCollectionInternal;

    /// <summary>
    ///   Базовый класс внутренних данных коллекции или сущности
    /// </summary>
    TDataNodeEntityOrCollectionInternal = class abstract(TObject, IInterface)
    protected
      function GetTarget(const AOwner : TDataNodeEntityOrCollection) : TDataNodeEntityOrCollection; virtual;
      function DoExportToJSON(const AOwner : TDataNodeEntityOrCollection; const ACompact : boolean) : TJSONValue; virtual;
      procedure DoImportFromJSON(const AOwner : TDataNodeEntityOrCollection;
        const AJSON : TJSONValue; const AReWrite : boolean); virtual;

      {$REGION 'IInterface'}
      function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
      function _AddRef: Integer; stdcall;
      function _Release: Integer; stdcall;
      {$ENDREGION}

    public
      constructor Create(const AOwner : TDataNodeEntityOrCollection); virtual;
      procedure InternalDataCreated(const AOwner : TDataNodeEntityOrCollection); virtual;
      procedure ClearBeforeFree(const AOwner : TDataNodeEntityOrCollection); virtual;

      class function GetLinkTarget(const AOwner : TDataNodeEntityOrCollection) : TDataNodeEntityOrCollection;
    end;

    /// <summary>
    ///   Внутренние данные ссылки на коллекцию или сущность
    /// </summary>
    IDataNodeEntityOrCollectionInternalLink = interface(IInterface)
    ['{7E053A54-3C9A-44AC-AFCF-BE8290D5807D}']
      /// <summary>
      ///   Отсоединение ссылки
      /// </summary>
      procedure DetachLink();
    end;

    /// <summary>
    ///   Внутренние данные ссылки на коллекцию или сущность
    /// </summary>
    TDataNodeEntityOrCollectionInternalLink = class (TDataNodeEntityOrCollectionInternal, IDataNodeEntityOrCollectionInternalLink)
    strict private

    protected
      function GetTarget(const AOwner : TDataNodeEntityOrCollection) : TDataNodeEntityOrCollection; override;

      {$REGION 'IDataNodeEntityOrCollectionInternalLink'}
      procedure DetachLink(); virtual;
      {$ENDREGION}

    public

    end;

    /// <summary>
    ///   Внутренние данные коллекции или сущности
    /// </summary>
    TDataNodeEntityOrCollectionInternalData = class abstract(TDataNodeEntityOrCollectionInternal)
    strict private

    private
      FEvents : TArray<TEntitiesEvent>;

    protected

    public
      procedure ClearBeforeFree(const AOwner : TDataNodeEntityOrCollection); override;
      procedure AddEvent(const AEvent : TEntitiesEvent);

    end;
    {$ENDREGION}

  strict private
    FLockerData : TMultithreadLockerFacade.TMultithreadLockerData;
    function GetIsLink: boolean; inline;
    function GetLink: string; inline;
    function GetTarget: TDataNodeEntityOrCollection;
    function GetMetaEvent: TMEEntities;

  private
    FInternalData : TDataNodeEntityOrCollectionInternal;

  strict protected
    function GetInternalDataClass : TDataNodeEntityOrCollectionInternalClass; virtual; abstract;
    function GetInternalDataLinkClass : TDataNodeEntityOrCollectionInternalClass; virtual;
    constructor InternalCreate(const AContext : TDataObject.TCreateContext); override;
    function GetLockerData() : TMultithreadLockerFacade.TMultithreadLockerData; override;
    procedure LockerDetachCurrentThread(); override;
    function HasEventSubscribers : boolean; override;
    procedure DoFireEvent(const AEvent : TEntitiesEvent); override;
    procedure DoFireEvents(const AEvents : TArray<TEntitiesEvent>); virtual;
    procedure DoUpdate(); override;
    function DoExportToJSON(const ACompact : boolean) : TJSONValue; override;
    procedure DoImportFromJSON(const AJSON : TJSONValue; const AReWrite : boolean); override;


  protected
    property InternalData : TDataNodeEntityOrCollectionInternal  read FInternalData;

  public
    destructor Destroy; override;

    /// <summary>
    ///   Ссылка на целевую сущность или коллекцию
    /// </summary>
    /// <remarks>
    ///   Если ссылка пуста, сущность или коллекция предоставляет свои данные
    /// </remarks>
    property Link : string  read GetLink;

    /// <summary>
    ///   Является ссылкой
    /// </summary>
    property IsLink : boolean  read GetIsLink;

    /// <summary>
    ///   Целевой объект.
    /// </summary>
    /// <remarks>
    ///   Если заполнена ссылка <c>Link</c> — соответствует ссылке,
    ///   иначе — <c>=Self</c>
    /// </remarks>
    property Target : TDataNodeEntityOrCollection  read GetTarget;

    /// <summary>
    ///   Собвтия
    /// </summary>
    property MetaEvent : TMEEntities  read GetMetaEvent;
  end;

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Данные узла сущности или коллекции. Генерик
  /// </summary>
  TDataNodeEntityOrCollection<TMeta, TOwner : class> =
    class abstract(TDataNodeEntityOrCollection)
  strict private
    function GetMeta: TMeta;
    function GetOwner: TOwner;

  protected
    class function GetMetaClass : TMetaNodeClass; override;
    class function GetDataOwnerClass : TDataObjectClass; override;

  public
    /// <summary>
    ///   Метаданне
    /// </summary>
    /// <remarks>
    ///   Weak
    /// </remarks>
    property Meta : TMeta  read GetMeta;

    /// <summary>
    ///   Прямой владелец (поле, список полей, сущность или список сущностей)
    /// </summary>
    property Owner : TOwner  read GetOwner;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные сущности
  /// </summary>
  TDataEntity =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataEntity;
  {$ELSE}
    class (TDataNodeEntityOrCollection<TMetaEntity, TDataNode>)
  protected type
    {$REGION 'protected types'}
    /// <summary>
    ///   Внутренние данные сущности
    /// </summary>
    TDataEntityInternalData = class (TDataNodeEntityOrCollectionInternalData)
    strict private

    private
      FFields : TArray<TDataField>;

    protected
      procedure CloneFields(const ASource, ADest: TDataEntity; const ACheckExists : Boolean);
      procedure ExtractField(const AField : TDataField);

      /// <summary>
      ///   Попытка поиска по метаполю
      /// </summary>
      /// <param name="AMetaField">
      ///   Метаполе
      /// </param>
      /// <param name="ADataField">
      ///   Найденное поле
      /// </param>
      /// <param name="AIndexPtr">
      ///   Если не <c>nil</c> — индекс найденного элемента
      /// </param>
      /// <returns>
      ///   Успешно
      /// </returns>
      function TryFindField(const AMetaField : TMetaField; out ADataField : TDataField;
        const AIndexPtr : PInteger = nil) : boolean;

      /// <summary>
      ///   Проверка существования по метаполю
      /// </summary>
      /// <param name="AMetaField">
      ///   Метаполе
      /// </param>
      /// <returns>
      ///   Успешно
      /// </returns>
      function ExistsField(const AMetaField : TMetaField) : boolean;

      /// <summary>
      ///   Попытка поиска по имени
      /// </summary>
      /// <param name="AName">
      ///   Имя поля
      /// </param>
      /// <param name="ADataField">
      ///   Найденное поле
      /// </param>
      /// <param name="AIndexPtr">
      ///   Если не <c>nil</c> — индекс найденного элемента
      /// </param>
      /// <returns>
      ///   Успешно
      /// </returns>
      function TryFindFieldByName(const AName : string; out ADataField : TDataField;
        const AIndexPtr : PInteger = nil) : boolean;

      /// <summary>
      ///   Проверка существования по имени
      /// </summary>
      /// <param name="AName">
      ///   Имя поля
      /// </param>
      /// <returns>
      ///   Успешно
      /// </returns>
      function ExistsFieldByName(const AName : string) : boolean;

      /// <summary>
      ///   Добавить поле
      /// </summary>
      /// <param name="ADataField">
      ///   Поле
      /// </param>
      procedure AddField(const ADataField : TDataField);

      procedure DoExportToJSONObject(const AOwner : TDataEntity;
        const AJSON : TJSONObject; const ACompact : boolean); virtual;
      function DoExportToJSON(const AOwner : TDataNodeEntityOrCollection;
        const ACompact : boolean) : TJSONValue; override;
      procedure DoImportFromJSON(const AOwner : TDataNodeEntityOrCollection;
        const AJSON : TJSONValue; const AReWrite : boolean); override;

    public
      constructor Create(const AOwner : TDataNodeEntityOrCollection); override;
      procedure ClearBeforeFree(const AOwner : TDataNodeEntityOrCollection); override;

    end;

    /// <summary>
    ///   Внутренние данные ссылки на коллекцию или сущность.
    ///   Содержит индекс во владеющем списке
    /// </summary>
    TDataEntityInternalLinkWithIndex = class (TDataNodeEntityOrCollectionInternalLink)
    private
      /// <summary>
      ///   Индекс во владеющем списке
      /// </summary>
      FIndex : integer;
    end;

    /// <summary>
    ///   Внутренние данные сущности.
    ///   Содержит индекс во владеющем списке
    /// </summary>
    TDataEntityInternalDataWithIndex = class (TDataEntityInternalData)
    private
      /// <summary>
      ///   Индекс во владеющем списке
      /// </summary>
      FIndex : integer;
    end;
    {$ENDREGION}

  public type
    {$REGION 'public types'}

    /// <summary>
    ///   Фасад данных полей
    /// </summary>
    TDataFieldsFacade = record
    strict private
      /// <summary>
      ///   Непосредственный источник полей (может быть ссылка)
      /// </summary>
      FTarget : TDataEntity;

      function GetInternalData: TDataEntityInternalData;
      function GetCount: integer;
      function GetByIndex(const AIndex: integer): TDataField;
      function GetByName(const AName : string): TDataField;
      function GetByMetaField(const AMetaField : TMetaField): TDataField;

    private
      property InternalData : TDataEntityInternalData  read GetInternalData;

    public
      constructor Create(const ATarget : TDataEntity);

      /// <summary>
      ///   Перечисление полей
      /// </summary>
      function GetEnumerator: TArrayEnumerator<TDataField>;

      /// <summary>
      ///   Попытка поиска по метаполю
      /// </summary>
      /// <param name="AMetaField">
      ///   Метаполе
      /// </param>
      /// <param name="ADataField">
      ///   Найденное поле
      /// </param>
      /// <param name="AIndexPtr">
      ///   Если не <c>nil</c> — индекс найденного элемента
      /// </param>
      /// <returns>
      ///   Успешно
      /// </returns>
      function TryFind(const AMetaField : TMetaField; out ADataField : TDataField;
        const AIndexPtr : PInteger = nil) : boolean;

      /// <summary>
      ///   Проверка существования по метаполю
      /// </summary>
      /// <param name="AMetaField">
      ///   Метаполе
      /// </param>
      /// <returns>
      ///   Успешно
      /// </returns>
      function Exists(const AMetaField : TMetaField) : boolean;

      /// <summary>
      ///   Поиск по метаполю с исключением при неудаче
      /// </summary>
      /// <param name="AMetaField">
      ///   Метаполе
      /// </param>
      /// <param name="AIndexPtr">
      ///   Если не <c>nil</c> — индекс найденного элемента
      /// </param>
      /// <returns>
      ///   Найденное поле
      /// </returns>
      function Find(const AMetaField : TMetaField;
        const AIndexPtr : PInteger = nil) : TDataField;

      /// <summary>
      ///   Попытка поиска по имени
      /// </summary>
      /// <param name="AFieldName">
      ///   Имя поля
      /// </param>
      /// <param name="ADataField">
      ///   Найденное поле
      /// </param>
      /// <param name="AIndexPtr">
      ///   Если не <c>nil</c> — индекс найденного элемента
      /// </param>
      /// <returns>
      ///   Успешно
      /// </returns>
      function TryFindByName(const AName : string; out ADataField : TDataField;
        const AIndexPtr : PInteger = nil) : boolean;

      /// <summary>
      ///   Проверка существования по имени
      /// </summary>
      /// <param name="AName">
      ///   Имя поля
      /// </param>
      /// <returns>
      ///   Успешно
      /// </returns>
      function ExistsByName(const AName : string) : boolean;

      /// <summary>
      ///   Поиск по имени с исключением при неудаче
      /// </summary>
      /// <param name="AFieldName">
      ///   Имя поля
      /// </param>
      /// <param name="AIndexPtr">
      ///   Если не <c>nil</c> — индекс найденного элемента
      /// </param>
      /// <returns>
      ///   Найденное поле
      /// </returns>
      function FindByName(const AName : string;
        const AIndexPtr : PInteger = nil) : TDataField;

      /// <summary>
      ///   Количество полей
      /// </summary>
      property Count : integer  read GetCount;

      /// <summary>
      ///   Элементы — поля
      /// </summary>
      property Items[const AIndex : integer] : TDataField  read GetByIndex; default;

      /// <summary>
      ///   Поля по имени
      /// </summary>
      /// <remarks>
      ///   Исключение, если не найдено
      /// </remarks>
      property Items[const AName : string] : TDataField  read GetByName; default;

      /// <summary>
      ///   Поля по метополям
      /// </summary>
      /// <remarks>
      ///   Исключение, если не найдено
      /// </remarks>
      property Items[const AMetaField : TMetaField] : TDataField  read GetByMetaField; default;
    end;
    {$ENDREGION}

  strict private
    function GetIndex: Integer;
    function GetFields: TDataFieldsFacade;
    function GetTarget: TDataEntity;

  strict protected
    function GetInternalDataClass : TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalClass; override;
    function GetInternalDataLinkClass : TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalClass; override;

  protected
    procedure OwnedDestroing(const AOwned : TDataObject); override;
    function DoFindChildNode(const ANodeType : TEntitiesNodeType;
      const AName : string; out ADataNode : TDataNode) : boolean; override;

    /// <summary>
    ///   Доступ к полю, xранящему индекс в родительском списке
    /// </summary>
    /// <param name="AIndexPtr">
    ///    Указатель на поле
    /// </param>
    /// <returns>
    ///   <para>
    ///     <c>True</c>: есть такое поле
    ///   </para>
    ///   <para>
    ///     <c>False</c>: владелей — НЕ список
    ///   </para>
    /// </returns>
    function AccessForIndexPtr(out AIndexPtr : PInteger) : boolean;

    /// <summary>
    ///   Дополнительные действия перед экспортом в JSON
    /// </summary>
    /// <param name="ACompact">
    ///   <para>
    ///     <c>True</c>  — cодержит только поля отличные от DEFAULT, НЕ пустые сущности и коллекции
    ///   </para>
    ///   <para>
    ///     <c>False</c> — cодержит всё
    ///   </para>
    /// </param>
    procedure DoBeforeExportToJSON(const AJSON : TJSONObject; const ACompact : boolean); virtual;

    /// <summary>
    ///   Дополнительные действия после экспорта в JSON
    /// </summary>
    /// <param name="ACompact">
    ///   <para>
    ///     <c>True</c>  — cодержит только поля отличные от DEFAULT, НЕ пустые сущности и коллекции
    ///   </para>
    ///   <para>
    ///     <c>False</c> — cодержит всё
    ///   </para>
    /// </param>
    procedure DoAfterExportToJSON(const AJSON : TJSONObject; const ACompact : boolean); virtual;

    {$REGION 'PropIdx'}
    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Генерик
    /// </summary>
    function PIG<T>(const AIndex : integer) : T;
    /// <summary>
    ///   Сеттер для индексного свойства для наследника с доступом к полю по индексу. Генерик
    /// </summary>
    procedure PIS<T>(const AIndex : integer; const AValue : T);

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип integer
    /// </summary>
    function PIG_Integer(const AIndex : integer) : integer;
    /// <summary>
    ///   Сеттер для индексного свойства для наследника с доступом к полю по индексу. Тип integer
    /// </summary>
    procedure PIS_Integer(const AIndex : integer; const AValue : integer);

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип Int64
    /// </summary>
    function PIG_Int64(const AIndex : integer) : Int64;
    /// <summary>
    ///   Сеттер для индексного свойства для наследника с доступом к полю по индексу. Тип Int64
    /// </summary>
    procedure PIS_Int64(const AIndex : integer; const AValue : Int64);

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип String
    /// </summary>
    function PIG_String(const AIndex : integer) : String;
    /// <summary>
    ///   Сеттер для индексного свойства для наследника с доступом к полю по индексу. Тип String
    /// </summary>
    procedure PIS_String(const AIndex : integer; const AValue : String);

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип Char
    /// </summary>
    function PIG_Char(const AIndex : integer) : Char;
    /// <summary>
    ///   Сеттер для индексного свойства для наследника с доступом к полю по индексу. Тип Char
    /// </summary>
    procedure PIS_Char(const AIndex : integer; const AValue : Char);

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип Extended
    /// </summary>
    function PIG_Extended(const AIndex : integer) : Extended;
    /// <summary>
    ///   Сеттер для индексного свойства для наследника с доступом к полю по индексу. Тип Extended
    /// </summary>
    procedure PIS_Extended(const AIndex : integer; const AValue : Extended);

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип Double
    /// </summary>
    function PIG_Double(const AIndex : integer) : Double;
    /// <summary>
    ///   Сеттер для индексного свойства для наследника с доступом к полю по индексу. Тип Double
    /// </summary>
    procedure PIS_Double(const AIndex : integer; const AValue : Double);

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип TDateTime
    /// </summary>
    function PIG_DateTime(const AIndex : integer) : TDateTime;
    /// <summary>
    ///   Сеттер для индексного свойства для наследника с доступом к полю по индексу. Тип TDateTime
    /// </summary>
    procedure PIS_DateTime(const AIndex : integer; const AValue : TDateTime);

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип Boolean
    /// </summary>
    function PIG_Boolean(const AIndex : integer) : Boolean;
    /// <summary>
    ///   Сеттер для индексного свойства для наследника с доступом к полю по индексу. Тип Boolean
    /// </summary>
    procedure PIS_Boolean(const AIndex : integer; const AValue : Boolean);

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип TGUID
    /// </summary>
    function PIG_TGUID(const AIndex : integer) : TGUID;
    /// <summary>
    ///   Сеттер для индексного свойства для наследника с доступом к полю по индексу. Тип TGUID
    /// </summary>
    procedure PIS_TGUID(const AIndex : integer; const AValue : TGUID);

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип T:TDataEntity
    /// </summary>
    function PIG_Entity<T : class>(const AIndex : integer) : T;

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип T:TDataEntity.Target
    /// </summary>
    function PIG_EntityTarget<T : class>(const AIndex : integer) : T;

    /// <summary>
    ///   Геттер для индексного свойства для наследника с доступом к полю по индексу. Тип T:TDataCollection
    /// </summary>
    function PIG_Collection<T : class>(const AIndex : integer) : T;
    {$ENDREGION}

  public
    destructor Destroy; override;

    procedure Refresh; override;

    /// <summary>
    ///   Индекс во владеющем списке
    /// </summary>
    /// <remarks>
    ///   Если владелец не список, значение -1
    /// </remarks>
    property Index : Integer  read GetIndex;

    /// <summary>
    ///   Данные полей сущности
    /// </summary>
    property Fields : TDataFieldsFacade  read GetFields;

    /// <summary>
    ///   Целевой объект.
    /// </summary>
    /// <remarks>
    ///   Если заполнена ссылка <c>Link</c> — соответствует ссылке,
    ///   иначе — <c>=Self</c>
    /// </remarks>
    property Target : TDataEntity  read GetTarget;
  end;
  {$ENDIF}

  /// <summary>
  ///   Фасад поиска перебором через предкат
  /// </summary>
  /// <param name="T">
  ///   Наследник <c>TDataEntity</c>
  /// </param>
  TDataFinderWithEnumeration<T : class> = record
  strict private
    FTarget : TDataNode;
    FEnumerator : TEnumeratorRef<T>;
    FPredicate : TPredicate<T>;
    FOnError : TProc<Exception>;
    procedure SetOnError(const AValue: TProc<Exception>);

  private
    procedure ItemsNotFoundError();

  public
    constructor Create(const ATarget : TDataNode; const AEnumerator : TEnumeratorRef<T>;
      const APredicate : TPredicate<T>);

    /// <summary>
    ///   Попытка поиска
    /// </summary>
    /// <param name="AResults">
    ///   Массив результатов
    /// </param>
    /// <param name="ASeparatePassForAllocation">
    ///   <para>
    ///     <c>False</c> — один проход с постепенным выделением памяти для массива результатов
    ///   </para>
    ///   <para>
    ///     <c>True</c> — два прохода: первый вычисляет размер массива, второй заполняет его
    ///     (Функция предиката будет вызываться дважды для каждого элемента)
    ///   </para>
    /// </param>
    /// <returns>
    ///   <c>True</c> — есть результаты поиска
    /// </returns>
    function TryFindRaw(out AResults; const ASeparatePassForAllocation : boolean = False) : boolean;

    /// <summary>
    ///   Попытка поиска
    /// </summary>
    /// <param name="AResults">
    ///   Массив результатов
    /// </param>
    /// <param name="ASeparatePassForAllocation">
    ///   <para>
    ///     <c>False</c> — один проход с постепенным выделением памяти для массива результатов
    ///   </para>
    ///   <para>
    ///     <c>True</c> — два прохода: первый вычисляет размер массива, второй заполняет его
    ///     (Функция предиката будет вызываться дважды для каждого элемента)
    ///   </para>
    /// </param>
    /// <returns>
    ///   <c>True</c> — есть результаты поиска
    /// </returns>
    function TryFind(out AResults : TArray<T>; const ASeparatePassForAllocation : boolean = False) : boolean;

    /// <summary>
    ///   Попытка поиска первого подходящего элемента
    /// </summary>
    /// <param name="AResult">
    ///   Первая подходящая сущность. <c>nil</c>, если нет результатов
    /// </param>
    /// <returns>
    ///   <c>True</c> — есть результаты поиска
    /// </returns>
    function TryFindFirst(out AResult : T) : boolean;

    /// <summary>
    ///   Проверка существования элементов, соответствующих предикату
    /// </summary>
    /// <param name="AMinCount">
    ///   <c>AMinCount</c> &gt;= 1
    ///   <para>
    ///     Требуется указанное число элементов или более
    ///   </para>
    ///    Иначе
    ///   <para>
    ///     Требуется хотя бы один элемент
    ///   </para>
    /// </param>
    /// <returns>
    ///   <c>True</c> — есть результаты поиска
    /// </returns>
    function Exists(AMinCount : integer = 1) : boolean;

    /// <summary>
    ///   Поиск с исключением при неудаче
    /// </summary>
    /// <param name="AResults">
    ///   Массив результатов
    /// </param>
    /// <param name="ASeparatePassForAllocation">
    ///   <para>
    ///     <c>False</c> — один проход с постепенным выделением памяти для массива результатов
    ///   </para>
    ///   <para>
    ///     <c>True</c> — два прохода: первый вычисляет размер массива, второй заполняет его
    ///     (Функция предиката будет вызываться дважды для каждого элемента)
    ///   </para>
    /// </param>
    /// <exceptions cref="EEntitiesFinderError">
    ///   При неудаче
    /// </exceptions>
    /// <returns>
    ///   Массив результатов
    /// </returns>
    procedure FindRaw(out AResults; const ASeparatePassForAllocation : boolean = False);

    /// <summary>
    ///   Поиск с исключением при неудаче
    /// </summary>
    /// <param name="ASeparatePassForAllocation">
    ///   <para>
    ///     <c>False</c> — один проход с постепенным выделением памяти для массива результатов
    ///   </para>
    ///   <para>
    ///     <c>True</c> — два прохода: первый вычисляет размер массива, второй заполняет его
    ///     (Функция предиката будет вызываться дважды для каждого элемента)
    ///   </para>
    /// </param>
    /// <exceptions cref="EEntitiesFinderError">
    ///   При неудаче
    /// </exceptions>
    /// <returns>
    ///   Массив результатов
    /// </returns>
    function Find(const ASeparatePassForAllocation : boolean = False) : TArray<T>;

    /// <summary>
    ///   Поиск первого подходящего элемента с исключением при неудаче
    /// </summary>
    /// <exceptions cref="EEntitiesFinderError">
    ///   При неудаче
    /// </exceptions>
    /// <returns>
    ///   Первая подходящая сущность
    /// </returns>
    function FindFirst() : T;

    /// <summary>
    ///   Callback при ошибках
    /// </summary>
    property OnError : TProc<Exception>  read FOnError  write SetOnError;
  end;

  {$REGION 'DataEntitiesIndexes'}
  /// <summary>
  ///   Данные индекса списка сущностей
  /// </summary>
  TDataCollectionIndex =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataCollectionIndex;
  {$ELSE}
    class(TDataObject)
  strict private
    FMeta: TMetaCollectionIndex;
    FOwner: TDataCollection;
    function GetIsUnique: boolean; inline;

  protected
    constructor InternalCreate(const AContext : TDataObject.TCreateContext); override;
    procedure AddItemToIndex(const AItem : TDataEntity); virtual; abstract;
    procedure RemoveItemFromIndex(const AItem : TDataEntity); virtual; abstract;

    /// <summary>
    ///   Очистка данных индекса
    /// </summary>
    procedure Clear(); virtual; abstract;

  public

    /// <summary>
    ///   Метаданные индекса
    /// </summary>
    property Meta : TMetaCollectionIndex  read FMeta;

    /// <summary>
    ///   Уникальный
    /// </summary>
    /// <remarks>
    ///   <c>=IsUnique</c>
    /// </remarks>
    property IsUnique : boolean  read GetIsUnique;

    /// <summary>
    ///   Данные владеющего списка сущностей
    /// </summary>
    property Owner : TDataCollection  read FOwner;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные индекса списка сущностей по одному полю
  /// </summary>
  TDataCollectionIndexSingle =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataCollectionIndexSingle;
  {$ELSE}
    class abstract(TDataCollectionIndex)
  strict private
    function GetMeta : TMetaCollectionIndexSingle;

  protected
    function GetIndexItem(const AValue : TValue) : TObject; virtual; abstract;
    procedure AddItemToIndex(const AItem : TDataEntity); override;
    procedure RemoveItemFromIndex(const AItem : TDataEntity); override;
    procedure AddIndexItem(const ADataField : TDataField); virtual; abstract;
    procedure RemoveIndexItem(const ADataField : TDataField); virtual; abstract;
    function GetFinder(const AValue : TValue; const ACompareOperation : TEntitiesCompareOperation) : TDataFinderWithEnumeration<TDataEntity>; virtual; abstract;

  public
    /// <summary>
    ///   Поиск по индексу
    /// </summary>
    function Finder(const AValue : TValue; const ACompareOperation : TEntitiesCompareOperation = ecoEquals) : TDataFinderWithEnumeration<TDataEntity>; inline;

    /// <summary>
    ///   Метаданные индекса
    /// </summary>
    property Meta : TMetaCollectionIndexSingle  read GetMeta;
  end;
  {$ENDIF}

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Данные индекса списка сущностей по одному полю. Генерик
  /// </summary>
  TDataCollectionIndexSingle<T> = class(TDataCollectionIndexSingle)
  private
    FIndexDict : TObjectDictionary<T, TObject>;

  protected
    constructor InternalCreate(const AContext : TDataObject.TCreateContext); override;
    procedure Clear(); override;

    function GetIndexItem(const AValue : TValue) : TObject; override;
    procedure AddIndexItem(const ADataField : TDataField); override;
    procedure RemoveIndexItem(const ADataField : TDataField); override;
    function KeyFromField(const ADataField : TDataField) : T; virtual;
    function GetFinder(const AValue : TValue; const ACompareOperation : TEntitiesCompareOperation) : TDataFinderWithEnumeration<TDataEntity>; override;

  public
    destructor Destroy; override;

    /// <summary>
    ///   Создать экземпляр поиска по индексу
    /// </summary>
    function MakeFinderRaw<TItem : class>(const AValue : T; const ACompareOperation : TEntitiesCompareOperation = ecoEquals) : TDataFinderWithEnumeration<TItem>;

    /// <summary>
    ///   Поиск по индексу
    /// </summary>
    function Finder(const AValue : T; const ACompareOperation : TEntitiesCompareOperation = ecoEquals) : TDataFinderWithEnumeration<TDataEntity>;

  end;
  {$ENDIF}

  /// <summary>
  ///   Данные индекса списка сущностей по одному полю. Integer
  /// </summary>
  TDataCollectionIndexSingleInteger =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataCollectionIndexSingleInteger;
  {$ELSE}
    class(TDataCollectionIndexSingle<Integer>);
  {$ENDIF}

  /// <summary>
  ///   Данные индекса списка сущностей по одному полю. Int64
  /// </summary>
  TDataCollectionIndexSingleInt64 =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataCollectionIndexSingleInt64;
  {$ELSE}
    class(TDataCollectionIndexSingle<Int64>);
  {$ENDIF}

  /// <summary>
  ///   Данные индекса списка сущностей по одному полю. String
  /// </summary>
  TDataCollectionIndexSingleString =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataCollectionIndexSingleInteger;
  {$ELSE}
    class(TDataCollectionIndexSingle<String>);
  {$ENDIF}

  /// <summary>
  ///   Данные индекса списка сущностей по одному полю. TGUID
  /// </summary>
  TDataCollectionIndexSingleGUID =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataCollectionIndexSingleGUID;
  {$ELSE}
    class(TDataCollectionIndexSingle<TGUID>);
  {$ENDIF}
  {$ENDREGION}

  /// <summary>
  ///   Данные списка сущностей
  /// </summary>
  TDataCollection =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataCollection;
  {$ELSE}
    class(TDataNodeEntityOrCollection<TMetaCollection, TDataField>)
  public type
    TEntitiesList = TObjectList<TDataEntity>;

  protected type
    {$REGION 'protected types'}
    /// <summary>
    ///   Абстрактные внутренние данные коллекции
    /// </summary>
    TDataCollectionInternalDataCustom = class abstract(TDataNodeEntityOrCollectionInternalData)
    strict private

    private
      FItems : TEntitiesList;
      FIndexes : TArray<TDataCollectionIndex>;

    protected
      procedure ClearIndexes();
      function GetOwnObjects : boolean; virtual; abstract;

    public
      constructor Create(const AOwner : TDataNodeEntityOrCollection); override;
      procedure ClearBeforeFree(const AOwner : TDataNodeEntityOrCollection); override;
    end;

    /// <summary>
    ///   Внутренние данные коллекции
    /// </summary>
    TDataCollectionInternalData = class (TDataCollectionInternalDataCustom)
    strict private

    protected
      function DoExportToJSON(const AOwner : TDataNodeEntityOrCollection;
        const ACompact : boolean) : TJSONValue; override;
      procedure DoImportFromJSON(const AOwner : TDataNodeEntityOrCollection;
        const AJSON : TJSONValue; const AReWrite : boolean); override;
      function GetOwnObjects : boolean; override;

    public

    end;

    /// <summary>
    ///   Внутренние данные ссылки на коллекцию c фильтрацией данных
    /// </summary>
    TDataCollectionInternalLink = class (TDataCollectionInternalDataCustom, IDataNodeEntityOrCollectionInternalLink)
    strict private
      FHandlers : TArray<TDataObject.TInternalNotifierData>;

    protected
      function GetTarget(const AOwner : TDataNodeEntityOrCollection) : TDataNodeEntityOrCollection; override;
      function GetOwnObjects : boolean; override;

      {$REGION 'IDataNodeEntityOrCollectionInternalLink'}
      procedure DetachLink(); virtual;
      {$ENDREGION}

    public
      procedure ClearBeforeFree(const AOwner : TDataNodeEntityOrCollection); override;
      procedure InternalDataCreated(const AOwner : TDataNodeEntityOrCollection); override;

      procedure Refill(const AOwner : TDataCollection);

      function GetResolvedLink(const AOwner : TDataCollection) : TPathResolver.TPathItem;

    end;

    {$ENDREGION}

  public type
    {$REGION 'public types'}
    /// <summary>
    ///   Фасад данных индексов
    /// </summary>
    TDataIndexesFacade = record
    strict private
      /// <summary>
      ///   Непосредственный источник индексов (может быть ссылка)
      /// </summary>
      FTarget : TDataCollection;

      function GetIndexes : TArray<TDataCollectionIndex>; inline;
      function GetCount: integer;
      function GetByIndex(const AIndex: integer): TDataCollectionIndex;

    private
      /// <summary>
      ///   Добавить во все индексы элемент
      /// </summary>
      procedure AddEntity(const AEntity : TDataEntity);

      /// <summary>
      ///   Убрать элемент из всех индексов
      /// </summary>
      procedure RemoveEntity(const AEntity : TDataEntity);

      property Indexes : TArray<TDataCollectionIndex>  read GetIndexes;

    public
      constructor Create(const ATarget : TDataCollection);

      /// <summary>
      ///   Попытка поиска метаиндекса по одному полю
      /// </summary>
      /// <param name="AMetaField">
      ///   Поле, по которому строится индекс
      /// </param>
      /// <param name="ADataEntitiesIndexSingle">
      ///   Найденый индекс
      /// </param>
      /// <returns>
      ///   <c>True</c> - успешно
      /// </returns>
      function TryFindSingleIndex(AMetaField : TMetaField;
        out ADataEntitiesIndexSingle : TDataCollectionIndexSingle) : boolean; overload;

      /// <summary>
      ///   Попытка поиска метаиндекса по одному полю
      /// </summary>
      /// <param name="AMetaField">
      ///   Поле, по которому строится индекс
      /// </param>
      /// <param name="ADataEntitiesIndexSingle">
      ///   Найденый индекс
      /// </param>
      /// <returns>
      ///   <c>True</c> - успешно
      /// </returns>
      function TryFindSingleIndex<T>(AMetaField : TMetaField;
        out ADataEntitiesIndexSingle : TDataCollectionIndexSingle<T>) : boolean; overload;

      /// <summary>
      ///   Количество полей
      /// </summary>
      property Count : integer  read GetCount;

      /// <summary>
      ///   Элементы — сущности
      /// </summary>
      property Items[const AIndex : integer] : TDataCollectionIndex  read GetByIndex; default;
    end;

    {$REGION 'Selection'}
    TSelection<T : class> = record
    private type
      TIternalData = class(TInterfacedObject, IObjRef)
      private
        FTarget : TDataCollection;

      protected
        {$REGION 'IObjRef'}
        function GetObjRef : TObject;
        {$ENDREGION}

      public
        constructor Create(const ATarget : TDataCollection);

      end;

    public type
      PLeftSideExpression = ^TLeftSideExpression;

      TEndOfConditionEnumerator = class
      strict private
        FInternalData : IInterface;
        FIndex: Integer;
        function GetCurrent: T;

      public
        constructor Create(const AIternalData : IInterface);

        property Current: T read GetCurrent;
        function MoveNext: Boolean;
      end;

      TEndOfCondition = record
      private
        FInternalData : IInterface;

      public
        function GetEnumerator() : TEndOfConditionEnumerator;

        function &And : PLeftSideExpression;
      end;

      TRightSideExpression = record
      private
        FInternalData : IInterface;

      public
        function Field(const AMetaField : TMetaField) : TEndOfCondition; overload;
        function Field(const AFieldName : string) : TEndOfCondition; overload;
        function Value(const AValue : TValue) : TEndOfCondition;
      end;

      TExpressionCondition = record
      private
        FInternalData : IInterface;

      public
        function Eq : TRightSideExpression;
        function Gr : TRightSideExpression;
        function Ls : TRightSideExpression;
        function GrEq : TRightSideExpression;
        function LsEq : TRightSideExpression;
      end;

      TLeftSideExpression = record
      private
        FInternalData : IInterface;

      public
        function Field(const AMetaField : TMetaField) : TExpressionCondition; overload;
        function Field(const AFieldName : string) : TExpressionCondition; overload;
        function Value(const AValue : TValue) : TExpressionCondition;
      end;

      TSelect = record
      private
        FInternalData : IInterface;

      public
        function Where : TLeftSideExpression;
      end;

    end;

    {$ENDREGION}

    {$ENDREGION}

  strict private
    function GetTargetList: TEntitiesList; inline;
    function GetCapacity : integer;
    procedure SetCapacity(const ACapacity : integer);
    function GetCount : integer;
    procedure SetCount(const ACount : integer);
    function GetItems(const AIndex: integer): TDataEntity;

  private
    /// <summary>
    ///   Понизить последующие индексы
    /// </summary>
    procedure DecNextIndexes(const AStartIndex : integer);
    function GetIndexes: TDataIndexesFacade;
    function GetTarget: TDataCollection;

  protected
    class function MakeCollection(const AOwnObjects : boolean) : TEntitiesList; virtual;
    constructor InternalCreate(const AContext : TDataObject.TCreateContext); override;
    function GetInternalDataClass : TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalClass; override;
    function GetInternalDataLinkClass : TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalClass; override;
    procedure OwnedDestroing(const AOwned : TDataObject); override;
    function DoFindChildNode(const ANodeType : TEntitiesNodeType; const AName : string; out ADataNode : TDataNode) : boolean; override;

    /// <summary>
    ///   Попытка поиска элемента в коллекции по данным для импорта из JSON
    /// </summary>
    /// <param name="AJSON">
    ///   JSON-объект для импорта
    /// </param>
    /// <param name="AItem">
    ///   Найденый элемент при успехе
    /// </param>
    /// <returns>
    ///   <c>True</c> — успешно
    /// </returns>
    function TryFindByJSON(const AJSON : TJSONObject; out AItem : TDataEntity) : boolean; virtual;

    /// <summary>
    ///   Создать экземпляр поиска
    /// </summary>
    function MakeFinderRaw<T : class>(const APredicate : TPredicate<T>) : TDataFinderWithEnumeration<T>;

    /// <summary>
    ///   Создать экземпляр поиска по значению одного поля
    /// </summary>
    function MakeFinderByFldRaw<TItem : class; TFieldValue>(const AFieldName : string; const AFieldValue : TFieldValue) : TDataFinderWithEnumeration<TItem>;

    /// <summary>
    ///   Обработка событий целевого объекта списком ссылок
    /// </summary>
    procedure LinksListInternalNotifier(const AContext : TDataObject.TInNtfCtx);

    procedure EntitiesListNotify(Sender: TObject; const AItem: TDataEntity;
      AAction: TCollectionNotification); virtual;

    procedure NotifyLinkEvent(Sender: TDataCollection; const AItem: TDataEntity;
      AAction: TCollectionNotification);

    property TargetList : TEntitiesList  read GetTargetList;

  public
    destructor Destroy; override;

    procedure Refresh; override;

    function GetEnumerator : TEnumerator<TDataEntity>;

    /// <summary>
    ///   Выбор, похожый на SQL запрос
    /// </summary>
    /// <remarks>
    ///   Пока не реализован, т.к. очень трудоемок...
    /// </remarks>
    function Select : TSelection<TDataEntity>.TSelect;

    /// <summary>
    ///   Поиск перебором по предикату без учета индексов
    /// </summary>
    function Finder(const APredicate : TPredicate<TDataEntity>) : TDataFinderWithEnumeration<TDataEntity>;

    /// <summary>
    ///   Добавление элемента
    /// </summary>
    /// <param name="ADataEntity">
    ///   Добавляемая сущность
    /// </param>
    /// <returns>
    ///   Добавленная сущность
    /// </returns>
    function Add(const ADataEntity : TDataEntity) : TDataEntity; overload;

    /// <summary>
    ///   Созданее и добавление элемента
    /// </summary>
    /// <returns>
    ///   Добавленная сущность
    /// </returns>
    function Add() : TDataEntity; overload;

    /// <summary>
    ///   Вставка элемента
    /// </summary>
    /// <param name="AIndex">
    ///   Индекс поля
    /// </param>
    /// <param name="ADataEntity">
    ///   Добавляемая сущность
    /// </param>
    /// <returns>
    ///   Добавленная сущность
    /// </returns>
    function Insert(const AIndex : integer; const ADataEntity : TDataEntity) : TDataEntity;

    /// <summary>
    ///   Перестановка элементов местами
    /// </summary>
    /// <param name="AIndex1">
    ///   Первый индекс
    /// </param>
    /// <param name="AIndex2">
    ///   Второй индекс
    /// </param>
    procedure Exchange(const AIndex1, AIndex2 : integer);

    /// <summary>
    ///   Удаление элемента
    /// </summary>
    /// <param name="AIndex">
    ///   Добавляемое поле
    /// </param>
    procedure Delete(const AIndex : integer);

    /// <summary>
    ///   Выделенная память под количество элементов
    /// </summary>
    property Capacity : integer  read GetCapacity  write SetCapacity;

    /// <summary>
    ///   Количество полей
    /// </summary>
    property Count : integer  read GetCount  write SetCount;

    /// <summary>
    ///   Элементы — сущности
    /// </summary>
    property Items[const AIndex : integer] : TDataEntity  read GetItems; default;

    /// <summary>
    ///   Индексы
    /// </summary>
    property Indexes : TDataIndexesFacade  read GetIndexes;

    /// <summary>
    ///   Целевой объект.
    /// </summary>
    /// <remarks>
    ///   Если заполнена ссылка <c>Link</c> — соответствует ссылке,
    ///   иначе — <c>=Self</c>
    /// </remarks>
    property Target : TDataCollection  read GetTarget;
  end;
  {$ENDIF}

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Данные списка сущностей. Генерик
  /// </summary>
  /// <remarks>
  ///   Используется для создания Rich-классов-наследников
  /// </remarks>
  TDataCollection<T : class> = class(TDataCollection)
  public type
    TEntitiesList = TObjectList<T>;

  strict private
    function GetTargetList : TEntitiesList; inline;
    function GetItems(const AIndex: integer): T; inline;

  private
    property TargetList : TEntitiesList  read GetTargetList;

  protected
    class function MakeCollection(const AOwnObjects : boolean) : TDataCollection.TEntitiesList; override;

    constructor InternalCreate(const AContext : TDataObject.TCreateContext); override;

  public
    function GetEnumerator : TEnumerator<T>;

    /// <summary>
    ///   Поиск перебором по предикату без учета индексов
    /// </summary>
    function Finder(const APredicate : TPredicate<T>) : TDataFinderWithEnumeration<T>;

    /// <summary>
    ///   Добавление элемента
    /// </summary>
    /// <param name="ADataEntity">
    ///   Добавляемая сущность
    /// </param>
    /// <returns>
    ///   Добавленная сущность
    /// </returns>
    function Add(const ADataEntity : T) : T; overload;

    /// <summary>
    ///   Созданее и добавление элемента
    /// </summary>
    /// <returns>
    ///   Добавленная сущность
    /// </returns>
    function Add() : T; overload;

    /// <summary>
    ///   Вставка элемента
    /// </summary>
    /// <param name="AIndex">
    ///   Индекс поля
    /// </param>
    /// <param name="ADataEntity">
    ///   Добавляемая сущность
    /// </param>
    /// <returns>
    ///   Добавленная сущность
    /// </returns>
    function Insert(const AIndex : integer; const ADataEntity : T) : T;

    /// <summary>
    ///   Элементы — сущности
    /// </summary>
    property Items[const AIndex : integer] : T  read GetItems; default;
  end;
  {$ENDIF}

  /// <summary>
  ///   Данные корневой сущности
  /// </summary>
  TDataEntityRoot =
  {$IFDEF USE_ALIASES}
    San.Entities.TDataEntityRoot;
  {$ELSE}
    class(TDataEntity)
  protected type
    {$REGION 'protected types'}
    /// <summary>
    ///   Внутренние данные корневой сущности
    /// </summary>
    TDataEntityRootInternalData = class (TDataEntityInternalData)
    protected
      procedure DoExportToJSONObject(const AOwner : TDataEntity;
        const AJSON : TJSONObject; const ACompact : boolean); override;
    end;
    {$ENDREGION}

  strict private
    function GetMeta: TMetaEntityRoot; inline;
    procedure SetVersion(const AValue: integer);

  private
    FVersion: integer;

  protected
    class function GetMetaClass : TMetaNodeClass; override;
    class function GetDataOwnerClass : TDataObjectClass; override;
    class function CheckOwnerClass(const AOwner : TDataNode) : boolean; override;
    function GetInternalDataClass : TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalClass; override;

    constructor InternalCreate(const AContext : TDataObject.TCreateContext); override;

    procedure DoImportFromJSON(const AJSON : TJSONValue; const AReWrite : boolean); override;

  public
    /// <summary>
    ///   Метаданне
    /// </summary>
    /// <remarks>
    ///   Weak
    /// </remarks>
    property Meta : TMetaEntityRoot  read GetMeta;

    /// <summary>
    ///   Актуальная версия данных
    /// </summary>
    property Version : integer  read FVersion  write SetVersion;


  end;
  {$ENDIF}

  {$ENDREGION}

{$IFNDEF USE_ALIASES}
type
  /// <summary>
  ///   Доступ к методам работы с объектами.
  /// </summary>
  Api = record
  strict private
    class var FMetaRoots : TThreadList<TMetaEntityRoot>;

  private
    class var FHolder: IInterface;
    class procedure Initialize(); static;
    class procedure Finalize(); static;
    class function GetDebugJSON: boolean; static;
    class procedure SetDebugJSON(const AValue: boolean); static;

    class procedure GetDataWeakRef(var AWeakRef; out ATarget); static;
    class procedure SetDataWeakRef(var AWeakRef; const ATarget); static;

  public
    /// <summary>
    ///   Текущий поток не собирается более использовать сущности
    /// </summary>
    /// <remarks>
    ///   Очистить служебную информацию о блокировках данным потоком
    /// </remarks>
    class procedure DetachCurThread(); static;

    /// <summary>
    ///   Конвертация стоки в <c>TGUID</c>. Если нужно, добавляются фигурные скобки
    /// </summary>
    class function StringToGUIDEx(AString : string) : TGUID; static;

    /// <summary>
    ///   Записать значение в JSON
    /// </summary>
    class function ValueToJSON<T>(const AValue : T) : TJSONValue; static;

    /// <summary>
    ///   Считать значение из JSON
    /// </summary>
    class function JSONToValue<T>(const AJValue : TJSONValue) : T; static;

    /// <summary>
    ///   Все созданные корневые метасущности
    /// </summary>
    class property MetaRoots : TThreadList<TMetaEntityRoot>  read FMetaRoots;

    /// <summary>
    ///   Отладочные данные в JSON
    /// </summary>
    class property DebugJSON : boolean  read GetDebugJSON  write SetDebugJSON;
  end;

implementation

{$REGION 'uses'}
uses
  {$IFDEF MSWINDOWS}
  Winapi.Windows,
  {$ENDIF}

  {$REGION 'System'}
  System.RTLConsts,
  {$ENDREGION}

  {$REGION 'San'}
  San.Intfs,
  San.Messages,
  {$ENDREGION}

  San.Entities.Messages
;
{$ENDREGION}

const
  iRoot    = 0;
  iParent  = 1;
  KNOWN_PATHS : array [iRoot..iParent] of string =
  (
    PATH_ROOT,
    PATH_PARENT
  );

{$REGION 'subs'}
procedure ClearObjsArray(var AArray);
begin
  San.Api.Objs.ClearObjsArray(AArray);
end;

function IndexOfObjsArray(const AArray; const AObj : TObject) : integer;
var
  Arr : TArray<TObject>;
  i : integer;
begin
  Arr := TArray<TObject>(AArray);
  for i := Low(Arr) to High(Arr) do
    if AObj = Arr[i] then
      Exit(i);

  Result := -1;
end;

procedure CastObjsArray(const AInArray; out AOutArray);
begin
  TArray<TObject>(AOutArray) := TArray<TObject>(AInArray);
end;

function MetaNameFromField(const AMetaNode : TMetaNode) : string;
var
  Node : TMetaNode;
begin
  Node := AMetaNode.Owner;

  while Assigned(Node) and not (Node is TMetaField)  do
    Node := Node.Owner;

  if not Assigned(Node) then
    Result := ''
  else
    Result := TMetaField(Node).Name;
end;

procedure FillFieldsByStrIDRegistry(const AMetaFieldClassesArr : array of TMetaFieldClass);
var
  MFC : TMetaFieldClass;
begin
  for MFC in AMetaFieldClassesArr do
  try
    TMetaField.FieldsByStrIDRegistry.Add(MFC.FieldStrID, MFC);
  except
    EEntities.CreateResFmt(@s_San_InternalErrorForFmt, [MFC.ClassName]).RaiseOuter();
  end;
end;

function IsLinkData(const ADataObject : TDataObject) : boolean;
begin
  Result := Assigned(ADataObject)
        and (ADataObject is TDataNodeEntityOrCollection)
        and TDataNodeEntityOrCollection(ADataObject).IsLink;
end;

procedure FreeAndNilIfDataLink(var ADataObject; const ALinksOnly: boolean);
begin
  if not ALinksOnly or IsLinkData(TDataObject(ADataObject)) then
    FreeAndNil(ADataObject);
end;
{$ENDREGION}

type
  // no System.Contnrs
  TObjectList = TObjectList<TObject>;

  /// <summary>
  ///   Реализация слабой ссылки
  /// </summary>
  TDataWeakRefImpl = class(TInterfacedObject, IObjRef)
  strict private
    FTarget : TDataObject;
    FNotifierData : TDataObject.TInternalNotifierData;
    procedure SetTarget(const AValue: TDataObject);

  protected
    {$REGION 'IObjRef'}
    function GetObjRef : TObject;
    {$ENDREGION}

    procedure InternalNotifier(const AContext : TDataObject.TInternalNotificationContext);

  public
    class function Access(var ARef : IObjRef) : TDataWeakRefImpl;

    destructor Destroy; override;

    /// <summary>
    ///   Целевой объект, на который ссылаемся
    /// </summary>
    /// <remarks>
    ///   При задании, ссылаемся на новый объект
    /// </remarks>
    property Target : TDataObject  read FTarget  write SetTarget;
  end;

{$REGION 'TDataWeakRefImpl'}
class function TDataWeakRefImpl.Access(
  var ARef: IObjRef): TDataWeakRefImpl;
begin
  if Assigned(ARef) then
    Result := ARef.ObjRef as TDataWeakRefImpl

  else
  begin
    Result := Self.Create();
    ARef := Result;
  end;
end;

destructor TDataWeakRefImpl.Destroy;
begin
  FreeAndNil(FNotifierData);
  inherited;
end;

function TDataWeakRefImpl.GetObjRef: TObject;
begin
  Result := Self;
end;

procedure TDataWeakRefImpl.InternalNotifier(
  const AContext: TDataObject.TInternalNotificationContext);
begin
  if AContext.Action = inaClear then
    FTarget := nil;
end;

procedure TDataWeakRefImpl.SetTarget(const AValue: TDataObject);
begin
  if FTarget = AValue then
    Exit;

  FreeAndNil(FNotifierData);

  FTarget := AValue;

  if Assigned(FTarget) then
    FNotifierData := TDataObject(FTarget).InternalNotificationAdd(InternalNotifier);
end;
{$ENDREGION}

{$REGION 'TDataWeakRef'}
class operator TDataWeakRef<T>.Implicit(const ARef: TDataWeakRef<T>): T;
begin
  Result := ARef.Target;
end;

class operator TDataWeakRef<T>.Implicit(const ARef: TDataWeakRef<T>): boolean;
begin
  Result := ARef.IsAlive;
end;

class operator TDataWeakRef<T>.Implicit(const ATarget: T): TDataWeakRef<T>;
begin
  Result := TDataWeakRef<T>.Create(ATarget);
end;

class operator TDataWeakRef<T>.NotEqual(const A: TDataWeakRef<T>;
  const B: T): boolean;
begin
  Result := A.Target <> B;
end;

class operator TDataWeakRef<T>.NotEqual(const A: T;
  const B: TDataWeakRef<T>): boolean;
begin
  Result := A <> B.Target;
end;

class operator TDataWeakRef<T>.Equal(const A: TDataWeakRef<T>;
  const B: T): boolean;
begin
  Result := A.Target = B;
end;

class operator TDataWeakRef<T>.Equal(const A: T;
  const B: TDataWeakRef<T>): boolean;
begin
  Result := A = B.Target;
end;

class function TDataWeakRef<T>.Create(const ATarget: T): TDataWeakRef<T>;
begin
  Result.Target := ATarget;
end;

function TDataWeakRef<T>.GetTarget: T;
begin
  {$IFDEF DEBUG}
  San.Api.Objs.ValidateGen<T, TDataObject>();
  {$ENDIF}

  San.Entities.Api.GetDataWeakRef(Self, Result);
end;

procedure TDataWeakRef<T>.SetTarget(const AValue: T);
begin
  San.Api.Objs.ValidateGen<T, TDataObject>();
  San.Entities.Api.SetDataWeakRef(Self, AValue);
end;

function TDataWeakRef<T>.GetIsAlive: boolean;
begin
  Result := Assigned(Target);
end;

procedure TDataWeakRef<T>.Assign(const ASource: TDataWeakRef<T>);
begin
  Target := ASource.Target;
end;
{$ENDREGION}

{$REGION 'TEntitiesNodeTypeHelper'}
function TEntitiesNodeTypeHelper.ToString: string;
begin
  Result := GetEnumName(TypeInfo(TEntitiesNodeType), Ord(Self));
end;
{$ENDREGION}

{$REGION 'TEntitiesEventKindHelper'}
function TEntitiesEventKindHelper.ToString: string;
begin
  Result := GetEnumName(TypeInfo(TEntitiesEventKind), Ord(Self));
end;
{$ENDREGION}

{$REGION 'TEntitiesCompareOperationHelper'}
function TEntitiesCompareOperationHelper.Flip: TEntitiesCompareOperation;
begin
  case Self of
    ecoLessThan:             Result := ecoGreaterThan;
    ecoLessThanOrEquals:     Result := ecoGreaterThanOrEquals;
    ecoGreaterThanOrEquals:  Result := ecoLessThanOrEquals;
    ecoGreaterThan:          Result := ecoLessThan;
  else
    Result := Self;
  end;
end;

function TEntitiesCompareOperationHelper.ToString: string;
begin
  Result := GetEnumName(TypeInfo(TEntitiesCompareOperation), Ord(Self));
end;
{$ENDREGION}

{$REGION 'events'}

{$REGION 'TEntitiesEvent'}
constructor TEntitiesEvent.Create(const ASource: TEntitiesObject);
begin
  inherited Create();
  FSource := ASource;
end;
{$ENDREGION}

{$REGION 'TEntitiesInsertEvent'}
constructor TEntitiesInsertEvent.Create(const ASource, AItem: TEntitiesObject;
  const AIndex: integer);
begin
  inherited Create(ASource);
  FItem := AItem;
  FIndex := AIndex;
end;

function TEntitiesInsertEvent.GetKind: TEntitiesEventKind;
begin
  Result := enevkInsert;
end;
{$ENDREGION}

{$REGION 'TEntitiesDeleteEvent'}
constructor TEntitiesDeleteEvent.Create(const ASource, AItem: TEntitiesObject;
  const AIndex: integer);
begin
  inherited Create(ASource);
  FItem := AItem;
  FIndex := AIndex;
end;

function TEntitiesDeleteEvent.GetKind: TEntitiesEventKind;
begin
  Result := enevkDelete;
end;

function TEntitiesDeleteEvent.IsDeletedItem(
  const AItem: TEntitiesObject): boolean;
begin
  Result := AItem = FItem;
end;
{$ENDREGION}

{$REGION 'TEntitiesMoveEvent'}
constructor TEntitiesMoveEvent.Create(const ASource, AItem: TEntitiesObject;
  const AOldIndex, ANewIndex: integer);
begin
  inherited Create(ASource);
  FItem := AItem;
  FOldIndex := AOldIndex;
  FNewIndex := ANewIndex;
end;

function TEntitiesMoveEvent.GetKind: TEntitiesEventKind;
begin
  Result := enevkMove;
end;
{$ENDREGION}

{$REGION 'TEntitiesChangeEvent'}
constructor TEntitiesChangeEvent.Create(const ASource: TEntitiesObject;
  const AField  : TDataField; const AOldValue, ANewValue: TValue);
begin
  inherited Create(ASource);
  FField := AField;
  FOldValue := AOldValue;
  FNewValue := ANewValue;
end;

function TEntitiesChangeEvent.GetKind: TEntitiesEventKind;
begin
  Result := enevkChange;
end;
{$ENDREGION}

{$REGION 'TMEEntities.Context'}
constructor TMEEntities.Context.Create(const AEvents: TArray<TEntitiesEvent>);
begin
  inherited Create();
  FEvents := AEvents;
end;

function TMEEntities.Context.TryGet(const AFieldName: string;
  out AEvent: TEntitiesChangeEvent): boolean;
var
  e : TEntitiesEvent;
  ec : TEntitiesChangeEvent;
begin
  for e in FEvents do
    if e.Kind = enevkChange then
    begin
      ec := e as TEntitiesChangeEvent;

      if  AnsiSameStr(ec.FField.Name, AFieldName) then
      begin
        AEvent := ec;
        Exit(True);
      end;
    end;

  Result := False;
end;

function TMEEntities.Context.TryGet(const AFieldName: string;
  out AEvents: TArray<TEntitiesChangeEvent>): boolean;
var
  e : TEntitiesEvent;
  ec : TEntitiesChangeEvent;
begin
  for e in FEvents do
    if e.Kind = enevkChange then
    begin
      ec := e as TEntitiesChangeEvent;

      if  AnsiSameStr(ec.FField.Name, AFieldName) then
        System.Insert(ec, AEvents, Length(AEvents));
    end;

  Result := Length(AEvents) > 0;
end;

function TMEEntities.Context.TryGet(const AFieldNames: array of string;
  out AEvent: TEntitiesChangeEvent): boolean;
var
  e : TEntitiesEvent;
  ec : TEntitiesChangeEvent;
begin
  for e in FEvents do
    if e.Kind = enevkChange then
    begin
      ec := e as TEntitiesChangeEvent;

      if AnsiMatchStr(ec.FField.Name, AFieldNames) then
      begin
        AEvent := ec;
        Exit(True);
      end;
    end;

  Result := False;
end;

function TMEEntities.Context.TryGet(const AFieldNames: array of string;
  out AEvents: TArray<TEntitiesChangeEvent>): boolean;
var
  e : TEntitiesEvent;
  ec : TEntitiesChangeEvent;
begin
  for e in FEvents do
    if e.Kind = enevkChange then
    begin
      ec := e as TEntitiesChangeEvent;

      if  AnsiMatchStr(ec.FField.Name, AFieldNames) then
        System.Insert(ec, AEvents, Length(AEvents));
    end;

  Result := Length(AEvents) > 0;
end;

{$ENDREGION}

{$REGION 'TMEEntities'}
class function TMEEntities.EventName: string;
begin
  Result := ClassName;
end;

class function TMEEntities.Description: string;
begin
  Result := UnitName + '.' + ClassName;
end;

function TMEEntities.MatchSubscribeAttr(
  const AAttr: TMetaEvent.Subtypes.SubscribeAttribute): boolean;
begin
  Result := AAttr is EvMEEntities;
end;

function TMEEntities.Fire(const ASender: TObject;
  const AEvents: TArray<TEntitiesEvent>): TMetaEvent.Subtypes.TFiring;
begin
  Result := inherited Fire(ASender, Context.Create(AEvents));
end;
{$ENDREGION}


{$ENDREGION}

{$REGION 'base'}

{$REGION 'TArrayEnumerator<T>'}
constructor TArrayEnumerator<T>.Create(const AArray: TArray<T>);
begin
  inherited Create();
  FArray := AArray;
end;

function TArrayEnumerator<T>.GetCurrent: T;
begin
  Result := FArray[FIndex];
end;

function TArrayEnumerator<T>.MoveNext: Boolean;
begin
  if FIndex >= Length(FArray) then
    Exit(False);
  Inc(FIndex);
  Result := FIndex < Length(FArray);
end;
{$ENDREGION}

{$REGION 'TPathResolver.TLexemeKindHelper'}
function TPathResolver.TLexemeKindHelper.ToCompareKind: TEntitiesCompareOperation;
begin
  case Self of
    lxkNotEquals:            Result := ecoNotEquals;
    lxkLessThan:             Result := ecoLessThan;
    lxkLessThanOrEquals:     Result := ecoLessThanOrEquals;
    lxkEquals:               Result := ecoEquals;
    lxkGreaterThanOrEquals:  Result := ecoGreaterThanOrEquals;
    lxkGreaterThan:          Result := ecoGreaterThan;

  else
    Result := ecoUnknown;
  end;
end;

function TPathResolver.TLexemeKindHelper.ToString: string;
begin
  Result := GetEnumName(TypeInfo(TPathResolver.TLexemeKind), Ord(Self));
end;
{$ENDREGION}

{$REGION 'TPathResolver.TLexeme'}
class operator TPathResolver.TLexeme.Equal(const A, B: TLexeme): boolean;
begin
  Result := (A.Kind = B.Kind) and (A.Start = B.Start) and (A.Length = B.Length);
end;

class operator TPathResolver.TLexeme.NotEqual(const A, B: TLexeme): boolean;
begin
  Result := (A.Kind <> B.Kind) or (A.Start <> B.Start) or (A.Length <> B.Length);
end;

class operator TPathResolver.TLexeme.Equal(const A: TLexeme;
  const B: TLexemeKind): boolean;
begin
  Result := A.Kind = B;
end;

class operator TPathResolver.TLexeme.NotEqual(const A: TLexeme;
  const B: TLexemeKind): boolean;
begin
  Result := A.Kind <> B;
end;

class operator TPathResolver.TLexeme.Implicit(const A: TLexeme): TLexemeKind;
begin
  Result := A.Kind;
end;

class operator TPathResolver.TLexeme.In(const A: TLexeme;
  const B: TLexemesKinds): Boolean;
begin
  Result := A.Kind in B;
end;

constructor TPathResolver.TLexeme.Create(const AKind: TLexemeKind; const AStart,
  ALength: integer);
begin
  FKind    := AKind;
  FStart   := AStart;
  FLength  := ALength;
end;

function TPathResolver.TLexeme.GetValue(const AText: string): string;
begin
  Result := System.Copy(AText, FStart, FLength);
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathItem'}
constructor TPathResolver.TPathItem.Create(const AOwner: TObject);
begin
  inherited Create();
  FOwner := AOwner;
end;

destructor TPathResolver.TPathItem.Destroy;
begin
  FreeAndNil(FChild);
  inherited;
end;

procedure TPathResolver.TPathItem.DoIsTargetStatic(var AResult: Bool3);
begin
end;

function TPathResolver.TPathItem.GetUltimateChild: TPathItem;
begin
  Result := Self;
  while Assigned(Result.Child) do
    Result := Result.Child;
end;

function TPathResolver.TPathItem.IsEndTargetStatic: Bool3;
var
  Item : TPathItem;
begin
  Result := Null3;
  Item := Self;
  while Assigned(Item) and not Result.IsFalse do
  begin
    Item.DoIsTargetStatic(Result);
    Item := Item.Child;
  end;
end;

function TPathResolver.TPathItem.GetCanHasMultiTargets: boolean;
begin
  Result := False;
end;

function TPathResolver.TPathItem.GetTargetsData(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TDataNode): TArray<TDataNode>;
begin
  System.Insert(TargetData[ANodeType, AContext, AThis], Result, 0);
end;

function TPathResolver.TPathItem.FindEndTargetMeta(const ANodeType : TEntitiesNodeType;
        const AContext, AThis: TMetaNode): TMetaNode;
var
  PathItem : TPathItem;
begin
  PathItem := Self;
  Result := AContext;

  repeat
    Result := PathItem.TargetMeta[ANodeType, Result, AThis];

    PathItem := PathItem.Child;
  until not Assigned(PathItem);

end;

function TPathResolver.TPathItem.FindEndTargetData(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TDataNode): TDataNode;
var
  PathItem : TPathItem;
begin
  PathItem := Self;
  Result := AContext;

  repeat
    Result := PathItem.TargetData[ANodeType, Result, AThis];

    PathItem := PathItem.Child;
  until not Assigned(PathItem);

end;

function TPathResolver.TPathItem.FindEndTargetsData(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TDataNode): TArray<TDataNode>;

  procedure DoFind(const APathItem : TPathItem; const AContexts : TArray<TDataNode>);
  var
    Context : TDataNode;
    Targets : TArray<TDataNode>;
  begin
    for Context in AContexts do
    begin
      Targets := APathItem.TargetsData[ANodeType, Context, AThis];
      if Assigned(APathItem.Child) then
        DoFind(APathItem.Child, Targets)
      else
        System.Insert(Targets, Result, Length(Result));
    end;
  end;

begin
  DoFind(Self, TArray<TDataNode>.Create(AContext));
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathItemSysRoot'}
function TPathResolver.TPathItemSysRoot.GetTargetMeta(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TMetaNode): TMetaNode;
begin
  Result := AContext.Root
end;

procedure TPathResolver.TPathItemSysRoot.DoIsTargetStatic(var AResult: Bool3);
begin
  // Не рассматриваем вариант одновременного применения одного пути к разным корням
  AResult := True3;
end;

function TPathResolver.TPathItemSysRoot.GetTargetData(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TDataNode): TDataNode;
begin
  Result := AContext.Root
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathItemSysParent'}
function TPathResolver.TPathItemSysParent.GetTargetMeta(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TMetaNode): TMetaNode;
begin
  if not Assigned(AContext.Parent) then
    raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingParentIsNilFmt, [AContext.Path]);

  Result := AContext.Parent;
end;

procedure TPathResolver.TPathItemSysParent.DoIsTargetStatic(var AResult: Bool3);
begin
  AResult := False3;
end;

function TPathResolver.TPathItemSysParent.GetTargetData(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TDataNode): TDataNode;
begin
  if not Assigned(AContext.Parent) then
    raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingParentIsNilFmt, [AContext.Path]);

  Result := AContext.Parent;
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathItemSysThis'}
function TPathResolver.TPathItemSysThis.GetTargetMeta(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TMetaNode): TMetaNode;
begin
  Result := AThis;
end;

procedure TPathResolver.TPathItemSysThis.DoIsTargetStatic(var AResult: Bool3);
begin
  AResult := True3;
end;

function TPathResolver.TPathItemSysThis.GetTargetData(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TDataNode): TDataNode;
begin
  Result := AThis;
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathItemStatic'}
procedure TPathResolver.TPathItemStatic.DoIsTargetStatic(var AResult: Bool3);
begin
  AResult := True3;
end;

function TPathResolver.TPathItemStatic.GetTargetData(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TDataNode): TDataNode;
begin
  Result := nil;
end;

function TPathResolver.TPathItemStatic.GetTargetMeta(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TMetaNode): TMetaNode;
begin
  Result := nil;
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathItemStatic<T>'}
function TPathResolver.TPathItemStatic<T>.CompareDataTo(
  const AOther: TPathItemStatic;
  const ACompareOperation: TEntitiesCompareOperation): Bool3;
var
  Comparer : IEqualityComparer<T>;
begin
  if (ACompareOperation in [ecoNotEquals, ecoEquals]) and (AOther is TPathItemStatic<T>) then
  begin
    Comparer := TEqualityComparer<T>.Default;

    Result := Comparer.Equals(FValue, TPathItemStatic<T>(AOther).FValue);

    if ACompareOperation = ecoNotEquals then
      Result := not Result;
  end

  else
    Result := Null3;
end;

function TPathResolver.TPathItemStatic<T>.GetValue: TValue;
begin
  Result := TValue.From<T>(FValue);
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathItemIdentifier'}
function TPathResolver.TPathItemIdentifier.GetTargetMeta(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TMetaNode): TMetaNode;
begin
  if not AContext.DoFindChildNode(ANodeType, FName, Result) then
    raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingChildNotFoundFmt, [FName, AContext.Path]);
end;

function TPathResolver.TPathItemIdentifier.GetTargetData(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TDataNode): TDataNode;
begin
  if not AContext.DoFindChildNode(ANodeType, FName, Result) then
    raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingChildNotFoundFmt, [FName, AContext.Path]);
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathFilterCondition'}
function TPathResolver.TPathFilterCondition.Negative(
  const ACollection: TDataCollection;
  const APositiveResults: TArray<TDataNode>): TArray<TDataNode>;

  function IsInArr(const ADataNode : TDataNode) : boolean;
  var
    i : integer;
  begin
    for i := 0 to High(APositiveResults) do
      if ADataNode = APositiveResults[i] then
        Exit(True);

    Result := False;
  end;

var
  i : integer;
begin
  for i := 0 to ACollection.Count - 1 do
    if not IsInArr(ACollection[i]) then
      System.Insert(ACollection[i], Result, Length(Result));
end;

function TPathResolver.TPathFilterCondition.FilterByCheckAll(
  const ACollection: TDataCollection;
  const AThis: TDataNode): TArray<TDataNode>;
var
  Item : TDataEntity;
begin
  for Item in ACollection do
    if Check(Item, AThis) then
      System.Insert(Item, Result, Length(Result));
end;

function TPathResolver.TPathFilterCondition.Check(const AContext,
  AThis: TDataNode): boolean;
begin
  Result := DoCheck(AContext, AThis);
  if FIsNegative then
    Result := not Result;
end;

function TPathResolver.TPathFilterCondition.Filter(
  const ACollection : TDataCollection; const AThis: TDataNode) : TArray<TDataNode>;
begin
  Result := DoFilter(ACollection, AThis);
end;

function TPathResolver.TPathFilterCondition.IsStatic: Bool3;
begin
  Result := Null3;
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathFilterMultiCondition'}
constructor TPathResolver.TPathFilterMultiCondition.Create(
  const ASubConditions: array of TPathFilterCondition);
var
  i : integer;
begin
  inherited Create();
  SetLength(FSubConditions, Length(ASubConditions));
  for i := 0 to High(ASubConditions) do
    FSubConditions[i] := ASubConditions[i];
end;

destructor TPathResolver.TPathFilterMultiCondition.Destroy;
begin
  ClearObjsArray(FSubConditions);
  inherited;
end;

function TPathResolver.TPathFilterMultiCondition.IsStatic: Bool3;
var
  C : TPathFilterCondition;
  B : Bool3;
begin
  Result := True3;

  for C in FSubConditions do
  begin
    B := C.IsStatic;
    case B.Value of
      False3:
        Exit(B);

      Null3:
        Result := B;
    end;
  end;

end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathFilterAndCondition'}
function TPathResolver.TPathFilterAndCondition.DoCheck(const AContext, AThis: TDataNode) : boolean;
var
  C : TPathFilterCondition;
begin
  for C in FSubConditions do
    if not C.Check(AContext, AThis) then
      Exit(False);

  Result := True;
end;

function TPathResolver.TPathFilterAndCondition.DoFilter(
  const ACollection : TDataCollection; const AThis: TDataNode): TArray<TDataNode>;

  function AndCheck(const ADataNode : TDataNode) : boolean;
  var
    i : integer;
  begin
    for i := 1 to High(SubConditions) do
      if not SubConditions[i].Check(ADataNode, AThis) then
        Exit(False);

    Result := True;
  end;

var
  i : integer;
begin
  Result := FSubConditions[0].Filter(ACollection, AThis);
  for i := High(Result) downto 0 do
    if not AndCheck(Result[i]) then
      System.Delete(Result, i, 1);

  if IsNegative then
    Result := Negative(ACollection, Result);
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathFilterOrCondition'}
function TPathResolver.TPathFilterOrCondition.DoCheck(const AContext, AThis: TDataNode) : boolean;
var
  C : TPathFilterCondition;
begin
  for C in FSubConditions do
    if C.Check(AContext, AThis) then
      Exit(True);

  Result := False;
end;

function TPathResolver.TPathFilterOrCondition.DoFilter(
  const ACollection : TDataCollection; const AThis: TDataNode): TArray<TDataNode>;
var
  C : TPathFilterCondition;
begin
  for C in FSubConditions do
    System.Insert(C.Filter(ACollection, AThis), Result, Length(Result));

  if IsNegative then
    Result := Negative(ACollection, Result);
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathFilterBoolFieldCheckCondition'}
constructor TPathResolver.TPathFilterBoolFieldCheckCondition.Create(
  const AFieldPath: TPathItem);
begin
  inherited Create;
  FFieldPath := AFieldPath;
  FFieldPath.FOwner := Self;
end;

destructor TPathResolver.TPathFilterBoolFieldCheckCondition.Destroy;
begin
  FreeAndNil(FFieldPath);
  inherited;
end;

function TPathResolver.TPathFilterBoolFieldCheckCondition.DoCheck(const AContext, AThis: TDataNode): boolean;
var
  DFld : TDataField;
begin
  Assert(not (FFieldPath is TPathItemStatic));
  DFld := DataField[AContext, AThis];
  Result := DFld.AsBoolean;
end;

function TPathResolver.TPathFilterBoolFieldCheckCondition.DoFilter(
  const ACollection : TDataCollection; const AThis: TDataNode): TArray<TDataNode>;
var
  MFld : TMetaField;
  DataColIdx : TDataCollectionIndexSingle;
begin
  Assert(not (FFieldPath is TPathItemStatic));

  MFld := MetaField[ACollection.Meta, AThis.Meta];
  if ACollection.Indexes.TryFindSingleIndex(MFld, DataColIdx) then
    DataColIdx.Finder( TValue.From<boolean>(not Self.IsNegative)).FindRaw(Result)

  else
    Result := FilterByCheckAll(ACollection, AThis);
end;

function TPathResolver.TPathFilterBoolFieldCheckCondition.GetMetaField(
  const AContext, AThis: TMetaNode): TMetaField;
var
  Node : TMetaNode;
begin
  Node := FFieldPath.FindEndTargetMeta(enntField, AContext, AThis);
  if Assigned(Node) and not (Node is TMetaField) then
    raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingFieldsConditionTargetMustBeFieldFmt, [Node.UnitName, Node.ClassName, AContext.Path]);

  Result := TMetaField(Node);
end;

function TPathResolver.TPathFilterBoolFieldCheckCondition.GetDataField(
  const AContext, AThis: TDataNode): TDataField;
var
  Node : TDataNode;
begin
  Node := FFieldPath.FindEndTargetData(enntField, AContext, AThis);
  if Assigned(Node) and not (Node is TDataField) then
    raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingFieldsConditionTargetMustBeFieldFmt, [Node.UnitName, Node.ClassName, AContext.Path]);

  Result := TDataField(Node);
end;

function TPathResolver.TPathFilterBoolFieldCheckCondition.IsStatic: Bool3;
begin
  Result := FFieldPath.IsEndTargetStatic;
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathFilterFieldsCompareCondition'}
class function TPathResolver.TPathFilterFieldsCompareCondition.GetMetaField(
  const APathItem: TPathItem; const AContext, AThis: TMetaNode;
  const AErrMsg: PResStringRec): TMetaField;
var
  Node : TMetaNode;
begin
  Node := APathItem.FindEndTargetMeta(enntField, AContext, AThis);
  if Assigned(Node) and not (Node is TMetaField) then
    raise EEntitiesResolvingError.CreateResFmt(AErrMsg, [Node.UnitName, Node.ClassName, AContext.Path]);

  Result := TMetaField(Node);
end;

class function TPathResolver.TPathFilterFieldsCompareCondition.GetDataField(
  const APathItem: TPathItem; const AContext, AThis: TDataNode;
  const AErrMsg: PResStringRec): TDataField;
var
  Node : TDataNode;
begin
  Node := APathItem.FindEndTargetData(enntField, AContext, AThis);
  if Assigned(Node) and not (Node is TDataField) then
    raise EEntitiesResolvingError.CreateResFmt(AErrMsg, [Node.UnitName, Node.ClassName, AContext.Path]);

  Result := TDataField(Node);
end;

constructor TPathResolver.TPathFilterFieldsCompareCondition.Create(const ALeft,
  ARight: TPathItem; const ACompareOperation: TEntitiesCompareOperation);
begin
  inherited Create();
  FLeft   := ALeft;
  FLeft.FOwner := Self;
  FRight  := ARight;
  FRight.FOwner := Self;
  FCompareOperation := ACompareOperation;
end;

destructor TPathResolver.TPathFilterFieldsCompareCondition.Destroy;
begin
  FreeAndNil(FLeft);
  FreeAndNil(FRight);
  inherited;
end;

function TPathResolver.TPathFilterFieldsCompareCondition.DoCheck(const AContext,
  AThis: TDataNode) : boolean;
var
  State : integer;
  DFldL, DFldR : TDataField;
begin
  State := Ord(FLeft is TPathItemStatic) + 2 * Ord(FRight is TPathItemStatic);
  case State of
    // !StaticLeft && !StaticRight
    0:
      begin
        DFldL := LeftDataField[ AContext, AThis];
        DFldR := RightDataField[AContext, AThis];
        Result := DFldL.CompareDataFieldTo(DFldR, CompareOperation);
      end;

    // StaticLeft && !StaticRight
    1:
      begin
        DFldR := RightDataField[AContext, AThis];
        Result := DFldR.CompareDataValueTo(TPathItemStatic(FLeft).Value, CompareOperation.Flip);
      end;

    // !StaticLeft && StaticRight
    2:
      begin
        DFldL := LeftDataField[ AContext, AThis];
        Result := DFldL.CompareDataValueTo(TPathItemStatic(FRight).Value, CompareOperation);
      end;

    // StaticLeft && StaticRight
    3:
      Result := TPathItemStatic(FLeft).CompareDataTo(TPathItemStatic(FRight), FCompareOperation);
  else
    San.Api.Excps.ItsImpossible(Result);
  end;
end;

function TPathResolver.TPathFilterFieldsCompareCondition.DoFilter(
  const ACollection : TDataCollection; const AThis: TDataNode): TArray<TDataNode>;
var
  Indexes : TDataCollection.TDataIndexesFacade;

  function DoFindByFieldsCondition(const AMetaField : TMetaField;
    const AOtherPathItem : TPathItem;
    const AIsStatic : boolean;
    const AResStr : PResStringRec;
    out AResults : TArray<TDataNode>) : boolean;
  var
    Item : TDataEntity;
    DFld : TDataField;
    DataIndex : TDataCollectionIndexSingle;
    i : integer;
    Exists : boolean;
  begin
    Result := Indexes.TryFindSingleIndex(AMetaField, DataIndex);
    if not Result then
      Exit;

    if AIsStatic then
    begin
      if AOtherPathItem is TPathItemStatic then
        DataIndex.Finder( TPathItemStatic(AOtherPathItem).Value, CompareOperation).TryFindRaw(AResults)

      else
      begin
        // Известно, что значение в этом месте не зависит от контекста,
        // в качаестве контекста можно передать что угодно,
        // здесь передается nil
        DFld := TPathFilterFieldsCompareCondition.GetDataField(AOtherPathItem, nil, AThis, AResStr);
        DataIndex.Finder(DFld.AsValue, CompareOperation).TryFindRaw(AResults);
      end;

      if IsNegative then
        AResults := Negative(ACollection, AResults);
    end
    else
      for i := 0 to ACollection.Count - 1 do
      begin
        Item := ACollection[i];
        DFld := TPathFilterFieldsCompareCondition.GetDataField(AOtherPathItem, Item, AThis, AResStr);
        Exists := DataIndex.Finder(DFld.AsValue, CompareOperation).Exists();
        if not IsNegative and Exists or IsNegative and not Exists then
          System.Insert(Item, AResults, Length(AResults));
      end;
  end;

var
  LStatic, RStatic : boolean;

  function MaybeFindByIndex() : boolean;
  begin

    if (CompareOperation <> ecoNotEquals) and not(LStatic and RStatic) then
    begin
      Indexes := ACollection.Indexes;
      if Indexes.Count > 0 then
        Exit(True);
    end;

    Result := False;
  end;

begin
  LStatic := Left.IsEndTargetStatic.IsTrue;
  RStatic := Right.IsEndTargetStatic.IsTrue;

  if MaybeFindByIndex() then
    if not LStatic and DoFindByFieldsCondition
      (
        LeftMetaField[ACollection.Meta.ItemMeta, AThis.Meta],
        FRight,
        RStatic,
        @s_San_Entities_ResolvingFieldsConditionRightTargetMustBeFieldFmt,
        Result
      )
    or not RStatic and DoFindByFieldsCondition
      (
        RightMetaField[ACollection.Meta.ItemMeta, AThis.Meta],
        FLeft,
        LStatic,
        @s_San_Entities_ResolvingFieldsConditionLeftTargetMustBeFieldFmt,
        Result
      )
    then
      Exit;

  Result := FilterByCheckAll(ACollection, AThis);
end;

function TPathResolver.TPathFilterFieldsCompareCondition.GetLeftMetaField(
  const AContext, AThis: TMetaNode): TMetaField;
begin
  Result := GetMetaField(FLeft, AContext, AThis, @s_San_Entities_ResolvingFieldsConditionLeftTargetMustBeFieldFmt);
end;

function TPathResolver.TPathFilterFieldsCompareCondition.GetRightMetaField(
  const AContext, AThis: TMetaNode): TMetaField;
begin
  Result := GetMetaField(FRight, AContext, AThis, @s_San_Entities_ResolvingFieldsConditionRightTargetMustBeFieldFmt);
end;

function TPathResolver.TPathFilterFieldsCompareCondition.GetLeftDataField(
  const AContext, AThis: TDataNode): TDataField;
begin
  Result := GetDataField(FLeft, AContext, AThis, @s_San_Entities_ResolvingFieldsConditionLeftTargetMustBeFieldFmt);
end;

function TPathResolver.TPathFilterFieldsCompareCondition.GetRightDataField(
  const AContext, AThis: TDataNode): TDataField;
begin
  Result := GetDataField(FRight, AContext, AThis, @s_San_Entities_ResolvingFieldsConditionRightTargetMustBeFieldFmt);
end;

function TPathResolver.TPathFilterFieldsCompareCondition.IsStatic: Bool3;
begin
  Result := FLeft.IsEndTargetStatic and FRight.IsEndTargetStatic;
end;
{$ENDREGION}

{$REGION 'TPathResolver.TPathItemFilter'}
destructor TPathResolver.TPathItemFilter.Destroy;
begin
  FreeAndNil(FCondition);
  inherited;
end;

procedure TPathResolver.TPathItemFilter.DoIsTargetStatic(var AResult: Bool3);
begin
  AResult := FCondition.IsStatic;
end;

function TPathResolver.TPathItemFilter.GetCanHasMultiTargets: boolean;
begin
  Result := True;
end;

function TPathResolver.TPathItemFilter.GetTargetData(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TDataNode): TDataNode;
var
  Targets : TArray<TDataNode>;
begin
  Targets := TargetsData[ANodeType, AContext, AThis];
  case Length(Targets) of
    0:
      Result := nil;

    1:
      Result := Targets[0];
  else
    raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingFilterMultiTargetsDataFmt, [AContext.Path, AThis.Path]);
  end;
end;

function TPathResolver.TPathItemFilter.GetTargetsData(const ANodeType : TEntitiesNodeType;
  const AContext, AThis: TDataNode): TArray<TDataNode>;
var
  IdfNode : TDataNode;
  Collection : TDataCollection;
begin
  IdfNode := inherited GetTargetData(ANodeType, AContext, AThis);

  if ANodeType = enntField then
  begin
    System.Insert(IdfNode, Result, Length(Result));
    Exit();
  end;

  if IdfNode is TDataCollection then
    Collection := IdfNode as TDataCollection

  else
  begin
    if not (IdfNode is TDataFieldCollection) then
      raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingFilterTargetMustBeCollectionFmt, [IdfNode.UnitName, IdfNode.ClassName, AContext.Path]);

    Collection := TDataFieldCollection(IdfNode).AsItems;
  end;

  if ANodeType = enntCollection then
  begin
    System.Insert(Collection, Result, Length(Result));
    Exit();
  end;

  if Collection.Count > 0 then
    Result := Condition.Filter(Collection, AThis);
end;
{$ENDREGION}

{$REGION 'TPathResolver.TLexicalAnalizer'}
constructor TPathResolver.TLexicalAnalizer.Create(const AText: string);
begin
  FText := AText;
end;

function TPathResolver.TLexicalAnalizer.Parse: TArray<TLexeme>;

  procedure Add(const AKind: TLexemeKind; const AStart, ALength: integer);
  begin
    System.Insert(TLexeme.Create(AKind, AStart, ALength), Result, Length(Result));
  end;

var
  i : integer;

  function PeekNextCharIs(const AChar : Char) : boolean;
  begin
    Result := (Succ(i) <= Length(FText)) and (FText[Succ(i)] = AChar);
  end;

  function PeekNextCharIn(const ASet : TSysCharSet) : boolean;
  begin
    Result := (Succ(i) <= Length(FText)) and CharInSet(FText[Succ(i)], ASet);
  end;

  function PopNextCharIs(const AChar : Char) : boolean;
  begin
    Result := PeekNextCharIs(AChar);

    if Result then
      Inc(i);
  end;

  procedure RaiseUnexpected(const AStr : string); overload;
  begin
    raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingPathUnexpectedEndFmt, [AStr]);
  end;

  procedure RaiseUnexpected(const ALexemeKind : TLexemeKind); overload;
  begin
    RaiseUnexpected(ALexemeKind.ToString);
  end;

  procedure Next(const ALexemeKind : TLexemeKind);
  begin
    Inc(i);
    if i > Length(FText) then
      RaiseUnexpected(ALexemeKind);
  end;

  function TryAddSys(const ASys : string; const AKind: TLexemeKind) : boolean;
  var
    L : integer;
  begin
    L := Length(ASys);

    Result := (Length(FText) - i + 1 >= L) and AnsiSameText(Copy(FText, i, L), ASys);
    if not Result then
      Exit;

    Add(AKind, i, L);
    Inc(i, L);
  end;

  function AddTryEqPair(const AKind1, AKind2: TLexemeKind) : boolean;
  begin
    Result := PeekNextCharIs('=');

    if Result then
    begin
      Add(AKind2, i, 2);
      Inc(i, 2)
    end

    else
    begin
      Add(AKind1, i, 1);
      Inc(i, 1)
    end;
  end;

  procedure AddDub(const AChar : Char; const AKind: TLexemeKind);
  begin
    if PeekNextCharIs(AChar) then
    begin
       Add(AKind, i, 2);
       Inc(i, 2)
    end

    else
      RaiseUnexpected(AKind);
  end;

var
  j, p : integer;
  C : Char;
begin
  i := 1;
  while i <= Length(FText) do
  try
    C := FText[i];
    if CharInSet(C, [' ', #13, #10, #9]) then
    begin
      Inc(i);
      Continue;
    end;

    case C of
      '/':
        if PopNextCharIs('*') then
        begin
          Inc(i);
          p := Pos('*/', FText, i) - i;
          if p < 1 then
            RaiseUnexpected('Comment');

          // Comment = Copy(FText, i, p)

          Inc(i, p + Length('*/'));
        end

        else
          RaiseUnexpected('Comment');

      '@':
        if not
          (
            TryAddSys(PATH_ROOT,    lxkSysIdentifierRoot)
            or
            TryAddSys(PATH_PARENT,  lxkSysIdentifierParent)
            or
            TryAddSys(PATH_THIS,    lxkSysIdentifierThis)
          )
        then
          RaiseUnexpected('lxkSysIdentifier...');

      '.', '[', ']', '(', ')', '-':
        begin
          case C of
            '.': Add(TLexemeKind.lxkDot,                 i, 1);
            '[': Add(TLexemeKind.lxkSquareBracketOpen,   i, 1);
            ']': Add(TLexemeKind.lxkSquareBracketClose,  i, 1);
            '(': Add(TLexemeKind.lxkRoundBracketOpen,    i, 1);
            ')': Add(TLexemeKind.lxkRoundBracketClose,   i, 1);
            '-': Add(TLexemeKind.lxkMinus,               i, 1);
          end;

          Inc(i);
        end;

      '!':
        AddTryEqPair(TLexemeKind.lxkNot, TLexemeKind.lxkNotEquals);

      '<':
        AddTryEqPair(TLexemeKind.lxkLessThan, TLexemeKind.lxkLessThanOrEquals);

      '>':
        AddTryEqPair(TLexemeKind.lxkGreaterThan, TLexemeKind.lxkGreaterThanOrEquals);

      '=':
        AddDub(C, TLexemeKind.lxkEquals);

      '&':
        AddDub(C, TLexemeKind.lxkAnd);

      '|':
        AddDub(C, TLexemeKind.lxkOr);

      '0'..'9':
        begin
          j := i;
          while PeekNextCharIn(['0'..'9']) do
            Inc(i);

          Add(TLexemeKind.lxkNumber, j, i - j + 1);
          Inc(i);
        end;

      '''':
        begin
          Inc(i);
          j := i;
          repeat
            p := Pos('''', FText, i);
            if p < 1 then
              RaiseUnexpected(lxkString);

            i := p + 1;
            if (i < Length(FText)) and (FText[i] = '''') then
            begin
              Inc(i);
              Continue;
            end;

            Break;
          until False;

          Add(TLexemeKind.lxkString, j, i - j - 1);
        end;

      '_','A'..'Z','a'..'z':
        begin
          j := i;
          while PeekNextCharIn(['_','A'..'Z','a'..'z','0'..'9']) do
            Inc(i);

          Add(TLexemeKind.lxkIdentifier, j, i - j + 1);
          Inc(i);
        end

    else
      raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingPathUnexpectedCharFmt, [C]);
    end;

  except
    EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingInternalErrorFmt, [FText, i]).RaiseOuter;
  end;

end;
{$ENDREGION}

{$REGION 'TPathResolver.TSyntaxAnalizer'}
constructor TPathResolver.TSyntaxAnalizer.Create(const AText: string; const ALexemes : TArray<TLexeme>);
begin
  FText := AText;
  FLexemes := ALexemes;
  FIndex := -1;
end;

function TPathResolver.TSyntaxAnalizer.GetCurLexeme: PLexeme;
begin
  if (FIndex < 0) or (FIndex > High(FLexemes)) then
    Result := nil
  else
    Result := @FLexemes[FIndex];
end;

function TPathResolver.TSyntaxAnalizer.PeekNext(
  const AExpectedLexemes: TLexemesKinds): boolean;
begin
  Result := (FIndex < High(FLexemes))
        and ( (AExpectedLexemes = []) or (FLexemes[Succ(FIndex)] in AExpectedLexemes) );
end;

function TPathResolver.TSyntaxAnalizer.Next(
  const AExpectedLexemes: TLexemesKinds): boolean;
begin
  if (FIndex >= High(FLexemes)) then
    Exit(False);

  Inc(FIndex);

  if (AExpectedLexemes <> []) and not (FLexemes[FIndex] in AExpectedLexemes) then
   raise EEntitiesResolvingError.CreateRes(@s_San_Entities_ResolvingUnexpectedLexeme);

  Result := True;
end;

function TPathResolver.TSyntaxAnalizer.TryNext(
  const AExpectedLexemes: TLexemesKinds): boolean;
begin
  Result := PeekNext(AExpectedLexemes);
  if Result then
    Inc(FIndex);
end;

procedure TPathResolver.TSyntaxAnalizer.RaiseUnxpextedEnd();
begin
  raise EEntitiesResolvingError.CreateRes(@s_San_Entities_ResolvingPathSintaxUnexpectedEnd);
end;

procedure TPathResolver.TSyntaxAnalizer.RaiseUnxpextedStatic(var AStaticPathItem);
var
  StaticPathItem : TPathItemStatic;
  S : string;
begin
  StaticPathItem := TPathItemStatic(AStaticPathItem);
  Pointer(AStaticPathItem) := nil;
  S := StaticPathItem.ClassName;
  FreeAndNil(StaticPathItem);

  raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingPathSintaxStaticPathItemFmt, [S]);
end;

function TPathResolver.TSyntaxAnalizer.ParseNumber(const AOwner: TPathItem;
  out AItem: TPathItem): boolean;
type
  TState = (nInt, nFrac, nError);
var
  Negative : boolean;
  State : TState;
  Values : array[nInt..nFrac] of Int64;
begin
  AItem := nil;
  Result := Next([lxkMinus, lxkNumber]);
  if not Result then
    Exit;

  Negative := CurLexeme^ = lxkMinus;
  if Negative and not Next([lxkNumber]) then
     RaiseUnxpextedEnd();

  State := nInt;
  FillChar(Values, SizeOf(Values), 0);

  repeat
    Values[State] := StrToInt64(CurLexeme^.Value[FText]);

    if TryNext([lxkDot]) then
    begin
      Inc(State);

      if State = nError then
        raise EEntitiesResolvingError.CreateRes(@s_San_Entities_ResolvingUnexpectedLexeme);

      if not Next([lxkNumber]) then
        RaiseUnxpextedEnd();

      Continue;
    end;

    Break;
  until False;

  if State = nInt then
  begin
    AItem := TPathItemInt64.Create(AOwner);
    TPathItemInt64(AItem).FValue := IfThen(Negative, -1, 1) * Values[nInt]
  end

  else
  begin
    AItem := TPathItemExtended.Create(AOwner);
    TPathItemExtended(AItem).FValue := IfThen(Negative, -1, 1) * (Values[nInt] + Values[nFrac] * Power(10, -1 * Length(IntToStr(Values[nFrac]))) );
  end
end;

function TPathResolver.TSyntaxAnalizer.ParseString(const AOwner: TPathItem;
  out AItem: TPathItem): boolean;
begin
  AItem := nil;
  Result := Next([lxkString]);
  if not Result then
    Exit;

  AItem := TPathItemString.Create(AOwner);
  TPathItemString(AItem).FValue := CurLexeme^.Value[FText];
end;

function TPathResolver.TSyntaxAnalizer.ParseCondition(out ACondition : TPathFilterCondition): boolean;

{$REGION 'const'}
const
  CONDITION_ITEM_LEXEMES : TLexemesKinds  = [lxkMinus, lxkNumber, lxkString,
    lxkSysIdentifierRoot, lxkSysIdentifierParent, lxkSysIdentifierThis, lxkIdentifier];

  COMPARE_LEXEMES : TLexemesKinds  = [lxkNotEquals, lxkLessThan, lxkLessThanOrEquals,
    lxkEquals, lxkGreaterThanOrEquals, lxkGreaterThan];

  BOOL_OP_LEXEMES : TLexemesKinds  = [lxkAnd, lxkOr];
{$ENDREGION}

  procedure ParsePart(out APart : TPathItem);
  var
    NextLexeme : PLexeme;
  begin
    if not PeekNext([lxkMinus, lxkNumber, lxkString, lxkSysIdentifierRoot, lxkSysIdentifierParent, lxkSysIdentifierThis, lxkIdentifier]) then
      RaiseUnxpextedEnd();

    NextLexeme := @FLexemes[Succ(FIndex)].Kind;

    if not
    (
      (NextLexeme^ in [lxkMinus, lxkNumber]) and ParseNumber(nil, APart)
      or
      (NextLexeme^ = lxkString) and ParseString(nil, APart)
      or
      (NextLexeme^ in [lxkSysIdentifierRoot, lxkSysIdentifierParent, lxkSysIdentifierThis, lxkIdentifier]) and ParseIdentifier(nil, APart)
    )
    then
      RaiseUnxpextedEnd();
  end;

  function MakeFieldsCompareCondition(var APathItem : TPathItem) : TPathFilterFieldsCompareCondition;
  var
    CmpOp : TEntitiesCompareOperation;
    RightPathItem : TPathItem;
  begin
    CmpOp := CurLexeme^.Kind.ToCompareKind;
    ParsePart(RightPathItem);
    Result := TPathFilterFieldsCompareCondition.Create(APathItem, RightPathItem, CmpOp);
  end;

  function MakeFieldCheckCondition(var APathItem : TPathItem) : TPathFilterBoolFieldCheckCondition;
  begin
    if APathItem is TPathItemStatic then
      RaiseUnxpextedStatic(APathItem);

    Result := TPathFilterBoolFieldCheckCondition.Create(APathItem);
  end;

  function ParseOrAnd(const AConditionsStack : TArray<TPathFilterCondition>;
    const AConditionsBoolOpStack : TArray<TLexemeKind>) : TArray<TPathFilterCondition>;
  var
    beg : integer;

    procedure Add(const AIndex : integer);
    var
      l : integer;
      Cnd : TPathFilterCondition;
    begin
      l := AIndex - beg + 1;

      if l = 1 then
        Cnd := AConditionsStack[AIndex]

      else
        Cnd := TPathFilterAndCondition.Create(System.Copy(AConditionsStack, beg, l));

      System.Insert(Cnd, Result, Length(Result));
      beg := Succ(AIndex);
    end;

  var
    i : integer;
  begin
    Assert(High(AConditionsStack) = Length(AConditionsBoolOpStack), 'High(AConditionsStack) != Length(AConditionsBoolOpStack)');

    try

      beg := 0;
      for i := 0 to High(AConditionsBoolOpStack) do
        case AConditionsBoolOpStack[i] of
          lxkOr:
            Add(i);

          lxkAnd:; //<- Empty action

        else
          San.Api.Excps.ItsImpossible();
        end;

      Add(High(AConditionsStack));

    except
      ClearObjsArray(Result);
      raise;
    end;

  end;

var
  IsNegative, HasAnd, HasOr : boolean;
  ConditionsStack : TArray<TPathFilterCondition>;
  ConditionsBoolOpStack : TArray<TLexemeKind>;
  SubCondition : TPathFilterCondition;
  PathItem : TPathItem;
begin
  HasAnd := False;
  HasOr  := False;

  try

    repeat
      IsNegative := False;

      // !
      while TryNext([lxkNot]) do
        IsNegative := not IsNegative;

      // ()
      if TryNext([lxkRoundBracketOpen]) then
      begin
        if not (ParseCondition(SubCondition) and Next([lxkRoundBracketClose])) then
          RaiseUnxpextedEnd();
      end

      // PathItem
      else
      begin
        ParsePart(PathItem);

        if TryNext(COMPARE_LEXEMES) then
          SubCondition := MakeFieldsCompareCondition(PathItem)
        else
          SubCondition := MakeFieldCheckCondition(PathItem);
      end;

      SubCondition.FIsNegative := IsNegative;

      if TryNext([lxkSquareBracketClose, lxkRoundBracketClose]) then
        if Length(ConditionsStack) = 0 then
        begin
          ACondition := SubCondition;
          Exit(True);
        end

        else
          Break;

      System.Insert(SubCondition, ConditionsStack, Length(ConditionsStack));

      if not Next(BOOL_OP_LEXEMES) then
        RaiseUnxpextedEnd();

      case CurLexeme^.Kind of
        lxkAnd:
          HasAnd := True;

        lxkOr:
          HasOr  := True;
      end;

      System.Insert(CurLexeme^.Kind, ConditionsBoolOpStack, Length(ConditionsBoolOpStack));
    until False;

    Assert(HasOr or HasAnd, 'not (HasOr or HasAnd)');

    System.Insert(SubCondition, ConditionsStack, Length(ConditionsStack));

    case 2*Ord(HasOr) + Ord(HasAnd) of

      // And
      1: ACondition := TPathFilterAndCondition.Create(ConditionsStack);

      // Or
      2: ACondition := TPathFilterOrCondition.Create(ConditionsStack);

      // Or(And)
      3: ACondition := TPathFilterOrCondition.Create( ParseOrAnd(ConditionsStack, ConditionsBoolOpStack) );

    else
      San.Api.Excps.ItsImpossible();
    end;

  except
    ClearObjsArray(ConditionsStack);
    raise;
  end;

  Result := True;
end;

function TPathResolver.TSyntaxAnalizer.ParseIdentifier(
  const AOwner : TPathItem; out AItem: TPathItem): boolean;
var
  SubItem : TPathItem;
begin
  AItem := nil;
  Result := Next([lxkSysIdentifierRoot, lxkSysIdentifierParent, lxkSysIdentifierThis, lxkIdentifier]);

  if not Result then
    Exit;

  try

    case CurLexeme^.Kind of
      lxkSysIdentifierRoot:
        AItem := TPathItemSysRoot.Create(AOwner);

      lxkSysIdentifierParent:
        AItem := TPathItemSysParent.Create(AOwner);

      lxkSysIdentifierThis:
        AItem := TPathItemSysThis.Create(AOwner);

      lxkIdentifier:
      begin
        if PeekNext([lxkSquareBracketOpen]) then
          AItem := TPathItemFilter.Create(AOwner)
        else
          AItem := TPathItemIdentifier.Create(AOwner);

        (AItem as TPathItemIdentifier).FName := CurLexeme^.Value[FText];

        if AItem is TPathItemFilter then
          Result := Next([lxkSquareBracketOpen]) and ParseCondition( TPathItemFilter(AItem).FCondition );
      end;
    end;

  except
    FreeAndNil(AItem);
    raise;
  end;

  if not TryNext([lxkDot]) then
    Exit;

  if not ParseIdentifier(AItem, SubItem) then
    RaiseUnxpextedEnd();

  AItem.FChild := SubItem;
end;

function TPathResolver.TSyntaxAnalizer.Parse: TPathItem;

  procedure InternalError();
  var
    CurLexeme : PLexeme;
  begin
    CurLexeme := Self.CurLexeme;

    if Assigned(CurLexeme) then
      EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingSintaxInternalErrorFmt, [FText, FIndex, CurLexeme^.Kind.ToString, CurLexeme^.Start, CurLexeme^.Length, CurLexeme^.Value[FText]]).RaiseOuter
    else
      EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingSintaxInternalErrorFmt, [FText, FIndex, lxkUnknown.ToString, -1, -1]).RaiseOuter
  end;

begin

  if Length(FLexemes) = 0 then
    Exit(nil);

  try

    ParseIdentifier(nil, Result);

  except
    InternalError();
  end;

end;
{$ENDREGION}

{$REGION 'TPathResolver'}
class function TPathResolver.LexicalAnalize(const AText: string): TArray<TLexeme>;
begin
  Result := TLexicalAnalizer.Create(AText).Parse();
end;

class function TPathResolver.SintaxAnalize(const AText: string;
  const ALexemes: TArray<TLexeme>): TPathItem;
begin
  Result := TSyntaxAnalizer.Create(AText, ALexemes).Parse();
end;

class function TPathResolver.SintaxAnalize(const AText: string): TPathItem;
begin
  Result := SintaxAnalize(AText, LexicalAnalize(AText));
end;
{$ENDREGION}

{$REGION 'TEntitiesObject'}
constructor TEntitiesObject.Create();
begin
  inherited Create();

  {$IFDEF IS_ALIVE_SUPPORTS}
  FAlive := San.Api.Intfs.NopInterfaceCreate();
  {$ENDIF}
end;

destructor TEntitiesObject.Destroy;
begin
  FreeAndNil(FUpdates);
  inherited;
end;

function TEntitiesObject.GetIsAlive: boolean;
begin
  {$IFDEF IS_ALIVE_SUPPORTS}
  Result := Assigned(FAlive);
  {$ELSE}
  raise ENotSupportedException.Create(ClassName + '.IsAlive');
  {$ENDIF}
end;

{$REGION 'MultithreadLocks'}
function TEntitiesObject.GetLocker: TMultithreadLockerFacade;
begin
  Result := TMultithreadLockerFacade.Create(Self);
end;

function TEntitiesObject.GetLockerData: TMultithreadLockerFacade.TMultithreadLockerData;
begin
  Result := nil;
end;

function TEntitiesObject.ExternalTryEnterRead(const AStartTicks,
  ATimeOut : Cardinal; out AFailedToLock : TObject): boolean;
begin
  AFailedToLock := nil;
  Result := True;
end;

procedure TEntitiesObject.ExternalLeaveRead(const AFailedToLock : TObject);
begin
end;

procedure TEntitiesObject.LockerDetachCurrentThread;
begin
  // In inheritors: ~GetLockerDataPtr^.DetachCurrentThread();
end;
{$ENDREGION}

{$REGION 'IInterface'}
function TEntitiesObject.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := S_OK
  else
    Result := E_NOINTERFACE;
end;

class procedure TEntitiesObject.RaiseJSONImportPairNotFound(const AName: string);
begin
  raise EEntitiesRWError.CreateResFmt(@s_San_Entities_JSONImportPairNotFoundFmt, [AName]);
end;

function TEntitiesObject._AddRef: Integer;
begin
  Result := -1;
end;

function TEntitiesObject._Release: Integer;
begin
  Result := -1;
end;
{$ENDREGION}

{$REGION 'Assigning'}
function TEntitiesObject.AssignCheck(const ADest: TEntitiesObject): boolean;
begin
  Result := False;
end;

procedure TEntitiesObject.AssignError(const ASource: TEntitiesObject);
var
  SourceName: string;
begin
  if ASource <> nil then
    SourceName := ASource.ClassName
  else
    SourceName := 'nil';

  raise EConvertError.CreateResFmt(@SAssignError, [SourceName, ClassName]);
end;

procedure TEntitiesObject.AssignTo(const ADest: TEntitiesObject);
begin
end;

procedure TEntitiesObject.Assign(const ASource: TEntitiesObject);
begin
  if not Assigned(ASource) then
    AssignError(nil);

  if not ASource.AssignCheck(Self) then
    Self.AssignError(ASource);

  ASource.AssignTo(Self)
end;
{$ENDREGION}

{$REGION 'Updates'}
function TEntitiesObject.GetProvideUpdates: TUpdates;
begin
  Result := Updates;
end;

function TEntitiesObject.GetProvideUpdates2: TUpdates2;
begin
  Result := Updates;
end;

function TEntitiesObject.GetUpdates: TUpdates2;
begin
  if not Assigned(FUpdates) then
    FUpdates := TUpdates2.Create(DoUpdate);

  Result := FUpdates;
end;

procedure TEntitiesObject.DoUpdate;
begin
end;
{$ENDREGION}

{$REGION 'Events'}
function TEntitiesObject.HasEventSubscribers: boolean;
begin
  Result := False;
end;

procedure TEntitiesObject.DoFireEvent(const AEvent: TEntitiesEvent);
begin
  AEvent.Free; // need override it
end;

procedure TEntitiesObject.FireEventInsert(const ASource: TEntitiesObject;
  const AIndex: integer; const AItem: TEntitiesObject);
begin
  if HasEventSubscribers or Assigned(FUpdates) and (FUpdates.UpdatesCount > 0) then
    DoFireEvent(TEntitiesInsertEvent.Create(ASource, AItem, AIndex));
end;

procedure TEntitiesObject.FireEventDelete(const ASource: TEntitiesObject;
  const AIndex: integer; const AItem: TEntitiesObject);
begin
  if HasEventSubscribers or Assigned(FUpdates) and (FUpdates.UpdatesCount > 0) then
    DoFireEvent(TEntitiesDeleteEvent.Create(ASource, AItem, AIndex));
end;

procedure TEntitiesObject.FireEventMove(const ASource: TEntitiesObject;
  const AOldIndex, ANewIndex: integer; const AItem: TEntitiesObject);
begin
  if HasEventSubscribers or Assigned(FUpdates) and (FUpdates.UpdatesCount > 0) then
    DoFireEvent(TEntitiesMoveEvent.Create(ASource, AItem, AOldIndex, ANewIndex));
end;

procedure TEntitiesObject.FireEventChange(const ASource: TEntitiesObject;
  const AField: TDataField; const AOldValue, ANewValue: TValue);
begin
  if HasEventSubscribers or Assigned(FUpdates) and (FUpdates.UpdatesCount > 0) then
    DoFireEvent(TEntitiesChangeEvent.Create(ASource, AField, AOldValue, ANewValue));
end;
{$ENDREGION}

{$REGION 'JSON'}
procedure TEntitiesObject.DoDebugExportToJSON(const AJSONObject: TJSONObject);
begin
end;

function TEntitiesObject.DoExportToJSON(const ACompact : boolean) : TJSONValue;
begin
  Result := nil;
end;

procedure TEntitiesObject.DoImportFromJSON(const AJSON: TJSONValue; const AReWrite : boolean);
begin
end;

function TEntitiesObject.ExportToJSON(const ACompact : boolean): TJSONValue;
begin
  Result := DoExportToJSON(ACompact);

  if San.Entities.Api.DebugJSON and Assigned(Result) and (Result is TJSONObject) then
    DoDebugExportToJSON(TJSONObject(Result));
end;

procedure TEntitiesObject.ImportFromJSON(const AJSON: TJSONValue; const AReWrite : boolean);
begin
  try
    Updates.BeginUpdate;
    try

      DoImportFromJSON(AJSON, AReWrite);

    finally
      Updates.EndUpdate;
    end;
  except
    EEntitiesRWError.CreateResFmt(@s_San_Entities_JSONImportInternalErrorFmt, [AJSON.ToString]).RaiseOuter;
  end;
end;
{$ENDREGION}

{$ENDREGION}

{$ENDREGION}

{$REGION 'meta'}

{$REGION 'TMetaObject'}
procedure TMetaObject.Assign(const ASource: TEntitiesObject);
begin
  ValidateDataEmpty();
  inherited;
end;

procedure TMetaObject.DataCreating(const ADataObject : TDataObject);
begin
  if not Assigned(FDataObjects) then
    FDataObjects := TDataObjects.Create;

  FDataObjects.Add(ADataObject);
end;

procedure TMetaObject.DataDestroing(const ADataObject : TDataObject);
begin
  FDataObjects.ExtractItem(ADataObject, FromEnd);
end;

procedure TMetaObject.BeforeDestruction;
var
  i : integer;
begin

  if Assigned(FDataObjects) then
    // Вызов FDataObjects.Clear() ведет к ошибкам
    // Поэтому перед разрушением FDataObjects явно удаляем объекты по одному,
    // давая им извлечься из списка в DataDestroing
    for i := FDataObjects.Count - 1 downto 0 do
      FDataObjects.Delete(i);

  inherited;
end;

destructor TMetaObject.Destroy;
begin
  FreeAndNil(FDataObjects);
  inherited;
end;

function TMetaObject.GetRichDataClass: TDataObjectClass;
begin
  Result := GetBaseDataClass();
end;

class function TMetaObject.GetBaseDataClass: TDataObjectClass;
begin
  raise ENotImplemented.Create(ClassName);
end;

procedure TMetaObject.MakeDataRaw(const AOwner: TDataObject; out AData);
var
  DataClass : TDataObjectClass;
  Context : TDataObject.TCreateContext;
begin
  DataClass := GetRichDataClass();

  Assert(Assigned(DataClass), 'DataClass = nil');

  Context.Meta   := Self;
  Context.Owner  := AOwner;

  TObject(AData) := DataClass.InternalCreate(Context);
end;

procedure TMetaObject.ValidateDataEmpty;
begin
  if Assigned(FDataObjects) and (FDataObjects.Count > 0) then
    raise EEntitiesMetaChangeForbidden.CreateRes(@s_San_Entities_MetaPropsChangingForbiddenWithExistingData);
end;

function TMetaObject.MakeData(const AOwner: TDataObject): TDataObject;
begin
  MakeDataRaw(AOwner, Result);
end;

function TMetaObject.DoExportToJSON(const ACompact : boolean): TJSONValue;
begin
  Result := ExportToJSON(ACompact);
end;

procedure TMetaObject.DoImportFromJSON(const AJSON: TJSONValue; const AReWrite : boolean);
begin
  ImportFromJSON(AJSON as TJSONObject, AReWrite);
end;

procedure TMetaObject.DoExportToJSON(const AJSON: TJSONObject; const ACompact : boolean);
begin
end;

procedure TMetaObject.DoImportFromJSON(const AJSON: TJSONObject; const AReWrite : boolean);
begin
end;

function TMetaObject.ExportToJSON(const ACompact : boolean): TJSONObject;
begin
  Result := TJSONObject.Create;
  try
    DoExportToJSON(Result, ACompact);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

procedure TMetaObject.ImportFromJSON(const AJSON: TJSONObject; const AReWrite : boolean);
begin
  try
    Updates.BeginUpdate;
    try

      DoImportFromJSON(AJSON, AReWrite);

    finally
      Updates.EndUpdate;
    end;
  except
    EEntitiesRWError.CreateResFmt(@s_San_Entities_JSONImportInternalErrorFmt, [AJSON.ToString]).RaiseOuter;
  end;
end;
{$ENDREGION}

{$REGION 'TMetaNode'}
constructor TMetaNode.Create(const AOwner: TMetaNode);

  procedure RaiseMetaOwnerClassError;
  var
    S : string;
  begin

    if Assigned(AOwner) then
      S := AOwner.ClassName
    else
      S := 'nil';

    raise EEntitiesError.CreateResFmt(@s_San_Entities_InvalidMetaOwnerClassErrorFmt, [S, Self.ClassName]);
  end;

var
  Parent : TMetaNode;
begin

  if not CheckMetaOwnerClass(AOwner) then
    RaiseMetaOwnerClassError;

  inherited Create();

  FOwner := AOwner;

  Parent := AOwner;
  while Assigned(Parent) and not (Parent is TMetaEntityRoot)  do
    Parent := Parent.Parent;

  FRoot := TMetaEntityRoot(Parent);
end;

procedure TMetaNode.Clone(const AOwner: TMetaNode; out AClone);
var
  Res : TMetaNode;
begin
  Res := TMetaNodeClass(ClassType).Create(AOwner);
  try
    Res.Assign(Self);
    Pointer(AClone) := Res;
  except
    FreeAndNil(Res);
    Pointer(AClone) := nil;
    raise;
  end;
end;

function TMetaNode.GetGuard: IEventsSubscribeGuard;
begin
  Result := FRoot.FGuard;
end;

function TMetaNode.GetInitialOwner: TMetaNode;
begin
  Result := FOwner;
end;

function TMetaNode.GetParent: TMetaEntity;
var
  Node : TMetaNode;
begin
  Node := FOwner;
  while Assigned(Node) and not (Node is TMetaEntity) do
    Node := Node.Owner;

  if Assigned(Node) then
    Result := TMetaEntity(Node)
  else
    Result := nil;
end;

function TMetaNode.GetPath: string;
begin
  if not Assigned(Self) then
    Result := 'nil'
  else
    Result := DoGetPath();
end;

procedure TMetaNode.SetName(const AValue: string);
begin
  raise ENotSupportedException.CreateResFmt(@s_San_Entities_SetNameNotSupportedErrorFmt, [ClassName]);
end;

class function TMetaNode.CheckMetaOwnerClass(const AOwner: TMetaNode): boolean;
begin
  Result := True;
end;

function TMetaNode.DoFindChildNode(const ANodeType : TEntitiesNodeType;
  const AName: string; out AMetaNode: TMetaNode): boolean;
begin
  AMetaNode  := nil;
  Result     := False;
end;

function TMetaNode.GetLockerData: TMultithreadLockerFacade.TMultithreadLockerData;
begin
  Result := Parent.GetLockerData();
end;

function TMetaNode.ExternalTryEnterRead(const AStartTicks, ATimeOut: Cardinal;
  out AFailedToLock: TObject): boolean;
var
  Node : TMetaNode;
begin
  Node := Owner;
  while Assigned(Node) and not (Node.NodeType in [enntEntity, enntCollection]) do
    Node := Node.Owner;

  Result := not Assigned(Node)
         or Node.Locker.TryEnterRead(AStartTicks, ATimeOut);

  if Result then
    AFailedToLock := nil
  else
    AFailedToLock := Node;
end;

procedure TMetaNode.ExternalLeaveRead(const AFailedToLock: TObject);
var
  Node : TMetaNode;
begin
  Node := Owner;
  while Assigned(Node) and not (Node.NodeType in [enntEntity, enntCollection]) do
    Node := Node.Owner;

  if Assigned(Node) and (Node <> AFailedToLock) then
    Node.Locker.LeaveRead();
end;

function TMetaNode.ExportToJSON(const ACompact : boolean): TJSONObject;
begin
  try
    Result := inherited;
  except
    EEntitiesRWError.CreateResFmt(@s_San_Entities_JSONExportInternalErrorForNodeFmt, [ClassName, Path]).RaiseOuter(Result);
  end;
end;
{$ENDREGION}

{$REGION 'TMetaNode<TOwner, TDataClassType>'}
class function TMetaNode<TOwner, TData>.CheckMetaOwnerClass(
  const AOwner: TMetaNode): boolean;
begin
  Result := AOwner.InheritsFrom(TOwner);
end;

class function TMetaNode<TOwner, TData>.GetBaseDataClass: TDataObjectClass;
begin
  Result := TDataObjectClass(TData);
end;

function TMetaNode<TOwner, TData>.GetOwner: TOwner;
begin
  Result := TOwner(GetInitialOwner());
end;
{$ENDREGION}

{$REGION 'TMetaField'}
class constructor TMetaField.Create;
begin
  FFieldsByStrIDRegistry := TDictionary<TUpperString,TMetaFieldClass>.Create(TUpperStringEqualityComparer.Create());
end;

class destructor TMetaField.Destroy;
begin
  FreeAndNil(FFieldsByStrIDRegistry);
end;

class function TMetaField.DataEntitiesIndexSingleClass: TDataCollectionIndexSingleClass;
begin
  Result := nil;
end;

function TMetaField.AssignCheck(const ADest: TEntitiesObject): boolean;
begin
  Result := ADest is TMetaField;
end;

procedure TMetaField.AssignTo(const ADest: TEntitiesObject);
var
  Dest : TMetaField;
begin
  inherited;
  Dest := ADest as TMetaField;
  Dest.FName := Self.Name;
end;

constructor TMetaField.Create(const AOwner: TMetaNode);
begin
  inherited;
  FIndex := -1; // Как признак, что место поля пока не определено
end;

function TMetaField.GetName: string;
begin
  Result := FName;
end;

function TMetaField.DoGetPath: string;
begin
  Result := Parent.Path + PATH_SEPARATOR + Self.Name;
end;

procedure TMetaField.SetName(const AValue: string);
begin

  if FName = AValue then
    Exit;

  FName := AValue;

  // Notification if need
end;

class function TMetaField.ValueType: PTypeInfo;
begin
  Result := TDataFieldClass(GetBaseDataClass()).ValueType;
end;

class function TMetaField.FieldStrID: string;
begin
  Result := string(ValueType^.NameFld.ToString);
end;

function TMetaField.GetIndex: integer;
begin
  Result := FIndex;
end;

function TMetaField.GetIsCollectionField: boolean;
begin
  Result := False;
end;

function TMetaField.GetIsEntityField: boolean;
begin
  Result := False;
end;

function TMetaField.GetIsSimpleField: boolean;
begin
  Result := True;
end;

function TMetaField.GetNodeType: TEntitiesNodeType;
begin
  Result := enntField;
end;

procedure TMetaField.DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean);
begin
  inherited;
  AJSON.AddPair(sName, FName);
  AJSON.AddPair(sType, FieldStrID);
end;

procedure TMetaField.DoImportFromJSON(const AJSON: TJSONObject; const AReWrite : boolean);
var
  JName : TJSONString;
begin
  inherited;
  if not AJSON.TryGetValue(sName, JName) then
    RaiseJSONImportPairNotFound(sName);

  FName := JName.Value;
end;
{$ENDREGION}

{$REGION 'TMetaField<T>'}
{$IFDEF DEBUG}
class constructor TMetaField<T>.Create;
var
  TI : PTypeInfo;
begin
  if GetBaseDataClass = TDataField then
    Exit;

  TI := TypeInfo(T);
  Assert(ValueType = TI, ValueType^.NameFld.ToString + ' != ' + TI^.NameFld.ToString);
end;
{$ENDIF}

procedure TMetaField<T>.SetDefaultValue(
  const AValue: T);
begin
  ValidateDataEmpty;
  FDefaultValue := AValue;
end;

procedure TMetaField<T>.DoExportToJSON(const AJSON: TJSONObject; const ACompact : boolean);
var
  TI : PTypeInfo;
  JVal : TJSONValue;
begin
  inherited;

  TI := TypeInfo(T);
  if (TI^.Kind <> tkClass) and (not ACompact or not TEqualityComparer<T>.Default.Equals(FDefaultValue, Default(T))) then
    AJSON.AddPair(sDefault, Api.ValueToJSON<T>(FDefaultValue));
end;

procedure TMetaField<T>.DoImportFromJSON(const AJSON: TJSONObject;
  const AReWrite: boolean);
var
  TI : PTypeInfo;
  JVal : TJSONValue;
begin
  inherited;

  TI := TypeInfo(T);
  if (TI^.Kind <> tkClass) and AJSON.TryGetValue(sDefault, JVal) then
    FDefaultValue := Api.JSONToValue<T>(JVal);
end;
{$ENDREGION}

{$REGION 'TMetaField<TDataFieldType, TDataCollectionIndexSingleType, T>'}
class function TMetaField<TDataFieldType, TDataCollectionIndexSingleType, T>.GetBaseDataClass(): TDataObjectClass;
begin
  Result := TDataObjectClass(TDataFieldType);
end;

class function TMetaField<TDataFieldType, TDataCollectionIndexSingleType, T>.DataEntitiesIndexSingleClass: TDataCollectionIndexSingleClass;
begin
  if TDataCollectionIndexSingle.InheritsFrom(TDataCollectionIndexSingleType) then
    Result := nil
  else
    Result := TDataCollectionIndexSingleClass(TDataCollectionIndexSingleType);
end;
{$ENDREGION}

{$REGION 'TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>'}
procedure TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>.SetDefaultValue(
  const AValue: TDataNode);
begin
  raise EEntitiesError.CreateResFmt(@s_San_Entities_SetDefaultValueIsForbiddenForNodeFieldXXXFmt, [Name]);
end;

class function TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>.SubNodeClass: TMetaNodeClass;
begin
  Result := TMetaNodeClass(TMetaDataType);
end;

constructor TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>.Create(
  const AOwner: TMetaNode);
begin
  inherited;
  FNode := SubNodeClass.Create(Self);
end;

destructor TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>.Destroy;
begin
  FreeAndNil(FNode);
  inherited;
end;

function TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>.DoFindChildNode(
  const ANodeType : TEntitiesNodeType; const AName: string; out AMetaNode: TMetaNode): boolean;
var
  Node : TMetaNode;
begin
  Node := TMetaNode(Self.Node);

  case ANodeType of
    enntField:
      case Node.NodeType of
        enntEntity:
          Exit( Node.DoFindChildNode(ANodeType, AName, AMetaNode) );

        enntCollection:
          Exit( (Node as TMetaCollection).ItemMeta.DoFindChildNode(ANodeType, AName, AMetaNode) );
      end;

    enntEntity:
      if Node.NodeType = enntCollection then
      begin
        AMetaNode := (Node as TMetaCollection).ItemMeta;
        Exit(True);
      end;
   end;

  AMetaNode := Node;
  Result := True;
end;

function TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>.GetIsSimpleField: boolean;
begin
  Result := False;
end;

function TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>.GetNode: TMetaDataType;
begin
  Result := TMetaDataType(FNode);
end;

function TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>.AssignCheck(
  const ADest: TEntitiesObject): boolean;
begin
  Result := ADest is TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>;
end;

procedure TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>.AssignTo(
  const ADest: TEntitiesObject);
var
  Dest : TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>;
begin
  inherited;

  Dest := ADest as TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>;

  Dest.FNode.Assign(Self.FNode);
end;

procedure TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>.DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean);
begin
  inherited;
  AJSON.AddPair(FieldStrID, FNode.ExportToJSON(ACompact));
end;

procedure TMetaFieldNode<TMetaDataType, TDataFieldType, TDataCollectionIndexSingleType>.DoImportFromJSON(
  const AJSON: TJSONObject; const AReWrite : boolean);
var
  JNode : TJSONObject;
begin
  inherited;

  if not AJSON.TryGetValue(FieldStrID, JNode) then
    RaiseJSONImportPairNotFound(FieldStrID);

  FNode.DoImportFromJSON(JNode, AReWrite);
end;
{$ENDREGION}

{$REGION 'TMetaFieldEntity'}
class function TMetaFieldEntity.FieldStrID: string;
begin
  Result := sEntity;
end;

function TMetaFieldEntity.GetEntity: TMetaEntity;
begin
  Result := TMetaEntity(Node);
end;

function TMetaFieldEntity.GetIsEntityField: boolean;
begin
  Result := True;
end;
{$ENDREGION}

{$REGION 'TMetaFieldCollection'}
class function TMetaFieldCollection.FieldStrID: string;
begin
  Result := sCollection;
end;

function TMetaFieldCollection.GetCollection: TMetaCollection;
begin
  Result := TMetaCollection(Node);
end;

function TMetaFieldCollection.GetIsCollectionField: boolean;
begin
  Result := True;
end;
{$ENDREGION}

{$REGION 'TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternal'}
constructor TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternal.Create;
begin
  inherited Create();
end;

procedure TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternal.ClearBeforeFree;
begin
end;

function TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternal.GetTarget(
  const AOwner: TMetaNodeEntityOrCollection): TMetaNodeEntityOrCollection;
begin
  Result := AOwner;
end;
{$ENDREGION}

{$REGION 'TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalCustomLink'}
constructor TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalCustomLink.Create(
  const ALink: string);
begin
  inherited Create();
  SetLink(ALink);
end;

procedure TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalCustomLink.ClearBeforeFree;
begin
  FreeAndNil(FResolvedLink);
  inherited;
end;

procedure TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalCustomLink.SetLink(
  const AValue: string);
begin
  if FLink = AValue then
    Exit;

  FreeAndNil(FResolvedLink);

  FLink := AValue;

  FResolvedLink := TPathResolver.SintaxAnalize(FLink);
end;

function TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalCustomLink.GetTarget(
  const AOwner: TMetaNodeEntityOrCollection): TMetaNodeEntityOrCollection;
var
  Node : TMetaNode;
  Field : TMetaField;
begin
  Node   := ResolvedLink.FindEndTargetMeta(enntField, AOwner, AOwner.Parent);
  Field  := Node as TMetaField;

  if Field.IsEntityField then
    Result := (Field as TMetaFieldEntity).Entity
  else
  if AOwner.NodeType = enntCollection then
    Result := (Field as TMetaFieldCollection).Collection
  else
    Result := (Field as TMetaFieldCollection).Collection.ItemMeta;
end;
{$ENDREGION}

{$REGION 'TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalLink'}
procedure TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalLink.DoExportToJSON(
  const AOwner: TMetaNodeEntityOrCollection; const AJSON: TJSONObject; const ACompact : boolean);
begin
  inherited;
  AJSON.AddPair(sLink, Link);
end;
{$ENDREGION}

{$REGION 'TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalRecursion'}
procedure TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalRecursion.DoExportToJSON(
  const AOwner: TMetaNodeEntityOrCollection; const AJSON: TJSONObject; const ACompact : boolean);
begin
  inherited;
  AJSON.AddPair(sRecursion, Link);
end;
{$ENDREGION}

{$REGION 'TMetaNodeEntityOrCollection'}
constructor TMetaNodeEntityOrCollection.Create(const AOwner: TMetaNode);
begin
  inherited;
  FLockerData := TMultithreadLockerFacade.TMultithreadLockerData.Create();
end;

destructor TMetaNodeEntityOrCollection.Destroy;
begin
  if Assigned(FInternalData) then
  begin
    FInternalData.ClearBeforeFree();
    FreeAndNil(FInternalData);
  end;

  FreeAndNil(FLockerData);
  inherited;
end;

procedure TMetaNodeEntityOrCollection.DoRecreateInternal(const ALink: string;
  const AIsRecursion : boolean);
begin
  ValidateDataEmpty();

  FreeAndNil(FInternalData);

  if ALink <> '' then
    if AIsRecursion then
      FInternalData := TMetaNodeEntityOrCollectionInternalRecursion.Create(ALink)
    else
      FInternalData := TMetaNodeEntityOrCollectionInternalLink.Create(ALink)
  else
    FInternalData := GetInternalDataClass().Create();
end;

function TMetaNodeEntityOrCollection.GetIsLink: boolean;
begin
  Result := FInternalData is TMetaNodeEntityOrCollectionInternalLink;
end;

function TMetaNodeEntityOrCollection.GetLink: string;
begin
  if IsLink then
    Result := TMetaNodeEntityOrCollectionInternalLink(FInternalData).Link
  else
    Result := '';
end;

procedure TMetaNodeEntityOrCollection.SetLink(const AValue: string);
begin
  if Link = AValue then
    Exit;

  DoRecreateInternal(AValue);
end;

function TMetaNodeEntityOrCollection.GetIsRecursive: boolean;
begin
  Result := Assigned(InternalData) and (InternalData is TMetaNodeEntityOrCollectionInternalRecursion);
end;

function TMetaNodeEntityOrCollection.GetRecursion: string;
begin
  if IsRecursive then
    Result := TMetaNodeEntityOrCollectionInternalRecursion(InternalData).Link
  else
    Result := '';
end;

procedure TMetaNodeEntityOrCollection.SetRecursion(const AValue: string);
begin
  if Recursion = AValue then
    Exit;

  DoRecreateInternal(AValue, True);
end;

function TMetaNodeEntityOrCollection.GetTarget: TMetaNodeEntityOrCollection;
begin
  if not Assigned(FInternalData) then
    DoRecreateInternal();

  Result := FInternalData.GetTarget(Self);
  while Result.IsLink do
    Result := Result.Target;
end;

procedure TMetaNodeEntityOrCollection.LockerDetachCurrentThread;
begin
  FLockerData.DetachCurrentThread();
end;

function TMetaNodeEntityOrCollection.AssignCheck(const ADest: TEntitiesObject): boolean;
begin
  Result := ADest is TMetaNodeEntityOrCollection;
end;

procedure TMetaNodeEntityOrCollection.AssignNoLinkTo(
  const ADest: TMetaNodeEntityOrCollection);
var
  SourceInternal, DestInternal : TMetaNodeEntityOrCollectionInternalData;
begin
  SourceInternal  := Self.FInternalData  as TMetaNodeEntityOrCollectionInternalData;
  DestInternal    := ADest.FInternalData as TMetaNodeEntityOrCollectionInternalData;

  DestInternal.RichDataClass := SourceInternal.RichDataClass;
end;

procedure TMetaNodeEntityOrCollection.AssignTo(const ADest: TEntitiesObject);
var
  Dest : TMetaNodeEntityOrCollection;
begin
  inherited;

  Dest := ADest as TMetaNodeEntityOrCollection;

  if not Assigned(Dest.FInternalData) then
    Dest.DoRecreateInternal(Self.Link, Self.InternalData is TMetaNodeEntityOrCollectionInternalRecursion)
  else
  if Self.InternalData.ClassType <> Dest.InternalData.ClassType then
    AssignError(Self);

  if Self.InternalData is TMetaNodeEntityOrCollectionInternalCustomLink then
    Dest.Link := Self.Link

  else
    Self.AssignNoLinkTo(Dest);
end;

function TMetaNodeEntityOrCollection.GetRichDataClass: TDataObjectClass;
begin
  Result := FInternalData.RichDataClass;
  if not Assigned(Result) then
    Result := inherited
end;

function TMetaNodeEntityOrCollection.GetLockerData: TMultithreadLockerFacade.TMultithreadLockerData;
begin
  Result := FLockerData;
end;

procedure TMetaNodeEntityOrCollection.DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean);
begin
  if not Assigned(FInternalData) then
    DoRecreateInternal();

  inherited;
  InternalData.DoExportToJSON(Self, AJSON, ACompact);
end;
{$ENDREGION}

{$REGION 'TMetaNodeEntityOrCollection<TOwner, TData>'}
class function TMetaNodeEntityOrCollection<TOwner, TData>.CheckMetaOwnerClass(
  const AOwner: TMetaNode): boolean;
begin
  Result := AOwner.InheritsFrom(TOwner);
end;

class function TMetaNodeEntityOrCollection<TOwner, TData>.GetBaseDataClass: TDataObjectClass;
begin
  Result := TDataObjectClass(TData);
end;

function TMetaNodeEntityOrCollection<TOwner, TData>.GetOwner: TOwner;
begin
  Result := TOwner(GetInitialOwner());
end;
{$ENDREGION}

{$REGION 'TMetaEntity.TMetaEntityInternalData'}
procedure TMetaEntity.TMetaEntityInternalData.ClearBeforeFree;
begin
  ClearObjsArray(FFields);
  inherited;
end;

function TMetaEntity.TMetaEntityInternalData.TryFindFieldByName(const AName: string;
  out AMetaField: TMetaField; const AIndexPtr: PInteger): boolean;
var
  i : integer;
begin

  for i := 0 to High(FFields) do
    if AnsiSameStr(FFields[i].Name, AName) then
    begin
      AMetaField := FFields[i];

      if Assigned(AIndexPtr) then
        AIndexPtr^ := i;

      Exit(True);
    end;

  AMetaField := nil;

  if Assigned(AIndexPtr) then
    AIndexPtr^ := -1;

  Result := False;
end;

function TMetaEntity.TMetaEntityInternalData.ExistsFieldByName(
  const AName: string): boolean;
var
  Dummy : TMetaField;
begin
  Result := TryFindFieldByName(AName, Dummy);
end;

procedure TMetaEntity.TMetaEntityInternalData.AddField(const AMetaField: TMetaField);
begin
  System.Insert(AMetaField, FFields, Length(FFields));
end;

procedure TMetaEntity.TMetaEntityInternalData.CloneFields(
  const ASource, ADest: TMetaEntity; const ACheckExists: Boolean);
var
  SourceField, ClonedField : TMetaField;
begin
  for SourceField in (ASource.InternalData as TMetaEntityInternalData).FFields do
    if not ACheckExists or ACheckExists and not ExistsFieldByName(SourceField.Name) then
    begin
      SourceField.Clone(ADest, ClonedField);
      AddField(ClonedField);
    end;
end;

procedure TMetaEntity.TMetaEntityInternalData.DoExportToJSON(
  const AOwner: TMetaNodeEntityOrCollection; const AJSON: TJSONObject; const ACompact : boolean);
var
  JFields : TJSONArray;
  AncestorFields : TMetaFieldsFacade;
  OwnerField, AncestorField : TMetaField;
begin
  inherited;

  if Assigned(FAncestor) then
  begin
    AJSON.AddPair(sAncestor, FAncestor.Path);
    AncestorFields := FAncestor.Fields;
  end
  else
    AncestorFields.FTarget := nil;

  JFields := nil;
  try

    for OwnerField in FFields do
    begin

      if Assigned(FAncestor)
      and AncestorFields.TryFindByName(OwnerField.Name, AncestorField)
      and (AncestorField.ClassType = OwnerField.ClassType)
      then
        Continue;

      if not Assigned(JFields) then
        JFields := TJSONArray.Create();

      JFields.AddElement( OwnerField.ExportToJSON(ACompact) );
    end;

    if Assigned(JFields) then
      AJSON.AddPair(sFields, JFields);

    JFields := nil;
  finally
    FreeAndNil(JFields);
  end;
end;
{$ENDREGION}

{$REGION 'TMetaFieldsFacade'}
constructor TMetaFieldsFacade.Create(const ATarget: TMetaEntity);
begin
  FTarget := ATarget;
end;

function TMetaFieldsFacade.GetCount: integer;
begin
  if not Assigned(FTarget) then
    Result := 0
  else
    Result := Length(InternalData.FFields);
end;

function TMetaFieldsFacade.GetByIndex(
  const AIndex: integer): TMetaField;
begin
  if (AIndex < 0) or (AIndex > High(InternalData.FFields)) then
    raise EEntitiesError.CreateFmt(SListIndexError, [AIndex]);

  Result := InternalData.FFields[AIndex];
end;

function TMetaFieldsFacade.GetByName(
  const AName: string): TMetaField;
begin
  if not TryFindByName(AName, Result) then
    raise EEntitiesError.CreateResFmt(@s_San_Entities_MetaFieldNotFoundFmt, [AName, FTarget.Path]);
end;

function TMetaFieldsFacade.GetEnumerator: TArrayEnumerator<TMetaField>;
begin
  Result := TArrayEnumerator<TMetaField>.Create(InternalData.FFields);
end;

function TMetaFieldsFacade.TryFindByName(const AName: string;
  out AMetaField: TMetaField; const AIndexPtr : PInteger = nil): boolean;
begin
  Result := InternalData.TryFindFieldByName(AName, AMetaField, AIndexPtr);
end;

function TMetaFieldsFacade.TryFindByName<TField>(
  const AName: string; out AMetaField: TField;
  const AIndexPtr: PInteger): boolean;
var
  MetaField: TMetaField;
begin
  San.Api.Objs.ValidateGen<TField, TMetaField>();
  Result := TryFindByName(AName, MetaField, AIndexPtr);
  if Result then
    AMetaField := TField(MetaField)
  else
    AMetaField := nil;
end;

function TMetaFieldsFacade.ExistsByName(
  const AName: string): boolean;
var
  Dummy : TMetaField;
begin
  Result := TryFindByName(AName, Dummy);
end;

function TMetaFieldsFacade.Add(const AMetaField: TMetaField): TMetaField;
begin
  Result := Insert(Length(InternalData.FFields), AMetaField);
end;

function TMetaFieldsFacade.Add<TField>(const AName: string): TField;
var
  TI : PTypeInfo;
  MetaField : TMetaField;
begin
  San.Api.Objs.ValidateGen<TField, TMetaField>();

  TI := TypeInfo(TField);

  MetaField := TMetaFieldClass(TI^.TypeData^.ClassType).Create(FTarget.Target);
  try
    MetaField.Name := AName;
    TMetaField(Result) := Insert(Length(InternalData.FFields), MetaField);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TMetaFieldsFacade.Insert(const AIndex: integer;
  AMetaField: TMetaField): TMetaField;
var
  InternalData : TMetaEntity.TMetaEntityInternalData;
  i : integer;
begin
  InternalData := Self.InternalData;

  System.Insert(AMetaField, InternalData.FFields, AIndex);
  AMetaField.FIndex := AIndex;

  for i := Succ(AIndex) to High(InternalData.FFields) do
    InternalData.FFields[i].FIndex := i;

  Result := AMetaField;

  if FTarget.Root.IsEventsEnabled then
    FTarget.FireEventInsert(FTarget, AIndex, AMetaField);
end;

procedure TMetaFieldsFacade.Exchange(const AIndex1,
  AIndex2: integer);
var
  MetaField : TMetaField;
begin
  MetaField := InternalData.FFields[AIndex1];
  InternalData.FFields[AIndex1] := InternalData.FFields[AIndex2];
  InternalData.FFields[AIndex2] := MetaField;

  if FTarget.Root.IsEventsEnabled then
  begin
    FTarget.FireEventMove(FTarget, AIndex1, AIndex2, InternalData.FFields[AIndex2]);
    FTarget.FireEventMove(FTarget, AIndex2, AIndex1, InternalData.FFields[AIndex1]);
  end;
end;

procedure TMetaFieldsFacade.Delete(const AIndex: integer);
begin
  if FTarget.Root.IsEventsEnabled then
    FTarget.FireEventInsert(FTarget, AIndex, InternalData.FFields[AIndex]);
  System.Delete(InternalData.FFields, AIndex, 1);
end;
{$ENDREGION}

{$REGION 'TMetaEntity.TMetaFieldsFacadeHelper'}
function TMetaEntity.TMetaFieldsFacadeHelper.GetInternalData: TMetaEntity.TMetaEntityInternalData;
begin
  Result := FTarget.Target.InternalData as TMetaEntity.TMetaEntityInternalData;
end;
{$ENDREGION}

{$REGION 'TMetaEntity'}
class function TMetaEntity.GetInternalDataClass: TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalClass;
begin
  Result := TMetaEntityInternalData;
end;

function TMetaEntity.GetTarget: TMetaEntity;
begin
  Result := (inherited Target) as TMetaEntity;
end;

function TMetaEntity.GetFields: TMetaFieldsFacade;
begin
  if not Assigned(Self) then
    Exit( TMetaFieldsFacade.Create(Self) );

  if not Assigned(InternalData) then
    DoRecreateInternal();

  Result := TMetaFieldsFacade.Create(Self);
end;

function TMetaEntity.GetName: string;
begin
  if not Assigned(Owner) then

  else
  if (Owner is TMetaCollection) then
    Result := TMetaCollection(Owner).Name

  else
  if Owner is TMetaField then
    Result := TMetaField(Owner).Name

  else
    San.Api.Excps.ItsImpossible(Result);
end;

function TMetaEntity.DoGetPath: string;
begin
  if not Assigned(Owner) then
    Result := ''

  else
  if Owner is TMetaField then
    Result := TMetaField(Owner).Path

  else
  if Owner is TMetaCollection then
    Result := TMetaCollection(Owner).Path + PATH_META_ENTITY_IN_COLLECTION

  else
    Result := '';
end;

function TMetaEntity.GetAncestor: TMetaEntity;
begin
  if not Assigned(InternalData) or (InternalData is TMetaNodeEntityOrCollectionInternalLink) then
    Result := nil
  else
    Result := (InternalData as TMetaEntityInternalData).FAncestor;
end;

procedure TMetaEntity.DoSetAncestor(const AValue: TMetaEntity;
  const AUpdateFields: boolean);

  procedure ValidateCircularInheritance();
  var
    MetaEntity : TMetaEntity;
  begin
    MetaEntity := AValue;
    while Assigned(MetaEntity) do
    begin
      if MetaEntity = Self then
        raise EEntitiesError.CreateRes(@s_San_Entities_MetaCircularInheritanceAttempt);

      MetaEntity := MetaEntity.Ancestor;
    end;
  end;

var
  InternalData : TMetaEntityInternalData;
  AncestorPath : string;
begin
  InternalData := nil;
  try
    if not Assigned(Self.InternalData) then
      DoRecreateInternal()
    else
    if IsLink then
      raise EEntitiesMetaInheritance.CreateRes(@s_San_Entities_MetaCanNotAssignAncestorForLink);

    InternalData := Self.InternalData as TMetaEntityInternalData;

    ValidateDataEmpty();

    if Assigned(InternalData.FAncestor) then
      raise EEntitiesMetaInheritance.CreateRes(@s_San_Entities_MetaAncestorIsSetOnceAndCanNotBeChanged);

    if Assigned(AValue) then
    begin
      ValidateCircularInheritance();

      if AUpdateFields then
        InternalData.CloneFields(AValue, Self, True);

      InternalData.FAncestor := AValue;
    end;

  except
    if Assigned(InternalData) then
      AncestorPath := InternalData.FAncestor.Path
    else
      AncestorPath := 'nil';

    EEntitiesError.CreateResFmt(@s_San_Entities_MetaSetAncestorInternalErrorFmt, [Self.Path, AncestorPath, AValue.Path]).RaiseOuter();
  end;
end;

procedure TMetaEntity.SetAncestor(const AValue: TMetaEntity);
begin
  DoSetAncestor(AValue, True);
end;

function TMetaEntity.GetNodeType: TEntitiesNodeType;
begin
  Result := enntEntity;
end;

function TMetaEntity.GetRichDataClass: TDataObjectClass;
begin
  Result := inherited;
  if Result = TDataEntity then
    Result := Root.DataEntityBaseClass;
end;

function TMetaEntity.DoFindChildNode(const ANodeType : TEntitiesNodeType;
  const AName: string; out AMetaNode: TMetaNode): boolean;
var
  Field : TMetaField;
begin
  Result := Fields.TryFindByName(AName, Field);
  if Result then
    AMetaNode := Field
  else
    AMetaNode := nil;

  if Result and (ANodeType <> enntField) then
    Result := AMetaNode.DoFindChildNode(ANodeType, AName, AMetaNode);
end;

function TMetaEntity.AssignCheck(const ADest: TEntitiesObject): boolean;
begin
  Result := ADest is TMetaEntity;
end;

procedure TMetaEntity.AssignNoLinkTo(const ADest: TMetaNodeEntityOrCollection);
var
  Dest : TMetaEntity;
  SourceInternal, DestInternal : TMetaEntityInternalData;
begin
  inherited;

  Dest := ADest as TMetaEntity;
  SourceInternal  := Self.FInternalData  as TMetaEntityInternalData;
  DestInternal    := ADest.FInternalData as TMetaEntityInternalData;

  ClearObjsArray(DestInternal.FFields);
  DestInternal.CloneFields(Self, Dest, False);

  Dest.DoSetAncestor(SourceInternal.FAncestor, False);
end;

procedure TMetaEntity.DoImportFromJSON(const AJSON: TJSONObject; const AReWrite : boolean);
var
  Fields : TMetaFieldsFacade; // Не инициализируется при AReWrite = True

  function GetField(const AType : string; const AJSON: TJSONObject) : TMetaField;
  var
    JName : TJSONString;
    MetaFieldClass : TMetaFieldClass;
  begin

    if not AReWrite then
    begin

      if not AJSON.TryGetValue(sName, JName) then
        RaiseJSONImportPairNotFound(sName);

      if Fields.TryFindByName(JName.Value, Result) then
        Exit;
    end;

    try
      MetaFieldClass := TMetaField.FieldsByStrIDRegistry[AType];
    except
      on EListError do
        EEntitiesError.CreateResFmt(@s_San_Entities_MetaFieldClassNotFoundFmt, [AType]).RaiseOuter(MetaFieldClass);
    end;

    Result := MetaFieldClass.Create(Self);
  end;

var
  JLink, JType : TJSONString;
  JFields : TJSONArray;
  JObj : TJSONObject;
  InData : TMetaEntityInternalData;
  i : integer;
  Field : TMetaField;
begin
  inherited;

  if AJSON.TryGetValue(sLink, JLink) then
    DoRecreateInternal(JLink.Value, False)
  else
  if AJSON.TryGetValue(sRecursion, JLink) then
    DoRecreateInternal(JLink.Value, True)
  else
  begin
    DoRecreateInternal();

    InData := InternalData as TMetaEntityInternalData;

    if AJSON.TryGetValue(sFields, JFields) then
    begin

      if AReWrite then
        SetLength(InData.FFields, JFields.Count)
      else
        Fields := Self.Fields;

      for i := 0 to JFields.Count - 1 do
      begin

        if not (JFields.Items[i] is TJSONObject) then
          raise EEntitiesRWError.CreateResFmt(@s_San_Entities_JSONImportUnexpectedValueFmt, [JFields.Items[i].ClassName, JFields.Items[i].ToString]);

        JObj := JFields.Items[i] as TJSONObject;

        if not JObj.TryGetValue(sType, JType) then
          RaiseJSONImportPairNotFound(sType);

        Field := GetField(JType.Value, JObj);

        if AReWrite then
        begin
          Field.FIndex := i;
          InData.FFields[i] := Field;
        end

        else
        if Field.FIndex < 0 then
          Fields.Add(Field);

        try
          Field.DoImportFromJSON(JObj, AReWrite);
        except
          EEntitiesRWError.CreateResFmt(@s_San_Entities_JSONImportInternalErrorForFieldFmt, [Field.FIndex, Field.Name]).RaiseOuter();
        end;
      end;
    end;

  end;

end;
{$ENDREGION}

{$REGION 'TMetaCollectionIndex'}
constructor TMetaCollectionIndex.Create(const AOwner: TMetaCollection;
  const AIsUnique: boolean);
begin
  inherited Create();

  FOwner     := AOwner;
  FIsUnique  := AIsUnique;
end;

procedure TMetaCollectionIndex.Clone(const AOwner: TMetaCollection; out AClone);
var
  Res : TMetaCollectionIndex;
begin
  Res := TMetaCollectionIndexClass(ClassType).Create(AOwner, FIsUnique);
  try
    Res.Assign(Self);
    Pointer(AClone) := nil;
  except
    FreeAndNil(Res);
    Pointer(AClone) := nil;
    raise;
  end;
end;


procedure TMetaCollectionIndex.DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean);
begin
  inherited;
  AJSON.AddPair(sUnique, TJSONBool.Create(FIsUnique));
end;
{$ENDREGION}

{$REGION 'TMetaCollectionIndexSingle'}
constructor TMetaCollectionIndexSingle.Create(const AOwner: TMetaCollection;
  const AUnique: boolean; const AField: TMetaField);
begin
  inherited Create(AOwner, AUnique);

  FField := AField;
end;

function TMetaCollectionIndexSingle.AssignCheck(
  const ADest: TEntitiesObject): boolean;
begin
  Result := ADest is TMetaCollectionIndexSingle;
end;

procedure TMetaCollectionIndexSingle.AssignTo(const ADest: TEntitiesObject);
var
  Dest : TMetaCollectionIndexSingle;
begin
  inherited;

  Dest := ADest as TMetaCollectionIndexSingle;
  Dest.FField := Self.FField;
end;

procedure TMetaCollectionIndexSingle.DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean);
begin
  inherited;
  AJSON.AddPair(sField, FField.Name);
end;

function TMetaCollectionIndexSingle.GetRichDataClass: TDataObjectClass;
begin
  Result := FField.DataEntitiesIndexSingleClass;
end;
{$ENDREGION}

{$REGION 'TMetaCollection.TMetaCollectionInternalData'}
procedure TMetaCollection.TMetaCollectionInternalData.ClearBeforeFree;
begin
  ClearObjsArray(FIndexes);
  FreeAndNil(FItemMeta);
  inherited;
end;

procedure TMetaCollection.TMetaCollectionInternalData.CloneIndexes(
  const ASource, ADest: TMetaCollection);
var
  SourceIndex, ClonedIndex : TMetaCollectionIndex;
begin
  for SourceIndex in (ASource.InternalData as TMetaCollectionInternalData).FIndexes do
  begin
    SourceIndex.Clone(ADest, ClonedIndex);
    AddIndex(ClonedIndex);
  end;
end;

procedure TMetaCollection.TMetaCollectionInternalData.AddIndex(
  const AIndex: TMetaCollectionIndex);
begin
  System.Insert(AIndex, FIndexes, Length(FIndexes));
end;

procedure TMetaCollection.TMetaCollectionInternalData.DoExportToJSON(
  const AOwner: TMetaNodeEntityOrCollection; const AJSON: TJSONObject;
  const ACompact : boolean);
var
  JItemMeta : TJSONObject;
  JIndexes : TJSONArray;
  Index : TMetaCollectionIndex;
begin
  inherited;

  JItemMeta := (AOwner as TMetaCollection).ItemMeta.ExportToJSON(ACompact);
  try
    AJSON.AddPair(sEntity, JItemMeta);
    JItemMeta := nil;
  finally
    FreeAndNil(JItemMeta);
  end;

  if Length(FIndexes) = 0 then
    Exit;

  JIndexes := TJSONArray.Create();
  try

    for Index in FIndexes do
      JIndexes.AddElement(Index.ExportToJSON(ACompact));

    AJSON.AddPair(sIndexes, JIndexes);
    JIndexes := nil;
  finally
    FreeAndNil(JIndexes);
  end;

end;
{$ENDREGION}

{$REGION 'TMetaCollection.TMetaCollectionIndexesFacade'}
constructor TMetaCollection.TMetaCollectionIndexesFacade.Create(
  const ATarget: TMetaCollection);
begin
  FTarget := ATarget;
end;

function TMetaCollection.TMetaCollectionIndexesFacade.GetInternalData: TMetaCollectionInternalData;
begin
  Result := FTarget.Target.InternalData as TMetaCollectionInternalData;
end;

function TMetaCollection.TMetaCollectionIndexesFacade.GetCount: integer;
begin
  Result := Length(InternalData.FIndexes);
end;

function TMetaCollection.TMetaCollectionIndexesFacade.GetEnumerator: TArrayEnumerator<TMetaCollectionIndex>;
begin
  Result := TArrayEnumerator<TMetaCollectionIndex>.Create(InternalData.FIndexes);
end;

function TMetaCollection.TMetaCollectionIndexesFacade.GetItems(
  const AIndex: integer): TMetaCollectionIndex;
begin
  if (AIndex < 0) or (AIndex > High(InternalData.FIndexes)) then
    raise EEntitiesError.CreateFmt(SListIndexError, [AIndex]);

  Result := InternalData.FIndexes[AIndex];
end;

function TMetaCollection.TMetaCollectionIndexesFacade.TryFindSingleIndex(
  const AMetaField: TMetaField;
  out AMetaEntitiesIndexSingle: TMetaCollectionIndexSingle;
  const AIndexPtr : PInteger = nil): boolean;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    if  (InternalData.FIndexes[i] is TMetaCollectionIndexSingle)
    and (TMetaCollectionIndexSingle(InternalData.FIndexes[i]).Field = AMetaField)
    then
    begin
      AMetaEntitiesIndexSingle := TMetaCollectionIndexSingle(InternalData.FIndexes[i]);

      if Assigned(AIndexPtr) then
        AIndexPtr^ := i;

      Exit(True);
    end;

  AMetaEntitiesIndexSingle := nil;

  if Assigned(AIndexPtr) then
    AIndexPtr^ := -1;

  Result := False;
end;

function TMetaCollection.TMetaCollectionIndexesFacade.AddSingleIndex(
  const AMetaField: TMetaField;
  const AUnique: boolean): TMetaCollectionIndexSingle;
begin
  Result := TMetaCollectionIndexSingle.Create(FTarget.Target, AUnique, AMetaField);

  InternalData.AddIndex(Result);
end;

function TMetaCollection.TMetaCollectionIndexesFacade.RemoveSingleIndex(
  const AMetaField: TMetaField): boolean;
var
  i : integer;
  Dummy : TMetaCollectionIndexSingle;
begin
  Result := TryFindSingleIndex(AMetaField, Dummy, @i);
  if Result then
    System.Delete(InternalData.FIndexes, i, 0);

  Result := False;
end;
{$ENDREGION}

{$REGION 'TMetaCollection'}
class function TMetaCollection.GetInternalDataClass: TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalClass;
begin
  Result := TMetaCollectionInternalData;
end;

function TMetaCollection.GetTarget: TMetaCollection;
begin
  Result := (inherited Target) as TMetaCollection;
end;

function TMetaCollection.GetFields: TMetaFieldsFacade;
begin
  Result := ItemMeta.Fields;
end;

function TMetaCollection.GetIndexes: TMetaCollectionIndexesFacade;
begin
  Result := TMetaCollectionIndexesFacade.Create(Self);
end;

function TMetaCollection.GetItemMeta: TMetaEntity;
var
  CollectionInternalData : TMetaCollectionInternalData;
begin
  if not Assigned(InternalData) then
    DoRecreateInternal()

  else
  if IsLink then
    Exit(Target.ItemMeta);


  CollectionInternalData := San.Api.Objs.Cast<TMetaCollectionInternalData>(InternalData);
  if not Assigned(CollectionInternalData.FItemMeta) then
    CollectionInternalData.FItemMeta := TMetaEntity.Create(Self);

  Result := CollectionInternalData.FItemMeta;
end;

function TMetaCollection.GetName: string;
begin
  Result := MetaNameFromField(Self);
end;

function TMetaCollection.DoGetPath: string;
begin
  Result := (Owner as TMetaField).Path
end;

procedure TMetaCollection.DoRecreateInternal(const ALink: string;
  const AIsRecursion: boolean);
begin
  if AIsRecursion then
    raise EEntitiesError.CreateResFmt(@s_San_Entities_MetaCollectionCanNotBeRecursiveFmt, [Path]);

  inherited;
end;

function TMetaCollection.GetNodeType: TEntitiesNodeType;
begin
  Result := enntCollection;
end;

function TMetaCollection.GetRichDataClass: TDataObjectClass;
begin
  Result := inherited;
  if Result = TDataCollection then
    Result := Root.DataCollectionBaseClass;
end;

function TMetaCollection.AssignCheck(const ADest: TEntitiesObject): boolean;
begin
  Result := ADest is TMetaCollection;
end;

procedure TMetaCollection.AssignNoLinkTo(
  const ADest: TMetaNodeEntityOrCollection);
var
  Dest : TMetaCollection;
  SourceInternal, DestInternal : TMetaCollectionInternalData;
begin
  inherited;

  Dest := ADest as TMetaCollection;
  SourceInternal  := Self.FInternalData  as TMetaCollectionInternalData;
  DestInternal    := ADest.FInternalData as TMetaCollectionInternalData;

  ClearObjsArray(DestInternal.FIndexes);
  DestInternal.CloneIndexes(Self, Dest);

  if not Assigned(DestInternal.FItemMeta) then
    DestInternal.FItemMeta := TMetaEntity.Create(Self);

  DestInternal.FItemMeta.Assign(SourceInternal.FItemMeta);
end;

procedure TMetaCollection.DoImportFromJSON(const AJSON: TJSONObject; const AReWrite : boolean);
var
  JLink, JField : TJSONString;
  JUniqueIndex : TJSONBool;
  JIndexes : TJSONArray;
  JObj, JItemMeta : TJSONObject;
  InData : TMetaCollectionInternalData;
  Fields : TMetaFieldsFacade;
  i : integer;
  Indexes : TMetaCollection.TMetaCollectionIndexesFacade; // Инициализируется только при AReWrite = False
  UniqueIndex, IsNew : boolean;
  MetaField : TMetaField;
  CollectionIndex : TMetaCollectionIndexSingle;
begin
  inherited;

  if AJSON.TryGetValue(sLink, JLink) then
    DoRecreateInternal(JLink.Value, False)
  else
  if AJSON.TryGetValue(sRecursion, JLink) then
    DoRecreateInternal(JLink.Value, True)
  else
  begin
    DoRecreateInternal();

    if not AJSON.TryGetValue(sEntity, JItemMeta) then
      RaiseJSONImportPairNotFound(sEntity);

    InData := InternalData as TMetaCollectionInternalData;

    InData.FItemMeta := TMetaEntity.Create(Self);
    InData.FItemMeta.DoImportFromJSON(JItemMeta, AReWrite);

    if AJSON.TryGetValue(sIndexes, JIndexes) then
    begin
      Fields := InData.FItemMeta.Fields;

      if AReWrite then
        SetLength(InData.FIndexes, JIndexes.Count)
      else
        Indexes := Self.Indexes;

      for i := 0 to JIndexes.Count - 1 do
      begin

        if not (JIndexes.Items[i] is TJSONObject) then
          raise EEntitiesRWError.CreateResFmt(@s_San_Entities_JSONImportUnexpectedValueFmt, [JIndexes.Items[i].ClassName, JIndexes.Items[i].ToString]);

        JObj := JIndexes.Items[i] as TJSONObject;

        UniqueIndex := JObj.TryGetValue(sUnique, JUniqueIndex) and JUniqueIndex.AsBoolean;
        if JObj.TryGetValue(sField, JField) then
          begin
            MetaField := Fields.Items[JField.Value];
            IsNew := AReWrite or not Indexes.TryFindSingleIndex(MetaField, CollectionIndex);
            if IsNew then
              CollectionIndex := TMetaCollectionIndexSingle.Create(Self, UniqueIndex, MetaField);
          end
        else
          RaiseJSONImportPairNotFound(sField);

        if AReWrite then
          InData.FIndexes[i] := CollectionIndex
        else
          InData.AddIndex(CollectionIndex);
      end;
    end;

  end;
end;
{$ENDREGION}

{$REGION 'RichData'}
constructor RichData.Create(const APath: string);
begin
  inherited Create();
  FPath := APath;
end;
{$ENDREGION}

{$REGION 'TMetaEntityRoot'}
class function TMetaEntityRoot.GetBaseDataClass: TDataObjectClass;
begin
  Result := TDataEntityRoot;
end;

class function TMetaEntityRoot.CheckMetaOwnerClass(const AOwner: TMetaNode): boolean;
begin
  Result := AOwner = nil;
end;

constructor TMetaEntityRoot.Create();
begin
  inherited Create(nil);

  FRoot := Self;

  FEventSpace := TEventSpace.Create();
  FGuard := FEventSpace.CreateEventsSubscribeGuard(Self);

  FDataEntityBaseClass      := TDataEntity;
  FDataCollectionBaseClass  := TDataCollection;
end;

destructor TMetaEntityRoot.Destroy;
begin
  inherited;

  FGuard := nil;
  FreeAndNil(FEventSpace);
end;

function TMetaEntityRoot.GetDataRoots(const AIndex: integer): TDataEntityRoot;
begin
  Result := FDataObjects[AIndex] as TDataEntityRoot
end;

function TMetaEntityRoot.GetDataRootsCount: integer;
begin
  Result := FDataObjects.Count;
end;

function TMetaEntityRoot.GetMetaEvent: TMEEntities;
begin
  if not Assigned(FMEEntities) then
    TMEEntities.CreateInSpace(EventSpace, FMEEntities);

  Result := FMEEntities;
end;

function TMetaEntityRoot.GetName: string;
begin
  Result := FName;
end;

procedure TMetaEntityRoot.SetName(const AValue: string);
begin
  if FName = AValue then
    Exit;

  FName := AValue;
end;

procedure TMetaEntityRoot.SetVersion(const AValue: integer);
begin
  if FVersion = AValue then
    Exit;

  FVersion := AValue;
end;

function TMetaEntityRoot.DoGetPath: string;
begin
  Result := PATH_ROOT;
end;

procedure TMetaEntityRoot.DoRecreateInternal(const ALink: string;
  const AIsRecursion: boolean);
begin
  if AIsRecursion then
    raise EEntitiesError.CreateResFmt(@s_San_Entities_MetaRootCanNotBeLinkTypeFmt, [Path]);

  inherited;
end;

function TMetaEntityRoot.MakeData: TDataEntityRoot;
begin
  inherited MakeDataRaw(nil, Result);
end;

procedure TMetaEntityRoot.MakeDataRaw(out ADataRoot);
begin
  inherited MakeDataRaw(nil, ADataRoot);
end;

procedure TMetaEntityRoot.RichDataClassesReg(const APath : string;
  const ADataEntityClass : TDataEntityClass;
  const ADataCollectionClass : TDataCollectionClass);
var
  Lexemes : TArray<TPathResolver.TLexeme>;
  Lexeme : TPathResolver.TLexeme;
  ResolvedPath : TPathResolver.TPathItem;
  Node : TMetaNode;
  Field : TMetaField;
  Collection : TMetaCollection;
  Entity : TMetaEntity;
begin
  Lexemes := TPathResolver.LexicalAnalize(APath);
  for Lexeme in Lexemes do
    if not
    (
      Lexeme in
      [
        lxkSysIdentifierRoot,
        lxkSysIdentifierParent,
        lxkSysIdentifierThis,
        lxkDot,
        lxkIdentifier
      ]
    )
    then
      raise EEntitiesResolvingError.CreateResFmt(@s_San_Entities_ResolvingRichClassPathUnexpectedLexemeFmt, [Lexeme.Kind.ToString, APath, Lexeme.Start]);

  ResolvedPath := TPathResolver.SintaxAnalize(APath, Lexemes);
  try

    Node := ResolvedPath.FindEndTargetMeta(enntField, Self, Self);

  finally
    FreeAndNil(ResolvedPath);
  end;

  if Node is TMetaEntityRoot then
  begin
    Collection := nil;
    Entity := TMetaEntityRoot(Node);
  end

  else
  begin
    Field := San.Api.Objs.Cast<TMetaField>(Node);

    if Field.IsEntityField then
    begin
      Collection := nil;
      Entity := (Field as TMetaFieldEntity).Entity;
    end

    else
    if Field.IsCollectionField then
    begin
      Collection := (Field as TMetaFieldCollection).Collection;
      Entity := Collection.ItemMeta;
    end

    else
      raise EEntitiesError.CreateResFmt(@s_San_Entities_RichClassUnexpectedNodeForFmt, [Node.ClassName, APath]);
  end;

  if Assigned(Collection) and Assigned(ADataCollectionClass) then
    Collection.InternalData.RichDataClass  := ADataCollectionClass;

  if Assigned(Entity) and Assigned(ADataEntityClass) then
    Entity.InternalData.RichDataClass      := ADataEntityClass;
end;

procedure TMetaEntityRoot.RichDataClassesRegRTTI;
var
  Tp : TRttiType;
  Attrs : TArray<TCustomAttribute>;
  Attr : TCustomAttribute;
  RD : RichData;
  TpI : TRttiInstanceType;
begin
{
var
  Attrs : TArray<TCustomAttribute>;
  Attr : TCustomAttribute;
begin
  Attrs := ARttiObj.GetAttributes();

  for Attr in Attrs do
    if Attr.InheritsFrom(AAttrClass)  then
    begin
      TCustomAttribute(AAttr) := Attr;
      Exit(True);
    end;

  TCustomAttribute(AAttr) := nil;
  Result := False;

}

  for Tp in San.Api.Rtti.Context.GetTypes do
  begin
    Attrs := Tp.GetAttributes;

    for Attr in Attrs do
      if Attr is RichData then
      begin
        RD := RichData(Attr);

        if not (Tp is TRttiInstanceType) then
          raise ENotSupportedException.Create(Tp.Name);

        TpI := Tp as TRttiInstanceType;

        if not TpI.MetaclassType.InheritsFrom(TDataObject) then
          raise ENotSupportedException.Create(TpI.Name);

        if TpI.MetaclassType.InheritsFrom(TDataEntity) then
          RichDataClassesReg(RD.Path, TDataEntityClass(TpI.MetaclassType), nil)
        else
        if TpI.MetaclassType.InheritsFrom(TDataCollection) then
          RichDataClassesReg(RD.Path, nil, TDataCollectionClass(TpI.MetaclassType))
        else
          raise ENotSupportedException.Create(TpI.Name);
      end;
  end;
end;

function TMetaEntityRoot.AssignCheck(const ADest: TEntitiesObject): boolean;
begin
  Result := ADest is TMetaEntityRoot;
end;

procedure TMetaEntityRoot.AssignTo(const ADest: TEntitiesObject);
var
  Dest : TMetaEntityRoot;
begin
  inherited;

  Dest := ADest as TMetaEntityRoot;

  Dest.FName          := Self.FName;
  Dest.FVersion       := Self.FVersion;
end;

procedure TMetaEntityRoot.DoExportToJSON(const AJSON : TJSONObject; const ACompact : boolean);
var
  JVersion : TJSONNumber;
begin
  AJSON.AddPair(sName,    FName);

  JVersion := TJSONNumber.Create(FVersion);
  try
    AJSON.AddPair(sVersion, JVersion);
    JVersion := nil;
  finally
    FreeAndNil(JVersion);
  end;

  inherited;
end;

procedure TMetaEntityRoot.DoImportFromJSON(const AJSON: TJSONObject; const AReWrite : boolean);
var
  JVersion : TJSONNumber;
  JName : TJSONString;
begin
  if not AJSON.TryGetValue(sVersion, JVersion) then
    RaiseJSONImportPairNotFound(sVersion);

  FVersion := JVersion.AsInt;

  if not AJSON.TryGetValue(sName, JName) then
    RaiseJSONImportPairNotFound(sName);

  FName := JName.Value;

  inherited;
end;
{$ENDREGION}

{$ENDREGION}

{$REGION 'data'}

{$REGION 'TDataObject.TInternalNotificationContext'}
constructor TDataObject.TInternalNotificationContext.Create(const ASender: TDataObject;
  const AAction: TDataObject.TInternalNotificationAction);
begin
  Sender := ASender;
  Action := AAction;
  Item   := nil;
end;

constructor TDataObject.TInternalNotificationContext.Create(const ASender,
  AItem: TDataObject; const AAction: TInternalNotificationAction);
begin
  Sender := ASender;
  Action := AAction;
  Item   := AItem;
end;

class operator TDataObject.TInternalNotificationContext.Equal(const A,
  B: TInternalNotificationContext): boolean;
begin
  Result :=     CompareMem(@A, @B, SizeOf(TInternalNotificationContext));
end;

class operator TDataObject.TInternalNotificationContext.NotEqual(const A,
  B: TInternalNotificationContext): boolean;
begin
  Result := not CompareMem(@A, @B, SizeOf(TInternalNotificationContext));
end;
{$ENDREGION}

{$REGION 'TDataObject.TInternalNotifierData'}
constructor TDataObject.TInternalNotifierData.Create(
  const ANotifier: TInternalNotifier; const AHost : TDataObject);
begin
  inherited Create();
  FNotifier  := ANotifier;
  FHost      := AHost;
end;

destructor TDataObject.TInternalNotifierData.Destroy;
begin
  if Assigned(FHost) then
    FHost.InternalNotificationRemove(Self);

  inherited;
end;

procedure TDataObject.TInternalNotifierData.Notify(
  const AContext: TDataObject.TInNtfCtx);
begin
  FNotifier(AContext);
end;

function TDataObject.TInternalNotifierData.ExportToJSON: TJSONValue;
var
  M : TMethod;
  D : TDataObject;
  J : TJSONObject;
begin
  if not Assigned(FNotifier) then
    Exit(nil);

  J := TJSONObject.Create();
  try

    M := TMethod(FNotifier);
    if not (TObject(M.Data) is TDataObject) then
      J.AddPair('Error', Format('Class "%s" is not %s', [TObject(M.Data).ClassName, TDataObject]) )

    else
    begin
      D := TDataObject(M.Data);
      J.AddPair('Class', D.ClassName);

      if (D is TDataNode) then
        if Assigned(TDataNode(D).Parent)  then
          J.AddPair('ParentClass', TDataNode(D).Parent.ClassName)
        else
          J.AddPair('ParentClass', 'nil');
    end;

    Result := J;
    J := nil;
  finally
    FreeAndNil(J);
  end;

end;

{$ENDREGION}

{$REGION 'TDataObject'}
class procedure TDataObject.ValidateCreateContext(
  const AContext: TDataObject.TCreateContext);
begin
end;

constructor TDataObject.InternalCreate(
  const AContext: TDataObject.TCreateContext);
begin
  inherited Create();

  FMeta := AContext.Meta;

  FMeta.DataCreating(Self);
end;

function TDataObject.InternalNotificationAdd(const ANotifier : TInternalNotifier) : TInternalNotifierData;
begin
  Result := TInternalNotifierData.Create(ANotifier, Self);
  try
    System.Insert(Result, FInternalNotification, Length(FInternalNotification));
  except
    FreeAndNil(Result);
    raise;
  end;
end;

procedure TDataObject.InternalNotificationRemove(
  const ANotifierData : TInternalNotifierData);
var
  i : integer;
begin
  for i := 0 to High(FInternalNotification) do
    if ANotifierData = FInternalNotification[i] then
    begin
      System.Delete(FInternalNotification, i, 1);
      ANotifierData.FHost := nil;
      Break;
    end;
end;

procedure TDataObject.DoDebugExportToJSON(const AJSONObject: TJSONObject);
var
  inArrJ : TJSONArray;
  inData : TInternalNotifierData;
begin
  inherited;

  if not Assigned(FInternalNotification) then
    Exit;

  inArrJ := TJSONArray.Create();
  try

    for inData in FInternalNotification do
      inArrJ.AddElement( inData.ExportToJSON() );

    AJSONObject.AddPair('InternalNotification', inArrJ);
    inArrJ := nil;
  finally
    FreeAndNil(inArrJ);
  end;

end;

procedure TDataObject.DoInternalNotificationNotify(
  const AContext: TDataObject.TInNtfCtx);
var
  i : integer;
  D : TInternalNotifierData;
begin
  for i := 0 to High(FInternalNotification) do
  begin
    D := FInternalNotification[i];
    if Assigned(D) then
      D.Notify(AContext);
  end;
end;

procedure TDataObject.InternalNotificationNotify(
  const AContext: TDataObject.TInNtfCtx);

  procedure RemoveUpdatesNotificationDuplicates();
  var
    i : integer;
  begin
    for i := High(FUpdatesNotification) downto 0 do
      if FUpdatesNotification[i] = AContext then
        System.Delete(FUpdatesNotification, i, 1);
  end;

begin
  if Assigned(FUpdates) and (FUpdates.UpdatesCount > 0) then
  begin
    RemoveUpdatesNotificationDuplicates();
    System.Insert(AContext, FUpdatesNotification, Length(FUpdatesNotification))
  end

  else
    DoInternalNotificationNotify(AContext);
end;

procedure TDataObject.DoUpdate;
var
  C : TDataObject.TInNtfCtx;
begin
  inherited;

  try

    for C in FUpdatesNotification do
      DoInternalNotificationNotify(C);

  finally
    SetLength(FUpdatesNotification, 0);
  end;

end;

constructor TDataObject.Create;
begin
  raise EEntitiesError.CreateResFmt(@s_San_Entities_ForbiddenCreateDataDirectlyFmt, [ClassName]);
end;

procedure TDataObject.BeforeDestruction;
var
  Ctx : TInNtfCtx;
begin
  if Length(FInternalNotification) > 0 then
  begin
    Ctx := TInNtfCtx.Create(Self, inaClear);
    InternalNotificationNotify(Ctx);
  end;

  inherited;
end;

destructor TDataObject.Destroy;
var
  i : integer;
begin
  FMeta.DataDestroing(Self);

  inherited;

  for i := 0 to High(FInternalNotification) do
    FInternalNotification[i].FHost := nil;

  {$IFDEF DEBUG}
    {$IFDEF MSWINDOWS}
    OutputDebugString(PChar(Format('"%s" destroyed', [ClassName])));
    {$ENDIF}
  {$ENDIF}
end;

function TDataObject.GetInitialMeta: TMetaObject;
begin
  Result := FMeta;
end;

function TDataObject.GetInternalNotificationCount: integer;
begin
  Result := Length(FInternalNotification);
end;

procedure TDataObject.Refresh;
begin
end;

procedure TDataObject.WeakRef(out ARef);
begin
  TDataWeakRef<TDataObject>(ARef).Target := Self;
end;
{$ENDREGION}

{$REGION 'TDataNode'}
class procedure TDataNode.ValidateCreateContext(
  const AContext: TDataObject.TCreateContext);
begin
  inherited;

  if not CheckMetaClass(AContext.Meta as TMetaNode) then
     raise EEntitiesError.CreateResFmt(@s_San_Entities_InvalidMetaClassErrorFmt, [AContext.Meta.ClassName, Self.ClassName]);

  if Assigned(AContext.Owner) and ( (AContext.Meta as TMetaNode).Root <> (AContext.Owner as TDataNode).Meta.Root) then
      raise EEntitiesError.CreateRes(@s_San_Entities_OtherRootError);

  if not CheckOwnerClass(AContext.Owner as TDataNode) then
    if Assigned(AContext.Owner) then
      raise EEntitiesError.CreateResFmt(@s_San_Entities_InvalidDataOwnerClassErrorFmt, [AContext.Owner.ClassName, Self.ClassName])

    else
      raise EEntitiesError.CreateResFmt(@s_San_Entities_InvalidDataOwnerClassErrorFmt, ['nil', Self.ClassName]);
end;

constructor TDataNode.InternalCreate(const AContext: TDataObject.TCreateContext);
var
  Parent : TDataNode;
begin
  inherited;

  FOwner := AContext.Owner as TDataNode;

  Parent := Self.Parent;
  while Assigned(Parent) and not (Parent is TDataEntityRoot) do
    Parent := Parent.Parent;

  FRoot := TDataEntityRoot(Parent);
end;

destructor TDataNode.Destroy;
begin

  if Assigned(Owner) then
    Owner.OwnedDestroing(Self);

  inherited;
end;

function TDataNode.DoFindChildNode(const ANodeType : TEntitiesNodeType;
  const AName: string; out ADataNode: TDataNode): boolean;
begin
  Result := False;
  ADataNode := nil;
end;

class function TDataNode.CheckMetaClass(const AMeta: TMetaNode): boolean;
begin
  Result := AMeta.InheritsFrom(GetMetaClass());
end;

class function TDataNode.CheckOwnerClass(const AOwner: TDataNode): boolean;
begin
  Result := AOwner.InheritsFrom(GetDataOwnerClass());
end;

procedure TDataNode.Clone(const AOwner: TDataNode; out AClone);
var
  Res : TDataObject;
begin
  Res := Meta.MakeData(AOwner);
  try

    Res.Assign(Self);
    Pointer(AClone) := Res;
  except
    FreeAndNil(Res);
    raise;
  end;

  Pointer(AClone) := nil;
end;

function TDataNode.GetNodeType: TEntitiesNodeType;
begin
  Result := Meta.NodeType;
end;

function TDataNode.GetParent: TDataEntity;
var
  Node : TDataNode;
begin
  Node := FOwner;
  while Assigned(Node) and not (Node is TDataEntity) do
    Node := Node.Owner;

  if Assigned(Node) then
    Result := TDataEntity(Node)
  else
    Result := nil;
end;

class function TDataNode.GetDataOwnerClass: TDataObjectClass;
begin
  raise ENotImplemented.Create(ClassName);
end;

function TDataNode.GetGuard: IEventsSubscribeGuard;
begin
  Result := Meta.Root.FGuard;
end;

function TDataNode.GetInitialOwner: TDataNode;
begin
  Result := FOwner;
end;

function TDataNode.GetMeta: TMetaNode;
begin
  Result := TMetaNode(GetInitialMeta());
end;

class function TDataNode.GetMetaClass: TMetaNodeClass;
begin
  raise ENotImplemented.Create(ClassName);
end;

function TDataNode.GetName: string;
begin
  Result := Meta.Name;
end;

function TDataNode.GetPath: string;
var
  pIndex : PInteger;
begin
  if not Assigned(Owner) then
    Result := Self.Name

  else
  if (NodeType = enntEntity) and TDataEntity(Self).AccessForIndexPtr(pIndex) then
    Result := Format('%s%s%s[%d]', [Parent.Path, PATH_SEPARATOR, Self.Name, pIndex^])

  else
    Result := Format('%s%s%s', [Parent.Path, PATH_SEPARATOR, Self.Name]);
end;

procedure TDataNode.OwnedDestroing(const AOwned: TDataObject);
begin
end;

function TDataNode.GetLockerData: TMultithreadLockerFacade.TMultithreadLockerData;
begin
  Result := Parent.GetLockerData();
end;

function TDataNode.ExternalTryEnterRead(const AStartTicks, ATimeOut: Cardinal;
  out AFailedToLock: TObject): boolean;
var
  Node : TDataNode;
begin
  Node := Owner;
  while Assigned(Node) and not (Node.NodeType in [enntEntity, enntCollection]) do
    Node := Node.Owner;

  Result := not Assigned(Node)
         or Node.Locker.TryEnterRead(AStartTicks, ATimeOut);

  if Result then
    AFailedToLock := nil
  else
    AFailedToLock := Node;
end;

function TDataNode.ExportToJSON(const ACompact : boolean): TJSONValue;
begin
  try
    Result := inherited;
  except
    EEntitiesRWError.CreateResFmt(@s_San_Entities_JSONExportInternalErrorForNodeFmt, [ClassName, Path]).RaiseOuter(Result);
  end;
end;

procedure TDataNode.ExternalLeaveRead(const AFailedToLock: TObject);
var
  Node : TDataNode;
begin
  Node := Owner;
  while Assigned(Node) and not (Node.NodeType in [enntEntity, enntCollection]) do
    Node := Node.Owner;

  if Assigned(Node) and (Node <> AFailedToLock) then
    Node.Locker.LeaveRead();
end;
{$ENDREGION}

{$REGION 'TDataNode<TMeta, TOwner>'}
class function TDataNode<TMeta, TOwner>.GetMetaClass: TMetaNodeClass;
begin
  Result := TMetaNodeClass(TMeta);
end;

class function TDataNode<TMeta, TOwner>.GetDataOwnerClass: TDataObjectClass;
begin
  Result := TDataObjectClass(TOwner);
end;

function TDataNode<TMeta, TOwner>.GetMeta: TMeta;
begin
  Result := TMeta(inherited Meta);
end;

function TDataNode<TMeta, TOwner>.GetOwner: TOwner;
begin
  Result := TOwner(inherited Owner);
end;
{$ENDREGION}

{$REGION 'TDataField'}
function TDataField.GetIndex: Integer;
begin
  Result := Meta.Index;
end;

procedure TDataField.CastReadError<TFrom, TTo>(out AResult);
var
  FromName, ToName : string;
begin
  FromName  := string( PTypeInfo(TypeInfo(TFrom))^.NameFld.ToString );
  ToName    := string( PTypeInfo(TypeInfo(TTo  ))^.NameFld.ToString );

  raise EEntitiesError.CreateResFmt(@s_San_Entities_DataFieldReadCastErrorFmt, [FromName, ToName]);
end;

procedure TDataField.CastWriteError<TFrom, TTo>;
var
  FromName, ToName : string;
begin
  FromName  := string( PTypeInfo(TypeInfo(TFrom))^.NameFld.ToString );
  ToName    := string( PTypeInfo(TypeInfo(TTo  ))^.NameFld.ToString );

  raise EEntitiesError.CreateResFmt(@s_San_Entities_DataFieldWriteCastErrorFmt, [FromName, ToName]);
end;

procedure TDataField.CheckCanEdit;
begin
  // is Edit mode?
end;

function TDataField.TryGetSingleIndex(
  out AIndex: TDataCollectionIndexSingle): boolean;
begin
  Result := Assigned(Owner.Owner)
        and (Owner.Owner is TDataCollection)
        and TDataCollection(Owner.Owner).Indexes.TryFindSingleIndex(Self.Meta, AIndex);

  if not Result then
    AIndex := nil;
end;

class function TDataField.ValueType: PTypeInfo;
begin
  raise ENotImplemented.Create(ClassName);
end;

function TDataField.AsType<TValueType>: TValueType;
begin
  GetAsType(TypeInfo(TValueType), Result);
end;

procedure TDataField.From<TValueType>(const AValue: TValueType);
begin
  SetAsType(TypeInfo(TValueType), AValue);
end;

procedure TDataField.GetAsType(const ATypeInfo: PTypeInfo; out AValue);
begin
  GetAsTypeWithCast(ATypeInfo, AValue);
end;

procedure TDataField.GetAsTypeWithCast(const ATypeInfo: PTypeInfo; out AValue);
var
  V, C : TValue;
begin
  V := AsValue;

  try
    C := V.Cast(ATypeInfo);
  except
    EEntitiesError.CreateResFmt(@s_San_Entities_ValueCastErrorFromToFmt, [ValueType^.NameFld.ToString, ATypeInfo^.NameFld.ToString]).RaiseOuter();
  end;

  C.ExtractRawData(@AValue);
end;

procedure TDataField.SetAsType(const ATypeInfo: PTypeInfo; const AValue);
var
  V : TValue;
begin
  if ValueType^.Kind <> ATypeInfo^.Kind then
    SetAsTypeWithCast(ATypeInfo, AValue)

  else
  begin
    TValue.Make(@AValue, ATypeInfo, V);
    AsValue := V;
  end;
end;

procedure TDataField.SetAsTypeWithCast(const ATypeInfo: PTypeInfo;
  const AValue);
var
  V, C : TValue;
begin
  TValue.Make(@AValue, ATypeInfo, V);

  try
    C := V.Cast(ValueType);
  except
    EEntitiesError.CreateResFmt(@s_San_Entities_ValueCastErrorFromToFmt, [ATypeInfo^.NameFld.ToString, ValueType^.NameFld.ToString]).RaiseOuter();
  end;

  AsValue := C;
end;
{$ENDREGION}

{$REGION 'TDataField<TMeta,T>'}
procedure TDataField<TMeta,T>.CastReadError<TTo>(out AResult);
begin
  inherited CastReadError<T, TTo>(AResult);
end;

procedure TDataField<TMeta,T>.CastWriteError<TTo>;
begin
  inherited CastWriteError<T, TTo>;
end;

constructor TDataField<TMeta, T>.InternalCreate(
  const AContext: TDataObject.TCreateContext);
begin
  inherited;

  FValue := DefaultValue;
end;

function TDataField<TMeta,T>.IsEqualValue(const AValue: T): boolean;
begin
  Result := CompareMem(@FValue, @AValue, SizeOf(T));
end;

function TDataField<TMeta, T>.GetDefaultValue: T;
begin
  if PTypeInfo(TypeInfo(T))^.Kind = tkClass then
    PObject(@Result)^ := nil
  else
    Result := San.Api.Objs.Cast<TMetaField<T>>(Meta).DefaultValue;
end;

procedure TDataField<TMeta,T>.SetNewValue<TFrom>(const AValue: T);
var
  Index : TDataCollectionIndexSingle;
  OldValue : T;
  Parent : TDataEntity;
begin
  CheckCanEdit;

  if IsEqualValue(AValue) then
    Exit;

  {$REGION 'Before Setting'}
  if TryGetSingleIndex(Index) then
    Index.RemoveIndexItem(Self);
  {$ENDREGION}

  {$REGION 'Setting'}
  OldValue := FValue;
  FValue := AValue;
  {$ENDREGION}

  {$REGION 'After Setting'}
  if Assigned(Index) then
    Index.AddIndexItem(Self);

  {$REGION 'Notification'}
  if InternalNotificationCount > 0 then
    InternalNotificationNotify( TDataObject.TInNtfCtx.Create(Self, TDataObject.TInternalNotificationAction.inaChanged) );

  if TMetaNode(Meta).Root.IsEventsEnabled then
  begin
    Parent := Self.Parent;;
    Parent.FireEventChange(Parent, Self, TValue.From<T>(OldValue), TValue.From<T>(AValue));
  end;
  {$ENDREGION}
  {$ENDREGION}
end;

function TDataField<TMeta,T>.GetAsBoolean: Boolean;
begin
  CastReadError<boolean>(Result);
end;

function TDataField<TMeta,T>.GetAsItems: TDataCollection;
begin
  CastReadError<TDataCollection>(Result);
end;

function TDataField<TMeta,T>.GetAsEntity: TDataEntity;
begin
  CastReadError<TDataEntity>(Result);
end;

function TDataField<TMeta,T>.GetAsExtended: Extended;
begin
  CastReadError<Extended>(Result);
end;

function TDataField<TMeta,T>.GetAsDouble: Double;
begin
  CastReadError<Double>(Result);
end;

function TDataField<TMeta,T>.GetAsGUID: TGUID;
begin
  CastReadError<TGUID>(Result);
end;

function TDataField<TMeta,T>.GetAsInt64: Int64;
begin
  CastReadError<Int64>(Result);
end;

function TDataField<TMeta,T>.GetAsInteger: Integer;
begin
  CastReadError<Integer>(Result);
end;

function TDataField<TMeta,T>.GetAsNode: TDataNode;
begin
  CastReadError<TDataNode>(Result);
end;

function TDataField<TMeta,T>.GetAsString: string;
begin
  CastReadError<string>(Result);
end;

function TDataField<TMeta,T>.GetAsValue: TValue;
begin
  Result := TValue.From<T>(FValue);
end;

function TDataField<TMeta, T>.GetMeta: TMeta;
begin
  Result := TMeta(inherited Meta);
end;

class function TDataField<TMeta, T>.GetMetaClass: TMetaNodeClass;
begin
  Result := TMetaNodeClass(TMeta);
end;

function TDataField<TMeta,T>.GetAsChar: Char;
begin
  CastReadError<Char>(Result);
end;

function TDataField<TMeta,T>.GetValue: Variant;
begin
  CastReadError<Variant>(Result);
end;

procedure TDataField<TMeta,T>.SetAsBoolean(const AValue: Boolean);
begin
  CastWriteError<boolean>;
end;

procedure TDataField<TMeta,T>.SetAsExtended(const AValue: Extended);
begin
  CastWriteError<Extended>;
end;

procedure TDataField<TMeta,T>.SetAsDouble(const AValue: Double);
begin
  CastWriteError<Double>;
end;

procedure TDataField<TMeta,T>.SetAsGUID(const AValue: TGUID);
begin
  CastWriteError<TGUID>;
end;

procedure TDataField<TMeta,T>.SetAsInt64(const AValue: Int64);
begin
  CastWriteError<Int64>;
end;

procedure TDataField<TMeta,T>.SetAsInteger(const AValue: Integer);
begin
  CastWriteError<Integer>;
end;

procedure TDataField<TMeta,T>.SetAsString(const AValue: string);
begin
  CastWriteError<string>;
end;

procedure TDataField<TMeta,T>.SetAsValue(const AValue: TValue);
begin
  SetNewValue<TValue>(AValue.AsType<T>);
end;

procedure TDataField<TMeta,T>.SetAsChar(const AValue: Char);
begin
  CastWriteError<Char>;
end;

procedure TDataField<TMeta,T>.SetValue(const AValue: Variant);
begin
  CastWriteError<Variant>;
end;

class function TDataField<TMeta,T>.ValueType: PTypeInfo;
begin
  Result := TypeInfo(T);
end;

function TDataField<TMeta, T>.CompareDataFieldTo(const AOtherField: TDataField;
  const AOperation: TEntitiesCompareOperation): Bool3;
begin
  if (AOtherField is TDataField<TMeta, T>) and (AOperation in [ecoNotEquals, ecoEquals]) then
  begin
    Result := TEqualityComparer<T>.Default.Equals(FValue, TDataField<TMeta, T>(AOtherField).FValue);
    if AOperation = ecoNotEquals then
      Result := not Result;
  end
  else
    Result := Null3;
end;

function TDataField<TMeta, T>.CompareDataValueTo(const AOtherData: TValue;
  const AOperation: TEntitiesCompareOperation): Bool3;
begin
  if (AOperation in [ecoNotEquals, ecoEquals]) then
  begin

    try
      Result := TEqualityComparer<T>.Default.Equals(FValue, AOtherData.AsType<T>);
    except
      EEntitiesError.CreateResFmt(@s_San_Entities_FieldCompareDataValueToInternalErrorFmt, [Self.Name, Self.ValueType^.NameFld.ToString, PTypeInfo(TypeInfo(T))^.NameFld.ToString]).RaiseOuter(Result);
    end;

    if AOperation = ecoNotEquals then
      Result := not Result;
  end
  else
    Result := Null3;
end;

procedure TDataField<TMeta, T>.GetAsType(const ATypeInfo: PTypeInfo;
  out AValue);
var
  ptiT : PTypeInfo;
  Node : TDataNode;
begin
  ptiT := TypeInfo(T);

  if ptiT^.Kind <> ATypeInfo^.Kind then
    GetAsTypeWithCast(ATypeInfo, AValue)

  else
  if ptiT^.Kind <> tkClass then
    T(AValue) := FValue

  else
  begin
    Node := AsNode;
    T(AValue) := San.Api.Objs.CastRaw<T>(PObject(@Node)^);
  end;
end;

function TDataField<TMeta, T>.DoExportToJSON(const ACompact : boolean = True): TJSONValue;
begin
  if not TEqualityComparer<T>.Default.Equals(FValue, DefaultValue) then
    Result := Api.ValueToJSON<T>(FValue)
  else
    Result := nil;
end;

procedure TDataField<TMeta, T>.DoImportFromJSON(const AJSON: TJSONValue;
  const AReWrite: boolean);
begin
  SetNewValue<T>(Api.JSONToValue<T>(AJSON));
end;
{$ENDREGION}

{$REGION 'TDataFieldInteger'}
function TDataFieldInteger.GetAsBoolean: Boolean;
begin
  Result := FValue <> 0;
end;

function TDataFieldInteger.GetAsChar: Char;
begin
  Result := Char(FValue);
end;

function TDataFieldInteger.GetAsDouble: Double;
begin
  Result := FValue;
end;

function TDataFieldInteger.GetAsExtended: Extended;
begin
  Result := FValue;
end;

function TDataFieldInteger.GetAsInt64: Int64;
begin
  Result := FValue;
end;

function TDataFieldInteger.GetAsInteger: Integer;
begin
  Result := FValue;
end;

function TDataFieldInteger.GetAsString: string;
begin
  Result := IntToStr(FValue);
end;

function TDataFieldInteger.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TDataFieldInteger.SetAsBoolean(const AValue: Boolean);
begin
  SetNewValue<Boolean>(Ord(AValue));
end;

procedure TDataFieldInteger.SetAsChar(const AValue: Char);
begin
  SetNewValue<Char>(Ord(AValue));
end;

procedure TDataFieldInteger.SetAsDouble(const AValue: Double);
begin
  SetNewValue<Double>(Trunc(AValue));
end;

procedure TDataFieldInteger.SetAsExtended(const AValue: Extended);
begin
  SetNewValue<Extended>(Trunc(AValue));
end;

procedure TDataFieldInteger.SetAsInt64(const AValue: Int64);
begin
  SetNewValue<Int64>(AValue);
end;

procedure TDataFieldInteger.SetAsInteger(const AValue: Integer);
begin
  SetNewValue<Integer>(AValue);
end;

procedure TDataFieldInteger.SetAsString(const AValue: string);
begin
  SetNewValue<string>( StrToInt(AValue) );
end;

procedure TDataFieldInteger.SetValue(const AValue: Variant);
begin
  SetNewValue<Variant>(AValue);
end;

procedure TDataFieldInteger.GetAsTypeWithCast(const ATypeInfo: PTypeInfo;
  out AValue);
begin
  if ATypeInfo = TypeInfo(boolean) then
    Boolean(AValue) := AsBoolean
  else
    inherited;
end;

procedure TDataFieldInteger.SetAsTypeWithCast(const ATypeInfo: PTypeInfo;
  const AValue);
begin
  if ATypeInfo = TypeInfo(boolean) then
    AsBoolean := Boolean(AValue)
  else
    inherited;
end;
{$ENDREGION}

{$REGION 'TDataFieldInt64'}
function TDataFieldInt64.GetAsBoolean: Boolean;
begin
  Result := FValue <> 0;
end;

function TDataFieldInt64.GetAsChar: Char;
begin
  Result := Char(FValue);
end;

function TDataFieldInt64.GetAsDouble: Double;
begin
  Result := FValue;
end;

function TDataFieldInt64.GetAsExtended: Extended;
begin
  Result := FValue;
end;

function TDataFieldInt64.GetAsInt64: Int64;
begin
  Result := FValue;
end;

function TDataFieldInt64.GetAsInteger: Integer;
begin
  Result := FValue;
end;

function TDataFieldInt64.GetAsString: string;
begin
  Result := IntToStr(FValue);
end;

function TDataFieldInt64.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TDataFieldInt64.SetAsBoolean(const AValue: Boolean);
begin
  SetNewValue<Boolean>(Ord(AValue));
end;

procedure TDataFieldInt64.SetAsChar(const AValue: Char);
begin
  SetNewValue<Char>(Ord(AValue));
end;

procedure TDataFieldInt64.SetAsDouble(const AValue: Double);
begin
  SetNewValue<Double>(Trunc(AValue));
end;

procedure TDataFieldInt64.SetAsExtended(const AValue: Extended);
begin
  SetNewValue<Extended>(Trunc(AValue));
end;

procedure TDataFieldInt64.SetAsInt64(const AValue: Int64);
begin
  SetNewValue<Int64>(AValue);
end;

procedure TDataFieldInt64.SetAsInteger(const AValue: Integer);
begin
  SetNewValue<Integer>(AValue);
end;

procedure TDataFieldInt64.SetAsString(const AValue: string);
begin
  SetNewValue<string>( StrToInt(AValue) );
end;

procedure TDataFieldInt64.SetValue(const AValue: Variant);
begin
  SetNewValue<Variant>(AValue);
end;
{$ENDREGION}

{$REGION 'TDataFieldString'}
function TDataFieldString.IsEqualValue(const AValue: string): boolean;
begin
  Result := FValue = AValue;
end;

function TDataFieldString.GetAsBoolean: Boolean;
begin
  Result := StrToBool(FValue);
end;

function TDataFieldString.GetAsChar: Char;
begin
  if Length(FValue) = 0 then
    Result := #0
  else
    Result := FValue[1];
end;

function TDataFieldString.GetAsDouble: Double;
begin
  Result := StrToFloat(FValue);
end;

function TDataFieldString.GetAsExtended: Extended;
begin
  Result := StrToFloat(FValue);
end;

function TDataFieldString.GetAsGUID: TGUID;
begin
  Result := StringToGUID(FValue);
end;

function TDataFieldString.GetAsInt64: Int64;
begin
  Result := StrToInt(FValue);
end;

function TDataFieldString.GetAsInteger: Integer;
begin
  Result := StrToInt(FValue);
end;

function TDataFieldString.GetAsString: string;
begin
  Result := FValue;
end;

function TDataFieldString.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TDataFieldString.SetAsBoolean(const AValue: Boolean);
begin
  SetNewValue<boolean>(BoolToStr(AValue));
end;

procedure TDataFieldString.SetAsChar(const AValue: Char);
begin
  SetNewValue<Char>(AValue);
end;

procedure TDataFieldString.SetAsDouble(const AValue: Double);
begin
  SetNewValue<Double>(FloatToStr(AValue));
end;

procedure TDataFieldString.SetAsExtended(const AValue: Extended);
begin
  SetNewValue<Extended>(FloatToStr(AValue));
end;

procedure TDataFieldString.SetAsGUID(const AValue: TGUID);
begin
  SetNewValue<TGUID>(GUIDToString(AValue));
end;

procedure TDataFieldString.SetAsInt64(const AValue: Int64);
begin
  SetNewValue<Int64>(IntToStr(AValue));
end;

procedure TDataFieldString.SetAsInteger(const AValue: Integer);
begin
  SetNewValue<Integer>(IntToStr(AValue));
end;

procedure TDataFieldString.SetAsString(const AValue: string);
begin
  SetNewValue<string>(AValue);
end;

procedure TDataFieldString.SetValue(const AValue: Variant);
begin
  SetNewValue<Variant>(AValue);
end;
{$ENDREGION}

{$REGION 'TDataFieldString'}
function TDataFieldChar.IsEqualValue(const AValue: Char): boolean;
begin
  Result := FValue = AValue;
end;

function TDataFieldChar.GetAsBoolean: Boolean;
begin
  Result := FValue <> #0;
end;

function TDataFieldChar.GetAsChar: Char;
begin
  Result := FValue;
end;

function TDataFieldChar.GetAsInt64: Int64;
begin
  Result := Ord(FValue);
end;

function TDataFieldChar.GetAsInteger: Integer;
begin
  Result := Ord(FValue);
end;

function TDataFieldChar.GetAsString: string;
begin
  Result := FValue;
end;

function TDataFieldChar.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TDataFieldChar.SetAsBoolean(const AValue: Boolean);
begin
  SetNewValue<boolean>(Char(AValue));
end;

procedure TDataFieldChar.SetAsChar(const AValue: Char);
begin
  SetNewValue<Char>(AValue);
end;

procedure TDataFieldChar.SetAsInt64(const AValue: Int64);
begin
  SetNewValue<Int64>(Char(AValue));
end;

procedure TDataFieldChar.SetAsInteger(const AValue: Integer);
begin
  SetNewValue<Integer>(Char(AValue));
end;

procedure TDataFieldChar.SetAsString(const AValue: string);
begin
  if Length(AValue) = 0 then
    SetNewValue<string>(#0)
  else
    SetNewValue<string>(AValue[1]);
end;

procedure TDataFieldChar.SetValue(const AValue: Variant);
begin
  SetAsString( VarToStr(AValue) );
end;
{$ENDREGION}

{$REGION 'TDataFieldExtended'}
function TDataFieldExtended.GetAsBoolean: Boolean;
begin
  Result := FValue <> 0.0;
end;

function TDataFieldExtended.GetAsDouble: Double;
begin
  Result := FValue;
end;

function TDataFieldExtended.GetAsExtended: Extended;
begin
  Result := FValue;
end;

function TDataFieldExtended.GetAsInt64: Int64;
begin
  Result := Trunc(FValue);
end;

function TDataFieldExtended.GetAsInteger: Integer;
begin
  Result := Trunc(FValue);
end;

function TDataFieldExtended.GetAsString: string;
begin
  Result := FloatToStr(FValue);
end;

function TDataFieldExtended.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TDataFieldExtended.SetAsDouble(const AValue: Double);
begin
  SetNewValue<Double>(AValue);
end;

procedure TDataFieldExtended.SetAsExtended(const AValue: Extended);
begin
  SetNewValue<Extended>(AValue);
end;

procedure TDataFieldExtended.SetAsInt64(const AValue: Int64);
begin
  SetNewValue<Int64>(AValue);
end;

procedure TDataFieldExtended.SetAsInteger(const AValue: Integer);
begin
  SetNewValue<Integer>(AValue);
end;

procedure TDataFieldExtended.SetAsString(const AValue: string);
begin
  SetNewValue<string>( StrToFloat(AValue) );
end;

procedure TDataFieldExtended.SetValue(const AValue: Variant);
begin
  SetNewValue<Variant>(AValue);
end;
{$ENDREGION}

{$REGION 'TDataFieldDouble'}
function TDataFieldDouble.GetAsBoolean: Boolean;
begin
  Result := FValue <> 0.0;
end;

function TDataFieldDouble.GetAsDouble: Double;
begin
  Result := FValue;
end;

function TDataFieldDouble.GetAsExtended: Extended;
begin
  Result := FValue;
end;

function TDataFieldDouble.GetAsInt64: Int64;
begin
  Result := Trunc(FValue);
end;

function TDataFieldDouble.GetAsInteger: Integer;
begin
  Result := Trunc(FValue);
end;

function TDataFieldDouble.GetAsString: string;
begin
  Result := FloatToStr(FValue);
end;

function TDataFieldDouble.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TDataFieldDouble.SetAsDouble(const AValue: Double);
begin
  SetNewValue<Double>(AValue);
end;

procedure TDataFieldDouble.SetAsExtended(const AValue: Extended);
begin
  SetNewValue<Extended>(AValue);
end;

procedure TDataFieldDouble.SetAsInt64(const AValue: Int64);
begin
  SetNewValue<Int64>(AValue);
end;

procedure TDataFieldDouble.SetAsInteger(const AValue: Integer);
begin
  SetNewValue<Integer>(AValue);
end;

procedure TDataFieldDouble.SetAsString(const AValue: string);
begin
  SetNewValue<string>( StrToFloat(AValue) );
end;

procedure TDataFieldDouble.SetValue(const AValue: Variant);
begin
  SetNewValue<Variant>(AValue);
end;
{$ENDREGION}

{$REGION 'TDataFieldDateTime'}
function TDataFieldDateTime.GetAsBoolean: Boolean;
begin
  Result := FValue <> 0.0;
end;

function TDataFieldDateTime.GetAsDouble: Double;
begin
  Result := FValue;
end;

function TDataFieldDateTime.GetAsExtended: Extended;
begin
  Result := FValue;
end;

function TDataFieldDateTime.GetAsInt64: Int64;
begin
  Result := Trunc(FValue);
end;

function TDataFieldDateTime.GetAsInteger: Integer;
begin
  Result := Trunc(FValue);
end;

function TDataFieldDateTime.GetAsString: string;
begin
  Result := DateTimeToStr(FValue);
end;

function TDataFieldDateTime.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TDataFieldDateTime.SetAsDouble(const AValue: Double);
begin
  SetNewValue<Double>(AValue);
end;

procedure TDataFieldDateTime.SetAsExtended(const AValue: Extended);
begin
  SetNewValue<Extended>(AValue);
end;

procedure TDataFieldDateTime.SetAsInt64(const AValue: Int64);
begin
  SetNewValue<Int64>(AValue);
end;

procedure TDataFieldDateTime.SetAsInteger(const AValue: Integer);
begin
  SetNewValue<Integer>(AValue);
end;

procedure TDataFieldDateTime.SetAsString(const AValue: string);
begin
  SetNewValue<string>( StrToDateTimeDef(AValue, 0) );
end;

procedure TDataFieldDateTime.SetValue(const AValue: Variant);
begin
  SetNewValue<Variant>(AValue);
end;
{$ENDREGION}

{$REGION 'TDataFieldDate'}
function TDataFieldDate.GetAsBoolean: Boolean;
begin
  Result := FValue <> 0.0;
end;

function TDataFieldDate.GetAsDouble: Double;
begin
  Result := FValue;
end;

function TDataFieldDate.GetAsExtended: Extended;
begin
  Result := FValue;
end;

function TDataFieldDate.GetAsInt64: Int64;
begin
  Result := Trunc(FValue);
end;

function TDataFieldDate.GetAsInteger: Integer;
begin
  Result := Trunc(FValue);
end;

function TDataFieldDate.GetAsString: string;
begin
  Result := DateToStr(FValue);
end;

function TDataFieldDate.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TDataFieldDate.SetAsDouble(const AValue: Double);
begin
  SetNewValue<Double>(AValue);
end;

procedure TDataFieldDate.SetAsExtended(const AValue: Extended);
begin
  SetNewValue<Extended>(AValue);
end;

procedure TDataFieldDate.SetAsInt64(const AValue: Int64);
begin
  SetNewValue<Int64>(AValue);
end;

procedure TDataFieldDate.SetAsInteger(const AValue: Integer);
begin
  SetNewValue<Integer>(AValue);
end;

procedure TDataFieldDate.SetAsString(const AValue: string);
begin
  SetNewValue<string>( StrToDateDef(AValue, 0) );
end;

procedure TDataFieldDate.SetValue(const AValue: Variant);
begin
  SetNewValue<Variant>(AValue);
end;
{$ENDREGION}

{$REGION 'TDataFieldTime'}
function TDataFieldTime.GetAsBoolean: Boolean;
begin
  Result := FValue <> 0.0;
end;

function TDataFieldTime.GetAsDouble: Double;
begin
  Result := FValue;
end;

function TDataFieldTime.GetAsExtended: Extended;
begin
  Result := FValue;
end;

function TDataFieldTime.GetAsInt64: Int64;
begin
  Result := Trunc(FValue);
end;

function TDataFieldTime.GetAsInteger: Integer;
begin
  Result := Trunc(FValue);
end;

function TDataFieldTime.GetAsString: string;
begin
  Result := TimeToStr(FValue);
end;

function TDataFieldTime.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TDataFieldTime.SetAsDouble(const AValue: Double);
begin
  SetNewValue<Double>(AValue);
end;

procedure TDataFieldTime.SetAsExtended(const AValue: Extended);
begin
  SetNewValue<Extended>(AValue);
end;

procedure TDataFieldTime.SetAsInt64(const AValue: Int64);
begin
  SetNewValue<Int64>(AValue);
end;

procedure TDataFieldTime.SetAsInteger(const AValue: Integer);
begin
  SetNewValue<Integer>(AValue);
end;

procedure TDataFieldTime.SetAsString(const AValue: string);
begin
  SetNewValue<string>( StrToTimeDef(AValue, 0) );
end;

procedure TDataFieldTime.SetValue(const AValue: Variant);
begin
  SetNewValue<Variant>(AValue);
end;
{$ENDREGION}

{$REGION 'TDataFieldBoolean'}
function TDataFieldBoolean.GetAsBoolean: Boolean;
begin
  Result := FValue;
end;

function TDataFieldBoolean.GetAsDouble: Double;
begin
  Result := Ord(FValue);
end;

function TDataFieldBoolean.GetAsExtended: Extended;
begin
  Result := Ord(FValue);
end;

function TDataFieldBoolean.GetAsInt64: Int64;
begin
  Result := Ord(FValue);
end;

function TDataFieldBoolean.GetAsInteger: Integer;
begin
  Result := Ord(FValue);
end;

function TDataFieldBoolean.GetAsString: string;
begin
  Result := BoolToStr(FValue);
end;

function TDataFieldBoolean.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TDataFieldBoolean.SetAsBoolean(const AValue: Boolean);
begin
  SetNewValue<Boolean>(AValue);
end;

procedure TDataFieldBoolean.SetAsInt64(const AValue: Int64);
begin
  SetNewValue<integer>(AValue <> 0);
end;

procedure TDataFieldBoolean.SetAsInteger(const AValue: Integer);
begin
  SetNewValue<integer>(AValue <> 0);
end;

procedure TDataFieldBoolean.SetAsString(const AValue: string);
begin
  SetNewValue<string>( StrToBool(AValue) );
end;

procedure TDataFieldBoolean.SetValue(const AValue: Variant);
begin
  SetNewValue<Variant>(AValue);
end;
{$ENDREGION}

{$REGION 'TDataFieldGUID'}
function TDataFieldGUID.GetAsGUID: TGUID;
begin
  Result := FValue;
end;

function TDataFieldGUID.GetAsString: string;
begin
  Result := GUIDToString(FValue);
  Result := Copy(Result, 2, Length(Result) - 2);
end;

function TDataFieldGUID.GetValue: Variant;
begin
  Result := GetAsString;
end;

procedure TDataFieldGUID.SetAsGUID(const AValue: TGUID);
begin
  SetNewValue<TGUID>(AValue);
end;

procedure TDataFieldGUID.SetAsString(const AValue: string);
begin
  SetNewValue<string>( Api.StringToGUIDEx(AValue) );
end;

procedure TDataFieldGUID.SetValue(const AValue: Variant);
begin
  SetNewValue<Variant>( StringToGUID( VarToStr(AValue) ) );
end;
{$ENDREGION}

{$REGION 'TDataFieldNode<TMeta,T>'}
destructor TDataFieldNode<TMeta, T>.Destroy;
begin
  FreeAndNil(FValue);
  inherited;
end;

function TDataFieldNode<TMeta,T>.DoFindChildNode(const ANodeType : TEntitiesNodeType;
  const AName: string; out ADataNode: TDataNode): boolean;
var
  Node : TDataNode;
begin
  Node := Self.AsNode;
  if (ANodeType = enntField) and (Node.NodeType = enntEntity) then
    Result := Node.DoFindChildNode(ANodeType, AName, ADataNode)

  else
  begin
    ADataNode := Node;
    Result := True;
  end;
end;

function TDataFieldNode<TMeta,T>.GetAsNode: TDataNode;
begin
  Result := TDataNode(FValue);
end;

function TDataFieldNode<TMeta,T>.GetAsString: string;
begin
  Result := TDataNode(FValue).Name;
end;

procedure TDataFieldNode<TMeta,T>.OwnedDestroing(const AOwned: TDataObject);
begin
  inherited;
  FValue := nil;
end;

function TDataFieldNode<TMeta, T>.DoExportToJSON(const ACompact : boolean): TJSONValue;
begin
  Result := AsNode.ExportToJSON(ACompact);
end;

procedure TDataFieldNode<TMeta, T>.DoImportFromJSON(const AJSON: TJSONValue; const AReWrite : boolean);
begin
  AsNode.ImportFromJSON(AJSON, AReWrite);
end;

procedure TDataFieldNode<TMeta, T>.Refresh;
begin
  inherited;
  AsNode.Refresh();
end;
{$ENDREGION}

{$REGION 'TDataFieldEntity'}
function TDataFieldEntity.GetAsEntity: TDataEntity;
begin
  if not Assigned(FValue) then
    Meta.Entity.MakeDataRaw(Self, FValue);

  Result := FValue;
end;

function TDataFieldEntity.GetAsNode: TDataNode;
begin
  Result := GetAsEntity();
end;

{$ENDREGION}

{$REGION 'TDataFieldCollection'}
function TDataFieldCollection.GetAsItems: TDataCollection;
begin
  if not Assigned(FValue) then
    Meta.Collection.MakeDataRaw(Self, FValue);

  Result := FValue;
end;

function TDataFieldCollection.GetAsNode: TDataNode;
begin
  Result := GetAsItems();
end;
{$ENDREGION}

{$REGION 'TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternal'}
constructor TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternal.Create(
  const AOwner : TDataNodeEntityOrCollection);
begin
  inherited Create();
end;

procedure TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternal.ClearBeforeFree(
  const AOwner : TDataNodeEntityOrCollection);
begin
end;

procedure TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternal.InternalDataCreated(
  const AOwner: TDataNodeEntityOrCollection);
begin
end;

function TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternal.GetTarget(
  const AOwner: TDataNodeEntityOrCollection): TDataNodeEntityOrCollection;
begin
  Result := AOwner;
end;

function TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternal.DoExportToJSON(
  const AOwner : TDataNodeEntityOrCollection; const ACompact : boolean) : TJSONValue;
begin
  Result := nil;
end;

procedure TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternal.DoImportFromJSON(
  const AOwner: TDataNodeEntityOrCollection; const AJSON: TJSONValue; const AReWrite : boolean);
begin
end;

class function TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternal.GetLinkTarget(
  const AOwner: TDataNodeEntityOrCollection): TDataNodeEntityOrCollection;
var
  ResolvedLink : TPathResolver.TPathItem;
  Node : TDataNode;
begin
  ResolvedLink := (AOwner.Meta.InternalData as TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalCustomLink).ResolvedLink;

  Node   := ResolvedLink.FindEndTargetData(AOwner.NodeType, AOwner, AOwner.Parent);
  Result := Node as TDataNodeEntityOrCollection;
end;

function TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternal.QueryInterface(
  const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := S_OK
  else
    Result := E_NOINTERFACE;
end;

function TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternal._AddRef: Integer;
begin
  Result := -1;
end;

function TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternal._Release: Integer;
begin
  Result := -1;
end;
{$ENDREGION}

{$REGION 'TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalLink'}
procedure TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalLink.DetachLink;
begin
end;

function TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalLink.GetTarget(
  const AOwner: TDataNodeEntityOrCollection): TDataNodeEntityOrCollection;
begin
  Result := GetLinkTarget(AOwner);
end;
{$ENDREGION}

{$REGION 'TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalData'}
procedure TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalData.ClearBeforeFree(
  const AOwner : TDataNodeEntityOrCollection);
begin
  ClearObjsArray(FEvents);
  inherited;
end;

procedure TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalData.AddEvent(
  const AEvent: TEntitiesEvent);
begin
  System.Insert(AEvent, FEvents, Length(FEvents));
end;
{$ENDREGION}

{$REGION 'TDataNodeEntityOrCollection'}
constructor TDataNodeEntityOrCollection.InternalCreate(
  const AContext: TDataObject.TCreateContext);
begin
  inherited;

  if not Assigned(Meta.InternalData) then
    raise EEntitiesError.CreateResFmt(@s_San_Entities_MetaInternalDataMustBeInitializedBeforeDataCreatingFmt, [Meta.Path]);

  FLockerData := TMultithreadLockerFacade.TMultithreadLockerData.Create();

  if Meta.IsLink then
    FInternalData := GetInternalDataLinkClass.Create(Self)
  else
    FInternalData := GetInternalDataClass.Create(Self);

  FInternalData.InternalDataCreated(Self);
end;

procedure TDataNodeEntityOrCollection.LockerDetachCurrentThread;
begin
  FLockerData.DetachCurrentThread();
end;

destructor TDataNodeEntityOrCollection.Destroy;
begin
  if Assigned(FInternalData) then
  begin
    FInternalData.ClearBeforeFree(Self);
    FreeAndNil(FInternalData);
  end;

  FreeAndNil(FLockerData);

  inherited;
end;

function TDataNodeEntityOrCollection.GetMetaEvent: TMEEntities;
begin
  Result := Root.Meta.MetaEvent;
end;

function TDataNodeEntityOrCollection.GetInternalDataLinkClass: TDataNodeEntityOrCollectionInternalClass;
begin
  Result := TDataNodeEntityOrCollectionInternalLink;
end;

function TDataNodeEntityOrCollection.GetIsLink: boolean;
begin
  Result := Meta.IsLink;
end;

function TDataNodeEntityOrCollection.GetLink: string;
begin
  Result := Meta.Link;
end;

function TDataNodeEntityOrCollection.GetTarget: TDataNodeEntityOrCollection;
begin
  if not Assigned(FInternalData) then
    Exit(nil);

  try

    Result := FInternalData.GetTarget(Self);

  except
    EEntities.CreateFmt('Self: %p, FInternalData: %p, Assigned(FInternalData): %s', [Pointer(Self), Pointer(FInternalData), BoolToStr(Assigned(FInternalData), True)]).RaiseOuter(Result);
  end;

  while Assigned(Result) and Result.IsLink do
    Result := Result.Target;
end;

function TDataNodeEntityOrCollection.GetLockerData: TMultithreadLockerFacade.TMultithreadLockerData;
begin
  Result := FLockerData;
end;

function TDataNodeEntityOrCollection.HasEventSubscribers: boolean;
var
  ME : TMEEntities;
begin
  if IsLink then
    Exit(False);

  ME := Root.Meta.FMEEntities;

  Result := Assigned(ME) and (ME.Handlers.Count > 0);
end;

procedure TDataNodeEntityOrCollection.DoFireEvent(const AEvent: TEntitiesEvent);
begin
  DoFireEvents([AEvent]);
end;

procedure TDataNodeEntityOrCollection.DoUpdate;
var
  InData : TDataNodeEntityOrCollectionInternalData;
begin
  inherited;

  if not (FInternalData is TDataNodeEntityOrCollectionInternalData) then
    Exit;

  InData := FInternalData as TDataNodeEntityOrCollectionInternalData;
  if Length(InData.FEvents) > 0 then
    DoFireEvents(nil);
end;

procedure TDataNodeEntityOrCollection.DoFireEvents(
  const AEvents: TArray<TEntitiesEvent>);
var
  InData : TDataNodeEntityOrCollectionInternalData;
  Immediately : boolean;
  Parent : TDataEntity;
begin
  InData := FInternalData as TDataNodeEntityOrCollectionInternalData;

  if Assigned(AEvents) then
    System.Insert(AEvents, InData.FEvents, Length(InData.FEvents));

  Immediately := not Assigned(FUpdates) or (FUpdates.UpdatesCount = 0);
  if not Immediately then
  begin
    FUpdates.Update(True);
    Exit;
  end;

  if Assigned(Root.Meta.FMEEntities) then
    Root.Meta.FMEEntities.Fire(Self, InData.FEvents).Std();

  Parent := Self.Parent;
  if Assigned(Parent) then
  begin
    Parent.DoFireEvents(InData.FEvents);
    InData.FEvents := nil;
  end
  else
    ClearObjsArray(InData.FEvents);
end;

function TDataNodeEntityOrCollection.DoExportToJSON(const ACompact : boolean) : TJSONValue;
begin
  Result := InternalData.DoExportToJSON(Self, ACompact);
end;

procedure TDataNodeEntityOrCollection.DoImportFromJSON(
  const AJSON: TJSONValue; const AReWrite : boolean);
begin
  inherited;
  InternalData.DoImportFromJSON(Self, AJSON, AReWrite);
end;
{$ENDREGION}

{$REGION 'TDataNodeEntityOrCollection<TMeta, TOwner>'}
class function TDataNodeEntityOrCollection<TMeta, TOwner>.GetMetaClass: TMetaNodeClass;
begin
  Result := TMetaNodeClass(TMeta);
end;

class function TDataNodeEntityOrCollection<TMeta, TOwner>.GetDataOwnerClass: TDataObjectClass;
begin
  Result := TDataObjectClass(TOwner);
end;

function TDataNodeEntityOrCollection<TMeta, TOwner>.GetMeta: TMeta;
begin
  Result := TMeta(GetInitialMeta())
end;

function TDataNodeEntityOrCollection<TMeta, TOwner>.GetOwner: TOwner;
begin
  Result := TOwner(GetInitialOwner())
end;
{$ENDREGION}

{$REGION 'TDataEntity.TDataEntityInternalData'}
procedure TDataEntity.TDataEntityInternalData.ClearBeforeFree(const AOwner : TDataNodeEntityOrCollection);
begin
  ClearObjsArray(FFields);
  inherited;
end;

function TDataEntity.TDataEntityInternalData.TryFindField(
  const AMetaField: TMetaField; out ADataField: TDataField;
  const AIndexPtr: PInteger): boolean;
var
  i : integer;
begin
  if (Length(FFields) = 0) or (FFields[0].Meta.Owner <> AMetaField.Owner) then
  begin
    ADataField := nil;
    Exit(False);
  end;

  for i := 0 to High(FFields) do
    if FFields[i].Meta = AMetaField then
    begin
      ADataField := FFields[i];

      if Assigned(AIndexPtr) then
        AIndexPtr^ := i;

      Exit(True);
    end;

  ADataField := nil;

  if Assigned(AIndexPtr) then
    AIndexPtr^ := -1;

  Result := False;
end;

function TDataEntity.TDataEntityInternalData.ExistsField(
  const AMetaField: TMetaField): boolean;
var
  Dummy : TDataField;
begin
  Result := TryFindField(AMetaField, Dummy);
end;

function TDataEntity.TDataEntityInternalData.TryFindFieldByName(
  const AName: string; out ADataField: TDataField;
  const AIndexPtr: PInteger): boolean;
var
  i : integer;
begin

  for i := 0 to High(FFields) do
    if AnsiSameStr(FFields[i].Name, AName) then
    begin
      ADataField := FFields[i];

      if Assigned(AIndexPtr) then
        AIndexPtr^ := i;

      Exit(True);
    end;

  ADataField := nil;

  if Assigned(AIndexPtr) then
    AIndexPtr^ := -1;

  Result := False;
end;

function TDataEntity.TDataEntityInternalData.ExistsFieldByName(
  const AName: string): boolean;
var
  Dummy : TDataField;
begin
  Result := TryFindFieldByName(AName, Dummy);
end;

procedure TDataEntity.TDataEntityInternalData.ExtractField(
  const AField: TDataField);
var
  i : integer;
begin
  for i := 0 to High(FFields) do
    if FFields[i] = AField then
    begin
      System.Delete(FFields, i, 1);
      Exit;
    end;
end;

procedure TDataEntity.TDataEntityInternalData.CloneFields(const ASource,
  ADest: TDataEntity; const ACheckExists: Boolean);
var
  SourceField, ClonedField : TDataField;
begin
  for SourceField in (ASource.InternalData as TDataEntityInternalData).FFields do
  begin
    SourceField.Clone(ADest, ClonedField);
    AddField(ClonedField);
  end;
end;

constructor TDataEntity.TDataEntityInternalData.Create(
  const AOwner : TDataNodeEntityOrCollection);
var
  MetaFields : TMetaFieldsFacade;
  Owner : TDataEntity;
  i : integer;
  DataField : TDataField;
begin
  inherited;

  Owner := AOwner as TDataEntity;
  MetaFields := Owner.Meta.Fields;
  SetLength(FFields, MetaFields.Count);

  for i := 0 to MetaFields.Count - 1 do
  begin
    MetaFields[i].MakeDataRaw(Owner, DataField);
    try
      FFields[i] := DataField;
    except
      FreeAndNil(DataField);
      raise;
    end;
  end;

end;

procedure TDataEntity.TDataEntityInternalData.AddField(const ADataField: TDataField);
begin
  System.Insert(ADataField, FFields, Length(FFields));
end;

procedure TDataEntity.TDataEntityInternalData.DoExportToJSONObject(
  const AOwner: TDataEntity; const AJSON: TJSONObject; const ACompact : boolean);
var
  i : integer;
begin
  for i := 0 to High(FFields) do
    AJSON.AddPair(FFields[i].Name, FFields[i].ExportToJSON(ACompact));
end;

function TDataEntity.TDataEntityInternalData.DoExportToJSON(const AOwner : TDataNodeEntityOrCollection;
  const ACompact : boolean) : TJSONValue;
var
  DataEntity : TDataEntity;
  JObj : TJSONObject;
begin
  DataEntity := AOwner as TDataEntity;

  JObj := TJSONObject.Create();
  Result := JObj;
  try
    DataEntity.DoBeforeExportToJSON(JObj, ACompact);
    DoExportToJSONObject(DataEntity, JObj, ACompact);
    DataEntity.DoAfterExportToJSON(JObj, ACompact);
  except
    FreeAndNil(Result);
    raise;
  end;

  if ACompact and (JObj.Count = 0) then
    FreeAndNil(Result);
end;

procedure TDataEntity.TDataEntityInternalData.DoImportFromJSON(
  const AOwner: TDataNodeEntityOrCollection; const AJSON: TJSONValue;
  const AReWrite : boolean);
var
  JObj : TJSONObject;
  JPair : TJSONPair;
  i : integer;
  Fields : TDataFieldsFacade;
  Field : TDataField;
begin
  JObj := AJSON as TJSONObject;

  Fields := (AOwner as TDataEntity).Fields;

  for i := 0 to JObj.Count - 1 do
  begin
    JPair := JObj.Pairs[i];
    if Fields.TryFindByName(JPair.JsonString.Value, Field) then
      Field.ImportFromJSON(JPair.JsonValue, AReWrite);
  end;
end;
{$ENDREGION}

{$REGION 'TDataEntity.TDataFieldsFacade'}
constructor TDataEntity.TDataFieldsFacade.Create(const ATarget: TDataEntity);
begin
  FTarget := ATarget;
end;

function TDataEntity.TDataFieldsFacade.GetInternalData: TDataEntityInternalData;
var
  Target : TDataEntity;
begin
  if Assigned(Self.FTarget) then
  begin
    Target := Self.FTarget.Target;

    if Assigned(Target) then
      Exit(Target.InternalData as TDataEntityInternalData);
  end;

  Result := nil;
end;

function TDataEntity.TDataFieldsFacade.GetCount: integer;
var
  InternalData : TDataEntityInternalData;
begin
  InternalData := Self.InternalData;
  if Assigned(InternalData) then
    Result := Length(InternalData.FFields)
  else
    Result := 0;
end;

function TDataEntity.TDataFieldsFacade.GetEnumerator: TArrayEnumerator<TDataField>;
var
  InternalData : TDataEntityInternalData;
begin
  InternalData := Self.InternalData;
  if Assigned(InternalData) then
    Result := TArrayEnumerator<TDataField>.Create(InternalData.FFields)
  else
    Result := TArrayEnumerator<TDataField>.Create(nil);
end;

function TDataEntity.TDataFieldsFacade.TryFind(
  const AMetaField: TMetaField; out ADataField: TDataField;
  const AIndexPtr: PInteger): boolean;
var
  InternalData : TDataEntityInternalData;
begin
  InternalData := Self.InternalData;
  Result := Assigned(InternalData)
        and InternalData.TryFindField(AMetaField, ADataField, AIndexPtr);

  if not Result then
    ADataField := nil;
end;

function TDataEntity.TDataFieldsFacade.Exists(
  const AMetaField: TMetaField): boolean;
var
  Dummy : TDataField;
begin
  Result := TryFind(AMetaField, Dummy);
end;

function TDataEntity.TDataFieldsFacade.TryFindByName(const AName: string;
  out ADataField: TDataField; const AIndexPtr: PInteger): boolean;
begin
  Result := InternalData.TryFindFieldByName(AName, ADataField, AIndexPtr);
end;

function TDataEntity.TDataFieldsFacade.ExistsByName(
  const AName: string): boolean;
var
  Dummy : TDataField;
begin
  Result := TryFindByName(AName, Dummy);
end;

function TDataEntity.TDataFieldsFacade.GetByIndex(
  const AIndex: integer): TDataField;
var
  InternalData : TDataEntityInternalData;
begin
  InternalData := Self.InternalData;

  if (AIndex < 0) or (AIndex > High(InternalData.FFields)) then
    raise EEntitiesError.CreateFmt(SListIndexError, [AIndex]);

  Result := InternalData.FFields[AIndex];
end;

function TDataEntity.TDataFieldsFacade.GetByMetaField(
  const AMetaField: TMetaField): TDataField;
begin
  if not TryFind(AMetaField, Result) then
    raise EEntitiesError.CreateResFmt(@s_San_Entities_DataFieldNotFoundFmt, [AMetaField.Name, FTarget.Path]);
end;

function TDataEntity.TDataFieldsFacade.GetByName(
  const AName: string): TDataField;
begin
  if not TryFindByName(AName, Result) then
    raise EEntitiesError.CreateResFmt(@s_San_Entities_DataFieldNotFoundFmt, [AName, FTarget.Path]);
end;

function TDataEntity.TDataFieldsFacade.Find(const AMetaField: TMetaField;
  const AIndexPtr: PInteger): TDataField;
begin
  if not TryFind(AMetaField, Result, AIndexPtr) then
    raise EEntitiesError.CreateResFmt(@s_San_Entities_DataFieldNotFoundFmt, [AMetaField.Name, FTarget.Path]);
end;

function TDataEntity.TDataFieldsFacade.FindByName(const AName: string;
  const AIndexPtr: PInteger): TDataField;
begin
  if not TryFindByName(AName, Result, AIndexPtr) then
    raise EEntitiesError.CreateResFmt(@s_San_Entities_DataFieldNotFoundFmt, [AName, FTarget.Path]);
end;
{$ENDREGION}

{$REGION 'TDataEntity'}
function TDataEntity.AccessForIndexPtr(out AIndexPtr: PInteger): boolean;
begin
  Result := Assigned(InternalData) and Assigned(Owner) and (Owner.Meta.NodeType = enntCollection);

  if not Result then
    AIndexPtr := nil
  else
  if InternalData is TDataEntityInternalDataWithIndex then
    AIndexPtr := @TDataEntityInternalDataWithIndex(InternalData).FIndex
  else
  {$IFDEF DEBUG}
  if InternalData is TDataEntityInternalLinkWithIndex then
  {$ENDIF}
    AIndexPtr := @TDataEntityInternalLinkWithIndex(InternalData).FIndex
  {$IFDEF DEBUG}
  else
    San.Api.Excps.ItsImpossible();
  {$ENDIF}
end;

function TDataEntity.GetIndex: Integer;
var
  pIndex : PInteger;
begin
  if AccessForIndexPtr(pIndex) then
    Result := pIndex^
  else
    Result := -1
end;

function TDataEntity.GetFields: TDataFieldsFacade;
begin
  Result := TDataFieldsFacade.Create(Self);
end;

function TDataEntity.GetInternalDataClass: TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalClass;
begin
  if Meta.Owner is TMetaCollection then
    Result := TDataEntityInternalDataWithIndex
  else
    Result := TDataEntityInternalData;
end;

function TDataEntity.GetInternalDataLinkClass: TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalClass;
begin
  if Meta.Owner is TMetaCollection then
    Result := TDataEntityInternalLinkWithIndex
  else
    Result := TDataNodeEntityOrCollectionInternalLink;
end;

function TDataEntity.GetTarget: TDataEntity;
begin
  Result := TDataEntity(inherited Target);
end;

procedure TDataEntity.OwnedDestroing(const AOwned: TDataObject);
begin
  inherited;

  Assert(AOwned is TDataField);
  Assert(not IsLink);
  (InternalData as TDataEntityInternalData).ExtractField(TDataField(AOwned));
end;

destructor TDataEntity.Destroy;
var
  pIndex : PInteger;
begin
  if AccessForIndexPtr(pIndex) then
    TDataCollection(Owner).DecNextIndexes(pIndex^);

  inherited;
end;

procedure TDataEntity.DoBeforeExportToJSON(const AJSON: TJSONObject; const ACompact : boolean);
begin
end;

procedure TDataEntity.DoAfterExportToJSON(const AJSON: TJSONObject; const ACompact : boolean);
begin
end;

function TDataEntity.DoFindChildNode(const ANodeType : TEntitiesNodeType;
  const AName: string; out ADataNode: TDataNode): boolean;
var
  Field : TDataField;
begin
  Result := Fields.TryFindByName(AName, Field);
  if Result then
    ADataNode := Field
  else
    ADataNode := nil;

  if Result and (ANodeType <> enntField) then
    Result := ADataNode.DoFindChildNode(ANodeType, AName, ADataNode);
end;

procedure TDataEntity.Refresh;
var
  Fields : TDataFieldsFacade;
  Field : TDataField;
begin
  inherited;

  Fields := Self.Fields;
  for Field in Fields do
  try
    Field.Refresh();
  except
    EEntitiesError.CreateResFmt(@s_San_Entities_DataFieldRefreshInternalErrorFmt, [Field.Path]).RaiseOuter;
  end;

end;

function TDataEntity.PIG<T>(const AIndex: integer): T;
var
  F : TDataField;
begin
  try
    F := Fields[AIndex];
  except
    EEntitiesError.CreateResFmt(@s_San_Entities_InternalPropIdxGetErrorForFieldFmt, [AIndex, Path]).RaiseOuter(F);
  end;

  try
    Result := F.AsType<T>;
  except
    EEntitiesError.CreateResFmt(@s_San_Entities_InternalPropIdxGetErrorForFieldFmt, [AIndex, F.Path]).RaiseOuter(Result);
  end;
end;

procedure TDataEntity.PIS<T>(const AIndex: integer; const AValue: T);
var
  F : TDataField;
begin
  try
    F := Fields[AIndex];
  except
    EEntitiesError.CreateResFmt(@s_San_Entities_InternalPropIdxSetErrorForFieldFmt, [AIndex, Path]).RaiseOuter(F);
  end;

  try
    F.From<T>(AValue);
  except
    EEntitiesError.CreateResFmt(@s_San_Entities_InternalPropIdxSetErrorForFieldFmt, [AIndex, F.Path]).RaiseOuter();
  end;
end;

function TDataEntity.PIG_Integer(const AIndex: integer): integer;
begin
  Result := {$IFDEF SE_PI_FAST}Fields[AIndex].AsInteger{$ELSE}PIG<Integer>(AIndex){$ENDIF};
end;

procedure TDataEntity.PIS_Integer(const AIndex, AValue: integer);
begin
  {$IFDEF SE_PI_FAST}
    Fields[AIndex].AsInteger := AValue;
  {$ELSE}
    PIS<Integer>(AIndex, AValue);
  {$ENDIF}
end;

function TDataEntity.PIG_Int64(const AIndex: integer): Int64;
begin
  Result := {$IFDEF SE_PI_FAST}Fields[AIndex].AsInt64{$ELSE}PIG<Int64>(AIndex){$ENDIF};
end;

procedure TDataEntity.PIS_Int64(const AIndex: integer;
  const AValue: Int64);
begin
  {$IFDEF SE_PI_FAST}
    Fields[AIndex].AsInt64 := AValue;
  {$ELSE}
    PIS<Int64>(AIndex, AValue);
  {$ENDIF}
end;

function TDataEntity.PIG_String(const AIndex: integer): String;
begin
  Result := {$IFDEF SE_PI_FAST}Fields[AIndex].AsString{$ELSE}PIG<String>(AIndex){$ENDIF};
end;

procedure TDataEntity.PIS_String(const AIndex: integer;
  const AValue: String);
begin
  {$IFDEF SE_PI_FAST}
    Fields[AIndex].AsString := AValue;
  {$ELSE}
    PIS<String>(AIndex, AValue);
  {$ENDIF}
end;

function TDataEntity.PIG_Char(const AIndex: integer): Char;
begin
  Result := {$IFDEF SE_PI_FAST}Fields[AIndex].AsChar{$ELSE}PIG<Char>(AIndex){$ENDIF};
end;

procedure TDataEntity.PIS_Char(const AIndex: integer;
  const AValue: Char);
begin
  {$IFDEF SE_PI_FAST}
    Fields[AIndex].AsChar := AValue;
  {$ELSE}
    PIS<Char>(AIndex, AValue);
  {$ENDIF}
end;

function TDataEntity.PIG_Extended(const AIndex: integer): Extended;
begin
  Result := {$IFDEF SE_PI_FAST}Fields[AIndex].AsExtended{$ELSE}PIG<Extended>(AIndex){$ENDIF};
end;

procedure TDataEntity.PIS_Extended(const AIndex: integer;
  const AValue: Extended);
begin
  {$IFDEF SE_PI_FAST}
    Fields[AIndex].AsExtended := AValue;
  {$ELSE}
    PIS<Extended>(AIndex, AValue);
  {$ENDIF}
end;

function TDataEntity.PIG_Double(const AIndex: integer): Double;
begin
  Result := {$IFDEF SE_PI_FAST}Fields[AIndex].AsDouble{$ELSE}PIG<Double>(AIndex){$ENDIF};
end;

procedure TDataEntity.PIS_Double(const AIndex: integer;
  const AValue: Double);
begin
  {$IFDEF SE_PI_FAST}
    Fields[AIndex].AsDouble := AValue;
  {$ELSE}
    PIS<Double>(AIndex, AValue);
  {$ENDIF}
end;

function TDataEntity.PIG_DateTime(const AIndex: integer): TDateTime;
begin
  Result := {$IFDEF SE_PI_FAST}Fields[AIndex].AsDouble{$ELSE}PIG<TDateTime>(AIndex){$ENDIF};
end;

procedure TDataEntity.PIS_DateTime(const AIndex: integer;
  const AValue: TDateTime);
begin
  {$IFDEF SE_PI_FAST}
    Fields[AIndex].AsDouble := AValue;
  {$ELSE}
    PIS<TDateTime>(AIndex, AValue);
  {$ENDIF}
end;

function TDataEntity.PIG_Boolean(const AIndex: integer): Boolean;
begin
  Result := {$IFDEF SE_PI_FAST}Fields[AIndex].AsBoolean{$ELSE}PIG<Boolean>(AIndex){$ENDIF};
end;

procedure TDataEntity.PIS_Boolean(const AIndex: integer;
  const AValue: Boolean);
begin
  {$IFDEF SE_PI_FAST}
    Fields[AIndex].AsBoolean := AValue;
  {$ELSE}
    PIS<Boolean>(AIndex, AValue);
  {$ENDIF}
end;

function TDataEntity.PIG_TGUID(const AIndex: integer): TGUID;
begin
  Result := {$IFDEF SE_PI_FAST}Fields[AIndex].AsGUID{$ELSE}PIG<TGUID>(AIndex){$ENDIF};
end;

procedure TDataEntity.PIS_TGUID(const AIndex: integer;
  const AValue: TGUID);
begin
  {$IFDEF SE_PI_FAST}
    Fields[AIndex].AsGUID := AValue;
  {$ELSE}
    PIS<TGUID>(AIndex, AValue);
  {$ENDIF}
end;

function TDataEntity.PIG_Entity<T>(const AIndex: integer): T;
begin
  Result := {$IFDEF SE_PI_FAST}San.Api.Objs.Cast<T>(Fields[AIndex].AsEntity){$ELSE}PIG<T>(AIndex){$ENDIF};
end;

function TDataEntity.PIG_EntityTarget<T>(const AIndex: integer): T;
var
  E : TDataEntity;
begin
  E := {$IFDEF SE_PI_FAST}Fields[AIndex].AsEntity{$ELSE}PIG<TDataEntity>(AIndex){$ENDIF};
  try
    Result := San.Api.Objs.Cast<T>(E.Target, True);
  except
    EEntitiesError.CreateResFmt(@s_San_Entities_InternalPropIdxSetErrorForFieldFmt, [AIndex, E.Path]).RaiseOuter();
  end;
end;

function TDataEntity.PIG_Collection<T>(const AIndex: integer): T;
begin
  Result := {$IFDEF SE_PI_FAST}San.Api.Objs.Cast<T>(Fields[AIndex].AsItems){$ELSE}PIG<T>(AIndex){$ENDIF};
end;
{$ENDREGION}

{$REGION 'TDataFinderWithEnumeration<T>'}
constructor TDataFinderWithEnumeration<T>.Create(const ATarget : TDataNode;
  const AEnumerator : TEnumeratorRef<T>; const APredicate: TPredicate<T>);
begin
  San.Api.Objs.ValidateGen<T, TDataEntity>();
  FTarget     := ATarget;
  FEnumerator := AEnumerator;
  FPredicate  := APredicate;
end;

procedure TDataFinderWithEnumeration<T>.ItemsNotFoundError;
begin
  if not Assigned(OnError) then
    raise EEntitiesFinderError.CreateResFmt(@s_San_Entities_ItemsNotFoundFmt, [FTarget.Path])

  else
  try
    raise EEntitiesFinderError.CreateResFmt(@s_San_Entities_ItemsNotFoundFmt, [FTarget.Path])
  except
    on E : Exception do
      OnError(E);
  end;
end;

procedure TDataFinderWithEnumeration<T>.SetOnError(
  const AValue: TProc<Exception>);
var
  OldValue : TProc<Exception>;
begin
  if @FOnError = @AValue then
    Exit;

  OldValue := FOnError;
  if not Assigned(OldValue) then
    FOnError := AValue

  else
    FOnError := procedure (AError : Exception)
      begin
        try

          OldValue(AError);

        except
          on E : Exception do
            AValue(E);
        end;
      end;
end;

function TDataFinderWithEnumeration<T>.TryFindRaw(out AResults;
  const ASeparatePassForAllocation: boolean): boolean;
var
  Results : TArray<T>;
begin
  Result := TryFind(Results, ASeparatePassForAllocation);
  TArray<T>(AResults) := Results;
end;

function TDataFinderWithEnumeration<T>.TryFind(out AResults: TArray<T>;
  const ASeparatePassForAllocation: boolean): boolean;
var
  j : integer;
  Item : T;
begin
  Result := False;

  if not FEnumerator(Item, True) then
    Exit;

  j := 0;
  if ASeparatePassForAllocation then
  begin

    repeat
      if FPredicate(Item) then
        Inc(j);

    until not FEnumerator(Item, False);

    if j = 0 then
      Exit();

    SetLength(AResults, j);

    j := 0;
    FEnumerator(Item, True);
    repeat
      if not FPredicate(Item) then
        Continue;

      AResults[j] := Item;
      Inc(j);
    until (j = Length(AResults))
       or not FEnumerator(Item, False);
  end

  else
  repeat
    if FPredicate(Item) then
    begin
      System.Insert(Item, AResults, j);
      Inc(j);
    end;

  until not FEnumerator(Item, False);

  Result := j > 0;
end;

function TDataFinderWithEnumeration<T>.TryFindFirst(out AResult: T): boolean;
var
  Item : T;
begin
  if FEnumerator(Item, True) then
    repeat
      if not FPredicate(Item) then
        Continue;

      AResult := Item;
      Exit(True);
    until not FEnumerator(Item, False);

  AResult  := nil;
  Result   := False;
end;

function TDataFinderWithEnumeration<T>.Exists(AMinCount : integer): boolean;
var
  Item : T;
  c : integer;
begin
  if AMinCount < 1 then
    AMinCount := 1;

  c := 0;
  if FEnumerator(Item, True) then
    repeat
      if FPredicate(Item) then
      begin
        Inc(c);

        if c >= AMinCount then
          Exit(True);
      end;

    until not FEnumerator(Item, False);

  Result   := False;
end;

procedure TDataFinderWithEnumeration<T>.FindRaw(out AResults;
  const ASeparatePassForAllocation: boolean);
begin
  if not TryFindRaw(AResults, ASeparatePassForAllocation) then
    ItemsNotFoundError;
end;

function TDataFinderWithEnumeration<T>.Find(
  const ASeparatePassForAllocation: boolean): TArray<T>;
begin
  if not TryFind(Result, ASeparatePassForAllocation) then
    ItemsNotFoundError;
end;

function TDataFinderWithEnumeration<T>.FindFirst: T;
begin
  if not TryFindFirst(Result) then
    ItemsNotFoundError;
end;
{$ENDREGION}

{$REGION 'TDataCollectionIndex'}
function TDataCollectionIndex.GetIsUnique: boolean;
begin
  Result := Meta.IsUnique;
end;

constructor TDataCollectionIndex.InternalCreate(
  const AContext: TDataObject.TCreateContext);
begin
  inherited;

  FMeta   := AContext.Meta   as TMetaCollectionIndex;
  FOwner  := AContext.Owner  as TDataCollection;
end;
{$ENDREGION}

{$REGION 'TDataCollectionIndexSingle'}
function TDataCollectionIndexSingle.Finder(
  const AValue: TValue; const ACompareOperation : TEntitiesCompareOperation): TDataFinderWithEnumeration<TDataEntity>;
begin
  Result := GetFinder(AValue, ACompareOperation);
end;

function TDataCollectionIndexSingle.GetMeta: TMetaCollectionIndexSingle;
begin
  Result := (inherited Meta) as TMetaCollectionIndexSingle;
end;

procedure TDataCollectionIndexSingle.AddItemToIndex(const AItem: TDataEntity);
begin
  AddIndexItem(AItem.Fields[Meta.Field]);
end;

procedure TDataCollectionIndexSingle.RemoveItemFromIndex(
  const AItem: TDataEntity);
var
  MetaField : TMetaField;
  DataField : TDataField;
begin
  MetaField := Meta.Field;
  if AItem.Fields.TryFind(MetaField, DataField) then
    RemoveIndexItem(DataField);
end;
{$ENDREGION}

{$REGION 'TDataCollectionIndexSingle<T>'}
constructor TDataCollectionIndexSingle<T>.InternalCreate(
  const AContext: TDataObject.TCreateContext);
begin
  inherited;

  FIndexDict := TObjectDictionary<T, TObject>.Create;
end;

procedure TDataCollectionIndexSingle<T>.Clear;
var
  O : TObject;
begin
  if not IsUnique then
    for O in FIndexDict.Values do
      O.Free;

  FIndexDict.Clear();
end;

destructor TDataCollectionIndexSingle<T>.Destroy;
begin
  FreeAndNil(FIndexDict);

  inherited;
end;

function TDataCollectionIndexSingle<T>.GetFinder(
  const AValue: TValue; const ACompareOperation : TEntitiesCompareOperation): TDataFinderWithEnumeration<TDataEntity>;
begin
  Result := Finder(AValue.AsType<T>, ACompareOperation);
end;

function TDataCollectionIndexSingle<T>.GetIndexItem(
  const AValue: TValue): TObject;
begin
  FIndexDict.TryGetValue(AValue.AsType<T>, Result);
end;

function TDataCollectionIndexSingle<T>.KeyFromField(
  const ADataField: TDataField): T;
begin
  Result := ADataField.AsType<T>;
end;

procedure TDataCollectionIndexSingle<T>.AddIndexItem(
  const ADataField: TDataField);
var
  Key : T;
  Obj : TObject;
  ObjList : TObjectList;
begin
  Key := KeyFromField(ADataField);

  if Meta.IsUnique then
    FIndexDict.AddOrSetValue(Key, ADataField.Parent)

  else
  begin

    if FIndexDict.TryGetValue(Key, Obj) then
      ObjList := TObjectList(Obj)
    else
      ObjList := TObjectList.Create;

    FIndexDict.AddOrSetValue(Key, ObjList);

    ObjList.Add(ADataField.Parent);
  end;
end;

procedure TDataCollectionIndexSingle<T>.RemoveIndexItem(
  const ADataField: TDataField);
var
  Key : T;
  Obj : TObject;
begin
  Key := KeyFromField(ADataField);

  if not Meta.IsUnique then
  begin
    FIndexDict.TryGetValue(Key, Obj);
    FreeAndNil(Obj);
  end;

  FIndexDict.Remove(Key);
end;

function TDataCollectionIndexSingle<T>.MakeFinderRaw<TItem>(const AValue : T;
  const ACompareOperation : TEntitiesCompareOperation = ecoEquals) : TDataFinderWithEnumeration<TItem>;
var
  ObjList : TObjectList;
  Index : integer;
begin
  Result := TDataFinderWithEnumeration<TItem>.Create(Owner,
    function (out AItem : TItem; const ARestart : boolean) : boolean
    var
      Obj : TObject;
    begin

      if ARestart then
      begin
        ObjList := nil;
        if not FIndexDict.TryGetValue(AValue, Obj) then
        begin
          AItem := nil;
          Exit(False);
        end;

        if Meta.IsUnique then
          AItem := TItem(Obj)
        else
        begin
          ObjList := Obj as TObjectList;
          Index := 0;
          Result := ObjList.Count = 0;
          if Result then
            AItem := TItem(ObjList[0])
          else
            AItem := nil;
        end;
      end

      else
      if Meta.IsUnique then
        Result := False

      else
      begin
        Inc(Index);
        Result := Index < ObjList.Count;
        if Result  then
          AItem := TItem(ObjList[Index])
        else
          AItem := nil;
      end;
    end,
    function (AItem : TItem) : boolean
    var
      DataField : TDataField;
    begin
      Result := TDataEntity(AItem).Fields.TryFind(Meta.Field, DataField)
      {$IFDEF DEBUG}
      ;
      Result := Result
      {$ENDIF}
            and DataField.CompareDataValueTo(TValue.From<T>(AValue), ACompareOperation);
    end
  );
end;

function TDataCollectionIndexSingle<T>.Finder(
  const AValue: T; const ACompareOperation : TEntitiesCompareOperation): TDataFinderWithEnumeration<TDataEntity>;
begin
  Result := MakeFinderRaw<TDataEntity>(AValue, ACompareOperation);
end;
{$ENDREGION}

{$REGION 'TDataCollection.TDataCollectionInternalDataCustom'}
constructor TDataCollection.TDataCollectionInternalDataCustom.Create(
  const AOwner: TDataNodeEntityOrCollection);
var
  Owner : TDataCollection;
  MetaIndexes : TMetaCollection.TMetaCollectionIndexesFacade;
  i : integer;
  MetaCollectionIndex : TMetaCollectionIndex;
  DataCollectionIndex : TDataCollectionIndex;
begin
  inherited;

  Owner := AOwner as TDataCollection;

  FItems := Owner.MakeCollection(GetOwnObjects);
  FItems.OnNotify := Owner.EntitiesListNotify;

  MetaIndexes := Owner.Meta.Indexes;
  if MetaIndexes.Count > 0 then
  begin
    SetLength(FIndexes, MetaIndexes.Count);

    for i := 0 to MetaIndexes.Count - 1 do
    begin
      MetaCollectionIndex := MetaIndexes[i];
      MetaCollectionIndex.MakeDataRaw(AOwner, DataCollectionIndex);
      FIndexes[i] := DataCollectionIndex;
    end;
  end;
end;

procedure TDataCollection.TDataCollectionInternalDataCustom.ClearIndexes();
var
  i : integer;
begin
  for i := 0 to High(FIndexes) do
    FIndexes[i].Clear;
end;

procedure TDataCollection.TDataCollectionInternalDataCustom.ClearBeforeFree(
  const AOwner: TDataNodeEntityOrCollection);
begin
  // Разделяем очистку и освобождение, чтобы при очистке ссылка была доступна
  if Assigned(FItems) then
  begin
    FItems.OnNotify := nil;
    FItems.Clear;
  end;

  ClearIndexes();

  FreeAndNil(FItems);
  ClearObjsArray(FIndexes);

  inherited;
end;
{$ENDREGION}

{$REGION 'TDataCollection.TDataCollectionInternalData'}
function TDataCollection.TDataCollectionInternalData.DoExportToJSON(
  const AOwner : TDataNodeEntityOrCollection;
  const ACompact : boolean) : TJSONValue;
var
  JArray : TJSONArray;
  i : integer;
begin
  JArray := TJSONArray.Create();
  Result := JArray;
  try
    for i := 0 to FItems.Count - 1 do
      JArray.AddElement(FItems[i].ExportToJSON(ACompact));
  except
    FreeAndNil(Result);
    raise;
  end;

  if ACompact and (JArray.Count = 0) then
    FreeAndNil(Result);
end;

procedure TDataCollection.TDataCollectionInternalData.DoImportFromJSON(
  const AOwner: TDataNodeEntityOrCollection; const AJSON: TJSONValue;
  const AReWrite : boolean);
var
  JArray : TJSONArray;
  ItemMeta : TMetaEntity;

  function JItem(const AIndex : integer) : TJSONObject;
  begin
    if not (JArray.Items[AIndex] is TJSONObject) then
      raise EEntitiesRWError.CreateResFmt(@s_San_Entities_JSONImportUnexpectedValueFmt, [JArray.Items[AIndex].ClassName, JArray.Items[AIndex].ToString]);

    Result := TJSONObject(JArray.Items[AIndex]);
  end;

var
  JObj : TJSONObject;
  i, Idx : integer;
  IsNew : boolean;
  Item : TDataEntity;
begin
  if not (AJSON is TJSONArray) then
    raise EEntitiesRWError.CreateResFmt(@s_San_Entities_JSONImportUnexpectedValueFmt, [AJSON.ClassName, AJSON.ToString]);

  JArray := TJSONArray(AJSON);
  ItemMeta := (AOwner as TDataCollection).Meta.ItemMeta;

  for i := 0 to JArray.Count - 1 do
  begin
    JObj := JItem(i);

    IsNew := not TDataCollection(AOwner).TryFindByJSON(JObj, Item);

    if IsNew then
    begin

      ItemMeta.MakeDataRaw(AOwner, Item);
      try

        if AReWrite then
          FItems.Insert(i, Item)
        else
          FItems.Add(Item);

      except
        FreeAndNil(Item);
        raise;
      end;

    end

    else
    if AReWrite then
    begin
      Idx := Item.Index;
      if Idx <> i then
        FItems.Move(Idx, i);
    end;

    Item.ImportFromJSON(JObj, AReWrite);
  end;

  if AReWrite then
    for i := FItems.Count - 1 downto JArray.Count do
      FItems.Delete(i);
end;

function TDataCollection.TDataCollectionInternalData.GetOwnObjects: boolean;
begin
  Result := True;
end;
{$ENDREGION}

{$REGION 'TDataCollection.TDataCollectionInternalLink'}
procedure TDataCollection.TDataCollectionInternalLink.ClearBeforeFree(
  const AOwner: TDataNodeEntityOrCollection);
//var
//  Owner, Target : TDataCollection;
begin
  ClearObjsArray(FHandlers);

//  Owner := AOwner as TDataCollection;
//  Target := Owner.Target;
//
//  if Assigned(Target) then
//    Target.InternalNotificationRemove((AOwner as TDataCollection).LinksListInternalNotifier);

  inherited;
end;

procedure TDataCollection.TDataCollectionInternalLink.DetachLink;
begin
end;

function TDataCollection.TDataCollectionInternalLink.GetTarget(
  const AOwner: TDataNodeEntityOrCollection): TDataNodeEntityOrCollection;
begin
  Result := GetLinkTarget(AOwner);
end;

procedure TDataCollection.TDataCollectionInternalLink.InternalDataCreated(
  const AOwner: TDataNodeEntityOrCollection);
var
  Owner, Target : TDataCollection;
  NtfData : TDataObject.TInternalNotifierData;
begin
  inherited;

  Owner := AOwner as TDataCollection;
  Target := Owner.Target;

  if not Assigned(Target) then
    Exit;

  NtfData := Target.InternalNotificationAdd(Owner.LinksListInternalNotifier);
  try

    System.Insert(NtfData, FHandlers, Length(FHandlers));

  except
    FreeAndNil(NtfData);
    raise;
  end;

  Refill(Owner);
end;

function TDataCollection.TDataCollectionInternalLink.GetResolvedLink(
  const AOwner: TDataCollection): TPathResolver.TPathItem;
var
  Meta : TMetaCollection;
  InMeta : TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternal;
  InMetaLink : TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalLink;
begin
  Meta := AOwner.Meta;
  Assert(Assigned(Meta), 'Meta is nil');

  InMeta := Meta.InternalData;
  Assert(Assigned(InMeta), 'InMeta is nil');

  InMetaLink := InMeta as TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalLink;
  Assert(Assigned(InMetaLink), 'InMetaLink is nil');

  Result := InMetaLink.ResolvedLink;
end;

procedure TDataCollection.TDataCollectionInternalLink.Refill(const AOwner : TDataCollection);
var
  ResolvedLink : TPathResolver.TPathItem;
  Parent : TDataEntity;
  Targets : TArray<TDataNode>;
  i : integer;
begin
  ResolvedLink := Self.GetResolvedLink(AOwner);

  Parent := AOwner.Parent;
  Targets := ResolvedLink.FindEndTargetsData(enntEntity, Parent, Parent);

  ClearIndexes;

  FItems.Count := Length(Targets);
  for i := 0 to High(Targets) do
    FItems[i] := Targets[i] as TDataEntity;
end;

function TDataCollection.TDataCollectionInternalLink.GetOwnObjects: boolean;
begin
  Result := False;
end;
{$ENDREGION}

{$REGION 'TDataCollection.TDataIndexesFacade'}
constructor TDataCollection.TDataIndexesFacade.Create(
  const ATarget: TDataCollection);
begin
  FTarget := ATarget;
end;

function TDataCollection.TDataIndexesFacade.GetIndexes : TArray<TDataCollectionIndex>;
var
  Target : TDataCollection;
begin
  Target := FTarget;
  repeat

    if Target.InternalData is TDataCollectionInternalData then
      Result := TDataCollectionInternalData(Target.InternalData).FIndexes
    else
    if Target.InternalData is TDataCollectionInternalLink then
      Result := TDataCollectionInternalLink(Target.InternalData).FIndexes
    else
    begin
      Target := Target.Target;
      Continue;
    end;

    Break;
  until False;
end;

function TDataCollection.TDataIndexesFacade.GetCount: integer;
var
  Indexes : TArray<TDataCollectionIndex>;
begin
  Indexes := Self.Indexes;
  Result := Length(Indexes);
end;

function TDataCollection.TDataIndexesFacade.GetByIndex(
  const AIndex: integer): TDataCollectionIndex;
var
  Indexes : TArray<TDataCollectionIndex>;
begin
  Indexes := Self.Indexes;
  if (AIndex < 0) or (AIndex > High(Indexes)) then
    raise EEntitiesError.CreateFmt(SListIndexError, [AIndex]);

  Result := Indexes[AIndex];
end;

function TDataCollection.TDataIndexesFacade.TryFindSingleIndex(
  AMetaField: TMetaField;
  out ADataEntitiesIndexSingle: TDataCollectionIndexSingle): boolean;
var
  Arr : TArray<TDataCollectionIndex>;
  i : integer;
  Item : TDataCollectionIndexSingle;
begin
  Arr := Self.Indexes;
  for i := 0 to High(Arr) do
    if Arr[i] is TDataCollectionIndexSingle then
    begin
      Item := Arr[i] as TDataCollectionIndexSingle;

      if Item.Meta.Field <> AMetaField then
        Continue;

      ADataEntitiesIndexSingle := Item;
      Exit(True);
    end;

  Result := False;
end;

function TDataCollection.TDataIndexesFacade.TryFindSingleIndex<T>(
  AMetaField: TMetaField;
  out ADataEntitiesIndexSingle: TDataCollectionIndexSingle<T>): boolean;
var
  Arr : TArray<TDataCollectionIndex>;
  i : integer;
  Item : TDataCollectionIndexSingle<T>;
begin
  Arr := Self.Indexes;
  for i := 0 to High(Arr) do
    if Arr[i] is TDataCollectionIndexSingle<T> then
    begin
      Item := Arr[i] as TDataCollectionIndexSingle<T>;

      if Item.Meta.Field <> AMetaField then
        Continue;

      ADataEntitiesIndexSingle := Item;
      Exit(True);
    end;

  Result := False;
end;

procedure TDataCollection.TDataIndexesFacade.AddEntity(
  const AEntity: TDataEntity);
var
  Arr : TArray<TDataCollectionIndex>;
  i : integer;
begin
  Arr := Self.Indexes;
  for i := 0 to High(Arr) do
    Arr[i].AddItemToIndex(AEntity);
end;

procedure TDataCollection.TDataIndexesFacade.RemoveEntity(
  const AEntity: TDataEntity);
var
  Arr : TArray<TDataCollectionIndex>;
  i : integer;
begin
  Arr := Self.Indexes;
  for i := 0 to High(Arr) do
    Arr[i].RemoveItemFromIndex(AEntity);
end;
{$ENDREGION}

{$REGION 'TDataCollection.TSelection<T>'}
{$REGION 'TDataCollection.TSelection<T>.TIternalData'}
function TDataCollection.TSelection<T>.TIternalData.GetObjRef: TObject;
begin
  Result := Self;
end;

constructor TDataCollection.TSelection<T>.TIternalData.Create(
  const ATarget: TDataCollection);
begin
  inherited Create();
  FTarget := ATarget;
end;
{$ENDREGION}

{$REGION 'TDataCollection.TSelection<T>.TEndOfConditionEnumerator'}
constructor TDataCollection.TSelection<T>.TEndOfConditionEnumerator.Create(
  const AIternalData : IInterface);
begin
  inherited Create;

  FInternalData := AIternalData;
end;

function TDataCollection.TSelection<T>.TEndOfConditionEnumerator.GetCurrent: T;
var
  Data : TSelection<T>.TIternalData;
begin
  San.Api.Intfs.IntfToObj(FInternalData, Data);
  Result := T(Data.FTarget.TargetList[FIndex]);
end;

function TDataCollection.TSelection<T>.TEndOfConditionEnumerator.MoveNext: Boolean;
var
  Data : TSelection<T>.TIternalData;
begin
  San.Api.Intfs.IntfToObj(FInternalData, Data);

  if FIndex >= Data.FTarget.TargetList.Count then
    Exit(False);

  //TODO  while
  Inc(FIndex);

  Result := FIndex < Data.FTarget.TargetList.Count;
end;
{$ENDREGION}

{$REGION 'TDataCollection.TSelection<T>.TEndOfCondition'}
function TDataCollection.TSelection<T>.TEndOfCondition.&And: PLeftSideExpression;
begin
  // Add and

  Result := @Self;
end;

function TDataCollection.TSelection<T>.TEndOfCondition.GetEnumerator: TEndOfConditionEnumerator;
begin
  Result := TEndOfConditionEnumerator.Create(FInternalData);
end;
{$ENDREGION}

{$REGION 'TDataCollection.TSelection<T>.TRightSideExpression'}
function TDataCollection.TSelection<T>.TRightSideExpression.Field(
  const AMetaField: TMetaField): TEndOfCondition;
begin

end;

function TDataCollection.TSelection<T>.TRightSideExpression.Field(
  const AFieldName: string): TEndOfCondition;
var
  Data : TSelection<T>.TIternalData;
begin
  San.Api.Intfs.IntfToObj(FInternalData, Data);
  Result := Field(Data.FTarget.Meta.ItemMeta.Fields.Items[AFieldName]);
end;

function TDataCollection.TSelection<T>.TRightSideExpression.Value(
  const AValue: TValue): TEndOfCondition;
begin

end;
{$ENDREGION}

{$REGION 'TDataCollection.TSelection<T>.TExpressionCondition'}
function TDataCollection.TSelection<T>.TExpressionCondition.Eq: TRightSideExpression;
begin
  Result.FInternalData := FInternalData;
end;

function TDataCollection.TSelection<T>.TExpressionCondition.Gr: TRightSideExpression;
begin

end;

function TDataCollection.TSelection<T>.TExpressionCondition.GrEq: TRightSideExpression;
begin

end;

function TDataCollection.TSelection<T>.TExpressionCondition.Ls: TRightSideExpression;
begin

end;

function TDataCollection.TSelection<T>.TExpressionCondition.LsEq: TRightSideExpression;
begin

end;
{$ENDREGION}

{$REGION 'TDataCollection.TSelection<T>.TLeftSideExpression'}
function TDataCollection.TSelection<T>.TLeftSideExpression.Field(
  const AMetaField: TMetaField): TExpressionCondition;
begin

end;

function TDataCollection.TSelection<T>.TLeftSideExpression.Field(
  const AFieldName: string): TExpressionCondition;
var
  Data : TSelection<T>.TIternalData;
begin
  San.Api.Intfs.IntfToObj(FInternalData, Data);
  Result := Field(Data.FTarget.Meta.ItemMeta.Fields.Items[AFieldName]);
end;

function TDataCollection.TSelection<T>.TLeftSideExpression.Value(
  const AValue: TValue): TExpressionCondition;
begin

end;
{$ENDREGION}

{$REGION 'TDataCollection.TSelection<T>.TSelect'}
function TDataCollection.TSelection<T>.TSelect.Where: TLeftSideExpression;
begin
  Result.FInternalData := FInternalData;
end;
{$ENDREGION}
{$ENDREGION}

{$REGION 'TDataCollection'}
class function TDataCollection.MakeCollection(const AOwnObjects : boolean): TEntitiesList;
begin
  Result := TEntitiesList.Create(AOwnObjects);
end;

procedure TDataCollection.NotifyLinkEvent(Sender: TDataCollection; const AItem: TDataEntity;
  AAction: TCollectionNotification);
begin
  Assert(IsLink);

  (InternalData as TDataCollectionInternalLink).Refill(Self);
end;

constructor TDataCollection.InternalCreate(
  const AContext: TDataObject.TCreateContext);
begin
  inherited;
end;

function TDataCollection.GetTarget: TDataCollection;
begin
  Result := TDataCollection(inherited Target);
end;

function TDataCollection.GetTargetList: TEntitiesList;
var
  Target : TDataCollection;
begin
  Target := Self;
  repeat

    if Target.InternalData is TDataCollectionInternalData then
      Result := TDataCollectionInternalData(Target.InternalData).FItems
    else
    if Target.InternalData is TDataCollectionInternalLink then
      Result := TDataCollectionInternalLink(Target.InternalData).FItems
    else
    begin
      Target := Target.Target;
      Continue;
    end;

    Break;
  until False;
end;

function TDataCollection.GetCapacity: integer;
begin
  Result := TargetList.Capacity;
end;

procedure TDataCollection.SetCapacity(const ACapacity: integer);
begin
  TargetList.Capacity := ACapacity;
end;

function TDataCollection.GetCount: integer;
begin
  Result := TargetList.Count;
end;

procedure TDataCollection.SetCount(const ACount: integer);
begin
  TargetList.Count := ACount;
end;

function TDataCollection.TryFindByJSON(const AJSON: TJSONObject;
  out AItem: TDataEntity): boolean;
begin
  AItem := nil;
  Result := False;
end;

function TDataCollection.GetEnumerator: TEnumerator<TDataEntity>;
begin
  Result := TargetList.GetEnumerator;
end;

function TDataCollection.GetIndexes: TDataIndexesFacade;
begin
  Result := TDataIndexesFacade.Create(Self);
end;

function TDataCollection.GetInternalDataClass: TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalClass;
begin
  Result := TDataCollectionInternalData;
end;

function TDataCollection.GetInternalDataLinkClass: TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalClass;
var
  ResolvedLink : TPathResolver.TPathItem;
begin
  ResolvedLink := (Meta.InternalData as TMetaNodeEntityOrCollection.TMetaNodeEntityOrCollectionInternalLink).ResolvedLink;
  if ResolvedLink.UltimateChild is TPathResolver.TPathItemFilter then
    Result := TDataCollectionInternalLink
  else
    Result := inherited;
end;

function TDataCollection.DoFindChildNode(const ANodeType: TEntitiesNodeType;
  const AName: string; out ADataNode: TDataNode): boolean;
begin
  Result := inherited;
end;

function TDataCollection.GetItems(const AIndex: integer): TDataEntity;
begin
  Result := TargetList[AIndex];
end;

function TDataCollection.MakeFinderRaw<T>(const APredicate : TPredicate<T>) : TDataFinderWithEnumeration<T>;
var
  Index : integer;
begin
  Result := TDataFinderWithEnumeration<T>.Create(Self,
    function (out AItem : T; const ARestart : boolean) : boolean
    begin
      if ARestart then
        Index := 0
      else
        Inc(Index);

      Result := Index < TargetList.Count;
      if Result then
        AItem := T(TargetList[Index])
      else
        AItem := nil;

    end,
    APredicate
  );
end;

function TDataCollection.MakeFinderByFldRaw<TItem, TFieldValue>(
  const AFieldName: string;
  const AFieldValue: TFieldValue): TDataFinderWithEnumeration<TItem>;
var
  MFld : TMetaField;
  AutoNoIndex : TDataCollectionIndexSingle<TFieldValue>;
  FieldValue: TValue;
begin
  MFld := Meta.Fields.Items[AFieldName];
  FieldValue := TValue.From<TFieldValue>(AFieldValue);

  if Indexes.TryFindSingleIndex<TFieldValue>(MFld, AutoNoIndex) then
    Result := AutoNoIndex.MakeFinderRaw<TItem>(AFieldValue)
  else
    Result := MakeFinderRaw<TItem>(
      function (ATestItem : TItem) : boolean
      begin
        Result := TDataEntity(ATestItem).Fields[MFld].CompareDataValueTo(FieldValue, ecoEquals);
      end
    );

  Result.OnError := procedure (AError : Exception)
  begin
    EEntitiesError.CreateResFmt(@s_San_Entities_FindByFieldInternalErrorXXXYYYFmt, [AFieldName, FieldValue.AsString]).RaiseOuter();
  end;

end;

function TDataCollection.Finder(
  const APredicate: TPredicate<TDataEntity>): TDataFinderWithEnumeration<TDataEntity>;
begin
  Result := MakeFinderRaw<TDataEntity>(APredicate);
end;

function TDataCollection.Select: TSelection<TDataEntity>.TSelect;
begin
  raise ENotImplemented.CreateFmt('%s.%s.Select', [UnitName, ClassName]);
  Result.FInternalData := TSelection<TDataEntity>.TIternalData.Create(Self);
end;

function TDataCollection.Add(const ADataEntity: TDataEntity): TDataEntity;
begin
  Result := ADataEntity;
  TargetList.Add(ADataEntity);
end;

function TDataCollection.Add(): TDataEntity;
begin
  Meta.ItemMeta.MakeDataRaw(Self, Result);
  try
    Result := Add(Result);
  except
    FreeAndNil(Result);
    raise;
  end;
end;

procedure TDataCollection.DecNextIndexes(const AStartIndex: integer);
var
  TargetList : TEntitiesList;
  i : integer;
  pIndex : PInteger;
begin
  if AStartIndex < 0 then
    Exit;

  TargetList := Self.TargetList;
  for i := AStartIndex to Count - 1 do
    if TargetList[i].AccessForIndexPtr(pIndex) then
      Dec(pIndex^);
end;

procedure TDataCollection.Delete(const AIndex: integer);
begin
  TargetList.Delete(AIndex);
end;

destructor TDataCollection.Destroy;
begin

  inherited;
end;

procedure TDataCollection.Exchange(const AIndex1, AIndex2: integer);
begin
  TargetList.Exchange(AIndex1, AIndex2);
end;

function TDataCollection.Insert(const AIndex: integer;
  const ADataEntity: TDataEntity): TDataEntity;
begin
  Result := ADataEntity;

  TargetList.Insert(AIndex, ADataEntity);
end;

procedure TDataCollection.EntitiesListNotify(Sender: TObject;
  const AItem: TDataEntity; AAction: TCollectionNotification);

  procedure NotyfyLinks();
  var
    Ctx : TInNtfCtx;
  begin
    if IsLink or (InternalNotificationCount = 0) then
      Exit;

    if AAction = cnAdded then
      Ctx := TInNtfCtx.Create(Self, AItem, inaAdded)
    else
      Ctx := TInNtfCtx.Create(Self, AItem, inaRemoved);

    InternalNotificationNotify(Ctx);
  end;

var
  pIndex : PInteger;
begin

  if not Assigned(AItem) then
    Exit;

  if AAction <> cnAdded then
  begin
    {$REGION 'Notification'}
    if (AAction = cnRemoved) and Root.Meta.IsEventsEnabled then
      FireEventDelete(Self, AItem.Index, AItem);
    {$ENDREGION}

    if (InternalData is TDataCollectionInternalDataCustom) then
      Indexes.RemoveEntity(AItem);

    NotyfyLinks();

    Exit;
  end;

  if not IsLink then
  begin
    if not AItem.AccessForIndexPtr(pIndex) then
      San.Api.Excps.ItsImpossible();

    pIndex^ := TargetList.IndexOf(AItem);
  end;

  if (InternalData is TDataCollectionInternalDataCustom) then
    Indexes.AddEntity(AItem);

  {$REGION 'Notification'}
  if Root.Meta.IsEventsEnabled then
    FireEventInsert(Self, AItem.Index, AItem);
  {$ENDREGION}

  NotyfyLinks();
end;

procedure TDataCollection.LinksListInternalNotifier(
  const AContext: TDataObject.TInNtfCtx);
var
  InternalData : TDataCollectionInternalLink;
begin
  InternalData := TDataCollectionInternalLink(Self.InternalData);
  if Assigned(InternalData) then
    if AContext.Action = inaClear then
    begin
      InternalData.ClearIndexes();
      InternalData.FItems.Clear();
    end

    else
      InternalData.Refill(Self);
end;

procedure TDataCollection.OwnedDestroing(const AOwned: TDataObject);
var
  Arr : TArray<TDataCollectionIndex>;
  i : integer;
begin
  inherited;

  if (AOwned is TDataEntity) then
    TargetList.ExtractItem( TDataEntity(AOwned), FromEnd)

  else
  if (AOwned is TDataCollectionIndex) then
  begin
    Arr := (InternalData as TDataCollectionInternalData).FIndexes;
    for i := 0 to High(Arr) do
      if Arr[i] = AOwned then
      begin
        System.Delete(Arr, i, 1);
        Exit;
      end;
  end;
end;

procedure TDataCollection.Refresh;
var
  Item : TDataEntity;
begin
  inherited;

  if InternalData is TDataCollectionInternalLink then
    TDataCollectionInternalLink(InternalData).Refill(Self)
  else
  if InternalData is TDataCollectionInternalData then
    for Item in TargetList do
      Item.Refresh();
end;
{$ENDREGION}

{$REGION 'TDataCollection<T>'}
class function TDataCollection<T>.MakeCollection(const AOwnObjects: boolean): TDataCollection.TEntitiesList;
begin
  Result := TDataCollection.TEntitiesList(TEntitiesList.Create(AOwnObjects));
end;

constructor TDataCollection<T>.InternalCreate(
  const AContext: TDataObject.TCreateContext);
begin
  San.Api.Objs.ValidateGen<T, TDataEntity>();
  inherited;
end;

function TDataCollection<T>.GetTargetList: TEntitiesList;
begin
  Result := TEntitiesList(inherited TargetList);
end;

function TDataCollection<T>.Add(const ADataEntity: T): T;
begin
  Result := ( inherited Add(TDataEntity(ADataEntity)) ) as T;
end;

function TDataCollection<T>.Add: T;
begin
  Result := ( inherited Add() ) as T;
end;

function TDataCollection<T>.Finder(
  const APredicate: TPredicate<T>): TDataFinderWithEnumeration<T>;
begin
  Result := MakeFinderRaw<T>(APredicate);
end;

function TDataCollection<T>.GetEnumerator: TEnumerator<T>;
begin
  Result := TargetList.GetEnumerator;
end;

function TDataCollection<T>.GetItems(const AIndex: integer): T;
begin
  Result := ( inherited Items[AIndex] ) as T;
end;

function TDataCollection<T>.Insert(const AIndex: integer;
  const ADataEntity: T): T;
begin
  Result := ( inherited Insert(AIndex, TDataEntity(ADataEntity)) ) as T;
end;
{$ENDREGION}

{$REGION 'TDataEntityRoot.TDataEntityRootInternalData'}
procedure TDataEntityRoot.TDataEntityRootInternalData.DoExportToJSONObject(
  const AOwner: TDataEntity; const AJSON: TJSONObject; const ACompact : boolean);
var
  JVersion : TJSONNumber;
begin
  JVersion := TJSONNumber.Create( (AOwner as TDataEntityRoot).FVersion);
  try
    AJSON.AddPair(sVersion, JVersion);
    JVersion := nil;
  finally
    FreeAndNil(JVersion);
  end;

  inherited;
end;
{$ENDREGION}

{$REGION 'TDataEntityRoot'}
class function TDataEntityRoot.GetMetaClass: TMetaNodeClass;
begin
  Result := TMetaEntityRoot;
end;

class function TDataEntityRoot.GetDataOwnerClass: TDataObjectClass;
begin
  Result := nil;
end;

function TDataEntityRoot.GetInternalDataClass: TDataNodeEntityOrCollection.TDataNodeEntityOrCollectionInternalClass;
begin
  Result := TDataEntityRootInternalData;
end;

class function TDataEntityRoot.CheckOwnerClass(const AOwner: TDataNode): boolean;
begin
  Result := AOwner = nil;
end;

constructor TDataEntityRoot.InternalCreate(
  const AContext: TDataObject.TCreateContext);
begin
  inherited;

  FRoot := Self;
end;

procedure TDataEntityRoot.SetVersion(const AValue: integer);
begin
  if FVersion = AValue then
    Exit;

  FVersion := AValue;
end;

function TDataEntityRoot.GetMeta: TMetaEntityRoot;
begin
  Result := (inherited Meta) as TMetaEntityRoot;
end;

procedure TDataEntityRoot.DoImportFromJSON(const AJSON: TJSONValue;
  const AReWrite: boolean);
var
  JVersion : TJSONNumber;
begin
  if not AJSON.TryGetValue(sVersion, JVersion) then
    RaiseJSONImportPairNotFound(sVersion);

  FVersion := JVersion.AsInt;

  inherited;
end;
{$ENDREGION}

{$ENDREGION}

{$REGION 'Api'}
class procedure Api.Initialize;
begin
  FMetaRoots := TThreadList<TMetaEntityRoot>.Create();
end;

class function Api.ValueToJSON<T>(const AValue: T): TJSONValue;
var
  TI : PTypeInfo;
  IsError : boolean;
begin
  TI := TypeInfo(T);
  IsError := False;

  case TI^.Kind of
    tkInteger:
      Result := TJSONNumber.Create(PInteger(@AValue)^);

    tkInt64:
      Result := TJSONNumber.Create(PInt64(@AValue)^);

    tkFloat:
      if TI = Typeinfo(Extended) then
        Result := TJSONNumber.Create(PExtended(@AValue)^)
      else
      if TI = Typeinfo(Double) then
        Result := TJSONNumber.Create(PDouble(@AValue)^)
      else
      if TI = Typeinfo(TDateTime) then
        Result := TJSONString.Create(DateTimeToStr(PDouble(@AValue)^, System.JSON.GetJSONFormat()))
      else
      if TI = Typeinfo(TDate) then
        Result := TJSONString.Create(DateToStr(PDouble(@AValue)^, System.JSON.GetJSONFormat()))
      else
      if TI = Typeinfo(TTime) then
        Result := TJSONString.Create(TimeToStr(PDouble(@AValue)^, System.JSON.GetJSONFormat()))
      else
        IsError := True;

    tkRecord:
      if TI = Typeinfo(TGUID) then
        Result := TJSONString.Create(GUIDToString(PGUID(@AValue)^))
      else
        IsError := True;

    tkChar:
      Result := TJSONString.Create(PChar(@AValue)^);

    tkUString:
      Result := TJSONString.Create(PString(@AValue)^);

  else
    IsError := False;
  end;

  if IsError then
    raise EEntitiesRWError.CreateResFmt(@s_San_Entities_ValueToJSONConvertErrorForXXXFmt, [TI^.NameFld.ToString, GetEnumName(TypeInfo(TTypeKind), Ord(TI^.Kind))]);
end;

class function Api.JSONToValue<T>(const AJValue: TJSONValue): T;
var
  TI : PTypeInfo;
  IsError : boolean;
begin
  Assert(Assigned(AJValue), 'AValue is nil');

  TI := TypeInfo(T);
  IsError := False;

  if AJValue is TJSONNumber then
    case TI^.Kind of
      tkInteger:
        PInteger(@Result)^ := TJSONNumber(AJValue).AsInt;

      tkInt64:
        PInt64(@Result)^ := TJSONNumber(AJValue).AsInt64;

      tkFloat:
        if TI = Typeinfo(Extended) then
          PExtended(@Result)^ := TJSONNumber(AJValue).AsDouble
        else
        if TI = Typeinfo(Double) then
          PDouble(@Result)^ := TJSONNumber(AJValue).AsDouble
        else
          IsError := True;
    else
      IsError := True;
    end

  else
  if AJValue is TJSONString then
    case TI^.Kind of
      tkFloat:
        if TI = Typeinfo(TDateTime) then
          PDouble(@Result)^ := StrToDateTime(AJValue.Value, System.JSON.GetJSONFormat())
        else
        if TI = Typeinfo(TDate) then
          PDouble(@Result)^ := StrToDate(AJValue.Value, System.JSON.GetJSONFormat())
        else
        if TI = Typeinfo(TTime) then
          PDouble(@Result)^ := StrToTime(AJValue.Value, System.JSON.GetJSONFormat())
        else
          IsError := True;

      tkRecord:
        if TI = Typeinfo(TGUID) then
          PGUID(@Result)^ := StringToGUIDEx(AJValue.Value)
        else
          IsError := True;

      tkChar:
        if AJValue.Value.IsEmpty then
          PChar(@Result)^ := #0
        else
          PChar(@Result)^ := AJValue.Value[1];

      tkUString:
        PString(@Result)^ := AJValue.Value;

    else
      IsError := True;
    end

  else
    IsError := True;

  if IsError then
    raise EEntitiesRWError.CreateResFmt(@s_San_Entities_JSONToValueConvertErrorForXXXFmt, [AJValue.ToString, AJValue.ClassName, TI^.NameFld.ToString, GetEnumName(TypeInfo(TTypeKind), Ord(TI^.Kind))]);
end;

class function Api.StringToGUIDEx(AString: string): TGUID;
begin
  if AString.IsEmpty then
    Exit(TGUID.Empty);

  AString := IfThen(AString[1] <> '{', '{') + AString + IfThen(AString[AString.Length] <> '}', '}');

  Result := StringToGUID(AString);
end;

class procedure Api.Finalize;
begin
  FreeAndNil(FMetaRoots);
end;

class procedure Api.DetachCurThread;
var
  List : TList<TMetaEntityRoot>;
  i : integer;
begin
  List := FMetaRoots.LockList;
  try

    for i := 0 to List.Count - 1 do
      List[i].LockerDetachCurrentThread();

  finally
    FMetaRoots.UnlockList();
  end;
end;

var Internal_Api_DebugJSON : boolean;

class function Api.GetDebugJSON: boolean;
begin
  Result := Internal_Api_DebugJSON;
end;

class procedure Api.SetDebugJSON(const AValue: boolean);
begin
  Internal_Api_DebugJSON := AValue;
end;

class procedure Api.GetDataWeakRef(var AWeakRef; out ATarget);
begin
  if not Assigned(TDataWeakRef<TDataObject>(AWeakRef).FInternalData) then
    TDataObject(ATarget) := nil
  else
    TDataObject(ATarget) := TDataWeakRefImpl.Access(TDataWeakRef<TDataObject>(AWeakRef).FInternalData).Target;
end;

class procedure Api.SetDataWeakRef(var AWeakRef; const ATarget);
begin
  TDataWeakRefImpl.Access(TDataWeakRef<TDataObject>(AWeakRef).FInternalData).Target := TDataObject(ATarget);
end;
{$ENDREGION}



initialization
  THolderApi.Init(Api.FHolder, Api.Initialize, Api.Finalize);

  FillFieldsByStrIDRegistry
  ([
    TMetaFieldInteger,
    TMetaFieldInt64,
    TMetaFieldString,
    TMetaFieldChar,
    TMetaFieldExtended,
    TMetaFieldDouble,
    TMetaFieldDateTime,
    TMetaFieldDate,
    TMetaFieldTime,
    TMetaFieldBoolean,
    TMetaFieldGUID,

    TMetaFieldEntity,
    TMetaFieldCollection
  ]);

end{$WARNINGS OFF}.
{$ENDIF}
