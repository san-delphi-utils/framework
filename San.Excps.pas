{*******************************************************}
{                                                       }
{       Работа с исключениями.                          }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2013-2016         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Работа с исключениями.
/// </summary>
unit San.Excps;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  System.SysUtils, System.Classes, System.Generics.Collections;
{$ENDREGION}
{$ENDIF}

type
  /// <summary>
  ///   Базовое исключение библиотеки San
  /// </summary>
  ESanBasic =
  {$IFDEF USE_ALIASES}
    San.Excps.ESanBasic;
  {$ELSE}
    class(Exception)
    public
      /// <summary>
      ///   Поднять, как внешнее исключение.
      /// </summary>
      /// <param name="AResult">
      ///   Переменная для подавления предупреждений вроде
      ///   <para>
      ///     <c>E1035 Return value of function XXX might be undefined</c>
      ///   </para>
      /// </param>
      procedure RaiseOuter(out AResult); overload;

      /// <summary>
      ///   Поднять, как внешнее исключение.
      /// </summary>
      procedure RaiseOuter(); overload;
    end;
  {$ENDIF}

  /// <summary>
  ///   Исключение работы с RTTI библиотеки San
  /// </summary>
  ESanRtti =
  {$IFDEF USE_ALIASES}
    San.Excps.ESanRtti;
  {$ELSE}
    class(ESanBasic)
    end;
  {$ENDIF}

  /// <summary>
  ///   Список исключений.
  /// </summary>
  TExceptionList =
  {$IFDEF USE_ALIASES}
    San.Excps.TExceptionList;
  {$ELSE}
    class(TObjectList<Exception>)
  strict private
    function GetIsEmpty : boolean;

  public
    type
      /// <summary>
      ///   Пустое перечисление.
      /// </summary>
      TEmptyEnumerator = class(TEnumerator<Exception>)
      protected
        function DoGetCurrent: Exception; override;
        function DoMoveNext: Boolean; override;
      end;

    /// <summary>
    ///   Перечисление.
    /// </summary>
    function GetEnumerator: TEnumerator<Exception>;

    /// <summary>
    ///   Добавить текущее исключение, забрав его у системы.
    /// </summary>
    /// <remarks>
    ///   Должно вызываться внутри блока except..end.
    /// </remarks>
    function AddAcquired : Exception;

    /// <summary>
    ///   Список не инициализирован или пуст.
    /// </summary>
    property IsEmpty : boolean  read GetIsEmpty;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
type
  /// <summary>
  ///   Доступ к методам работы с исключениями.
  /// </summary>
  Api = record
  strict private

  public
    /// <summary>
    ///   Поднять аналогичное исключение.
    /// </summary>
    class procedure ReRaise(const AException : Exception); static;

    /// <summary>
    ///   Исключение невозможной ситуации.
    /// </summary>
    /// <param name="AResult">
    ///   Контейнер результата, подавляющий предупреждение.
    /// </param>
    class procedure ItsImpossible(out AResult); overload; static;

    /// <summary>
    ///   Исключение невозможной ситуации.
    /// </summary>
    class procedure ItsImpossible; overload; static;

  end;

implementation

{$REGION 'ESanBasic'}
procedure ESanBasic.RaiseOuter(out AResult);
begin
  RaiseOuterException(Self);
end;

procedure ESanBasic.RaiseOuter();
begin
  RaiseOuterException(Self);
end;
{$ENDREGION}

{$REGION 'TExceptionList.TEmptyEnumerator'}
function TExceptionList.TEmptyEnumerator.DoGetCurrent: Exception;
begin
  Result := nil;
end;

function TExceptionList.TEmptyEnumerator.DoMoveNext: Boolean;
begin
  Result := False;
end;
{$ENDREGION}

{$REGION 'TExceptionList'}
function TExceptionList.AddAcquired: Exception;
begin
  Result := Exception(AcquireExceptionObject);
  try
    Add(Result);
  except
    FreeAndNil(Result);
  end;
end;

function TExceptionList.GetEnumerator: TEnumerator<Exception>;
begin
  if Assigned(Self) then
    Result := TEnumerator<Exception>(inherited GetEnumerator())
  else
    Result := TEmptyEnumerator.Create();
end;

function TExceptionList.GetIsEmpty: boolean;
begin
  Result := not (Assigned(Self) and (Count > 0));
end;
{$ENDREGION}


{$REGION 'Api'}
class procedure Api.ReRaise(const AException: Exception);

  function CloneException : Exception;
  begin
    Result := ExceptClass(AException.ClassType).Create(AException.Message);
  end;

begin

  if Assigned(AException.InnerException) then
  try

    ReRaise(AException.InnerException)

  except
    Exception.RaiseOuterException(CloneException);
  end

  else
    raise CloneException;
end;

class procedure Api.ItsImpossible(out AResult);
begin
  Api.ItsImpossible;
end;

class procedure Api.ItsImpossible;
begin
  raise ESanBasic.Create('It`s impossible');
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
