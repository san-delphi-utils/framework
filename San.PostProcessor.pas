п»ї{*******************************************************}
{                                                       }
{       РњРѕРґСѓР»СЊ РїСЃРµРІРґРѕРЅРёРјРѕРІ Р±РёР±Р»РёРѕС‚РµРєРё San.              }
{                                                       }
{       Р Р°Р·СЂР°Р±РѕС‚Р°РЅРѕ Рђ.Р’.РЎС‚Р°РЅС†Рѕ                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2016-2018         РЎС‚Р°РЅС†Рѕ Рђ.Р’.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   РџРѕСЃС‚РїСЂРѕС†РµСЃСЃРѕСЂ.
/// </summary>
unit San.PostProcessor;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'WinApi'}
  {$IFDEF MSWINDOWS}
  WinApi.Windows, WinApi.Messages,
  {$ENDIF}
  {$ENDREGION}

  {$REGION 'System'}
  System.SysUtils, System.Classes, System.Generics.Collections
  {$ENDREGION}
;
{$ENDREGION}

{$ENDIF}

type
  /// <summary>
  ///   РџСЂРѕС†РµРґСѓСЂР° РѕР±СЉРµРєС‚Р° Р±РµР· РїР°СЂР°РјРµС‚СЂРѕРІ.
  /// </summary>
  TObjProcedure =
  {$IFDEF USE_ALIASES}
    San.PostProcessor.TObjProcedure;
  {$ELSE}
     procedure of object;
  {$ENDIF}

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   РџСЂРѕС†РµРґСѓСЂР° РѕР±СЉРµРєС‚Р° c 1 РїР°СЂР°РјРµС‚СЂРѕРј.
  /// </summary>
  TObjProcedure<T> = procedure (Arg1: T) of object;

  /// <summary>
  ///   РџСЂРѕС†РµРґСѓСЂР° РѕР±СЉРµРєС‚Р° c 2 РїР°СЂР°РјРµС‚СЂР°РјРё.
  /// </summary>
  TObjProcedure<T1, T2> = procedure (Arg1: T1; Arg2: T2) of object;
  {$ENDIF}

  /// <summary>
  ///   РџРѕСЃС‚РїСЂРѕС†РµСЃСЃРѕСЂ. Р’С‹РїРѕР»РЅСЏРµС‚ РґРµР№СЃС‚РІРёСЏ С‡РµСЂРµР· PostMessage РІРЅСѓС‚СЂРµРЅРЅРµРјСѓ РѕРєРЅСѓ.
  /// </summary>
  TPostProcessor =
  {$IFDEF USE_ALIASES}
    San.PostProcessor.TPostProcessor;
  {$ELSE}
    class(TObject)
  private
    {$IFDEF MSWINDOWS}
    const CM_PROCESSING = WM_USER + 1;
    {$ENDIF}

  private
    {$IFDEF MSWINDOWS}
    FHandle : HWND;

    /// <summary>
    ///   РћР±СЂР°Р±РѕС‚РєР° СЃРѕРѕР±С‰РµРЅРёСЏ.
    /// </summary>
    procedure Processing(var AMessage : TMessage);
    {$ENDIF}

    procedure DoExecute(const AData : TObject);

  public
    constructor Create;
    destructor Destroy; override;

    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ Р±РµР· РїР°СЂР°РјРµС‚СЂРѕРІ.
    /// </summary>
    procedure Execute(const AProc : TProc); overload;

    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ c 1 РїР°СЂР°РјРµС‚СЂРѕРј.
    /// </summary>
    procedure Execute<T>(const AProc : TProc<T>; const AParam : T); overload;

    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ c 2 РїР°СЂР°РјРµС‚СЂР°РјРё.
    /// </summary>
    procedure Execute<T1, T2>(const AProc : TProc<T1, T2>; const AParam1 : T1; const AParam2 : T2); overload;

    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ РѕР±СЉРµРєС‚Р° Р±РµР· РїР°СЂР°РјРµС‚СЂРѕРІ.
    /// </summary>
    procedure Execute(const AProc : TObjProcedure); overload;

    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ РѕР±СЉРµРєС‚Р° c 1 РїР°СЂР°РјРµС‚СЂРѕРј.
    /// </summary>
    procedure Execute<T>(const AProc : TObjProcedure<T>; const AParam : T); overload;

    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ РѕР±СЉРµРєС‚Р° c 2 РїР°СЂР°РјРµС‚СЂР°РјРё.
    /// </summary>
    procedure Execute<T1, T2>(const AProc : TObjProcedure<T1, T2>; const AParam1 : T1; const AParam2 : T2); overload;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
  /// <summary>
  ///   РџРѕСЃС‚РїСЂРѕС†РµСЃСЃРѕСЂ. Р’РЅСѓС‚СЂРµРЅРЅРёРµ РґР°РЅРЅС‹Рµ.
  /// </summary>
  TPostProcessorData = class abstract(TObject)
  private
    procedure Execute; virtual; abstract;
  end;

  /// <summary>
  ///   РџРѕСЃС‚РїСЂРѕС†РµСЃСЃРѕСЂ. Р’РЅСѓС‚СЂРµРЅРЅРёРµ РґР°РЅРЅС‹Рµ. 0.
  /// </summary>
  TPostProcessorData0Param = class(TPostProcessorData)
  private
    FProc : TProc;

    procedure Execute; override;
  end;

  /// <summary>
  ///   РџРѕСЃС‚РїСЂРѕС†РµСЃСЃРѕСЂ. Р’РЅСѓС‚СЂРµРЅРЅРёРµ РґР°РЅРЅС‹Рµ. 1.
  /// </summary>
  TPostProcessorData1Param<T> = class(TPostProcessorData)
  private
    FProc : TProc<T>;
    FParam : T;

    procedure Execute; override;
  end;

  /// <summary>
  ///   РџРѕСЃС‚РїСЂРѕС†РµСЃСЃРѕСЂ. Р’РЅСѓС‚СЂРµРЅРЅРёРµ РґР°РЅРЅС‹Рµ. 2.
  /// </summary>
  TPostProcessorData2Param<T1, T2> = class(TPostProcessorData)
  private
    FProc : TProc<T1, T2>;
    FParam1 : T1;
    FParam2 : T2;

    procedure Execute; override;
  end;

  /// <summary>
  ///   РџРѕСЃС‚РїСЂРѕС†РµСЃСЃРѕСЂ. Р’РЅСѓС‚СЂРµРЅРЅРёРµ РґР°РЅРЅС‹Рµ. РњРµС‚РѕРґ.
  /// </summary>
  TPostProcessorDataMethod = class(TPostProcessorData)
  private
    FProc : TObjProcedure;

    procedure Execute; override;
  end;

  /// <summary>
  ///   РџРѕСЃС‚РїСЂРѕС†РµСЃСЃРѕСЂ. Р’РЅСѓС‚СЂРµРЅРЅРёРµ РґР°РЅРЅС‹Рµ. РњРµС‚РѕРґ 1.
  /// </summary>
  TPostProcessorDataMethod1<T> = class(TPostProcessorData)
  private
    FProc : TObjProcedure<T>;
    FParam : T;

    procedure Execute; override;
  end;

  /// <summary>
  ///   РџРѕСЃС‚РїСЂРѕС†РµСЃСЃРѕСЂ. Р’РЅСѓС‚СЂРµРЅРЅРёРµ РґР°РЅРЅС‹Рµ. РњРµС‚РѕРґ 2.
  /// </summary>
  TPostProcessorDataMethod2<T1, T2> = class(TPostProcessorData)
  private
    FProc : TObjProcedure<T1, T2>;
    FParam1 : T1;
    FParam2 : T2;

    procedure Execute; override;
  end;

  /// <summary>
  ///   Р”РѕСЃС‚СѓРї Рє СЂР°Р±РѕС‚Рµ СЃ РїРѕСЃС‚РїСЂРѕС†РµСЃСЃРѕСЂРѕРј.
  /// </summary>
  Api = record
  private
    class var FHolder: IInterface;
    class var FInstance: TPostProcessor;
    class procedure Finalize(); static;
    class function InitIfNeeded() : TPostProcessor; static;

  public
    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ Р±РµР· РїР°СЂР°РјРµС‚СЂРѕРІ.
    /// </summary>
    class procedure Execute(const AProc : TProc); overload; static;

    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ c 1 РїР°СЂР°РјРµС‚СЂРѕРј.
    /// </summary>
    class procedure Execute<T>(const AProc : TProc<T>; const AParam : T); overload; static;

    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ c 2 РїР°СЂР°РјРµС‚СЂР°РјРё.
    /// </summary>
    class procedure Execute<T1, T2>(const AProc : TProc<T1, T2>; const AParam1 : T1; const AParam2 : T2); overload; static;

    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ РѕР±СЉРµРєС‚Р° Р±РµР· РїР°СЂР°РјРµС‚СЂРѕРІ.
    /// </summary>
    class procedure Execute(const AProc : TObjProcedure); overload; static;

    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ РѕР±СЉРµРєС‚Р° c 1 РїР°СЂР°РјРµС‚СЂРѕРј.
    /// </summary>
    class procedure Execute<T>(const AProc : TObjProcedure<T>; const AParam : T); overload; static;

    /// <summary>
    ///   Р’С‹РїРѕР»РЅРёС‚СЊ РїСЂРѕС†РµРґСѓСЂСѓ РѕР±СЉРµРєС‚Р° c 2 РїР°СЂР°РјРµС‚СЂР°РјРё.
    /// </summary>
    class procedure Execute<T1, T2>(const AProc : TObjProcedure<T1, T2>; const AParam1 : T1; const AParam2 : T2); overload; static;
  end;

implementation

{$REGION 'uses'}
uses
  {$REGION 'San'}
  San.Intfs.Holders
  {$ENDREGION}
  ;
{$ENDREGION}

{$REGION 'TPostProcessorData0Param'}
procedure TPostProcessorData0Param.Execute;
begin
  FProc();
end;
{$ENDREGION}

{$REGION 'TPostProcessorData1Param<T>'}
procedure TPostProcessorData1Param<T>.Execute;
begin
  FProc(FParam);
end;
{$ENDREGION}

{$REGION 'TPostProcessorData2Param<T1, T2>'}
procedure TPostProcessorData2Param<T1, T2>.Execute;
begin
  FProc(FParam1, FParam2);
end;
{$ENDREGION}

{$REGION 'TPostProcessorDataMethod'}
procedure TPostProcessorDataMethod.Execute;
begin
  FProc;
end;
{$ENDREGION}

{$REGION 'TPostProcessorDataMethod1<T>'}
procedure TPostProcessorDataMethod1<T>.Execute;
begin
  FProc(FParam);
end;
{$ENDREGION}

{$REGION 'TPostProcessorDataMethod2<T1, T2>'}
procedure TPostProcessorDataMethod2<T1, T2>.Execute;
begin
  FProc(FParam1, FParam2);
end;
{$ENDREGION}

{$REGION 'TPostProcessor'}
constructor TPostProcessor.Create;
begin
  inherited Create;

  {$IFDEF MSWINDOWS}
  FHandle := AllocateHWnd(Processing);
  if FHandle = 0 then
    RaiseLastOSError;
  {$ENDIF}
end;

destructor TPostProcessor.Destroy;
begin
  {$IFDEF MSWINDOWS}
  DeallocateHWnd(FHandle);
  {$ENDIF}
  inherited;
end;

procedure TPostProcessor.DoExecute(const AData: TObject);
begin
  {$IFDEF MSWINDOWS}
  PostMessage(FHandle, CM_PROCESSING, 0, IntPtr(AData))
  {$ENDIF}
end;

procedure TPostProcessor.Execute(const AProc: TProc);
var
  Data : TPostProcessorData0Param;
begin
  Data := TPostProcessorData0Param.Create;
  Data.FProc := AProc;

  DoExecute(Data);
end;

procedure TPostProcessor.Execute<T>(const AProc: TProc<T>; const AParam: T);
var
  Data : TPostProcessorData1Param<T>;
begin
  Data := TPostProcessorData1Param<T>.Create;
  Data.FProc   := AProc;
  Data.FParam  := AParam;

  DoExecute(Data);
end;

procedure TPostProcessor.Execute<T1, T2>(const AProc: TProc<T1, T2>;
  const AParam1: T1; const AParam2: T2);
var
  Data : TPostProcessorData2Param<T1, T2>;
begin
  Data := TPostProcessorData2Param<T1, T2>.Create;
  Data.FProc    := AProc;
  Data.FParam1  := AParam1;
  Data.FParam2  := AParam2;

  DoExecute(Data);
end;

procedure TPostProcessor.Execute(const AProc: TObjProcedure);
var
  Data : TPostProcessorDataMethod;
begin
  Data := TPostProcessorDataMethod.Create;
  Data.FProc  := AProc;

  DoExecute(Data);
end;

procedure TPostProcessor.Execute<T>(const AProc: TObjProcedure<T>; const AParam : T);
var
  Data : TPostProcessorDataMethod1<T>;
begin
  Data := TPostProcessorDataMethod1<T>.Create;
  Data.FProc  := AProc;
  Data.FParam := AParam;

  DoExecute(Data);
end;

procedure TPostProcessor.Execute<T1, T2>(const AProc: TObjProcedure<T1, T2>; const AParam1 : T1; const AParam2 : T2);
var
  Data : TPostProcessorDataMethod2<T1, T2>;
begin
  Data := TPostProcessorDataMethod2<T1, T2>.Create;
  Data.FProc  := AProc;
  Data.FParam1 := AParam1;
  Data.FParam2 := AParam2;

  DoExecute(Data);
end;

{$IFDEF MSWINDOWS}
procedure TPostProcessor.Processing(var AMessage: TMessage);
var
  Data : TPostProcessorData;
begin

  if AMessage.Msg <> CM_PROCESSING then
    Exit;

  Data := TPostProcessorData(AMessage.LParam);
  try

    Data.Execute;

  finally
    FreeAndNil(Data);
  end;
end;
{$ENDIF}
{$ENDREGION}

{$REGION 'Api'}
class procedure Api.Execute(const AProc: TObjProcedure);
begin
  InitIfNeeded().Execute(AProc);
end;

class procedure Api.Execute(const AProc: TProc);
begin
  InitIfNeeded().Execute(AProc);
end;

class procedure Api.Execute<T1, T2>(const AProc: TObjProcedure<T1, T2>;
  const AParam1: T1; const AParam2: T2);
begin
  InitIfNeeded().Execute<T1, T2>(AProc, AParam1, AParam2);
end;

class procedure Api.Execute<T1, T2>(const AProc: TProc<T1, T2>;
  const AParam1: T1; const AParam2: T2);
begin
  InitIfNeeded().Execute<T1, T2>(AProc, AParam1, AParam2);
end;

class procedure Api.Execute<T>(const AProc: TProc<T>; const AParam: T);
begin
  InitIfNeeded().Execute<T>(AProc, AParam);
end;

class procedure Api.Execute<T>(const AProc: TObjProcedure<T>; const AParam: T);
begin
  InitIfNeeded().Execute<T>(AProc, AParam);
end;

class function Api.InitIfNeeded: TPostProcessor;
begin
  if not Assigned(FInstance) then
    FInstance := TPostProcessor.Create();

  Result := FInstance;
end;

class procedure Api.Finalize;
begin
  FreeAndNil(FInstance);
end;
{$ENDREGION}

initialization
  THolderApi.Init(Api.FHolder, Api.Finalize);

end{$WARNINGS OFF}.
{$ENDIF}

