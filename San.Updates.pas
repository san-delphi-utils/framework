{*******************************************************}
{                                                       }
{       Механизм скоординированных изменений.           }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
unit San.Updates;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.Generics.Collections, System.Generics.Defaults,
  System.Types,
  {$ENDREGION}

  {$REGION 'San'}
  San.Intfs.Basic
  {$ENDREGION}
  ;
{$ENDREGION}
{$ENDIF}

type
  /// <summary>
  ///   Механизм серийных изменений из двух состояний
  TUpdates =
  {$IFDEF USE_ALIASES}
    San.Updates.TUpdates;
  {$ELSE}
  class abstract(TObject)
  private
    FUpdatesCount : integer;

  public
    /// <summary>
    ///   Начать серийные изменения
    /// </summary>
    procedure BeginUpdate(); virtual; abstract;

    /// <summary>
    ///   Завершить серию изменений
    /// </summary>
    procedure EndUpdate(); virtual; abstract;

    /// <summary>
    ///   Завершить серию изменений с ОТМЕНОЙ оповещения
    /// </summary>
    procedure CancelUpdate(); virtual; abstract;

    /// <summary>
    ///   Заявить об изменениях, с масксимальным состоянием изменений
    /// </summary>
    procedure Update(); virtual; abstract;

    /// <summary>
    ///   Глубина серийных изменений
    /// </summary>
    property UpdatesCount : integer  read FUpdatesCount;
  end;
  {$ENDIF}

  {$IFNDEF USE_ALIASES}
  /// <summary>
  ///   Механизм серийных изменений. Генерик состояний
  /// </summary>
  /// <param name="TStates">
  ///   Порядковый тип НЕ БОЛЕЕ 127 значений
  ///   <para>
  ///     При <c>FState = 000</c> обработчик <c>FOnUpdate</c> не вызывается
  ///   </para>
  /// </param>
  TUpdates<TStates> = class abstract (TUpdates)
  strict private
    FState : Byte;
    FOnUpdate : TProc<TStates>;
    function GetIsInUpdate: boolean; inline;
    procedure GetStateRaw(out AState); inline;
    procedure SetStateRaw(const AValue); inline;
    function GetState: TStates; inline;
    procedure SetState(const AValue: TStates); inline;
    procedure ClearState; inline;

  protected
    class function MaxState : TStates; virtual;
    class function IsMinState(const AState : TStates) : boolean; virtual;
    class function CompareStates(const AState1, AState2 : TStates) : TValueRelationship; virtual;

    procedure DoUpdate(const AState : TStates);

    /// <summary>
    ///   Состояние
    /// </summary>
    property State : TStates  read GetState  write SetState;

  public
    constructor Create(const AOnUpdate : TProc<TStates>);

    /// <summary>
    ///   Начать серийные изменения
    /// </summary>
    procedure BeginUpdate(); override;

    /// <summary>
    ///   Завершить серию изменений
    /// </summary>
    procedure EndUpdate(); override;

    /// <summary>
    ///   Завершить серию изменений с ОТМЕНОЙ оповещения
    /// </summary>
    procedure CancelUpdate(); override;

    /// <summary>
    ///   Заявить об изменениях, с указанием состояния изменений
    /// </summary>
    procedure Update(const AState : TStates); reintroduce; overload;

    /// <summary>
    ///   Заявить об изменениях, с масксимальным состоянием изменений
    /// </summary>
    procedure Update(); overload; override;

    /// <summary>
    ///   Задать значение поля, с указанием состояния изменений
    /// </summary>
    /// <param name="AField">
    ///   Поле
    /// </param>
    /// <param name="AValue">
    ///   Новое значение
    /// </param>
    /// <param name="AState">
    ///   Состояние, порождаемое изменениями
    /// </param>
    /// <param name="ABeforeChange">
    ///   Действие перед сменой значения, если нужно
    /// </param>
    /// <returns>
    ///   <c>True</c> — старое и новое значение различались, задано новое значение
    /// </returns>
    function Setter<T>(var AField : T; const AValue : T; const AState : TStates;
     const ABeforeChange : TProc = nil) : boolean; overload;

    /// <summary>
    ///   Задать значение поля, с масксимальным состоянием изменений
    /// </summary>
    /// <param name="AField">
    ///   Поле
    /// </param>
    /// <param name="AValue">
    ///   Новое значение
    /// </param>
    /// <param name="ABeforeChange">
    ///   Действие перед сменой значения, если нужно
    /// </param>
    /// <returns>
    ///   <c>True</c> — старое и новое значение различались, задано новое значение
    /// </returns>
    function Setter<T>(var AField : T; const AValue : T;
      const ABeforeChange : TProc = nil) : boolean; overload; inline;

    /// <summary>
    ///   Задать значение свойства, с указанием состояния изменений
    /// </summary>
    /// <param name="AGetter">
    ///   Функция, возвращающая старое значение поля
    /// </param>
    /// <param name="ASetter">
    ///   Процедура, задающая новое значение поля
    /// </param>
    /// <param name="AValue">
    ///   Новое значение
    /// </param>
    /// <param name="AState">
    ///   Состояние, порождаемое изменениями
    /// </param>
    /// <param name="ABeforeChange">
    ///   Действие перед сменой значения, если нужно
    /// </param>
    /// <returns>
    ///   <c>True</c> — старое и новое значение различались, задано новое значение
    /// </returns>
    function Setter<T>(const AGetter : TFunc<T>; const ASetter : TProc<T>;
      const AValue : T; const AState : TStates; const ABeforeChange : TProc = nil) : boolean; overload;

    /// <summary>
    ///   Задать значение свойства, с масксимальным состоянием изменений
    /// </summary>
    /// <param name="AGetter">
    ///   Функция, возвращающая старое значение поля
    /// </param>
    /// <param name="ASetter">
    ///   Процедура, задающая новое значение поля
    /// </param>
    /// <param name="AValue">
    ///   Новое значение
    /// </param>
    /// <param name="ABeforeChange">
    ///   Действие перед сменой значения, если нужно
    /// </param>
    /// <returns>
    ///   <c>True</c> — старое и новое значение различались, задано новое значение
    /// </returns>
    function Setter<T>(const AGetter : TFunc<T>; const ASetter : TProc<T>;
      const AValue : T; const ABeforeChange : TProc = nil) : boolean; overload; inline;

    /// <summary>
    ///   Признак того, что сейчас выполнение в стеке внутри обработчика <c>FOnUpdate</c>
    /// </summary>
    property IsInUpdate : boolean  read GetIsInUpdate;
  end;
  {$ENDIF}

  /// <summary>
  ///   Механизм серийных изменений из двух состояний
  ///   <para>
  ///     <c>False</c> нет изменений
  ///     <c>True</c> есть изменения
  ///   </para>
  TUpdates2 =
  {$IFDEF USE_ALIASES}
    San.Updates.TUpdates2;
  {$ELSE}
  class(TUpdates<boolean>)
  protected
    class function MaxState : boolean; override;
    class function IsMinState(const AState : boolean) : boolean; override;
    class function CompareStates(const AState1, AState2 : boolean) : TValueRelationship; override;

  public
    constructor Create(const AOnUpdate : TProc); overload;
    constructor Creare(); overload; dynamic; deprecated;
  end;
  {$ENDIF}

  /// <summary>
  ///   Провайдер <c>TUpdates</c>
  /// </summary>
  IProvideUpdates =
  {$IFDEF USE_ALIASES}
    San.Updates.IProvideUpdates;
  {$ELSE}
  interface(IInterface)
  ['{7ED3E904-1307-426F-840E-86BAA92D5E4E}']
    {$REGION 'Gets&Sets'}
    function GetUpdates : TUpdates;
    {$ENDREGION}

    /// <summary>
    ///   Цель провайдера
    /// </summary>
    property Updates : TUpdates  read GetUpdates;
  end;
  {$ENDIF}

  /// <summary>
  ///   Провайдер <c>TUpdates2</c>
  /// </summary>
  IProvideUpdates2 =
  {$IFDEF USE_ALIASES}
    San.Updates.IProvideUpdates2;
  {$ELSE}
  interface(IProvideUpdates)
  ['{2353FBF6-9517-4258-BD2E-A6C51AC485C4}']
    {$REGION 'Gets&Sets'}
    function GetUpdates2 : TUpdates2;
    {$ENDREGION}

    /// <summary>
    ///   Цель провайдера
    /// </summary>
    property Updates : TUpdates2  read GetUpdates2;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
implementation

{$REGION 'uses'}
uses
  {$REGION 'San'}
  San.Messages
  {$ENDREGION}
;
{$ENDREGION}

{$REGION 'TUpdates<TStates>'}
class function TUpdates<TStates>.MaxState: TStates;
begin
  raise ENotSupportedException.CreateResFmt(@sMethodMustBeOverriddenFmt, [UnitName, ClassName, 'MaxState']);
end;

class function TUpdates<TStates>.IsMinState(const AState: TStates): boolean;
begin
  raise ENotSupportedException.CreateResFmt(@sMethodMustBeOverriddenFmt, [UnitName, ClassName, 'IsMinState']);
end;

class function TUpdates<TStates>.CompareStates(const AState1,
  AState2: TStates): TValueRelationship;
begin
  raise ENotSupportedException.CreateResFmt(@sMethodMustBeOverriddenFmt, [UnitName, ClassName, 'CompareStates']);
end;

constructor TUpdates<TStates>.Create(const AOnUpdate: TProc<TStates>);
begin
  inherited Create();
  FOnUpdate := AOnUpdate;
end;

function TUpdates<TStates>.GetIsInUpdate: boolean;
begin
  Result := FState and 128 <> 0;
end;

procedure TUpdates<TStates>.GetStateRaw(out AState);
begin
  Byte(AState) := FState and not 128;
end;

procedure TUpdates<TStates>.SetStateRaw(const AValue);
begin
  FState := Byte(AValue) and not 128 or FState and 128;
end;

function TUpdates<TStates>.GetState: TStates;
begin
  GetStateRaw(Result);
end;

procedure TUpdates<TStates>.SetState(const AValue: TStates);
begin
  SetStateRaw(AValue);
end;

procedure TUpdates<TStates>.ClearState;
begin
  // Чистим все биты кроме последнего
  FState := FState and 128;
end;

procedure TUpdates<TStates>.DoUpdate(const AState: TStates);
begin
  try

    if not IsInUpdate then
    begin
      FState := FState and 128;
      try
        FOnUpdate(AState);
      finally
        FState := FState and not 128;
      end;
    end;

  finally
    ClearState;
  end;
end;

procedure TUpdates<TStates>.BeginUpdate;
begin
  Inc(FUpdatesCount);
end;

procedure TUpdates<TStates>.CancelUpdate;
begin
  ClearState;
  if FUpdatesCount > 0 then
    Dec(FUpdatesCount);
end;

procedure TUpdates<TStates>.EndUpdate;
var
  State : TStates;
begin
  if FUpdatesCount > 0 then
  begin
    Dec(FUpdatesCount);

    State := Self.State;
    if (FUpdatesCount = 0) and not IsMinState(State) then
      DoUpdate(State);
  end;
end;

procedure TUpdates<TStates>.Update(const AState: TStates);
var
  State : TStates;
begin
  if (CompareStates(Self.State, AState) = LessThanValue) then
    Self.State := AState;

  State := Self.State;
  if (FUpdatesCount > 0) or IsMinState(State) then
    Exit;

  DoUpdate(State);
end;

procedure TUpdates<TStates>.Update;
begin
  Update(MaxState);
end;

function TUpdates<TStates>.Setter<T>(var AField: T; const AValue: T;
  const AState: TStates; const ABeforeChange: TProc): boolean;
var
  lComparer: IEqualityComparer<T>;
begin
  lComparer := TEqualityComparer<T>.Default;
  if lComparer.Equals(AField, AValue) then
    Exit(False);

  if Assigned(ABeforeChange) then
    ABeforeChange();

  AField := AValue;

  Update(AState);

  Result := True;
end;

function TUpdates<TStates>.Setter<T>(var AField: T; const AValue: T;
  const ABeforeChange: TProc): boolean;
begin
  Result := Setter<T>(AField, AValue, MaxState, ABeforeChange);
end;

function TUpdates<TStates>.Setter<T>(const AGetter: TFunc<T>;
  const ASetter: TProc<T>; const AValue: T; const AState: TStates;
  const ABeforeChange: TProc): boolean;
var
  lComparer: IEqualityComparer<T>;
begin
  lComparer := TEqualityComparer<T>.Default;
  if lComparer.Equals(AGetter(), AValue) then
    Exit(False);

  if Assigned(ABeforeChange) then
    ABeforeChange();

  ASetter(AValue);

  Update(AState);

  Result := True;
end;

function TUpdates<TStates>.Setter<T>(const AGetter: TFunc<T>;
  const ASetter: TProc<T>; const AValue: T;
  const ABeforeChange: TProc): boolean;
begin
  Result := Setter<T>(AGetter, ASetter, AValue, MaxState, ABeforeChange);
end;
{$ENDREGION}

{$REGION 'TUpdates2'}
class function TUpdates2.MaxState: boolean;
begin
  Result := High(Boolean);
end;

class function TUpdates2.IsMinState(const AState: boolean): boolean;
begin
  Result := AState = Low(Boolean);
end;

class function TUpdates2.CompareStates(const AState1,
  AState2: boolean): TValueRelationship;
begin
  if AState1 = AState2 then
    Result := EqualsValue
  else if AState1 < AState2 then
    Result := LessThanValue
  else
    Result := GreaterThanValue;
end;

constructor TUpdates2.Create(const AOnUpdate: TProc);
begin
  inherited Create(procedure (AState : boolean)
    begin
      AOnUpdate();
    end
  )
end;

constructor TUpdates2.Creare;
begin
  raise ENotSupportedException.CreateFmt('Use %s.%s.Creare(AOnUpdate: TProc)', [UnitName, ClassName]);
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
