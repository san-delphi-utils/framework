{*******************************************************}
{                                                       }
{       Модуль псевдонимов библиотеки San.              }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2013-2016         Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Модуль псевдонимов библиотеки San.
/// </summary>
unit San;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.TypInfo, System.Generics.Defaults,
  System.Generics.Collections, System.Variants,
  {$ENDREGION}

  {$REGION 'San'}
  San.Messages,
  San.Utils,
  San.Logger,
  San.Rtti,
  San.Intfs.Basic,
  San.Intfs.Holders,
  San.Intfs,
  San.PostProcessor,
  San.UpperString,
  San.Excps,
  San.Objs,
  San.Thrds,
  San.Events,
  San.Generics,
  San.Replacement,
  San.StrUtils,
  San.Updates,
  San.IDE
  {$ENDREGION}
;
{$ENDREGION}

  {$DEFINE USE_ALIASES}
    {$I San.Inc.inc}
  {$UNDEF USE_ALIASES}
{$ELSE}
  {$I San.Inc.inc}
{$ENDIF}

{$IFNDEF USE_ALIASES}
type
  /// <summary>
  ///   Доступ к объединенному функционалу библиотеки.
  /// </summary>
  Api = record
  public
    {$I San.Api.Intf.Public.Partial.inc}
  end;

implementation

{$I San.Utils.Impl.inc}

{$I San.Generics.Impl.inc}

end{$WARNINGS OFF}.
{$ENDIF}
