{*******************************************************}
{                                                       }
{       Взаимодействие со средой разработки.            }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Взаимодействие со средой разработки
/// </summary>
unit San.IDE;
{$I San.inc}
interface

uses
  San.Bool3;
{$ENDIF}

{$IFNDEF USE_ALIASES}
type
  /// <summary>
  ///   Доступ к методам работы со средой разработки
  /// </summary>
  Api = record
  strict private

  public
    /// <summary>
    ///   Запуск произведен под отладчиком
    /// </summary>
    class function RunUnderDebugger : Bool3; static;

  end;

implementation

{$REGION 'Api'}
class function Api.RunUnderDebugger: Bool3;
begin
  {$IFDEF MSWINDOWS}
    {$WARN SYMBOL_PLATFORM OFF}
      Result := DebugHook <> 0
    {$WARN SYMBOL_PLATFORM ON}
  {$ELSE}
    Result := Null3
  {$ENDIF}
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
