{*******************************************************}
{                                                       }
{       Логический тип троичной логики.                 }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2017              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Логический тип троичной логики.
/// </summary>
unit San.Bool3;
{$I San.inc}
{.$DEFINE BOOL3_INLINE}
interface
{$ENDIF}

type
  /// <summary>
  ///   Возможные значения троичной логики.
  /// </summary>
  /// <remarks>
  ///   Сравнения значений для данного типа происходят,
  ///   как для прочих порядклвых типов. В отличии от Bool3.
  /// </remarks>
  Bool3Value = (False3, Null3, True3);

  /// <summary>
  ///   Хелпер для <c>Bool3Value</c>
  /// </summary>
  TBool3ValueHelper = record helper for Bool3Value
  strict private
    function GetIsFalse: boolean; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    function GetIsNull: boolean; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    function GetIsTrue: boolean; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

  public
    function ToString() : string;{$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    /// <summary>
    ///   Имеет значение False3.
    /// </summary>
    property IsFalse : boolean read GetIsFalse;

    /// <summary>
    ///   Имеет значение Null3.
    /// </summary>
    property IsNull : boolean read GetIsNull;

    /// <summary>
    ///   Имеет значение True3.
    /// </summary>
    property IsTrue : boolean read GetIsTrue;
  end;

  /// <summary>
  ///   Множество возможных значений троичной логики.
  /// </summary>
  Bool3ValueSet = set of Bool3Value;

  /// <summary>
  ///   Указатель на значение троичной логики.
  /// </summary>
  PBool3 =
  {$IFDEF USE_ALIASES}
    San.Bool3.PBool3;
  {$ELSE}
    ^Bool3;
  {$ENDIF}

  /// <summary>
  ///   Значение троичной логики.
  /// </summary>
  /// <remarks>
  ///   Сравнения значений происходят в рамках троичной логики.
  /// </remarks>
  Bool3 =
  {$IFDEF USE_ALIASES}
    San.Bool3.Bool3;
  {$ELSE}
    record
  strict private
    function GetIsFalse: boolean; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    function GetIsNull: boolean; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    function GetIsTrue: boolean; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

  private
    FData : Bool3Value;
    class procedure RaiseConvertErrorNull3ToBool2(out AResult); static; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

  public
    class operator In(const A: Bool3; const B: Bool3ValueSet) : Boolean; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalNot(const A: Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    class operator Implicit(const A : Bool3): Bool3Value; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator Implicit(const A : Bool3): boolean; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator Implicit(const A : Bool3Value): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator Implicit(const A : boolean): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    class operator Equal(const A, B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator Equal(const A : Bool3Value; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator Equal(const A : Bool3; const B : Bool3Value): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator Equal(const A : boolean; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator Equal(const A : Bool3; const B : boolean): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    class operator NotEqual(const A, B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator NotEqual(const A : Bool3Value; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator NotEqual(const A : Bool3; const B : Bool3Value): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator NotEqual(const A : boolean; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator NotEqual(const A : Bool3; const B : boolean): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    class operator GreaterThan(const A, B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator GreaterThan(const A : Bool3Value; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator GreaterThan(const A : Bool3; const B : Bool3Value): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator GreaterThan(const A : boolean; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator GreaterThan(const A : Bool3; const B : boolean): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    class operator GreaterThanOrEqual(const A, B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator GreaterThanOrEqual(const A : Bool3Value; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator GreaterThanOrEqual(const A : Bool3; const B : Bool3Value): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator GreaterThanOrEqual(const A : boolean; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator GreaterThanOrEqual(const A : Bool3; const B : boolean): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    class operator LessThan(const A, B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LessThan(const A : Bool3Value; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LessThan(const A : Bool3; const B : Bool3Value): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LessThan(const A : boolean; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LessThan(const A : Bool3; const B : boolean): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    class operator LessThanOrEqual(const A, B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LessThanOrEqual(const A : Bool3Value; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LessThanOrEqual(const A : Bool3; const B : Bool3Value): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LessThanOrEqual(const A : Bool3; const B : boolean): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LessThanOrEqual(const A : boolean; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    class operator LogicalAnd(const A, B: Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalAnd(const A : Bool3Value; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalAnd(const A : Bool3; const B : Bool3Value): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalAnd(const A : Bool3; const B : boolean): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalAnd(const A : boolean; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    class operator LogicalOr(const A, B: Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalOr(const A : Bool3Value; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalOr(const A : Bool3; const B : Bool3Value): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalOr(const A : Bool3; const B : boolean): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalOr(const A : boolean; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    class operator LogicalXor(const A, B: Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalXor(const A : Bool3Value; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalXor(const A : Bool3; const B : Bool3Value): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalXor(const A : Bool3; const B : boolean): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}
    class operator LogicalXor(const A : boolean; const B : Bool3): Bool3; {$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    function ToString() : string;{$IFDEF BOOL3_INLINE}inline;{$ENDIF}

    /// <summary>
    ///   Имеет значение False3.
    /// </summary>
    property IsFalse : boolean read GetIsFalse;

    /// <summary>
    ///   Имеет значение Null3.
    /// </summary>
    property IsNull : boolean read GetIsNull;

    /// <summary>
    ///   Имеет значение True3.
    /// </summary>
    property IsTrue : boolean read GetIsTrue;

    /// <summary>
    ///   Значение
    /// </summary>
    property Value : Bool3Value  read FData;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
implementation

uses
  System.SysUtils, System.TypInfo;

{$REGION 'TBool3ValueHelper'}
function TBool3ValueHelper.GetIsFalse: boolean;
begin
  Result := Self = False3;
end;

function TBool3ValueHelper.GetIsNull: boolean;
begin
  Result := Self = Null3;
end;

function TBool3ValueHelper.GetIsTrue: boolean;
begin
  Result := Self = True3;
end;

function TBool3ValueHelper.ToString: string;
begin
  Result := GetEnumName(TypeInfo(Bool3Value), Ord(Self));
end;
{$ENDREGION}

{$REGION 'TBool3'}
class procedure Bool3.RaiseConvertErrorNull3ToBool2(out AResult);
begin
  raise EConvertError.Create('Can not implicit convert ' + Null3.ToString + ' to ' + string(PTypeInfo(TypeInfo(boolean))^.NameFld.ToString) );
end;

class operator Bool3.In(const A: Bool3; const B: Bool3ValueSet): Boolean;
begin
  Result := A.FData in B;
end;

class operator Bool3.LogicalNot(const A: Bool3): Bool3;
begin
  case A.FData of
    False3:  Result.FData := True3;
    True3:   Result.FData := False3;
  else
    Result.FData := Null3;
  end;
end;

{$REGION 'Implicit'}
class operator Bool3.Implicit(const A: Bool3): boolean;
begin
  case A.FData of
    False3:  Result := False;
    True3:   Result := True;
  else
    RaiseConvertErrorNull3ToBool2(Result);
  end;
end;

class operator Bool3.Implicit(const A: Bool3): Bool3Value;
begin
  Result := A.FData;
end;

class operator Bool3.Implicit(const A: boolean): Bool3;
begin
  if A then
    Result.FData := True3
  else
    Result.FData := False3
end;

class operator Bool3.Implicit(const A: Bool3Value): Bool3;
begin
  Result.FData := A;
end;
{$ENDREGION}

{$REGION 'Equal'}
class operator Bool3.Equal(const A, B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData = B.FData;
end;

class operator Bool3.Equal(const A: Bool3Value; const B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A = B.FData;
end;

class operator Bool3.Equal(const A: Bool3; const B: Bool3Value): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData = B;
end;

class operator Bool3.Equal(const A: boolean; const B: Bool3): Bool3;
begin
  if A then
    Result := B
  else
    Result := not B;
end;

class operator Bool3.Equal(const A: Bool3; const B: boolean): Bool3;
begin
  if B then
    Result := A
  else
    Result := not A;
end;
{$ENDREGION}

{$REGION 'NotEqual'}
class operator Bool3.NotEqual(const A, B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData <> B.FData;
end;

class operator Bool3.NotEqual(const A: Bool3Value; const B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A <> B.FData;
end;

class operator Bool3.NotEqual(const A: Bool3; const B: Bool3Value): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData <> B;
end;

class operator Bool3.NotEqual(const A: Bool3; const B: boolean): Bool3;
begin
  if B then
    Result := not A
  else
    Result := A;
end;

class operator Bool3.NotEqual(const A: boolean; const B: Bool3): Bool3;
begin
  if A then
    Result := not B
  else
    Result := B;
end;
{$ENDREGION}

{$REGION 'GreaterThan'}
class operator Bool3.GreaterThan(const A, B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData > B.FData
end;

class operator Bool3.GreaterThan(const A: Bool3Value;
  const B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A > B.FData
end;

class operator Bool3.GreaterThan(const A: Bool3;
  const B: Bool3Value): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData > B;
end;

class operator Bool3.GreaterThan(const A: boolean; const B: Bool3): Bool3;
begin
  if B.IsNull then
    Result := Null3
  else
    Result := A > Boolean(B);
end;

class operator Bool3.GreaterThan(const A: Bool3; const B: boolean): Bool3;
begin
  if A.IsNull then
    Result := Null3
  else
    Result := Boolean(A) > B;
end;
{$ENDREGION}

{$REGION 'GreaterThanOrEqual'}
class operator Bool3.GreaterThanOrEqual(const A: Bool3Value;
  const B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A >= B.FData;
end;

class operator Bool3.GreaterThanOrEqual(const A, B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData >= B.FData;
end;

class operator Bool3.GreaterThanOrEqual(const A: Bool3;
  const B: Bool3Value): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData >= B;
end;

class operator Bool3.GreaterThanOrEqual(const A: boolean;
  const B: Bool3): Bool3;
begin
  if B.IsNull then
    Result := Null3
  else
    Result := A >= Boolean(B);
end;

class operator Bool3.GreaterThanOrEqual(const A: Bool3;
  const B: boolean): Bool3;
begin
  if A.IsNull then
    Result := Null3
  else
    Result := Boolean(A) >= B;
end;
{$ENDREGION}

{$REGION 'LessThan'}
class operator Bool3.LessThan(const A: Bool3Value; const B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A < B.FData;
end;

class operator Bool3.LessThan(const A, B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData < B.FData;
end;

class operator Bool3.LessThan(const A: Bool3; const B: Bool3Value): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData < B;
end;

class operator Bool3.LessThan(const A: Bool3; const B: boolean): Bool3;
begin
  if A.IsNull then
    Result := Null3
  else
    Result := Boolean(A) < B;
end;

class operator Bool3.LessThan(const A: boolean; const B: Bool3): Bool3;
begin
  if B.IsNull then
    Result := Null3
  else
    Result := A < Boolean(B);
end;
{$ENDREGION}

{$REGION 'LessThanOrEqual'}
class operator Bool3.LessThanOrEqual(const A, B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData <= B.FData;
end;

class operator Bool3.LessThanOrEqual(const A: Bool3Value;
  const B: Bool3): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A <= B.FData;
end;

class operator Bool3.LessThanOrEqual(const A: Bool3;
  const B: Bool3Value): Bool3;
begin
  if A.IsNull or B.IsNull then
    Result := Null3
  else
    Result := A.FData <= B;
end;

class operator Bool3.LessThanOrEqual(const A: boolean;
  const B: Bool3): Bool3;
begin
  if B.IsNull then
    Result := Null3
  else
    Result := A <= Boolean(B);
end;

class operator Bool3.LessThanOrEqual(const A: Bool3;
  const B: boolean): Bool3;
begin
  if A.IsNull then
    Result := Null3
  else
    Result := Boolean(A) <= B;
end;
{$ENDREGION}

{$REGION 'LogicalAnd'}
class operator Bool3.LogicalAnd(const A: Bool3Value; const B: Bool3): Bool3;
begin
  if A.IsTrue and B.IsTrue  then
    Result := True3
  else
  if A.IsFalse or B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalAnd(const A, B: Bool3): Bool3;
begin
  if A.IsTrue and B.IsTrue  then
    Result := True3
  else
  if A.IsFalse or B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalAnd(const A: Bool3; const B: Bool3Value): Bool3;
begin
  if A.IsTrue and B.IsTrue  then
    Result := True3
  else
  if A.IsFalse or B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalAnd(const A: boolean; const B: Bool3): Bool3;
begin
  if A and B.IsTrue then
    Result := True3
  else
  if not A or B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalAnd(const A: Bool3; const B: boolean): Bool3;
begin
  if A.IsTrue and B then
    Result := True3
  else
  if A.IsFalse or not B then
    Result := False3
  else
    Result := Null3;
end;
{$ENDREGION}

{$REGION 'LogicalOr'}
class operator Bool3.LogicalOr(const A: Bool3Value; const B: Bool3): Bool3;
begin
  if A.IsTrue or B.IsTrue  then
    Result := True3
  else
  if A.IsFalse and B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalOr(const A, B: Bool3): Bool3;
begin
  if A.IsTrue or B.IsTrue  then
    Result := True3
  else
  if A.IsFalse and B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalOr(const A: Bool3; const B: Bool3Value): Bool3;
begin
  if A.IsTrue or B.IsTrue  then
    Result := True3
  else
  if A.IsFalse and B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalOr(const A: boolean; const B: Bool3): Bool3;
begin
  if A or B.IsTrue  then
    Result := True3
  else
  if not A and B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalOr(const A: Bool3; const B: boolean): Bool3;
begin
  if A.IsTrue or B  then
    Result := True3
  else
  if A.IsFalse and not B then
    Result := False3
  else
    Result := Null3;
end;
{$ENDREGION}

{$REGION 'LogicalXor'}
class operator Bool3.LogicalXor(const A: Bool3Value; const B: Bool3): Bool3;
begin
  if A.IsTrue xor B.IsTrue  then
    Result := True3
  else
  if A.IsFalse and B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalXor(const A, B: Bool3): Bool3;
begin
  if A.IsTrue xor B.IsTrue  then
    Result := True3
  else
  if A.IsFalse and B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalXor(const A: Bool3; const B: Bool3Value): Bool3;
begin
  if A.IsTrue xor B.IsTrue  then
    Result := True3
  else
  if A.IsFalse and B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalXor(const A: boolean; const B: Bool3): Bool3;
begin
  if A xor B.IsTrue  then
    Result := True3
  else
  if not A and B.IsFalse then
    Result := False3
  else
    Result := Null3;
end;

class operator Bool3.LogicalXor(const A: Bool3; const B: boolean): Bool3;
begin
  if A.IsTrue xor B  then
    Result := True3
  else
  if A.IsFalse and not B then
    Result := False3
  else
    Result := Null3;
end;
{$ENDREGION}

function Bool3.GetIsFalse: boolean;
begin
  Result := FData = False3;
end;

function Bool3.GetIsNull: boolean;
begin
  Result := FData = Null3;
end;

function Bool3.GetIsTrue: boolean;
begin
  Result := FData = True3;
end;

function Bool3.ToString: string;
begin
  Result := FData.ToString;
end;
{$ENDREGION}

//// Test
//var
//  b2 : boolean;
//  b3 : bool3;
//
//initialization
//  if b3 and b2 then
//  ;

end{$WARNINGS OFF}.
{$ENDIF}

