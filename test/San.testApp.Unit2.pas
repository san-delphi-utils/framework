unit San.testApp.Unit2;

interface

uses
  San;

type
  TMEMouseDown = class(TMetaEventSeparate)
  protected
    function MatchSubscribeAttr(const AAttr : TMetaEvent.Subtypes.SubscribeAttribute) : boolean; override;

  public
    {$REGION 'type'}
    type
      Context = class
      private
        FX: integer;
        FY: integer;

      public
        property X : integer  read FX;
        property Y : integer  read FY;
      end;

      Event = TMetaEvent.Event<TMEMouseDown.Context>;
    {$ENDREGION}

    class function EventName : string; override;
    class function Description : string; override;

    function Fire(const ASender : TObject; const AX, AY : integer) : TMetaEvent.Subtypes.TFiring;
  end;
  EvMouseDown = class(TMetaEvent.Subtypes.SubscribeAttribute);

  // !!! Не работает: не читаются атрибуты !!!
  TEventMouseDown = TMetaEvent.Event<TMEMouseDown.Context>;


  EvMouseUp = class(TMetaEvent.Subtypes.SubscribeAttribute);

  [MetaEv('MouseUp', 'MouseUp на основе TMetaEventEx'), EvMouseUp]
  TEMMouseUp = class(TMetaEventEx)
  public
    {$REGION 'type'}
    type
      Context = class
      private
        FX: integer;
        FY: integer;

      public
        property X : integer  read FX;
        property Y : integer  read FY;
      end;

      Event = TMetaEvent.Event<Context>;
    {$ENDREGION}

    function Fire(const ASender : TObject; const AX, AY : integer) : TMetaEvent.Subtypes.TFiring;
  end;

implementation


{$REGION 'TMEMouseDown'}
function TMEMouseDown.MatchSubscribeAttr(const AAttr: TMetaEvent.Subtypes.SubscribeAttribute): boolean;
begin
  Result := AAttr is EvMouseDown;
end;

function TMEMouseDown.Fire(const ASender: TObject; const AX,
  AY: integer): TMetaEvent.Subtypes.TFiring;
var
  Context : TMEMouseDown.Context;
begin
  Context := TMEMouseDown.Context.Create();

  Context.FX := AX;
  Context.FY := AY;

  Result := inherited Fire(ASender, Context);
end;

class function TMEMouseDown.Description: string;
begin
  Result := '';
end;

class function TMEMouseDown.EventName: string;
begin
  Result := 'MouseDown';
end;
{$ENDREGION}

{$REGION 'TEMMouseUp'}
function TEMMouseUp.Fire(const ASender: TObject; const AX, AY: integer): TMetaEvent.Subtypes.TFiring;
var
  Ctx : Context;
begin
  Ctx := Context.Create();

  Ctx.FX := AX;
  Ctx.FY := AY;

  Result := inherited Fire(ASender, Ctx);
end;
{$ENDREGION}

end.
