unit San.testApp.Form.Main;

interface

uses
  WinApi.Windows, WinApi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,

  San,

  San.testApp.Unit2, San.testApp.OmniMREW
  ;

const
  EV_10 = 10;

type
  TfmMain = class(TForm, IEventsSubscriber)
    pnl1: TPanel;
    mmLog: TMemo;
    btn1: TButton;
    btnMouseDownDel: TButton;
    btnMouseDownAdd: TButton;
    chHandled: TCheckBox;
    btnMRSW: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MouseEnterOn(Sender: TObject);
    procedure MouseDownOn(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btn1Click(Sender: TObject);
    procedure btnMouseDownDelClick(Sender: TObject);
    procedure btnMouseDownAddClick(Sender: TObject);
    procedure MouseUpOn(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnMRSWClick(Sender: TObject);

  private
    FEventSpace : TEventSpace;
    FME_MouseEnter : TMetaEvent;
    FME_MouseDown : TMEMouseDown;
    FME_MouseUp : TEMMouseUp;

    FGuard : IEventsSubscribeGuard;

  protected
    {$REGION 'IEventsSubscriber'}
    function GetGuard : IEventsSubscribeGuard;
    {$ENDREGION}

  public
    procedure MouseEnterFirst (const AEvent : TMetaEvent.Event);
    procedure MouseEnterSecond (const AEvent : TMetaEvent.Event);
    procedure MouseEnterAll (const AEvent : TMetaEvent.Event);

    [EvMouseDown(EV_10)]
    procedure MouseDown1 (const AEvent : TMetaEvent.Event);

    [EvMouseDown(EV_10, -50)]
    procedure MouseDown2 (const AEvent : TMetaEvent.Event<TMEMouseDown.Context>);

    [EvMouseDown(EV_10, -50)]
    procedure MouseDown3 (const AEvent : TEventMouseDown);

    [EvMouseDown(EV_10, 2)]
    procedure MouseDown4 (const AEvent : TMetaEvent.Event);

    [EvMouseDown(EV_10, 3)]
    procedure MouseDown5 (const AEvent : TMEMouseDown.Event);

    [EvMouseUp(EV_10, 8)]
    procedure MouseUp1 (const AEvent : TEMMouseUp.Event);
  end;

var
  fmMain: TfmMain;

implementation

{$R *.dfm}

{$REGION 'TfmMain'}
function TfmMain.GetGuard: IEventsSubscribeGuard;
begin
  Result := FGuard;
end;

procedure TfmMain.btn1Click(Sender: TObject);

//  procedure A(var B);
//  var
//    m : TMethod;
//  begin
//    m := TMethod(B);
//
//    ShowMessageFmt('Code: %p; Data: %p; Form: %p', [m.Code, m.Data, Pointer(Self)]);
//  end;

begin
//  A(MouseDown1);
//  m := TMethod(MouseDown1);
//
//  ShowMessageFmt('Code: %p; Data: %p; Form: %p', [m.Code, m.Data, Pointer(Self)]);
end;

procedure TfmMain.btnMouseDownAddClick(Sender: TObject);
begin
  FEventSpace.SubscribeByAttrs(Self, EV_10);
end;

procedure TfmMain.btnMouseDownDelClick(Sender: TObject);
begin
  FEventSpace.UnSubscribeByAttrs(Self, EV_10);
end;

procedure TfmMain.btnMRSWClick(Sender: TObject);
var
  MRSW : TOmniMREW;
begin
  MRSW := TOmniMREW.Create;

//  MRSW.EnterReadLock;
  MRSW.EnterWriteLock;
  try

//    MRSW.EnterWriteLock;
    MRSW.EnterWriteLock;
//    MRSW.EnterReadLock;

    Sleep(1);

  finally
    MRSW.ExitReadLock;
  end;

end;

procedure TfmMain.FormCreate(Sender: TObject);
begin
  {$REGION 'Источник событий'}

    {$REGION 'Пространство'}
    FEventSpace := TEventSpace.Create();
    {$ENDREGION}

    {$REGION 'Регистрация всех типов событий'}
    TMetaEventSimple.CreateInSpace('MouseEnter', '', FEventSpace, FME_MouseEnter);
    TMEMouseDown.CreateInSpace(FEventSpace, FME_MouseDown);
    TEMMouseUp.CreateInSpace(FEventSpace, FME_MouseUp);
    {$ENDREGION}

  {$ENDREGION}

  {$REGION 'Подписчик'}

    {$REGION 'Соблюдение необходимых условий подписки'}
    FGuard := FEventSpace.CreateEventsSubscribeGuard(Self);
    {$ENDREGION}

    {$REGION 'Добавление одработчиков событий'}

      {$REGION 'FME_MouseEnter'}
      FME_MouseEnter.Subscribe(MouseEnterSecond).Ex(-2, Self);
      FME_MouseEnter.Subscribe(MouseEnterFirst).Ex(-3, Self);
      FME_MouseEnter.Subscribe(MouseEnterAll).Std();
      {$ENDREGION}

      {$REGION 'FME_MouseDown'}
//      FME_MouseDown.Subscribe(MouseDown1).Std();
//      FME_MouseDown.Subscribe(@TfmMain.MouseDown2, Self).Std();
//      FME_MouseDown.Subscribe<TMEMouseDown.Context>(MouseDown2).Std();
//      FME_MouseDown.Subscribe(@TfmMain.MouseDown3, Self).Std();
//      FME_MouseDown.Subscribe<TMEMouseDown.Context>(MouseDown3).Std();
      {$ENDREGION}

    {$ENDREGION}

  {$ENDREGION}
end;

procedure TfmMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FEventSpace);
end;

procedure TfmMain.MouseUpOn(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FME_MouseUp.Fire(Sender, X, Y).Std();
end;

procedure TfmMain.MouseEnterOn(Sender: TObject);
begin
  FME_MouseEnter.Fire(Sender).Std();
end;

procedure TfmMain.MouseDownOn(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FME_MouseDown.Fire(Sender, X, Y).Std();
end;

procedure TfmMain.MouseEnterFirst(const AEvent: TMetaEvent.Event);
var
  f : TForm;
begin
  f := AEvent.Sender as TForm;

  mmLog.Lines.Add(f.Caption + '!');
end;

procedure TfmMain.MouseEnterSecond(const AEvent: TMetaEvent.Event);
var
  f : TForm;
begin
  f := AEvent.Sender as TForm;

  mmLog.Lines.Add(f.Caption + '#');
end;

procedure TfmMain.MouseEnterAll(const AEvent: TMetaEvent.Event);
begin
  mmLog.Lines.Add((AEvent.Sender as TComponent).Name);
end;

procedure TfmMain.MouseDown1(const AEvent: TMetaEvent.Event);
var
  P : TMEMouseDown.Context;
begin
  P := AEvent.Context as TMEMouseDown.Context;

  mmLog.Lines.Add( Format('MouseDown1 for %s at (%d, %d)', [(AEvent.Sender as TComponent).Name, P.X, P.Y]) );
end;

procedure TfmMain.MouseDown2(const AEvent: TMetaEvent.Event<TMEMouseDown.Context>);
begin
  AEvent.Handled := chHandled.Checked;

  mmLog.Lines.Add( Format('MouseDown2 for %s at (%d, %d)', [(AEvent.Sender as TComponent).Name, AEvent.Context.X, AEvent.Context.Y]) );
end;

procedure TfmMain.MouseDown3(const AEvent: TEventMouseDown);
begin
  mmLog.Lines.Add( Format('MouseDown3 for %s at (%d, %d)', [(AEvent.Sender as TComponent).Name, AEvent.Context.X, AEvent.Context.Y]) );
end;

procedure TfmMain.MouseDown4(const AEvent: TMetaEvent.Event);
var
  P : TMEMouseDown.Context;
begin
  P := AEvent.Context as TMEMouseDown.Context;

  mmLog.Lines.Add( Format('MouseDown4 for %s at (%d, %d)', [(AEvent.Sender as TComponent).Name, P.X, P.Y]) );
end;

procedure TfmMain.MouseDown5(const AEvent: TMEMouseDown.Event);
begin
  mmLog.Lines.Add( Format('MouseDown5 for %s at (%d, %d)', [(AEvent.Sender as TComponent).Name, AEvent.Context.X, AEvent.Context.Y]) );
end;

procedure TfmMain.MouseUp1(const AEvent: TEMMouseUp.Event);
begin
  mmLog.Lines.Add( Format('MouseUp1 for %s at (%d, %d)'#13#10'MetaEvent.Name: %s'#13#10'MetaEvent.Description: %s', [(AEvent.Sender as TComponent).Name, AEvent.Context.X, AEvent.Context.Y, AEvent.MetaEvent.EventName, AEvent.MetaEvent.Description]) );
end;

{$ENDREGION}



end.
