object fmMain: TfmMain
  Left = 0
  Top = 0
  Caption = 'fmMain'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnMouseDown = MouseDownOn
  OnMouseEnter = MouseEnterOn
  OnMouseUp = MouseUpOn
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 40
    Top = 32
    Width = 289
    Height = 121
    Caption = 'pnl1'
    TabOrder = 0
    OnMouseDown = MouseDownOn
    OnMouseEnter = MouseEnterOn
    OnMouseUp = MouseUpOn
  end
  object mmLog: TMemo
    Left = 360
    Top = 0
    Width = 275
    Height = 299
    Align = alRight
    Lines.Strings = (
      'mmLog')
    TabOrder = 1
  end
  object btn1: TButton
    Left = 40
    Top = 184
    Width = 75
    Height = 25
    Caption = 'btn1'
    TabOrder = 2
    OnClick = btn1Click
  end
  object btnMouseDownDel: TButton
    Left = 145
    Top = 206
    Width = 89
    Height = 25
    Caption = 'MouseDown-'
    TabOrder = 3
    OnClick = btnMouseDownDelClick
  end
  object btnMouseDownAdd: TButton
    Left = 145
    Top = 175
    Width = 89
    Height = 25
    Caption = 'MouseDown+'
    TabOrder = 4
    OnClick = btnMouseDownAddClick
  end
  object chHandled: TCheckBox
    Left = 42
    Top = 215
    Width = 97
    Height = 17
    Caption = 'Handled'
    TabOrder = 5
  end
  object btnMRSW: TButton
    Left = 120
    Top = 264
    Width = 75
    Height = 25
    Caption = 'btnMRSW'
    TabOrder = 6
    OnClick = btnMRSWClick
  end
end
