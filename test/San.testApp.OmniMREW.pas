unit San.testApp.OmniMREW;

interface

type
  TOmniMREW = record
  strict private
    omrewReference: integer;      //Reference.Bit0 is 'writing in progress' flag
  public
    class function Create : TOmniMREW; static;

    procedure EnterReadLock; //inline;
    procedure EnterWriteLock; //inline;
    procedure ExitReadLock; //inline;
    procedure ExitWriteLock; //inline;
  end; { TOmniMREW }

implementation

uses
  Windows;

class function TOmniMREW.Create: TOmniMREW;
begin
  FillChar(Result, SizeOf(TOmniMREW), 0);
end;

procedure TOmniMREW.EnterReadLock;
var
  currentReference: integer;
begin
  //Wait on writer to reset write flag so Reference.Bit0 must be 0 than increase Reference
  repeat
    currentReference := omrewReference AND NOT 1;
  until currentReference = InterlockedCompareExchange(omrewReference, currentReference + 2, currentReference);
end; { TOmniMREW.EnterReadLock }

procedure TOmniMREW.EnterWriteLock;
var
  currentReference: integer;
begin
  //Wait on writer to reset write flag so omrewReference.Bit0 must be 0 then set omrewReference.Bit0
  repeat
    currentReference := omrewReference AND NOT 1;
  until currentReference = InterlockedCompareExchange(omrewReference, currentReference + 1, currentReference);
  //Now wait on all readers
  repeat
  until omrewReference = 1;
end; { TOmniMREW.EnterWriteLock }

procedure TOmniMREW.ExitReadLock;
begin
  //Decrease omrewReference
  InterlockedExchangeAdd(omrewReference, -2);
end; { TOmniMREW.ExitReadLock }

procedure TOmniMREW.ExitWriteLock;
begin
  omrewReference := 0;
end; { TOmniMREW.ExitWriteLock }

end.
