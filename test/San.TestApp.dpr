program San.TestApp;

uses
  Vcl.Forms,
  San.testApp.Form.Main in 'San.testApp.Form.Main.pas' {fmMain},
  San.testApp.Unit2 in 'San.testApp.Unit2.pas',
  San.testApp.OmniMREW in 'San.testApp.OmniMREW.pas';

{$R *.res}

begin
  System.ReportMemoryLeaksOnShutdown := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmMain, fmMain);
  Application.Run;
end.
