{*******************************************************}
{                                                       }
{       Строка в верхнем регистре.                      }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2016              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Механизм хранения строк в верхнем регистре с приведением регистра по необходимости.
/// </summary>
unit San.UpperString;
{$I San.inc}
{.$DEFINE UPPER_STR_INLINE}
{.$DEFINE UPPER_STR_USE_SAME}
interface

{$REGION 'uses'}
uses
  System.SysUtils, System.Generics.Defaults, System.Generics.Collections,

  {$IF RTLVersion >= 31}
  System.Hash,
  {$ENDIF}

  System.TypInfo;
{$ENDREGION}
{$ENDIF}

type
  /// <summary>
  ///   Указатель на строку в верхнем регистре.
  /// </summary>
  PUpperString =
  {$IFDEF USE_ALIASES}
    San.UpperString.PUpperString;
  {$ELSE}
    ^TUpperString;
  {$ENDIF}

  /// <summary>
  ///   Строка в верхнем регистре.
  /// </summary>
  /// <remarks>
  ///   При использовании в Dictionary в качестве ключа,
  ///   надо передать TUpperStringEqualityComparer.Create
  ///   в конструктор Dictionary.
  /// </remarks>
  TUpperString =
  {$IFDEF USE_ALIASES}
    San.UpperString.TUpperString;
  {$ELSE}
    record
  strict private
    function GetIsEmpty: boolean;

  private
    FData : string;

  public
    class operator Implicit(const AStr : string): TUpperString; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}
    class operator Implicit(const AUpStr : TUpperString): string; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}
    {$IFNDEF NEXTGEN}
      class operator Implicit(const ASymbolName : TSymbolName): TUpperString; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}
    {$ENDIF}
    class operator Equal(const A, B : TUpperString): Boolean; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}
    class operator Equal(const A : string; const B : TUpperString): Boolean; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}
    class operator Equal(const A : TUpperString; const B : string): Boolean; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}
    class operator NotEqual(const A, B : TUpperString): Boolean; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}
    class operator NotEqual(const A : string; const B : TUpperString): Boolean; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}
    class operator NotEqual(const A : TUpperString; const B : string): Boolean; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}
    class operator Add(const A, B : TUpperString): TUpperString; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}
    class operator Add(const A : string; const B : TUpperString): TUpperString; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}
    class operator Add(const A : TUpperString; const B : string): TUpperString; {$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}

    /// <summary>
    ///   Явный доступ к строке.
    /// </summary>
    /// <remarks>
    ///   Нужен, например, для передачи в аргументы функции Format().
    /// </remarks>
    function ToString() : string;{$IFDEF UPPER_STR_INLINE}inline;{$ENDIF}

    /// <summary>
    ///   Является ли пустой строкой.
    /// </summary>
    property IsEmpty : boolean  read GetIsEmpty;
  end;
  {$ENDIF}

  /// <summary>
  ///   Сравнитель строк в верхнем регистре.
  /// </summary>
  /// <remarks>
  ///   Необходим для использования в Dictionary.
  /// </remarks>
  TUpperStringEqualityComparer =
  {$IFDEF USE_ALIASES}
    San.UpperString.TUpperStringEqualityComparer;
  {$ELSE}
    class(TEqualityComparer<TUpperString>)
  public
    function Equals(const Left, Right: TUpperString): Boolean; override;
    function GetHashCode(const Value: TUpperString): Integer; override;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
implementation

{$REGION 'TUpperString'}
class operator TUpperString.Implicit(const AStr: string): TUpperString;
begin
  Result.FData := AnsiUpperCase(AStr);
end;

class operator TUpperString.Implicit(const AUpStr: TUpperString): string;
begin
  Result := AUpStr.FData;
end;

{$IFNDEF NEXTGEN}
class operator TUpperString.Implicit(
  const ASymbolName: TSymbolName): TUpperString;
begin
  Result.FData := AnsiUpperCase(string(ASymbolName));
end;
{$ENDIF}

class operator TUpperString.Equal(const A, B: TUpperString): Boolean;
begin
  Result := {$IFDEF UPPER_STR_USE_SAME}AnsiSameStr(A.FData, B.FData){$ELSE}A.FData = B.FData{$ENDIF};
end;

class operator TUpperString.Equal(const A: string;
  const B: TUpperString): Boolean;
begin
  Result := {$IFDEF UPPER_STR_USE_SAME}AnsiSameStr(AnsiUpperCase(A), B.FData){$ELSE}AnsiUpperCase(A) = B.FData{$ENDIF};
end;

class operator TUpperString.Equal(const A: TUpperString;
  const B: string): Boolean;
begin
  Result := {$IFDEF UPPER_STR_USE_SAME}AnsiSameStr(A.FData, AnsiUpperCase(B)){$ELSE}A.FData = AnsiUpperCase(B){$ENDIF};
end;

function TUpperString.GetIsEmpty: boolean;
begin
  Result := FData = '';
end;

class operator TUpperString.NotEqual(const A, B: TUpperString): Boolean;
begin
  Result := {$IFDEF UPPER_STR_USE_SAME}not AnsiSameStr(A.FData, B.FData){$ELSE}A.FData <> B.FData{$ENDIF};
end;

class operator TUpperString.NotEqual(const A: string;
  const B: TUpperString): Boolean;
begin
  Result := {$IFDEF UPPER_STR_USE_SAME}not AnsiSameStr(AnsiUpperCase(A), B.FData){$ELSE}AnsiUpperCase(A) <> B.FData{$ENDIF};
end;

class operator TUpperString.NotEqual(const A: TUpperString;
  const B: string): Boolean;
begin
  Result := {$IFDEF UPPER_STR_USE_SAME}not AnsiSameStr(A.FData, AnsiUpperCase(B)){$ELSE}A.FData <> AnsiUpperCase(B){$ENDIF};
end;

class operator TUpperString.Add(const A, B: TUpperString): TUpperString;
begin
  Result.FData := A.FData + B.FData;
end;

class operator TUpperString.Add(const A: string; const B: TUpperString): TUpperString;
begin
  Result.FData := AnsiUpperCase(A) + B.FData;
end;

class operator TUpperString.Add(const A: TUpperString; const B: string): TUpperString;
begin
  Result.FData := A.FData + AnsiUpperCase(B);
end;

function TUpperString.ToString(): string;
begin
  Result := FData;
end;
{$ENDREGION}

{$REGION 'TUpperStringEqualityComparer'}
function TUpperStringEqualityComparer.Equals(const Left,
  Right: TUpperString): Boolean;
begin
  Result := Left = Right;
end;

function TUpperStringEqualityComparer.GetHashCode(
  const Value: TUpperString): Integer;
begin
  {$IF RTLVersion >= 31}
    Result :=System.Hash.THashBobJenkins.GetHashValue(Value.FData);
  {$ELSE}
    Result := BobJenkinsHash(PChar(Value.FData)^, SizeOf(Char) * Length(Value.FData), 0);
  {$ENDIF}
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}

