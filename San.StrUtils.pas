{*******************************************************}
{                                                       }
{       Работа со строками.                             }
{                                                       }
{       Разработано А.В.Станцо                          }
{         https://www.avstantso.ru/programming/         }
{                                                       }
{       Copyright (C) 2018              Станцо А.В.     }
{                                                       }
{*******************************************************}
{$IFNDEF USE_ALIASES}
/// <summary>
///   Работа со строками.
/// </summary>
unit San.StrUtils;
{$I San.inc}
interface

{$REGION 'uses'}
uses
  {$REGION 'System'}
  System.SysUtils, System.TypInfo, System.Generics.Collections, System.StrUtils,
  {$ENDREGION}

  San.Intfs.Holders;
{$ENDREGION}

{$ENDIF}

type
  /// <summary>
  ///   Пара строка-строка.
  /// </summary>
  TPairStrStr =
  {$IFDEF USE_ALIASES}
    San.StrUtils.TPairStrStr;
  {$ELSE}
  record
    Key, Value : string;
    constructor Create(const AKey : string; const AValue: string); overload;
    class function Create() : TPairStrStr; overload; static;
    function ToString(const ADelimiter : string = '=') : string;
  end;
  {$ENDIF}

{$IFNDEF USE_ALIASES}
type
  /// <summary>
  ///   Доступ к методам работы со строками
  /// </summary>
  Api = record
  strict private

  public
    /// <summary>
    ///   Разделить сторку на имя и значение
    /// </summary>
    class function NameValue(const AString : string; const ADelimiter : string = '=') : TPairStrStr; static;

    /// <summary>
    ///   Фильтровать строку (удалить всё, что не входит в фильтр)
    /// </summary>
    class function Filter(const AString : string; const AFilterChars : TSysCharSet) : string; static;

  end;

implementation

{$REGION 'TPairStrStr'}
constructor TPairStrStr.Create(const AKey, AValue: string);
begin
  Key := AKey;
  Value := AValue;
end;

class function TPairStrStr.Create: TPairStrStr;
begin
  Result := TPairStrStr.Create('', '');
end;

function TPairStrStr.ToString(const ADelimiter: string): string;
begin
  Result := Key + ADelimiter + Value;
end;
{$ENDREGION}

{$REGION 'Api'}
class function Api.NameValue(const AString, ADelimiter: string): TPairStrStr;
var
  p : integer;
begin
  p := Pos(ADelimiter, AString);
  Result := TPairStrStr.Create(Copy(AString, 1, p - 1), Copy(AString, p + Length(ADelimiter)));
end;

class function Api.Filter(const AString: string;
  const AFilterChars: TSysCharSet): string;
var
  i : integer;
begin
  Result := AString;
  for i := Length(Result) downto 1 do
    if not CharInSet(Result[i], AFilterChars) then
      System.Delete(Result, i, 1);
end;
{$ENDREGION}

end{$WARNINGS OFF}.
{$ENDIF}
